(* $Id$ *)

(* Tests Nn_blocklist *)

(* The underlying RangeMap implementation is already covered by
   ps_rangemap, so we do simply assume here this part works correctly.
   It is mainly interesting here whether the blocks are handled right
   when they refer to various datanodes
 *)

open Plasma_util
open Pfs_rpcapi_aux
open Printf

let mk_block ident index block =
  { index = index;
    node = "";
    identity = ident;
    block = block;
    length = 1L;
    node_alive = true;
    checksum = None;
    inode_seqno = 0L;
    inode_committed = true;
    ticket = Nn_blocklist.empty_ticket
  }

let mk_blocks ident index block number =
  let rec loop k =
    if k < number then
      let b = mk_block ident (Int64.add index k) (Int64.add block k) in
      b :: loop (Int64.succ k)
    else
      [] in
  loop 0L

let dest_block b =
  (b.identity, b.index, b.block, b.length)

let print_blocks l =
  String.concat " , "
    (List.map (fun (id,idx,b,l) -> sprintf "%s:%Ld/%Ld/%Ld" id idx b l) l)

let ( >> ) = fun n f -> if n=0 then f() else n

let cmp_blocks b1 b2 =
  String.compare b1.identity b2.identity
  >> (fun () ->
	Int64.compare b1.index b2.index
     )


let do_test name f =
  try
    printf "Test: %s %!" name;
    if f() then
      printf "PASSED\n%!"
    else
      printf "FAILED\n%!"
  with
    | err ->
	printf "EXCEPTION %s\n%!" (Printexc.to_string err)

let test1() =
  do_test "test1" (
    fun () ->
      let l =
	mk_blocks "d0" 0L 100L 10L @
	mk_blocks "d1" 0L 100L 10L @
        mk_blocks "d2" 0L 200L 10L @
        mk_blocks "d3" 0L 200L 10L in
      let bl = Nn_blocklist.to_blocklist l in
      let l1 = Nn_blocklist.to_blockinfo_list bl in
      let l2 = List.sort cmp_blocks l1 in
      let l3 = List.map dest_block l2 in
      l3 = [ "d0", 0L, 100L, 10L;
	     "d1", 0L, 100L, 10L;
	     "d2", 0L, 200L, 10L;
	     "d3", 0L, 200L, 10L ]
  )


let test2() =
  do_test "test2" (
    fun () ->
      let l =
	mk_blocks "d0" 0L 100L 10L @
	mk_blocks "d1" 0L 100L 10L @
        mk_blocks "d2" 0L 200L 10L @
        mk_blocks "d3" 0L 200L 10L in
      let bl0 = Nn_blocklist.to_blocklist l in
      let bl = Nn_blocklist.sub (2L, 8L) bl0 in
      let l1 = Nn_blocklist.to_blockinfo_list bl in
      let l2 = List.sort cmp_blocks l1 in
      let l3 = List.map dest_block l2 in
      l3 = [ "d0", 2L, 102L, 7L;
	     "d1", 2L, 102L, 7L;
	     "d2", 2L, 202L, 7L;
	     "d3", 2L, 202L, 7L ]
  )

let test3() =
  do_test "test3" (
    fun () ->
      let l =
	mk_blocks "d0" 0L 100L 10L @
	mk_blocks "d1" 0L 100L 10L @
        mk_blocks "d0" 15L 200L 10L @
        mk_blocks "d1" 15L 200L 10L in
      let bl0 = Nn_blocklist.to_blocklist l in
      let bl = Nn_blocklist.sub (5L, 15L) bl0 in
      let l1 = Nn_blocklist.to_blockinfo_list bl in
      let l2 = List.sort cmp_blocks l1 in
      let l3 = List.map dest_block l2 in
      l3 = [ "d0", 5L, 105L, 5L;
	     "d0", 15L, 200L, 1L;
	     "d1", 5L, 105L, 5L;
	     "d1", 15L, 200L, 1L ]
  )

let test4() =
  do_test "test4" (
    fun () ->
      let l =
	mk_blocks "d0" 0L 100L 10L @
	mk_blocks "d1" 0L 100L 10L @
        mk_blocks "d0" 15L 200L 10L @
        mk_blocks "d1" 15L 200L 10L in
      let bl0 = Nn_blocklist.to_blocklist l in
      let bl = Nn_blocklist.remove (5L, 6L) bl0 in
      let l1 = Nn_blocklist.to_blockinfo_list bl in
      let l2 = List.sort cmp_blocks l1 in
      let l3 = List.map dest_block l2 in
      l3 = [ "d0", 0L, 100L, 5L;
	     "d0", 7L, 107L, 3L;
	     "d0", 15L, 200L, 10L;
	     "d1", 0L, 100L, 5L;
	     "d1", 7L, 107L, 3L;
	     "d1", 15L, 200L, 10L ]
  )

let test5() =
  do_test "test5" (
    fun () ->
      let l1 =
	mk_blocks "d0" 0L 100L 10L @
	mk_blocks "d1" 0L 100L 10L in
      let l2 =
        mk_blocks "d0" 10L 200L 10L @
        mk_blocks "d1" 10L 110L 10L @
	  mk_blocks "d2" 0L 300L 20L in
      let bl1 = Nn_blocklist.to_blocklist l1 in
      let bl2 = Nn_blocklist.to_blocklist l2 in
      let bl = Nn_blocklist.merge bl1 bl2 in
      let l1 = Nn_blocklist.to_blockinfo_list bl in
      let l2 = List.sort cmp_blocks l1 in
      let l3 = List.map dest_block l2 in
      l3 = [ "d0", 0L, 100L, 10L;
	     "d0", 10L, 200L, 10L;
	     "d1", 0L, 100L, 20L;
	     "d2", 0L, 300L, 20L ]
  )

let test6() =
  do_test "test6" (
    fun () ->
      let l1 =
	mk_blocks "d0" 0L 100L 10L @
	mk_blocks "d1" 0L 100L 10L in
      let l2 =
        mk_blocks "d0" 5L 200L 1L @
        mk_blocks "d1" 10L 110L 10L in
      let bl1 = Nn_blocklist.to_blocklist l1 in
      let bl2 = Nn_blocklist.to_blocklist l2 in
      let bl = Nn_blocklist.merge bl1 bl2 in
      let l1 = Nn_blocklist.to_blockinfo_list bl in
      let l2 = List.sort cmp_blocks l1 in
      let l3 = List.map dest_block l2 in
      l3 = [ "d0", 0L, 100L, 5L;
	     "d0", 5L, 200L, 1L;
	     "d0", 6L, 106L, 4L;
	     "d1", 0L, 100L, 20L ]
  )
		     
let () =
  printf "ps_blocklist\n%!";
  test1();
  test2();
  test3();
  test4();
  test5();
  test6();
  printf "ps_blocklist done\n%!"
