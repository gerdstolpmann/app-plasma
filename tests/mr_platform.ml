(* Test Platform *)

open Printf

let prep_data seg_len =
  let sixtyfour = 64 * 1024 * 1024 in  (* 64 M *)
  let m = Bigarray.Array1.create Bigarray.char Bigarray.c_layout sixtyfour in
  Bigarray.Array1.fill m 'X';
  let k = ref 0 in
  let j = ref 1 in
  let cnt = ref 0 in
  while !k < sixtyfour do
    m.{ !k } <- '\n';
    k := !k + seg_len + !j;
    incr j;
    if !j = 100 then j := 0;
    incr cnt;
  done;
  printf "Number of LF chars for seg_len=%d: %d\n%!" seg_len !cnt;
  m


let extra_work seg_len =
  (* This simulates some extra work that also needs to be done, in addition
     to line splitting
   *)
  let acc = ref 0 in
  for k=0 to seg_len do
    acc := !acc + k
  done

let test_single_thread m seg_len =
  let t0 = Unix.gettimeofday() in
  let n = Bigarray.Array1.dim m in
  let k = ref 0 in
  let j = ref 1 in
  let cursor = ref 0 in
  while !k < n do
    let p = Platform.find_lf m !cursor n in
    assert(p = !k);
    cursor := p+1;
    k := !k + seg_len + !j;
    incr j;
    if !j = 100 then j := 0;
    extra_work seg_len
  done;
  let t1 = Unix.gettimeofday() in
  printf
    "Time single-threaded, segment length=%d: %.3f\n%!" 
    seg_len (t1 -. t0)

  
let test_worker_thread m seg_len =
  let open Platform.Line_splitter in
  let t0 = Unix.gettimeofday() in
  let n = Bigarray.Array1.dim m in
  let k = ref 0 in
  let j = ref 1 in
  let e = create m in
  while !k < n do
    let p = next_lf e in
    assert(p = !k);
    k := !k + seg_len + !j;
    incr j;
    if !j = 100 then j := 0;
    extra_work seg_len
  done;
  stop e;
  let t1 = Unix.gettimeofday() in
  printf
    "Time with worker thread, segment length=%d: %.3f\n%!" 
    seg_len (t1 -. t0)

  
let () =
  let m = prep_data 20 in
  test_single_thread m 20;
  test_worker_thread m 20;

  let m = prep_data 100 in
  test_single_thread m 100;
  test_worker_thread m 100;

  let m = prep_data 300 in
  test_single_thread m 300;
  test_worker_thread m 300;

  let m = prep_data 1000 in
  test_single_thread m 1000;
  test_worker_thread m 1000;

  let m = prep_data 3000 in
  test_single_thread m 3000;
  test_worker_thread m 3000;

  let m = prep_data 10000 in
  test_single_thread m 10000;
  test_worker_thread m 10000;

  ()
