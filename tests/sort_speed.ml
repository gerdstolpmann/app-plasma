open Printf

let measure name f =
  let t0 = Unix.gettimeofday() in
  f();
  let t1 = Unix.gettimeofday() in
  printf "Time for %s: %.2f\n%!" name (t1-.t0)

let () =
  let n = 1_000_000 in
  let a = Array.init n (fun i -> n-i) in
  measure
    "Array.sort"
    (fun () -> Array.sort Pervasives.compare a);

  let a = Array.init n (fun i -> n-i) in
  measure
    "Mapred_sort.merge_sort"
    (fun () -> Mapred_sort.merge_sort Pervasives.compare a);

  let ba = Bigarray.Array1.create Bigarray.int Bigarray.c_layout n in
  for k = 0 to n-1 do
    ba.{k} <- n-k
  done;
  measure
    "Mapred_sort.ba_merge_sort"
    (fun () -> Mapred_sort.ba_merge_sort Pervasives.compare ba)


    
