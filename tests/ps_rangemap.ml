(* $Id$ *)

(* Tests whether Plasma_util.RangeMap is ok *)

(* First Test Round: Only I64RM, i.e. all ranges are mergable 

   1. add 20-29, 60-69, 40-49  (add separate range)
   2. delete 40-49             (del separate range)
   3. add 80-89                (add separate range on the right)
   4. add 0-9                  (add separate range on the left)
   5. sub 20-29
   6. sub 20-28
   7. sub 21-29
   8. sub 21-28
   9. sub 21-68
  10. sub 32-33
  11. sub 0-99
  12. sub 0-0
  13. sub 89-89
  14. del 20-29
  15. del 20-28
  16. del 21-29
  17. del 21-28
  18. del 21-68
  19. del 32-33
  20. del 0-99
  21. del 0-0
  22. del 89-89
  23. add 30-31
  24. add 18-19
  25. add 32-59
*)

open Plasma_util
open Printf

let i64rm_add key t =
  I64RM.add key key t


let i64rm_list rm =
  List.rev
    (I64RM.fold
       (fun (min,max) _ acc ->
	  (min,max) :: acc
       )
       rm
       [])

let i64rm_string rm =
  String.concat ","
    (List.map (fun (min,max) -> sprintf "%Ld-%Ld" min max) (i64rm_list rm))

let check name f =
  try
    printf "Test %s: %!" name;
    let b = f() in
    if b then
      printf "passed\n%!"
    else
      printf "FAILED\n%!"
  with
    | err ->
	printf "EXCEPTION: %s\n%!" (Printexc.to_string err)


let round1() =
  printf "ps_rangemap round 1\n%!";
  let s0 = I64RM.empty in

  let s1 = i64rm_add (40L,49L) (i64rm_add (60L,69L) (i64rm_add (20L,29L) s0)) in
  check "s1" (fun () -> i64rm_list s1 = [20L,29L; 40L,49L; 60L,69L]);

  let s2 = I64RM.remove (40L,49L) s1 in
  check "s2" (fun () -> i64rm_list s2 = [20L,29L; 60L,69L]);

  let s3 = i64rm_add (80L,89L) s2 in
  check "s3" (fun () -> i64rm_list s3 = [20L,29L; 60L,69L; 80L,89L]);

  let s4 = i64rm_add (0L,9L) s3 in
  check "s4" (fun () -> i64rm_list s4 = [0L,9L; 20L,29L; 60L,69L; 80L,89L]);

  let s5 = I64RM.sub (20L,29L) s4 in
  check "s5" (fun () -> i64rm_list s5 = [ 20L, 29L ]);

  let s6 = I64RM.sub (20L,28L) s4 in
  check "s6" (fun () -> i64rm_list s6 = [ 20L, 28L ]);

  let s7 = I64RM.sub (21L,29L) s4 in
  check "s7" (fun () -> i64rm_list s7 = [ 21L, 29L ]);

  let s8 = I64RM.sub (21L,28L) s4 in
  check "s8" (fun () -> i64rm_list s8 = [ 21L, 28L ]);

  let s9 = I64RM.sub (21L,68L) s4 in
  check "s9" (fun () -> i64rm_list s9 = [ 21L, 29L; 60L, 68L ]);

  let s10 = I64RM.sub (32L,33L) s4 in
  check "s5" (fun () -> i64rm_list s10 = [ ]);

  let s11 = I64RM.sub (0L,99L) s4 in
  check "s11" (fun () -> i64rm_list s11 = [0L,9L; 20L,29L; 60L,69L; 80L,89L]);

  let s12 = I64RM.sub (0L,0L) s4 in
  check "s12" (fun () -> i64rm_list s12 = [ 0L, 0L ]);

  let s13 = I64RM.sub (89L,89L) s4 in
  check "s13" (fun () -> i64rm_list s13 = [ 89L, 89L ]);

  let s14 = I64RM.remove (20L,29L) s4 in
  check "s14" (fun () -> i64rm_list s14 = [ 0L, 9L; 60L,69L; 80L,89L ]);

  let s15 = I64RM.remove (20L,28L) s4 in
  check "s15"
    (fun () -> i64rm_list s15 = [ 0L, 9L; 29L,29L; 60L,69L; 80L,89L ]);

  let s16 = I64RM.remove (21L,29L) s4 in
  check "s16"
    (fun () -> i64rm_list s16 = [ 0L, 9L; 20L,20L; 60L,69L; 80L,89L ]);

  let s17 = I64RM.remove (21L,28L) s4 in
  check "s17"
    (fun () -> i64rm_list s17 = [ 0L, 9L; 20L,20L; 29L,29L; 60L,69L; 80L,89L ]);

  let s18 = I64RM.remove (21L,68L) s4 in
  check "s18"
    (fun () -> i64rm_list s18 = [ 0L, 9L; 20L,20L; 69L,69L; 80L,89L ]);

  let s19 = I64RM.remove (32L,33L) s4 in
  check "s19"
    (fun () -> i64rm_list s19 = [ 0L, 9L; 20L,29L; 60L,69L; 80L,89L ]);

  let s19a = I64RM.remove (30L,31L) s4 in
  check "s19a"
    (fun () -> i64rm_list s19a = [ 0L, 9L; 20L,29L; 60L,69L; 80L,89L ]);

  let s19b = I64RM.remove (18L,19L) s4 in
  check "s19b"
    (fun () -> i64rm_list s19b = [ 0L, 9L; 20L,29L; 60L,69L; 80L,89L ]);

  let s20 = I64RM.remove (0L,99L) s4 in
  check "s20" (fun () -> i64rm_list s20 = []);

  let s21 = I64RM.remove (0L,0L) s4 in
  check "s21"
    (fun () -> i64rm_list s21 = [ 1L, 9L; 20L,29L; 60L,69L; 80L,89L ]);

  let s22 = I64RM.remove (89L,89L) s4 in
  check "s22"
    (fun () -> i64rm_list s22 = [ 0L, 9L; 20L,29L; 60L,69L; 80L,88L ]);

  let s23 = i64rm_add (30L,31L) s4 in
  check "s23"
    (fun () -> i64rm_list s23 = [ 0L, 9L; 20L,31L; 60L,69L; 80L,89L ]);
  (* printf "s23: %s\n" (i64rm_string s23); *)

  let s24 = i64rm_add (18L,19L) s23 in
  check "s24"
    (fun () -> i64rm_list s24 = [ 0L, 9L; 18L,31L; 60L,69L; 80L,89L ]);

  let s25 = i64rm_add (32L,59L) s24 in
  check "s25" (fun () -> i64rm_list s25 = [ 0L, 9L; 18L,69L; 80L,89L ]);

  printf "ps_rangemap round 1 done\n%!";
  ()

(* Second test round: Use Trivial_blockrange

   u1: add 20-29, 60-69
   u2: add 40-49 SHIFTED
   u3: add 30-31
   u4: add 18-19
   u5: add 32-39
   u6: add 50-59 SHIFTED
   u7: del 40-49
   u8: add 40-49
 *)

module RM = RangeMap(I64)(Trivial_blockrange)

let rm_add ((min,max) as key) t =
  let v =
    { Trivial_blockrange.idx_from = min;
      block_from = min;
      len = Int64.to_int (Int64.sub max min) + 1;
    } in
  RM.add key v t

let rm_add_shifted ((min,max) as key) t =
  let v =
    { Trivial_blockrange.idx_from = min;
      block_from = Int64.add min 100L;
      len = Int64.to_int (Int64.sub max min) + 1;
    } in
  RM.add key v t

let rm_list rm =
  List.rev
    (RM.fold
       (fun (min,max) _ acc ->
	  (min,max) :: acc
       )
       rm
       [])

let round2() =
  printf "ps_rangemap round 2\n%!";
  let u0 = RM.empty in

  let u1 = rm_add (60L,69L) (rm_add (20L,29L) u0) in
  check "u1" (fun () -> rm_list u1 = [ 20L, 29L; 60L,69L ]);
  
  let u2 = rm_add_shifted (40L,49L) u1 in
  check "u2" (fun () -> rm_list u2 = [ 20L,29L; 40L,49L; 60L,69L ]);

  let u3 = rm_add (30L,31L) u2 in
  check "u3" (fun () -> rm_list u3 = [ 20L,31L; 40L,49L; 60L,69L ]);

  let u4 = rm_add (18L,19L) u3 in
  check "u4" (fun () -> rm_list u4 = [ 18L,31L; 40L,49L; 60L,69L ]);
  
  let u5 = rm_add (32L, 39L) u4 in
  check "u5" (fun () -> rm_list u5 = [ 18L,39L; 40L,49L; 60L,69L ]);

  let u6 = rm_add_shifted (50L,59L) u5 in
  check "u6" (fun () -> rm_list u6 = [ 18L,39L; 40L,59L; 60L,69L ]);

  let u7 = RM.remove (40L,49L) u6 in
  check "u7" (fun () -> rm_list u7 = [ 18L,39L; 50L,59L; 60L,69L ]);

  let u8 = rm_add (40L,49L) u7 in
  check "u8" (fun () -> rm_list u8 = [ 18L,49L; 50L,59L; 60L,69L ]);

  let u9 = RM.remove (45L, 55L) u8 in
  check "u9" (fun () -> rm_list u9 = [ 18L,44L; 56L,59L; 60L,69L ]);


  printf "ps_rangemap round 2 done\n%!";
  ()



let () =
  round1();
  round2()
