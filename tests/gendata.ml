open Printf

let () =
  let k = ref 65536 in
  let n = ref 10000 in
  let c = ref 1 in
  Arg.parse
    [ "-k", Arg.Set_int k,
      "   set number of different keys";

      "-n", Arg.Set_int n,
      "   set number of lines (each 100 bytes)";

      "-c", Arg.Set_int c,
      "   repeat the same line this number of times";
    ]
    (fun arg -> raise(Arg.Bad("Unexpected arg: " ^ arg)))
    "usage: gendata [options]";

  Random.self_init();

  let bits = ref 0 in
  let bits_cnt = ref 0 in

  let get7() =
    (* get 7 random bits 0..127 *)
    let k1 = !bits land 127 in
    let k1_cnt = min !bits_cnt 7 in
    bits := !bits lsr k1_cnt;
    bits_cnt := !bits_cnt - k1_cnt;
    let k2 =
      if k1_cnt < 7 then (
	bits := Random.bits();
	let k2_cnt = 7 - k1_cnt in
	let k2_mask = (1 lsl k2_cnt) - 1 in  (* k2_cnt times one bit *)
	let k2_bits = !bits land k2_mask in
	bits := !bits lsr k2_cnt;
	bits_cnt := 30 - k2_cnt;
	k2_bits lsl k1_cnt
      )
      else
	0 in
    k1 lor k2 in

  let q1 = Queue.create() in
  let q2 = Queue.create() in
  let r = ref 0 in

  let get_line() =
    if Queue.is_empty q1 then (
      if Queue.is_empty q2 || !r = !c then (
	let data = String.create 90 in
	for line = 1 to 145 do
	  let key = Random.int !k in
	  for i=0 to String.length data - 1 do
	    let x = (get7() lsl 23) mod 94 in  (* old: Random.int 94 *)
	    String.unsafe_set data i (Char.unsafe_chr(33 + x))
	  done;
	  let s = sprintf "%08x\t%s\n" key data in
	  Queue.add s q1;
	done;
	Queue.clear q2;
	r := 1
      )
      else (
	Queue.transfer q2 q1;
	incr r
      )
    );
    let line = Queue.take q1 in
    Queue.add line q2;
    line in

  for line = 1 to !n do
    print_string(get_line())
  done
