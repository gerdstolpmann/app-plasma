(* $Id$ *)

(* Random reads from a file using multi_read_e *)

open Printf
open Plasma_rpcapi_aux
open Uq_engines.Operators

let run c directoryname basename n =
  let trans = Plasma_client.start c in
  let dir_inode = Plasma_client.lookup trans directoryname false in
  let () = Plasma_client.commit trans in

  let ii = Plasma_client.regular_ii c 0o666 in
  
  for k = 0 to n-1 do
    let trans = Plasma_client.start c in
    let inode = Plasma_client.create_inode trans ii in
    Plasma_client.link_at trans dir_inode (basename ^ string_of_int k) inode;
    Plasma_client.commit trans
  done;
  printf "Done\n%!"
	  

let () =
  let cluster = ref None in
  let namenodes = ref [] in
  let dirname = ref None in
  let basename = ref "tfile" in
  let n = ref 1024 in
  let args =
    [ "-cluster", Arg.String (fun s -> cluster := Some s),
      "<name>    Set the cluster name";
      
      "-namenode", Arg.String (fun n -> namenodes := n :: !namenodes),
      "<host>:<port>   Also use this namenode (can be given several times)";

      "-n", Arg.Set_int n,
      "<n>    Number of files to create (default: 1024)";

      "-basename", Arg.Set_string basename,
      "<name>     Basename of the created files"

    ] in

  Arg.parse
    args
    (fun s -> dirname := Some s)
    "usage: ps_create directorypath";

  let esys = Unixqueue.create_unix_event_system() in
  let nn_nodes =
    if !namenodes = [] then None else Some !namenodes in
  let cfg = Plasma_client_config.get_config
    ?clustername:!cluster
    ?nn_nodes () in
  let c = Plasma_client.open_cluster_cc cfg esys in
  Plasma_client.configure_auth_daemon c;
  (* Intentionally we do not configure buffers *)

  match !dirname with
    | None -> failwith "Missing filename"
    | Some fn ->
	run c fn !basename !n
