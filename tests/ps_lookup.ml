(* $Id$ *)

(* A sequence of inode lookups *)

open Printf
open Plasma_rpcapi_aux
open Uq_engines.Operators

let run c inode n =

  let trans = Plasma_client.start c in
  for k = 1 to n do
    let _ii = Plasma_client.get_inodeinfo trans inode in
    ()
  done;
  printf "Done\n%!"
	  

let () =
  let cluster = ref None in
  let namenodes = ref [] in
  let filename = ref None in
  let n = ref 1024 in
  let args =
    [ "-cluster", Arg.String (fun s -> cluster := Some s),
      "<name>    Set the cluster name";
      
      "-namenode", Arg.String (fun n -> namenodes := n :: !namenodes),
      "<host>:<port>   Also use this namenode (can be given several times)";

      "-n", Arg.Set_int n,
      "<n>    Number of iterations (default: 1024)"

    ] in

  Arg.parse
    args
    (fun s -> filename := Some s)
    "usage: ps_lookup inode";

  let esys = Unixqueue.create_unix_event_system() in
  let nn_nodes =
    if !namenodes = [] then None else Some !namenodes in
  let cfg = Plasma_client_config.get_config
    ?clustername:!cluster
    ?nn_nodes () in
  let c = Plasma_client.open_cluster_cc cfg esys in
  Plasma_client.configure_auth_daemon c;
  (* Intentionally we do not configure buffers *)

  match !filename with
    | None -> failwith "Missing inode"
    | Some fn ->
	run c (Int64.of_string fn) !n
