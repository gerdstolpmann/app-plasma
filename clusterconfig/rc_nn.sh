#! /bin/sh

# Start/stop namenodes

usage () {
    echo "usage: $0 (start|stop|status) inst_name" >&2
    exit 2
}

op=""
inst=""

while [ $# -gt 0 ]; do
    case "$1" in
	-*)
	    usage ;;
	*)
	    if [ -z "$op" ]; then
		op="$1"
	    elif [ -z "$inst" ]; then
		inst="$1"
	    else
		usage
	    fi
	    shift
    esac
done

if [ -z "$op" ]; then
    echo "Error: missing operation";
    usage
fi

case "$op" in
    start|stop|status) true ;;
    *)
	echo "Error: bad operation: $op" >&2
	exit 2 ;;
esac

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

. "./lib.sh"

# Collect now all hosts from the .hosts files and form the union set:

hosts="$(get_hosts "$inst" namenode)"
if [ $? -ne 0 ]; then
    echo "Stopping after error" >&2
    exit 2
fi

. "instances/$inst/global.conf"

for host in $hosts; do
    printf "$host: "
    ssh $host "$prefix/etc/rc_nn.sh $op"
    if [ $? -eq 0 ]; then
	echo "ok"
    else 
	echo "error"
    fi
done
