#! /bin/sh

# Start/stop all configured services in the right order

usage () {
    echo "usage: $0 (start|stop|status) inst_name" >&2
    exit 2
}

op=""
inst=""

while [ $# -gt 0 ]; do
    case "$1" in
	-*)
	    usage ;;
	*)
	    if [ -z "$op" ]; then
		op="$1"
	    elif [ -z "$inst" ]; then
		inst="$1"
	    else
		usage
	    fi
	    shift
    esac
done

if [ -z "$op" ]; then
    echo "Error: missing operation";
    usage
fi

case "$op" in
    start|stop|status) true ;;
    *)
	echo "Error: bad operation: $op" >&2
	exit 2 ;;
esac

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

case "$op" in
    start|status)
	echo "Datanodes:"; ./rc_dn.sh "$op" "$inst"
	echo "Namenodes:"; ./rc_nn.sh "$op" "$inst"
	echo "Authnodes:"; ./rc_an.sh "$op" "$inst"
	echo "NFS nodes:"; ./rc_nfsd.sh "$op" "$inst" ;;
    stop)
	echo "NFS nodes:"; ./rc_nfsd.sh "$op" "$inst"
	echo "Authnodes:"; ./rc_an.sh "$op" "$inst"
	echo "Namenodes:"; ./rc_nn.sh "$op" "$inst"
	echo "Datanodes:"; ./rc_dn.sh "$op" "$inst" ;;
esac

