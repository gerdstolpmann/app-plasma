# Shell library

translate_kmg() {
    # $1: name
    # $2: value
    # returns bytes in stdout

    kmg="$(echo "$2" | sed -e 's/^[0-9][0-9]*[KMG]\{0,1\}$//')"
    if [ -n "$kmg" ]; then
	echo "Error: $1 must be a number, optionally followed by K, M, G" >&2
	exit 2
    fi

    case "$2" in
	*K)
	    echo "$2" | sed -e 's/K/ * 1024/' | bc ;;
	*M)
	    echo "$2" | sed -e 's/M/ * 1048576/' | bc ;;
	*G)
	    echo "$2" | sed -e 's/G/ * 1073741824/' | bc ;;
	*)
	    echo "$2"
    esac
}


get_hosts() {
    # $1: instance
    # $2: "datanode" or "namenode"
    # returns list of hosts on stdout
    awk '
  /^[ \t]*$/ { next }
  /^[ \t]*#/ { next }
  /^[^ \t]+[ \t]*#/ { hosts[$1]=1; next }
  /^[^ \t]+[ \t]*$/ { hosts[$1]=1; next }
  // { print "bad line in " FILENAME ": " $0 >"/dev/stderr"; next }
  END { for (host in hosts) { print host } }' \
    "instances/$1/$2.hosts"
}


get_all_hosts() {
    # $1: instance
    # returns list of hosts on stdout
    awk '
  /^[ \t]*$/ { next }
  /^[ \t]*#/ { next }
  /^[^ \t]+[ \t]*#/ { hosts[$1]=1; next }
  /^[^ \t]+[ \t]*$/ { hosts[$1]=1; next }
  // { print "bad line in " FILENAME ": " $0 >"/dev/stderr"; next }
  END { for (host in hosts) { print host } }' \
    instances/"$1"/*.hosts
}


get_pingable_nn_host() {
    # $1: instance
    # outputs host on stdout
    get_hosts "$1" namenode | {
	found=0
	while read h; do
	    if [ $found -eq 0 ]; then
		ping -c 1 -q "$h" >/dev/null 2>&1 || continue
		echo "$h"
		found=1
	    fi
	done
    }
}
