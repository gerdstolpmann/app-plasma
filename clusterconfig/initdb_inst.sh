#! /bin/sh

# Creates the PostgreSQL databases. It is required that the software
# is already deployed (deploy_inst.sh).

set -e

usage () {
    echo "usage: $0 [-drop] inst_name" >&2
    exit 2
}

inst=""
drop=0
destroy_ok=0

while [ $# -gt 0 ]; do
    case "$1" in
	-I-want-to-destroy-data)
	    destroy_ok=1; shift ;;
	-drop)
	    drop=1; shift ;;
	-*)
	    usage ;;
	*)
	    if [ -z "$inst" ]; then
		inst="$1"
	    else
		usage
	    fi
	    shift
    esac
done

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

if [ $drop -ne 0 -a $destroy_ok -eq 0 ]; then
    echo "Error: In order to drop a database, you need to call this script" >&2
    echo "with the additional switch -I-want-to-destroy-data!" >&2
    exit 2
fi

. "./lib.sh"

# Collect now all namenode hosts from the .hosts files and form the union set:

hosts="$(get_hosts "$inst" namenode)"
if [ $? -ne 0 ]; then
    echo "Stopping after error" >&2
    exit 2
fi

. "instances/$inst/global.conf"

for host in $hosts; do
    if [ $drop -gt 0 ]; then
	echo "Dropping database plasma_$inst on $host:"
	ssh $host dropdb $psql_args "plasma_$inst"
    else
	echo "Creating database plasma_$inst on $host:"
	ssh $host createdb $psql_args "plasma_$inst"
	ssh $host psql $psql_args -v ON_ERROR_STOP=1 \
	    "plasma_$inst" "-f '$prefix/etc/namenode.sql'" -q 2>&1 |
	    grep -v NOTICE || true
    fi
    echo "  done"
done
