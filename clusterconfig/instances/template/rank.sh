#! /bin/sh

case `uname` in
  Linux)
    ip addr show label 'eth*' | grep link/ether | awk '{print $2}';;
  Darwin) 
    ioreg -rd1 -c IOPlatformExpertDevice | awk '/IOPlatformUUID/ { split($0, line, "\""); printf("%s\n", line[4]); }';;
esac



