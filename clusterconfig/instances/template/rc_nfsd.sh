#! /bin/sh

# rc script for NFS server start/stop

thisdir="$(dirname "$0")"

. "$thisdir/global.conf"

descrname="Plasma nfsnode"
progname="nfs3d"
logname="nfsd"
sockdir="/tmp/plasma_nfs_$inst"
conf="$prefix/etc/nfsnode.conf"

status() {
    pid=`cat "$sockdir/pid" 2>/dev/null`
    if kill -0 $pid 2>/dev/null; then
        if [ -d /proc/$pid ]; then
            exe=`readlink /proc/$pid/exe | sed -e 's/ (deleted)$//'`
            exe=`basename "$exe"`
            if [ "X$exe" = "X$progname" ]; then
                return 0
            fi
        else
            return 0
        fi
    fi
    return 1
}

start() {
    if status; then
        echo `hostname`": $descrname already running"; exit 1
    fi

    if [ "X$disable_list_check" != "X1" ]; then
	this="$(hostname)"
	ok="$(awk -v "this=$this" '
      /^[ \t]*$/ { next }
      /^[ \t]*#/ { next }
      /^[^ \t]+[ \t]*#/ { hosts[$1]=1; next }
      /^[^ \t]+[ \t]*$/ { hosts[$1]=1; next }
      // { print "bad line in " FILENAME ": " $0 >"/dev/stderr"; next }
      END { for (host in hosts) { if (host == this) { print "found"; break } } }' \
	$thisdir/nfsnode.hosts)"

	if [ -z "$ok" ]; then
	    echo "$0: This node is not an nfsnode!"; exit 1
	fi
    fi

    mkdir -p "$sockdir"
    $prefix/bin/$progname \
	-pid "$sockdir/pid" \
	-conf "$conf" \
        2>>$prefix/log/${logname}_stderr.log
}

stop() {
    if ! status; then
        echo `hostname`": $descrname not running"; exit 1
    fi

    pid="$(cat "$sockdir/pid")"
    $prefix/bin/netplex-admin -sockdir "$sockdir" -shutdown

    # Wait until the process group vanishes:
    n=0
    while kill -0 -$pid 2>/dev/null; do
        sleep 0.1
        n=$((n+1))
	if [ $n -eq 50 ]; then
	    echo `hostname`",$descrname: Killing processes (hard shutdown)" >&2
	    kill -15 $pid
	fi
        if [ $n -gt 600 ]; then break; fi
    done
    
    if [ $n -gt 600 ]; then
        echo `hostname`": Timing out shutdown of $descrname" >&2
        exit 1
    fi
}

case "$1" in
    start)
        start ;;
    stop)
        stop ;;
    status)
        status ;;
    *)
        echo "$0 (start|stop|status)" ;;
esac
