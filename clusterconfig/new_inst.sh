#! /bin/sh

# Creates a new instance in the "instances/" subdirectory.

usage () {
    echo "usage: $0 [-template templ_name] inst_name prefix blocksize[KM]" >&2
    exit 2
}

set -e

template="template"
inst=""
prefix=""
blocksize=""

while [ $# -gt 0 ]; do
    case "$1" in
	-template)
	    template="$2"
	    shift 2 ;;
	-*)
	    usage ;;
	*)
	    if [ -z "$inst" ]; then
		inst="$1"
	    elif [ -z "$prefix" ]; then
		prefix="$1"
	    elif [ -z "$blocksize" ]; then
		blocksize="$1"
	    else
		usage
	    fi
	    shift
    esac
done

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ -z "$prefix" ]; then
    echo "Error: missing prefix";
    usage
fi

if [ -z "$blocksize" ]; then
    echo "Error: missing blocksize";
    usage
fi

. "./lib.sh"


# Check validity of instance:
s="$(echo "$inst" | tr -d a-zA-Z0-9_)"
if [ -n "$s" ]; then
    echo "Error: instance name must only consist of alphanumeric chars and _" >&2
    exit 2
fi

# Check validity of prefix:
case "$prefix" in
    /*) true ;;
    *)  echo "Error: prefix must start with /" >&2; exit 2 ;;
esac

# Check/translate blocksize:
blocksize="$(translate_kmg 'blocksize' "$blocksize")"

code="$(echo "$blocksize > 67108864" | bc)"
if [ $code = 1 ]; then
    echo "Error: blocksize is limited to 64M" >&2
    exit 2
fi

m="$(echo "$blocksize % 4096" | bc)"
if [ $m != 0 ]; then
    echo "Error: blocksize is not a multiple of 4096" >&2
    exit 2
fi

echo "Creating new instance:"
echo "Name:      $inst"
echo "Prefix:    $prefix"
echo "Blocksize: $blocksize"
echo "Template:  $template"

mkdir -p instances/$inst
for f in namenode.hosts datanode.hosts nfsnode.hosts authnode.hosts global.conf rc_dn.sh rc_nn.sh rc_nfsd.sh rc_an.sh rank.sh; do
    cp -p "instances/$template/$f" "instances/$inst/$f"
done

# create passwords
for pw in password_proot password_pnobody; do
    rm -f instances/$inst/$pw
    touch instances/$inst/$pw
    chmod 600 instances/$inst/$pw
    dd if=/dev/urandom bs=1 count=64 2>/dev/null | md5sum | cut -f 1 -d ' ' \
	>instances/$inst/$pw
done

for f in namenode.conf datanode.conf nfsnode.conf authnode.conf; do
    sed -e 's/<INST>/'$inst'/g' \
	-e 's|<PREFIX>|'$prefix'|g' \
	-e 's/<BLOCKSIZE>/'$blocksize'/g' \
	"instances/$template/$f" > "instances/$inst/$f"
done

echo "inst=\"$inst\"" >> instances/$inst/global.conf
echo "prefix=\"$prefix\"" >> instances/$inst/global.conf
echo "blocksize=$blocksize" >> instances/$inst/global.conf
echo "nn_port=2730" >> instances/$inst/global.conf
echo "dn_port=2728" >> instances/$inst/global.conf
echo "psql_args=\"\"" >> instances/$inst/global.conf
echo "disable_list_check=0" >> instances/$inst/global.conf

echo "Done"
