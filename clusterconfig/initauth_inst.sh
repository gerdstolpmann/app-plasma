#! /bin/sh

# Initializes passwd and group
#
# For this, the software must already deployed (deploy_inst.sh), and
# the namenode daemon must already run (rc_nn.sh)

set -e

usage () {
    echo "usage: $0 inst_name [-passwd file] [-group file]" >&2
    exit 2
}

inst=""
passwd="/etc/passwd"
group="/etc/group"
apple=0

case `uname` in
    Darwin) apple=1 ;;
esac

while [ $# -gt 0 ]; do
    case "$1" in
	-passwd)
	    passwd="$2"; apple=0; shift 2 ;;
	-group)
	    group="$2"; apple=0; shift 2 ;;
	-*)
	    usage ;;
	*)
	    if [ -z "$inst" ]; then
		inst="$1"
	    else
		usage
	    fi
	    shift
    esac
done

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

. "./lib.sh"
. "instances/$inst/global.conf"
. "./inst.conf"

nn_host="$(get_pingable_nn_host "$inst")"
nn="-namenode $nn_host:$nn_port -cluster $inst"

# the password for plasma:
PLASMA_PASSWD_proot="$(cat "instances/$inst/password_proot")"
export PLASMA_PASSWD_proot

# Check $nn:
echo "Testing namenode connectivity:"
if $plasma_admin list_datanodes $nn >/dev/null;
then
    echo "ok"
else
    echo "Error: No namenode found"
    echo "   (hint: you may have to start the namenodes first, using rc_nn.sh)"
    exit 2
fi

echo "Setting passwd and group"
if [ $apple -gt 0 ]; then
    dscl -url . -readall /Users | 
        $plasma admin_table $nn -auth proot -apple -put passwd
    dscl -url . -readall /Groups | 
        $plasma admin_table $nn -auth proot -apple -put group
else
    cat "$passwd" | $plasma admin_table $nn -auth proot -put passwd
    cat "$group" | $plasma admin_table $nn -auth proot -put group
fi
echo "ok"

echo "Done"
