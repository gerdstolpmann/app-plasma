#! /bin/sh

# Initializes datanode space
#
# For this, the software must already deployed (deploy_inst.sh), and
# the namenode daemon must already run (rc_nn.sh)

set -e

usage () {
    echo "usage: $0 inst_name size[KMG] (host host ... | all)" >&2
    exit 2
}

inst=""
size=""
hosts=""
all_hosts=0
destroy_ok=0

while [ $# -gt 0 ]; do
    case "$1" in
	-I-want-to-destroy-data)
	    destroy_ok=1; shift ;;
	-*)
	    usage ;;
	*)
	    if [ -z "$inst" ]; then
		inst="$1"
	    elif [ -z "$size" ]; then
		size="$1"
	    elif [ "$1" = "all" ]; then
		all_hosts=1
	    else
		hosts="$hosts $1"
	    fi
	    shift
    esac
done

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ -z "$size" ]; then
    echo "Error: missing size parameter";
    usage
fi

if [ -z "$hosts" -a $all_hosts = 0 ]; then
    echo "Error: no hosts"
    usage
fi

if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

. "instances/$inst/global.conf"
. "./inst.conf"
. "./lib.sh"

# Check/translate size:
size="$(translate_kmg 'size' "$size")"
blocks="$(echo "$size / $blocksize" | bc)"
echo "Using $blocks blocks"

if [ $all_hosts -gt 0 ]; then
    hosts="$(get_hosts "$inst" datanode)"
    if [ $? -ne 0 ]; then
	echo "Stopping after error" >&2
	exit 2
    fi
fi

nn_host="$(get_pingable_nn_host "$inst")"
nn="-namenode $nn_host:$nn_port -cluster $inst"

# the password for plasma_admin:
PLASMA_PASSWD_proot="$(cat "instances/$inst/password_proot")"
export PLASMA_PASSWD_proot

# Check $nn:
echo "Testing namenode connectivity ($nn_host:$nn_port):"
if $plasma_admin list_datanodes $nn >/dev/null;
then
    echo "ok"
else
    echo "Error: No namenode found"
    echo "   (hint: you may have to start the namenodes first, using rc_nn.sh)"
    exit 2
fi


for host in $hosts; do
    echo "Checking $host:"
    have_data="$(ssh $host "if [ -f '$prefix/data/data' ]; then echo 1; else echo 0; fi")"
    if [ $have_data -gt 0 -a $destroy_ok -eq 0 ]; then
	echo "Error: There are already data blocks on $host. This script does not" >&2
	echo "overwrite data by default. If you want to do so, and destroy the" >&2
	echo "existing data blocks, call this script again with the additional" >&2
	echo "switch: -I-want-to-destroy-data" >&2
	exit 2
    fi
    echo "Initializing on $host:"
    ssh $host \
	"rm -f '$prefix/data/config' '$prefix/data/data'"
    ssh $host \
	"$prefix/bin/plasma_datanode_init -blocksize $blocksize '$prefix/data' $blocks"
    id="$(ssh $host head -1 $prefix/data/config)"
    echo "  starting datanode on $host:"
    ssh $host \
	"$prefix/etc/rc_dn.sh start"
    echo "  adding identity to namenode"
    $plasma_admin \
	add_datanode $nn -size $blocks "$id"
    echo "  enabling"
    $plasma_admin \
	enable_datanode $nn "$id" "$host:$dn_port"
done

echo "Done"
