#! /bin/sh

# Manages datanode space
#
# For this, the software must already deployed (deploy_inst.sh), and
# the namenode daemon must already run (rc_nn.sh)

set -e

usage () {
    echo "usages:" >&2
    echo "$0 list    inst_name" >&2
    echo "$0 enable  inst_name identity host" >&2
    echo "$0 disable inst_name identity" >&2
    echo "$0 destroy inst_name identity" >&2
    exit 2
}

mode=""
inst=""
identity=""
host=""

while [ $# -gt 0 ]; do
    case "$1" in
	-*)
	    usage ;;
	*)
	    if [ -z "$mode" ]; then
		mode="$1"
	    elif [ -z "$inst" ]; then
		inst="$1"
	    elif [ -z "$identity" ]; then
		identity="$1"
	    elif [ -z "$host" ]; then
		host="$1"
	    else
		usage
	    fi
	    shift
    esac
done

case "$mode" in
    list|enable|disable|destroy) true ;;
    *)
	echo "Error: unknown mode";
	usage ;;
esac

if [ -z "$inst" ]; then
    echo "Error: missing instance" >&2
    exit 2
fi


if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

. "instances/$inst/global.conf"
. "./inst.conf"
. "./lib.sh"

nn_host="$(get_pingable_nn_host "$inst")"
nn="-namenode $nn_host:$nn_port -cluster $inst"

# the password for plasma_admin:
PLASMA_PASSWD_proot="$(cat "instances/$inst/password_proot")"
export PLASMA_PASSWD_proot

case "$mode" in
    list)
	$plasma_admin \
	    list_datanodes $nn ;;

    enable)
	if [ -z "$identity" ]; then
	    echo "Error: missing identity" >&2
	    usage
	fi
	if [ -z "$host" ]; then
	    echo "Error: missing host" >&2
	    usage
	fi
	$plasma_admin \
	    enable_datanode $nn "$identity" "$host" ;;

    disable)
	if [ -z "$identity" ]; then
	    echo "Error: missing identity" >&2
	    usage
	fi
	$plasma_admin \
	    disable_datanode $nn "$identity" ;;

    destroy)
	if [ -z "$identity" ]; then
	    echo "Error: missing identity" >&2
	    usage
	fi
	$plasma_admin \
	    destroy_datanode $nn "$identity" ;;



esac
