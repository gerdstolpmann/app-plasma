#! /bin/sh

# Deploys software for an instance. Does NOT initialize databases, and
# does NOT initialize datanodes

usage () {
    echo "usage: $0 [-only-config] [-only-host <host>] inst_name" >&2
    exit 2
}

set -e
inst=""
only_config=0
only_host=""

while [ $# -gt 0 ]; do
    case "$1" in
	-only-config)
	    only_config=1; shift ;;
	-only-host)
	    only_host="$2"; shift 2 ;;
	-*)
	    usage ;;
	*)
	    if [ -z "$inst" ]; then
		inst="$1"
	    else
		usage
	    fi
	    shift
    esac
done

if [ -z "$inst" ]; then
    echo "Error: missing instance";
    usage
fi

if [ ! -d "instances/$inst" ]; then
    echo "Error: no such instance: $inst" >&2
    exit 2
fi

. "./lib.sh"

# Collect now all hosts from the .hosts files and form the union set:

if [ -n "$only_host" ]; then
    hosts="$only_host"
else
    hosts="$(get_all_hosts "$inst")"
    if [ $? -ne 0 ]; then
	echo "Stopping after error" >&2
	exit 2
    fi
    if [ -z "$hosts" ]; then
	echo "No hosts found. Did you edit the files instances/$inst/*.hosts?"
    fi
fi

. "instances/$inst/global.conf"
. "./inst.conf"

na="$(which netplex-admin)"
if [ -z "$na" ]; then
    echo "Error: netplex-admin not found in PATH"
fi

for host in $hosts; do
    echo "Deploying to $host:"
    ssh $host \
	"mkdir -p '$prefix/bin' '$prefix/etc' '$prefix/log' '$prefix/data' \
                  '$prefix/dump'"
    scp -p \
        instances/$inst/*.conf instances/$inst/*.hosts instances/$inst/*.sh  \
        instances/$inst/password_proot instances/$inst/password_pnobody \
	$host:"$prefix/etc"
    scp $namenode_sql $host:"$prefix/etc"
    if [ $only_config -eq 0 ]; then
	scp -p $plasmad $nfs3d $plasma_datanode_init $plasma_admin "$na" \
	    $plasma \
	    $host:"$prefix/bin"
    fi
done


