/* $Id$ -*- c -*- */

/* The easy-going interface of task servers */

#ifndef PMR_TASK
#define PMR_TASK

typedef string longstring<>;
typedef longstring longstrings<>;

struct resource_config { 
    /* POSIX shared memory: */
    hyper *rc_shm_low;
    hyper *rc_shm_high;
    hyper *rc_shm_max;
    /* If rc_shm_max is NULL, it is tried to determine a default (for
       some OS only), which is then multiplied by rc_shm_max_p. If 
       rc_shm_low and rc_shm_high are NULL, these are
       determined by multiplying rc_shm_max with the following factors:
    */
    double rc_shm_low_p;
    double rc_shm_high_p;
    double rc_shm_max_p;
    
    /* Buffer memory: */
    hyper *rc_buf_low;
    hyper *rc_buf_high;
    hyper *rc_buf_max;
    /* If rc_buf_max is NULL, it is tried to determine a default (for
       some OS only), which is then multiplied by rc_buf_max_p. If 
       rc_buf_low and rc_buf_high are NULL, these are
       determined by multiplying rc_buf_max with the following factors:
    */
    double rc_buf_low_p;
    double rc_buf_high_p;
    double rc_buf_max_p;
};

struct capacity {
    double max_capacity;
    double avail_capacity;
};

program Mapred_task {
    version V1 {
	void null (void) = 0;

	longstring exec(longstring, int, longstring, longstring, longstring) = 1;
	/* let r = exec(command_name, req_id, ticket, jobconfig, t):
	   Executes the encoded task t and returns the encoded result r.
	*/

	/* TODO:
	   .- get_req_id : void -> int
	   .- status : int -> bool   (running/not running)
	*/

	longstring simulate(longstring, int, longstring, longstring, longstring) = 7;
	/* like exec, but for simulation */

	void kill(int,longstring) = 2;
	/* [kill(req_id,ticket)]: Kill the started task with this req ID */

	void kill_all(longstring) = 3;
	/* [kill_all(ticket)]: Kill all started tasks */

	void configure(resource_config, longstring) = 4;
	/* configure(rc, ticket): see above */

	bool check_version(longstring, longstring) = 5;
	/* [check_version(version, ticket)]:
           The version string describes the expected version of the executable
	   running the task server. Returns whether the version is matched.
	   Actually, the string is a MD5 sum of the server executable.
	*/

	capacity job_capacity(longstring, longstring) = 6;
	/* [job_capacity(job_id, ticket)]: Returns the load this job is allowed
	   to put onto this server. This is defined as:

	   .- [max_capacity]: the number of cores
	   . [avail_capacity]: number_of_cores / number_of_running_jobs

	   The number_of_running_jobs is the number of jobs currently
	   being executed, including the passed job_id. Returns a negative
	   value if the functionality is not available.

	   Note that the returned number is independent of the number
	   of running tasks. It is just the capacity the current job
	   may use at most. Also note that this number may change
	   during the runtime of the job (when other jobs are started).
	*/

    } = 1;
} = 0x80013001;
#endif
