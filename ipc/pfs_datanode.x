/* $Id$ -*- c -*- */

/** {1:datanode [Datanode]} */

/** Datanode access. 
 */

#include "pfs_types.x"

#ifndef PFS_DATANODE_X
#define PFS_DATANODE_X

program Datanode {
    version V1 {
	
	/** {2 [null] } */

	void null(void) = 0;

	/** {2 [identity] } */

	mediumstring identity(mediumstring) = 1;
	/** Returns the identity of this node (an ID which is assigned anew
           when the datanode is initialized). The arg is the clustername.
           If the node belongs to the wrong cluster, this RPC must return
           [SYSTEM_ERR].
	*/

	/** {2 [size] } */

	hyper size(void) = 2;
	/** Returns the number of blocks. The blocks have numbers from 0
           to [size-1]
	*/

	/** {2 [blocksize] } */

	int blocksize(void) = 3;
	/** Returns the blocksize */

	/** {2 [clustername] } */

	mediumstring clustername(void) = 4;
	/** Returns the clustername */

	/** {2:read [read] } */

	dn_channel_rd_data read
	    (dn_channel_rd_req, hyper, int, int, ticket) = 5;
	/** [read(req, block, pos, len, ticket)]:
	    Reads a block, or a part of it. [req] defines how the data
	    is passed back to the caller (see docs for [dn_channel_rd_req]
	    in {!Pfs_types}). The [block] is the block number of this
	    datanode. [pos] and [len] select a substring of this block.
	    [ticket] is the ticket returned by [get_blocks].
	   
	    Right now this ticket is not checked.
	 */

	/** {2:write [write] } */

	void write(hyper, dn_channel_wr_data, ticket) = 6;
	/** [write(block, contents, ticket)]:
	    Writes a block. It is only possible to write a block completely.
	    The [block] is the block number of this datanode.
	    In [contents] the data to write is passed. (See the docs
	    for [dn_channel_wr_data] in {!Pfs_types} for details.)
            The data in [contents] must have the length [blocksize].
	    [ticket] is the ticket granting write access.

	    The safetrans ticket {i is} checked!
	*/

	/** {2:copy [copy] } */

	void copy(hyper, longstring, longstring, hyper,
		  ticket, ticket) = 7;
	/** [copy(block, dest_node, dest_identity, dest_block, ticket,
	        dest_ticket)]:
	   Copies a block, possibly to a remote system. [block] identifies
	   the block on this datanode. [dest_node] is the datanode server
	   to where the block is written to. [dest_identity] is the
	   identity of the destination server. [dest_block] is the
	   block number of the destination server.
	   
           If [dest_identity] is equal to the own identity, this is a local
           copy. Otherwise, [dest_node] is interpreted as "host:port", and the
           block is written to the remote host.

           [ticket] is the ticket granting read access to the block.

           [dest_ticket] is the ticket granting write access to the block
	   on the destination datanode.
	*/

	/** {2:zero [zero] } */

	void zero(hyper, ticket) = 8;
	/** [zero(block, ticket)]:
	*/

	/** {2:sync [sync] } */

	void sync(void) = 9;
	/** Waits until the next sync cycle is done */


	/** {2:alloc_shm_if_local [alloc_shm_if_local] } */

	longstring_opt alloc_shm_if_local(void) = 10;
	/** If the client is on the same node, this RPC allocates a new
	   POSIX shm object, and returns the path of this object.
	   The object has zero size, and is created with mode 666.
	   If the client is not on the same node, the RPC returns NULL.

	   The shm object exists only as long as the TCP connection
	   persists (i.e. the client needs not to care about deleting it).

	   This method of shm-based communication is inherently
	   insecure.
	*/

	/** {2:udsocket_if_local [udsocket_if_local] } */

	longstring_opt udsocket_if_local(void) = 11;
	/** If the client is on the same node, this RPC may return the
	   name of a Unix Domain socket to contact instead.
	*/

    } = 1;
} = 0x8000d001;



#endif
