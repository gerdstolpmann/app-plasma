/* $Id$  -*- c -*- */

#define SERVER_CONTEXT

#include "pfs_types.x"
#include "pfs_datanode.x"
#include "pfs_dn_internal.x"
#include "pfs_nn_fsys.x"
#include "pfs_nn_coord.x"
#include "pfs_nn_dnadmin.x"
#include "pfs_nn_internal.x"
#include "pfs_nn_auth.x"
#include "pfs_nn_namesync.x"

