/* $Id$ -*- c -*- */

/** {1:auth [Auth]} */

/** Authentication daemon */

#include "pfs_types.x"

#ifndef PFS_NN_AUTH
#define PFS_NN_AUTH

program Auth {
    version V1 {
	
	/** This program can only be contacted over Unix Domain sockets. */

	/** {2 [null]} */

	void null(void) = 0;


	/** {2 [get_auth_ticket]} */

	longstring_opt get_auth_ticket(void) = 1;
	/** Return a new authentication ticket for the user contacting the
	    daemon.
	*/

    } = 1;
} = 0x8000e005;

#endif

