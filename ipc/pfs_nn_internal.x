/* $Id$ -*- c -*- */

/** Internal interfaces used by the namenodes
 */

#include "pfs_types.x"

#ifndef PFS_NN_INTERNAL_X
#define PFS_NN_INTERNAL_X

/** {1:elect [Elect]} */

/** The election happens at cluster startup. The goal is to determine
    the coordinator. Participants are all namenodes.
*/

program Elect {
    version V1 {

	/** {2 [null] } */

	void null(void) = 0;

	/** {2 [announce] } */

	ann_result announce(announcement) = 1;
	/** At cluster start the namenodes start calling the
           [announce] RPC of all other namenodes - until they get a
           reply from each, or until the end of the startup period is
           reached.

           If received within the startup period, the response is [ANN_ACCEPT]
           if the announcement is better than the server to which it is sent.
	   Otherwise it is [ANN_REJECT].
           If received after startup, the response is always [ANN_REJECT],
	   and the
           sender must not start up.
	   The response is [ANN_SELF] if the namenode instance sends the
	   announcement to itself.

	   As all namenodes call [announce] of all other namenodes, the
	   question is whether there is a winner. If we assume there
	   is a total ordering between the [announcement]s, there is
	   a best announcement if no two namenodes emit equal announcements.
	   So given the announcements are all distinct, there is a winner.
	*/

	/** {2 [set_coordinator] } */

	void set_coordinator(mediumstring, mediumstring, mediumstring,
			     mediumstring) = 2;
	/** When the end of the startup period is reached, one of the name
	   nodes sends [set_coordinator] to all other nodes, and becomes the
           coordinator. The coordinator must be eligible by all other nodes
           that actually respond. Also, the coordinator must have a highest
           revision number, and among all nodes with the highest revision
           number, the coordinator has the lowest rank.

           The first arg is the "host:port" name of the coordinator.
	   (Instead of the host name this string can also name the coordinator
	   by IP address if this is the preferred method of referencing it.)
  
           The second arg is the clustername.

           The third arg is the maximum revision identifier.

	   The fourth arg is the checksum of all revision identifiers in the
	   DB.
	*/

	/** There is right now no provision for the case that the coordinator
           crashes - no other node is then automatically elected. Best is
           to restart everything then.
	*/
	
    } = 1;
} = 0x8000f001;

/** {1:nameslave [Nameslave]} */

/** This RPC program is activated on the non-coordinator namenodes. It
    is called by the coordinator to push updates of the database.
*/

program Nameslave {
    version V1 {
	/* This is what the non-coordinators implement */

	/** The first arg (an [int]) is the ID of the committer (usually PID) */

	/** {2 [null] } */

	void null(void) = 0;

	/** {2 [begin_transaction] } */

	void begin_transaction(int, mediumstring, mediumstring) = 1;
	/** [begin_transaction(commit_id, clustername, expected_rev)]:
	   The last arg is the expected revision string (last commit of
	   this committer)
	*/

	/** {2 [prepare_commit] } */

	bool prepare_commit(int) = 2;
	/** [prepare_commit(commit_id)]:
	    Result is true if the name database could be updated.
	 */

	/** {2 [commit] } */

	void commit(int) = 3;
	/** [commit(commit_id)]:
	    The response of [commit] is the ACK in the extended 2-phase
           commit protocol
	*/

	/* void abort(void) = 4; */

	/** Note that the names of the following RPCs correspond to
	    function names in {!Nn_db}:
	*/

	/** {2 [set_ds_cache] } */

	void set_ds_cache(ds_info_list) = 5;
	/** [set_ds_cache(l)]: calls
	    {!Nn_db.ds_cache_init}[(`Datastores l)] */

	/** {2 [push] } */

        void push(int, modification) = 30;
        /** [push(commit_id, mod)] */
    } = 1;
} = 0x8000f002;


/** {1:monitor [Monitor]} */

program Monitor {
    version V1 {
	/** {2 [null] } */

	void null(void) = 0;

	/** {2 [start] } */

	void start(void) = 1;
	/** Starts the monitor: First, the state is loaded from the db.
           Second, all known datanodes are discovered and enabled.
           Third, the newsfeed for monitoring results is started.
           Fourth, the Dn_admin interface is enabled.
	*/

    } = 1;
} = 0x8000f003;


#endif
