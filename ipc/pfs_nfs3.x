/* $Id$ -*- c -*- */

/* This file was obtained by copying the specs from RFC 1813 and
   editing it for brevity and for better output of ocamlrpcgen
*/

#ifndef PFS_NFS3_X
#define PFS_NFS3_X

/* -- Types for NFS protocol -- */

#define NFS3_FHSIZE 64
#define NFS3_COOKIEVERFSIZE 8
#define NFS3_CREATEVERFSIZE 8
#define NFS3_WRITEVERFSIZE 8

typedef unsigned hyper uint64;
typedef unsigned _int64 int uint32;
typedef string filename3<>;
typedef string nfspath3<>;
typedef hyper fileid3;
typedef hyper cookie3;
typedef opaque cookieverf3[NFS3_COOKIEVERFSIZE];
typedef opaque createverf3[NFS3_CREATEVERFSIZE];
typedef opaque writeverf3[NFS3_WRITEVERFSIZE];
typedef _managed string opaque_data<>;
typedef uint32 uid3;
typedef uint32 gid3;
typedef uint64 size3;
typedef uint64 offset3;
typedef uint32 mode3;
typedef uint32 count3;

enum nfsstat3 {
    NFS3_OK             = 0,
    NFS3ERR_PERM        = 1,
    NFS3ERR_NOENT       = 2,
    NFS3ERR_IO          = 5,
    NFS3ERR_NXIO        = 6,
    NFS3ERR_ACCES       = 13,
    NFS3ERR_EXIST       = 17,
    NFS3ERR_XDEV        = 18,
    NFS3ERR_NODEV       = 19,
    NFS3ERR_NOTDIR      = 20,
    NFS3ERR_ISDIR       = 21,
    NFS3ERR_INVAL       = 22,
    NFS3ERR_FBIG        = 27,
    NFS3ERR_NOSPC       = 28,
    NFS3ERR_ROFS        = 30,
    NFS3ERR_MLINK       = 31,
    NFS3ERR_NAMETOOLONG = 63,
    NFS3ERR_NOTEMPTY    = 66,
    NFS3ERR_DQUOT       = 69,
    NFS3ERR_STALE       = 70,
    NFS3ERR_REMOTE      = 71,
    NFS3ERR_BADHANDLE   = 10001,
    NFS3ERR_NOT_SYNC    = 10002,
    NFS3ERR_BAD_COOKIE  = 10003,
    NFS3ERR_NOTSUPP     = 10004,
    NFS3ERR_TOOSMALL    = 10005,
    NFS3ERR_SERVERFAULT = 10006,
    NFS3ERR_BADTYPE     = 10007,
    NFS3ERR_JUKEBOX     = 10008
};

enum ftype3_enum {
    NF3REG    = 1,
    NF3DIR    = 2,
    NF3BLK    = 3,
    NF3CHR    = 4,
    NF3LNK    = 5,
    NF3SOCK   = 6,
    NF3FIFO   = 7
};

union ftype3 switch (ftype3_enum d) {
case NF3REG:
    void;
default:
    void;
};


struct specdata3 {
    uint32     specdata1;
    uint32     specdata2;
};

typedef opaque nfs_fh3<NFS3_FHSIZE>;

struct nfstime3 {
    uint32   seconds;
    uint32   nseconds;
};

struct fattr3 {
    ftype3     fa_type;
    mode3      fa_mode;
    uint32     fa_nlink;
    uid3       fa_uid;
    gid3       fa_gid;
    size3      fa_size;
    size3      fa_used;
    specdata3  fa_rdev;
    hyper      fa_fsid;
    fileid3    fa_fileid;
    nfstime3   fa_atime;
    nfstime3   fa_mtime;
    nfstime3   fa_ctime;
};

typedef fattr3 *post_op_attr;

struct wcc_attr {
    size3       wcc_size;
    nfstime3    wcc_mtime;
    nfstime3    wcc_ctime;
};

typedef wcc_attr *pre_op_attr;

struct wcc_data {
    pre_op_attr    before;
    post_op_attr   after;
};

typedef nfs_fh3 *post_op_fh3;

enum time_how {
    DONT_CHANGE        = 0,
    SET_TO_SERVER_TIME = 1,
    SET_TO_CLIENT_TIME = 2
};

typedef mode3 *set_mode3;
typedef uid3 *set_uid3;
typedef gid3 *set_gid3;
typedef size3 *set_size3;

union set_atime switch (time_how set_it) {
case SET_TO_CLIENT_TIME:
    nfstime3  atime;
default:
    void;
};

union set_mtime switch (time_how set_it) {
case SET_TO_CLIENT_TIME:
    nfstime3  mtime;
default:
    void;
};

struct sattr3 {
    set_mode3   sa_mode;
    set_uid3    sa_uid;
    set_gid3    sa_gid;
    set_size3   sa_size;
    set_atime   sa_atime;
    set_mtime   sa_mtime;
};

struct diropargs3 {
    nfs_fh3     dirop_dir;
    filename3   dirop_name;
};

struct GETATTR3resok {
    fattr3   getattr_obj_attributes;
};

union GETATTR3res switch (nfsstat3 status) {
case NFS3_OK:
    GETATTR3resok  resok;
default:
    void;
};

typedef nfstime3 *sattrguard3;

struct SETATTR3resok {
    wcc_data  setattr_obj_wcc_ok;
};

struct SETATTR3resfail {
    wcc_data  setattr_obj_wcc_fail;
};

union SETATTR3res switch (nfsstat3 status) {
case NFS3_OK:
    SETATTR3resok   resok;
default:
    SETATTR3resfail resfail;
};

struct LOOKUP3resok {
    nfs_fh3      lookup_object;
    post_op_attr lookup_obj_attributes;
    post_op_attr lookup_dir_attributes;
};

struct LOOKUP3resfail {
    post_op_attr lookup_dir_attributes_fail;
};

union LOOKUP3res switch (nfsstat3 status) {
case NFS3_OK:
    LOOKUP3resok    resok;
default:
    LOOKUP3resfail  resfail;
};

const ACCESS3_READ    = 0x0001;
const ACCESS3_LOOKUP  = 0x0002;
const ACCESS3_MODIFY  = 0x0004;
const ACCESS3_EXTEND  = 0x0008;
const ACCESS3_DELETE  = 0x0010;
const ACCESS3_EXECUTE = 0x0020;

struct ACCESS3resok {
    post_op_attr   access_obj_attributes_ok;
    uint32         access_rights_ok;
};

struct ACCESS3resfail {
    post_op_attr   access_obj_attributes_fail;
};

union ACCESS3res switch (nfsstat3 status) {
case NFS3_OK:
    ACCESS3resok   resok;
default:
    ACCESS3resfail resfail;
};

struct READLINK3resok {
    post_op_attr   readlink_symlink_attributes_ok;
    nfspath3       readlink_data;
};

struct READLINK3resfail {
    post_op_attr   readlink_symlink_attributes_fail;
};

union READLINK3res switch (nfsstat3 status) {
case NFS3_OK:
    READLINK3resok   resok;
default:
    READLINK3resfail resfail;
};

struct READ3resok {
    post_op_attr   read_file_attributes_ok;
    count3         read_count_ok;
    bool           read_eof;
    _managed string read_data<>;
};

struct READ3resfail {
    post_op_attr   read_file_attributes_fail;
};

union READ3res switch (nfsstat3 status) {
case NFS3_OK:
    READ3resok   resok;
default:
    READ3resfail resfail;
};

enum stable_how_enum {
    UNSTABLE  = 0,
    DATA_SYNC = 1,
    FILE_SYNC = 2
};

union stable_how switch (stable_how_enum d) {
case UNSTABLE:
    void;
default:
    void;
};


struct WRITE3resok {
    wcc_data    write_file_wcc_ok;
    count3      write_count_ok;
    stable_how  write_committed;
    writeverf3  write_verf;
};

struct WRITE3resfail {
    wcc_data    write_file_wcc_fail;
};

union WRITE3res switch (nfsstat3 status) {
case NFS3_OK:
    WRITE3resok    resok;
default:
    WRITE3resfail  resfail;
};

enum createmode3 {
    UNCHECKED = 0,
    GUARDED   = 1,
    EXCLUSIVE = 2
};

union createhow3 switch (createmode3 mode) {
case UNCHECKED:
case GUARDED:
    sattr3       obj_attributes;
case EXCLUSIVE:
    createverf3  verf;
};

struct CREATE3resok {
    post_op_fh3   create_obj;
    post_op_attr  create_obj_attributes;
    wcc_data      create_dir_wcc_ok;
};

struct CREATE3resfail {
    wcc_data      create_dir_wcc_fail;
};

union CREATE3res switch (nfsstat3 status) {
case NFS3_OK:
    CREATE3resok    resok;
default:
    CREATE3resfail  resfail;
};

struct MKDIR3resok {
    post_op_fh3   mkdir_obj;
    post_op_attr  mkdir_obj_attributes;
    wcc_data      mkdir_dir_wcc_ok;
};

struct MKDIR3resfail {
    wcc_data      mkdir_dir_wcc_fail;
};

union MKDIR3res switch (nfsstat3 status) {
case NFS3_OK:
    MKDIR3resok   resok;
default:
    MKDIR3resfail resfail;
};

struct symlinkdata3 {
    sattr3    symlink_attributes;
    nfspath3  symlink_path;
};

struct SYMLINK3resok {
    post_op_fh3   symlink_obj;
    post_op_attr  symlink_obj_attributes;
    wcc_data      symlink_dir_wcc_ok;
};

struct SYMLINK3resfail {
    wcc_data      symlink_dir_wcc_fail;
};

union SYMLINK3res switch (nfsstat3 status) {
case NFS3_OK:
    SYMLINK3resok   resok;
default:
    SYMLINK3resfail resfail;
};


struct devicedata3 {
    sattr3     dev_attributes;
    specdata3  spec;
};

union mknoddata3 switch (ftype3_enum type) {
case NF3CHR:
case NF3BLK:
    devicedata3  device;
case NF3SOCK:
case NF3FIFO:
    sattr3       pipe_attributes;
default:
           void;
};

struct MKNOD3resok {
    post_op_fh3   mknod_obj;
    post_op_attr  mknod_obj_attributes;
    wcc_data      mknod_dir_wcc_ok;
};

struct MKNOD3resfail {
    wcc_data      mknod_dir_wcc_fail;
};

union MKNOD3res switch (nfsstat3 status) {
case NFS3_OK:
           MKNOD3resok   resok;
default:
    MKNOD3resfail resfail;
};

struct REMOVE3resok {
    wcc_data    remove_dir_wcc_ok;
};

struct REMOVE3resfail {
    wcc_data    remove_dir_wcc_fail;
};

union REMOVE3res switch (nfsstat3 status) {
case NFS3_OK:
    REMOVE3resok   resok;
default:
    REMOVE3resfail resfail;
};

struct RMDIR3resok {
    wcc_data    rmdir_dir_wcc_ok;
};

struct RMDIR3resfail {
    wcc_data    rmdir_dir_wcc_fail;
};

union RMDIR3res switch (nfsstat3 status) {
case NFS3_OK:
    RMDIR3resok   resok;
default:
    RMDIR3resfail resfail;
};

struct RENAME3resok {
    wcc_data     rename_fromdir_wcc_ok;
    wcc_data     rename_todir_wcc_ok;
};

struct RENAME3resfail {
    wcc_data     rename_fromdir_wcc_fail;
    wcc_data     rename_todir_wcc_fail;
};

union RENAME3res switch (nfsstat3 status) {
case NFS3_OK:
    RENAME3resok   resok;
default:
    RENAME3resfail resfail;
};

struct LINK3resok {
    post_op_attr   link_file_attributes_ok;
    wcc_data       linkdir_wcc_ok;
};

struct LINK3resfail {
    post_op_attr   link_file_attributes_fail;
    wcc_data       linkdir_wcc_fail;
};

union LINK3res switch (nfsstat3 status) {
case NFS3_OK:
    LINK3resok    resok;
default:
    LINK3resfail  resfail;
};

struct entry3 {
    fileid3      entry_fileid;
    filename3    entry_name;
    cookie3      entry_cookie;
    entry3       *entry_nextentry;
};

struct dirlist3 {
    entry3       *dl_entries;
    bool         dl_eof;
};

struct READDIR3resok {
    post_op_attr readdir_dir_attributes_ok;
    cookieverf3  readdir_cookieverf_ok;
    dirlist3     readdir_reply;
};

struct READDIR3resfail {
    post_op_attr readdir_dir_attributes_fail;
};

union READDIR3res switch (nfsstat3 status) {
case NFS3_OK:
    READDIR3resok   resok;
default:
    READDIR3resfail resfail;
};

struct entryplus3 {
    fileid3      entryp_fileid;
    filename3    entryp_name;
    cookie3      entryp_cookie;
    post_op_attr entryp_name_attributes;
    post_op_fh3  entryp_name_handle;
    entryplus3   *entryp_nextentry;
};

struct dirlistplus3 {
    entryplus3   *dlp_entries;
    bool         dlp_eof;
};

struct READDIRPLUS3resok {
    post_op_attr readdirp_dir_attributes_ok;
    cookieverf3  readdirp_cookieverf_ok;
    dirlistplus3 readdirp_reply;
};

struct READDIRPLUS3resfail {
    post_op_attr readdirp_dir_attributes_fail;
};

union READDIRPLUS3res switch (nfsstat3 status) {
case NFS3_OK:
    READDIRPLUS3resok   resok;
default:
    READDIRPLUS3resfail resfail;
};

struct FSSTAT3resok {
    post_op_attr fsstat_obj_attributes_ok;
    size3        fsstat_tbytes;
    size3        fsstat_fbytes;
    size3        fsstat_abytes;
    size3        fsstat_tfiles;
    size3        fsstat_ffiles;
    size3        fsstat_afiles;
    uint32       fsstat_invarsec;
};

struct FSSTAT3resfail {
    post_op_attr fsstat_obj_attributes_fail;
};

union FSSTAT3res switch (nfsstat3 status) {
case NFS3_OK:
    FSSTAT3resok   resok;
default:
    FSSTAT3resfail resfail;
};

const FSF3_LINK        = 0x0001;
const FSF3_SYMLINK     = 0x0002;
const FSF3_HOMOGENEOUS = 0x0008;
const FSF3_CANSETTIME  = 0x0010;

struct FSINFO3resok {
    post_op_attr fsinfo_obj_attributes_ok;
    uint32       fsinfo_rtmax;
    uint32       fsinfo_rtpref;
    uint32       fsinfo_rtmult;
    uint32       fsinfo_wtmax;
    uint32       fsinfo_wtpref;
    uint32       fsinfo_wtmult;
    uint32       fsinfo_dtpref;
    size3        fsinfo_maxfilesize;
    nfstime3     fsinfo_time_delta;
    uint32       fsinfo_properties;
};

struct FSINFO3resfail {
    post_op_attr fsinfo_obj_attributes_fail;
};

union FSINFO3res switch (nfsstat3 status) {
case NFS3_OK:
    FSINFO3resok   resok;
default:
    FSINFO3resfail resfail;
};

struct PATHCONF3resok {
    post_op_attr pathconf_obj_attributes_ok;
    uint32       pathconf_linkmax;
    uint32       pathconf_name_max;
    bool         pathconf_no_trunc;
    bool         pathconf_chown_restricted;
    bool         pathconf_case_insensitive;
    bool         pathconf_case_preserving;
};

struct PATHCONF3resfail {
    post_op_attr pathconf_obj_attributes_fail;
};

union PATHCONF3res switch (nfsstat3 status) {
case NFS3_OK:
    PATHCONF3resok   resok;
default:
    PATHCONF3resfail resfail;
};

struct COMMIT3resok {
    wcc_data   commit_file_wcc_ok;
    writeverf3 commit_verf;
};

struct COMMIT3resfail {
    wcc_data   commit_file_wcc_fail;
};

union COMMIT3res switch (nfsstat3 status) {
case NFS3_OK:
    COMMIT3resok   resok;
default:
    COMMIT3resfail resfail;
};


/* -- Types for mount protocol -- */

const MNTPATHLEN = 1024;
const MNTNAMLEN  = 255;
/* also FHSIZE3 from above */

typedef string dirpath<MNTPATHLEN>;
typedef string name<MNTNAMLEN>;
/* also nfs_fh3 from above */

enum mountstat3 {
    MNT3_OK = 0,                 /* no error */
    MNT3ERR_PERM = 1,            /* Not owner */
    MNT3ERR_NOENT = 2,           /* No such file or directory */
    MNT3ERR_IO = 5,              /* I/O error */
    MNT3ERR_ACCES = 13,          /* Permission denied */
    MNT3ERR_NOTDIR = 20,         /* Not a directory */
    MNT3ERR_INVAL = 22,          /* Invalid argument */
    MNT3ERR_NAMETOOLONG = 63,    /* Filename too long */
    MNT3ERR_NOTSUPP = 10004,     /* Operation not supported */
    MNT3ERR_SERVERFAULT = 10006  /* A failure on the server */
};

struct mountres3_ok {
    nfs_fh3    fhandle;
    int        auth_flavors<>;
};

union mountres3 switch (mountstat3 fhs_status) {
case MNT3_OK:
    mountres3_ok  mountinfo;
default:
    void;
};

typedef struct mountbody *mountlist;

struct mountbody {
    name       ml_hostname;
    dirpath    ml_directory;
    mountlist  ml_next;
};

typedef struct groupnode *groups;

struct groupnode {
    name     gr_name;
    groups   gr_next;
};

typedef struct exportnode *exports;

struct exportnode {
    dirpath  ex_dir;
    groups   ex_groups;
    exports  ex_next;
};

program NFS {
    version V3 {
	
	void
	    null(void)                    = 0;
	
	GETATTR3res
	    getattr(nfs_fh3)              = 1;
	
	SETATTR3res
	    setattr(nfs_fh3, sattr3, sattrguard3) = 2;
	
	LOOKUP3res
	    lookup(diropargs3)            = 3;
	
	ACCESS3res
	    access(nfs_fh3, uint32)       = 4;
	
	READLINK3res
	    readlink(nfs_fh3)             = 5;
	
	READ3res
	    read(nfs_fh3, offset3, count3) = 6;
	
	WRITE3res
	    write(nfs_fh3, offset3, count3, stable_how, opaque_data) = 7;
	
	CREATE3res
	    create(diropargs3, createhow3) = 8;
	
	MKDIR3res
	    mkdir(diropargs3, sattr3)       = 9;
	
	SYMLINK3res
	    symlink(diropargs3, symlinkdata3) = 10;

	MKNOD3res
	    mknod(diropargs3, mknoddata3) = 11;

	REMOVE3res
	    remove(diropargs3)            = 12;
	
	RMDIR3res
	    rmdir(diropargs3)             = 13;
	
	RENAME3res
	    rename(diropargs3, diropargs3) = 14;
	
	LINK3res
	    link(nfs_fh3, diropargs3)      = 15;
	
	READDIR3res
	    readdir(nfs_fh3, cookie3, cookieverf3, count3) = 16;
	
	READDIRPLUS3res
	    readdirplus(nfs_fh3, cookie3, cookieverf3, count3, count3) = 17;
	
	FSSTAT3res
	    fsstat(nfs_fh3)               = 18;
	
	FSINFO3res
	    fsinfo(nfs_fh3)               = 19;
	
	PATHCONF3res
	    pathconf(nfs_fh3)             = 20;
	
	COMMIT3res
	    commit(nfs_fh3, offset3, count3) = 21;
	
    } = 3;
} = 100003;


program MOUNT {
    version V3 {

	void      null(void)    = 0;
	mountres3 mnt(dirpath)  = 1;
	mountlist dump(void)    = 2;
	void      umnt(dirpath) = 3;
	void      umntall(void) = 4;
	exports   export(void)  = 5;

    } = 3;

} = 100005;


#endif
