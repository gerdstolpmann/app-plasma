/* $Id$ -*- c -*- */

/** {1:namesync [Namesync]} */

/** This RPC program is used to synchronize several namenodes
    by exchanging checkpoints and log files. It is running on the
    coordinator only.
*/

#include "pfs_types.x"

#ifndef PFS_NN_NAMESYNC
#define PFS_NN_NAMESYNC

program Namesync {
    version V1 {
        /** {2 [null] } */

        void null(void) = 0;

        /** {2:create_checkpoint [create_checkpoint] } */

        rdump create_checkpoint(void) = 10;
        /** Triggers the creation of a new checkpoint. This procedure
            does not wait until the checkpoint is done, but returns
            as soon as the name of the dump is known.

            Codes:
             - [ECONFLICT]: another checkpoint is still being created
             - [EPERM]: checkpointing is currently blocked
        */

        /** {2:get_checkpointing_status [get_checkpointing_status] } */

        rvoid get_checkpointing_status(dump) = 11;
        /** Retrieve the success status of an ongoing checkpoint creation.
            Codes:
             - [OK]: the checkpoint exists
             - [ETBUSY]: the creation is not yet finished
             - [ENOENT]: no such dump at all
             - [ESTALE]: there are no transaction data for this dump (i.e.
               it is an old checkpoint)
	     - [EFAILED]: an error occurred during the creation of the cp
        */

        /** {2:block_checkpointing [block_checkpointing] } */

        hyper block_checkpointing(hyper) = 12;
        /** Requests to block checkpointing until the specified time
            (in seconds since the epoch). The function returns the
            confirmed time until which checkpointing is blocked, which
            may be lower than the request.

            Blocking new checkpoints is useful when a resync is in
            progress in order to prevent that the current checkpoint
            outdates.
        */

        /** {2:create_log_dump [create_log_dump] } */

        rdump create_log_dump(dump, hyper) = 20;
        /** [create_log_dump(checkpoint, min_id)]: Creates a new log
            dump for this checkpoint with all log entries >= [min_id].

            This procedure
            does not wait until the checkpoint is done, but returns
            as soon as the name of the dump is known.

            Codes:
             - [ESTALE]: The passed checkpoint is not the current checkpoint
        */

        /** {2:get_log_dumping_status [get_log_dumping_status] } */

        rvoid get_log_dumping_status(dump) = 21;
        /** Retrieve the success status of an ongoing dumping operation.
            Codes:
             - [OK]: the dump exists
             - [ETBUSY]: the dump is not yet finished
             - [ENOENT]: no such dump at all
             - [ESTALE]: there are no transaction data for this dump (i.e.
               it is an old dump)
        */

        /** {2:list_dumps [list_dumps] } */

        dumps list_dumps(dump) = 30;
        /** Lists available and finished dumps matching the pattern passed
            as parameter. If a non-empty string is passed as [dump_prefix],
            [dump_ref] or [dump_suffix] only dumps are returned with the
            same property. An empty string does not restrict the property.
            A negative [dump_ts] means to only return dumps older than
            [-dump_ts]. A positive [dump_ts] means to only return dumps
            newer than [dump_ts]. A zero [dump_ts] does not restrict the
            timestamp.
        */

        /** {2:list_dump_files [list_dump_files] } */

        dumpfiles list_dump_files(dump) = 31;
        /** Return the dump files for a single dump. Empty on error. */

        /** {2:read_dump_file [read_dump_file] } */
        
        rlongstring read_dump_file(dump, longstring, hyper, int) = 32;
        /** [read_dump_file(dump, dump_file, pos, len)]: reads a
            portion of a dump file, and returns the data as string.
	    The length len must not be larger than 1M (1024*1024).

            If the file has at least the length [pos+len] the returned
            string has exactly the length [len]. If the file is shorter,
            the string covers only the existing part of the file. If
            [pos] points to a file position beyond the end of the file,
            the returned string is empty.
        */

        bool attach1(longstring, longstring) = 40;
        /** [attach1(clustername,hostport)]: Prepares the reattachment
            of a dead namenode to an existing cluster at the end of the
            synchronization. [hostport] must be the host and port of
            the caller, i.e. the namenode to attach.

            This function blocks any new commits in the coordinator
            (until [attach2] or until the TCP connection breaks).
            Also, it is waited until the commits being done are finished.
            Basically, all writing filesystem procedures are halted.
            The caller has now the chance to get and apply the last
            log dump to get synchronized with the coordinator database.

            Returns [true] on success.
        */

        bool attach2(longstring, longstring, longstring, longstring) = 41;
        /** [attach1(clustername,hostport,max_revid,cksum_revids)]: 
            Reattaches the caller as namenode slave to the coordinator.
            This unblocks pending commits again, and the filesystem is
            again fully functional.

            The slave [hostport] is re-enabled, and will now get
            db updates (with the {!Pfs_nn_internal.nameslave} program).

            [max_revid] and [cksum_revids] are as for [Elect.set_coordinator].
         */
        

    } = 1;
} = 0x8000e006;


#endif
