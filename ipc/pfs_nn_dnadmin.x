/* $Id$ -*- c -*- */

/** {1:dn_admin [Dn_admin]} */

/** Administration of datanodes */

#include "pfs_types.x"

#ifndef PFS_NN_DNADMIN
#define PFS_NN_DNADMIN

program Dn_admin {
    version V1 {
	/** {2 [null] } */

	void null(void) = 0;

	/** {2 [add_datanode] } */

	void add_datanode(longstring, hyper) = 1;
	/** [add_datanode(identity, size)]:
	   adds a new datanode. It is in disabled state initially
	*/

	/** {2 [enable_datanode] } */

	void enable_datanode(longstring, longstring) = 2;
	/** [enable_datanode(identity, hostport)]: Enables the data node for
           [identity] on [hostport].
	*/

	/** {2 [disable_datanode] } */

	void disable_datanode(longstring) = 3;
	/** [disable_datanode(identity)]:

           This means blocks are no longer allocated on the node, and
           if blocks from this node are reported as non-alive by
           [get_blocks]. This datanode state is considered temporary
           (e.g. while moving a disk from one node to another).

           The coordinator pings the datanodes anyway, and finds quickly
           out whether nodes are down. This RPC is more useful for
           administratively taking nodes down, e.g. for maintenance. 
	 */

	/** {2 [is_enabled] } */

	bool is_enabled(longstring) = 4;
	/** whether this identity is enabled */

	/** {2 [is_alive] } */

	bool is_alive(longstring) = 8;
	/** whether this identity is alive */

	/** {2 [fsstat] } */

	fsstat fsstat(longstring) = 9;
	/** return the block statistics for this datanode. Fills only
	    [total_blocks], [used_blocks], and [trans_blocks].
	 */

	/** {2 [lookup] } */

	longstring lookup(longstring) = 5;
	/** looks the identity up and returns hostport. Only for enabled
           nodes that could be at least contacted once. The node may be
           unresponsive now, though.

           Returns the empty string if the identity is known, but no
           address is associated with it, or if the node is disabled.

           Fails if the identity is unknown.
	*/

	/** {2 [datanodes] } */

	longstrings datanodes(void) = 6;
	/** Returns the identities of all datanodes */

	/** {2 [destroy_datanode] } */

	void destroy_datanode(longstring) = 7;
	/** [destroy_datanode(identity)]:

	    This removes all information about this datanode from the
	    namenode database.
	*/

    } = 1;
} = 0x8000e003;
#endif
