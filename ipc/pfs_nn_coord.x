/* $Id$ -*- -c -*- */

/** {1:coordination [Coordination]} */

/** Find the coordinator.
 */

/** One of the name nodes is elected to be the coordinator at cluster
    startup time. Right now, all namenode requests have to go to the
    coordinator. (In the future, the other namenodes may be allowed to
    respond to certain read requests.)
*/

#include "pfs_types.x"

#ifndef PFS_NN_COORD
#define PFS_NN_COORD

program Coordination {
    version V1 {

        /** {2 [null] } */

	void null(void) = 0;

        /** {2:find_coordinator [find_coordinator] } */

	longstring_opt find_coordinator(longstring) = 1;
	/** Returns the current coordinator as host:port string. The Filesystem
           requests must be sent to the coordinator.

           The arg is the clustername. If this node is not part of this
           cluster the RPC will return NULL.
	 */

        /** {2 [find_namenodes] } */

	longstrings find_namenodes(longstring) = 2;
	/** Return all name nodes, including the coordinator and the slaves.

           The arg is the clustername. If this node is not part of this
           cluster the RPC will return an empty array.
	 */

        /** {2:is_coordinator [is_coordinator] } */

	bool is_coordinator(longstring) = 3;
	/** Whether this node is the coordinator

           The arg is the clustername. If this node is not part of this
           cluster the RPC will return false.
	*/

        /** {2 [clustername] } */

	longstring clustername(void) = 4;
	/** Returns the clustername */

	/* find_inodecaches: no longer needed. This functionality is now
	   directly provided by Filesystem.
	 */	

        /** {2 [local_identities] } */

	longstrings local_identities(longstring) = 6;
	/** Returns the identities of the data nodes running on the machine
	   of the caller. Only available data nodes are returned.
	   The arg is the clustername.
	*/

    } = 1;
} = 0x8000e002;

#endif
