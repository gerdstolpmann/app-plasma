(* $Id$ *)

(* This is the cardgames map/reduce example from the toolkit tutorial.
   The program here contains both solutions, the non-distributed, and
   the distributed one. Also, there is a helper to produce test
   data.

   Build: omake cardgames

   Check the configuration file cardgames.conf before starting it!

   --- How to produce testdata ---

   In order to write test data to the PlasmaFS directory /games, use
   this command. Note that the directory will be created.

   $ ./cardgames testdata plasma::/games

   The switches -games and -players can be used to modify the sample.


   --- How to run the non-distributed implementation ---

   Assumed /games is the input directory, we want to write the output
   to /avg (which is created by the program):

   $ ./cardgames non_distrib plasma::/games plasma::/avg

   --- How to run the distributed implementation ---

   First start the task servers:

   $ ./cardgames start_task_servers

   After that, the invocation is practically the same as in the
   non-distributed case:

   $ ./cardgames distrib plasma::/games plasma::/avg

   Note that the "work" and "log" directories are placed below /avg
   (/avg/_work and /avg/_log, respectively).
 *)

open Printf
open Mapred_toolkit


type game_feature =
  { player_name : string;
    game_id : int;
    money : float
  }


type avg = string * float
  (* player name, average win *)


let game_codec =
  ( <:rfun< @ game -> 
       sprintf "%s\t%d\t%f" game.player_name game.game_id game.money
    >>,
    <:rfun< @ s p l ->
       match Mapred_fields.text_fields ~pos:p ~len:l ~num:3 s with
         | [ pn; gid; m ] ->
	     { player_name = pn;
               game_id = int_of_string gid;
               money = float_of_string m
             }
         | _ ->
	     failwith "bad game_feature line"
    >>
  )


let avg_codec =
  ( <:rfun< @ r -> 
       let (name,avg) = r in
       sprintf "%s\t%f" name avg
    >>,
    <:rfun< @ s p l ->
       match Mapred_fields.text_fields ~pos:p ~len:l ~num:2 s with
         | [ name; avg_s ] ->
	     (name, float_of_string avg_s)
         | _ ->
	     failwith "bad avg line"
    >>
  )


(**********************************************************************)

module Testdata = struct

  let generate players games pl_out =
    let names = Hashtbl.create 17 in

    let rec random_name() =
      let s = String.create 10 in
      for k = 0 to 9 do
	s.[k] <- Char.chr (65 + Random.int 26);
      done;
      if Hashtbl.mem names s then
	random_name()
      else (
	Hashtbl.add names s ();
	s
      ) in

    let all_players =
      Array.init players (fun _ -> random_name()) in
    
    let random_game id =
      let picked = Hashtbl.create 5 in

      let rec pick_name() =
	let name = all_players.(Random.int players) in
	if Hashtbl.mem picked name then
	  pick_name()
	else (
	  Hashtbl.add picked name ();
	  name
	) in

      let n = 2 + Random.int 6 in
      let sum = ref(Random.float 100.0) in
      let winner =
	{ player_name = pick_name();
	  game_id = id;
	  money = !sum
	} in
      let losers = 
	Array.init
	  n
	  (fun i -> 
	     let money =
	       if i < n-1 then
		 Random.float (!sum /. 2.0)
	       else
		 !sum in
	     sum := !sum -. money;
	     { player_name = pick_name();
	       game_id = id;
	       money = (-.money);
	     }
	  ) in
      winner :: Array.to_list losers
    in

    let rec gen_games k seq =
      if k < games then (
	let g = random_game k in
	let seq' = Seq.addl g seq in
	gen_games (k+1) seq'
      )
      else seq
    in

    let st = Store.write_place pl_out in
    let seq = Seq.extend st in
    let _seq' = gen_games 0 seq in
    Store.close st
 

  let exec_job me mj =
    let players = ref 2000 in
    let games = ref 100000 in

    let output_place = ref "" in

    Arg.parse
      [ "-players", Arg.Set_int players,
	"<n>   Set number of players to simulate";

	"-games", Arg.Set_int games,
	"<n>   Set number of games to simulate"
      ]
      (fun arg -> 
         if !output_place = "" then
           output_place := arg
         else
           raise(Arg.Bad("Unexpected arg: " ^ arg))
      )
      "usage: testdata <output_place>";

    if !output_place = "" then
      failwith "Missing argument";

    if !players < 7 then
      failwith "At least 7 players required";

    let rc = Mapred_def.get_rc me (16 * 1024 * 1024) in

    let pl_out = 
      Place.create
	me#filesystem game_codec `Line_structured rc (`Deep_dir !output_place)
    in

    generate !players !games pl_out
end

(**********************************************************************)

module Non_distrib = struct
  let mapred me mj pl_in pl_out partitions =

    let _s_out =
      Seq.mapl_sort_fold
	~mapl:(fun game -> [game])
	~hash:(fun g ->
		 let p = g.player_name in
		 Mapred_sorters.String_asc.hash p 0 (String.length p))
	~cmp:(fun gx gy ->
		String.compare gx.player_name gy.player_name) 
	~initfold:(fun p -> None)
	~fold:(fun acc g ->
		 match acc with
		   | None -> 
		       (Some(g.player_name,1,g.money), [])
		   | Some(n,k,s) when n = g.player_name ->
		       (Some(n,k+1,s+.g.money), [])
		   | Some(n,k,s) ->
		       (Some(g.player_name,1,g.money), [n, s /. float k])
	      )
	~finfold:(fun acc ->
		    match acc with
		      | None -> []
		      | Some(n,k,s) -> [n, s /. float k]
		 )
	~partition_of:(fun g -> (Hashtbl.hash g.player_name) mod partitions)
	~partitions
	pl_in
	pl_out in
    ()

  let exec_job me mj =
    let input_place = ref "" in
    let output_place = ref "" in
    let partitions = ref 1 in

    Arg.parse
      [ "-p", Arg.Set_int partitions,
	"<n>   Set the number of partitions";
      ]
      (fun arg -> 
	 if !input_place = "" then
           input_place := arg
	 else
           if !output_place = "" then
             output_place := arg
           else
             raise(Arg.Bad("Unexpected arg: " ^ arg))
      )
      "usage: non_distrib <input_place> <output_place>";
    
    if !input_place = "" || !output_place = "" then
      failwith "Missing argument";
    
    
    let rc = Mapred_def.get_rc me (16 * 1024 * 1024) in
    
    let pl_in = 
      Place.from
	me#filesystem game_codec `Line_structured rc (`Deep_dir !input_place) in
    let pl_out = 
      Place.create
	me#filesystem avg_codec `Line_structured rc (`Deep_dir !output_place) in
    mapred me mj pl_in pl_out !partitions
end

(**********************************************************************)

module Distrib = struct
  let f_mapl =
    <:rfun< @ mi game -> [ game ] >> ;;
    
  let f_hash =
    <:rfun< @ mi g -> 
      let p = g.player_name in
      Mapred_sorters.String_asc.hash p 0 (String.length p)
    >> ;;

  let f_cmp =
    <:rfun< @ mi gx gy ->
       String.compare gx.player_name gy.player_name
    >> ;;

  let f_initfold =
    <:rfun< @ mi p -> None >> ;;

  let f_fold =
    <:rfun< @ mi acc g ->
      match acc with
	| None -> 
	    (Some(g.player_name,1,g.money), [])
	| Some(n,k,s) when n = g.player_name ->
	    (Some(n,k+1,s+.g.money), [])
	| Some(n,k,s) ->
	    (Some(g.player_name,1,g.money), [n, s /. float k])
    >> ;;

  let f_finfold =
    <:rfun< @ mi acc ->
      match acc with
	| None -> []
        | Some(n,k,s) -> [n, s /. float k]
    >> ;;

  let f_partition_of =
    <:rfun< partitions @ mi g ->
      (Hashtbl.hash g.player_name) mod partitions
    >> ;;
	
  let mapred me mj pl_in pl_out partitions =
    let config =
      DSeq.create_config ~report:true ~partitions me in
    
    let r_out =
      DSeq.mapl_sort_fold
	~mapl:f_mapl
	~hash:f_hash
	~cmp:f_cmp
	~initfold:f_initfold
	~fold:f_fold
	~finfold:f_finfold
	~partition_of:(f_partition_of partitions)
	pl_in
	pl_out
	config
	game_codec in
    let _s_out =
      DSeq.get_result r_out in
    ()

  let exec_job me mj =
    let input_place = ref "" in
    let output_place = ref "" in
    let partitions = ref 1 in

    Arg.parse
      [ "-p", Arg.Set_int partitions,
	"<n>   Set the number of partitions";
      ]
      (fun arg -> 
	 if !input_place = "" then
           input_place := arg
	 else
           if !output_place = "" then
             output_place := arg
           else
             raise(Arg.Bad("Unexpected arg: " ^ arg))
      )
      "usage: distrib <input_place> <output_place>";
    
    if !input_place = "" || !output_place = "" then
      failwith "Missing argument";
    
    
    let rc = Mapred_def.get_rc me (16 * 1024 * 1024) in
    
    let pl_in = 
      Place.from
	me#filesystem game_codec `Line_structured rc (`Deep_dir !input_place) in
    let pl_out = 
      Place.create
	me#filesystem avg_codec `Line_structured rc (`Deep_dir !output_place) in
    mapred me mj pl_in pl_out !partitions

end

(**********************************************************************)

let () =
  Mapred_main.dispatch
    [ "testdata", "Generate testdata", 
      Mapred_toolkit.toolkit_job, Testdata.exec_job;

      "non_distrib", "Average profit per player (non-distributed)",
      Mapred_toolkit.toolkit_job, Non_distrib.exec_job;

      "distrib", "Average profit per player (distributed)",
      Mapred_toolkit.toolkit_job, Distrib.exec_job

    ]

