(* $Id$ *)

(* The [distill] example from the toolkit tutorial.

   This program scans an input place, and copies every tenth record
   to the output place. Input and output places must be given using
   a filename with tree prefix, i.e. "file::/path" or "plasma::/path".
   The output place must not yet exist.
   Also don't forget to adapt tenth.conf to your needs.

   Build: omake tenth
   Run: ./tenth exec_job file::/home/gerd/input file::/home/gerd/output
 *)

open Mapred_toolkit

let codec =
  ( <:rfun< @ s -> String.copy s >>,
    <:rfun< @ s p l -> String.sub s p l >>
  )


let distill pl1 pl2 =
  let rec distill_seq s1 s2 n =
    if not (Seq.is_empty s1) then (
      if n = 10 then
        distill_seq (Seq.tl s1) (Seq.add (Seq.hd s1) s2) 1
      else
        distill_seq (Seq.tl s1) s2 (n+1)
    ) in
  let distill_store st1 st2 =
    let s1 = Seq.read st1 in
    let s2 = Seq.extend st2 in
    distill_seq s1 s2 1
  in
  List.iter
    (fun st1 ->
       let filename = Filename.basename(Store.file_name st1) in
       let st2 = Store.write_place ~filename pl2 in
       distill_store st1 st2;
       Store.close st1;
       Store.close st2;
    )
    (Store.read_place pl1)


let exec_job me mj =
  let input_place = ref "" in
  let output_place = ref "" in

  Arg.parse
    []
    (fun arg -> 
       if !input_place = "" then
	 input_place := arg
       else
	 if !output_place = "" then
	   output_place := arg
	 else
	   raise(Arg.Bad("Unexpected arg: " ^ arg))
    )
    "usage: exec_job <input_place> <output_place>";

  if !input_place = "" || !output_place = "" then
    failwith "Missing argument";


  let rc = Mapred_def.get_rc me (16 * 1024 * 1024) in

  let pl1 = 
    Place.from
      me#filesystem codec `Line_structured rc (`Deep_dir !input_place) in
  let pl2 = 
    Place.create
      me#filesystem codec `Line_structured rc (`Deep_dir !output_place) in
  distill pl1 pl2


let () =
  Mapred_main.dispatch
    [ "exec_job", "This is my_job", Mapred_toolkit.toolkit_job, exec_job ]
