(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* Count the frequencies of words in the input files. Every input
   line is split into words (assuming whitespace as delimiter),
   and the frequency of each word is counted. The output consists
   then of lines "<word> TAB <freq>".
 *)

let job : Mapred_def.mapred_job =
object
  method custom_params = []
  method check_config _ _ = ()
  method pre_job_start _ _ = ()
  method post_job_finish _ _ = ()

  method map me jc id r w =
    try
      while true do
        let r = r#input_record() in
	let words = Pcre.split r in
        List.iter (fun word -> w # output_record word) words
      done
    with End_of_file ->
      w # flush()

  method extract_key me jc line = (0,String.length line)

  method partition_of_key me jc s p l = 
    let key = String.sub s p l in
    (Hashtbl.hash key) mod jc#partitions

  method reduce me jc p r w =
    let last = ref "" in
    let freq = ref 0  in
    try
      while true do
        let r = r#input_record() in
	if r = !last then
	  incr freq
	else (
	  if !freq > 0 then
	    w # output_record (!last ^ " " ^ string_of_int !freq);
	  last := r;
	  freq := 1
	);
      done
    with End_of_file ->
      if !freq > 0 then 
	w # output_record (!last ^ " " ^ string_of_int !freq);
      w # flush()

  method sorter =
    Mapred_sorters.generic_sorter
      ~hash:(Mapred_sorters.String_asc.hash)
      ~cmp:(Mapred_sorters.String_asc.cmp)

  method combine _ _ _ = None

  method input_record_io me jc =
    Mapred_io.line_structured_format()
  method internal_record_io me jc =
    Mapred_io.var_size_format()
  method output_record_io me jc =
    Mapred_io.line_structured_format()
      
end

let () =
  Mapred_main.main job
