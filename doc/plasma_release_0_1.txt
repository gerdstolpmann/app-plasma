{1 Release Notes For Plasma}

{b This is version:} 0.1 "vorfreude". This is an alpha release to make
Plasma known to interested developers.

{2 What is working and not working in PlasmaFS}

Generally, PlasmaFS works as described in the documentation. Crashes
have not been observed for quite some time now, but occasionally one
might see critical exceptions in the log file.

PlasmaFS has so far only been tested on 64 bit, and only on Linux
as operation system. There are known issues for 32 bit machines,
especially the blocksize must not be larger than 4M.

Data safety: Cannot be guaranteed. It is not suggested to put valuable
data into PlasmaFS.

Known problems:
 - It is still unclear whether the timeout settings are acceptable.
 - There might be name clashes for generated file names. Right now it is
   assumed that the random number generator returns unique names, but this
   is for sure not the case.
 - The generated inode numbers are not necessarily unique after namenode 
   restarts.
 - The transaction semantics is not fully clear. Some operations use
   "read committed", some "repeatable read".


Not implemented features:
 - The namenodes cannot yet detect crashed datanodes. Datanodes are always
   reported as alive.
 - The ticket system is not fully implemented (support for "read").
 - There is no authorization system (file access rights are ignored)
 - There is no authentication system to secure filesystem accesses (such
   as Kerberos)
 - There are too many hard-coded constants.
 - The [names] SQL table should also store the parent inode number.
 - The file name read/lookup functions should never return [ECONFLICT]
   errors.
 - Write support for NFS
 - Translation of access rights to NFS
 - Support for checksums
 - Support for "host groups", so that it is easier to control which machines
   may store which blocks. Semantics have to be specified yet.
 - Better block allocation algorithms. The current algorithm works only well
   when many blocks are allocated at once. It is very bad when a file is
   extended block by block.
 - Define how blocks are handled that are allocated but never written.
 - Support for resolving symbolic links
 - Recognition of the death of the coordinator, and restart of the
   election algorithm.
 - Multicast discovery of datanodes
 - Lock manager (avoid that clients have to busy wait on locks)
 - Restoration of missing replicas
 - Rebalancing of the cluster
 - Automated copying of the namenode database to freshly added namenode slaves

{2 What is working and not working in Plasma MapReduce}

Known problems:
 - Management of available RAM is not yet sufficient

Not implemented features:
 - Task servers should be able to provide several kinds of jobs
 - Think about dynamically extensible task servers
 - Run jobs only defining [map] but no [reduce].
 - Streaming mode as in Hadoop
 - Support for combining (an additional fold function run after each
   shuffle task to reduce the amount of data)
 - nice web interface
 - per-task log files (stored in PlasmaFS)
 - support user counters as in Hadoop
 - restart/relocation of failed tasks 
 - recompute intermediate files that are no longer accessible due to node
   failures
 - Support chunk sizes larger than the block size
 - Speculative execution of tasks
 - Support job management (remember which jobs have been run etc.)
 - Support to execute several jobs at once

What we will never implement:
 - Jobs only consisting of [reduce] but no [map] cannot be supported
   due to the task scheme. (Reason: Input files for sort tasks must
   not exceed [sort_limit].)
