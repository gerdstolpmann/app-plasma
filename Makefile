# This invoke omake

.PHONY: all clean install

all:
	omake

clean:
	omake clean

install:
	omake install
