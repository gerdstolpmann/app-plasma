#! /bin/sh

# $Id$

#######################################################################
# Helpers:

# Split $PATH into words:
oldifs="$IFS"
IFS="   :"
spacepath=`echo $PATH`
IFS="$oldifs"


in_path () {
    # Does $1 exist in $PATH?
    for d in $spacepath; do
        if test -x "$d/$1"; then
            return 0
        fi
    done
    return 1
}


get_path () {
    for d in $spacepath; do
        if test -x "$d/$1"; then
            echo "$d/$1"
            return
        fi
    done
}

######################################################################

set_defaults () {
    bindir=`dirname "$ocamlc"`
    sharedir=`dirname $bindir`/share/plasma
}

ocamlc=`get_path ocamlc`
set_defaults

version="0.6.2"

######################################################################

check_library () {
    # $1: the name of the library (findlib)
    ocamlfind query "$1" >/dev/null 2>/dev/null
    return
}


print_options () {
    echo "    -bindir $bindir"
    echo "    -sharedir $sharedir"
}


usage () {
        set_defaults
        cat <<_EOF_ >&2
usage: ./configure [ options ]

-bindir dir
        Install binaries into this directory

-sharedir dir
        Install shared files into this directory

Defaults are:

_EOF_
	print_options

	fldir="$(ocamlfind printconf destdir || echo "n/a")"
cat <<_EOF_ >&2

Ocaml libraries are installed to the findlib location which is:
     $fldir
_EOF_
	exit 1
}


echo "Welcome to Plasma version $version" >&2

while [ "$#" -gt 0 ]; do
    case "$1" in
	-bindir)
	    bindir="$2"; shift; shift ;;
	-sharedir)
	    sharedir="$2"; shift; shift ;;
	-version)
	    echo "$version"
	    exit 0 ;;
	*)
	    usage ;;
    esac
done

######################################################################
# Check ocaml, ocamlfind and libraries

printf "%s" "Checking for Ocaml..."
if in_path ocamlc; then
    echo "found"
else
    echo "not found"
    echo "Get ocaml from http://caml.inria.fr"
    exit 1
fi

echo "Checking Ocaml setup..."
if ! in_path ocamlopt; then
    echo "The native-code compiler ocamlopt was not found. Compiling to"
    echo "bytecode will generally work, but may result in poor performance"
fi

ocaml_version="$(ocamlc -version)"
old_ocaml_version=0
case "$ocaml_version" in
    [4-9]*)
	true ;;
    3.1[23456789].*)
	true ;;
    3.[23456789][01234567890].*)
	true ;;
    3.10.*)
	old_ocaml_version=1 ;;
    3.11.*)
	old_ocaml_version=1 ;;
    3.*)
	old_ocaml_version=1 ;;
    2.*|1.*|0.*)
	echo "Your version of ocaml is too old. Get at least 3.12!"
	exit 1 ;;
    *)
	echo "Your version of ocaml could not be recognized" ;;
esac
if [ $old_ocaml_version -gt 0 ]; then
    echo "Your version of ocaml is relatively old. This is untested, and you"
    echo "may run into problems when building or running Plasma. For avoiding"
    echo "problems get version 3.12 or newer"
fi

printf "%s" "Checking for findlib... "
if ocamlfind query stdlib >/dev/null 2>/dev/null; then
    echo "found"
else
    echo "not found"
    echo "Make sure that ocamlfind is in your PATH, or download findlib"
    echo "from http://projects.camlcity.org/projects/findlib.html"
    exit 1
fi

printf "%s" "Checking for Ocamlnet... "
if check_library netsys; then
    echo "found"
else
    echo "not found (or very old)"
    echo "Sorry, installation is not possible without Ocamlnet."
    echo "Get this library from:"
    echo "http://projects.camlcity.org/projects/ocamlnet.html"
    echo "Minimum required version is: 3.0test3"
    exit 1
fi

printf "%s" "Checking Ocamlnet version... "
netsys_version="$(ocamlfind query -format '%v' netsys)"
extra_pkgs=""
case "$netsys_version" in
    0.*|1.*|2.*)
	echo "too old"
	exit 1 ;;
    3.0|3.0.*|3.0test*)
	echo "too old"
	exit 1 ;;
    3.1|3.1.*|3.1test*)
	echo "too old"
	exit 1 ;;
    3.2|3.2.*|3.2test*)
	echo "too old"
	exit 1 ;;
    3.3|3.3.*|3.3test*)
	echo "too old"
	exit 1 ;;
    3.4)
	echo "too old"
	exit 1 ;;
    3.5|3.5.*)
	echo "ok" ;;
    *)
	extra_pkgs="$extra_pkgs netstring-pcre"
	echo "ok" ;;
esac


printf "%s" "Checking for crypto support in Ocamlnet..."
if check_library netmech-scram; then
    echo "found"
else
    echo "not found"
    echo "Build ocamlnet with option -enable-crypto"
fi

printf "%s" "Checking for omake... "
if in_path omake; then
    echo "found"
else
    echo "not found"
    echo "Get omake from http://omake.metaprl.org"
    exit 1
fi

printf "%s" "Checking for PostgreSQL client library (libpq)... "
if in_path pg_config; then
    echo "pg_config found"
else
    echo "pg_config not found"
    echo "This may mean you forgot to install libpq or libpq-dev"
    exit 1
fi

printf "%s" "Checking for ocamlgraph... "
if check_library ocamlgraph; then
    echo "found"
else
    echo "not found"
    echo "Get ocamlgraph from ocamlgraph.lri.fr"
    exit 1
fi

printf "%s" "Checking PostgreSQL version... "
pg_version="$(pg_config --version)"
pg_too_old=0
case "$pg_version" in
    PostgreSQL\ 8.[01].*)
	pg_too_old=1 ;;
    PostgreSQL\ 7.*)
	pg_too_old=1 ;;   # Note pg_config exists since pg 7.1
esac

if [ $pg_too_old -gt 0 ]; then
    echo "too old"
    echo "Get at least PostgreSQL 8.2."
    exit 1
else
    echo "ok"
fi


printf "%s" "Checking for xstrp4... "
if check_library xstrp4; then
    echo "found"
else
    echo "not found"
    echo "Get xstrp4 from camlcity.org"
    exit 1
fi

######################################################################
# Write omake.conf

echo "BINDIR = $bindir" > omake.conf
echo "SHAREDIR = $sharedir" >> omake.conf
echo "EXTRA_PKGS = $extra_pkgs" >> omake.conf

echo "Configuration successful. Start build with"
echo "  omake"
echo "Install with"
echo "  omake install"
echo
echo "Lots of fun with Plasma!"
