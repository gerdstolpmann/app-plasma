#use "topfind";;
#require "pfs_namenode";;
open Pfs_db;;
open Printf;;
let db_config =
  ( object
      method db_host = None
      method db_hostaddr = None
      method db_port = None
      method db_dbname = Some "copy_edge" 
      method db_user = None
      method db_password = None
      method db_ro_connections_max = 20
    end
  );;
let esys = Unixqueue.create_unix_event_system();;
let () = Nn_db.init db_config esys;;
let db = new connect db_config;;

let do_load() =
  let t = Nn_db.transact esys 0 in
  Nn_db.load t#connection "dir";
  ignore(t#commit());
  Unixqueue.run esys

(* do_load() ;;
 *)
