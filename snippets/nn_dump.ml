#use "topfind";;
#require "pfs_namenode";;
open Pfs_db;;
open Uq_engines.Operators;;
open Printf;;
let db_config =
  ( object
      method db_host = None
      method db_hostaddr = None
      method db_port = None
      method db_dbname = Some "plasma_edge" 
      method db_user = None
      method db_password = None
      method db_ro_connections_max = 20
    end
  );;
let esys = Unixqueue.create_unix_event_system();;
let () = Nn_db.init db_config esys;;
let db = new connect db_config;;

let do_dump() =
  let t = Nn_db.transact esys 0 in
  t # isolevel `Read_committed
  ++ (fun () ->
        Nn_db.dump_e t#ro_connection esys "dir"
        ++ (fun files ->
              printf "Wrote: %s\n%!" (String.concat "," files);
              eps_e (`Done()) esys
           )
     )
(* let e = do_dump();; 
   Unixqueue.run esys;;
 *)
