#use "topfind";;
#require "pfs_support";;
open Pfs_db;;
let db_config =
  ( object
      method db_host = None
      method db_hostaddr = None
      method db_port = None
      method db_dbname = Some "plasma_edge" 
      method db_user = None
      method db_password = None
      method db_ro_connections_max = 20
    end
  );;
let db = new connect db_config;;
let esys = Unixqueue.create_unix_event_system();;
let ch = new Netchannels.output_channel (open_out "dump");;
let () = Plasma_util.debug := true;;
let e = copy_out_e db esys ch "datastore" ["id";"identity";"size";"enabled"];;

(*
let e = copy_out_e db esys ch "blockalloc" ["datastore";"blkidx";"blkmap";"blkcnt"];;
 *)
