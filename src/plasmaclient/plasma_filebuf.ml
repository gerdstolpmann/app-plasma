(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Plasma_util.Operators
open Printf

let dlogf = Plasma_util.dlogf

type errno = Plasma_util.errno
type strmem = [`String of string | `Memory of Netsys_mem.memory]

type buf_state =
    [ `Invalid
    | `Clean
    | `Dirty
    | `Reading of int64 option Uq_engines.engine
    | `Writing of int64 option Uq_engines.engine
    | `Written
    | `Dropped
    ]

type buffer =
    { buf_inode : int64;
      buf_index : int64;
      buf : Netsys_mem.memory;
      buf_ord : int;              (* ordinal number of the buffer *)
      mutable buf_seqno : int64;  (* for cache validation only *)
      mutable buf_state : buf_state;
      mutable buf_dirty : bool;
      mutable buf_delayed_drop : bool;
      mutable buf_flushing : bool;
    }

module I64Set = Plasma_util.I64Set

type flush_request =
    { flush_inode : int64;
      mutable flush_min_eof : int64;
      mutable flush_min_mtime : Plasma_rpcapi_aux.time;
      mutable flush_index_list : I64Set.t;
    }

module BLRU = 
  Plasma_util.LRU(struct 
		    type t = buffer
		    let compare b1 b2 =
		      compare b1.buf_ord b2.buf_ord
		  end)

type buffer_system =
    { n_buffers : int;
      blocksize : int;
      esys : Unixqueue.event_system;

      mutable buf_mem : Netsys_mem.memory;
      (** The memory used for the buffers. It is first initialized
	  when the blocksize is known. This is always shared memory
	  obtained via [mmap].
       *)

      mutable buf_next : int;
      (** The buffers 0 to [buf_next-1] are already entered into 
	  the [buffers] hashtable.
       *)

      buffers : (int64 * int64, buffer) Hashtbl.t;
      (** buffers by (inode,index) *)

      mutable dbuffers : BLRU.t;
      (** buffers in state [`Dirty]. The
	  first buffer in the queue is the "oldest" (was for the longest time
	  not accessed), and is the first candidate for starting flushing
       *)

      mutable cbuffers : BLRU.t;
      (** buffers in state [`Clean]. First buffer is the oldest,
	  and the first candidate for reclaiming
       *)

      mutable dropped : BLRU.t;
      (** [`Dropped] buffers *)

      mutable waiting : (int64 * int64) Queue.t;
      mutable notify : (int64 * int64, ((buffer option) -> unit) list) Hashtbl.t;
      (** Users waiting for a buffer. The user has to put a callback function
	  into the [notify] hashtable for the right (inode,index) pair.
	  Also, if this is the first callback function for this pair it
	  is required to append the pair to the [waiting] queue. The
	  callback function is called with [Some b] when a buffer is available,
	  and with [None] when another user got the buffer for the pair.
       *)

      flushreqs : (int64, flush_request) Hashtbl.t;
      (** flush requests per inode *)

      errors : (int64, errno) Hashtbl.t;
      (** errors per inode *)
    }

let zero_time =
  { Plasma_rpcapi_aux.tsecs = 0L;
    tnsecs = 0
  }

let max_time t1 t2 =
  let d =
    Int64.compare t1.Plasma_rpcapi_aux.tsecs t2.Plasma_rpcapi_aux.tsecs in
  if d > 0 then
    t1
  else if d < 0 then
    t2
  else
    if t1.Plasma_rpcapi_aux.tnsecs > t2.Plasma_rpcapi_aux.tnsecs then
      t1
    else
      t2


let now_mtime() =
  let t = Unix.gettimeofday() in
  let t1 = Int64.of_float t in
  let t2 = truncate ((t -. Int64.to_float t1) *. 1E9) in
  { Plasma_rpcapi_aux.tsecs = t1;
    tnsecs = t2
  }


let create_buffer_system n_bufs bsize esys =
  let ps = Netsys_mem.getpagesize() in
  if bsize mod ps <> 0 then
    failwith "Blocksize is not a multiple of the page size";
  let mem_size = bsize * n_bufs in
  let buf_mem = Netsys_mem.alloc_memory_pages mem_size in
  { n_buffers = n_bufs;
    blocksize = bsize;
    esys = esys;
    buf_mem = buf_mem;
    buf_next = 0;
    buffers = Hashtbl.create 7;
    dbuffers = BLRU.create();
    cbuffers = BLRU.create();
    dropped = BLRU.create();
    waiting = Queue.create();
    notify = Hashtbl.create 7;
    flushreqs = Hashtbl.create 7;
    errors = Hashtbl.create 7
  }


let mem_size sys =
  sys.n_buffers * sys.blocksize


let blit_from_buffer sys b bpos sm spos len =
  match sm with
    | `String s ->
	Netsys_mem.blit_memory_to_string b.buf bpos s spos len
    | `Memory m ->
	Plasma_platform.blit_memory b.buf bpos m spos len
	  

let blit_to_buffer c sm spos b bpos len =
  match sm with
    | `String s ->
	Netsys_mem.blit_string_to_memory s spos b.buf bpos len
    | `Memory m ->
	Plasma_platform.blit_memory m spos b.buf bpos len
	  

let clean_access sys b =
  (* For clean and dirty buffers just mark the buffer as "young", i.e.
     recently accessed. The states [`Invalid], [`Reading] and [`Dropped]
     are forbidden here.
   *)
  match b.buf_state with
    | `Clean ->
	BLRU.add b sys.cbuffers
    | `Dirty ->
	BLRU.add b sys.dbuffers
    | `Invalid | `Reading _ | `Dropped ->
	invalid_arg "Plasma_filebuf.clean_access: bad state"
    | _ ->
	()

let dirty_access sys b eof =
  (* [`Invalid] and [`Clean] states are changed to [`Dirty]. [`Written]
     is also possible, but only [buf_dirty] is set
     (as a reminder to transition to [`Dirty] later). [`Reading],
     [`Writing] and [`Dropped] are forbidden here.
   *)
  b.buf_dirty <- true;
  ( match b.buf_state with
      | `Invalid ->
	  BLRU.add b sys.dbuffers;
	  b.buf_state <- `Dirty
      | `Clean ->
	  BLRU.remove b sys.cbuffers;
	  BLRU.add b sys.dbuffers;
	  b.buf_state <- `Dirty
      | `Dirty ->
	  BLRU.add b sys.dbuffers;
	  b.buf_state <- `Dirty
      | `Written ->
	  ()
      | `Reading _ | `Writing _ | `Dropped ->
	  invalid_arg "Plasma_filebuf.dirty_access: bad state"
  );
  (* Also update the flush requests. Of course, we do this only for 
     [`Dirty] buffers, not for [`Written] buffers.
   *)
  if b.buf_state = `Dirty && not b.buf_flushing then (
    let fr = 
      try Hashtbl.find sys.flushreqs b.buf_inode 
      with Not_found ->
	{ flush_inode = b.buf_inode;
	  flush_min_eof = 0L;
	  flush_min_mtime = zero_time;
	  flush_index_list = I64Set.empty;
	} in
    fr.flush_min_eof <- max fr.flush_min_eof eof;
    fr.flush_min_mtime <- max_time fr.flush_min_mtime (now_mtime());
    fr.flush_index_list <- I64Set.add b.buf_index fr.flush_index_list;
    Hashtbl.replace sys.flushreqs b.buf_inode fr
  )


let get_flush_min_eof sys inode =
  let fr = 
    Hashtbl.find sys.flushreqs inode in
  fr.flush_min_eof

let get_flush_min_mtime sys inode =
  let fr = 
    Hashtbl.find sys.flushreqs inode in
  fr.flush_min_mtime


let notify sys inode index b =
  let callbacks = Hashtbl.find sys.notify (inode,index) in
  Hashtbl.remove sys.notify (inode,index);
  match callbacks with
    | [] -> ()
    | success_cb :: fail_cb_list ->
	success_cb (Some b);
	List.iter (fun fail_cb -> fail_cb None) fail_cb_list


let notify_after_error sys inode =
  (* notify all observers for this inode with argument [None]. Also remove
     all these observers from the queue 
   *)
  let l =
    Hashtbl.fold
      (fun (n_inode, n_index) callbacks acc ->
	 if n_inode = inode then
	   (callbacks, n_index) :: acc
	 else
	   acc
      )
      sys.notify
      [] in
  List.iter
    (fun (_, index) ->
       Hashtbl.remove sys.notify (inode,index)
    )
    l;
  let w2 = Queue.create() in
  Queue.transfer sys.waiting w2;
  while not (Queue.is_empty w2) do
    let (w_inode,w_index) = Queue.take w2 in
    if w_inode <> inode then
      Queue.add (w_inode,w_index) sys.waiting
  done;
  let callbacks = List.flatten (List.map fst l) in
  List.iter (fun fail_cb -> fail_cb None) callbacks


let check_for_next_user sys b =
  (* b has been dropped, so check whether another user is waiting for a
     buffer 
   *)
  if not (Queue.is_empty sys.waiting) then (
    let (inode,index) = Queue.take sys.waiting in
    notify sys inode index b
  )


let drop_immediately sys b =
  let old_state = b.buf_state in
  b.buf_state <- `Dropped;
  ( match old_state with
      | `Clean ->
	  BLRU.remove b sys.cbuffers
      | `Dirty ->
	  BLRU.remove b sys.dbuffers
      | _ ->
	  ()
  );
  ( try
      let fr = Hashtbl.find sys.flushreqs b.buf_inode in
      fr.flush_index_list <- I64Set.remove b.buf_index fr.flush_index_list;
    with
      | Not_found -> ()
  );
  BLRU.add b sys.dropped;
  if old_state <> `Dropped then
    Hashtbl.remove sys.buffers (b.buf_inode, b.buf_index);
  check_for_next_user sys b


let drop_if_needed sys b =
  (* Drop b only if there is a user waiting for a fresh buffer *)
  if not (Queue.is_empty sys.waiting) then
    drop_immediately sys b


let switch_to_reading sys b e eof_opt =
  (* First switch to [`Reading e] *)
  ( match e # state with
      | `Working _ -> ()
      | _ ->
	  invalid_arg "Plasma_filebuf.switch_to_reading: bad engine"
  );
  ( match b.buf_state with
      | `Invalid ->
	  ()
      | `Clean -> 
	  BLRU.remove b sys.cbuffers
      | `Dirty ->
	  BLRU.remove b sys.dbuffers
      | _ ->
	  invalid_arg "Plasma_filebuf.switch_to_reading: bad state"
  );
  (* The visible engine e' reaches a terminal state when we are already
     done without own postprocessing
   *)
  let (e', signal) = Uq_engines.signal_engine sys.esys in
  b.buf_state <- `Reading e';
  (* When [e] is done and the state hasn't been switched yet again, we
     switch by default to [`Clean] or [`Dirty]
   *)
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun seqno_opt ->
		( match seqno_opt with
		    | Some seqno when not b.buf_delayed_drop ->
			b.buf_seqno <- seqno;
			( match eof_opt with
			    | Some eof ->
				b.buf_state <- `Invalid;
				dirty_access sys b eof
			    | None ->
				b.buf_state <- `Clean;
				b.buf_dirty <- false;
				BLRU.add b sys.cbuffers;
				drop_if_needed sys b
			)
		    | _ ->
			drop_immediately sys b
		);
		signal (`Done seqno_opt)
	     )
    ~is_error:(fun e -> drop_immediately sys b; signal (`Error e))
    ~is_aborted:(fun _ -> drop_immediately sys b; signal `Aborted)
    e


let switch_to_writing sys b e =
  (* First switch to [`Writing e] *)
  ( match e # state with
      | `Working _ -> ()
      | _ ->
	  invalid_arg "Plasma_filebuf.switch_to_writing: bad engine"
  );
  ( match b.buf_state with
      | `Dirty ->
	  BLRU.remove b sys.dbuffers
      | _ ->
	  invalid_arg "Plasma_filebuf.switch_to_writing: bad state"
  );
  let (e', signal) = Uq_engines.signal_engine sys.esys in
  b.buf_state <- `Writing e';
  b.buf_flushing <- false;
  let on_finished seqno_opt =
    if not b.buf_delayed_drop then (
      b.buf_state <- `Written;
      b.buf_dirty <- false;
      match seqno_opt with
	| None -> b.buf_seqno <- (-1L)  (* don't know better *)
	| Some seqno -> b.buf_seqno <- seqno
    ) 
    else
      drop_immediately sys b in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun r -> on_finished r; signal (`Done r))
    ~is_error:(fun e -> on_finished None; signal (`Error e))
    ~is_aborted:(fun _ -> on_finished None; signal `Aborted)
    e


let schedule_drop sys b =
  match b.buf_state with
    | `Invalid | `Clean | `Dirty | `Written ->
	drop_immediately sys b
    | `Dropped ->
	()
    | `Reading _ | `Writing _ ->
	b.buf_delayed_drop <- true


let schedule_drop_inode sys inode =
  let l =
    Hashtbl.fold
      (fun (_inode,index) b acc ->
	 if inode = _inode then b :: acc else acc
      )
      sys.buffers
      [] in
  List.iter (schedule_drop sys) l


let lookup_buffer sys inode index =
  let b = Hashtbl.find sys.buffers (inode,index) in
  if b.buf_state = `Dropped then raise Not_found;
    (* actually, `Dropped should not occur in sys.buffers *)
  b


let reassign_buffer sys b inode index =
  (* Re-use an old dropped or clean buffer *)
  ( match b.buf_state with
      | `Clean ->
	  BLRU.remove b sys.cbuffers;
	  Hashtbl.remove sys.buffers (b.buf_inode, b.buf_index);
      | `Dropped ->
	  BLRU.remove b sys.dropped
	    (* dropped buffers are not in sys.buffers *)
      | _ ->
	  failwith "Plasma_filebuf.reassign_buffer"
  );
  let b' =
    { b with
	buf_inode = inode;
	buf_index = index;
	buf_seqno = (-1L);
	buf_state = `Invalid;
	buf_dirty = false;
	buf_delayed_drop = false;
	buf_flushing = false
    } in
  Hashtbl.replace sys.buffers (inode,index) b';
  b'


let new_buffer sys inode index =
  (* Allocate a new invalid buffer record for [(inode,index)] *)
  let p = sys.buf_next in
  let pos = p * sys.blocksize in
  let b =
    { buf_inode = inode;
      buf_index = index;
      buf_seqno = 0L;
      buf_ord = p;
      buf = Bigarray.Array1.sub sys.buf_mem pos sys.blocksize;
      buf_state = `Invalid;
      buf_dirty = false;
      buf_delayed_drop = false;
      buf_flushing = false;
    } in
  sys.buf_next <- sys.buf_next + 1;
  Hashtbl.add sys.buffers (inode,index) b;
  b


let request_buffer_e sys inode index =
  try 
    if not(Hashtbl.mem sys.errors inode) then (
      let _b = lookup_buffer sys inode index in
      ();
    );
    eps_e (`Done None) sys.esys
  with Not_found ->
    (* If we have less than [n_buffers], just create a new one *)
    if sys.buf_next < sys.n_buffers then (
      dlogf
	"request_buffer_e inode=%Ld index=%Ld: creating buffer" inode index;
      let b = new_buffer sys inode index in
      eps_e (`Done(Some b)) sys.esys
    )
    else (
      (* Look for a dropped or clean buffer *)
      try
	let b = 
	  try BLRU.take sys.dropped
	  with Not_found -> BLRU.take sys.cbuffers in
	dlogf
	  "request_buffer_e inode=%Ld index=%Ld: reusing buffer" inode index;
	let b' = reassign_buffer sys b inode index in
	eps_e (`Done (Some b')) sys.esys
      with
	| Not_found ->
	    (* It is not possible to immediately fulfill the request.
	       Add ourselves to the queue of waiters 
	     *)
	    dlogf
	      "request_buffer_e inode=%Ld index=%Ld: waiting for buffer"
	      inode index;
	    let (e,signal) = Uq_engines.signal_engine sys.esys in
	    let on_buffer b_opt = 
	      match b_opt with
		| Some b ->
		    dlogf
		      "request_buffer_e inode=%Ld index=%Ld: got buffer"
		      inode index;
		    let b' = reassign_buffer sys b inode index in
		    signal(`Done(Some b'))
		| None ->
		    dlogf
		      "request_buffer_e inode=%Ld index=%Ld: \
                       buffer already claimed, or error situation"
		      inode index;
		    signal(`Done None) in
	    let l =
	      try Hashtbl.find sys.notify (inode,index) 
	      with Not_found -> [] in
	    Hashtbl.replace sys.notify (inode,index) (l @ [on_buffer]);
	    if l = [] then Queue.add (inode,index) sys.waiting;
	    (* Check whether another buffer should be flushed: *)
	    (* maybe_flush_buffer c; *)
	    (* Wait until we are notified: *)
	    e
    )

let select_for_flush sys inode =
  let fr = Hashtbl.find sys.flushreqs inode in
  Hashtbl.remove sys.flushreqs inode;
  I64Set.iter
    (fun index ->
       try
	 let b = Hashtbl.find sys.buffers (inode,index) in
	 b.buf_flushing <- true
       with
	 | Not_found -> assert false
    )
    fr.flush_index_list;
  fr


let select_inodes sys =
  (* There is no order right now. We could use the information available
     in dbuffers, though
   *)
  Hashtbl.fold
    (fun inode _ acc -> inode::acc)
    sys.flushreqs
    []


let write_committed sys b =
  if b.buf_state <> `Written then
    invalid_arg "Plasma_filebuf.write_committed: bad state";

  b.buf_flushing <- false;
  if b.buf_dirty then (
    b.buf_state <- `Invalid; (* ... so that the following is allowed *)
    dirty_access sys b 0L
  )
  else (
    b.buf_state <- `Clean;
    b.buf_dirty <- false;
    BLRU.add b sys.cbuffers;
    drop_if_needed sys b
  )


let write_cancelled sys b =
  if b.buf_state <> `Written then
    invalid_arg "Plasma_filebuf.write_cancelled: bad state";

  b.buf_flushing <- false;
  b.buf_state <- `Invalid; (* ... so that the following is allowed *)
  dirty_access sys b 0L


let write_erroneous sys inode errno =
  if not (Hashtbl.mem sys.errors inode) then (
    Hashtbl.replace sys.errors inode errno;
    notify_after_error sys inode
  )


let release_flush_request sys fr =
  I64Set.iter
    (fun index ->
       let b = 
	 try lookup_buffer sys fr.flush_inode index 
	 with Not_found -> assert false in
       if b.buf_state = `Written then
	 write_cancelled sys b
       else
	 b.buf_flushing <- false
    )
    fr.flush_index_list


let inode_error sys inode =
  try Some(Hashtbl.find sys.errors inode) with Not_found -> None


let reset_inode_error sys inode =
  Hashtbl.remove sys.errors inode


let n_waiting sys =
  Queue.length sys.waiting


let string_of_state =
  function
    | `Invalid -> "invalid"
    | `Clean -> "clean"
    | `Dirty -> "dirty"
    | `Reading _ -> "reading"
    | `Writing _ -> "writing"
    | `Written -> "written"
    | `Dropped -> "dropped"


let dump_buffers sys =
  dlogf "START BUFFER DUMP";
  Hashtbl.iter
    (fun (inode,index) b ->
       let s = string_of_state b.buf_state in
       let flags =
	 (if b.buf_dirty then "Y" else "_") ^ 
	 (if b.buf_delayed_drop then "D" else "_") ^
	 (if b.buf_flushing then "F" else "_") in
       dlogf
	 "buffer inode=%Ld index=%Ld ord=%d state=%s seqno=%Ld flags=%s"
	 inode index b.buf_ord s b.buf_seqno flags
    )
    sys.buffers;
  Hashtbl.iter
    (fun inode fr ->
       let s =
	 String.concat ","
	   (I64Set.fold
	      (fun i acc -> Int64.to_string i :: acc) 
	      fr.flush_index_list 
	      []) in
       dlogf
	 "fr inode=%Ld min_eof=%Ld indexes=%s"
	 inode fr.flush_min_eof s
    )
    sys.flushreqs;
  dlogf "END BUFFER DUMP"
