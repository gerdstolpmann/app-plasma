(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* TODO Improve:
    - config for ic_size (and maybe other params like n_fill and n_evict)
    - n_fill could be a value that is changed dynamically. If the file
      is rarely modified, n_fill should be big. If it is frequently modified
      small n_fill is better.
    - check_up_to_date: Maybe this optimization helps sometimes: When
      a number of blocks are read from the same inode, and this happens
      concurrently, the up-to-date checks could be merged. This might be
      difficult to organize, though - if more up-to-date checks are expected
      already requested checks should be minimally delayed to increase the
      chance that such a merge can happen. So the check_up_to_date
      function should have another parameter [p] which is the estimate of
      soon coming calls. Only if this parameter is > 0 the delay happens.
      There is an upper bound for the delay which is a function of [f]
      (experiments are needed to get the exact formula).

 *)

open Plasma_rpcapi_aux
open Plasma_util.Operators
open Printf

let dlogf = Plasma_util.dlogf

module Fsys = Plasma_rpcapi_clnt.Make'Filesystem(Rpc_proxy.ManagedClient)

(** [ic] contains inode information for a cached inode *)
type ic =
    { cache_inode : int64;
      mutable cache_val_ts : float; (* when cache was last validated *)
      mutable cache_tmo_ts : float; (* when cache must time out *)
      mutable cache_ii : inodeinfo;
    }


module IC = struct
  type t = int64   (* inode number *)
  let compare = Int64.compare
end

module ICLRU = Plasma_util.LRU(IC)

type ic_manager =
    { mutable cacheclient : Rpc_proxy.ManagedClient.t option;
      (** If none, there is no inodecache functionality. 
	  Otherwise this is the RPC client for is_up_to_date *)

      inodecache : (int64, ic) Hashtbl.t;
      (** Cache per inode *)

      inodecache_lru : ICLRU.t;
      (** For determining the oldest cache member *)

      inodecache_refresh_e : (int64, rinodeinfo Uq_engines.engine) Hashtbl.t;
      (** For each inode this is an engine loading a fresh [ii]. Avoids
	  that several queries are emitted for the same inode at the same time
       *)

      inodecache_getting_blocks : 
	(int64, (int64 * int * unit Uq_engines.engine)) Hashtbl.t;
      (** Whether there is a get_blocks query for this inode *)

      mutable ic_size : int;
      (** max number of cached inodes *)

      esys : Unixqueue.event_system
    }


(* NB. lazy_transactions must not be cached here *)
class type lazy_transaction = object
  method serialized : (unit->unit Uq_engines.engine) -> unit Uq_engines.engine
  method get_tid_e : unit -> int64 Uq_engines.engine
  method get_ii_e : int64 -> bool -> inodeinfo Uq_engines.engine
  method invalidate_ii : int64 -> unit
  method release_e : unit -> unit Uq_engines.engine
  method snapshot : bool
  method event_system : Unixqueue.event_system
end


let expand_blockinfo bi =
  let rec loop k =
    if k < bi.length then
      { bi with
	  index = Int64.add bi.index k;
	  block = Int64.add bi.block k;
	  length = 1L
      } :: loop (Int64.succ k)
    else
      []
  in
  loop 0L


let expand_blocklist bi_list =
  List.flatten
    (List.map expand_blockinfo bi_list)


let create_manager esys =
  { cacheclient = None;
    inodecache = Hashtbl.create 128;
    inodecache_lru = ICLRU.create();
    inodecache_getting_blocks = Hashtbl.create 7;
    inodecache_refresh_e = Hashtbl.create 7;
    ic_size = 1024;
    esys = esys;
  }


let enable c mc =
  c.cacheclient <- Some mc

let disable c =
  c.cacheclient <- None 


(** The inodecache stores the inodeinfo record, plus a subset of the
    blockinfo records. Every access to the inodecache is validated -
    it is checked whether inodeinfo is the committed version. If so,
    the cached values can be taken.

    This assumes that the validating is_up_to_date call is very cheap.
    This is achieved by separating the inodecache server into its own
    process, and avoiding db lookups most of the time.

    There is also a cache for blocklists. We must not cache these for 
    too long, however, because these lists include the [node_alive]
    flag.
 *)

let ic_evict c =
  (** Limit to ic_size entries *)
  while Hashtbl.length c.inodecache > c.ic_size do
    let oldest = ICLRU.take c.inodecache_lru in
    Hashtbl.remove c.inodecache oldest
  done;
  ()


let ic_update c inode ii =
  (** Update the cache with ii if possible *)
  dlogf "ic_update: inode=%Ld ii.seqno=%Ld ii.committed=%B"
    inode ii.seqno ii.committed;
  if ii.committed && c.cacheclient <> None then (
    let old_seqno, exists =
      try (Hashtbl.find c.inodecache inode).cache_ii.seqno, true
      with Not_found -> (-1L), false in
    if ii.seqno > old_seqno then (
      (* There is a newer version. Replace the old one. *)
      let now = Unix.gettimeofday() in
      let ce =
	{ cache_inode = inode;
	  cache_val_ts = now;
	  cache_tmo_ts = now +. 60.0;     (* FIXME *)
	  cache_ii = ii;
	} in
      Hashtbl.replace c.inodecache inode ce;
      ICLRU.add inode c.inodecache_lru;
      dlogf "ic_update: inode=%Ld updating cache" inode;
      if not exists then
	ic_evict c
    )
    else
      if exists then (
	(* The version in the cache is already as good or newer. If it is
	   as good, we can at least update the validation timestamp.
	 *)
	if ii.seqno = old_seqno then (
	  let now = Unix.gettimeofday() in
	  let old_ce = Hashtbl.find c.inodecache inode in
	  old_ce.cache_val_ts <- now;
	  old_ce.cache_tmo_ts <- now +. 60.0;     (* FIXME *)
	  dlogf "ic_update: inode=%Ld confirming cache" inode;
	);
	(* Anyway, update the position in the LRU list. *)
	ICLRU.add inode c.inodecache_lru  (* "touch" *)
      )
  )


let ic_touch c inode =
  (** "touches" the cache entry so that it becomes the most recently
      accessed entry
   *)
  if ICLRU.mem inode c.inodecache_lru then
    ICLRU.add inode c.inodecache_lru;
  ()


let get_ii_e c mc tid inode =
  (** Get inodeinfo directly from namenode *)
  dlogf "get_ii_e(%Ld)" inode;
  Plasma_util.rpc_engine mc Fsys.V1.get_inodeinfo'async (tid,inode)
  ++ (fun r ->
	( match r with
	    | `ok ii -> ic_update c inode ii
	    | _ -> ()
	);
	eps_e (`Done r) c.esys
     )

let get_ii_serialized_e c mc tid inode fresh =
  (** Same, but serialized, so that usually at most one [get_ii_e] is active
      at a time for the same inode. By setting [fresh] one can enforce that
      a new [get_ii_e] is started (possibly in parallel to an already running
      one)
   *)
  try
    if fresh then raise Not_found;
    let e = Hashtbl.find c.inodecache_refresh_e inode in
    dlogf "get_ii_serialized_e(%Ld): already in progress" inode;
    e
  with
    | Not_found ->
	dlogf "get_ii_serialized_e(%Ld): starting" inode;
	let e = get_ii_e c mc tid inode in
	Hashtbl.replace c.inodecache_refresh_e inode e;
	e 
	>> (fun st ->
	      Hashtbl.remove c.inodecache_refresh_e inode;
	      st
	   )


let invalidate_ii c inode =
  (* invalidates the current version in the cache, but does not prevent
     the ongoing refresh
   *)
  Hashtbl.remove c.inodecache inode;
  ICLRU.remove inode c.inodecache_lru


let get_ii_from_cache_e c inode =
  (** Returns inodeinfo from cache (after validation) *)
  try
    let ce = Hashtbl.find c.inodecache inode in
    ( match c.cacheclient with
	| None ->
	    raise Not_found
	| Some ic ->
	    (** we do not validate each time, but based on a time criterion
	     *)
	    let now = Unix.gettimeofday() in
	    let way_too_old = now >= ce.cache_tmo_ts in
	       (* after a certain timeout we never take the cache *)
	    let gratuitously_valid = now < ce.cache_val_ts +. 1.0 in
	       (* shortly after a validation we believe the cache *)
	    ( if gratuitously_valid then
		(eps_e (`Done true) c.esys)
	      else 
		if way_too_old then
		  (eps_e (`Done false) c.esys)
		else (
		  dlogf
		    "get_ii_from_cache_e: validating inode=%Ld" inode;
		  Plasma_util.rpc_engine
		    ic Fsys.V1.is_up_to_date'async (inode,ce.cache_ii)
		)
	    )
	    ++ (fun flag ->
		  dlogf "get_ii_from_cache_e(%Ld): valid=%B"
		      inode flag;
		  if flag then (
		    ic_touch c inode;
		    if not gratuitously_valid then
		      ce.cache_val_ts <- now;
		    eps_e (`Done (Some ce.cache_ii)) c.esys
		  )
		  else(
		    invalidate_ii c inode;
		    eps_e (`Done None) c.esys
		  )
	       )
    )
  with
    | Not_found ->
	dlogf "get_ii_from_cache_e(%Ld): not found" inode;
	eps_e (`Done None) c.esys


let get_ii_via_cache_e c mc lt inode =
  (** Returns inodeinfo from cache (after validation), or loads it *)
  get_ii_from_cache_e c inode
  ++ (function
	| Some ii ->
	    eps_e (`Done(`ok ii)) c.esys
	| None ->
	    lt # get_tid_e() ++
	      (fun tid ->
		 get_ii_serialized_e c mc tid inode false)
     )


let check_up_to_date_e c mc lt inode seqno =
  (** Returns whether this seqno is the seqno of the inode *)
  ( match c.cacheclient with
      | None -> 
	  dlogf "check_up_to_date_e(%Ld): inactive" inode;
	  eps_e (`Done false) c.esys
      | Some ic ->
	  dlogf "check_up_to_date_e(%Ld): starting" inode;
	  Plasma_util.rpc_engine 
	    ic Fsys.V1.is_up_to_date_seqno'async (inode,seqno)
  ) ++ (fun flag ->
	  match flag with
	    | true ->
		dlogf "check_up_to_date_e(%Ld): true" inode;
		eps_e (`Done true) c.esys
	    | false ->
		lt # get_tid_e() ++
		  (fun tid ->
		     get_ii_serialized_e c mc tid inode true) ++
		  (fun r ->
		     let flag =
		       match r with 
			 | `ok ii -> ii.seqno = seqno
			 | _ -> false in
		     dlogf "check_up_to_date_e(%Ld): %B" 
		       inode flag;
		     eps_e (`Done flag) c.esys
		  )
       )


module StrSet = Plasma_util.StrSet
module StrMap = Plasma_util.StrMap
module Int64Map = Map.Make(Int64)


(* We need the following module for the compressed representation
   of the blocklist cache. It is identical with Nn_blocklist.Blockinfo.
 *)

module Blockinfo = struct
  type key = int64
  type t = blockinfo

  let split split_idx bi =
    let idx_end = Int64.add bi.index bi.length in
    if split_idx <= bi.index || split_idx >= idx_end then
      failwith "Nn_blocklist.Blockinfo.split: cannot split";
    let left_lenL = Int64.sub split_idx bi.index in
    let left =
      { bi with
          length = left_lenL
      } in
    let right_lenL = Int64.sub idx_end split_idx in
    let right =
      { bi with
          index = split_idx;
          block = Int64.add bi.block left_lenL;
          length = right_lenL
      } in
    (left, right)

  let merge b1 b2 =
    try
      let idx_end1 = Int64.add b1.index b1.length in
      if idx_end1 < b1.index then raise Not_found;  (* overflow *)
      let idx_end2 = Int64.add b2.index b2.length in
      if idx_end2 < b2.index then raise Not_found;  (* overflow *)
      let block_end1 = Int64.add b1.block b1.length in
      if block_end1 < b1.block then raise Not_found;  (* overflow *)
      let block_end2 = Int64.add b2.block b2.length in
      if block_end2 < b2.block then raise Not_found;  (* overflow *)
      let compat =
        b1.identity = b2.identity &&
        b1.ticket = b2.ticket &&
        b1.inode_seqno = b2.inode_seqno &&
        b1.inode_committed = b2.inode_committed in
      let left_compat =
        compat && idx_end1 = b2.index && block_end1 = b2.block in
      let right_compat =
        compat && idx_end2 = b1.index && block_end2 = b1.block in
      if left_compat then
        Some { b1 with length = Int64.add b1.length b2.length }
      else
        if right_compat then
          Some { b2 with length = Int64.add b1.length b2.length }
        else
          raise Not_found
    with
      | Not_found -> None
end


module Identities = struct
  type key = int64
  type t = int64 * int64 * StrSet.t (* from, to, identities *)

      (* Note that we cannot merge/split the identity set.
	 Because of this, this module works only if the set
	 of identities is already complete when something is
	 added to the range for a certain block
       *)

  let merge (min1,max1,iset1) (min2,max2,iset2) =
    let eq_lz = lazy(StrSet.equal iset1 iset2) in
    if Int64.succ max1 = min2 && Lazy.force eq_lz then
      Some(min1, max2, iset1)
    else if Int64.succ max2 = min1 && Lazy.force eq_lz then
      Some(min2, max1, iset1)
    else 
      None
  let split p (min,max,iset) =
    if p <= min || p > max then 
      failwith "Plasma_util.I64RangeAtom: cannot split";
    (min, Int64.pred p, iset), (p, max, iset)
end


module BlockRM = Plasma_util.RangeMap(Plasma_util.I64)(Blockinfo)
module BlockDom = Plasma_util.RangeMap(Plasma_util.I64)(Identities)


type compressed_blocklist = BlockRM.t StrMap.t
    (* maps identity to range map *)

let compress l =
  List.fold_left
    (fun acc bi ->
       assert(bi.length >= 1L);
       let idx_from = bi.index in
       let idx_to = Int64.pred (Int64.add idx_from bi.length) in
       let brm =
         try StrMap.find bi.identity acc
         with Not_found -> BlockRM.empty in
       let brm' =
         BlockRM.add (idx_from,idx_to) bi brm in
       StrMap.add bi.identity brm' acc
    )
    StrMap.empty
    l


type bl_cache =
    { blc_mc : Rpc_proxy.ManagedClient.mclient;
      blc_lt : lazy_transaction;
      blc_inode : int64;
      mutable blc_blocks : compressed_blocklist;
	(** [blc_blocks] is an incomplete mapping of indexes to
	    blockinfos
	 *)
      mutable blc_domain : BlockDom.t;
        (** for which block ranges we have information in the cache.
	    The values include the set of identities
	 *)
      mutable blc_getting : (int64 * int64 * unit Uq_engines.engine) option;
	(** Whether there is a running get_blocks query *)
      mutable blc_snapshot : [`None | `Full | `Append of int64];
    }


let create_bl_cache mc lt inode =
  { blc_mc = mc;
    blc_lt = lt;
    blc_inode = inode;
    blc_blocks = StrMap.empty;
    blc_domain = BlockDom.empty;
    blc_getting = None;
    blc_snapshot = `None
  }


let forget_blocks blc index number =
  let last = Int64.add index (Int64.pred number) in
  blc.blc_domain <- BlockDom.remove (index,last) blc.blc_domain;
  blc.blc_blocks <- (StrMap.map 
		       (fun r ->
			  BlockRM.remove (index,last) r
		       )
		       blc.blc_blocks
		    )

let notify_got_eio blc =
  (* We just drop the blocks. The blocks will then be reloaded from the server
     with updated healthiness bit. This works even for snapshots, because the
     server will always return the same blocks in a transaction.
   *)
  blc.blc_blocks <- StrMap.empty;
  blc.blc_domain <- BlockDom.empty


let into_blocklist_cache blc index number bl =
  (* bl must be expanded!!! *)
  (** Put this result into the cache *)
  let identities_by_block =
    (* For each block, return the set of identities *)
    List.fold_left
      (fun acc block ->
	 assert(block.length = 1L);
	 let iset =
	   try Int64Map.find block.index acc
	   with Not_found -> StrSet.empty in
	 Int64Map.add block.index (StrSet.add block.identity iset) acc
      )
      Int64Map.empty
      bl in
  blc.blc_domain <- (Int64Map.fold
		       (fun i iset acc ->
			  BlockDom.add (i,i) (i,i,iset) acc
		       )
		       identities_by_block
		       blc.blc_domain);
  blc.blc_blocks <- (List.fold_left
		       (fun acc block ->
			  let i = block.index in
			  let r1 =
			    try StrMap.find block.identity acc
			    with Not_found -> BlockRM.empty in
			  let r2 =
			    BlockRM.add (i,i) block r1 in
			  StrMap.add block.identity r2 acc
		       )
		       blc.blc_blocks
		       bl);
  dlogf
    "get_blocks_e: inode=%Ld caching" blc.blc_inode
	  

let override_blocks blc index number bl =
  into_blocklist_cache blc index number (expand_blocklist bl)


let get_blocks_e blc index numberL req_seqno =
  let esys = blc.blc_lt # event_system in
  dlogf
    "get_blocks_e: inode=%Ld index=%Ld number=%Ld" blc.blc_inode index numberL;
  blc.blc_lt # get_tid_e()
  ++ (fun tid ->
	Plasma_util.rpc_engine
	  blc.blc_mc Fsys.V1.get_blocks'async
	  (tid,blc.blc_inode,index,numberL,req_seqno,true)
	++ (fun r ->
	      ( match r with
		  | `ok bl ->
		      let bl = expand_blocklist (Array.to_list bl) in
		      let r = `ok (Array.of_list bl) in
		      dlogf
			"get_blocks_e: inode=%Ld got %d blocks" 
			blc.blc_inode (List.length bl);
		      into_blocklist_cache blc index numberL bl;
		      eps_e (`Done r) esys
		  | _ ->
		      eps_e (`Done r) esys
	      )
	   )
     )


let get_blocks_sticky_e blc index number =
  (* remember the get_blocks request *)
  let esys = blc.blc_lt # event_system in
  let is_sticky = blc.blc_getting = None in
  if is_sticky then (
    let e1, signal = Uq_engines.signal_engine esys in
    blc.blc_getting <- Some (index,number,e1);
    get_blocks_e blc index number 0L
    >> (fun st ->
	  blc.blc_getting <- None;
	  signal (`Done ());
	  st
       )
  )
  else
    get_blocks_e blc index number 0L
	  

let n_fill = 256

let get_blocks_and_fill_e blc index =
  (** Fill the cache with some blocks, but return only [index].

      Usually, clients iterate over the blocks in either ascending or
      descending order. We try to adapt to this:
       - if [index-1] is in the cache, but [index] is missing: conclude
         ascending order, and fill the range [index..index+n] for some [n]
       - if [index+1] is in the cache, but [index] is missing: conclude
         descending order, and fill the range [index-n..index] for some [n]
       - if both [index-1] and [index+1] are in the cache, or both
         are missing, or [index] is already loaded: assume unspecific
         order, and fill the range [index-n/2..index+n/2]

      We also remove entries from the cache: All blocks farer away from index
      than n_evict are deleted (in get_block_e).
   *)
  let esys = blc.blc_lt # event_system in
  let in_cache i =
    try 
      let (_, _) =
	BlockDom.find index blc.blc_domain in
      true
    with Not_found -> false in
  let index_in_cache = in_cache index in
  let pred_in_cache = in_cache (Int64.pred index) in
  let succ_in_cache = in_cache (Int64.succ index) in
  let ascending = pred_in_cache && not succ_in_cache && not index_in_cache in
  let descending = not pred_in_cache && succ_in_cache && not index_in_cache in
  (* let other = not ascending && not descending in *)

  let n_fillL = Int64.of_int n_fill in
  let min_index, n =
    if ascending then 
      index, n_fillL
    else if descending then
      max 0L (Int64.succ(Int64.sub index n_fillL)), n_fillL
    else
      max 0L (Int64.succ(Int64.sub index (Int64.div n_fillL 2L))), n_fillL in

  get_blocks_sticky_e blc min_index n
  ++ (fun r ->
	match r with
	  | `ok bl ->
	      (** Extract only the requested index: *)
	      let bl' =
		Array.of_list
		  (List.fold_left
		     (fun acc block ->
			if block.index = index then
			  block :: acc
			else
			  acc
		     )
		     []
		     (Array.to_list bl)
		  ) in
	      eps_e (`Done (`ok bl')) esys
	  | #Plasma_util.errno as errno ->
	      eps_e (`Done errno) esys
     )

let get_blocks_from_cache_e blc index =
  let esys = blc.blc_lt # event_system in
  try
    let (_, (_,_,identities)) =
      BlockDom.find index blc.blc_domain in
    let l =
      StrSet.fold
	(fun id acc ->
	   let r =
	     try StrMap.find id blc.blc_blocks
	     with Not_found -> BlockRM.empty in
	   try
	     let (_, bi1) = BlockRM.find index r in
	     let d = Int64.sub index bi1.index in
	     let bi2 =
	       { bi1 with 
		   index = index; 
		   block = Int64.add bi1.block d; 
		   length = 1L
	       } in
	     bi2 :: acc
	   with
	     | Not_found -> acc
	)
	identities
	[] in
    if l = [] then raise Not_found;
    eps_e (`Done (Some (Array.of_list l))) esys
  with
    | Not_found ->
	eps_e (`Done None) esys


let rec get_blocks_via_cache_e blc index =
  let esys = blc.blc_lt # event_system in
  get_blocks_from_cache_e blc index
  ++ (fun blocks_opt ->
	match blocks_opt with
	  | Some blocks ->
	      dlogf
		"get_blocks_via_cache inode=%Ld index=%Ld: from cache"
		blc.blc_inode index;
	      eps_e (`Done (`ok blocks)) esys
	  | _ ->
	      let force_no_results =
		match blc.blc_snapshot with
		  | `Full -> true
		  | `Append limit -> index >= limit
		  | `None -> false in
	      if force_no_results then
		eps_e (`Done (`ok [| |] )) esys
	      else
                (* Get the data from the server. There might already be
                   a running get_blocks query
                 *)
                let reaction =
		  match blc.blc_getting with
		    | Some (i,l,e) ->
			if index >= i && Int64.sub index i < l then
			  `Wait_for_request e
			else
			  `Request_single_block
		    | None ->
			`Request_several_blocks in
                match reaction with
                  | `Request_single_block ->
                      dlogf
                        "get_blocks_via_cache inode=%Ld index=%Ld: single block"
                        blc.blc_inode index;
                      get_blocks_e blc index 1L 0L
                  | `Request_several_blocks ->
                      dlogf
                        "get_blocks_via_cache inode=%Ld index=%Ld: \
                         several blocks"
                        blc.blc_inode index;
                      get_blocks_and_fill_e blc index
                  | `Wait_for_request e ->
                      (* Wait until the current request is done, and
                         then check the cache again.
                       *)
                      dlogf
                        "get_blocks_from_cache_e inode=%Ld index=%Ld: waiting"
                        blc.blc_inode index;
                      e
                      ++ (fun () ->
                            get_blocks_via_cache_e blc index
                         )
     )


let snapshot_blocks_e ?(append=false) blc ii =
  if blc.blc_snapshot <> `None then
    failwith "Plasma_inodecache.snapshot_blocks_e: snapshot already active";
  blc.blc_snapshot <- if append then `Append ii.blocklimit else `Full;
  let esys = blc.blc_lt # event_system in
  let blkidx, n =
    if append then
      (max (Int64.pred ii.blocklimit) 0L, 1L)
    else
      0L, ii.blocklimit in
  get_blocks_e blc blkidx n ii.seqno
  ++ (function
	| `ok _ ->
	    eps_e (`Done `ok) esys
	| #Plasma_util.errno as e ->
	    eps_e (`Done e) esys
     )	    
