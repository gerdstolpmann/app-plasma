(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

module StrMap = Plasma_util.StrMap

type brange =
    { br_index : int64;
      br_length : int64;
      br_datanodes : int64 StrMap.t;
    }


module Brange = struct
  (* very similar to Nn_blocklist.Blockinfo but not identical *)
  type key = int64
  type t = brange

  let split split_idx bi =
    let idx_end = Int64.add bi.br_index bi.br_length in
    if split_idx <= bi.br_index || split_idx >= idx_end then
      failwith "BI.split: cannot split";
    let left_lenL = Int64.sub split_idx bi.br_index in
    let left =
      { bi with
	  br_length = left_lenL
      } in
    let right_lenL = Int64.sub idx_end split_idx in
    let right_dn =
      StrMap.map (fun block -> Int64.add block left_lenL) bi.br_datanodes in
    let right =
      { br_index = split_idx;
	br_datanodes = right_dn;
	br_length = right_lenL
      } in
    (left, right)

  let merge b1 b2 =
    try
      let idx_end1 = Int64.add b1.br_index b1.br_length in
      if idx_end1 < b1.br_index then raise Not_found;  (* overflow *)
      let idx_end2 = Int64.add b2.br_index b2.br_length in
      if idx_end2 < b2.br_index then raise Not_found;  (* overflow *)
      let compat =
	List.map fst (StrMap.bindings b1.br_datanodes) = 
	List.map fst (StrMap.bindings b2.br_datanodes) in
      let left_compat =
	compat && idx_end1 = b2.br_index && 
	(StrMap.for_all
	   (fun id block ->
	      let blockend = Int64.add block b1.br_length in
	      if blockend < block then raise Not_found;  (* overflow *)
	      let b2_block = StrMap.find id b2.br_datanodes in
	      blockend = b2_block
	   )
	   b1.br_datanodes
	) in
      let right_compat =
	compat && idx_end2 = b1.br_index && 
	(StrMap.for_all
	   (fun id block ->
	      let blockend = Int64.add block b2.br_length in
	      if blockend < block then raise Not_found;  (* overflow *)
	      let b1_block = StrMap.find id b1.br_datanodes in
	      blockend = b1_block
	   )
	   b2.br_datanodes
	) in
      if left_compat then
	Some { b1 with br_length = Int64.add b1.br_length b2.br_length }
      else
	if right_compat then
	  Some { b2 with br_length = Int64.add b1.br_length b2.br_length }
	else
	  raise Not_found
    with
      | Not_found -> None

end


module Bset = Plasma_util.RangeMap(Plasma_util.I64)(Brange)

module I64RM = Plasma_util.I64RM


let bset_of_blocklist list =
  let open Plasma_rpcapi_aux in
  let set = ref Bset.empty in
  List.iter
    (fun b ->
       let index_max = Int64.pred (Int64.add b.index b.length) in

       (* Get the part of rm that overlaps with b *)
       let olap = Bset.sub (b.index,index_max) !set in

       (* Add the overlapping part. Merge datanodes *)
       Bset.iter
	 (fun (idxmin,idxmax) bi ->
	    let offs = Int64.sub idxmin b.index in
	    let block = Int64.add b.block offs in
	    let bi' =
	      { bi with
		  br_datanodes = 
		    StrMap.add b.identity block bi.br_datanodes } in
	    set := Bset.add (idxmin,idxmax) bi' !set
	 )
	 olap;
       
       (* Get the part of set that does not overlap with b *)
       let b_range = 
	 I64RM.add (b.index,index_max) (b.index,index_max) I64RM.empty in
       let non_olap_range =
	 Bset.fold
	   (fun key _ acc ->
	      I64RM.remove key acc
	   )
	   olap
	   b_range in
       
       (* Add the non-overlapping part of b *)
       I64RM.iter
	 (fun (idxmin,idxmax) _ ->
	    let offs = Int64.sub idxmin b.index in
	    let len = Int64.succ (Int64.sub idxmax idxmin) in
	    let block = Int64.add b.block offs in
	    let bi =
	      { br_datanodes = StrMap.add b.identity block StrMap.empty;
		br_index = idxmin;
		br_length = len;
	      } in
	    set := Bset.add (idxmin,idxmax) bi !set
	 )
	 non_olap_range
    )
    list;
  !set
