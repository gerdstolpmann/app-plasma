(* $Id$ *)

(* Functions for converting the output of MacOSX
   - dscl -url . -readall /Users, and
   - dscl -url . -readall /Groups
   to the /etc/passwd and /etc/group formats, resp
 *)

open Printf

type passwd_entry =
  { mutable uid : int option;
    mutable name : string option;
    mutable real_name : string option;
    mutable gid : int option;
    mutable home : string option;
    mutable shell : string option
  }

let empty_entry =
  { uid = None;
    name = None;
    real_name = None;
    gid = None;
    home = None;
    shell = None
  }

type group_entry =
  { mutable g_id : int option;
    mutable g_name : string option;
    mutable g_members : string list
  }

let empty_group =
  { g_id = None;
    g_name = None;
    g_members = []
  }

let re = Netstring_str.regexp "[ \t\r\n]+"

let decode s =
  let words = Netstring_str.split re s in
  List.map
    (fun w ->
      try Netencoding.Url.decode ~plus:false w
      with _ -> w
    )
    words

let to_passwd (ch : Netchannels.in_obj_channel) =
  let entry = ref empty_entry in
  let cur_att = ref None in
  let out = ref [] in

  let process_cur_att() =
    match !cur_att with
      | None -> ()
      | Some(name,value) ->
        let values = decode value in
        ( try
            ( match name with
              | "UniqueID" ->
                  !entry.uid <- Some (int_of_string (List.hd values))
              | "RecordName" ->
                  !entry.name <- Some (List.hd values)
              | "RealName" ->
                  !entry.real_name <- Some (String.concat " " values)
              | "PrimaryGroupID" ->
                  !entry.gid <- Some (int_of_string (List.hd values))
              | "NFSHomeDirectory" ->
                  !entry.home <- Some (List.hd values)
              | "UserShell" ->
                  !entry.shell <- Some (List.hd values)
              | _ ->
                  ()
            );
            cur_att := None
          with _ ->
            prerr_endline ("Cannot parse attribute: " ^ name)
        ) in

  let output_entry() =
    let get =
      function
        | None -> raise Not_found
        | Some x -> x in
    try
      let uid = get !entry.uid in
      let gid = get !entry.gid in
      let name = get !entry.name in
      let real_name = get !entry.real_name in
      let home = get !entry.home in
      let shell = get !entry.shell in
      let passwd_line =
        sprintf "%s:*:%d:%d:%s:%s:%s" 
          name uid gid real_name home shell in
      out := passwd_line :: !out
    with Not_found -> () (* incomplete *) in

  try
    while true do
      let line = ch # input_line () in
      if line = "" then ()
      else if line = "-" then (     (* end of user entry *)
        process_cur_att();
        output_entry();
        entry := empty_entry
      )
      else if line.[0] = ' ' then (    (* continuation line *)
        match !cur_att with
          | None -> ()
          | Some(name,value) ->
              cur_att := Some(name,value ^ line)
      )
      else (
        try
          let colon = String.index line ':' in
          process_cur_att();
          let name = String.sub line 0 colon in
          let value =
            String.sub line (colon+1) (String.length line - colon - 1) in
          cur_att := Some(name,value)
        with 
          | Not_found -> ()
      )
    done;
    assert false
  with
    | End_of_file ->
        process_cur_att();
        output_entry();
        String.concat "\n" (List.rev !out)


let to_group (ch : Netchannels.in_obj_channel) =
  let entry = ref empty_group in
  let cur_att = ref None in
  let out = ref [] in

  let process_cur_att() =
    match !cur_att with
      | None -> ()
      | Some(name,value) ->
        let values = decode value in
        ( try
            ( match name with
              | "PrimaryGroupID" ->
                  !entry.g_id <- Some (int_of_string (List.hd values))
              | "RecordName" ->
                  !entry.g_name <- Some (List.hd values)
              | "GroupMembership" ->
                  !entry.g_members <- values
              | _ ->
                  ()
            );
            cur_att := None
          with _ ->
            prerr_endline ("Cannot parse attribute: " ^ name)
        ) in

  let output_entry() =
    let get =
      function
        | None -> raise Not_found
        | Some x -> x in
    try
      let gid = get !entry.g_id in
      let name = get !entry.g_name in
      let group_line =
        sprintf "%s:*:%d:%s" 
          name gid (String.concat "," !entry.g_members) in
      out := group_line :: !out
    with Not_found -> () (* incomplete *) in

  try
    while true do
      let line = ch # input_line () in
      if line = "" then ()
      else if line = "-" then (     (* end of user entry *)
        process_cur_att();
        output_entry();
        entry := empty_group
      )
      else if line.[0] = ' ' then (    (* continuation line *)
        match !cur_att with
          | None -> ()
          | Some(name,value) ->
              cur_att := Some(name,value ^ line)
      )
      else (
        try
          let colon = String.index line ':' in
          process_cur_att();
          let name = String.sub line 0 colon in
          let value =
            String.sub line (colon+1) (String.length line - colon - 1) in
          cur_att := Some(name,value)
        with 
          | Not_found -> ()
      )
    done;
    assert false
  with
    | End_of_file ->
        process_cur_att();
        output_entry();
        String.concat "\n" (List.rev !out)
