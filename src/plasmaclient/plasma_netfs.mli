(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Accessing PlasmaFS via the [Netfs.stream_fs] interface *)

class netfs : ?repl:int -> ?verbose:bool -> ?notranslate:bool ->
              Plasma_client.plasma_cluster -> Netfs.stream_fs
val netfs : ?repl:int -> ?verbose:bool ->  ?notranslate:bool ->
            Plasma_client.plasma_cluster -> Netfs.stream_fs
  (** Access this cluster with the [stream_fs] interface:

      - [read] and [write] are fully implemented, and always use
        streaming mode. The I/O bases on the buffered access layer
        provided by {!Plasma_client}, and so it is a good idea to
        configure some number of buffers to use, with
        {!Plasma_client.configure_buffer}.

      Arguments:

      - [repl]: The replication factor for new files. Defaults to 0, i.e.
        to the server default.
      - [verbose]: Whether to output progress messages to stderr for
        all metadata modifications.
        Defaults to [false]. (This exists for the command-line utility
        [plasma].)
      - [notranslate]: Normally, all [Plasma_error] exceptions are
        translated to [Unix_error]. By setting this switch the translation
        is not done.
   *)


(** {2 Helper for {!Mapred_fs.plasma_filesystem}} *)

(** This is an extended version of [stream_fs]. It exists only as a
    definition step for {!Mapred_fs.plasma_filesystem}. Please
    don't use directly.
 *)

type x_read_file_flag =
    [ `Binary
    | `Temp of string * string | `Destination of string
    | `Dummy
    ]

type x_write_flag =
    [ `Create | `Exclusive | `Truncate | `Binary | `Streaming 
    | `Repl of int | `Location of string list
    | `Dummy
    ]

type x_write_file_flag =
    [ `Create | `Exclusive | `Truncate | `Binary | `Link
    | `Repl of int | `Location of string list
    | `Dummy
    ]

type x_copy_flag =
    [ `Repl of int | `Location of string list | `Dummy ]


type x_link_flag =
    [ `Dummy ]

class type x_stream_fs =
object
  inherit Netfs.stream_fs

  method x_read_file : x_read_file_flag list -> string -> Netfs.local_file

  method x_write : x_write_flag list -> string -> Netchannels.out_obj_channel
  method x_write_file : x_write_file_flag list -> string -> Netfs.local_file -> unit
  method x_copy : x_copy_flag list -> string -> string -> unit
  method x_link : x_link_flag list -> string -> string -> unit
  method x_copy_out_to_buf : string -> int64 -> Netsys_mem.memory -> int -> unit
  method x_copy_in_from_buf : string -> int64 -> Netsys_mem.memory -> int -> unit
  method x_blocksize : unit -> int
  method x_location_info : unit -> (Unix.inet_addr * string) list
  method x_blocklist : string -> Plasma_rpcapi_aux.blockinfo list
  method x_commit_files : (string * string) list -> unit
end


val x_netfs : ?repl:int -> ?verbose:bool ->  ?notranslate:bool ->
                Plasma_client.plasma_cluster -> x_stream_fs
class x_netfs : ?repl:int -> ?verbose:bool ->  ?notranslate:bool ->
                  Plasma_client.plasma_cluster -> x_stream_fs
