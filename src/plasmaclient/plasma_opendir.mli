(* $Id$ *)

(* Functions for converting the output of MacOSX
   - dscl -url . -readall /Users, and
   - dscl -url . -readall /Groups
   to the /etc/passwd and /etc/group formats, resp
 *)

val to_passwd : Netchannels.in_obj_channel -> string
val to_group : Netchannels.in_obj_channel -> string
