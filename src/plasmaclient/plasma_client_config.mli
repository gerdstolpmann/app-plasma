(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Configuration of clients *)

class type client_config =
object
  method clustername : string
    (** The clustername *)

  method nn_nodes : string list
    (** The name nodes in "host:port" syntax *)
end


val parse_config_file : string -> client_config list
  (** Parses the config file and returns all included configs *)

(** The config file should look like:

    {[
      plasmafs {
        cluster {
          clustername = "cluster1";
          node { addr = "hostname:port" };
          node { addr = "hostname:port" };
          ...
        }
        ... (* more [cluster] sections possible *)
      }
    ]}

    As usual, one can also have

    {[ 
       node_list = "filename";
       port = <default_port>;
    ]}

    instead of several [node]/[addr] sections. [node_list] points then
    to another file enumerating the hostnames (and optionally ports).
 *)

val get_config : ?clustername:string -> 
                 ?nn_nodes:string list ->
                 unit ->
                   client_config
  (** [get_config ()]: This is the standard way for
      clients to obtain a config object. Generally, the settings can
      be passed directly to [get_config], by environment variables,
      and by the config file [~/.plasmafs].

      - If both [clustername] and [nn_nodes] are passed, this configuration
        is taken
      - The file [~/.plasmafs] is parsed and all configurations are extracted.
      - If a [clustername] is passed, this configuration is selected (it is
        an error if this does not exist). Environment variables are ignored.
      - If no clustername is passed, and the environment variable
        [PLASMAFS_CLUSTER] is set, this variable selects the cluster.
        If also [PLASMAFS_NAMENODES] is set, the namenodes are configured
        from there. Otherwise the [PLASMAFS_CLUSTER] must be described
        in the configuration file.
      - Otherwise, the first configuration from [~/.plasmafs]
        is selected.
      - Otherwise the algorithm fails.

      The precedence in short: function arguments, then environment variable,
      then first entry from config file. Only if the cluster name is taken
      on a certain precdence level, the host/port list on this level is
      meaningful.

      [PLASMAFS_NAMENODES] enumerates the namenodes in [host:port] syntax,
      separated by commas.

   *)
