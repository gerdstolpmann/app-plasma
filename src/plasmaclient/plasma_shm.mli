(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Management of large buffers incl. shared memory *)

(** The idea of this module is to count the shared memory allocations,
    and to provide information whether shared memory is well available
    or tight

    This is now also used for large buffers that are not shared.
 *)

type id = [`Shm_id of int | `Buf_id of int ]

type kind = [`Shm | `Buf ]

type condition = [ `Normal | `Tight ]

type watermarks =
    { low : int64;
      high : int64;
      max : int64;
    }

exception Shm_max_exceeded
  (** The allocation would exceed the configured maximum for shm consumption.
      This exception does {b not} mean that the OS does not have any shm
      available (this is {!Plasma_util.Out_of_shared_memory}).
   *)

exception Buf_max_exceeded
  (** The allocation would exceed the configured maximum for large buffers *)


class type shm_manager =
object
  method new_id : kind -> id
    (** allocates a new ID for an shm or buf object *)

  method book_allocation : id -> int -> unit
    (** Books this ID with this (total) size, and pushes the new entry *)

  method delayed_allocation : id -> int -> unit
    (** A delayed allocation is not immediately pushed to the shared
	book. This is safe to be called from GC finalisers.
     *)

  method condition : kind -> condition
    (** Estimate where we are *)

  method push : unit -> unit
    (** Push allocations to the shared book. This also includes delayed
	allocations.
     *)

  method wm : kind -> watermarks
    (** Effective watermarks *)

  method dump : string
    (** Current sums for `Shm and `Buf as debug string *)

  method have_local_allocations : bool
    (** Whether this manager has allocations *)
end


class null_shm_manager : unit -> shm_manager
  (** This manager always reports the condition [`Normal] *)

class netplex_shm_manager : unit -> shm_manager
  (** This manager puts the shm accouning information into Netplex
      shared variables. This only works from Netplex components.
      Management must be initialized, see the module
      {!Plasma_shm.Netplex_shm_manager}.
   *)

module Netplex_shm_manager : sig
  (** Configuration and initialization of the Netplex shm manager. *)

  val post_add_hook : Netplex_types.controller -> unit
    (** This function must be called from the [post_add_hook] of the
	component
     *)

  val init : shm_wm:watermarks -> buf_wm:watermarks ->
               unit
    (** This can be called from component context to set the low-water
	and high-water marks. By default, these marks are [max_int],
	and [shm_condition] always returns [`Normal].

	When the sum of all allocated shared memory exceeds the high-water
	mark, the condition is switched to [`Tight]. It is switched back
	to [`Normal] when the sum sinks below the low-water mark.

	It is not allocated more than [max] shared memory.
     *)
end


