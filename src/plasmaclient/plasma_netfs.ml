(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf
open Uq_engines.Operators

type x_read_file_flag =
    [ `Binary
    | `Temp of string * string | `Destination of string
    | `Dummy
    ]

type x_write_flag =
    [ `Create | `Exclusive | `Truncate | `Binary | `Streaming 
    | `Repl of int | `Location of string list
    | `Dummy
    ]

type x_write_file_flag =
    [ `Create | `Exclusive | `Truncate | `Binary | `Link
    | `Repl of int | `Location of string list
    | `Dummy
    ]

type x_copy_flag =
    [ `Repl of int | `Location of string list | `Dummy ]


type x_link_flag =
    [ `Dummy ]

class type x_stream_fs =
object
  inherit Netfs.stream_fs

  method x_read_file : x_read_file_flag list -> string -> Netfs.local_file

  method x_write : x_write_flag list -> string -> Netchannels.out_obj_channel
  method x_write_file : x_write_file_flag list -> string -> Netfs.local_file -> unit
  method x_copy : x_copy_flag list -> string -> string -> unit
  method x_link : x_link_flag list -> string -> string -> unit
  method x_copy_out_to_buf : string -> int64 -> Netsys_mem.memory -> int -> unit
  method x_copy_in_from_buf : string -> int64 -> Netsys_mem.memory -> int -> unit
  method x_blocksize : unit -> int
  method x_location_info : unit -> (Unix.inet_addr * string) list
  method x_blocklist : string -> Plasma_rpcapi_aux.blockinfo list
  method x_commit_files : (string * string) list -> unit
end


let tr_errno e =
  match e with
    | `enotrans      -> Unix.EPERM
    | `efailedcommit -> Unix.EIO
    | `elongtrans    -> Unix.EIO
    | `efailed       -> Unix.EIO
    | `eperm         -> Unix.EPERM
    | `enoent        -> Unix.ENOENT
    | `eaccess       -> Unix.EACCES
    | `eexist        -> Unix.EEXIST
    | `efhier        -> Unix.EINVAL
    | `einval        -> Unix.EINVAL
    | `efbig         -> Unix.EFBIG
    | `enospc        -> Unix.ENOSPC
    | `erofs         -> Unix.EROFS
    | `enametoolong  -> Unix.ENAMETOOLONG
    | `econflict     -> Unix.EPERM
    | `ecoord        -> Unix.EPERM
    | `enonode       -> Unix.EIO
    | `etbusy        -> Unix.EPERM
    | `eio           -> Unix.EIO
    | `estale        -> (* Unix.ESTALE *) Unix.ENOENT
    | `eloop         -> Unix.ELOOP
    | `enotdir       -> Unix.ENOTDIR
    | `eisdir        -> Unix.EISDIR
    | `enotempty     -> Unix.ENOTEMPTY
    | `ebadpath      -> Unix.ENOTDIR

let tr_error ~notranslate e fn file =
  match e with
    | Plasma_client.Plasma_error errno when not notranslate ->
	let s = Netexn.to_string e in
	Unix.Unix_error(tr_errno errno, (fn ^ " [" ^ s ^ "]"), file)
    | _ ->
	e


let catch_error ~notranslate fn file f arg =
  try
    f arg
  with
    | e ->
	raise(tr_error ~notranslate e fn file)


let w_trans ~notranslate fn file cluster f =
  (* Includes support for retry and for catch_error *)
  catch_error ~notranslate fn file
    (Plasma_client.retry cluster file
       (fun () ->
	  Plasma_client.with_trans cluster f
       )
    )
    ()


let check_is_regular cluster inode =
  let ii = Plasma_client.get_cached_inodeinfo cluster inode false in
          (* also refreshes metadata *)
  match ii.Plasma_rpcapi_aux.filetype with
    | `ftype_regular -> ()
    | `ftype_directory ->
	raise(Plasma_client.Plasma_error `eisdir)
    | _ -> assert false


let input_file_1 ~notranslate cluster inode file init_pos =
  let () = check_is_regular cluster inode in
  let pos = ref init_pos in
  let closed = ref false in
object(self)
  method input s p len =
    if !closed then raise Netchannels.Closed_channel;
    if len = 0 then
      0
    else (
      let (n,eof,_ii) =
	catch_error ~notranslate "Plasma_netfs.read" file
	  (Plasma_client.read cluster inode !pos (`String s) p) len in
      if n=0 && eof then raise End_of_file;
      pos := Int64.add !pos (Int64.of_int n);
      n
    )
  method close_in() =
    closed := true
end


let input_file ~notranslate cluster inode file init_pos =
  Netchannels.lift_in
    (`Rec (input_file_1 ~notranslate cluster inode file init_pos))


let output_file_1 ~notranslate cluster inode file cancelled =
  let () = check_is_regular cluster inode in
  let pos = ref 0L in
  let closed = ref false in
object(self)
  method output s p l =
    if !closed then raise Netchannels.Closed_channel;
    if l = 0 then
      0
    else (
      let n =
	catch_error ~notranslate "Plasma_netfs.write" file
	  (Plasma_client.write cluster inode !pos (`String s) p) l in
      pos := Int64.add !pos (Int64.of_int n);
      n
    )

  method flush() =
    if !closed then raise Netchannels.Closed_channel;
    if !cancelled then
      catch_error ~notranslate "Plasma_netfs.write" file
	(Plasma_client.drop_inode cluster) inode
    else
      catch_error ~notranslate "Plasma_netfs.write" file
	(Plasma_client.flush cluster inode 0L) 0L

  method close_out() =
    if not !closed then (
      self # flush();
      closed := true
    )
end


let output_file ~notranslate cluster inode file cancelled =
  Netchannels.lift_out
    ~buffered:false
    (`Rec (output_file_1 ~notranslate cluster inode file cancelled))

let list_isect_empty l1 l2 = (* whether intersection is empty *)
  List.for_all
    (fun x1 -> not (List.mem x1 l2))
    l1

let slash_re = Netstring_pcre.regexp "/+"


let rec extract f l d =
  match l with
    | x :: l' ->
	( match f x with
	    | None -> extract f l' d
	    | Some v -> v
	)
    | [] ->
	d


let extract_repl repl flags =
  extract
    (function
       | `Repl n -> Some n
       | _ -> None
    )
    flags repl


let extract_location local_ids flags =
  let locs1 =
    extract
      (function 
	 | `Location l -> Some l
	 | _ -> None
      )
      flags [] in
  let locs2 =
    List.flatten
      (List.map
	 (function
	    | "_local" -> local_ids
	    | id -> [id]
	 )
	 locs1
      ) in
  locs2


class x_netfs ?(repl=0) ?(verbose=false) ?(notranslate=false)
              cluster : x_stream_fs =
  let fix_ii ?(repl=repl) ii =
    { ii with 
	Plasma_rpcapi_aux.replication = repl } in
  let get_local_ids() =
    Plasma_client.local_identities cluster in
  let cancelled = ref false in
object(self)
  inherit Netfs.empty_fs "Plasma_netfs"

  method path_encoding = Some `Enc_utf8
  method path_exclusions = [ 0,0; 47,47 ]
  method nominal_dot_dot = false

  method read flags name =
    catch_error ~notranslate "Plasma_netfs.read" name
      (fun () ->
	 let inode =
	   Plasma_client.with_trans
	     cluster
	     (fun trans -> Plasma_client.lookup trans name false) in
	 let skip = 
	   extract
	     (function
		| `Skip n -> Some n
		| _ -> None
	     )
	     flags 0L in
	 input_file ~notranslate cluster inode name skip
      )
      ()

  method read_file flags name =
    self # x_read_file (flags :> x_read_file_flag list) name

  method x_read_file flags name =
    catch_error ~notranslate "Plasma_netfs.read_file" name
      (fun () ->
	 let (inode,len) =
	   Plasma_client.with_trans
	     cluster
	     (fun trans -> 
		let inode = Plasma_client.lookup trans name false in
		let ii = Plasma_client.get_inodeinfo trans inode in
		let len = ii.Plasma_rpcapi_aux.eof in
		(inode,len)
	     ) in
         let destination =
           extract
             (function
                | `Destination filename -> Some (Some filename)
                | _ -> None
             )
             flags
             None in
         let (outfd,filename,del_flag) =
           match destination with
             | Some n ->
                 let outfd =
                   Unix.openfile 
                     n [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC] 0o600 in
                 (outfd, n, false)
             | None ->
                 let tmp_directory, tmp_prefix =
                   extract
                     (function
                        | `Temp(dir,prefix) -> Some(Some dir,Some prefix)
                        | _ -> None
                     )
                     flags
                     (None, None) in
	         let (tmp_name, inch, outch) =
                   Netchannels.make_temporary_file 
                     ?tmp_directory ?tmp_prefix () in
                 close_in inch;
	         let outfd = Unix.descr_of_out_channel outch in
                 (outfd, tmp_name, true) in
	 try
	   let _real_len =
	     Plasma_client.copy_out
	       ~flags:[`Retry]
	       cluster inode 0L outfd len in
	   Unix.close outfd;
	   ( object
	       method filename = filename
	       method close() =
                 if del_flag then
		   ( try Unix.unlink filename with _ -> () )
	     end
	   )
	 with
	   | error ->
	       Unix.close outfd;
	       raise error
      )
      ()

  method x_copy_out_to_buf name =
    let inode =
      w_trans ~notranslate "Plasma_netfs.x_copy_out_to_buf" name cluster
	(fun trans -> 
	   Plasma_client.lookup trans name false
	) in
    fun pos buf len ->
      catch_error ~notranslate "Plasma_netfs.x_copy_out_to_buf" name
	(fun () ->
	   let _real_len =
	     Plasma_client.copy_out_to_buf
	       ~flags:[`Retry]
	       cluster inode pos buf len in
	   ()
	)
	()
	
  method private x_write_prep
                    trunc_flag create_flag excl_flag wr_repl locs name =
    let inode =
      w_trans ~notranslate "Plasma_netfs.write[_file]" name cluster
	(fun trans -> 
	   try
	     let inode = Plasma_client.lookup trans name false in
	     if create_flag && excl_flag then
	       raise(Plasma_client.Plasma_error `eexist);
	     if trunc_flag then (
	       Plasma_client.truncate trans inode 0L;
	       if verbose then
		 eprintf "Truncated: %s\n%!" name;
	       inode
	     )
	     else
	       inode
	   with
	     | Plasma_client.Plasma_error `enoent ->
		 let inode =
		   Plasma_client.create_file trans name 
		     (fix_ii
			~repl:wr_repl
			(Plasma_client.regular_ii cluster 0o666)) in
		 if verbose then
		   eprintf "Created: %s\n%!" name;
		 inode
	) in
    if locs <> [] then
      Plasma_client.configure_pref_nodes_of_inode
	cluster inode (Some locs);
    inode


  method write flags name =
    self # x_write (flags :> x_write_flag list) name

  method x_write flags name =
    let trunc_flag = List.mem `Truncate flags in
    let create_flag = List.mem `Create flags in
    let excl_flag = List.mem `Exclusive flags in
    let wr_repl = extract_repl repl flags in
    let local_ids = get_local_ids() in
    let locs = extract_location local_ids flags in
    let inode = 
      self # x_write_prep trunc_flag create_flag excl_flag wr_repl locs name in
    cancelled := false;
    output_file ~notranslate cluster inode name cancelled

  method cancel() =
    cancelled := true

  method write_file flags name local =
    self # x_write_file (flags :> x_write_file_flag list) name local

  method x_write_file flags name local =
    let cleanup = ref [ (fun () -> local#close()) ] in
    let do_cleanup() = List.iter (fun f -> f()) !cleanup in
    try
      let trunc_flag = List.mem `Truncate flags in
      let create_flag = List.mem `Create flags in
      let excl_flag = List.mem `Exclusive flags in
      let wr_repl = extract_repl repl flags in
      let local_ids = get_local_ids() in
      let locs = extract_location local_ids flags in
      let local_fd = Unix.openfile local#filename [Unix.O_RDONLY] 0 in
      cleanup := (fun () -> Unix.close local_fd) :: !cleanup;
      let st = Unix.LargeFile.fstat local_fd in
      let len = st.Unix.LargeFile.st_size in
      let inode = 
	self # x_write_prep 
	  trunc_flag create_flag excl_flag wr_repl locs name in
      let _real_len =
	Plasma_client.copy_in
	  ~flags:[`Late_datasync; `Retry]
	  cluster inode 0L local_fd len `Star in
      do_cleanup()
    with
      | error ->
	  do_cleanup();
	  raise error

  method x_copy_in_from_buf name =
    let inode =
      w_trans ~notranslate "Plasma_netfs.x_copy_in_from_buf" name cluster
	(fun trans -> 
	   Plasma_client.lookup trans name false
	) in
    fun pos buf len ->
      catch_error ~notranslate "Plasma_netfs.x_copy_in_from_buf" name
	(fun () ->
	   let _real_len =
	     Plasma_client.copy_in_from_buf 
	       ~flags:[`Late_datasync; `Retry]
	       cluster inode pos buf len `Star in
	   ()
	)
	()


  method size _ name =
    w_trans ~notranslate "Plasma_netfs.size" name cluster
      (fun trans -> 
	 let inode = Plasma_client.lookup trans name false in
	 let ii = Plasma_client.get_inodeinfo trans inode in
	 ii.Plasma_rpcapi_aux.eof
      )

  method private test_list_NH trans flags name =
    try
      let inode = Plasma_client.lookup trans name true in
      let ii = Plasma_client.get_inodeinfo trans inode in
      if ii.Plasma_rpcapi_aux.filetype = `ftype_symlink then
        [ `N; `H ]
      else
        [ `N ]
    with
      | Plasma_client.Plasma_error(`enoent | `ebadpath) -> []
	  
  method private test_list_EDFS trans flags name =
    try
      let inode = Plasma_client.lookup trans name (List.mem `Link flags) in
      let ii = Plasma_client.get_inodeinfo trans inode in
      let non_empty = ii.Plasma_rpcapi_aux.eof <> 0L in
      let kind_l =
        match ii.Plasma_rpcapi_aux.filetype with
          | `ftype_regular -> [ `F ]
          | `ftype_directory -> [ `D ]
          | _ -> [] in
      [ `E ] @ kind_l @ (if non_empty then [`S] else [])
    with
      | Plasma_client.Plasma_error(`enoent | `ebadpath) -> []
	  
  method test flags name ttype =
    w_trans ~notranslate "Plasma_netfs.test" name cluster
      (fun trans -> 
	 let l =
	   match ttype with
             | `N | `H -> self#test_list_NH trans flags name
             | `E | `D | `F | `S -> self#test_list_EDFS trans flags name
             | `R | `W | `X -> [] in
         List.mem ttype l
      )

  method test_list flags name tests =
    w_trans ~notranslate "Plasma_netfs.test_list" name cluster
      (fun trans -> 
         let nh =
           if not(list_isect_empty tests [`N;`H]) then
             self#test_list_NH trans flags name
           else
             [] in
         let edfs =
           if not(list_isect_empty tests [`E;`D;`F;`S]) then
             self#test_list_EDFS trans flags name
           else
             [] in
         let rwx = [] in
         List.map
           (fun t ->
              match t with
		| `N | `H -> List.mem t nh
		| `E | `D | `F | `S -> List.mem t edfs
		| `R | `W | `X -> List.mem t rwx
           )
           tests
      )

  method remove flags name = (* `Recursive *)
    let recurse = List.mem `Recursive flags in
    let rm_trans = ref None in
    let rm_count = ref 0 in

    (* We only delete here 100 files in one transaction, and not more.
       The reason is that the deletion becomes the slower the longer
       the transaction is. This is hard to fix in the namenode, so we
       avoid long "rm" transactions in the client.
     *)

    let unlink_at dir_inode name =
      let t =
	match !rm_trans with
	  | Some t -> t
	  | None ->
	      let t = Plasma_client.start cluster in
	      rm_trans := Some t;
	      rm_count := 0;
	      t in
      (* The namelock is only effective for the case that the unlink
	 is not successful (ENOTEMPTY)!
       *)
      Plasma_client.namelock t dir_inode name;
      Plasma_client.unlink_at t dir_inode name;
      incr rm_count;
      if !rm_count >= 100 then (
	Plasma_client.commit t;
	rm_trans := None;
      ) in

    let commit() =
      match !rm_trans with
	| None -> ()
	| Some t ->
	    Plasma_client.commit t;
	    if verbose then eprintf "Committed.\n%!";
      	    rm_trans := None in
    
    w_trans ~notranslate "Plasma_netfs.remove" name cluster
      (fun trans -> 
	 (* [trans] is only used for lookups *)

	 let rec remove_dir_contents dir_name dir_inode =
	   let entries = Plasma_client.list_inode trans dir_inode in
	   List.iter
	     (fun (base,_) -> 
		remove_entry (dir_name ^ "/" ^ base) dir_inode base)
	     entries
    
	 and remove_entry name dir_inode base =
	   try
	     unlink_at dir_inode base;
	     if verbose then
	       eprintf "Removed: %s\n%!" name
	   with
	     | Plasma_client.Plasma_error `enotempty when recurse ->
		 let inode = 
		   Plasma_client.dir_lookup trans dir_inode base true in
		 let ii = Plasma_client.get_inodeinfo trans inode in
		 if ii.Plasma_rpcapi_aux.filetype = `ftype_directory then
		   remove_dir_contents name inode;
		 unlink_at dir_inode base;
		 if verbose then
		   eprintf "Removed: %s\n%!" name
	 in
	 
	 let dir = Filename.dirname name in
	 let base = Filename.basename name in
	 let dir_inode = Plasma_client.lookup trans dir false in
	 remove_entry name dir_inode base;
	 commit()
      )

  method rename flags oldname newname =
    w_trans ~notranslate "Plasma_netfs.rename" oldname cluster
      (fun trans -> 
	 ( try
	     Plasma_client.unlink trans newname
	   with
	     | Plasma_client.Plasma_error (`enoent|`eisdir) ->
		 ()
	 );
	 Plasma_client.rename trans oldname newname;
	 if verbose then
	   eprintf "Renamed: %s -> %s\n%!" oldname newname
      );
    if verbose then eprintf "Committed.\n%!"


  method x_commit_files l =
    if l <> [] then (
      w_trans ~notranslate "Plasma_netfs.x_commit_files" "<various>" cluster
	(fun trans -> 
	   List.iter
	     (fun (o,n) ->
		Plasma_client.rename trans o n;
		if verbose then
		  eprintf "Renamed: %s -> %s\n%!" o n
	     )
	     l;
	);
      if verbose then eprintf "Committed.\n%!"
    )


  method x_link flags oldname newname =
    w_trans ~notranslate "Plasma_netfs.x_link" oldname cluster
      (fun trans -> 
	 let inode = Plasma_client.lookup trans oldname true in
	 Plasma_client.link trans newname inode;
	 if verbose then
	   eprintf "Linked: %s -> %s\n%!" oldname newname
      );
    if verbose then eprintf "Committed.\n%!"


  method symlink flags oldname newname =
    w_trans ~notranslate "Plasma_netfs.symlink" newname cluster
      (fun trans -> 
	 ignore(
	   Plasma_client.create_file trans newname 
	     (Plasma_client.symlink_ii cluster oldname));
	 if verbose then
	   eprintf "Linked: %s -> %s\n%!" newname oldname
      );
    if verbose then eprintf "Committed.\n%!"


  method readdir flags name =
    w_trans ~notranslate "Plasma_netfs.readdir" name cluster
      (fun trans -> 
	 [ "."; ".." ] @
	   (List.map 
	      fst
	      (Plasma_client.list trans name)
	   )
      )

  method readlink flags name =
    w_trans ~notranslate "Plasma_netfs.readlink" name cluster
      (fun trans -> 
	 let inode = Plasma_client.lookup trans name true in
	 let ii = Plasma_client.get_inodeinfo trans inode in
	 match ii.Plasma_rpcapi_aux.filetype with
	   | `ftype_symlink ->
	       ii.Plasma_rpcapi_aux.field1
	   | _ ->
	       raise(Plasma_client.Plasma_error `einval)
      )    

  method mkdir flags name = (* `Path, `Nonexcl *)
    w_trans ~notranslate "Plasma_netfs.mkdir" name cluster
      (fun trans ->
	 if List.mem `Path flags then
	   self#mkdir_p trans name
	 else (
	   try
	     ignore(
	       Plasma_client.mkdir trans name 
		 (Plasma_client.dir_ii cluster 0o777));
	     if verbose then
	       eprintf "Created dir: %s\n%!" name
	   with
             | Plasma_client.Plasma_error `eexist 
		 when List.mem `Nonexcl flags ->
		 ()
	 )
      );
    if verbose then eprintf "Committed.\n%!"


  method private mkdir_p trans filename =
    let rec traverse curdir todo =
      match todo with
        | [] -> ()
        | d :: todo' ->
            let curdir' = curdir @ [d] in
            let p = String.concat "/" curdir' in
            ( try 
		ignore(
		  Plasma_client.mkdir trans p
		    (Plasma_client.dir_ii cluster 0o777));
		if verbose then
		  eprintf "Created dir: %s\n%!" p
	      with 
		| Plasma_client.Plasma_error `eexist -> ()
            );
            traverse curdir' todo' in
    let l = Netstring_pcre.split_delim slash_re filename in
    match l with
      | "" :: tl ->
          traverse [""] tl
      | _ ->
	  raise(Plasma_client.Plasma_error `einval)
	    
  method rmdir flags name =
    w_trans ~notranslate "Plasma_netfs.rmdir" name cluster
      (fun trans -> 
	 let inode = Plasma_client.lookup trans name true in
	 let ii = Plasma_client.get_inodeinfo trans inode in
	 match ii.Plasma_rpcapi_aux.filetype with
	   | `ftype_directory ->
	       Plasma_client.unlink trans name;  (* FIXME (potential race) *)
	       if verbose then
		 eprintf "Removed dir: %s\n%!" name
	   | _ ->
	       raise(Plasma_client.Plasma_error `enotdir)
      );
    if verbose then eprintf "Committed.\n%!"


  method copy flags oldname newname =
    self # x_copy (flags :> x_copy_flag list) oldname newname


  method x_copy flags oldname newname =
    (* FIXME: This is a dumb implementation. We could avoid the pipe.
       Ideally, the datanodes storing the original version of the file
       would directly send the blocks to the datanodes for the copy
     *)
    let local_ids = get_local_ids() in
    let esys = Plasma_client.event_system cluster in
    let fd_rd, fd_wr = Unix.pipe() in
    Unix.set_nonblock fd_rd;
    Unix.set_nonblock fd_wr;
    let fd_wr_closed = ref false in

    let copy_e oldinode newinode len =
      let e1 =
	Plasma_client.copy_out_e
	  ~flags:[`Retry]
	  cluster oldinode 0L fd_wr len 
	++ (fun _ ->
	      Unix.close fd_wr;
	      fd_wr_closed := true;
	      eps_e (`Done ()) esys 
	   ) in
      let e2 = 
	Plasma_client.copy_in_e 
	  ~flags:[`Late_datasync; `Retry]
	  cluster newinode 0L fd_rd len `Star in
      Uq_engines.sync_engine e1 e2 in

    let e =
      Plasma_client.retry_e cluster "Plasma_netfs.copy"
	(fun () ->
	   Plasma_client.with_trans_e cluster
	     (fun trans ->
		Plasma_client.lookup_e trans oldname false
		++ (fun oldinode ->
		      Plasma_client.get_inodeinfo_e trans oldinode
		      ++ (fun oldii ->
			    let oldft = oldii.Plasma_rpcapi_aux.filetype in
			    if oldft <> `ftype_regular then
			      raise(Plasma_client.Plasma_error `eisdir);
			    Uq_engines.meta_engine
			      (Plasma_client.lookup_e trans newname false)
			    ++ (function
				  | `Error(Plasma_client.Plasma_error _) ->
				      let wr_repl = extract_repl repl flags in
				      Plasma_client.create_file_e trans newname 
					(fix_ii
					   ~repl:wr_repl
					   (Plasma_client.regular_ii 
					      cluster 0o666))
				  | `Done newinode ->
				      Plasma_client.truncate_e trans newinode 0L
				      ++ (fun () ->
					    eps_e (`Done newinode) esys
					 )
				  | st -> 
				      eps_e
					(st :> _ Uq_engines.engine_state) esys
			       )
			    ++ (fun newinode ->
				  let len = oldii.Plasma_rpcapi_aux.eof in
				  eps_e (`Done (oldinode, newinode, len)) esys
			       )
			 )
		   )
	     )
	)
	()
      ++ (fun (oldinode, newinode, len) ->
	    let locs = extract_location local_ids flags in
	    if locs <> [] then
	      Plasma_client.configure_pref_nodes_of_inode
		cluster newinode (Some locs);
	    copy_e oldinode newinode len
	 )
    in
    Unixqueue.run esys;
    if not !fd_wr_closed then Unix.close fd_wr;
    Unix.close fd_rd;
    match e # state with
      | `Done _ -> 
	  if verbose then
	    eprintf "Copied: %s -> %s\n%!" oldname newname
      | `Error e -> raise (tr_error e ~notranslate "Plasma_netfs.copy" oldname)
      | `Aborted -> failwith "aborted"
      | `Working _ -> assert false

  method x_blocksize () =
    Plasma_client.blocksize cluster

  method x_location_info() =
    let info = Plasma_client.get_dn_info cluster in
    List.map
      (fun dn ->
	 let (h,p) = Plasma_util.parse_host_port dn.Plasma_rpcapi_aux.dn_node in
         let ip = Unix.inet_addr_of_string h in
	 (ip, dn.Plasma_rpcapi_aux.dn_identity)
      )
      (Array.to_list info)

  method x_blocklist name =
    w_trans ~notranslate "Plasma_netfs.x_blocklist" name cluster
      (fun trans -> 
	 let inode = Plasma_client.lookup trans name false in
	 let ii = Plasma_client.get_inodeinfo trans inode in
	 Plasma_client.get_blocklist 
	   trans inode 0L ii.Plasma_rpcapi_aux.blocklimit false 
      )    

end


let x_netfs = new x_netfs 


class netfs ?repl ?verbose ?notranslate cluster =
  let x = x_netfs ?repl ?verbose ?notranslate cluster in
object(self)
  inherit Netfs.empty_fs "Plasma_netfs.netfs"
  method path_encoding = x#path_encoding
  method path_exclusions = x#path_exclusions
  method nominal_dot_dot = x#nominal_dot_dot
  method read = x#read
  method read_file = x#read_file
  method write = x#write
  method write_file = x#write_file
  method size = x#size
  method test = x#test
  method test_list = x#test_list
  method remove = x#remove
  method rename = x#rename
  method symlink = x#symlink
  method readdir = x#readdir
  method readlink = x#readlink
  method mkdir = x#mkdir
  method rmdir = x#rmdir
  method copy = x#copy
  method cancel = x#cancel
end


let netfs = new netfs
