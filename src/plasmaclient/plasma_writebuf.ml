(* $Id$ *)

open Uq_engines.Operators

module Piece = struct
  (* A piece of data to write: A string containing the data from
     file position from_pos to to_pos (incl.).
   *)
  type key = int64    (* a file position *)
  type t = int64 * int64 * string   (* (from_pos, to_pos, data) *)
  let merge (min1,max1,data1) (min2,max2,data2) =
    let l1 = String.length data1 in
    let l2 = String.length data2 in
    if l1+l2 <= 512 then (    (* merge only small pieces! *)
      if Int64.succ max1 = min2 then
        Some(min1, max2, data1 ^ data2)
      else if Int64.succ max2 = min1 then
        Some(min2, max1, data1 ^ data2)
      else
        None
    )
    else
      None
  let split p (min,max,data) =
    if p <= min || p > max then 
      failwith "Plasma_util.I64RangeAtom: cannot split";
    let len = Int64.to_int (Int64.sub p min) in
    let d1 = String.sub data 0 len in
    let d2 = String.sub data len (String.length data - len) in
    (min, Int64.pred p, d1), (p, max, d2)

end

module PieceMap = Plasma_util.RangeMap(Plasma_util.I64)(Piece)
  (** A mapping from file position ranges to pieces *)

type write_buffer =
    { buffers : (int64 * int64, block_buffer) Hashtbl.t;
        (* maps (inode, block) to block_buffer *)
      client : Plasma_client.plasma_cluster;
      timeout : float;
      max_blocks : int;
      (* cur_blocks = Hashtbl.length buffers *)
      blocksize : int;
    }

and block_buffer =
    { mutable flushing : unit Uq_engines.engine option;
        (* If flushing, no more pieces must be added *)
      mutable pieces : PieceMap.t;
      mutable timer : unit Uq_engines.engine option;
      mutable eof : int64 option;
        (* the minimum eof position because of this written block *)
      mutable mtime : Plasma_rpcapi_aux.time option
        (* the minimum mtime because of this written block *)
    }

type strmem = Plasma_client.strmem

let max_time mtime1 mtime2 =
  let open Plasma_rpcapi_aux in
  if mtime1.tsecs > mtime2.tsecs then
    mtime1
  else
    if mtime1.tsecs < mtime2.tsecs then
      mtime2
    else
      if mtime1.tnsecs < mtime2.tnsecs then
        mtime1
      else
        mtime2


let get_time() =
  let open Plasma_rpcapi_aux in
  let t = Unix.gettimeofday() in
  let t1 = Int64.of_float t in
  let t2 = Pervasives.truncate ((t -. Int64.to_float t1) *. 1E9) in
  { tsecs = t1; tnsecs = t2 }


let zero_time =
  { Plasma_rpcapi_aux.tsecs = 0L; tnsecs = 0 }



let create cluster num tmo =
  { buffers = Hashtbl.create 41;
    client = cluster;
    timeout = tmo;
    max_blocks = num;
    blocksize = Plasma_client.blocksize cluster;
  }


let stop_timer bbuf =
  match bbuf.timer with
    | None -> ()
    | Some e -> 
        e#abort();
        bbuf.timer <- None

let is_complete wbuf block bbuf =
  (* Whether all pieces have been written *)
  let startpos = Int64.mul block (Int64.of_int wbuf.blocksize) in
  let endpos = Int64.add startpos (Int64.of_int wbuf.blocksize) in
  let lastpos = ref startpos in
  let whole_block = ref true in
  ( try
      PieceMap.iter
        (fun _ (pos_from, pos_to, _) ->
           if pos_from <> !lastpos then raise Not_found;
           lastpos := Int64.succ pos_to;
        )
        bbuf.pieces;
    with Not_found -> whole_block := false
  );
  if !lastpos <> endpos then whole_block := false;
  !whole_block


let count_pieces bbuf =
  PieceMap.fold
    (fun _ _ n -> n+1)
    bbuf.pieces
    0


let submit_e wbuf inode block bbuf =
  (* Submit writes for a block *)
  let esys = Plasma_client.event_system wbuf.client in
  (* First check whether we write a whole block. If so, and if we have
     more than one piece, we copy first the data into a single string.
     This way, we avoid to load the block.
   *)
  let startpos = Int64.mul block (Int64.of_int wbuf.blocksize) in
  let complete = is_complete wbuf block bbuf in
  if complete && count_pieces bbuf > 1 then (
    (* Whole block case: just copy all pieces to [data], and write that *)
    let data = String.create wbuf.blocksize in
    PieceMap.iter
      (fun _ (pos_from, pos_to, data_piece) ->
         let p = Int64.to_int(Int64.sub pos_from startpos) in
         let l = Int64.to_int (Int64.sub pos_to pos_from) + 1 in
         String.blit data_piece 0 data p l
      )
      bbuf.pieces;
    Plasma_client.write_e
      wbuf.client 
      inode
      startpos
      (`String data)
      0
      wbuf.blocksize
    ++ (fun _ -> 
          bbuf.eof <- None;
          bbuf.mtime <- None;
          eps_e (`Done()) esys
       )
  )
  else (
    (* Simple case: just submit the data piece by piece *)
    let elist =
      PieceMap.fold
        (fun _ (pos_from, pos_to, data) acc ->
           (Plasma_client.write_e
              wbuf.client 
              inode
              pos_from
              (`String data)
              0
              (Int64.to_int (Int64.sub pos_to pos_from) + 1)
            ++ (fun _ -> 
                  eps_e (`Done ()) esys
               )
           ) :: acc
        )
        bbuf.pieces
        [] in
    (* Wait until done *)
    Uq_engines.msync_engine
      elist
      (fun () () -> ())
      ()
      esys
    ++ (fun () ->
          bbuf.eof <- None;
          bbuf.mtime <- None;
          eps_e (`Done ()) esys
       )
  )


let flush_e wbuf inode =
  let esys = Plasma_client.event_system wbuf.client in
  (* First select the blocks to flush *)
  let blocks =
    Hashtbl.fold
      (fun (i, block) bbuf acc ->
         let we_flush = (bbuf.flushing = None) in
         if i=inode then (block,bbuf,we_flush) :: acc else acc
      )
      wbuf.buffers
      [] in
  (* Submit *)
  let elist =
    List.map
      (fun (block,bbuf,_) ->
         match bbuf.flushing with
           | None ->
               let e = submit_e wbuf inode block bbuf in
               bbuf.flushing <- Some e;
               e
           | Some e ->
               e
      )
      blocks in
  (* Wait until done *)
  Uq_engines.msync_engine
    elist
    (fun () () -> ())
    ()
    esys
  ++
    (fun _ ->
       (* Remove the blocks from the hash table *)
       List.iter
         (fun (block,bbuf,we_flush) ->
            if we_flush then (
              stop_timer bbuf;
              Hashtbl.remove wbuf.buffers (inode,block)
            )
         )
         blocks;
       eps_e (`Done ()) esys
    )


let flush_single_e wbuf inode block bbuf =
  (* This submits the block; a Plasma_client-level flush is NOT started *)
  let esys = Plasma_client.event_system wbuf.client in
  if bbuf.flushing = None then (
    let e =
      submit_e wbuf inode block bbuf
      ++ (fun () ->
            stop_timer bbuf;
            Hashtbl.remove wbuf.buffers (inode,block);
            eps_e (`Done ()) esys
         ) in
    bbuf.flushing <- Some e;
    e
  )
  else
    eps_e (`Done ()) esys



let start_timer wbuf inode block bbuf =
  let esys = Plasma_client.event_system wbuf.client in
  let e =
    Uq_engines.delay_engine
      wbuf.timeout
      (fun () ->
         bbuf.timer <- None;
         flush_single_e wbuf inode block bbuf
      )
      esys in
  bbuf.timer <- Some e


let strmem_sub sm pos len =
  match sm with
    | `String data -> `String(String.sub data pos len)
    | `Memory data -> `Memory(Bigarray.Array1.sub data pos len)
        (* Warning: the latter does not copy! *)


let as_string sm =
  match sm with
    | `String data -> data
    | `Memory data ->
        let l = Bigarray.Array1.dim data in
        let s = String.create l in
        Netsys_mem.blit_memory_to_string data 0 s 0 l;
        s

let strmem_zero sm pos len =
  match sm with
    | `String s -> 
        String.fill s pos len '\000'
    | `Memory m -> 
        Bigarray.Array1.fill
          (Bigarray.Array1.sub m pos len)
          '\000'
          
let rec write_e wbuf inode filepos data datapos datalen =
  let esys = Plasma_client.event_system wbuf.client in
  (* We only write up to one block *)
  let blocksizeL = Int64.of_int wbuf.blocksize in
  let block = Int64.div filepos blocksizeL in
  let block_space = 
    wbuf.blocksize -
      Int64.to_int (Int64.rem filepos blocksizeL) in
  let datalen1 = min datalen block_space in
  let endpos = Int64.add filepos (Int64.of_int datalen1) in
  (* - If the block is complete:
       call Plasma_client directly, and destroy our old version
     - If we are out of write buffers:
       call Plasma_client directly
     - If we are already flushing the block:
       call Plasma_client directly, but don't destroy the old version.
       The writes will compete against each other.
     - If we have the block already in a write buffer:
       continue to use this buffer
     - If the block is buffered by Plasma_client:
       call Plasma_client directly
     - Otherwise create a new write buffer for it     
   *)
  let (need_blocks, flushing) =
    try
      let bbuf = Hashtbl.find wbuf.buffers (inode,block) in
      (0, bbuf.flushing <> None)
    with Not_found ->
      (1, false) in
  (* need_blocks = 0: we have already a write buffer.
     need_blocks = 1: we would need a new write buffer
   *)
  let out_of_buffers =
    Hashtbl.length wbuf.buffers + need_blocks >= wbuf.max_blocks in
  let block_complete =
    datalen1=wbuf.blocksize in
  let in_mem =
    need_blocks = 1 &&
    Plasma_client.in_buffer wbuf.client inode filepos in
  if out_of_buffers || flushing || block_complete || in_mem || datalen1=0 then (
    if block_complete && not flushing then (
      try
        let bbuf = Hashtbl.find wbuf.buffers (inode,block) in
        stop_timer bbuf;
        Hashtbl.remove wbuf.buffers (inode,block);
      with Not_found -> ()
    );
    Plasma_client.write_e
      wbuf.client inode filepos data datapos datalen1
  )
  else (
    let now = get_time() in
    let finalpos = 
      Int64.add filepos (Int64.of_int (datalen1-1)) in
    (* If we have already a write buffer, continue to use it *)
    try
      let bbuf = Hashtbl.find wbuf.buffers (inode,block) in
      assert(bbuf.flushing = None);
      bbuf.pieces <- 
        PieceMap.add
          (filepos,finalpos)
          (filepos,finalpos,as_string(strmem_sub data datapos datalen1))
          bbuf.pieces;
      bbuf.eof <- Some (match bbuf.eof with
                          | Some eof -> max eof endpos
                          | None -> endpos
                       );
      bbuf.mtime <- Some (match bbuf.mtime with
                            | Some t -> max_time t now
                            | None -> now
                         );
      stop_timer bbuf;
      (* If it is complete: submit now *)
      if is_complete wbuf block bbuf then 
        flush_single_e wbuf inode block bbuf
        ++ (fun () -> eps_e (`Done datalen1) esys)
      else (
        start_timer wbuf inode block bbuf;
        eps_e (`Done datalen1) esys
      )
    with
      | Not_found ->
          (* Create a new write_buffer *)
          let bbuf =
            { flushing = None;
              pieces = PieceMap.empty;
              timer = None;
              eof = Some endpos;
              mtime = Some now;
            } in
          bbuf.pieces <- 
            PieceMap.add
              (filepos,finalpos)
              (filepos,finalpos,as_string(strmem_sub data datapos datalen1))
              PieceMap.empty;
          Hashtbl.add wbuf.buffers (inode,block) bbuf;
          start_timer wbuf inode block bbuf;
          eps_e (`Done datalen1) esys
  )

let our_write_eof wbuf inode =
  let eof =
    Hashtbl.fold
      (fun (block,i) bbuf acc ->
         match bbuf.eof with
           | None -> acc
           | Some eof ->
               if i=inode then max eof acc else acc
      )
      wbuf.buffers
      (-1L) in
  if eof < 0L then raise Not_found;
  eof


let get_write_eof wbuf inode =
  try
    let eof1 = Plasma_client.get_write_eof wbuf.client inode in
    let eof2 =
      try our_write_eof wbuf inode
      with Not_found -> eof1 in
    max eof1 eof2
  with
    | Not_found ->
        our_write_eof wbuf inode


let our_write_mtime wbuf inode =
  let (valid,mtime) =
    Hashtbl.fold
      (fun (block,i) bbuf acc ->
         if i=inode then 
           match bbuf.mtime with
             | None -> acc
             | Some bbuf_t ->
                 let (acc_valid,acc_t) = acc in
                 (true, if acc_valid then max_time bbuf_t acc_t else bbuf_t)
         else
           acc
      )
      wbuf.buffers
      (false, zero_time) in
  if not valid then raise Not_found;
  mtime


let get_write_mtime wbuf inode =
  try
    let mtime1 = Plasma_client.get_write_mtime wbuf.client inode in
    let mtime2 =
      try our_write_mtime wbuf inode
      with Not_found -> mtime1 in
    max_time mtime1 mtime2
  with
    | Not_found -> 
        our_write_mtime wbuf inode


let rec read_e ?lazy_validation wbuf inode filepos data datapos datalen =
  let esys = Plasma_client.event_system wbuf.client in
  let blocksizeL = Int64.of_int wbuf.blocksize in
  let block = Int64.div filepos blocksizeL in
  let blockstart = Int64.mul block blocksizeL in
  let blockend = Int64.mul (Int64.succ block) blocksizeL in
  let blocklen = Int64.to_int(Int64.sub blockend filepos) in
  let datalen1 = min datalen blocklen in
  try
    let bbuf = Hashtbl.find wbuf.buffers (inode,block) in
    strmem_zero data datapos datalen1;
    Plasma_client.read_e
      ?lazy_validation
      wbuf.client
      inode
      filepos
      data
      datapos
      datalen1
    ++ (fun (n,_,ii) ->
          (* Copy the contents of the write buffer *)
          let n = ref n in
          PieceMap.iter
            (fun _ (spos,epos,content) ->
               let piece_s = Int64.to_int(Int64.sub spos blockstart) in
               let piece_n = Int64.to_int(Int64.sub epos spos) + 1 in
               let offset = Int64.to_int(Int64.sub spos filepos) in
               if offset < datalen1 then (
                 let p = min piece_n (datalen1 - offset) in
                 match data with
                   | `String s ->
                       String.blit
                         content piece_s s offset p
                   | `Memory m ->
                       Netsys_mem.blit_string_to_memory
                         content piece_s m offset p
               );
               n := max !n offset
            )
            bbuf.pieces;
          if !n = datalen1 && datalen1 < datalen then
            read_e 
              ?lazy_validation
              wbuf inode
              (Int64.add filepos (Int64.of_int !n))
              data
              (datapos + !n)
              (datalen - !n)
            ++ (fun (m,at_eof,ii) -> 
                  eps_e (`Done (!n + m,at_eof,ii)) esys
               )
          else
            let new_eof =
              try get_write_eof wbuf inode
              with Not_found -> Plasma_rpcapi_aux.(ii.eof) in
            let ii' =
              { ii with
                  Plasma_rpcapi_aux.eof = new_eof
              } in
            let at_eof' =
              Int64.add filepos (Int64.of_int !n) >= new_eof in
            eps_e (`Done (!n, at_eof', ii')) esys
       )
  with
    | Not_found ->
        (* The block does not have a write buffer. Just try to
           read the block from Plasma_client, and try again
         *)
        Plasma_client.read_e
          ?lazy_validation
          wbuf.client
          inode
          filepos
          data
          datapos
          datalen1
        ++ (fun (n,at_eof,ii) ->
              if n > 0 then
                read_e 
                  ?lazy_validation
                  wbuf inode
                  (Int64.add filepos (Int64.of_int n))
                  data
                  (datapos+n)
                  (datalen-n)
                ++ (fun (m,at_eof,ii) -> 
                      eps_e (`Done (n + m,at_eof,ii)) esys
                   )
              else
                eps_e (`Done (n,at_eof,ii)) esys
           )
