(* $Id$ *)

(** Write buffers

    This is a thin layer on top of {!Plasma_client}. The task of these
    buffers is to combine several small writes to few large writes
    submitted to PlasmaFS. This is reasonable when the small writes
    are smaller than a block but combine together to a whole block
    or more.

    The idea is to call the [write_e] function of this module instead
    of the same-name function of {!Plasma_client}. The data is buffered
    up at most until [flush_e] is called.
*)

type write_buffer

type strmem = Plasma_client.strmem

val create : Plasma_client.plasma_cluster -> int -> float -> write_buffer
  (** [create client num timeout]: The buffer has space for [num] blocks.
      In [timeout] the maximum time is passed blocks are held back.
      This means after [timeout] seconds an non-accessed block is
      submitted to PlasmaFS
   *)

val write_e : write_buffer -> int64 -> int64 -> strmem -> int -> int -> 
    int Uq_engines.engine
  (** [write_e wbuf inode filepos data datapos datalen].

      Same as {!Plasma_client.write_e} on the level of write buffers.
   *)

val read_e : ?lazy_validation:bool ->
             write_buffer -> int64 -> int64 -> strmem -> int -> int -> 
    (int * bool * Plasma_rpcapi_aux.inodeinfo) Uq_engines.engine
  (** [read_e wbuf inode filepos data datapos datalen].

      This version of {!Plasma_client.read_e} takes data in the write
      buffer into account.
   *)

val flush_e : write_buffer -> int64 -> unit Uq_engines.engine
  (** [flush_e wbuf inode] 

      Forces that the contents of the write buffers are written to the
      {!Plasma_client.plasma_cluster} layer. This function does not
      invoke {!Plasma_client.flush_e}, but the user can of course do it
      to flush the data to disk.
   *)

val get_write_eof : write_buffer -> int64 -> int64
  (** [get_write_eof wbuf inode]

      Same as {!Plasma_client.get_write_eof} on the level of write buffers.
   *)

val get_write_mtime : write_buffer -> int64 -> Plasma_rpcapi_aux.time
  (** [get_write_mtime wbuf inode]

      Same as {!Plasma_client.get_write_mtime} on the level of write buffers.
   *)

