(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type client_config =
object
  method clustername : string
  method nn_nodes : string list
end


let parse_config_file name =
  let cf = Netplex_config.read_config_file name in
  if cf # root_name <> "plasmafs" then
    failwith ("Not a plasmafs configuration file: " ^ name);
  
  let clusters =
    cf # resolve_section cf#root_addr "cluster" in
  List.map
    (fun cluster ->
       let clustername =
         try
           let p = cf#resolve_parameter cluster "clustername" in
           cf#string_param p
         with
           | Not_found ->
               failwith "missing 'cluster.clustername' parameter" in
       let addrs = Plasma_util.node_list cf cluster in
       ( object
	   method clustername = clustername
	   method nn_nodes = addrs
	 end
       )
    )
    clusters


let nodes_re = Pcre.regexp "[ \t\r\n,]+"

let parse_nodes s =
  try
    let l =
      List.filter
	(fun u -> u <> "")
	(Pcre.split ~rex:nodes_re s) in
    List.iter (fun p -> ignore(Plasma_util.parse_host_port p)) l;
    l
  with
    | _ ->
	failwith "Cannot parse environment variable PLASMAFS_NAMENODES"


let get_config ?clustername ?nn_nodes () =
  match clustername, nn_nodes with
    | Some cname, Some nodes ->
	( object
	    method clustername = cname
	    method nn_nodes = nodes
	  end
	)
    | _, _ ->
	let configs = lazy (
	  let home_dir =
	    (* First try HOME *)
	    try Unix.getenv "HOME"
	    with Not_found ->
	      let uid = Unix.getuid() in (* real user ID (of caller) *)
	      let pw = 
		try Unix.getpwuid uid 
		with Not_found ->
		  failwith "Plasma_client_config: cannot determine home directory"
	      in
	      pw.Unix.pw_dir in
	  let cf_name = 
	    home_dir ^ "/.plasmafs" in
	  parse_config_file cf_name
	) in
	let find_config name =
	  try
	    List.find
	      (fun c -> c#clustername = name)
	      (Lazy.force configs)
	  with Not_found ->
	    failwith ("Plasma_client_config: cluster undefined in \
                       ~/.plasmafs: " ^ name) in
	
	( match clustername with
	    | Some cname ->
		find_config cname
	    | None ->
		( try
		    let cname = Unix.getenv "PLASMAFS_CLUSTER" in
		    ( try
			let nodes = Unix.getenv "PLASMAFS_NAMENODES" in
			let parsed_nodes = parse_nodes nodes in
			( object
			    method clustername = cname
			    method nn_nodes = parsed_nodes
			  end
			)
		      with Not_found ->
			find_config cname
		    )
		  with
		    | Not_found ->
			( match Lazy.force configs with
			    | config :: _ -> config
			    | _ ->
				failwith "Plasma_client_config: insufficient \
                                    information given to select the cluster \
                                    from ~/.plasmafs"
			)
		)
	)
