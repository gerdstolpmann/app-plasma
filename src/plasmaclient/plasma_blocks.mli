(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Helpers for analyzing blocklists *)

module StrMap : Map.S with type key = string

type brange =
    { br_index : int64;                  (** the range starts at this index *)
      br_length : int64;         (** ... and includes this number of blocks *)
      br_datanodes : int64 StrMap.t;    (** maps dn identity to start block *)
    }

module Brange : 
  Plasma_util.Mergable with type key=int64 and type t=brange
  (** block ranges can be merged and split *)

module Bset : 
  Plasma_util.RangeMapType with type point=int64 and type value=brange
  (** a set of block ranges *)

val bset_of_blocklist : Plasma_rpcapi_aux.blockinfo list -> Bset.t
  (** Converts a normal blocklist into a set of block ranges *)
