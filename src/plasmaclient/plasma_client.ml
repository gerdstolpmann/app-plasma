(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* 1. contact auth daemon before find_coord if auth=`Auth_daemon
      -> with_nn_session
      -> reset the ticket if the coordinator changes

   2. call "impersonate" as first RPC if auth=`Auth_daemon or `Auth_ticket
      -> with_nn_session

   3. delete the auth ticket finally if auth=`Auth_daemon
      -> with_nn_session
      ALT: impersonate gets new arg "auto delete". Saves an RPC call.
 *)

open Plasma_rpcapi_aux
open Plasma_util.Operators
open Printf

let dlogf = Plasma_util.dlogf

module MCSet = Set.Make (Rpc_proxy.ManagedClient)
module Coord = Plasma_rpcapi_clnt.Make'Coordination(Rpc_proxy.ManagedClient)
module Fsys = Plasma_rpcapi_clnt.Make'Filesystem(Rpc_proxy.ManagedClient)
module Dn = Plasma_rpcapi_clnt.Make'Datanode(Rpc_proxy.ManagedClient)
module Auth = Plasma_rpcapi_clnt.Make'Auth(Rpc_proxy.ManagedClient)
module Util = Plasma_util
module A = Plasma_rpcapi_aux

type errno = Plasma_util.errno
type strmem = [`String of string | `Memory of Netsys_mem.memory]

module I64RangeSet = Plasma_util.RangeSet(Plasma_util.I64)

module ESet = 
  Set.Make(struct type t = unit Uq_engines.engine let compare = compare end )

module StrSet = Plasma_util.StrSet
module I64Map = Plasma_util.I64Map

type dn_connectivity =
    [ `Client of Rpc_proxy.ManagedClient.t
    | `Connecting of Rpc_proxy.ManagedClient.t Uq_engines.engine
    ]

type auth =
    [ `None
    | `Direct of string * string * (string -> string)
	(* (nn_user, dn_user, get_password) *)
    | `Auth_daemon
    | `Auth_ticket
    ]

type plasma_cluster =
    { name : string;
      namenodes_conf : (string * int) list;
      namenodes : Unix.sockaddr array;

      mutable auth : auth;
      mutable auth_ticket : string option; 
              (* for `Auth_daemon and `Auth_ticket *)
      mutable principal : (string * string * string list) option;
      (* (user, group, supp_groups) *)

      mutable default_user_group : string * string;
      (* The default value for user and group when creating new files
	 and directories
       *)

      mutable down : bool;

      mutable pref_nodes : string list;
      mutable inode_pref_nodes : (string list) Plasma_util.I64Map.t;

      mutable params : (string * string) list;  (* from get_params *)
      
      mutable shm_mng : Plasma_shm.shm_manager;

      mutable shm_clean : Plasma_shm.id list;
      (** Id's to clean at close time *)

      mutable filebuf : Plasma_filebuf.buffer_system option;
      (** The buffer system. This is initialized once the blocksize is known
       *)

      mutable coord : Unix.sockaddr;
      (** Current coordinator *)

      mutable find_coord_ser : 
                     (Rpc_proxy.ManagedClient.mclient * string option) 
                        Uq_engines.serializer_t;
      (** Serializer for the coordinator search *)

      mutable next_tid : int64;
      (** next transaction ID *)

      mutable clients : MCSet.t;
      (** Clients in use (for [close_cluster]) *)

      mutable nn_client_opt : Rpc_proxy.ManagedClient.t option;
      (** Current connection to namenode *)

      mutable dn_clients : (Unix.sockaddr, dn_connectivity) Hashtbl.t;
      (** Current connections to datanodes *)

      mutable blocksize : int;
      (** initialized first after the namenode connection is established *)

      mutable inodecache : Plasma_inodecache.ic_manager;
      (** The inodecache for buffered access *)

      mutable n_buffers : int;
      (** maximum number of buffers *)

      mutable flush_ser : unit Uq_engines.serializer_t;
      (** Serializer for all flush operations *)

      mutable flush_scheduled : (int64, unit Uq_engines.engine) Hashtbl.t;
      (** for each inode an engine representing a scheduled flush operation *)

      mutable flush_thread_active : bool;
      (** Whether the loop flushing inodes is running *)

      mutable lazy_trans : (int64, Plasma_inodecache.lazy_transaction *
			           Plasma_inodecache.bl_cache * int ref *
				   float * Unixqueue.group option
			   ) Hashtbl.t;
      (** For an inode: (lt, blc, n_users, timer) *)

      mutable snap_trans : (int64, plasma_trans) Hashtbl.t;
      (** The snapshot transaction for an inode *)

      esys : Unixqueue.event_system
    }

and plasma_trans =
    { cluster : plasma_cluster;
      tid : int64;
      mutable is_open : bool;
      mutable snapshots : int64 list (* inodes *)
    }

type inode = int64

type topology = [`Star|`Chain]

type copy_in_flags = [`No_datasync | `Late_datasync | `Retry]
type copy_out_flags = [ `No_truncate | `Retry ]

exception Plasma_error = Plasma_util.Plasma_error

exception Cluster_down of string

exception Datanode_error of exn


let () =
  Netexn.register_printer (Plasma_error `einval)
    (function
       | Plasma_error code ->
	   "Plasma_error(" ^ Plasma_util.string_of_errno code ^ ")"
       | _ ->
	   assert false
    )


let () =
  Netexn.register_printer (Datanode_error Not_found)
    (function
       | Datanode_error err ->
	   "Datanode_error(" ^ Netexn.to_string err ^ ")"
       | _ ->
	   assert false
    )


let cluster trans =
  trans.cluster

let cluster_namenodes c =
  c.namenodes_conf

let event_system c =
  c.esys

let sync f arg =
  let e = f arg in
  Uq_engines.when_state  (* OK *)
    ~is_error:(fun e ->
		 dlogf
		   "Plasma_client.sync: Exception %s"
		   (Netexn.to_string e))
    ~is_aborted:(fun () ->
		   dlogf "Plasma_client.sync: Aborted")
    e;
  if Unixqueue.is_running e#event_system then
    failwith "Event queue is already processing events";
  Unixqueue.run e#event_system;
  match e#state with
    | `Done v -> v
    | `Error e -> raise e
    | `Aborted -> failwith "Aborted"
    | `Working _ -> assert false

let add_client c mc =
  c.clients <- MCSet.add mc c.clients

let del_client c mc =
  c.clients <- MCSet.remove mc c.clients

let nn_msg_timeout = 300.0

let dn_msg_timeout = 300.0
  (* This is only a fallback value. Normally this is provided by the
     server
   *)

let lock_timeout = 300.0
  (* This is only a fallback value. Normally this is provided by the
     server
   *)

let pause =
  [| 0.01; 0.02; 0.04; 0.08; 0.16; 0.32; 0.64; 1.0;
     2.0; 4.0; 8.0; 16.0; 32.0; 60.0
  |]


let scram_auth_method c user get_pw integrity privacy =
  let client_key_ring =
    ( object
        method password_of_user_name n = get_pw n
        method default_user_name =
	  Some user
      end
    ) in
  let gss_api =
    new Netmech_scram_gssapi.scram_gss_api
      ~client_key_ring
      (Netmech_scram.profile `GSSAPI) in
  let m =
    Rpc_auth_gssapi.client_auth_method
      ~integrity
      ~privacy
      ~user_name_interpretation:(`Plain_name Netgssapi.nt_user_name)
      gss_api
      Netmech_scram_gssapi.scram_mech in
  m


let nn_auth_methods c =
  match c.auth with
    | `None ->
	[Rpc_client.auth_none]
    | `Direct(nn_user,_,get_pw) ->
	[scram_auth_method c nn_user get_pw `Required `Required]
    | `Auth_daemon | `Auth_ticket ->
	( match c.auth_ticket with
	    | None ->
		[Rpc_client.auth_none]
	    | Some ticket ->
		let (user,pw) = Plasma_ug.rpc_login_of_auth_ticket ticket in
		[scram_auth_method c user (fun _ -> pw) `Required `Required]
	)


let dn_auth_methods c =
  let lev =
    try String.lowercase(List.assoc "data_security_level" c.params)
    with _ -> "none" in
  let int =
    if lev = "integrity" then `Required else `None in
  let priv =
    if lev = "privacy" then `Required else `None in
  let need_auth =
    lev <> "none" in
  match c.auth with
    | `None ->
	if need_auth then
	  failwith "Plasma_client: No authentication method configured";
	[Rpc_client.auth_none]
    | `Direct(_,dn_user,get_pw) ->
	[scram_auth_method c dn_user get_pw int priv]
    | `Auth_daemon | `Auth_ticket ->
	( match c.auth_ticket with
	    | None ->
		if need_auth then
		  failwith "Plasma_client: No authentication method configured";
		[Rpc_client.auth_none]
	    | Some ticket ->
		let (user,pw) = Plasma_ug.rpc_login_of_auth_ticket ticket in
		[scram_auth_method c user (fun _ -> pw) int priv]
	)


let create_nn_client ?(msg_timeout=nn_msg_timeout) c conn =
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Plasma_rpcapi_clnt.Coordination.V1._program;
		  Plasma_rpcapi_clnt.Filesystem.V1._program;
		]
      ~msg_timeout
      ~msg_timeout_is_fatal:true
      ~initial_ping:true
      ~auth_methods:(nn_auth_methods c)
      (* no idle timeout - when the connection is closed, the transactions
	 are implicitly aborted
       *)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      c.esys in
  add_client c mclient;
  mclient

let mfac = Hashtbl.create 5

let () =
  Hashtbl.add mfac "readdata" Xdr_mstring.string_based_mstrings


let create_dn_client c conn =
  let msg_timeout =
    try float_of_string(List.assoc "data_timeout" c.params)
    with _ -> dn_msg_timeout in
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Plasma_rpcapi_clnt.Datanode.V1._program ]
      ~msg_timeout
      ~msg_timeout_is_fatal:true
      ~initial_ping:true
      ~idle_timeout:15.0
      ~mstring_factories:mfac
      ~auth_methods:(dn_auth_methods c)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      c.esys in
  add_client c mclient;
  mclient


let create_auth_client c conn =
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Plasma_rpcapi_clnt.Auth.V1._program ]
      ~msg_timeout:60.0
      ~msg_timeout_is_fatal:true
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      c.esys in
  add_client c mclient;
  mclient


let shutdown_client c mc =
  Rpc_proxy.ManagedClient.trigger_shutdown mc (fun () -> del_client c mc)


let open_cluster name nodes esys =
  if nodes = [] then invalid_arg "open_cluster";
  let namenodes =
    Array.of_list
      (List.map
	 (fun (h,p) -> Util.sockaddr_of_host_port (h,p))
	 nodes
      ) in
  let init_tid =
    (* for better debugging only! *)
    10 * (Plasma_rng.random_int 65536) in
  { name = name;
    namenodes = namenodes;
    namenodes_conf = nodes;
    auth = `None;
    auth_ticket = None;
    principal = None;
    default_user_group = ("","");
    down = false;
    pref_nodes = [];
    inode_pref_nodes = I64Map.empty;
    shm_mng = new Plasma_shm.null_shm_manager();
    shm_clean = [];
    coord = namenodes.(0);
    find_coord_ser = Uq_engines.serializer esys;
    next_tid = Int64.of_int init_tid;
    blocksize = 0;
    clients = MCSet.empty;
    nn_client_opt = None;
    dn_clients = Hashtbl.create 5;
    filebuf = None;
    n_buffers = 1;
    inodecache = Plasma_inodecache.create_manager esys;
    flush_ser = Uq_engines.serializer esys;
    flush_scheduled = Hashtbl.create 13;
    flush_thread_active = false;
    lazy_trans = Hashtbl.create 3;
    snap_trans = Hashtbl.create 3;
    esys = esys;
    params = [];
  }

let open_cluster_cc (cfg : Plasma_client_config.client_config) esys =
  let nodes =
    List.map Plasma_util.parse_host_port cfg#nn_nodes in
  open_cluster cfg#clustername nodes esys


let open_cluster_like ?(same_buffer=false) ?(same_pref_nodes=false)
                      ?(same_shm_manager=false)
                      c_orig esys =
  let c =
    open_cluster c_orig.name c_orig.namenodes_conf esys in
  c.blocksize <- c_orig.blocksize;
  c.params <- c_orig.params;
  c.auth <- c_orig.auth;
  c.auth_ticket <- c_orig.auth_ticket;
  c.default_user_group <- c_orig.default_user_group;
  if same_buffer then
    c.n_buffers <- c_orig.n_buffers;
  if same_pref_nodes then (
    c.pref_nodes <- c_orig.pref_nodes;
    c.inode_pref_nodes <- c_orig.inode_pref_nodes;
  );
  if same_shm_manager then
    c.shm_mng <- c_orig.shm_mng;
  c


let abort_cluster c =
  dlogf "Plasma_client.abort_cluster";
  MCSet.iter
    (fun mc -> Rpc_proxy.ManagedClient.shut_down mc)
    c.clients;
  c.clients <- MCSet.empty;
  c.nn_client_opt <- None;
  if c.auth = `Auth_daemon then c.auth_ticket <- None;
  Hashtbl.clear c.dn_clients

let close_cluster c =
  if not c.down then (
    c.down <- true;
    List.iter (fun id -> c.shm_mng # book_allocation id 0) c.shm_clean;
    c.shm_clean <- [];
    abort_cluster c
  )

let configure_buffer c n =
  c.n_buffers <- n

let configure_pref_nodes c l =
  c.pref_nodes <- l

let configure_pref_nodes_of_inode c inode l_opt =
  match l_opt with
    | Some l ->
	c.inode_pref_nodes <- I64Map.add inode l c.inode_pref_nodes
    | None ->
	c.inode_pref_nodes <- I64Map.remove inode c.inode_pref_nodes

let get_pref_nodes c inode =
  try
    I64Map.find inode c.inode_pref_nodes
  with
    | Not_found -> c.pref_nodes

let configure_shm_manager c m =
  c.shm_mng <- m

let configure_auth c nn_user dn_user get_pw =
  c.auth <- `Direct(nn_user,dn_user,get_pw);
  c.auth_ticket <- None

let configure_auth_daemon c =
  c.auth <- `Auth_daemon;
  c.auth_ticket <- None

let configure_auth_ticket c ticket =
  c.auth <- `Auth_ticket;
  c.auth_ticket <- Some ticket

let configure_default_user_group c u g =
  c.default_user_group <- (u,g)

let shm_manager c =
  c.shm_mng

let cluster_name c = c.name

let mc_info mc =
  let s =
    match Rpc_proxy.ManagedClient.mclient_state mc with
      | `Down -> "down"
      | `Connecting -> "connecting"
      | `Up None -> "up(n/a)"
      | `Up (Some addr) -> "up(" ^ Netsys.string_of_sockaddr addr ^ ")" in
  sprintf "mclient(serial=%d,state=%s)"
    (Rpc_proxy.ManagedClient.mclient_serial mc) s


let extract v =
  match v with
    | `ok x -> x
    | #errno as code -> raise (Plasma_error code)

let extract_unit v =
  match v with
    | `ok -> ()
    | #errno as code -> raise (Plasma_error code)

let no_extract v = v

let fextract_map f =
  function
    | `Done (`ok x) -> `Done (f x)
    | `Done (#errno as code) -> `Error(Plasma_error code)
    | `Error e -> `Error e
    | `Aborted -> `Aborted

let fextract g = fextract_map (fun x -> x) g

let fextract_map_unit x =
  function
    | `Done `ok -> `Done x
    | `Done (#errno as code) -> `Error(Plasma_error code)
    | `Error e -> `Error e
    | `Aborted -> `Aborted

let fextract_unit = fextract_map_unit ()

let plasma_error_e c code =
  eps_e (`Error (Plasma_error code)) c.esys


let filebuf c =
  if c.down then
    failwith "Plasma_client: already closed";
  match c.filebuf with
    | Some fb -> fb
    | None ->
	if c.blocksize=0 then
	  failwith "Plasma_client: blocksize is not known yet (init problem)";
	let shm = shm_manager c in
	let id = shm # new_id `Buf in
	shm # book_allocation id (c.n_buffers * c.blocksize);
	c.shm_clean <- id :: c.shm_clean;
	let fb = 
	  Plasma_filebuf.create_buffer_system c.n_buffers c.blocksize c.esys in
	c.filebuf <- Some fb;
	fb


let auto_auth_ticket_e c =
  (** Return the authentication ticket to use, if any. The ticket can
      either be set in [c], or it can be obtained by contacting the
      authentication daemon.
   *)
  match c.auth with
    | `Auth_daemon ->
	let socket_name = "/tmp/plasma_auth-" ^ c.name ^ ".rpc" in
	if Sys.file_exists socket_name then (
	  let mc = create_auth_client c (Rpc_client.Unix socket_name) in
	  Plasma_util.failsafe_e
	    (Util.rpc_engine mc Auth.V1.get_auth_ticket'async ())
	  ++ (fun st ->
		shutdown_client c mc;
		match st with
		  | `Done ticket_opt ->
		      eps_e (`Done ticket_opt) c.esys
		  | `Error e ->
		      Netlog.logf `Warning "get_auth_ticket_e: Exception %s" 
			(Netexn.to_string e);
		      eps_e (`Done None) c.esys
		  | `Aborted ->
		      eps_e `Aborted c.esys
	     )
	)	  
	else
	  eps_e (`Done None) c.esys

    | `Auth_ticket ->
	eps_e (`Done c.auth_ticket) c.esys
  
    | _ ->
	eps_e (`Done None) c.esys


let find_coord_e c =
  (** Find the coordinator. [coord] is our first attempt. Returns the
      new client.

      On success, tries also to create a client for the inodecache.
   *)

  let down_ic() =
    Plasma_inodecache.disable c.inodecache in

  let rec search_live_nn_e addr k_next k_lim round =
    (** First try [addr] and submit a [find_coordinator]. If we get a
	response, everything is fine and we stop the search. Otherwise
	go on with [namenodes.(k_next)], but fail when [k=k_lim] 
     *)
    let conn = Util.connector_of_sockaddr addr in
    let mc = create_nn_client ~msg_timeout:20.0 c conn in
    Plasma_util.failsafe_e
      (Util.rpc_engine mc Coord.V1.find_coordinator'async c.name)
    ++ (fun st ->
	  shutdown_client c mc;
	  match st with
	    | `Done (Some hp) ->
		let (h,p) = Plasma_util.parse_host_port hp in
		let caddr = Plasma_util.sockaddr_of_host_port (h,p) in
		c.coord <- caddr;
		eps_e (`Done caddr) c.esys
	    | `Done None ->
		search_next_e k_next k_lim round
	    | `Error e ->
		Netlog.logf `Warning "find_coord_e: Exception %s" 
		  (Netexn.to_string e);
		search_next_e k_next k_lim round
	   | `Aborted ->
	       eps_e `Aborted c.esys
       )
  and search_next_e k_next k_lim round =
    if k_next = k_lim && round=3 then (
      eps_e
	(`Error (Cluster_down "find_coord_e: cannot find coordinator"))
	c.esys
    ) else (
      let addr = c.namenodes.(k_next) in
      let k' = (k_next+1) mod (Array.length c.namenodes) in
      if k' = k_lim then (
	Uq_engines.delay_engine 
	  1.0
	  (fun () -> search_live_nn_e addr k' k_lim (round+1))
	  c.esys
      )
      else
	search_live_nn_e addr k' k_lim round
    )
  in

  if c.down then
    failwith "Plasma_client: already closed";
  down_ic();
  search_live_nn_e c.coord 0 0 1
  ++ (fun addr ->
	let conn = Util.connector_of_sockaddr addr in
	let mc = create_nn_client c conn in
	Plasma_inodecache.enable c.inodecache mc;
	eps_e (`Done mc) c.esys
     )


let with_nn_session_e c f =
  (** Run [f mc] for a managed client [mc] of the namenode *)
  if c.down then
    failwith "Plasma_client: already closed";
  match c.nn_client_opt with
    | Some mc when Rpc_proxy.ManagedClient.mclient_state mc <> `Down ->
	f mc
    | _ ->
	c.nn_client_opt <- None;
	if c.auth = `Auth_daemon then c.auth_ticket <- None;
	c.find_coord_ser # serialized
	  (fun _ ->
	     match c.nn_client_opt with
	       | Some mc ->
		   eps_e (`Done (mc,None)) c.esys
	       | _ ->
		   auto_auth_ticket_e c
		   ++ (fun ticket_opt ->
			 c.auth_ticket <- ticket_opt;
			 find_coord_e c 
			 ++ (fun mc ->
			       c.nn_client_opt <- Some mc;
			       Util.rpc_engine mc Fsys.V1.get_params'async ()
			       ++ (fun params ->
				     c.params <- 
				       List.map
				         (fun p -> (p.A.name, p.A.value))
				         (Array.to_list params);
				     let size =
				       int_of_string
					 (List.assoc "blocksize" c.params) in
				     if c.blocksize = 0 then
				       c.blocksize <- size;
				     if c.blocksize <> size then (
				       Netlog.logf `Crit
					 "After reconnecting the namenode reports a \
                                    different blocksize";
				       eps_e
					 (`Error (Failure "blocksize mismatch")) 
					 c.esys
				     )
				     else 
				       eps_e (`Done (mc,ticket_opt)) c.esys
				  )
			    )
		      )
	  )
	++ (fun (mc,ticket_opt) ->
	      dlogf
		"with_nn_session: %s" (mc_info mc);
	      dlogf
		"auth ticket: %s" 
		(match ticket_opt with None -> "-" | Some s -> s);
	      match ticket_opt with
		| None -> 
		    c.principal <- None;
		    f mc
		| Some ticket ->
		    let (user,group,supp_groups_set) =
		      Plasma_ug.principal_of_auth_ticket ticket in
		    let sg = StrSet.elements supp_groups_set in
		    let supp_groups =
		      Array.of_list sg in
		    let delete_flag =
		      c.auth = `Auth_daemon in
		    Util.rpc_engine mc Fsys.V1.impersonate'async
		      (user, group, supp_groups, Some ticket, delete_flag)
		    ++ (fun success ->
			  c.principal <- Some(user,group,sg);
			  if success then
			    f mc
			  else
			    eps_e (`Error(Plasma_error `eaccess)) c.esys
		       )
	   )


let dn_session_e c (h,p) =
  (** Create/continue a session to the datanode [hp], and start the engine
      [f mc]. Returns the engine.
   *)
  if c.down then
    failwith "Plasma_client: already closed";
  let saddr = Util.sockaddr_of_host_port (h,p) in
  let mc_conn =
    try
      Some(Hashtbl.find c.dn_clients saddr)
    with Not_found -> None in
  match mc_conn with
    | Some (`Client mc) 
	when Rpc_proxy.ManagedClient.mclient_state mc <> `Down ->
	eps_e (`Done mc) c.esys
    | Some (`Connecting e) ->
	e
    | _ ->
	Hashtbl.remove c.dn_clients saddr;
	let conn = Util.connector_of_sockaddr saddr in
	let mc = create_dn_client c conn in
	let e = 
	  Util.rpc_engine mc Dn.V1.udsocket_if_local'async ()
	  ++ (function
		| None ->
		    Hashtbl.replace c.dn_clients saddr (`Client mc);
		    dlogf
		      "Datanode connection to %s:%d established" h p;
		    eps_e (`Done mc) c.esys
		| Some path ->
		    shutdown_client c mc;
		    let conn2 = Rpc_client.Unix path in
		    let mc2 = create_dn_client c conn2 in
		    Hashtbl.replace c.dn_clients saddr (`Client mc2);
		    dlogf
		      "Local datanode connection to %s:%d established" h p;
		    eps_e (`Done mc2) c.esys
	     ) in
	Hashtbl.replace c.dn_clients saddr (`Connecting e);
	e

let current_user_e c =
  with_nn_session_e c
    (fun mc ->
       match c.principal with
	 | None ->
	     eps_e (`Error (Plasma_error `efailed)) c.esys
	 | Some(u,g,sg) ->
	     eps_e (`Done (u,g,sg)) c.esys
    )

let current_user c = sync current_user_e c


let lazy_transaction c mc =
  (* A lazily created transaction for read-only accesses (non-snapshot case) *)
  let ser = Uq_engines.serializer c.esys in
  ( object(self)
      val mutable state = None
      val mutable starting = None
      val mutable stopping = None

      method serialized
        : (unit -> unit Uq_engines.engine) -> unit Uq_engines.engine =
        fun f ->
          ser # serialized (fun _ -> f())

      method get_tid_e() =
	try
	  match state with
	    | Some(tid, serial) ->
		if Rpc_proxy.ManagedClient.mclient_serial mc = serial then
		  eps_e (`Done tid) c.esys
		else
		  raise Not_found
	    | None ->
		raise Not_found
	with 
	  | Not_found ->
	      if c.down then
		failwith "Plasma_client: already closed";
	      ( match starting with
		  | Some e -> e
		  | None ->
		      let e = self#new_tid_e() in
		      starting <- Some e;
		      e
	      )

      method private new_tid_e() =
	let tid = c.next_tid in
	c.next_tid <- Int64.succ tid;
	dlogf
	  "lazy_transaction: tid=%Ld %s" tid (mc_info mc);
	Util.rpc_engine mc Fsys.V1.begin_transaction'async tid
	++
	  (function
	     | `ok ->
		 state <- Some(tid, Rpc_proxy.ManagedClient.mclient_serial mc);
		 starting <- None;
		 stopping <- None;
		 eps_e (`Done tid) c.esys
	     | #errno as e ->
		 dlogf
		   "lazy_transaction: start error tid=%Ld %s exn: %s"
		   tid (mc_info mc) (Plasma_util.string_of_errno e);
		 starting <- None;
		 stopping <- None;
		 eps_e (`Error (Plasma_error e)) c.esys
	  )
		
      method get_ii_e inode force_flag =
	if force_flag then
	  self#get_tid_e()
	  ++ (fun tid ->
		Plasma_inodecache.get_ii_e c.inodecache mc tid inode
	     )
	  >> fextract
	else
	  Plasma_inodecache.get_ii_via_cache_e c.inodecache mc self inode
	  >> fextract
	    
      method invalidate_ii inode =
	Plasma_inodecache.invalidate_ii c.inodecache inode

      method snapshot = false

      method release_e() =
	match state with
	  | Some _ ->
	      self # drop_tid_e ()
	  | None ->
	      ( match starting with
		  | None -> eps_e (`Done ()) c.esys
		  | Some e -> e ++ (fun _ -> self # drop_tid_e ())
	      )

      method private drop_tid_e () =
	match state with
	  | None -> assert false
	  | Some(tid, serial) ->
	      if Rpc_proxy.ManagedClient.mclient_serial mc = serial then (
		match stopping with
		  | Some e -> e
		  | None ->
		      dlogf
			"lazy_transaction: dropping tid=%Ld %s"
			tid (mc_info mc);
		      let e = 
			Util.rpc_engine
			  mc Fsys.V1.abort_transaction'async tid
			++ (function
			      | `ok -> 
				  state <- None;
				  stopping <- None;
				  eps_e (`Done()) c.esys
			      | #errno as e ->
				  dlogf
				    "lazy_transaction: abort error \
                                     tid=%Ld %s exn: %s"
				    tid (mc_info mc)
				    (Plasma_util.string_of_errno e);
				  state <- None;
				  stopping <- None;
				  eps_e (`Done()) c.esys
			   ) in
		      stopping <- Some e;
		      e
	      )
	      else (
		state <- None;
		eps_e (`Done()) c.esys
	      )

      method event_system = c.esys
    end
  )


(* The idea here is reuse the current pair (lt, blc) within some limits.
   n_users counts the number of users. When this number drops to 0, it
   is possible to release lt.

   For better concurrency, we delay the release of lt for some small
   period (0.1 seconds). If another uses appears in that time, lt
   continues to exist.

   However, there is some absolute limit for lt. After at most 10 seconds
   we release it. We do this because (1) blc may grow too much otherwise,
   and (2) every block in blc is also pinned in the server (consuming
   disk space).

   If snapshots are enabled for inode, we never time out.
 *)

let with_lazy_transaction_e c mc inode f =
  let now = Unix.gettimeofday() in
  let (lt, blc, n_users, t_create, g_tmo_opt) =
    try
      let (lt, blc, n_users, t_create, g_tmo_opt) =
	Hashtbl.find c.lazy_trans inode in
      if not lt#snapshot && now > t_create +. 10. then raise Not_found;
      (lt, blc, n_users, t_create, g_tmo_opt)
    with Not_found ->
      let lt = lazy_transaction c mc in
      let blc = Plasma_inodecache.create_bl_cache mc lt inode in
      let n_users = ref 0 in
      let t_create = now in
      Hashtbl.add c.lazy_trans inode (lt, blc, n_users, t_create, None);
      (lt, blc, n_users, t_create, None) in
  ( match g_tmo_opt with
      | None -> ()
      | Some g_tmo -> 
	  Unixqueue.clear c.esys g_tmo ;
	  Hashtbl.replace c.lazy_trans inode (lt, blc, n_users, t_create, None)
  );
  incr n_users;
  let out_state = ref `Aborted in
  (* We need to serialize the execution of f, because only one user can
     submit an fsys RPC call at a time
   *)
  lt#serialized
    (fun () ->
       Uq_engines.meta_engine(f lt blc)
       >> (fun state -> out_state := state; `Done ())
    )
  ++ (fun () ->
	(* N.B. the contents of c.lazy_trans may be different now *)
	decr n_users;
	( if !n_users = 0 && not lt#snapshot then (
	    let g_tmo = Unixqueue.new_group c.esys in
	    ( try 
		let (lt1, _, _, _, _) = Hashtbl.find c.lazy_trans inode in
		if lt1 <> lt then raise Not_found;
		Hashtbl.replace c.lazy_trans 
		  inode (lt, blc, n_users, t_create, (Some g_tmo));
	      with Not_found -> ()
	    );
	    Unixqueue.weak_once c.esys g_tmo 0.1
	      (fun () ->
		 Hashtbl.remove c.lazy_trans inode;
		 ignore(lt#release_e())
	      )
	  );
	  (eps_e (`Done ()) c.esys)
	)
     )
  >> (fun _ -> 
        match !out_state with
          | `Done st -> st
          | `Error e -> `Error e
          | `Aborted -> `Aborted
     )


let with_new_nn_transaction_e c mc extract f =
  (** Start a new transaction on [mc], and run the engine returned by
      [f mc trans],
      and commit the transaction if this engine is successful. Otherwise
      abort it. [tid] is the transaction ID.

      The result value is extracted via [extract].
   *)
  (* TODO: option so that this transaction is repeated on ECONFLICT *)
  if c.down then
    failwith "Plasma_client: already closed";
  let tid = c.next_tid in
  c.next_tid <- Int64.succ tid;
  dlogf
    "with_new_nn_transaction: tid=%Ld %s" tid (mc_info mc);
  Util.rpc_engine mc Fsys.V1.begin_transaction'async tid
  ++
    (function
       | `ok ->
	   let trans =
	     { cluster = c;
	       tid = tid;
	       is_open = true;
	       snapshots = [];
	     } in
	   Plasma_util.failsafe_e (f mc trans)
	   ++ (fun st ->
		 trans.is_open <- false;
		 try
		   match st with
		     | `Done x ->
			 let x' = extract x in
			 new Uq_engines.map_engine
			   ~map_done:(function
					| `ok -> `Done x'
					| #errno as e -> 
					    dlogf
					      "with_new_nn_transaction: \
                                                commit error tid=%Ld %s exn: %s"
					      tid (mc_info mc) 
					      (Plasma_util.string_of_errno e);
					    `Error(Plasma_error e)
				     )
			   (Util.rpc_engine 
			      mc Fsys.V1.commit_transaction'async tid)
		     | `Error err ->
			 raise err
		     | `Aborted ->
			 eps_e `Aborted c.esys
		 with
		   | err ->
		       (** Here is the risk that for abort_transaction a
			   new connection is created by ManagedClient. Nonsense.
			   Check if [mc] is already down, and do not open it
			   for only aborting.
			*)
		       dlogf
			 "with_new_nn_transaction: abort tid=%Ld %s exn: %s"
			 tid (mc_info mc) (Netexn.to_string err);
		       let st1 = `Error err in
		       ( match Rpc_proxy.ManagedClient.mclient_state mc with
			   | `Down ->
			       eps_e st1 c.esys
			   | _ ->
			       new Uq_engines.map_engine
				 ~map_done:(fun _ -> st1)
				 (Util.rpc_engine
				    mc Fsys.V1.abort_transaction'async tid)
		       )
	      )
       | #errno as e ->
	    dlogf
	      "with_new_nn_transaction: start error tid=%Ld %s exn: %s"
	      tid (mc_info mc) (Plasma_util.string_of_errno e);
	   eps_e (`Error (Plasma_error e)) c.esys
    )

let impersonate_e c user group sg ticket_opt =
  with_nn_session_e c
    (fun mc ->
       let sga = Array.of_list sg in
       ( Util.rpc_engine mc Fsys.V1.impersonate'async
	   (user,group,sga,ticket_opt,false)
	 ++ (fun ok ->
	       if ok then (
		 c.principal <- Some(user,group,sg);
		 eps_e (`Done()) c.esys
	       )
	       else plasma_error_e c `eperm
	    )
       )
    )
  
let impersonate c user group sg ticket_opt =
  sync (impersonate_e c user group sg) ticket_opt
    
let get_auth_ticket_e c user =
  with_nn_session_e c
    (fun mc ->
       ( Util.rpc_engine mc Fsys.V1.get_auth_ticket'async user
	 ++ (fun t_opt ->
	       match t_opt with
		 | None -> plasma_error_e c `eperm
		 | Some t -> eps_e (`Done t) c.esys
	    )
       )
    )

let get_auth_ticket c user = sync (get_auth_ticket_e c) user


(* ---- transaction statements ---- *)

let start_e c =
  with_nn_session_e c
    (fun mc ->
       let tid = c.next_tid in
       c.next_tid <- Int64.succ tid;
       let trans =
	 { cluster = c;
	   tid = tid;
	   is_open = true;
	   snapshots = [];
	 } in
       Util.rpc_engine mc Fsys.V1.begin_transaction'async tid
       ++ (function
	     | `ok ->
		 eps_e (`Done trans) c.esys
	     | #errno as code ->
		 plasma_error_e c code
	  )
    )

let start trans = sync start_e trans

let cancel_snapshots trans drop_flag =
  List.iter
    (fun inode ->
       try
	 let (lt, _, _, _, _) = Hashtbl.find trans.cluster.lazy_trans inode in
	 assert(lt#snapshot);
	 Hashtbl.remove trans.cluster.lazy_trans inode;
	 Hashtbl.remove trans.cluster.snap_trans inode;
	 if drop_flag then
	   let fb = filebuf trans.cluster in
	   Plasma_filebuf.schedule_drop_inode fb inode
       with Not_found -> assert false
    )
    trans.snapshots


let abort_e trans =
  if not trans.is_open then
    eps_e (`Done()) trans.cluster.esys
  else (
    trans.is_open <- false;
    cancel_snapshots trans true;
    with_nn_session_e trans.cluster
      (fun mc ->
	 Util.rpc_engine mc Fsys.V1.abort_transaction'async trans.tid
	 >> fextract_unit
      )
  )

let abort trans = sync abort_e trans

let commit_e trans =
  if not trans.is_open then
    plasma_error_e trans.cluster `enotrans
  else (
    with_nn_session_e trans.cluster
      (fun mc ->
	 Util.rpc_engine mc Fsys.V1.commit_transaction'async trans.tid
	 >> (fun st ->
	       ( match st with
		   | `Done _ ->
		       trans.is_open <- false;
		       cancel_snapshots trans false;
		   | _ ->
		       ()
	       );
	       st
	    )
	 >> fextract_unit
      )
  )

let commit trans = sync commit_e trans

let blocksize_e c =
  if c.blocksize <> 0 then
    eps_e (`Done c.blocksize) c.esys
  else (
    with_nn_session_e c
      (fun mc ->
	 eps_e (`Done c.blocksize) c.esys
      )
  )

let blocksize c = sync blocksize_e c

let params_e c =
  if c.params <> [] then
    eps_e (`Done c.params) c.esys
  else (
    with_nn_session_e c
      (fun mc ->
	 eps_e (`Done c.params) c.esys
      )
  )

let params c = sync params_e c


let fsstat_e c =
  with_nn_session_e c
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.get_fsstat'async ()
       >> fextract
    )

let fsstat c = sync fsstat_e c


let local_identities_e c =
  with_nn_session_e c
    (fun mc ->
       Util.rpc_engine mc Coord.V1.local_identities'async c.name
       ++ (fun l -> eps_e (`Done(Array.to_list l)) c.esys)
    )

let local_identities c = sync local_identities_e c


let get_dn_info_e c =
  with_nn_session_e c
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.get_dn_info'async ()
    )

let get_dn_info c = sync get_dn_info_e c


let snapshot_info trans inode =
  let (lt, blc, _, _, _) = Hashtbl.find trans.cluster.lazy_trans inode in
  if not lt#snapshot then raise Not_found;
  if not (List.mem inode trans.snapshots) then raise Not_found;
  (lt, blc)


let get_inodeinfo_e trans inode =
  with_nn_session_e trans.cluster
    (fun mc ->
       try
	 (* Catch the snapshot case: *)
	 let (lt, _) = snapshot_info trans inode in
	 lt # get_ii_e inode true
       with
	 | Not_found ->
	     (* We do not call get_inodeinfo directly, but via this cache function.
		This function gets the inodeinfo struct directly from the server
		(not from the cache), but puts it into the cache if possible.
		So the cache can learn
	      *)
	     Plasma_inodecache.get_ii_e trans.cluster.inodecache mc trans.tid inode
	     >> fextract
    )

let get_inodeinfo trans inode = sync(get_inodeinfo_e trans) inode


let get_cached_inodeinfo_e c inode refresh_flag =
  with_nn_session_e c
    (fun mc ->
       with_lazy_transaction_e c mc inode
	 (fun lt blc ->
	    if refresh_flag then
	      lt # get_tid_e()
	      ++ (fun tid ->
		    Plasma_inodecache.get_ii_e c.inodecache mc tid inode
		    >> fextract
		 )
	    else
	      Plasma_inodecache.get_ii_via_cache_e c.inodecache mc lt inode
	      >> fextract
	 )
    )

let get_cached_inodeinfo c inode refresh_flag = 
  sync(get_cached_inodeinfo_e c inode) refresh_flag


let invalidate_ii trans inode =
  (* Catch here the snapshot case *)
  ( try
      let (lt, _) = snapshot_info trans inode in
      lt # invalidate_ii inode
    with
      | Not_found -> ()
  );
  Plasma_inodecache.invalidate_ii trans.cluster.inodecache inode


let set_inodeinfo_e trans inode ii =
  with_nn_session_e trans.cluster
    (fun mc ->
       ( Util.rpc_engine mc Fsys.V1.update_inodeinfo'async (trans.tid,inode,ii)
	 >> fextract_unit
       )
       ++ (fun () ->
	     invalidate_ii trans inode;
	     eps_e (`Done ()) trans.cluster.esys
	  )
    )

let set_inodeinfo trans inode ii = sync(set_inodeinfo_e trans inode) ii


let free_blocks_e trans inode index length set_mtime = (* not exported *)
  with_nn_session_e trans.cluster
    (fun mc ->
       ( Util.rpc_engine mc Fsys.V1.free_blocks'async
	   (trans.tid,inode,index,length,set_mtime)
	 >> fextract_unit
       )
       ++ (fun () ->
	     (* Catch here the snapshot case *)
	     ( try
		 let (lt, blc) = snapshot_info trans inode in
		 Plasma_inodecache.forget_blocks blc index length
	       with
		 | Not_found -> ()
	     );
	     eps_e (`Done ()) trans.cluster.esys
	  )
    )

let truncate_e trans inode eof =
  let c = trans.cluster in
  get_inodeinfo_e trans inode
  ++ (fun ii ->
	let new_block_eof = 
	  Int64.succ
	    (Int64.div
	       (Int64.pred eof) (Int64.of_int c.blocksize)) in
	set_inodeinfo_e trans inode { ii with eof = eof }
	++ ( fun () ->
	       if ii.blocklimit > new_block_eof then
		 free_blocks_e
		   trans inode new_block_eof 
		   (Int64.sub ii.blocklimit new_block_eof)
		   false
	       else
		 eps_e (`Done()) c.esys
	   ) 
     )

let truncate trans inode eof =
  sync (truncate_e trans inode) eof


let get_blocklist_e trans inode block n keep_flag =
  with_nn_session_e trans.cluster
    (fun mc ->
       ( try
	   let (_, _) = snapshot_info trans inode in
	   (* TODO (very low prio) *)
	   failwith "Plasma_client.get_blocklist: unimplemented for \
                     snapshot transactions"
	 with
	   | Not_found -> ()
       );
       ( Util.rpc_engine mc Fsys.V1.get_blocks'async
	   (trans.tid,inode,block,n,0L,keep_flag)
	 >> fextract
       ) ++ (fun l -> eps_e (`Done(Array.to_list l)) trans.cluster.esys)
    )

let get_blocklist trans inode block n keep_flag = 
  sync(get_blocklist_e trans inode block n) keep_flag


let create_inode_e trans ii =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.allocate_inode'async (trans.tid,ii)
       >> fextract
    )

let create_inode trans ii = sync(create_inode_e trans) ii


let delete_inode_e trans inode =
  invalidate_ii trans inode;
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.delete_inode'async (trans.tid,inode)
       >> fextract_unit
    )

let delete_inode trans inode = sync (delete_inode_e trans) inode


let lookup_e trans name symbolic =
  (*
  if name = "" || name.[0] <> '/' then
    invalid_arg "Plasma_client.lookup";
   *)
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.lookup'async (trans.tid,(-1L),name,symbolic)
       >> fextract
    )

let lookup trans name symbolic = sync (lookup_e trans name) symbolic

let dir_lookup_e trans dir_inode name symbolic =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.lookup'async
	 (trans.tid,dir_inode,name,symbolic)
       >> fextract
    )

let dir_lookup trans dir_inode name symbolic =
  sync (dir_lookup_e trans dir_inode name) symbolic

let rev_lookup_e trans inode =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.rev_lookup'async (trans.tid,inode)
       >> fextract_map Array.to_list
    )

let rev_lookup trans inode = sync (rev_lookup_e trans) inode

let rev_lookup_dir_e trans inode =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.rev_lookup_dir'async (trans.tid,inode)
       >> fextract
    )

let rev_lookup_dir trans inode = sync (rev_lookup_dir_e trans) inode

let namelock_e trans inode name = 
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.namelock'async (trans.tid,inode,name)
       >> fextract_unit
    )

let namelock trans inode name = sync (namelock_e trans inode) name

let link_count_e trans inode =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.link_count'async (trans.tid,inode)
       >> fextract
    )

let link_count trans inode = sync (link_count_e trans) inode

let link_e trans name inode =
(*
  if name = "" || name.[0] <> '/' then
    invalid_arg "Plasma_client.link";
 *)
  invalidate_ii trans inode;
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.link'async (trans.tid,name,inode)
       >> fextract_unit
    )

let link trans name inode = sync (link_e trans name) inode

let link_at_e trans dir_inode name inode =
  invalidate_ii trans inode;
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.link_at'async (trans.tid,dir_inode,name,inode)
       >> fextract_unit
    )

let link_at trans dir_inode name inode = 
  sync (link_at_e trans dir_inode name) inode

let unlink_e trans name =
(*
  if name = "" || name.[0] <> '/' then
    invalid_arg "Plasma_client.unlink";
 *)
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.unlink'async (trans.tid,name)
       >> fextract_unit
    )

let unlink trans name = sync (unlink_e trans) name

let unlink_at_e trans dir_inode name =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.unlink_at'async (trans.tid,dir_inode,name)
       >> fextract_unit
    )

let unlink_at trans dir_inode name = sync (unlink_at_e trans dir_inode) name

let rename_e trans old_path new_path =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.rename'async (trans.tid,old_path,new_path)
       >> fextract_unit
    )

let rename trans old_path new_path = sync (rename_e trans old_path) new_path

let rename_at_e trans old_dir_inode old_name new_dir_inode new_name =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.rename_at'async 
	 (trans.tid,old_dir_inode,old_name,new_dir_inode,new_name)
       >> fextract_unit
    )

let rename_at trans  old_dir_inode old_name new_dir_inode new_name =
  sync (rename_at_e trans old_dir_inode old_name new_dir_inode) new_name 


let unwrap_entries entries =
  Array.map
    (fun ent ->
       (ent.entry_name, ent.entry_inode)
    )
    entries

let list_inode_e trans inode =
  with_nn_session_e trans.cluster
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.list'async (trans.tid,inode)
       >> fextract_map (fun x -> Array.to_list(unwrap_entries x))
    )


let list_inode trans inode = sync (list_inode_e trans) inode

let list_e trans name =
(*
  if name = "" || name.[0] <> '/' then
    invalid_arg "Plasma_client.list";
 *)
  with_nn_session_e trans.cluster
    (fun mc ->
       ( Util.rpc_engine mc Fsys.V1.lookup'async (trans.tid,(-1L),name,false)
	 >> fextract
       ) 
       ++ (fun inode ->
	     Util.rpc_engine mc Fsys.V1.list'async (trans.tid,inode)
	     >> fextract_map (fun x -> Array.to_list(unwrap_entries x))
	  )
    )

let list trans name = sync (list_e trans) name

let create_file_e trans name ii =
  if ii.filetype = `ftype_directory then
    invalid_arg "Plasma_client.create_file";
  with_nn_session_e trans.cluster
    (fun mc ->
       ( Util.rpc_engine mc Fsys.V1.allocate_inode'async (trans.tid,ii)
	 >> fextract
       )
       ++ (fun inode ->
	     Util.rpc_engine mc Fsys.V1.link'async (trans.tid,name,inode)
	     >> fextract_map_unit inode
	  )
    )
  (* CHECK: if the file name already exists, the inode is created nevertheless.
     It's an inode without name, so it will be deleted when the transaction
     finishes. This strange behaviour is invisible for the user, so we don't
     do anything right now about it.
   *)

let create_file trans name ii = sync (create_file_e trans name) ii

let mkdir_e trans name ii =
  if ii.filetype <> `ftype_directory then
    invalid_arg "Plasma_client.mkdir";
  with_nn_session_e trans.cluster
    (fun mc ->
       ( Util.rpc_engine mc Fsys.V1.allocate_inode'async (trans.tid,ii)
	 >> fextract
       )
       ++ (fun inode ->
	     Util.rpc_engine mc Fsys.V1.link'async (trans.tid,name,inode)
	     >> fextract_map_unit inode
	  )
    )

let mkdir trans name ii = sync (mkdir_e trans name) ii


let create_ii c ftype mode field1 =
  let t = Unix.gettimeofday() in
  let t1 = Int64.of_float t in
  let t2 = Pervasives.truncate ((t -. Int64.to_float t1) *. 1E9) in
  let (u,g) = c.default_user_group in
  { filetype = ftype;
    usergroup = { user = u; group = g };
    mode = mode;
    eof = 0L;
    replication = 0;  (* means: server takes default *)
    blocklimit = 0L;
    mtime = { tsecs = t1; tnsecs = t2 };
    ctime = { tsecs = t1; tnsecs = t2 };
    field1 = field1;
    seqno = 0L;
    committed = false;
    create_verifier = 0L;
    anonymous = false;
  }
 
let regular_ii c mode =
  create_ii c `ftype_regular (mode land 0o755) ""

(* TODO: use process umask. Problem: Unix.umask does not allow to only
   query the umask
 *)

let symlink_ii c dest =
  create_ii c `ftype_symlink 0o777 dest

let dir_ii c mode =
  create_ii c `ftype_directory (mode land 0o755) ""



(* ----------------- buffered access: ------------------- *)

let wrbuffer_e c b blocklist seqno =
  (** Writes buffer b out. This engine represents the [`Writing] state *)
  let blocksize = Bigarray.Array1.dim b.Plasma_filebuf.buf in

  let write_block_e block =
    assert(block.length = 1L);
    let ms =
      Xdr_mstring.memory_based_mstrings # create_from_memory
	b.Plasma_filebuf.buf
	0
	blocksize
	false in
    let hp = Plasma_util.parse_host_port block.node in
    (* TODO: use dnch_shm for local writes *)
    dn_session_e c hp
    ++ (fun dc ->
	  Plasma_util.rpc_engine dc Dn.V1.write'async
	    (block.block, `dnch_rpc ms, block.ticket)
       )
    >> (fun st ->
	  match st with
	    | `Error err ->
		`Error(Datanode_error err)
	    | other ->
		other
       )
  in
  
  let rec write_blocks_e blocklist =
    match blocklist with
      | [] ->
	  (* We are done with the buffer. *)
	  eps_e (`Done (Some seqno)) c.esys
      | block :: blocklist' ->
	  write_block_e block
	  ++
	    (fun _ -> write_blocks_e blocklist')
  in

  write_blocks_e blocklist
  

let wrbuffers_e c inode imin imax blocklist seqno wrbufs =
  (** Writes the buffers for the indexes imin...imax *)
  (* wrbufs: collects all buffers that actually entered `Writing state *)
  let fb = filebuf c in
  let blocks_of_index = Hashtbl.create 5 in
  
  List.iter
    (fun block ->
       let blocks =
	 try Hashtbl.find blocks_of_index block.index
	 with Not_found -> [] in
       Hashtbl.replace blocks_of_index block.index (block :: blocks)
    )
    blocklist;
  
  let rec write_loop i =
    if i > imax then
      eps_e (`Done ()) c.esys
    else (
      try
	let blocks = Hashtbl.find blocks_of_index i in
	let b = Plasma_filebuf.lookup_buffer fb inode i in
	let wr_eng = wrbuffer_e c b blocks seqno in
	Plasma_filebuf.switch_to_writing fb b wr_eng;
	let e = 
	  wr_eng 
	  ++ (fun _ -> write_loop (Int64.succ i)) in
	Queue.add b wrbufs;
	e
      with
	| Not_found -> assert false
    )
  in
  
  write_loop imin
    


let flinode_e c fr =
  (** Flushes the dirty blocks of an inode. 

      As flushing happens in a separate transaction, we can face the
      following problems:
       - the inode does not exist anymore
       - the blocks to write are behind EOF
   *)
  let fb = filebuf c in
  let inode = fr.Plasma_filebuf.flush_inode in
  let min_eof = fr.Plasma_filebuf.flush_min_eof in
  let min_mtime = fr.Plasma_filebuf.flush_min_mtime in

  let snapcase = Hashtbl.mem c.snap_trans inode in

  dlogf
    "flinode_e: inode=%Ld n_blocks=%d min_eof=%Ld"
    inode
    (Plasma_util.I64Set.cardinal fr.Plasma_filebuf.flush_index_list) min_eof;

  let ranges =
    Plasma_util.I64Set.fold
      (fun index acc ->
	 I64RangeSet.add index acc
      )
      fr.Plasma_filebuf.flush_index_list
      I64RangeSet.empty in

  let rangelist = ref [] in
  I64RangeSet.iter
    (fun imin imax -> rangelist := (imin,imax) :: !rangelist)
    ranges;
  rangelist := List.rev !rangelist;

  dlogf
    "flinode_e: rangelist=%s"
    (String.concat ","
       (List.map (fun (imin,imax) -> sprintf "%Ld-%Ld" imin imax) !rangelist));
  
  (* Record the written buffers in this queue: *)
  let wrbufs = Queue.create() in

  (* We do all in one transaction, cur_trans. The first part of the
     transaction must obtain a lock for the inode. If this fails, this
     part is repeated until successful. The remaining part of the transaction
     should then work without problems
   *)

  let cur_trans = ref None in

  let rec establish_trans rep f =
    assert(!cur_trans = None);
    if rep > 10 then (   (* TODO: this limit should be configurable *)
      Netlog.logf `Crit
	"Stopping flush after %d write errors" rep;
      plasma_error_e c `efailed
    )
    else (
      start_e c
      ++ (fun trans ->
	    cur_trans := Some trans;
	    Uq_engines.meta_engine
	      (f trans)
	    ++ (function
		  | `Done() -> 
		      eps_e (`Done true) c.esys
		  | `Error (Plasma_error `estale) ->
		      (** [ESTALE] is perfectly legal - the inode
			  has been deleted in the meantime. We skip
			  the rest of flushing ([false])
		       *)
		      eps_e (`Done false) c.esys
		  | `Error (Plasma_error `econflict) ->
		      (** [ECONFLICT] means that another transaction locks
			  the inode. As this is outside the control of
			  the user, we handle it here automatically by
			  retrying the write.
		       *)
		      let t = pause.(min (Array.length pause - 1) rep) in
		      dlogf
			"Cannot flush because of ECONFLICT; \
                         retrying in %f seconds"
			t;
		      (** Abort what we've written to prevent deadlocks: *)
		      abort_e trans 
		      ++ (fun () ->
			    cur_trans := None;
			    Plasma_util.delay_engine t 
			      (fun () -> 
				 establish_trans (rep+1) f
			      )
			      c.esys
			 )
		  | `Error e ->
		      eps_e (`Error e) c.esys
		  | `Aborted ->
		      eps_e `Aborted c.esys
	       )
	 )
    ) in

  let get_trans f =
    try
      (* snapshot case: just use this transaction *)
      let trans = Hashtbl.find c.snap_trans inode in
      cur_trans := Some trans;
      f trans
      ++ (fun () -> eps_e (`Done true) c.esys)
    with Not_found ->
      establish_trans 0 f in

  let with_trans f =
    match !cur_trans with
      | None -> assert false
      | Some trans -> f trans in

  let update_ii mc =
    dlogf
      "Plasma_client.update_ii: inode=%Ld" inode;
    get_trans
      (fun trans ->
	 get_inodeinfo_e trans inode
	 ++ (fun ii ->
	       let ii' =
		 { ii with
		     eof = max ii.eof min_eof;
		     mtime = Plasma_filebuf.max_time ii.mtime min_mtime;
		 } in
	       set_inodeinfo_e trans inode ii'
	    )
      )
  in

  let rec allocate_and_write mc rangelist =
    match rangelist with
      | (imin,imax) :: rangelist' ->
	  dlogf
	    "Plasma_client.allocate_and_write: inode=%Ld imin=%Ld imax=%Ld"
	    inode imin imax;
	  with_trans
	    (fun trans ->
	       let ilen = Int64.succ(Int64.sub imax imin) in
	       let pref = get_pref_nodes c inode in
	       ( Plasma_util.rpc_engine mc 
		   Fsys.V1.allocate_blocks'async
		   (trans.tid,inode,imin,ilen,false,Array.of_list pref)
		 >> fextract
	       )
	       ++ (fun blocklist ->
		     assert(blocklist <> [| |]);
		     let seqno = blocklist.(0).inode_seqno in
		     let blocklist = 
		       Plasma_inodecache.expand_blocklist 
			 (Array.to_list blocklist) in
		     ( try
			 let (lc,blc) = snapshot_info trans inode in
			 Plasma_inodecache.override_blocks 
			   blc imin ilen blocklist
		       with Not_found -> ()
		     );
		     wrbuffers_e 
		       c inode imin imax blocklist seqno wrbufs
		  )
	    )		     
	  ++ (fun () -> 
		allocate_and_write mc rangelist'
	     )
      | [] ->
	  eps_e (`Done ()) c.esys 
  in

  (* The following Plasma_filebuf calls cause that the buffers leave
     the [`Written] state.
   *)

  let commit_buffers() =
    Queue.iter (fun b -> Plasma_filebuf.write_committed fb b) wrbufs in

  let cancel_buffers() =
    Queue.iter (fun b -> Plasma_filebuf.write_cancelled fb b) wrbufs;
    Plasma_filebuf.release_flush_request fb fr in

  let error_buffers err =
    cancel_buffers();
    Plasma_filebuf.write_erroneous fb inode err in

  (* drop_buffers: when the inode does not exist anymore *)
  let drop_buffers() =
    Plasma_filebuf.schedule_drop_inode fb inode in

  match Plasma_filebuf.inode_error fb inode with
    | None ->
	dlogf
	  "Plasma_client.flinode_e: starting inode=%Ld "
	  inode;

	with_nn_session_e c
	  (fun mc ->
	     Uq_engines.meta_engine
	       (update_ii mc
		++ (fun inode_exists ->
		      if inode_exists then
			allocate_and_write mc !rangelist
		      else (
			drop_buffers(); eps_e (`Done ()) c.esys
		      ))
		++ (fun () ->
		      (* Commit, but only in the non-snapshot case *)
		      if snapcase then eps_e (`Done()) c.esys else
			with_trans commit_e
		   )
	       )
	     ++ (fun st ->
		   dlogf
		     "Plasma_client.flinode_e: done writing \
                      inode=%Ld success=%B"
		     inode (st = `Done());
		   let st' = (st :> _ Uq_engines.engine_state) in
		   match st with
		     | `Done () ->
			 commit_buffers(); 
			 (* Partial invalidation only. Only affects the
			    non-snapshot case
			  *)
			 Plasma_inodecache.invalidate_ii c.inodecache inode;
			 eps_e (`Done()) c.esys
		     | `Error (Plasma_error ecode) ->
			 Netlog.logf `Err
			   "Stopping flush after plasma error %s"
			   (Plasma_util.string_of_errno ecode);
			 error_buffers ecode;
			 ( match !cur_trans with
			     | Some t -> abort_e t
			     | None -> eps_e (`Done ()) c.esys
			 ) ++ (fun () -> eps_e st' c.esys)
		     | `Error e ->
			 Netlog.logf `Err
			   "Stopping flush after exception %s"
			   (Netexn.to_string e);
			 error_buffers (`eio);
			 ( match !cur_trans with
			     | Some t -> abort_e t
			     | None -> eps_e (`Done ()) c.esys
			 ) ++ (fun () -> eps_e st' c.esys)
		     | `Aborted ->
			 cancel_buffers();
			 ( match !cur_trans with
			     | Some t -> abort_e t
			     | None -> eps_e (`Done ()) c.esys
			 ) ++ (fun () -> eps_e `Aborted c.esys)
		)
	  )       
    | Some err ->
	dlogf
	  "Plasma_client.flinode_e: NOT starting because inode err is set \
           inode=%Ld"
	  inode;
	eps_e (`Error (Plasma_error err)) c.esys


let rec flush_inode_e c inode =
  (* flush all dirty buffers of an inode *)
  (* There is a serializer causing that only one flush at a time can be
     done. While a flush is being done, more flush requests can arrive.
     All these requests (for the same inode) are accumulated, so that 
     there will only be one follow-up flush
   *)
  (* be a bit careful: fb might not yet be initializable because the
     blocksize is unknown (but then there is nothing to flush)
   *)
  match c.filebuf with
    | None -> 
	eps_e (`Done()) c.esys
    | Some fb ->
	(* is there a scheduled flush? If so just wait *)
	( try
	    Hashtbl.find c.flush_scheduled inode
	  with
	    | Not_found ->
		(* Schedule the flush *)
		let (e, signal) = Uq_engines.signal_engine c.esys in
		Hashtbl.replace c.flush_scheduled inode e;
		(* Now do serialized, i.e. we wait until the preceding
		   flush (possibly for another inode) is done
		 *)
		Uq_engines.delay_engine 0.1
		  (fun () ->
		     c.flush_ser # serialized
		       (fun _ -> 
			  (* we are starting now - so remove the schedule -
			     nobody should wait for this from now on
			   *)
			  Hashtbl.remove c.flush_scheduled inode;
			  ( try
			      let fr = Plasma_filebuf.select_for_flush fb inode in
			      flinode_e c fr
			    with
			      | Not_found -> 
				  eps_e (`Done()) c.esys
			  )
			  >> (fun st -> signal st; st)
		       )
		  )
		  c.esys
	)
	  

let rec flush_thread c =
  (** Only flush a buffer if there are users that can benefit: *)
  let fb = filebuf c in
  let do_it = Plasma_filebuf.n_waiting fb > 0 in
  (* Note that this criterion is not the most exact. There could already
     be pending flush requests for these waiting buffers. To make it even
     more complicated, a flushed buffer can again become dirty... We don't
     know here, so we use the simplest meaningful criterion
   *)

  dlogf "flush_thread: \
                      do_it=%b"
    do_it;
  if do_it then (
    (* Check whether there are buffers we can flush: *)
    match Plasma_filebuf.select_inodes fb with
      | inode :: _ ->
	  dlogf "flush_thread: calling flush_inode_e";
	  let e = flush_inode_e c inode in
	  c.flush_thread_active <- true;
	  let cont _ =
	    dlogf "flush_thread: continuation";
	    c.flush_thread_active <- false;
	    flush_thread c in
	  Uq_engines.when_state (* OK *)
	    ~is_done:cont ~is_error:cont ~is_aborted:cont
	    e;
	  ()
      | [] -> 
	  ()
  )


let start_flush_thread c =
  if not c.flush_thread_active then
    flush_thread c


let all_dead blocklist =
  let dead = ref true in
  Array.iter (fun block -> if block.node_alive then dead := false) blocklist;
  !dead

let hostname =
  Unix.gethostname()


let pick_block index blocklist =
  (* Return the index in the array blocklist of the picked block. We could
     always pick the first, or always pick a random block. However, this
     is not done. There are two partially contradictory requirements:
      - Balance load on the available datanodes
      - It is advantageous when several consecutive blocks are read from
        the same datanode (non-random access)

     What we do:
      - Sort the blocklist by datanode
      - Use only the higher bits of [index] to pick a datanode
      - The hostname and the process ID has an influence on picking
   *)
  let blocklist_alive =
    Array.of_list
      (List.filter (fun binfo -> binfo.node_alive) (Array.to_list blocklist)) in
  assert(blocklist_alive <> [||]);  (* otherwise [all_dead] not working *)
  let blocklist2 =
    Array.mapi (fun i binfo -> (i,binfo)) blocklist_alive in
  Array.sort
    (fun (_,binfo1) (_,binfo2) -> 
       String.compare binfo1.identity binfo2.identity)
    blocklist2;
  let relevant_index =
    Int64.shift_right index 6 in   (* Assuming contiguity of 64 blocks *)
  let dg =
    Digest.string (Int64.to_string relevant_index ^ " " ^ 
		     hostname ^ " " ^ string_of_int (Unix.getpid())) in
  let k1 = (Char.code dg.[0]) + 256 * (Char.code dg.[1]) in
  let k2 = k1 mod (Array.length blocklist2) in
  fst (blocklist2.(k2))
  

let rec really_rdblock_e c mc lt blc ii blocksize inode index notify_read b 
                         action =
  (* This is the engine that performs the read. The target buffer is [b].
     Before the engine finishes the [action] is run.

     This engine represents the [`Reading] state as defined in 
     Plasma_filebuf.

     notify_read: this is called back when the block is requested from the
     datanode. There is guarantees whether this callback is invoked at all.
     Also it can be invoked several times.
   *)
  dlogf
    "really_rdblock_e inode=%Ld index=%Ld" inode index;

  let call_action_e seqno =
    (** Called once [b.buf] is filled with data *)
    action b;
    dlogf
      "really_rdblock_e inode=%Ld index=%Ld returning" inode index;
    eps_e (`Done (Some seqno)) c.esys
  in

  (** Get the locations of the block: *)
  Plasma_inodecache.get_blocks_via_cache_e blc index  
  ++ (function
	| `ok blocklist ->
	    dlogf
	      "really_rdblock_e inode=%Ld index=%Ld: got blocklist n=%d"
	      inode index (Array.length blocklist);

	    (** If the blocklist is non-empty but all nodes are dead,
		return an error
	     *)
	    if blocklist <> [| |] && all_dead blocklist then
	      plasma_error_e c `eio
	    else
	      (** If the blocklist is empty, we are reading a file hole.
		  Return zeros to the user
	       *)
	      if blocklist = [| |] then (
		Bigarray.Array1.fill b.Plasma_filebuf.buf '\000';
		call_action_e ii.seqno (* FIXME *)
	      )
	      else (
		(** Get any block: *)
		(* Better: Instead of taking a block randomly, look at
		   the number of outstanding responses.
		   FIXME. There might be even better criterions (determinism)
		 *)
		let j = pick_block index blocklist in
		let block = blocklist.(j) in  
		let hp = Util.parse_host_port block.node in
		assert(block.length = 1L);
		dlogf
		  "really_rdblock_e inode=%Ld index=%Ld: reading %Ld from %s"
		  inode index block.block block.node;
		dn_session_e c hp
		++ (fun dc ->
		      (* TODO: optimize local accesses with `dnch_shm *)
		      (* TODO: report any RPC error as `eio *)
		      notify_read();
		      ( Util.rpc_engine 
			  dc Dn.V1.read'async 
			  (`dnch_rpc, block.block, 0, blocksize, 
			   block.ticket)
			>> (fun st ->
			      match st with
				| `Error err ->
				    `Error(Datanode_error err)
				| other ->
				    other
			   )
		      )
		      ++ (fun dnch ->
			    let data =
			      match dnch with
				| `dnch_rpc data -> data
				| `dnch_shm -> assert false in
			    assert(data#length = blocksize);
			    dlogf
			      "really_rdblock_e inode=%Ld index=%Ld: \
                                got %Ld from %s"
			      inode index block.block block.node;
			    (* This blit is a string-to-bigarray copy *)
			    data # blit_to_memory 
			      0 b.Plasma_filebuf.buf 0 blocksize;
			    call_action_e block.inode_seqno
			 )
		   )
	      )
	| #errno as e ->
	    plasma_error_e c e
     )
    

let is_active state =
  match state with
      `Working _ -> true
    | _          -> false


let rec rdblock_e c mc lt blc ii blocksize inode index notify_read action eof_opt =
  (** Read this block and perform [action]. If [eof_opt=None] this 
      read leads to a clean buffer, and [action] must only read-access
      the buffer. If [eof_opt=Some eof] this read leads to a dirty
      buffer, and [action] is allowed to modify the buffer. The [eof]
      position is considered for the flush request.

      notify_read: see really_rdblock_e
   *)
  let fb = filebuf c in
  let accept b =
    ( match eof_opt with
	| None -> 
	    Plasma_filebuf.clean_access fb b
	| Some eof -> 
	    Plasma_filebuf.dirty_access fb b eof;
	    start_flush_thread c  (* maybe there is a new dirty buffer *)
    );
    action b;
    dlogf
      "rdblock_e inode=%Ld index=%Ld returning" inode index;
    eps_e (`Done ()) c.esys in

  dlogf
    "rdblock_e inode=%Ld index=%Ld: start" inode index;
  try
    let b = Plasma_filebuf.lookup_buffer fb inode index in
    match b.Plasma_filebuf.buf_state with
      | `Invalid ->
	  (* For very short periods of time buffers can be in [`Invalid]
	     state. Only [request_buffer_e] cause this. We can simply
	     raise [Not_found] because the [request_buffer_e] call below
	     will detect the competing request and return [None].
	     As the event queue is invoked competing engines can run.
	   *)
	  raise Not_found
      | `Dropped ->
	  assert false
	    (* impossible because [lookup_buffer] raises [Not_found] *)
      | `Clean ->
	  (* do a validity check before *)
	  if ii.seqno <> b.Plasma_filebuf.buf_seqno then (
	    Plasma_filebuf.schedule_drop fb b;
	    raise Not_found
	  );
	  accept b
      | `Dirty 
      | `Written ->
	  (* a validity check does not make sense - we cannot fix it
	     anyway. So always return our local view of the data 
	   *)
	  accept b
      | `Reading e ->
	  (* another user is reading the block. This is fine - we just
	     wait until e finishes, and check then the state again
	   *)
	  dlogf
	    "rdblock_e inode=%Ld index=%Ld: attaching to existing rd"
	    inode index;
	  assert(is_active e#state);
	  Uq_engines.meta_engine e    (* i.e. ignore errors of e *)
	  ++ (fun _ -> 
		rdblock_e
		  c mc lt blc ii blocksize inode index notify_read action eof_opt
	     )
      | `Writing e ->
	  (* writing: if we only want to read (eof_opt=None) this is
	     just fine. Otherwise we have to wait 
	   *)
	  if eof_opt = None then
	    accept b
	  else (
	    dlogf
	      "rdblock_e inode=%Ld index=%Ld: attaching to existing wr"
	      inode index;
	    assert(is_active e#state);
	    Uq_engines.meta_engine e    (* i.e. ignore errors of e *)
	    ++ (fun _ -> 
		  rdblock_e
		    c mc lt blc ii blocksize inode index notify_read action eof_opt
	       )
	  )
  with
    | Not_found ->
	(** We don't have a valid copy of this block. Get a buffer,
	    and start reading.
	 *)
	dlogf
	  "rdblock_e inode=%Ld index=%Ld: requesting buffer" inode index;
	(* First check whether we have an inode error to report *)
	match Plasma_filebuf.inode_error fb inode with
	  | Some errno ->
	      Plasma_filebuf.reset_inode_error fb inode;
	      eps_e (`Error(Plasma_error errno)) c.esys
	  | None ->
	      let eng =
		Plasma_filebuf.request_buffer_e fb inode index
		++ (fun b_opt ->
		      match b_opt with
			| None ->
			    (** There is a competing access, or an error.
				Just start over *)
			    dlogf
			      "rdblock_e inode=%Ld index=%Ld: retrying" inode index;
			    rdblock_e
			      c mc lt blc ii blocksize inode index notify_read action
			      eof_opt
			| Some b ->
			    let e = 
			      really_rdblock_e
				c mc lt blc ii blocksize inode index notify_read
				b action in
			    Plasma_filebuf.switch_to_reading fb b e eof_opt;
			    e ++ (fun _ -> eps_e (`Done ()) c.esys)
			      (* NB. When we return the buffer is still in
				 `Reading state. The read is done, and the
				 buffer will soon go to `Dirty or `Clean,
				 so this is not a problem
			       *)
		   ) in
	      start_flush_thread c;

	      Uq_engines.when_state (* OK *)
		~is_error:(fun e ->
			     dlogf
			       "Plasma_client.rdblock_e: Exception %s"
			       (Netexn.to_string e))
		eng;
	      eng

let read_with_ii_e c mc lt blc inode ii pos sm spos len =
  let fb = filebuf c in
  let blocksize = c.blocksize in 
  let blocksizeL = Int64.of_int blocksize in
  let eof =
    try
      max (Plasma_filebuf.get_flush_min_eof fb inode) ii.eof
    with Not_found -> ii.eof in
  let max_len = (** how much we could read at most *)
    max (Int64.sub eof pos) 0L in
  let eff_len = (** how much we will read into [sm] *)
    Int64.to_int(min max_len (Int64.of_int len)) in
  let end_pos =
    Int64.add pos (Int64.of_int eff_len) in
  let at_eof =
    end_pos >= eof in

  let rec submit_read_requests_e offs acc =
    (** create read requests for [pos+offs] on *)
    let offsL = Int64.of_int offs in
    let cur_pos = Int64.add pos offsL in
    let index =
      Int64.div cur_pos blocksizeL in
    let bpos = 
      Int64.to_int (Int64.sub cur_pos (Int64.mul index blocksizeL)) in
    let req_len =
      min (eff_len - offs) (blocksize - bpos) in
    if req_len = 0 then
      eps_e (`Done acc) c.esys
    else
      let action b =
	Plasma_filebuf.blit_from_buffer fb b bpos sm (spos+offs) req_len in
      let (next_e, signal) = Uq_engines.signal_engine c.esys in
      let notify_read() = signal (`Done()) in
      let e =
	rdblock_e c mc lt blc ii blocksize inode index notify_read action None in
      (* The next block is best first dealt with when this block is requested
	 from the datanode. We catch this moment here via the notify_read
	 callback. The block can also be in the buffer cache, so we also
	 need to signal when e is done.

	 The effect of this serialization is that we request the blocks
	 from the datanode in ascending order.
       *)
      Uq_engines.when_state (* OK *)
	~is_done:(fun () -> signal(`Done()))
	~is_error:(fun _ -> signal(`Done()))
	~is_aborted:(fun _ -> signal(`Done()))
	e;
      next_e ++ (fun () ->
		   submit_read_requests_e (offs + req_len) (e::acc)
		)
  in
  
  submit_read_requests_e 0 []
  ++ (fun elist ->
	Uq_engines.msync_engine
	  elist
	  (fun () () -> ())
	  ()
	  c.esys
     )
  ++
    (fun () ->
       eps_e (`Done(eff_len,at_eof,ii)) c.esys
    )


let sm_length =
  function
    | `String s -> String.length s 
    | `Memory m -> Bigarray.Array1.dim m
    

let read_e ?(lazy_validation=false) c inode pos sm spos len =
  let l = sm_length sm in
  if spos < 0 || len < 0 || spos > l-len then
    invalid_arg "Plasma_client.read_e";

  with_nn_session_e c
    (fun mc ->
     with_lazy_transaction_e c mc inode
       (fun lt blc ->
	  (* This container caches the blocks for the duration of the
	     transaction (if any)
	   *)
	  (* normally update the cache - except we have lazy_validation *)
	  if not lazy_validation && not lt#snapshot then
	    lt # invalidate_ii inode;
	  lt # get_ii_e inode false
	  ++ (fun ii ->
		read_with_ii_e c mc lt blc inode ii pos sm spos len
	     )
	  >> (function
		| `Error (Plasma_error `eio) as st ->
		    (* forget blocks after EIO, because the blocks also
		       contain the healthiness bit, and we want that it is
		       updated
		     *)
		    Plasma_inodecache.notify_got_eio blc;
		    st
		| st -> st
	     )
       )
    )

let read ?lazy_validation c inode pos sm spos len =
  sync (read_e ?lazy_validation c inode pos sm spos) len

let in_buffer c inode pos =
  try
    let filebuf = match c.filebuf with Some b -> b | None -> raise Not_found in
    let blocksize = c.blocksize in
    let blocksizeL = Int64.of_int blocksize in
    let index = Int64.div pos blocksizeL in
    let b = Plasma_filebuf.lookup_buffer filebuf inode index in
    let st = Plasma_filebuf.(b.buf_state) in
    match st with
      | `Clean | `Dirty | `Writing _ | `Written -> true
      | _ -> false  (* incl. `Reading *)
  with
    | Not_found -> false


type read_request = int64 * strmem * int * int

type read_response = int * bool * inodeinfo

type multi_read_task =
    (read_request * (read_response -> unit))
      option 
        Uq_engines.engine


let multi_read_e ?(lazy_validation=false) 
                 c inode (mrt:multi_read_task Stream.t) =

  let rec next_task_e mc lt blc ii =
    try
      let task_e = Stream.next mrt in
      task_e
      ++ (function
	    | None ->
		eps_e (`Done ()) c.esys
	    | Some ((pos,sm,spos,len), pass_resp) ->
		let l = sm_length sm in
		if spos < 0 || len < 0 || spos > l-len then
		  invalid_arg "Plasma_client.multi_read_e (inside stream)";
		read_with_ii_e c mc lt blc inode ii pos sm spos len
		++ (fun (n,eof,ii) ->
		      pass_resp (n,eof,ii);
		      next_task_e mc lt blc ii
		   )
		
	 )
    with
      | Stream.Failure ->
	  eps_e (`Done ()) c.esys
  in

  with_nn_session_e c
    (fun mc ->
       with_lazy_transaction_e c mc inode
	 (fun lt blc ->
	    (* normally update the cache - except we have lazy_validation *)
	    if not lazy_validation && not lt#snapshot then
	      lt # invalidate_ii inode;
	    lt # get_ii_e inode false
	    ++ (fun ii ->
		  next_task_e mc lt blc ii
	       )
	 )
    )



let rec drop_inode c inode =
  let fb = filebuf c in
  Plasma_filebuf.schedule_drop_inode fb inode


let get_inode_error c inode =
  (* be a bit careful: fb might not yet be initializable because the
     blocksize is unknown
   *)
  match c.filebuf with
    | None -> None
    | Some fb -> Plasma_filebuf.inode_error fb inode


let reset_inode_error c inode =
  match c.filebuf with
    | None -> ()
    | Some fb -> Plasma_filebuf.reset_inode_error fb inode


let write_e c inode pos sm spos len =
  let l = sm_length sm in
  if spos < 0 || len < 0 || spos > l-len then
    invalid_arg "Plasma_client.write_e";
  if pos < 0L || pos > Int64.sub Int64.max_int (Int64.of_int len) then
    invalid_arg "Plasma_client.write_e";

  let rec find_and_write_e mc index bpos blen min_eof =
    (** If the block is already known: Just write. *)
    let fb = filebuf c in
    try
      let b = Plasma_filebuf.lookup_buffer fb inode index in
      match b.Plasma_filebuf.buf_state with
	| `Clean | `Dirty | `Written ->
	    Plasma_filebuf.blit_to_buffer fb sm spos b bpos blen;
	    Plasma_filebuf.dirty_access fb b min_eof;
	    start_flush_thread c;
	    eps_e (`Done blen) c.esys
	| `Reading e ->
	    (* There is an ongoing read. Wait until this is done, and
	       check the buffer again
	     *)
	    assert(is_active e#state);
	    Uq_engines.meta_engine e   (* ignore errors *)
	    ++ (fun _ -> find_and_write_e mc index bpos blen min_eof)
	| `Writing e ->
	    (* There is an ongoing write. Wait until this is done, and
	       check the buffer again
	     *)
	    assert(is_active e#state);
	    Uq_engines.meta_engine e   (* ignore errors *)
	    ++ (fun _ -> find_and_write_e mc index bpos blen min_eof)
	| `Invalid | `Dropped -> assert false
    with
      | Not_found ->
	  (* Nothing is known about the block. If we replace the whole
	     block we can just request a new buffer and put our data
	     there. If we write only parts of the block we have to
	     read the old version first
	   *)
	  if blen = c.blocksize then (
	    (* First check whether we have an inode error to report *)
	    match Plasma_filebuf.inode_error fb inode with
	      | Some errno ->
		  Plasma_filebuf.reset_inode_error fb inode;
		  eps_e (`Error(Plasma_error errno)) c.esys
	      | None ->
		  let eng =
		    Plasma_filebuf.request_buffer_e fb inode index
		    ++ (fun b_opt ->
			  match b_opt with
			    | None -> 
				(* A competing request for the same block,
				   or an error. Try again *)
				find_and_write_e mc index bpos blen min_eof
			    | Some b ->
				(* We got the buffer (initially invalid) *)
				Plasma_filebuf.blit_to_buffer fb sm spos b bpos blen;
				Plasma_filebuf.dirty_access fb b min_eof;
				eps_e (`Done blen) c.esys
		       ) in
		  start_flush_thread c;
		  eng
	  ) else (
	    (* Now we have to read the block first. We have to
	       do this in a new transaction. This call of [rdblock_e]
	       will leave the buffer in [`Dirty] state. The [action]
	       is allowed to modify the buffer. Note that [min_eof] is
	       already handled by [rdblock_e].
	     *) 
	    let action b =
	      Plasma_filebuf.blit_to_buffer fb sm spos b bpos blen in
	    with_lazy_transaction_e c mc inode
	      (fun lt blc ->
		 lt # get_ii_e inode false
		 ++ (fun ii ->
		       rdblock_e 
			 c mc lt blc ii c.blocksize inode index (fun () -> ()) 
			 action (Some min_eof)
		    )
		 ++ (fun () ->
		       eps_e (`Done blen) c.esys
		    )
	      )
	  )

  in

  (** maybe we have an error to report *)
  let err_opt = get_inode_error c inode in

  match err_opt with
    | None ->
	with_nn_session_e c
	  (fun mc ->
	     let blocksize = c.blocksize in
	     let index =
	       Int64.div pos (Int64.of_int blocksize) in
	     let bpos = 
	       Int64.to_int
		 (Int64.sub pos
		    (Int64.mul index (Int64.of_int blocksize))) in
	     let blen = (* len' *)
	       min len (blocksize - bpos) in
	     
	     let min_eof =
	       Int64.add pos (Int64.of_int blen) in
	     
	     if blen = 0 then
	       eps_e (`Done 0) c.esys
	     else (
	       find_and_write_e mc index bpos blen min_eof 
	     )
	  )
    | Some err ->
	(** Report the error and reset the error flag *)
	reset_inode_error c inode;
	eps_e (`Error (Plasma_error err)) c.esys


let write c inode pos s spos len = sync (write_e c inode pos s spos) len

let flush_e c inode pos len =
  if len < 0L then invalid_arg "Plasma_client.flush_e";
  (* pos,len: ignored in this implementation *)

  (** maybe we have an error to report *)
  let err_opt = get_inode_error c inode in

  dlogf "flush_e inode=%Ld" inode;

  match err_opt with
    | None ->
	flush_inode_e c inode
	(* On error we can immdediately reset the error code because the
	   error is reported via e
	 *)
	>> (fun st ->
	      ( match st with
		  | `Error _ -> reset_inode_error c inode
		  | _ -> ()
	      );
	      st
	   )
    | Some err ->
	(** Report the error and reset the error flag *)
  	reset_inode_error c inode;
	eps_e (`Error (Plasma_error err)) c.esys



let flush c inode pos len = sync (flush_e c inode pos) len


let flush_all_e c =
  match c.filebuf with
    | None ->
	eps_e (`Done ()) c.esys
    | Some fb ->
	let engines = ref [] in
	List.iter
	  (fun inode ->
	     engines := (flush_inode_e c inode) :: !engines
	  )
	  (Plasma_filebuf.select_inodes fb);
	Plasma_util.msync_engine !engines c.esys
	  (* flush_ser causes that the engines are executed in serialized 
	     order anyway. We only wait here until all are terminated.
	   *)

let flush_all c = sync flush_all_e c


let dump_buffers c =
  match c.filebuf with
    | None ->
	dlogf "BUFFERS NOT YET INITIALIZED"
    | Some fb ->
	Plasma_filebuf.dump_buffers fb


let get_write_eof c inode =
  match c.filebuf with
    | None -> raise Not_found
    | Some fb ->
	Plasma_filebuf.get_flush_min_eof fb inode


let get_write_mtime c inode =
  match c.filebuf with
    | None -> raise Not_found
    | Some fb ->
	Plasma_filebuf.get_flush_min_mtime fb inode


let rec snapshot_e ?(append=false) trans inode =
  let c = trans.cluster in
  if Hashtbl.mem c.snap_trans inode then
    failwith
      "Plasma_client.snapshot: there is already a snapshot for this inode";
  (* Do this now to prevent competing snapshots: *)
  Hashtbl.replace c.snap_trans inode trans;
  trans.snapshots <- inode :: trans.snapshots;
  with_nn_session_e c
    (fun mc ->
       get_inodeinfo_e trans inode
       ++ (fun init_ii ->
	     let cur_ii = ref init_ii in
	     let force_ii = ref false in
             let ser = Uq_engines.serializer c.esys in
	     let lt =
	       ( object(self)
                   method serialized
                     : (unit -> unit Uq_engines.engine) -> 
                       unit Uq_engines.engine =
                     fun f -> ser # serialized (fun _ -> f())
		   method get_tid_e() = eps_e (`Done trans.tid) c.esys
		   method get_ii_e i force_flag = 
		     let get_ii_from_server_e() =
		       (* The idea: if the server has an ii with
			  committed=false, this corresponds to our
			  own transactional view. Otherwise use init_ii
			  to keep our initial view
			*)
		       assert(i=inode); 
		       ( Plasma_inodecache.get_ii_e
			   c.inodecache mc trans.tid inode
			 >> fextract
		       )
		       ++ (fun ii ->
			     let ret_ii =
			       if ii.committed then init_ii else ii in
			     cur_ii := ret_ii;
			     force_ii := false;
			     eps_e (`Done ret_ii) c.esys
			  ) in
		     if force_flag || !force_ii then
		       get_ii_from_server_e()
		     else
		       eps_e (`Done !cur_ii) c.esys
		   method invalidate_ii i =
		     Plasma_inodecache.invalidate_ii
		       trans.cluster.inodecache inode;
		     force_ii := true
		   method release_e() = eps_e (`Done ()) c.esys
		   method snapshot = true
		   method event_system = c.esys
		 end
	       ) in
	     let blc = Plasma_inodecache.create_bl_cache mc lt inode in
	     Plasma_inodecache.snapshot_blocks_e ~append blc init_ii
	     ++ (function
		   | `ok ->
		       let fb = filebuf c in
		       Plasma_filebuf.schedule_drop_inode fb inode;
		       Hashtbl.replace c.lazy_trans inode
			 (lt, blc, ref 0, (-1.0), None);
		       eps_e (`Done()) c.esys
		   | `econflict -> (* conflict: just repeat *)
		       snapshot_e trans inode
		   | #errno as code ->
		       raise (Plasma_error code)
		)
	  )
    )
  >> (fun st ->
	match st with
	  | `Error _ | `Aborted ->
	      Hashtbl.remove c.snap_trans inode;
	      trans.snapshots <- 
		List.filter (fun i -> i <> inode) trans.snapshots;
	      st
	  | _ -> st
     )

let snapshot ?append trans inode = sync (snapshot_e ?append trans) inode


let with_trans_e c f =
  let t_opt = ref None in
  Uq_engines.meta_engine
    (start_e c ++ (fun trans -> t_opt := Some trans; f trans))
  ++ (fun st ->
	match st with
	  | `Done r ->
	      let t =
		match !t_opt with
		  | None -> assert false
		  | Some t -> t in
	      commit_e t ++ (fun () -> eps_e (`Done r) c.esys)
	  | `Error err ->
	      ( match !t_opt with
		  | None -> eps_e (`Error err) c.esys
		  | Some t ->
		      abort_e t ++ (fun () -> eps_e (`Error err) c.esys)
	      )
	  | `Aborted ->
	      ( match !t_opt with
		  | None -> eps_e `Aborted c.esys
		  | Some t ->
		      abort_e t ++ (fun () -> eps_e `Aborted c.esys)
	      )
     )

let with_trans c f =
  let trans = start c in
  try
    let r = f trans in
    commit trans;
    r
  with
    | error ->
	abort trans; raise error



let retry c name f arg =
  let t0 = Unix.gettimeofday() in
  let n = ref 0 in
  let timeout =
    try float_of_string(List.assoc "lock_timeout" c.params)
    with _ -> lock_timeout in
   

  let rec loop() =
    try
      let r = f arg in
      r
    with
      | Plasma_error (`econflict as code) as error ->
	  Netlog.logf `Err
	    "Plasma error for %s: %s"
	    name
	    (Plasma_util.string_of_errno code);
	  next_attempt error
      | Plasma_error (`eio as code) as error ->
	  Netlog.logf `Err
	    "Plasma error for %s: %s"
	    name
	    (Plasma_util.string_of_errno code);
	  n := max !n 7;  
	  (* min 1 second; the check interval for liveness is also 1 second *)
	  next_attempt error
      | Rpc_client.Message_timeout as error ->
	  Netlog.logf `Err
	    "Plasma error for %s: Timeout"
	    name;
	  next_attempt error
      | Plasma_error code as error ->
	  Netlog.logf `Debug
	    "Exception accessing %s: %s"
	    name
	    (Netexn.to_string error);
	  raise error
      | error ->
	  Netlog.logf `Crit
	    "Exception accessing %s: %s"
	    name
	    (Netexn.to_string error);
	  raise error

  and next_attempt error =
    let l = Array.length pause in
    let d = if !n < l then pause.( !n ) else pause.( l - 1 ) in
    incr n;
    let t1 = Unix.gettimeofday() in
    if t1 -. t0 +. d < timeout then (
      Netsys.restarting_sleep d;
      Netlog.logf `Err
	"Retrying operation after error";
      loop()
    ) else (
      Netlog.logf `Err "No retry";
      raise error
    ) in
  loop()
  

let retry_e c name f arg =
  let t0 = Unix.gettimeofday() in
  let n = ref 0 in
  let esys = c.esys in
  let timeout =
    try float_of_string(List.assoc "lock_timeout" c.params)
    with _ -> lock_timeout in
  
  let rec loop() =
    Uq_engines.meta_engine (
      try f arg
      with error -> eps_e (`Error error) esys
    )
    ++ (fun st ->
	  match st with
	    | `Done x -> eps_e (`Done x) esys
	    | `Error(Plasma_error (`econflict as code) as error) ->
		Netlog.logf `Err
		  "Plasma error for %s: %s"
		  name
		  (Plasma_util.string_of_errno code);
		next_attempt_e error
	    | `Error(Plasma_error (`eio as code) as error) ->
		Netlog.logf `Err
		  "Plasma error for %s: %s"
		  name
		  (Plasma_util.string_of_errno code);
		n := max !n 7;  
		(* min 1 second; the check interval for liveness is also 1 
		   second *)
		next_attempt_e error
	    | `Error(Rpc_client.Message_timeout as error) ->
		Netlog.logf `Err
		  "Plasma error for %s: Timeout"
		  name;
		next_attempt_e error
	    | `Error(Plasma_error code as error) ->
		Netlog.logf `Debug
		  "Exception accessing %s: %s"
		  name
		  (Netexn.to_string error);
		next_attempt_e error
	    | `Error error ->
		Netlog.logf `Crit
		  "Exception accessing %s: %s"
		  name
		  (Netexn.to_string error);
		eps_e (`Error error) esys
	    | `Aborted ->
		eps_e `Aborted esys
       )
  and next_attempt_e error =
    let l = Array.length pause in
    let d = if !n < l then pause.( !n ) else pause.( l - 1 ) in
    incr n;
    let t1 = Unix.gettimeofday() in
    if t1 -. t0 +. d < timeout then
      Uq_engines.delay_engine d
	(fun () ->
	   Netlog.logf `Err
	     "Retrying operation after error";
	   loop()
	)
	esys
    else (
      Netlog.logf `Err "No retry";
      eps_e (`Error error) esys
    )
  in
  loop()


(* --- serial I/O --- *)


let split_by f l =
  let ht = Hashtbl.create 7 in
  List.iter
    (fun x ->
       let k = f x in
       try
	 let p = Hashtbl.find ht k in
	 Hashtbl.replace ht k (x::p)
       with
	 | Not_found ->
	 Hashtbl.replace ht k [x]
    )
    l;
  let v =
    Hashtbl.fold
      (fun _ p acc -> List.rev p :: acc)
      ht
      [] in
  let kl =
    Hashtbl.fold
      (fun k _ acc -> k :: acc)
      ht
      [] in
  (v, kl)


type dn_shm =
    { shm_serial : int;
      shm_dn_client : Rpc_proxy.ManagedClient.t;
      mutable shm_opath : string;
      mutable shm_blocks : int;
      mutable shm_free : (Netsys_mem.memory * int) list;
      shm_id : Plasma_shm.id;
      mutable shm_destroyed : bool
    }

type supp_entry =
    [ `Supported of dn_shm
    | `Unsupported
    | `Requesting of unit Uq_engines.engine * dn_shm
    ]

type dn_shm_support = (string * int, supp_entry) Hashtbl.t
    (** Remembers for which (host,port) pair shared memory is supported *)


let get_shm_buf c shm blocksize =
  assert(not shm.shm_destroyed);
  match shm.shm_free with
    | (m,idx) :: free' ->
	shm.shm_free <- free';
	m, idx
    | [] ->
	let fd = Plasma_util.open_shm_fd shm.shm_opath in
	( try
	    let idx = shm.shm_blocks in
	    let pos = Int64.of_int (idx * blocksize) in
	    let eof = (idx+1) * blocksize in
	    (* The following three calls may fail for various reasons.
	       Take care not to corrupt the state because of this.
	     *)
	    c.shm_mng # book_allocation shm.shm_id eof;
	    Plasma_util.enlarge_shm fd eof;
	    let m = Plasma_util.map_shm fd pos blocksize in
	    Unix.close fd;
	    shm.shm_blocks <- idx + 1;
	    m, idx
	  with
	    | error ->
		(* NB. it is no big problem that the allocation is not 
		   undone here, because it is finally reset to 0 anyway
		 *)
		Unix.close fd;
		raise error
	)
    

let release_shm_buf shm m idx =
  shm.shm_free <- (m,idx) :: shm.shm_free


let destroy_shm c shm =
  (** The shm file is deleted by the datanode server when we close the
      TCP connection. For releasing memory, we also should unmap memory
      as quickly as possible.
   *)
  if not shm.shm_destroyed then (
    try
      shm.shm_destroyed <- true;
      List.iter
	(fun (m,_) ->
	   Netsys_mem.memory_unmap_file m
	)
	shm.shm_free;
      shm.shm_free <- [];
      if shm.shm_opath <> "" then (
	try
	  (* NB. After the truncate we will get SIGBUS in the case we still
	     access the shm blocks!
	   *)
	  let fd = Plasma_util.open_shm_fd shm.shm_opath in
	  Unix.ftruncate fd 0;
	  Unix.close fd
	with
	  | Unix.Unix_error(Unix.ENOENT,_,_) -> ()
      );
      c.shm_mng # book_allocation shm.shm_id 0
    with
      | error ->
	  Netlog.logf `Warning
	    "Exception while destroying shm: %s"
	    (Netexn.to_string error)
  )

let rec request_shm_e c dc (supp : dn_shm_support) (h,p) =
  (** Check whether there is shm support, and return [shm option] *)
  let dc_serial = Rpc_proxy.ManagedClient.mclient_serial dc in
  let hp = (h,p) in
  try
    let shm_entry = Hashtbl.find supp hp in
    match shm_entry with
      | `Unsupported -> 
	  eps_e (`Done None) c.esys
      | `Supported shm ->
	  if shm.shm_dn_client != dc || shm.shm_serial <> dc_serial then (
	    (* The dn client reconnected in the meantime; so all our past
	       shm reservations are lost
	     *)
	    destroy_shm c shm;
	    raise Not_found
	  );
	  assert(not shm.shm_destroyed);
	  eps_e (`Done(Some shm)) c.esys
      | `Requesting (e,shm) ->
	  e ++ (fun () -> request_shm_e c dc supp hp)
  with
    | Not_found ->
	dlogf
	  "Plasma_client: Checking SHM support for %s:%d" h p;
	let shm =
	  { shm_serial = dc_serial;
	    shm_dn_client = dc;
	    shm_opath = "";
	    shm_blocks = 0;
	    shm_free = [];
	    shm_id = c.shm_mng # new_id `Shm;
	    shm_destroyed = false;
	  } in
	let e =
	  ( Util.rpc_engine dc Dn.V1.alloc_shm_if_local'async() 
	    >> (fun st ->
		  match st with
		    | `Error err ->
			`Error(Datanode_error err)
		    | other ->
			other
	       )
	  )
	  ++ (fun path_opt ->
		let shm_opt =
		  match path_opt with
		    | None -> 
			dlogf
			  "Plasma_client: No SHM support for %s:%d" h p;
			Hashtbl.replace supp hp `Unsupported;
			None
		    | Some path -> 
			if shm.shm_destroyed then (
			  dlogf
			    "Plasma_client: Too late for SHM support %s:%d"
			    h p;
			  Hashtbl.replace supp hp `Unsupported;
			  None
			)
			else (
			  dlogf
			    "Plasma_client: Got SHM support for %s:%d with %s"
			    h p path;
			  shm.shm_opath <- path;
			  Hashtbl.replace supp hp (`Supported shm);
			  Some shm
			) in
		eps_e (`Done shm_opt) c.esys
	     ) in
	let e' = e ++ (fun _ -> eps_e (`Done ()) c.esys) in
	Hashtbl.replace supp hp (`Requesting (e',shm));
	e


type fd_layer =
    { pread : Netsys_mem.memory -> int -> int64 -> int -> int;
         (* pread mem mempos filepos len *)
      pwrite : Netsys_mem.memory -> int -> int64 -> int -> unit;
         (* pwrite mem mempos filepos len *)
      zero : int64 -> int -> unit;
         (* zero filepos len *)
      fadvise : int64 -> int64 -> unit;
         (* fadvise pos len (always WILLNEED) *)
    }


let fd_layer_of_fd fd =
  let null_data =
    Bigarray.Array1.create Bigarray.char Bigarray.c_layout 4096 in
  Bigarray.Array1.fill null_data '\000';
  { pread = (fun data datapos filepos len ->
		ignore(Unix.LargeFile.lseek fd filepos Unix.SEEK_SET);
		Netsys_mem.mem_read fd data datapos len
	    );
    pwrite = (fun data datapos filepos len ->
		ignore(Unix.LargeFile.lseek fd filepos Unix.SEEK_SET);
		ignore(Netsys_mem.mem_write fd data datapos len)
	    );
    zero = (fun filepos len ->
	      ignore(Unix.LargeFile.lseek fd filepos Unix.SEEK_SET);
	      for k = 0 to (len / 4096) - 1 do
		ignore(Netsys_mem.mem_write fd null_data 0 4096)
	      done;
	      ignore(Netsys_mem.mem_write fd null_data 0 (len mod 4096))
	   );
    fadvise = (fun filepos len ->
		 Netsys_posix.fadvise fd filepos len Netsys_posix.FADV_WILLNEED
	      );
  }


let fd_layer_of_buf buf buf_filepos buf_len =
  (* it is assumed buf contains already the file data from buf_filepos
     with length buf_len
   *)
  assert(buf_len >= 0);
  let buf_lenL = Int64.of_int buf_len in
  { pread = (fun data datapos filepos len ->
	       if len > 0 then (
		 if filepos < buf_filepos then
		   invalid_arg "fd_layer_of_buf (pread #1)";
		 if Int64.sub filepos buf_lenL > buf_filepos then
		   invalid_arg "fd_layer_of_buf (pread #2)";
		 let offs = Int64.to_int(Int64.sub filepos buf_filepos) in
		 let eff_len = min len (buf_len - offs) in
		 Plasma_platform.blit_memory buf offs data datapos eff_len;
		 eff_len
	       ) else 0
	    );
    pwrite = (fun data datapos filepos len ->
		if len > 0 then (
		  if filepos < buf_filepos then
		    invalid_arg "fd_layer_of_buf (pwrite #1)";
		  if Int64.sub filepos buf_lenL > buf_filepos then
		    invalid_arg "fd_layer_of_buf (pwrite #2)";
		  let offs = Int64.to_int(Int64.sub filepos buf_filepos) in
		  if buf_len - offs < len then
		    invalid_arg "fd_layer_of_buf (pwrite #3)";
		  Plasma_platform.blit_memory data datapos buf offs len;
		  ()
		)
	     );
    zero = (fun filepos len ->
	      if len > 0 then (
		if filepos < buf_filepos then
		  invalid_arg "fd_layer_of_buf (zero #1)";
		if Int64.sub filepos buf_lenL > buf_filepos then (
		  let detail =
		    sprintf "buf_filepos=%Ld buf_len=%d filepos=%Ld len=%d"
		      buf_filepos buf_len filepos len in
		  invalid_arg ("fd_layer_of_buf (zero #2): " ^ detail)
		);
		let offs = Int64.to_int(Int64.sub filepos buf_filepos) in
		if buf_len - offs < len then
		  invalid_arg "fd_layer_of_buf (zero #3)";
		Plasma_platform.fill_memory buf offs len '\000'
	      )
	   );
    fadvise = (fun filepos len -> () )
  }

(* ---------------------------------------------------------------------- *)
(* because copy_in is quite complicated we put it into a functor that is
   instantiated on every copy_in invocation
 *)

module type COPY_IN_ARGS = sig
  val flags : copy_in_flags list
  val c : plasma_cluster
  val inode : int64
  val topo : topology
end

module Copy_in(A:COPY_IN_ARGS) = struct

  let copy_in_limit = 128 * 1024 * 1024
    (** Limit to 128M by now. Should be more intelligent. (Experience:
	* For an 1G limit there is the danger of running into timeout because
	* sync takes too long.)
     *)
    
  let no_datasync = List.mem `No_datasync A.flags
  let late_datasync = List.mem `Late_datasync A.flags

  let get_info_e mc tid =
    (** Get information: inodeinfo *)
    (Util.rpc_engine mc Fsys.V1.get_inodeinfo'async (tid,A.inode))
    >> fextract

  let dn_shm = Hashtbl.create 5



  let data_q = Queue.create()

  let get_data_buf blocksize =
    try
      Queue.take data_q
    with
      | Queue.Empty ->
	  Netsys_mem.alloc_memory_pages blocksize



  let write_block_e fdlayer blocksize (block, block_fd_pos, block_len) =
    let read_data data =
      let p = fdlayer.pread data 0 block_fd_pos (Int64.to_int block_len) in
      if (p < Int64.to_int block_len) then (
	Netlog.logf `Warning
	  "copy_in: padding with zeros inode=%Ld fd_pos=%Ld block_len=%Ld p=%d"
	  A.inode block_fd_pos block_len p;
	Plasma_platform.fill_memory data p (blocksize-p) '\000'
      ) in
    
    dlogf
      "write_block: node=%s index=%Ld" block.node block.index;
    assert(block.length = 1L);
    try
      let hp = Util.parse_host_port block.node in
      dn_session_e A.c hp
      ++ (fun dc ->
	    (** Check whether there is shm support *)
	    request_shm_e A.c dc dn_shm hp ++
	      (fun shm_opt ->
		 let no_shm_buf() =
		   let data = get_data_buf blocksize in
		   read_data data;
		   let ms =
		     Xdr_mstring.memory_based_mstrings # create_from_memory
		       data
		       0
		       blocksize
		       false in
		   let release_buf() =
		     Queue.add data data_q in
		   (`dnch_rpc ms, release_buf) in
		 let wreq, release_buf =
		   match shm_opt with
		     | None ->
			 no_shm_buf()
		     | Some shm ->
			 ( try
			     let m, idx = get_shm_buf A.c shm blocksize in
			     read_data m;
			     let release_buf() =
			       release_shm_buf shm m idx in
			     (`dnch_shm
				{ shm_path = shm.shm_opath;
				  shm_offset = Int64.of_int (idx*blocksize);
				  shm_length = blocksize
				},
			      release_buf) 
			   with
			     | error -> (* from get_shm_buf *)
				 Netlog.logf `Err
				   "Cannot get shm: %s" 
				   (Netexn.to_string error);
				 no_shm_buf()
			 ) in
		 Util.rpc_engine dc Dn.V1.write'async
		   (block.block, wreq, block.ticket)
		 >> (fun st ->
		       release_buf();
		       match st with
			 | `Error err ->
			     `Error(Datanode_error err)
			 | other ->
			     other
		    )
	      )
	 )
    with
      | error ->
	  Netlog.logf `Err
	    "Exception reading from file: %s"
	    (Netexn.to_string error);
	  eps_e (`Error error) A.c.esys



  let copy_block_e blocksize orig_block dest_block =
    dlogf
      "copy_block: from_node=%s from_index=%Ld to_node=%s to_index=%Ld"
      orig_block.node orig_block.index dest_block.node dest_block.index;
    assert(orig_block.length = 1L);
    assert(dest_block.length = 1L);
    try
      let hp = Util.parse_host_port orig_block.node in
      dn_session_e A.c hp
      ++ (fun dc ->
	    Util.rpc_engine dc Dn.V1.copy'async
	      (orig_block.block,
	       dest_block.node, dest_block.identity, dest_block.block,
	       orig_block.ticket, dest_block.ticket	      )
	 )
      >> (fun st ->
	    match st with
	      | `Error err ->
		  `Error(Datanode_error err)
	      | other ->
		  other
	 )
    with
      | error ->
	  Netlog.logf `Err
	    "Exception in copy_block_e: %s"
	    (Netexn.to_string error);
	  eps_e (`Error error) A.c.esys
  

  let dn_parallelism blocksize =
    (** Write this many blocks to a datanode in parallel.

	We use now a low number here. Higher numbers (like in the commented
	out block) have the problem that it is much likelier to run into
	timeouts. Also, high numbers mean high memory consumption on the
	datanode (flow control is _after_ the RPC messages are received
	and decoded).
     *)
    (*
	We usually want 8 blocks here, but we also want to limit the memory
	if the blocksize is high. So try to limit to 64M
     *)
    (*
    let shm_cond = A.c.shm_mng # condition `Shm in
    let lim =
      match shm_cond with
	| `Normal -> 64 * 1024 * 1024 
	| `Tight -> 16 * 1024 * 1024 in
    let lim_p = max 1 (lim / blocksize) in
    match shm_cond with
      | `Normal -> min 8 lim_p
      | `Tight -> min 4 lim_p
     *)
    2  (* the low number *)


  let rec fadvise fdlayer f ext_blocklist n =
    if n = 0 then
      ()
    else (
      match ext_blocklist with
	| x :: ext_blocklist' ->
	    let (pos,len) = f x in
	    fdlayer.fadvise pos len;
	    fadvise fdlayer f ext_blocklist' (n-1)
	| [] -> ()
    )



  let datanodes = Hashtbl.create 7
    (* remember here all nodes we got *)

  let sync_datanode node =
    let hp = Util.parse_host_port node in
    dn_session_e A.c hp
    ++ (fun dc ->
	  Util.rpc_engine dc Dn.V1.sync'async ()   (* :-) *)
       )
    >> (fun st ->
	  match st with
	    | `Error err ->
		`Error(Datanode_error err)
	    | other ->
		other
       )

  let sync_datanodes nodes =
    Plasma_util.msync_engine
      (List.map
	 (fun node ->
	    sync_datanode node
	 )
	 nodes
      )
      A.c.esys


  let sync_datanodes_ht() =
    let nodes = Hashtbl.fold (fun n _ acc -> n::acc) datanodes [] in
    sync_datanodes nodes


  let late_sync () =
    if late_datasync then
      sync_datanodes_ht()
    else
      eps_e (`Done()) A.c.esys


  let star_write_stream_e fdlayer blocksize ext_blocklist =
    let n = ref 0 in
    let ext_blocklist = ref ext_blocklist in
    let dn_par = dn_parallelism blocksize in

    let running = ref ESet.empty in

    let abort() =
      ESet.iter (fun e -> e#abort()) !running in

    let stream_e, signal =
      Plasma_util.signal_engine A.c.esys in

    let rec maybe_add_more () =
      if Netsys_posix.have_fadvise () then
	fadvise fdlayer (fun (_,pos,len) -> (pos,len)) !ext_blocklist 4;
      if !n < dn_par then (
	match !ext_blocklist with
	  | (block, block_fd_pos, block_len) :: l ->
	      ext_blocklist := l;
	      let e =
		write_block_e
		  fdlayer blocksize (block, block_fd_pos, block_len)
		++ (fun () ->
		      decr n;
		      maybe_add_more();
		      eps_e (`Done ()) A.c.esys
		   ) in
	      incr n;
	      (* Check for errors: *)
	      Uq_engines.when_state (* OK *)
		~is_done:(fun () -> running := ESet.remove e !running)
		~is_error:(fun e -> abort(); signal (`Error e))
		e;
	      running := ESet.add e !running;
	      maybe_add_more()
	  | [] ->
	      ()
      );
      if !n=0 then
	signal(`Done())
    in

    maybe_add_more();
    stream_e


  let star_write_blocks_e fdlayer blocksize ext_blocklist datasync =
    (** Split the blocklist by datanode, and write for each datanode
	sequentially. 
     *)
    let ext_blocklist_by_node, nodes =
      split_by (fun (block,_,_) -> block.node) ext_blocklist in
    List.iter (fun n -> Hashtbl.replace datanodes n ()) nodes;
    Plasma_util.msync_engine
      (List.map
	 (fun ext_blocklist' ->
	    star_write_stream_e fdlayer blocksize ext_blocklist'
	 )
	 ext_blocklist_by_node
      )
      A.c.esys
    ++ (fun () ->
	  if datasync then sync_datanodes nodes
	  else eps_e (`Done()) A.c.esys
       )


  let rec chain_copy_stream_e blocksize orig_block ext_blocklist =
    match ext_blocklist with
      | dest_block :: l ->
	  copy_block_e blocksize orig_block dest_block
	  ++ (fun () ->
		chain_copy_stream_e blocksize dest_block l
	     )
      | [] ->
	  eps_e (`Done ()) A.c.esys


  let chain_write_stream_e fdlayer blocksize ext_blocklist_by_block =
    let n = ref 0 in
    let ext_blocklist_by_block = ref ext_blocklist_by_block in
    let dn_par = dn_parallelism blocksize in

    let stream_e, signal =
      Plasma_util.signal_engine A.c.esys in

    let g = Unixqueue.new_group A.c.esys in
    let running = ref ESet.empty in

    let abort() =
      ESet.iter (fun e -> e#abort()) !running in

    let rec maybe_add_more () =
      if Netsys_posix.have_fadvise () then
	fadvise fdlayer
	  (fun l ->
	     match l with
	       | (_,pos,len) :: _ -> (pos,len)
	       | _ -> assert false
	  )
	  !ext_blocklist_by_block 4;
      if !n < dn_par then (
	match !ext_blocklist_by_block with
	  | ext_blocklist :: l ->
	      ext_blocklist_by_block := l;

	      ( match ext_blocklist with
		  | (block, block_fd_pos, block_len) :: xl ->

		      let e =
			write_block_e 
			  fdlayer blocksize (block, block_fd_pos, block_len)
			++ (fun () ->
			      chain_copy_stream_e 
				blocksize block 
				(List.map (fun (b,_,_) -> b) xl)
			   ) 
			++ (fun () ->
			      decr n;
			      maybe_add_more();
			      eps_e (`Done ()) A.c.esys
			   ) in
		      incr n;
		      (* Check for errors: *)
		      Uq_engines.when_state  (* OK *)
			~is_done:(fun () -> running := ESet.remove e !running)
			~is_error:(fun e -> abort(); signal (`Error e))
			e;
		      running := ESet.add e !running;

		      (* This ensures that the RPCs are submitted in the
			 right order:
		       *)
		      Unixqueue.once A.c.esys g 0.0 maybe_add_more
		  | [] ->
		      assert false
	      )
	  | [] ->
	      ()
      );
      if !n=0 then
	signal(`Done())
    in

    maybe_add_more();
    stream_e


  let chain_write_blocks_e fdlayer blocksize ext_blocklist datasync =
    (** Split the blocklist by blocks
     *)
    let ext_blocklist_by_block0, _ =
      split_by (fun (block,_,_) -> block.index) ext_blocklist in
    let _, nodes =
      split_by (fun (block,_,_) -> block.node) ext_blocklist in

    let ext_blocklist_by_block =
      List.sort
	(fun bl1 bl2 ->
	   match bl1, bl2 with
	     | ((b1,_,_) :: _), ((b2,_,_) :: _) -> compare b1.block b2.block
	     | _ -> assert false
	)
	ext_blocklist_by_block0 in

    List.iter (fun n -> Hashtbl.replace datanodes n ()) nodes;
    chain_write_stream_e fdlayer blocksize ext_blocklist_by_block
    ++ (fun () ->
	  if datasync then sync_datanodes nodes
	  else eps_e (`Done()) A.c.esys
       )


  let write_blocks_e =
    match A.topo with
      | `Star -> star_write_blocks_e
      | `Chain -> chain_write_blocks_e 


  let upload_e fdlayer mc ii blocksize fd_pos pos len datasync =
    (** Uploads data from [fd_pos] to [fd_pos+len] in a transaction.
	The data is written to [pos] to [pos+len].
        This includes block allocation, and writing data blocks, and
        updating EOF.
     *)
    assert(Int64.rem pos (Int64.of_int blocksize) = 0L);
    let blocks =
      Int64.succ (Int64.div (Int64.pred len) (Int64.of_int blocksize)) in
    let idx =
      Int64.div pos (Int64.of_int blocksize) in
    let eof =
      max ii.eof (Int64.add pos len) in
    let pref =
      get_pref_nodes A.c A.inode in
    with_new_nn_transaction_e A.c mc no_extract
      (fun mc trans ->
	 ( Util.rpc_engine 
	     mc 
	     Fsys.V1.allocate_blocks'async
	     (trans.tid,A.inode,idx,blocks,true,Array.of_list pref)
	   >> fextract
	 ) 
	 ++ (fun blocklist ->
	       let blocklist = 
		 Plasma_inodecache.expand_blocklist
		   (Array.to_list blocklist) in
	       let ext_blocklist =
		 List.map
		   (fun block ->
		      let rel_pos =
			Int64.mul 
			  (Int64.sub block.index idx) 
			  (Int64.of_int blocksize) in
		      let block_fd_pos =
			Int64.add fd_pos rel_pos in
		      let block_len =
			min 
			  (Int64.sub len rel_pos)
			  (Int64.of_int blocksize) in
		      (block, block_fd_pos, block_len)
		   )
		   blocklist in
	       write_blocks_e
		 fdlayer blocksize ext_blocklist datasync
	       ++ (fun _ ->
		     Util.rpc_engine 
		       mc
		       Fsys.V1.update_inodeinfo'async
		       (trans.tid, A.inode, 
			{ ii with 
			    eof = eof;
			    mtime = { tsecs = (-1L); tnsecs = 0 };
			})
		     >> fextract_unit
		  )
	    )
      )

  let upload_retry_e fdlayer mc ii blocksize fd_pos pos len datasync =
    if List.mem `Retry A.flags then
      retry_e
	A.c ("inode_" ^ Int64.to_string A.inode)
	(upload_e fdlayer mc ii blocksize fd_pos pos len) datasync
    else
      upload_e fdlayer mc ii blocksize fd_pos pos len datasync


  let rec upload_rest_e fdlayer mc ii blocksize fd_pos pos len =
    (** Uploads the remaining data, starting at pos/fd_pos.
	Finally sync each datanode.
     *)
    let blocksizeL = Int64.of_int blocksize in
    let len_limit = 
      max blocksizeL (Int64.of_int copy_in_limit) in
    let eff_len =
      min len len_limit in
    if eff_len > 0L then
      let fd_pos' = Int64.add fd_pos eff_len in
      let pos' = Int64.add pos eff_len in
      let len' = Int64.sub len eff_len in
      let no_datasync = no_datasync || late_datasync in
      upload_retry_e 
	fdlayer mc ii blocksize fd_pos pos eff_len (not no_datasync)
      ++ (fun _ ->
	    upload_rest_e fdlayer mc ii blocksize fd_pos' pos' len'
	 )
    else
      eps_e (`Done()) A.c.esys


  let clean_shm() =
    Hashtbl.iter 
      (fun _ e -> 
	 match e with
	   | `Supported shm -> destroy_shm A.c shm
	   | `Requesting (e,shm) -> destroy_shm A.c shm; e#abort()
	   | _ -> ()
      ) 
      dn_shm



  let upload_seekable_e fd mc ii blocksize fd_pos pos len =
    let fdlayer = fd_layer_of_fd fd in
    upload_rest_e fdlayer mc ii blocksize fd_pos pos len
    ++ late_sync
    ++ (fun () ->
	  eps_e (`Done len) A.c.esys
       )

  let rec input_e io_dev sm pos len acc =
    (* Special version of input: Only if end of file is reached, less than
       len bytes are read. Now n<len means EOF. Call with acc=0
     *)
    Uq_engines.meta_engine
      (Uq_io.input_e io_dev sm pos len)
    ++ (fun st ->
	  match st with
	    | `Done n ->
		if n < len then
		  input_e io_dev sm (pos+n) (len-n) (acc+n)
		else
		  eps_e (`Done (acc + n)) A.c.esys
	    | `Error End_of_file ->
		eps_e (`Done acc) A.c.esys
	    | `Error err ->
		eps_e (`Error err) A.c.esys
	    | `Aborted ->
		eps_e `Aborted A.c.esys
       )


  let rec upload_nonseekable_e eff_fd buf buf_size mc ii blocksize pos acc_len =
    (* buf_size is a multiple of blocksize *)
    let io_dev = `Polldescr(Netsys.get_fd_style eff_fd,eff_fd,A.c.esys) in
    input_e io_dev (`Memory buf) 0 buf_size 0
    ++ (fun n ->
	  let have_next_chunk = (n = buf_size) in
	  let fdlayer =
	    fd_layer_of_buf buf 0L buf_size in
	  upload_rest_e 
	    fdlayer mc ii blocksize 0L pos (Int64.of_int n)
	  ++ (fun _ ->
		let acc_len' = Int64.add acc_len (Int64.of_int n) in
		if have_next_chunk then (
		  (* Next chunk: *)
		  let pos' = Int64.add pos (Int64.of_int n) in
		  upload_nonseekable_e
		    eff_fd buf buf_size mc ii blocksize pos' acc_len'
		)
		else
		  late_sync()
		  ++ (fun () -> eps_e (`Done acc_len') A.c.esys)
	     )
       )
end (* of Copy_in *)
(* ---------------------------------------------------------------------- *)



let copy_in_e ?(flags=[]) c inode pos req_fd len topo =
  if pos < 0L || len < 0L then invalid_arg "Plasma_client.copy_in_e";

  let module A = struct
    let flags = flags
    let c = c
    let inode = inode
    let topo = topo
  end in

  let module C = Copy_in(A) in

  with_nn_session_e c
    (fun mc ->
       with_new_nn_transaction_e c mc no_extract
	 (fun mc trans ->
	    C.get_info_e mc trans.tid
	    ++ (fun ii ->
		  let blocksize = c.blocksize in
		  try
		    let fd_pos =
		      Unix.LargeFile.lseek req_fd 0L Unix.SEEK_CUR in
		    Uq_engines.meta_engine
		      (C.upload_seekable_e 
			 req_fd mc ii blocksize fd_pos pos len)
		    ++ (fun st ->
			  C.clean_shm();
			  eps_e (st :> _ Uq_engines.engine_state) c.esys
		       )
		  with
		    | Unix.Unix_error (Unix.ESPIPE,_,_) ->
			(* Work around with an extra buffer *)
			let buf_size =
			  max blocksize
			    (min (64 * blocksize) C.copy_in_limit) in
			let shm_mng = shm_manager c in
			let shm_id = shm_mng # new_id `Buf in
			shm_mng # book_allocation shm_id buf_size;
			c.shm_clean <- shm_id :: c.shm_clean;
			let buf = 
			  Netsys_mem.alloc_memory_pages buf_size in
			Uq_engines.meta_engine
			  ( C.upload_nonseekable_e
			      req_fd buf buf_size mc ii blocksize pos 0L)
			++ (fun st ->
			      C.clean_shm();
			      Netsys_mem.memory_unmap_file buf;
			      shm_mng # book_allocation shm_id 0;
			      eps_e (st :> _ Uq_engines.engine_state) c.esys
			   )
		 )
	 )
    )


let copy_in ?flags c inode pos fd len topo = 
  sync (copy_in_e ?flags c inode pos fd len) topo


let copy_in_from_buf_e ?(flags=[]) c inode pos buf len topo =
  if pos < 0L || len < 0 then 
    invalid_arg "Plasma_client.copy_in_from_buf_e";
  if len > Bigarray.Array1.dim buf then
    invalid_arg "Plasma_client.copy_in_from_buf_e";

  let module A = struct
    let flags = flags
    let c = c
    let inode = inode
    let topo = topo
  end in

  let module C = Copy_in(A) in

  with_nn_session_e c
    (fun mc ->
       with_new_nn_transaction_e c mc no_extract
	 (fun mc trans ->
	    C.get_info_e mc trans.tid
	    ++ (fun ii ->
		  let blocksize = c.blocksize in
		  let fdlayer =
		    fd_layer_of_buf buf 0L len in
		  C.upload_rest_e 
		    fdlayer mc ii blocksize 0L pos (Int64.of_int len)
		  >> (fun st ->
			C.clean_shm(); st
		     )
	       )
	    ++ (fun () ->
		  eps_e (`Done len) c.esys
	       )
	 )
    )

let copy_in_from_buf ?flags c inode pos buf len topo =
  sync (copy_in_from_buf_e ?flags c inode pos buf len) topo


(* ---------------------------------------------------------------------- *)
(* because copy_out is quite complicated we put it into a functor that is
   instantiated on every copy_in invocation
 *)

module type COPY_OUT_ARGS = sig
  val flags : copy_out_flags list
  val c : plasma_cluster
  val inode : int64
end


module Copy_out(A:COPY_OUT_ARGS) = struct

  exception Stream_mode

  let no_trunc = List.mem `No_truncate A.flags

  let get_info_e mc tid =
    (** Get information: inodeinfo *)
    Util.rpc_engine mc Fsys.V1.get_inodeinfo'async (tid,A.inode)
    >> fextract

  
  let dn_shm = Hashtbl.create 5

  let read_block_e fdlayer blocksize (block, block_fd_pos, block_len) =
    dlogf
      "read_block: node=%s index=%Ld" block.node block.index;
    assert(block.length = 1L);
    let hp = Util.parse_host_port block.node in
    let blen = Int64.to_int block_len in
    dn_session_e A.c hp
    ++ (fun dc ->
	  (** Check whether there is shm support *)
	  request_shm_e A.c dc dn_shm hp ++
	    (fun shm_opt ->
	       let rreq, shm_details =
		 match shm_opt with
		   | None ->
		       (`dnch_rpc, None)
		   | Some shm ->
		       ( try
			   let m, idx = get_shm_buf A.c shm blocksize in
			   (`dnch_shm
			      { shm_path = shm.shm_opath;
				shm_offset = Int64.of_int (idx*blocksize);
				shm_length = blocksize
			      },
			    Some(shm,m,idx)
			   )
			 with
			   | error ->
			       Netlog.logf `Err
				 "Cannot get shm: %s"
				 (Netexn.to_string error);
			       (`dnch_rpc, None)
		       ) in
	       ( Util.rpc_engine 
		   dc Dn.V1.read'async 
		   (rreq, block.block, 0, blen, block.ticket)
		 >> (fun st ->
		       match st with
			 | `Error err ->
			     `Error(Datanode_error err)
			 | other ->
			     other
		    )
	       )
	       ++ (fun data_designator ->
		     try
		       ( match data_designator, shm_details with
			   | `dnch_rpc data, None ->
			       let data, offs = data # as_memory in
			       fdlayer.pwrite data offs block_fd_pos blen
			   | `dnch_shm, Some(shm,m,idx) ->
			       fdlayer.pwrite m 0 block_fd_pos blen;
			       release_shm_buf shm m idx
			   | _ ->
			       assert false
		       );
		       eps_e (`Done ()) A.c.esys
		     with
		       | error ->
			   Netlog.logf `Err
			     "Exception writing to file: %s"
			     (Netexn.to_string error);
			   eps_e (`Error error) A.c.esys
		  )
	    )
       )

  
  (** Reading in optimal order is complicated:
      - each block is on several datanodes, and we can pick one
       - the datanodes may be loaded differently, so the read speed may
         also differ
       - we can request several blocks from each datanode in parallel

      Solution:
       - for each datanode we have a separate stream of blocks that are
         being read
       - for each stream we request N blocks at the same time. If a
         block is returned to us, we request another one

   *)

  let requested = Hashtbl.create 113 
  (** If a block is requested from a datanode, the block is put into this
      hash table, so that the streams for other datanodes can skip this
   *)

  let dn_parallelism blocksize =
    (** Read this many blocks to a datanode in parallel.
	
	We usually want 8 blocks here, but we also want to limit the memory
	if the blocksize is high. So try to limit to 64M
     *)
    let shm_cond = A.c.shm_mng # condition `Shm in
    let lim =
      match shm_cond with
	| `Normal -> 64 * 1024 * 1024 
	| `Tight -> 16 * 1024 * 1024 in
    let lim_p = max 1 (lim / blocksize) in
    match shm_cond with
      | `Normal -> min 8 lim_p
      | `Tight -> min 4 lim_p 


  let read_stream_e fdlayer blocksize ext_blocklist =
    (** Reads the blocks which are all from the same datanode. Skip blocks
	already requested (from other datanodes)

	FIXME: When timeouts or other socket errors occur, remove the
	request again, so that the block is fetched from another datanode.
	Also, ensure that all blocks are finally requested (i.e. restart
	reading if not)
     *)
    let n = ref 0 in
    let ext_blocklist = ref ext_blocklist in
    let dn_par = dn_parallelism blocksize in

    let stream_e, signal =
      Plasma_util.signal_engine A.c.esys in

    let running = ref ESet.empty in

    let abort() =
      ESet.iter (fun e -> e#abort()) !running in

    let rec maybe_add_more () =
      if !n < dn_par then (
	match !ext_blocklist with
	  | (block, block_fd_pos, block_len) :: l ->
	      ext_blocklist := l;
	      if Hashtbl.mem requested block.index then
		maybe_add_more()
	      else (
		Hashtbl.add requested block.index ();
		let e =
		  read_block_e 
		    fdlayer blocksize (block, block_fd_pos, block_len)
		  ++ (fun () ->
			decr n;
			maybe_add_more();
			eps_e (`Done ()) A.c.esys
		     ) in
		incr n;
		(* Check for errors: *)
		Uq_engines.when_state  (* OK *)
		  ~is_done:(fun () -> running := ESet.remove e !running)
		  ~is_error:(fun e -> abort(); signal (`Error e))
		  e;
		running := ESet.add e !running;
		maybe_add_more()
	      )
	  | [] ->
	      ()
      );
      if !n=0 then
	signal(`Done())
    in

    maybe_add_more();
    stream_e


  let read_blocks_e fdlayer blocksize ext_blocklist =
    (** Split the blocklist by datanode, and read for each datanode
	sequentially. 
     *)
    Hashtbl.clear requested;
    let ext_blocklist_by_node, nodes =
      split_by (fun (block,_,_) -> block.node) ext_blocklist in
    Plasma_util.msync_engine
      (List.map
	 (fun ext_blocklist' ->
	    read_stream_e fdlayer blocksize ext_blocklist'
	 )
	 ext_blocklist_by_node
      )
      A.c.esys


  let download_e fdlayer mc tid ii blocksize fd_pos pos len =
    (** Downloads data from the Plasma file position [pos] to [pos+len-1]
	(in bytes) to the fdlayer at buffer position [fd_pos] to
	[fd_pos+len-1]
     *)
    let blocksL =
      Int64.succ (Int64.div (Int64.pred len) (Int64.of_int blocksize)) in
    let blocks =
      Int64.to_int blocksL in
    let idx =                           (* block index of the Plasma file *)
      Int64.div pos (Int64.of_int blocksize) in
    let blocksizeL =
      Int64.of_int blocksize in
    ( Util.rpc_engine 
	mc Fsys.V1.get_blocks'async(tid,A.inode,idx,blocksL,0L,true)
      >> fextract
    ) 
    ++ (fun blocklist ->
	  let blocklist = 
	    Plasma_inodecache.expand_blocklist (Array.to_list blocklist) in
	  (** We only filter here dead nodes out, and check that
	      each block is available on at least one node.
	   *)
	  let block_ht = Hashtbl.create 7 in
	  List.iter
	    (fun block ->
	       Hashtbl.replace block_ht block.index false
	    )
	    blocklist;
	  
	  let ext_blocklist =
	    List.map
	      (fun block ->
		 Hashtbl.replace block_ht block.index true;
		 let rel_pos =
		   Int64.mul 
		     (Int64.sub block.index idx) 
		     blocksizeL in
		 let block_fd_pos =
		   Int64.add fd_pos rel_pos in
		 (* block_fd_pos is the position in the buffer for this block *)
		 let block_len =
		   min (Int64.sub len rel_pos) blocksizeL in
		 (block, block_fd_pos, block_len)
	      )
	      (List.filter
		 (fun block ->
		    block.node_alive)
		 blocklist) in
	  
	  let all_ok =
	    Hashtbl.fold (fun _ ok acc -> ok && acc) block_ht true in

	  (* Fill file holes: *)
	  for k = 0 to blocks - 1 do
	    let i = Int64.add idx (Int64.of_int k) in
	    if not (Hashtbl.mem block_ht i) then (
	      let rel_pos =
		Int64.mul (Int64.sub i idx) blocksizeL in
	      let block_fd_pos =
		Int64.add fd_pos rel_pos in
	      let block_len =
		min (Int64.sub len rel_pos) blocksizeL in
	      fdlayer.zero block_fd_pos (Int64.to_int block_len)
	    )
	  done;
	  
	  if all_ok then
	    read_blocks_e fdlayer blocksize ext_blocklist
	  else
	    plasma_error_e A.c `eio
       )

  let download_trans_e fdlayer mc ii blocksize fd_pos pos len =
    (* Do this in a new transaction - just to avoid that too many blocks
       need to be pinned
     *)
    with_new_nn_transaction_e A.c mc no_extract
      (fun mc trans ->
	 download_e fdlayer mc trans.tid ii blocksize fd_pos pos len
      )

  let download_trans_retry_e fdlayer mc ii blocksize fd_pos pos len =
    if List.mem `Retry A.flags then
      retry_e
	A.c ("inode_" ^ Int64.to_string A.inode)
	(download_trans_e fdlayer mc ii blocksize fd_pos pos) len
    else
      download_trans_e fdlayer mc ii blocksize fd_pos pos len

  let clean_shm() =
    Hashtbl.iter
      (fun _ e -> 
	 match e with
	   | `Supported shm -> destroy_shm A.c shm
	   | `Requesting (e,shm) -> destroy_shm A.c shm; e#abort()
	   | _ -> ()
      )
      dn_shm

  let rec download_rest_e fdlayer mc ii blocksize fd_pos pos len =
    (** Downloads the remaining data, starting at pos/fd_pos *)
    let blocksizeL = Int64.of_int blocksize in
    let len_limit = 
      (** Limit to 1G by now. Should be more intelligent *)
      max blocksizeL 1073741824L in
    let eff_len =
      min len len_limit in
    new Uq_engines.seq_engine
      (download_trans_retry_e fdlayer mc ii blocksize fd_pos pos eff_len)
      (fun _ ->
	 if eff_len < len then
	   let fd_pos' = Int64.add fd_pos eff_len in
	   let pos' = Int64.add pos eff_len in
	   let len' = Int64.sub len eff_len in
	   download_rest_e fdlayer mc ii blocksize fd_pos' pos' len'
	 else
	   new Uq_engines.epsilon_engine (`Done ()) A.c.esys
      )


  let download_seekable_e fd mc ii blocksize fd_pos pos len =
    let fd_eof =
      Unix.LargeFile.lseek fd 0L Unix.SEEK_END in

    if fd_pos > Int64.sub fd_eof len && not no_trunc then (
      let new_eof = Int64.add fd_pos len in
      dlogf "copy_out (pid %d): truncate fd=%Ld new_eof=%Ld"
	(Unix.getpid()) (Netsys.int64_of_file_descr fd) new_eof;
      Unix.LargeFile.ftruncate fd new_eof
    );
    let fdlayer = fd_layer_of_fd fd in
    download_rest_e fdlayer mc ii blocksize fd_pos pos len
    ++ (fun () ->
	  eps_e (`Done len) A.c.esys
       )
    

  let rec download_nonseekable_e 
        req_fd buf buf_size mc ii blocksize pos len acc_len =
    (* buf_size is a multiple of blocksize *)
    let eff_len = min len (Int64.of_int buf_size) in
    let fdlayer = fd_layer_of_buf buf 0L buf_size in
    download_rest_e fdlayer mc ii blocksize 0L pos eff_len
    ++ (fun _ ->
	  (* Print chunk *)
	  let io_dev = `Polldescr(Netsys.get_fd_style req_fd,req_fd,A.c.esys) in
	  Uq_io.really_output_e io_dev (`Memory buf) 0 (Int64.to_int eff_len)
	  ++ (fun () ->
		(* Next chunk: *)
		let acc_len' = Int64.add acc_len eff_len in
		if eff_len < len then
		  let pos' = Int64.add pos eff_len in
		  let len' = Int64.sub len eff_len in
		  download_nonseekable_e
		    req_fd buf buf_size mc ii blocksize pos' len' acc_len'
		else
		  eps_e (`Done acc_len') A.c.esys
	     )
       )
end (* of Copy_out *)
(* ---------------------------------------------------------------------- *)



let copy_out_e ?(flags=[]) c inode pos req_fd len =
  if pos < 0L || len < 0L then invalid_arg "Plasma_client.copy_out_e";

  let module A = struct
    let flags = flags
    let c = c 
    let inode = inode
  end in

  let module C = Copy_out(A) in

  with_nn_session_e c
    (fun mc ->
       with_new_nn_transaction_e c mc no_extract
	 (fun mc trans ->
	    C.get_info_e mc trans.tid
	    ++ (fun ii ->
		  let blocksize = c.blocksize in
		  try
		    (* On Linux, /dev/null is seekable! But one cannot
		       set the file position past EOF (which is always 0)
		     *)
		    let fd_pos =
		      Unix.LargeFile.lseek req_fd 0L Unix.SEEK_CUR in
		    ignore(Unix.lseek req_fd 1 Unix.SEEK_SET);
		    if Unix.lseek req_fd 0 Unix.SEEK_CUR <> 1 then
		      raise C.Stream_mode;
		    Uq_engines.meta_engine
		      (C.download_seekable_e
			 req_fd mc ii blocksize fd_pos pos len)
		    ++ (fun st ->
			  C.clean_shm();
			  eps_e (st :> _ Uq_engines.engine_state) c.esys
		       )
		  with
		    | Unix.Unix_error (Unix.ESPIPE,_,_) | C.Stream_mode ->
			(* Work around by allocating a buffer *)
			let buf_size =
			  max blocksize
			    (min (64 * blocksize) (256 * 1024 * 1024)) in
			let shm_mng = shm_manager c in
			let shm_id = shm_mng # new_id `Buf in
			shm_mng # book_allocation shm_id buf_size;
			c.shm_clean <- shm_id :: c.shm_clean;
			let buf = Netsys_mem.alloc_memory_pages buf_size in
			Uq_engines.meta_engine
			  ( C.download_nonseekable_e 
			      req_fd buf buf_size mc ii blocksize pos len 0L )
			++ (fun st ->
			      C.clean_shm();
			      Netsys_mem.memory_unmap_file buf;
			      shm_mng # book_allocation shm_id 0;
			      eps_e (st :> _ Uq_engines.engine_state) c.esys
			   )
	       )
	 )
    )
    

let copy_out ?flags c inode pos fd len = 
  sync (copy_out_e ?flags c inode pos fd) len


let copy_out_to_buf_e ?(flags=[]) c inode pos buf len =
  if pos < 0L || len < 0 then
    invalid_arg "Plasma_client.copy_out_to_buf_e";
  if len > Bigarray.Array1.dim buf then
    invalid_arg "Plasma_client.copy_out_to_buf_e";

  let module A = struct
    let flags = flags
    let c = c 
    let inode = inode
  end in

  let module C = Copy_out(A) in

  with_nn_session_e c
    (fun mc ->
       with_new_nn_transaction_e c mc no_extract
	 (fun mc trans ->
	    C.get_info_e mc trans.tid
	    ++ (fun ii ->
		  let blocksize = c.blocksize in
		  let fdlayer = fd_layer_of_buf buf 0L len in
		  C.download_rest_e
		    fdlayer mc ii blocksize 0L pos (Int64.of_int len)
		  >> (fun st ->
			C.clean_shm(); st
		     )
	       )
	    ++ (fun () ->
		  eps_e (`Done len) c.esys
	       )
	 )
    )
    
let copy_out_to_buf ?flags c inode pos buf len =
  sync (copy_out_to_buf_e ?flags c inode pos buf) len


let read_admin_table_e c key =
  with_nn_session_e c
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.read_admin_table'async key
       ++ (function
	     | `ok contents ->
		 eps_e (`Done contents) c.esys
	     | #errno as code ->
		 plasma_error_e c code
	  )
    )

let read_admin_table c key = sync (read_admin_table_e c) key

let read_ug_admin_e c =
  let rec loop_e keys acc =
    match keys with
      | key :: keys' ->
	  read_admin_table_e c key
	  ++ (fun data -> loop_e keys' ((key,data) :: acc))
      | [] ->
	  let ug = Plasma_ug.parse_ug_admin acc in
	  eps_e (`Done ug) c.esys in
  loop_e Plasma_ug.req_admin_tables []

let read_ug_admin c = sync read_ug_admin_e c


let write_admin_table_e c key contents =
  with_nn_session_e c
    (fun mc ->
       Util.rpc_engine mc Fsys.V1.write_admin_table'async (key,contents)
       ++ (function
	     | `ok ->
		 eps_e (`Done()) c.esys
	     | #errno as code ->
		 plasma_error_e c code
	  )
    )

let write_admin_table c key contents = sync (write_admin_table_e c key) contents

let write_ug_admin_e c ug =
  let rec loop_e tabs =
    match tabs with
      | (key,data) :: tabs' ->
	  write_admin_table_e c key data
	  ++ (fun () -> loop_e tabs')
      | [] ->
	  eps_e (`Done ()) c.esys in
  loop_e ug#admin_tables

let write_ug_admin c ug = sync (write_ug_admin_e c) ug


