(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let dlogf = Plasma_util.dlogf

type id = [`Shm_id of int | `Buf_id of int ]
type kind = [`Shm | `Buf ]
type condition = [ `Normal | `Tight ]
type watermarks =
    { low : int64;
      high : int64;
      max : int64;
    }

exception Shm_max_exceeded
exception Buf_max_exceeded

class type shm_manager =
object
  method new_id : kind -> id
  method book_allocation : id -> int -> unit
  method delayed_allocation : id -> int -> unit
  method condition : kind -> condition
  method push : unit -> unit
  method wm : kind -> watermarks
  method dump : string
  method have_local_allocations : bool
end


class null_shm_manager () : shm_manager =
object(self)
  val mutable next_id = 0
  method new_id kind =
    let id = next_id in
    next_id <- id+1;
    match kind with
      | `Shm -> `Shm_id id
      | `Buf -> `Buf_id id
  method book_allocation _ _ = ()
  method delayed_allocation _ _ = ()
  method condition _ = `Normal
  method push() = ()
  method wm _ = { low = 0L; high = Int64.max_int; max = Int64.max_int }
  method dump = "NO DUMP from null_shm_manager"
  method have_local_allocations = false
end


(** For the Netplex shm manager, we keep track of allocated memory
    per component. For each component there is a variable with name
    "plasma_shm.shm.sum.<cid>" where <cid> is a component ID. This variable
    contains the sum of all allocated shm.

    We configure that Netplex deletes this variable automatically when
    the component terminates.

    There is also "plasma_shm.shm.directory" which is a list of all
    of the above sum variables. The mutex "plasma_shm.dir_mutex" is used
    to control access to the directory variable.
 *)

module Shm_sum_var =
  Netplex_sharedvar.Make_var_type (struct type t = int64 end)

module Shm_dir_var =
  Netplex_sharedvar.Make_var_type (struct type t = string list end)

module Shm_watermarks_var =
  Netplex_sharedvar.Make_var_type (struct type t = watermarks end)

module Shm_condition_var =
  Netplex_sharedvar.Make_var_type (struct type t = condition end)


let mutex_name = "plasma_shm.dir_mutex"

let sum_prefix =
  function
    | `Shm -> "plasma_shm.shm.sum" 
    | `Buf -> "plasma_shm.buf.sum" 

let dir_name =
  function
    | `Shm -> "plasma_shm.shm.directory" 
    | `Buf -> "plasma_shm.buf.directory" 

let wm_name = 
  function
    | `Shm -> "plasma_shm.shm.watermarks"
    | `Buf -> "plasma_shm.buf.watermarks"

let cond_name =
  function
    | `Shm -> "plasma_shm.shm.condition"
    | `Buf -> "plasma_shm.buf.condition"

let string_of_kind =
  function
    | `Shm -> "SHM"
    | `Buf -> "BUF"

let (kind_of_id : id -> kind) =
  function
    | `Shm_id _ -> `Shm
    | `Buf_id _ -> `Buf

let (string_of_id : id -> string) =
  function
    | `Shm_id n -> sprintf "Shm_id(%d)" n
    | `Buf_id n -> sprintf "Buf_id(%d)" n


let wait_for_enc_value name =
  match Netplex_sharedvar.wait_for_enc_value name with
    | None -> raise (Netplex_sharedvar.Sharedvar_not_found name)
    | Some _ -> ()


let modify_sum_var kind f =
  let created = Netplex_sharedvar.create_var ~enc:true (dir_name kind) in
  if created then (
    Shm_dir_var.set (dir_name kind) ( f [] )
  )
  else (
    wait_for_enc_value (dir_name kind);
    let mutex = Netplex_mutex.access mutex_name in
    Netplex_mutex.lock mutex;
    let l = Shm_dir_var.get (dir_name kind) in
    Shm_dir_var.set (dir_name kind) (f l);
    Netplex_mutex.unlock mutex;
  )

let announce_sum_var kind sum_name =
  modify_sum_var kind (fun l -> sum_name :: l)

let revoke_sum_var kind sum_name =
  modify_sum_var kind (fun l -> List.filter (fun n -> n <> sum_name) l)


let get_global_sum kind =
  let sum_names = 
    try
      wait_for_enc_value (dir_name kind);
      Shm_dir_var.get (dir_name kind)
    with Netplex_sharedvar.Sharedvar_not_found _ -> [] in
  List.fold_left
    (fun acc sum_name ->
       try
	 Int64.add acc (Shm_sum_var.get sum_name) 
       with
	 | Netplex_sharedvar.Sharedvar_not_found _ ->
	     (* In this case we should also remove the variable
		from the directory 
	      *)
	     revoke_sum_var kind sum_name;
	     acc
    )
    0L
    sum_names


let get_watermarks kind =
  try
    wait_for_enc_value (wm_name kind);
    Shm_watermarks_var.get (wm_name kind)
  with
    | Netplex_sharedvar.Sharedvar_not_found _ -> 
	{ low = 0L; high = Int64.max_int; max = Int64.max_int }


let init_watermarks kind wm =
  let _ =
     Netplex_sharedvar.create_var ~enc:true (wm_name kind) in
  Shm_watermarks_var.set (wm_name kind) wm


let get_condition kind =
  try
    wait_for_enc_value (cond_name kind);
    Shm_condition_var.get (cond_name kind)
  with
    | Netplex_sharedvar.Sharedvar_not_found _ -> `Normal
  

let set_condition kind c =
  let _ =
     Netplex_sharedvar.create_var ~enc:true (cond_name kind) in
  Shm_condition_var.set (cond_name kind) c


let push_sum kind sum =
  let cont = Netplex_cenv.self_cont() in
  let sum_name = 
    (sum_prefix kind) ^ "." ^ string_of_int (Oo.id cont#container_id) in
  (* N.B. container IDs are unique in the whole Netplex system *)
  let created =  Netplex_sharedvar.create_var ~own:true ~enc:true sum_name in
  Shm_sum_var.set sum_name sum;
  if created then
    announce_sum_var kind sum_name;
  let gsum = get_global_sum kind in
  dlogf
    "Plasma_shm: %s local_sum = %Ld global_sum = %Ld"
    (string_of_kind kind) sum gsum;
  let wm = get_watermarks kind in
  let c = get_condition kind in
  if c = `Normal && gsum >= wm.high then
    set_condition kind `Tight
  else
    if c = `Tight && gsum <= wm.low then
      set_condition kind `Normal


class netplex_shm_manager() : shm_manager =
  let shm_account = (Hashtbl.create 5 : (id, _) Hashtbl.t) in
  let buf_account = (Hashtbl.create 5 : (id, _) Hashtbl.t) in

  let account kind =
    match kind with
      | `Shm -> shm_account
      | `Buf -> buf_account in

  let local_sum kind =
    Hashtbl.fold
      (fun _ size acc -> Int64.add acc (Int64.of_int size))
      (account kind)
      0L in
object(self)

  val mutable next_id = 0

  method new_id kind =
    let id = next_id in
    next_id <- id+1;
    match kind with
      | `Shm -> `Shm_id id
      | `Buf -> `Buf_id id


  method book_allocation id size =
    let kind = kind_of_id id in
    dlogf
      "Plasma_shm.book_allocation id=%s size=%d"
      (string_of_id id) size;
    let wm = get_watermarks kind in
    let old_sum = local_sum kind in
    let old_size = 
      try Hashtbl.find (account kind) id with Not_found -> 0 in
    Hashtbl.replace (account kind) id size;
    let sum = local_sum kind in
    let gsum = get_global_sum kind in
    if sum > old_sum && gsum > wm.max then (
      dlogf "Plasma_shm: %s maximum exceeded" (string_of_kind kind);
      Hashtbl.replace (account kind) id old_size;
      match kind with
	| `Shm -> raise Shm_max_exceeded
	| `Buf -> raise Buf_max_exceeded
    );
    push_sum kind sum

  method delayed_allocation id size =
    dlogf
      "Plasma_shm.delayed_allocation id=%s size=%d"
      (string_of_id id) size;
    let kind = kind_of_id id in
    Hashtbl.replace (account kind) id size
    

  method condition kind =
    let c = get_condition kind in
    dlogf
      "Plasma_shm: %s condition = %s"
      (string_of_kind kind)
      ( match c with
	  | `Normal -> "normal"
	  | `Tight -> "tight"
      );
    c

  method push () =
    let shm_sum = local_sum `Shm in
    let buf_sum = local_sum `Buf in
    push_sum `Shm shm_sum;
    push_sum `Buf buf_sum

  method wm kind =
    get_watermarks kind

  method dump =
    sprintf "PID %d - LOCAL SUM: shm=%Ld buf=%Ld - GLOBAL SUM: shm=%Ld buf=%Ld"
      (Unix.getpid())
      (local_sum `Shm)
      (local_sum `Buf)
      (get_global_sum `Shm)
      (get_global_sum `Buf)

  method have_local_allocations =
    (local_sum `Shm > 0L) || (local_sum `Buf > 0L)
end


module Netplex_shm_manager = struct
  let post_add_hook ctrl =
    ctrl # add_plugin Netplex_sharedvar.plugin;
    ctrl # add_plugin Netplex_mutex.plugin

  let init ~shm_wm ~buf_wm =
    init_watermarks `Shm shm_wm;
    init_watermarks `Buf buf_wm
end
