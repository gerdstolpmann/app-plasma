(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Uq_engines.Operators

module Coord = Pfs_rpcapi_clnt.Make'Coordination(Rpc_proxy.ManagedClient)
module Fsys = Pfs_rpcapi_clnt.Make'Filesystem(Rpc_proxy.ManagedClient)

type state =
    { mutable mclient : Rpc_proxy.ManagedClient.mclient option;
      ser : Rpc_proxy.ManagedClient.mclient Uq_engines.serializer_t;
      esys : Unixqueue.event_system;
    }


module State_var =
  Netplex_cenv.Make_var_type(struct type t = state end)

let state_name = "state"


let create_nn_client config cauth conn esys =
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Coordination.V1._program;
		  Pfs_rpcapi_clnt.Filesystem.V1._program;
		]
      ~msg_timeout:config#nn_node_timeout
      ~msg_timeout_is_fatal:true
      ~idle_timeout:5.0
      ~initial_ping:true
      ~auth_methods:(Pfs_auth.rpc_proxy_auth_methods cauth true)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  mclient


let find_coord_e config cauth esys =
  (* Find the coordinator (patterned after Plasma_client.find_coord_e) *)

  let namenodes =
    Array.of_list
      (List.map
	 (fun hp -> 
	    let (h,p) = Plasma_util.parse_host_port hp in
	    Plasma_util.sockaddr_of_host_port (h,p))
	 config#nn_nodes
      ) in

  let name = config#nn_clustername in

  let rec search_live_nn_e addr k_next k_lim first_round =
    let conn = Plasma_util.connector_of_sockaddr addr in
    let mc = create_nn_client config cauth conn esys in
    Plasma_util.failsafe_e
      (Plasma_util.rpc_engine mc Coord.V1.find_coordinator'async name)
    ++ (fun st ->
	  Rpc_proxy.ManagedClient.shut_down mc;
	  match st with
	    | `Done (Some hp) ->
		let (h,p) = Plasma_util.parse_host_port hp in
		let caddr = Plasma_util.sockaddr_of_host_port (h,p) in
		eps_e (`Done caddr) esys
	    | `Done None ->
		search_next_e k_next k_lim first_round
	    | `Error e ->
		Netlog.logf `Warning "find_coord_e: Exception %s" 
		  (Netexn.to_string e);
		search_next_e k_next k_lim first_round
	   | `Aborted ->
	       eps_e `Aborted esys
       )
  and search_next_e k_next k_lim first_round =
    if k_next = k_lim && not first_round then (
      eps_e
	(`Error (Failure "find_coord_e: cannot find coordinator"))
	esys
    ) else (
      let addr = namenodes.(k_next) in
      let k' = (k_next+1) mod (Array.length namenodes) in
      let first_round' = first_round && k' <> k_lim in
      search_live_nn_e addr k' k_lim first_round'
    )
  in
  search_live_nn_e namenodes.(0) 0 0 true


let init_mclient_e st config cauth =
  st.ser # serialized
    (fun esys ->
       find_coord_e config cauth esys
       ++ (fun addr ->
	     let conn = Plasma_util.connector_of_sockaddr addr in
	     let mc = create_nn_client config cauth conn esys in
	     st.mclient <- Some mc;
	     eps_e (`Done mc) st.esys
	  )
    )


let get_fsys_proxy_e config cauth =
  let st = State_var.get state_name in
  match st.mclient with
    | None ->
	init_mclient_e st config cauth
    | Some p ->
	if Rpc_proxy.ManagedClient.mclient_state p = `Down then
	  init_mclient_e st config cauth
	else
	  eps_e (`Done p) st.esys


exception AF

let proc_get_auth_ticket config cauth sess () emit =
  try
    let m = Rpc_server.get_auth_method sess in
    if m#name <> "AUTH_LOCAL" then raise AF;
    let u_rpc = Rpc_server.get_user sess in
    let (uid, _, _, _) = Rpc_auth_sys.parse_user_name u_rpc in
    let pw = Unix.getpwuid uid in
    let user = pw.Unix.pw_name in
    let e =
      get_fsys_proxy_e config cauth
      ++ (fun mc ->
	    Plasma_util.rpc_engine mc Fsys.V1.get_auth_ticket'async user
	 ) in
    Uq_engines.when_state  (* OK *)
      ~is_done:(fun ticket -> 
		  try emit ticket with Rpc_server.Connection_lost -> ())
      ~is_error:(fun error ->
		   Netlog.logf `Err
		     "Exception in Plasmad_auth.proc_get_auth_ticket: %s"
		     (Netexn.to_string error);
		   Rpc_server.reply_error sess Rpc.System_err
		)
      e
  with
    | AF ->
	Rpc_server.reply_error sess Rpc.Auth_failed

    | error ->
	Netlog.logf `Err
	  "Exception in Plasmad_auth.proc_get_auth_ticket: %s"
	  (Netexn.to_string error);
	Rpc_server.reply_error sess Rpc.System_err


let setup srv (cf, cfaddr, config, cauth) =
  Pfs_rpcapi_srv.Auth.V1.bind_async
    ~proc_null:(fun _ _ emit -> emit ())
    ~proc_get_auth_ticket:(proc_get_auth_ticket config cauth)
    srv;
  Rpc_server.set_auth_methods srv 
    [ Rpc_auth_local.server_auth_method();
      Rpc_server.auth_none
    ]


let factory() =
  Rpc_netplex.rpc_factory
    ~configure:(fun cf cfaddr -> 
		  let config = Nn_config.extract_min_config cf in
		  let cauth = Pfs_auth.extract_client_config cf cf#root_addr in
		  (cf, cfaddr, config, cauth)
	       )
    ~hooks:(fun (cf, cfaddr, config, cauth) ->
	    object
              inherit Netplex_kit.empty_processor_hooks()


	      method post_add_hook sockserv ctrl =
		(* Change the file mode of the Unix domain sockets *)
		List.iter
		  (fun (_, a) ->
		     Array.iter
		       (fun fd ->
			  (* At least Linux does not like fchmod on
			     an Unix domain socket!
			     http://lkml.indiana.edu/hypermail/linux/kernel/9901.2/1074.html
			   *)
			  ( match Unix.getsockname fd with
			      | Unix.ADDR_UNIX path ->
                                  ( try
				      Unix.chmod path 0o666
                                    with
                                      | _ ->
                                        Printf.eprintf
                                          "Cannot chmod %S" path
                                  )
			      | _ ->
				  ()
			  )
			  (* try Unix.fchmod fd 0o666 with _ -> () *)
		       )
		       a
		  )
		  sockserv#sockets

	      method post_start_hook cont =
		let esys = cont#event_system in
		State_var.set state_name
		  { mclient = None;
		    ser = Uq_engines.serializer esys;
		    esys = esys
		  }
	    end
	   )
    ~name:"an_manager"
    ~setup
    ()
