(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let main () =
  let (opt_list, cmdline_cfg) = Netplex_main.args() in
  let opt_list' =
    ["-debug", Arg.String (fun s -> Netlog.Debug.enable_module s),
      "<module>  Enable debug messages for <module>";

      "-debug-all", Arg.Unit (fun () -> Netlog.Debug.enable_all()),
      "  Enable all debug messages";

      "-debug-list", Arg.Unit (fun () -> 
                                 List.iter print_endline (Netlog.Debug.names());
                                 exit 0),
      "  Show possible modules for -debug, then exit";
   ] @ opt_list in
  
  Arg.parse
    opt_list'
    (fun s -> raise (Arg.Bad ("Don't know what to do with: " ^ s)))
    (sprintf "usage: %s [options]" (Filename.basename Sys.argv.(0)));

  Printexc.record_backtrace true;

  let parallelizer =
    Netplex_mp.mp ~keep_fd_open:true () in

  Plasma_util.init_debug cmdline_cfg;

  Netplex_main.startup
    ~late_initializer:(fun _ _ ->
			 Pfs_pmanage.delete_registered_objects();
			 (* Get rid of remaining files from the last time 
			    the daemon was run *)
		      )
    parallelizer
    Netplex_log.logger_factories
    Netplex_workload.workload_manager_factories
    [ Dn_manager.factory();
      Nn_manager.factory();
      Plasmad_auth.factory()
    ]
    cmdline_cfg


let () =
  main()
