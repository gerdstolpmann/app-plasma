(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Utility library for record-based I/O *)

(** A file consists here of records. A record usually needs some framing
    bytes in order to be recognizable in the file. For example, the
    representation as text file uses the LF byte to mark the end of the
    record. Other representations are also supported, see below.

    The record reader can be used to iterate over a whole file, or
    only a part. For the latter, it is assumed that the file is processed
    bigblock by bigblock. Of course, it is then possible that the records do not
    end at bigblock boundaries. However, it must be determined whether the
    block reader for bigblock N or the block reader for bigblock N+1 processes
    such records. The following rules do that:

    - The first bigblock does not have this problem. Its first record is always
      processed by the block reader for block 0.
    - For the other bigblocks we define that the first processed record
      is the first record that can be recognized as the first record
      starting in the bigblock. (See below for the details per
      representation.)
    - The last record in the bigblock to process is the record that preceeds
      the first record of the next bigblock

    For best efficiency, the block reader should not be used for
    reading individual bigblocks, but for contiguous ranges of bigblocks.
 *)

(** {1 Types} *)

class type record_config =
object
  method bigblock_size : int
    (** Same as {!Mapred_def.mapred_job_config.bigblock_size}. This is the
	requested size. The actual size is rounded up to the next
	multiple of filesystem blocks.
     *)
  method mr_buffer_size : int
    (** Same as {!Mapred_config.mapred_config.mr_buffer_size}. Also
	rounded up.
     *)
  method mr_buffer_size_tight : int
    (** Same as {!Mapred_config.mapred_config.mr_buffer_size_tight}.
	Also rounded up.
     *)
end

class type record_reader =
object
  method pos_in : int
    (** The ordinal number of the record that will be read next. Numbers
	start at 0
     *)

  method input_record : unit -> string
    (** Reads one record, and advances the cursor. May raise [End_of_file] *)

  method peek_record : unit -> string
    (** Reads the record, but does not advance the cursor, i.e. the
	next [peek_record] or [input_record] will read the line again.
	May raise [End_of_file].
     *)

  method input_records : string Queue.t -> unit
    (** Reads some records and appends them to the passed queue. It
	is at least one record read. Will raise [End_of_file] if
	the end of the file is reached.
     *)

  method close_in : unit -> unit
    (** Releases resources (e.g. closes transactions) *)

  method abort : unit -> unit
    (** Drops resources - intended to be used for error cleanup *)

  method to_fd_e : Unix.file_descr -> Unixqueue.event_system -> 
                      unit Uq_engines.engine
    (** [to_fd_e fd esys]: The records are written to [fd].
	The position [pos_in] is not updated. The length of the records
	is not checked except for a few records that are crucial for
	interpreting the boundaries of the bigblocks.

	One is only allowed to use either [to_fd_e] or [input_record],
	but not to switch between these APIs.

	While the engine is running no other method must be called.

	This method is only available if the underlying filesystem
	is PlasmaFS.
     *)

  method to_dev_e : Uq_io.out_device -> Unixqueue.event_system -> 
                      unit Uq_engines.engine
    (** Similar to [to_fd_e], only that this method writes to
	an [Uq_io.out_device].

	This method is only available if the underlying filesystem
	is PlasmaFS.
     *)

  method to_any_e :
         (Netsys_mem.memory -> int -> int -> unit Uq_engines.engine) ->
         Unixqueue.event_system ->
           unit Uq_engines.engine
    (** [to_any_e dest esys]: like [to_fd_e] but the data is not written to
	a file descriptor. Instead, the function [dest] is called like
	[dest m pos len] to output some data.

	This is an experimental method! It might not be defined on every
	record reader.

	This method is only available if the underlying filesystem
	is PlasmaFS.
     *)

  method filesystem : Mapred_fs.filesystem
    (** The filesystem *)

  method record_config : record_config
    (** The [record_config] *)

  method stats : Mapred_stats.stats
    (** returns statistics:
	- [read_blocks]: how many blocks have been read in
	- [read_lines]: how many lines have been processed (unavailable
	  if to_fd_e is used)
	- [read_bytes]: how many bytes have been processed
	- [read_fs_time]: the time spent for waiting on the filesystem layer
	  for new data
     *)

end


class type record_writer =
object
  method output_record : string -> unit
    (** Outputs these records. The records must not contain LF chars.
	The length is limited to the blocksize-1.
     *)

  method output_records : string Queue.t -> unit
    (** Outputs the records in the queue. The queue is empty when this
	method returns.
     *)

  method flush : unit -> unit
    (** Flushes the records from the buffer to disk *)

  method from_fd_e : Unix.file_descr ->  Unixqueue.event_system -> 
                       unit Uq_engines.engine
    (** Outputs the records coming from this file. The file is read until EOF.
	While the engine is running [output_record] and [flush] must
	not be called.

	The file must be line-structured. If the LF after the last line
	is missing it is silently added. The length of the lines is not
	checked.

	One should only either use [from_fd_e] or [output_record]. When
	mixing both styles, it is undefined which data is read by which
	method.

	Note that this method is not implemented for all writers!
	In particular, this method is only available if the underlying
	filesystem is PlasmaFS.
     *)

  method from_dev_e : string Queue.t -> Uq_io.in_bdevice -> 
                      int64 option -> int option ->
                      Unixqueue.event_system -> 
                        bool Uq_engines.engine
    (** [from_dev_e q dev size_limit lines_limit esys]; Generalization of
	[from_fd_e]. In this version, the size and the number of
	records can be limited that are read from the input device.
	If a [size_limit] is set, only up to this number of bytes
	are read (not counting record framing). If a [lines_limit]
	is set, only up to this number of records are read.

	The data is read from the queue [q] first, and then from
	[dev].

	When a limit is hit, it can happen that there are records
	which have not been processed. These records are left behind
	in [q].

	The method returns [true] if EOF is reached.

	Note that this method is not implemented for all writers!
	In particular, this method is only available if the underlying
	filesystem is PlasmaFS.
     *)

  method close_out : unit -> unit
    (** Releases resources (e.g. closes transactions) *)

  method abort : unit -> unit
    (** Drops resources - intended to be used for error cleanup *)

  method filesystem : Mapred_fs.filesystem
    (** The filesystem *)

  method record_config : record_config
    (** The [record_config] *)

  method stats :  Mapred_stats.stats
    (** returns statistics:
	- [write_blocks]: how many blocks have been written out
	- [write_lines]: how many lines have been processed (unavailable
	  if from_fd_e is used)
	- [write_bytes]: how many bytes have been processed
	- [write_fs_time]: the time spent for waiting on the filesystem layer
	  for writing out data
     *)

end


type read_flag = [ `Bof_block of int64 | `Eof_pos of int64 ]
    (** Flags for the reader:
	- [`Bof_block n]: Assume that the first record to read is at position
	  [n] of the file. The position should not be after the first block
	  to read.
	- [`Eof_pos n]: Assume that the EOF position is at [n] (in bytes)

	[`Bof] and [`Eof] can be used to read from a part of the file
	only.

     *)

class type record_reader_factory =
object
  method read_file : read_flag list ->
                     Mapred_fs.filesystem ->
                     Plasma_shm.shm_manager ->
                     record_config ->
                       string -> int64 -> int64 -> record_reader
  (** [read_file flags fs rc name block len]: Reads from [name], starting at
      [block], ending at [block+len-1] (plus the following overflow region).
      Reading is done in a separate transaction.

      Note that len>=1 is a requirement here. 

      If [block=0] (or the [`Process_first_record] flag is given), the first 
      record of the selected file
      region is returned to the user. If [block>0] (or the
      [`Skip_first_record] flag is set),
      the first record is skipped, i.e. reading starts after the first
      encountered record separator.

      Reading continues past the last block if the last record does not
      terminate within the block. The maximum record length is the size
      of a bigblock (including the separator), so [read_file] will at most
      read the bigblock following the last selected block.

      Reading always stops if the real EOF of the file is reached, or
      the position given with the [`Eof] option (whatever comes first).
      
      It is not required that [block] or [len] are multiples of bigblocks,
      although this is usually the case.
   *)
end


class type record_writer_factory =
object
  method  write_file : Mapred_fs.filesystem -> 
                       Plasma_shm.shm_manager ->
                       record_config ->
                       string -> record_writer
  (** [write_file fs rc name]: Appends records to this file (which must already
      exist). Writing is done in separate transactions.
   *)

  method write_suffix : string
    (** The preferred suffix for written files *)

end

class type record_rw_factory =
object
  inherit record_reader_factory
  inherit record_writer_factory
end

(** {1 File formats} *)

val line_structured_format : unit -> record_rw_factory
  (** The line structured file format represents every record as a line
      in a text file. The line is terminated by a LF character. Of course,
      it is not possible to have an LF character inside the record data
      then.

      Otherwise, a line can include every byte (including NUL). We do
      not assume any character set - a line is simply a sequence of bytes.

      For line-structured files, the first record (line) to process in
      a bigblock is the line following the first LF byte. The only
      exception is the first bigblock, where the first line starts at
      the beginning of the file.

   *)

val fixed_size_format : int -> record_rw_factory
  (** A record has exactly n bytes. The record can have any contents,
      and binary representations are fully supported.

      For files with fixed-size records, the first record of block [k]
      of the file starts at byte

      {[ (k * blocksize) mod recordsize ]}

      {b This format cannot be used with streaming mode!}
     
   *)

val var_size_format : unit -> record_rw_factory
  (** A record consists of a variable number of bytes (at most a bigblock).
      Binary representations are fully supported.

      The file consists of chunks (where a whole number of chunks must
      fit into a bigblock). Every chunk of N bytes has a header of 32 bytes,
      followed by a data region until the end of the chunk:

      {[ chunk<k> = header<k> ^ data<k> ]}

      Here, [k] is the index identifying the chunk, [k=0..K-1].

      The length of a chunk is currently always 64K, with no option
      for the user to change it. Note that the last chunk of the file
      may be incompletely stored: The data part may be cut off when
      the real end of the data stream is reached.

      If no compression is done, the concatenated data regions contain
      the sequence of records. A record may start in one region and
      be continued in the next:

      {[ data<0> ^ ... ^ data<K-1> = record<0> ^ ... ^ record<R-1> ]}

      If a compression algorithm is applied, every chunk is compressed
      individually. So:

      {[ U(data<0>) ^ ... ^ U(data<K-1>) = record<0> ^ ... ^ record<R-1> ]}

      where [U] is the uncompression function.

      The header consists of:

      {[ header = chunksize ^ datasize ^ recstart ^ flags ^ checksum ]}

      where:

      - [chunksize] is the number N of bytes every chunk consists of.
        It is represented as 64 bit number in big endian order.
      - [datasize] is the number of bytes for the data area. Normally,
        this should be [chunksize-32], but for compressed chunks it
        is possible that some bytes must remain free. Again a 64 bit
        number in big endian order.
      - [recstart] is the offset to the start of the first record
        in the data area (if compression is done, this refers to the
        data in uncompressed form). The offset is relative to the
        start of the data area. E.g. 0 means that the data area
        starts immediately with a record. If -1, no record begins in this
        chunk.  Again a 64 bit number in big endian order.
      - [flags] are 32 bits that can be used for flags (also big endian).
        Bit 0 says whether GZIP compression is enabled. Other bits are not
        yet assigned.
      - [checksum] are the first four bytes of
        [MD5(chunksize ^ datasize ^ recstart ^ flags ^ dec(k))]
        where [dec(k)] is the decimal representation of [k], the chunk index.
     
      Records consist of a length header followed by the data:

      {[ record = length ^ record_data ]}

      If [record_data] is up to 254 bytes long, the length is just this
      length as single byte. If [record_data] is longer, the [length]
      is represented as a single byte 0xFF followed by a 64 bit number
      in big endian order.

      {b This format cannot be used with streaming mode!}
   *)


val auto_input_format : unit -> record_rw_factory
  (** This "format" recognizes the format for input files by the
      file suffix (".var", "fixed<n>", or nothing).

      Writing files is not supported!
   *)


(** {1 Generic functions} *)

val bigblock_size : Mapred_fs.filesystem -> string -> int -> int
  (** [bigblock_size fs path suggested]: Returns the real bigblock size for
      the [suggested] value. The real size is rounded up to the
      next block multiple.

      The blocksize is retrieved from [fs] (synchronously)
   *)

type sync_readers =
    (unit -> record_reader) list

type async_readers =
    (unit -> record_reader Uq_engines.engine) list

val read_multiple : Mapred_fs.filesystem -> 
                    record_config ->
                    readers:[`Sync of sync_readers | `Async of async_readers] ->
                    unit -> 
                      record_reader
  (** Constructs a record reader that reads from the input [readers] one
      after the other. The readers can be given in a synchronous form,
      or in an asynchronous form. The latter is preferrable when the
      reader is in asynchronous mode (i.e. when [to_fd_e] is
      running).
   *)

val write_multiple : Mapred_fs.filesystem ->
                     Plasma_shm.shm_manager ->
                     record_config ->
                     string -> int64 -> int ->
                     create_sync:(string -> int -> string) ->
                     create_async:(string -> int -> string Uq_engines.engine) option ->
                     record_writer_factory ->
                        record_writer
  (** [write_multiple fs shm rc prefix size_limit lines_limit create_sync create_async rwf]:
      Writes into a sequence of files
      whose names are composed of [prefix] followed by an integer [k]. The
      files are created by calling [create_async prefix k]. A new file is
      started when the current file reaches the size [size_limit] (in bytes),
      or when the current file has [lines_limit] lines.

      Note that the size limit is checked without taking the LF line
      separator into account.

      [create_async]: If passed, this function is used instead of [create_sync]
      in asynchronous mode (i.e. when [from_fd_e] is running).

      Restriction: [from_dev_e] is not implemented.
   *)

val divide : record_config -> int -> record_config
  (** Divides [buffer_size] and [buffer_size_tight] by this number *)
			 

(** {1 File system access} *)

(** More functions for file system access can be found in
    {!Plasma_netfs}.

    On the lower level you can of course also use {!Plasma_client}
    directly, but the interface is slightly more complicated.
 *)


val file_exists : Mapred_fs.filesystem -> string -> bool
  (** [file_exists fs name]: whether this file, directory or symbolic
      link exists
   *)

val is_directory : Mapred_fs.filesystem -> string -> bool
  (** [file_exists fs name]: whether this directory exists
   *)

val create_file : ?repl:int -> ?pref_nodes:string list ->
                    Mapred_fs.filesystem -> string -> unit
  (** [create_file fs name]: Creates this file exclusively. [repl] is
      the replication factor, 0 by default (i.e. use server default).
      [pref_nodes] can be set to the list of preferred datanode
      identities (actually, this only configured the cluster).
   *)

val create_dir : Mapred_fs.filesystem -> string -> unit
  (** [create_dir fs name]: Creates a directory exclusively *)


val delete_file :  Mapred_fs.filesystem -> string -> unit
  (** Delete this file or empty dir *)

val delete_rec : Mapred_fs.filesystem -> string -> unit
  (** Deletes this file or directory recursively *)

val file_blocks : Mapred_fs.filesystem -> string -> int64
  (** Get the length of the file in blocks *)

type fileinfo =
    [ `Regular of int64
    | `Directory
    | `Other
    ]
  (** A file is either [`Regular n] where [n] is the  length, or
      [`Directory], or [`Other]. We follow symlinks in this module.
      [`Other] is also returned for dead symlinks
   *)

val scan_file : Mapred_fs.filesystem -> string -> fileinfo
  (** Returns information about this file *)

val scan_dir : Mapred_fs.filesystem -> string -> bool ->
                 (string * fileinfo) list
  (** [scan_dir fs dir deeply]: Scans the directory [dir] for files,
      and returns the files (w/o directory prefix) as pairs
      [(name,info)].  If [deeply], subdirectories
      are recursively scanned, otherwise they are ignored.

      Files and directories starting with "." and "_" are ignored.

      Symbolic links are followed (and the max scanning depth is restricted).
      The returned data refers to the files pointed to by the symlinks.
      Dead symlinks are skipped.
   *)

(**/**)

(** For unit testing only! *)

class vs_write_file : 
        ?chunklen:int -> Mapred_fs.filesystem -> Plasma_shm.shm_manager ->
        record_config ->
        string -> record_writer

val more_logging : bool ref
