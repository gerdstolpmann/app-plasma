(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* TODO:
   - var_size_format: buf needs to be a multiple of chunklen?
 *)


open Plasma_rpcapi_aux
open Uq_engines.Operators
open Printf

let dlogf = Plasma_util.dlogf
let dlogr = Plasma_util.dlogr

let more_logging = ref false

class type record_config =
object
  method bigblock_size : int
  method mr_buffer_size : int
  method mr_buffer_size_tight : int
end


class type record_reader =
object
  method pos_in : int
  method input_record : unit -> string
  method peek_record : unit -> string
  method input_records : string Queue.t -> unit
  method close_in : unit -> unit
  method abort : unit -> unit
  method to_fd_e : Unix.file_descr -> Unixqueue.event_system -> 
           unit Uq_engines.engine
  method to_dev_e : Uq_io.out_device -> Unixqueue.event_system -> 
           unit Uq_engines.engine
  method to_any_e :
         (Netsys_mem.memory -> int -> int -> unit Uq_engines.engine) ->
         Unixqueue.event_system -> 
         unit Uq_engines.engine
  method filesystem : Mapred_fs.filesystem
  method record_config : record_config
  method stats :  Mapred_stats.stats
end


class type record_writer =
object
  method output_record : string -> unit
  method output_records : string Queue.t -> unit
  method flush : unit -> unit
  method from_fd_e : Unix.file_descr -> Unixqueue.event_system -> 
             unit Uq_engines.engine
  method from_dev_e : string Queue.t -> Uq_io.in_bdevice -> 
                      int64 option -> int option ->
                      Unixqueue.event_system -> 
                        bool Uq_engines.engine
  method close_out : unit -> unit
  method abort : unit -> unit
  method filesystem : Mapred_fs.filesystem
  method record_config : record_config
  method stats : Mapred_stats.stats
end

type read_flag = [ `Bof_block of int64 | `Eof_pos of int64 ]

class type record_reader_factory =
object
  method read_file : read_flag list ->
                     Mapred_fs.filesystem -> 
                     Plasma_shm.shm_manager ->
                     record_config ->
                       string -> int64 -> int64 -> record_reader
end

class type record_writer_factory =
object
  method  write_file : Mapred_fs.filesystem ->
                       Plasma_shm.shm_manager -> record_config ->
                       string -> record_writer
  method write_suffix : string
end

class type record_rw_factory =
object
  inherit record_reader_factory
  inherit record_writer_factory
end



let zero_mem =
  Bigarray.Array1.create Bigarray.char Bigarray.c_layout 0

(** rounds the value m up to the next multiple of n *)
let round_up m n =
  ((m-1) / n + 1) * n

let bigblock_size fs path suggested =
  let bsize = fs#blocksize path in
  round_up suggested bsize


let output_string_e dest s =
  let l = String.length s in
  let m = Bigarray.Array1.create Bigarray.char Bigarray.c_layout l in
  Netsys_mem.blit_string_to_memory s 0 m 0 l;
  dest m 0 l


let rec consume f q =
  if Queue.is_empty q then
    ()
  else
    let x = Queue.take q in
    f x;
    consume f q


let get_cluster fs name esys =
  match fs # open_cluster name esys with
    | None ->
	failwith "Mapred_io: engine ops are only supported for PlasmaFS"
    | Some c -> c


let lookup_inode_e c name =
  Plasma_client.with_trans_e c
    (fun trans ->
       Plasma_client.lookup_e trans name false
    )


(**********************************************************************)
(* Line structured format                                             *)
(**********************************************************************)


let ls_read_file ?(force_bof=0L) ?(force_eof=Int64.max_int)
              fs shm_mng rc name index len : record_reader =
  let () =
    if index < 0L || len < 0L then invalid_arg "Mapred_io.read_file" in
  let () =
    dlogf "Mapred_io.ls_read_file name=%s index=%Ld len=%Ld force_bof=%Ld \
           force_eof=%Ld"
      name index len force_bof force_eof in

  let bof = force_bof=index || index=0L in
    (* whether to apply "beginning of file" logic *)

  (* This block of variables is initialized in buf_init_e: *)
  let bsize = ref 0 in
  let bigblocksize = ref 0 in
  let file_eof = ref force_eof in  (* the end of the file *)
  let end_index = ref 0L in     (* the end of the requested region as index *)
  let hard_end_index = ref 0L in(* the end of the overflow region as index *)
  let semi_end_index = ref 0L in(* the preliminary end of the overflow reg *)
  let buf_num = ref 0 in        (* length of bufs in number of blocks *)
  let buf_dim = ref 0 in        (* length of bufs in bytes *)
  let bufs = ref zero_mem in    (* The buffer as bigarray *)
  let shm_undo = ref (fun () -> ()) in    (* deregisters the buffer in shm *)

  (* These are initialized by the first fill_buffers_e call: *)
  let buf_end = ref 0 in        (* end position in bufs in bytes *)
  let buf_count = ref 0 in      (* how many blocks we have *)
  let buf_eof = ref None in     (* the (logical) eof position if reached *)
  let buf_index = ref index in  (* the block the first buffer holds *)
  let buf_cursor = ref 0 in     (* as byte position in bufs *)
  let buf_eof_reached = ref false in
  let buf_bgrd = ref None in  (* initialized in fill_buffers *)

  (** So what are we going to do? The user requests:
   - to read the blocks [index] to [index+len-1] (the "requested region")
   - and if the last line does not end in this block region, the remaining
     part of the last line is taken from the following blocks (the
     "overflow region")

   Also, it is assumed that the normal unit of processing are bigblocks, i.e.
   a whole multiple of blocks. So [index] normally points to the beginning of
   a bigblock, and [len] must be a multiple of bigblocks (unless we are
   at the end of the file, and the last bigblock exists only partially).
   Note that there is the corner case that the last byte of the requested region
   is an LF char. Then the last line to read is the following line, i.e.
   this line is solely taken from the overflow region.

   We allocate buffers, and each of the buffers is one block. The number of
   buffers [buf_limit] is limited by [mem_limit] - but at least there should be
   as many buffers so a bigblock fits in. This is no longer enforced, though,
   and it is possible that the buffer is in total smaller than a bigblock
   (with a performance loss, but maybe saving memory is more important).

   Reading is now done by: filling the buffers, taking line by line from
   the filled buffers, and if the end of the buffers is reached, the buffers
   are refilled. This is done until the complete requested region is read, and
   the part of the overflow region that is needed for getting
   the last line.

   At refill time the current line is usually split in a first part that is
   taken from the buffers before refill, and a trailing part that is taken
   from the buffers after refill. The maximum line length is [bigblock_size],
   and we have at least as many buffers to hold a whole line. This means
   it is ensured that at most one refill is needed to get the remaining part
   of a line.

   The [end_index] is the file index (i.e. file position in units of
   blocks) that marks the end of the requested region. [end_index] is
   also the beginnning of the overflow region. (Note that this is only
   in units of blocks - when computing the byte position of the end
   of the requested region, one also has to take into account that the
   last block might not be fully used.)

   The [hard_end_index] is the end of the overflow region, again in 
   blocks.

   [semi_end_index] is the preliminary end of the overflow region. It is
   just one block more then [end_index]. In the first round it is only
   read until there. Only if [semi_end_index] is reached and the LF of the
   last line cannot be found, the remaining blocks until [hard_end_index]
   are read.

   Every refill sets:
    - [buf_index]: the index of the first buffered block
    - [buf_count]: the number of buffered blocks
    - [buf_end]: the number of buffered bytes (important if the last
      buffered block is the last block of the file which might be incomplete)
    - [buf_eof]: this is [None] if the end of the requested region is not
      yet reached. It is [Some p] when the end is reached, and the byte [p]
      of the current buffers (counting from 0 to [buf_end]) is the first
      byte of the overflow region. A negative [p] is possible and means that
      the first
      byte of the overflow region was already reached before the last
      refill. [Some end_buf] means that either the end of the file is
      reached or when [end_buf=buf_dim] that the overflow region starts
      at the first byte after the current buffers.

   Note that the overflow region can be empty.

   While extracting lines from the buffers, the value [buf_cursor] is
   the current byte position in the buffer (ranging from 0 to [buf_end]),
   and [buf_eof_reached] is set when the last line is returned ot the
   user.

 *)

  let lazy_init = ref false in        (* whether lazy_init_e is done *)
  let api_type = ref `Indefinite in
  let pos_in = ref 0 in               (* number of lines read so far *)
  let cur_rec = Queue.create() in     (* currently peeked lines *)
  let cur_rec_len = ref 0 in          (* length of cur_rec in bytes *)
  let closed = ref false in           (* whether closed *)
  let to_fd_engine = ref None in      (* whether a to_fd_e is active *)
  let drop_next_line = ref (not bof) in
    (* We drop the very first line unless [bof] is set *)

  (** [cur_rec] may also buffer lines up. This is mostly intended to speed
      up reading when the lines are very short.
   *)

  let find_lf pos end_pos =
    assert(pos >= 0 && end_pos >= 0 &&
	     pos <= !buf_dim && end_pos <= !buf_dim);
    Platform.find_lf !bufs pos end_pos in

  (* stats *)
  let st_blocks = ref 0L in
  let st_lines = ref 0L in
  let st_bytes = ref 0L in
  let st_fs_time = ref 0.0 in


object(self)

  method private lazy_init sync_mode =
    self # buffer_init();
    if sync_mode then
      self # fill_buffers index true;
    dlogf
      "Mapred_io.read_file(%s): init done" name;
    lazy_init := true;

  method private buffer_init() =
    let bs = fs # blocksize name in
    bsize := bs;
    let bsizeL = Int64.of_int bs in
    bigblocksize := round_up rc#bigblock_size bs;
    let bigblockmult = !bigblocksize / bs in
    let bigblockmultL = Int64.of_int bigblockmult in
    (* bigblockmult: how many blocks fit into a bigblock *)

    let mr_buffer_size = round_up rc#mr_buffer_size bs in
    let mr_buffer_size_tight = round_up rc#mr_buffer_size_tight bs in
    let shm_cond = shm_mng # condition `Buf in
    let shm_id = shm_mng # new_id `Buf in
    let mem_limit = 
      ( match shm_cond with
	  | `Normal -> mr_buffer_size
	  | `Tight -> mr_buffer_size_tight
      ) in
    shm_undo := (fun () -> shm_mng # book_allocation shm_id 0);
    let size = fs # size [] name in
    file_eof := min size force_eof;
    let max_index = 
      if !file_eof = 0L then
	(-1L) 
      else
	Int64.div (Int64.pred !file_eof) bsizeL in
    let eof_index =
      Int64.succ max_index in
    (** [max_index] is the index of the last block of the file.
	[eof_index] is the following block.
     *)
    dlogf
      "Mapred_io.read_file(%s,index=%Ld,len=%Ld,size=%Ld,eofidx=%Ld,bsize=%d)" 
      name index len size eof_index bs;
    if index > Int64.sub eof_index len then
      failwith "Mapred_io.read_file: cannot read beyond eof";

    end_index := Int64.add index len;
    semi_end_index := min (Int64.succ !end_index) eof_index;
    hard_end_index := 
      min (Int64.add !end_index bigblockmultL) eof_index;
    dlogf
      "Mapred_io.read_file(%s,index=%Ld,len=%Ld,endidx=%Ld,hendidx=%Ld)" 
      name index len !end_index !hard_end_index;
    let buf_limit0 = max 1 (mem_limit / bs) in
    let buf_limit =
      if buf_limit0 mod 2 = 1 then buf_limit0+1 else buf_limit0 in
    (* buf_limit is divisible by 2 *)
    let buf_numL = (* min hard_len *) (Int64.of_int buf_limit) in
    buf_num := Int64.to_int buf_numL;
    buf_dim := !buf_num * bs;
    shm_mng # book_allocation shm_id !buf_dim;
    dlogf
      "Mapred_io.read_file(buf_num=%d,buf_dim=%d)"
      !buf_num !buf_dim;
    (* bufs := Netsys_mem.alloc_memory_pages !buf_dim; *)
    if len = 0L then (
      buf_eof_reached := true;
      semi_end_index := !end_index;
      hard_end_index := !end_index;
    );

  method private end_of_file() =
    dlogf
      "Mapred_io.read_file(%s,EOF,pos_in=%d)" name !pos_in;
    raise End_of_file

  method pos_in = !pos_in

  method peek_record() =
    if Queue.is_empty cur_rec then self # refill();
    try
      Queue.peek cur_rec
    with
      | Queue.Empty -> raise End_of_file

  method input_record() =
    if Queue.is_empty cur_rec then self # refill();
    try
      let line = Queue.take cur_rec in
      cur_rec_len := !cur_rec_len - String.length line - 1;
      incr pos_in;
      line
    with
      | Queue.Empty -> raise End_of_file

  method input_records q =
    if Queue.is_empty cur_rec then self # refill();
    if Queue.is_empty cur_rec then raise End_of_file;
    pos_in := !pos_in + Queue.length cur_rec;
    cur_rec_len := 0;
    Queue.transfer cur_rec q
    
  method private refill() =
    assert(Queue.is_empty cur_rec && !cur_rec_len = 0);
    if !closed then failwith "Mapred_io.read_file: closed";
    if !api_type = `Async then 
      failwith "Mapred_io.read_file: this method is unsupported in async mode";
    if !api_type = `Indefinite then
      api_type := `Sync;
    if not !lazy_init then self#lazy_init true;
    assert(!lazy_init && !buf_cursor <= !buf_end);
    try
      let input_record_start = self # input_record_start in (* method lookup *)
      (* Just queue up a few lines *)
      while !cur_rec_len < 16384 do
	let line = input_record_start () in
	let len = String.length line in
	if len>0 && String.unsafe_get line 0 = '\000' then
	  Netlog.logf `Warning
	    "Mapred_io.read_file: Line starts with NUL byte pos_in=%d" 
	    !pos_in;
	if not !drop_next_line then (
	  Queue.add line cur_rec;
	  cur_rec_len := !cur_rec_len + len + 1
	)
	else
	  drop_next_line := false;
      done;
      st_lines := Int64.add !st_lines (Int64.of_int (Queue.length cur_rec))
    with
      | End_of_file -> 
	  ()

  method private input_record_start() =
    (** Same as [input_record_rest ""] but slightly faster *)
    if !buf_eof_reached then self # end_of_file();
    try
      let p = find_lf !buf_cursor !buf_end in
      let l = p - !buf_cursor in
      if l >= !bigblocksize then
	failwith "Mapred_io.read_file: record too large";
      let line = String.create l in
      Netsys_mem.blit_memory_to_string !bufs !buf_cursor line 0 l;
      buf_cursor := p+1;
      ( match !buf_eof with
	  | None -> ()
	  | Some eof ->
	      if p >= eof then
		buf_eof_reached := true
      );
      line
    with
      | Not_found -> self # input_record_rest ""

  (** Read the remaining part of the current line. The first part of the
      line is [line0]. The first part can be passed as [""] which means
      that nothing is read yet. The method raises [End_of_file] if 
      [line0=""] and there is no more line.
   *)
  method private input_record_rest line0 =
    if !buf_eof_reached then self # end_of_file();
    assert(!lazy_init && !buf_cursor <= !buf_end);
    (** Note that [buf_cursor=buf_end] is possible when the previous line
	ended at the last byte of the buffer. In this case we always run
	into the [Not_found] handler below, and this line can only be
	read after a refill. (Another cause is a totally empty file.)
     *)
    let l0 = String.length line0 in
    try
      let p = find_lf !buf_cursor !buf_end in
      let l = l0 + p - !buf_cursor in
      if l >= !bigblocksize then
	failwith "Mapred_io.read_file: record too large";
      let line = String.create l in
      if l0 > 0 then
	String.blit line0 0 line 0 l0;
      Netsys_mem.blit_memory_to_string !bufs !buf_cursor line l0 (l-l0);
      buf_cursor := p+1;
      (** If we know already where the requested region ends, we can check
	  whether this is the last line. [p] is the position of the LF
	  char of the current line. If the LF byte is in the overflow
	  region, this is the last line. (Note that [p=eof-1] just means
	  that the last line is the following line which is completely
	  taken from the overflow region.)
       *)
      ( match !buf_eof with
	  | None -> ()
	  | Some eof ->
	      if p >= eof then
		buf_eof_reached := true
      );
      line
    with
      | Not_found ->
	  (** The LF char cannot be found. If this is already the
	      inner call [input_record_rest], i.e. [line0 <> ""],
	      there is no chance to extract a line of at most the
	      size of bigblocks. So fail. If [line0 = ""], we extract
	      here the new version of [line0] for the inner call.
	   *)
	  if l0 + !buf_end - !buf_cursor >= !bigblocksize then
	    failwith "Mapred_io.read_file: record too large";
	  let l1 = l0 + !buf_end - !buf_cursor in
	  let line1 = String.create l1 in
	  String.blit line0 0 line1 0 l0;
	  Netsys_mem.blit_memory_to_string !bufs !buf_cursor line1 l0 (l1-l0);
	  (** We refill here. We do this even if [buf_eof] is already set.
	      When [buf_eof] is set, we probably already have read a part
	      of the overflow region. Nevertheless, we need to ensure that
	      we read at least a whole bigblock from the overflow region,
	      and the remaining bigblock might become available only after
	      a refill. Note that the refill might lead to empty buffers
	      when we are at the EOF position of the file.
	   *)
	  self # fill_buffers 
	    (Int64.add !buf_index (Int64.of_int !buf_count))
	    false;
	  if !buf_count = 0 then (
	    (** No more data exist. This means the current line is the
		last line, and it does not end with an LF char.
	     *)
	    buf_eof_reached := true;
	    (** [l1=0] can happen when the length of the file is
		a whole multiple of bigblocks. The previous line ended
		at the end of the previous bigblock, and there are no
		more lines to read.
	     *)
	    if l1 = 0 then self # end_of_file();
	    line1
	  ) else
	    (** Read the remaining part of the line *)
	    self # input_record_rest line1
		    
  method to_fd_e fd esys =
    if !api_type = `Sync then 
      failwith "Mapred_io.read_file: this method is unsupported in sync mode";
    if !api_type = `Indefinite then
      api_type := `Async;

    self # to_dev_e (`Polldescr(Netsys.get_fd_style fd,fd,esys)) esys

  method to_dev_e io_dev esys =
    if !api_type = `Sync then 
      failwith "Mapred_io.read_file: this method is unsupported in sync mode";
    if !api_type = `Indefinite then
      api_type := `Async;

    self # to_any_e
      (fun m pos len ->
	 Uq_io.really_output_e
	   io_dev
	   (`Memory m)
	   pos
	   len
      )
      esys

  method to_any_e
         (dest : Netsys_mem.memory -> int -> int -> unit Uq_engines.engine)
         esys =
    (** We write everything until the end of the requested region, 
	plus the beginning of the overflow region until the first
	LF (or EOF).

	The contents of the [cur_rec] buffer is ignored for simplicity.
     *)
    if !closed then failwith "Mapred_io.read_file: closed";

    if !api_type = `Sync then 
      failwith "Mapred_io.read_file: this method is unsupported in sync mode";
    if !api_type = `Indefinite then
      api_type := `Async;

    if !to_fd_engine <> None then 
      failwith "Mapred_io.read_file: The fd engine is still running";

    let c = get_cluster fs name esys in

    let rec out_loop inode () =
      dlogr
	(fun () ->
	   sprintf "out_loop: i=%Ld buf_cursor=%d buf_end=%d"
	     !buf_index !buf_cursor !buf_end
	);
      assert (!lazy_init);
      match !buf_eof with
	| None ->
	    (* Write whole buffer *)
	    out_until inode !buf_end ++ out_loop inode
	| Some eof ->
	    dlogr
	      (fun () -> sprintf "eof=%d" eof);
	    (* If the cursor is before eof, just write until eof *)
	    if !buf_cursor < eof then
	      out_until inode eof ++ out_loop inode
	    else (
	      (* Search for the LF byte in the overflow region:
		 - p_end_file_eof: the position of the file EOF as buffer pos
		 - p_end_max: the end position of the overflow region
		 - p_end: the end of the LF search
	       *)
	      let bsizeL = Int64.of_int !bsize in
	      let p_end_file_eofL = 
		Int64.sub !file_eof (Int64.mul !buf_index bsizeL) in
	      let p_end_max = eof + !bigblocksize in
	      let p_end = min !buf_end p_end_max in
	      (* First check whether this is the very last line of the
		 file, and the LF char is just missing
	       *)
	      if Int64.of_int !buf_cursor = p_end_file_eofL then
		(* FIXME: we never add the LF char - just because we do
		   not know whether the line has at least one char
		 *)
		out_lf false () ++ out_eof
	      else (
		if !buf_cursor >= p_end_max then
		  record_too_large()
		else (
		  try
		    if !buf_cursor >= p_end then raise Not_found;
		    let p = find_lf !buf_cursor p_end in
		    (* So this is the last line of the overflow region *)
		    out_until inode (p+1) ++ out_eof
		  with
		    | Not_found ->
			(* No LF so far. However, the overflow region might still
			   be incompletely read in. So output what we have
			   and continue the search for the LF until the end
			   of the overflow region ([out_until p_end]
			   triggers that more data is read in)
			 *)
			out_until inode p_end ++ out_loop inode
		)
	      )
	    )

    and out_until inode p =
      (* Output until byte pos p *)
      dlogf
	"Mapred_io.read_file(%s): out_until start=%d end=%d" name !buf_cursor p;
(*
let q = p - !buf_cursor in
let s = String.create q in
Netsys_mem.blit_memory_to_string !bufs !buf_cursor s 0 q;
prerr_endline("out: " ^ s);
 *)
      dest !bufs !buf_cursor (p - !buf_cursor)
      ++ (fun () ->
	    dlogf
	      "Mapred_io.read_file(%s): out_until done" name;
	    buf_cursor := p;
	    out_refill inode ()
	 )

    and skip_until inode p =
      (* Skip until byte pos p *)
      dlogf
	"Mapred_io.read_file(%s): skip_until start=%d end=%d" name !buf_cursor p;
      buf_cursor := p;
      out_refill inode ()

    and out_refill inode () =
      (* read the next buffer if we are at the end of the current one,
	 and if the current one has still max size
       *)
      let p = !buf_cursor in
      if p = !buf_end && p = !buf_count * !bsize then
	self # fill_buffers_e 
	  (Int64.add !buf_index (Int64.of_int !buf_count)) false c inode esys
      else
	eps_e (`Done()) esys
	
    and out_lf cond () =
      (* Output a single LF char if conf *)
      dlogf
	"Mapred_io.read_file(%s): out_lf cond=%B" name cond;
      if cond then
	output_string_e dest "\n"
      else
	eps_e (`Done ()) esys

    and out_eof() =
      dlogf
	"Mapred_io.read_file(%s): out_eof" name;
      buf_eof_reached := true;
      eps_e (`Done ()) esys

    and out_skip_first_line inode k () =
      (* Unless [bof] is set we have to skip the first line *)
      dlogf
	"Mapred_io.read_file(%s): skip first line" name;
      let m =
	match !buf_eof with
	  | None -> !buf_end
	  | Some eof -> min !buf_end eof in
      (* Search for LF *)
      try
	let p = find_lf !buf_cursor m in
	if p + k >= !bigblocksize then
	  record_too_large()
	else (  
	  drop_next_line := false;
	  skip_until inode (p+1)
	)
      with Not_found ->
	if !buf_eof = None then (
	  let k' = k + !buf_end - !buf_cursor in
	  skip_until inode !buf_end ++ out_skip_first_line inode k'
	)
	else (
	  (* Test if we already hit file_eof. If so, we are at EOF.
	     Otherwise, the first line is too long
	   *)
	  let bsizeL = Int64.of_int !bsize in
	  let p_end_file_eofL = 
	    Int64.sub !file_eof (Int64.mul !buf_index bsizeL) in
	  if Int64.of_int m = p_end_file_eofL then
	    out_eof()
	  else
	    record_too_large()
	)

    and out_maybe_skip_first_line inode () =
      if !drop_next_line then
	out_skip_first_line inode 0 ()
      else
	eps_e (`Done ()) esys

    and out_start() =
      dlogr
	(fun () ->
	   sprintf "to_fd_e: end_index=%Ld hard_end_index=%Ld file_eof=%Ld"
	     !end_index !hard_end_index !file_eof
	);
      lookup_inode_e c name
      ++ (fun inode ->
	    if len = 0L then
	      eps_e (`Done ()) esys
	    else if not (Queue.is_empty cur_rec) then (
	      let l = Queue.fold (fun acc line -> line::acc) [] cur_rec in
	      let s = String.concat "\n" (List.rev ("" :: l)) in
	      Queue.clear cur_rec;
	      output_string_e dest s 
	      ++ out_maybe_skip_first_line inode
	      ++ out_loop inode
	    )
	    else out_maybe_skip_first_line inode() ++ out_loop inode
	 )

    and record_too_large () =
      eps_e (`Error (Failure "Mapred_io.read_file: record too large")) esys

    in

    if not !lazy_init then
      self # lazy_init false;
      (* always synchronously, including the first buffer fill *)

    let e = out_start() in
    to_fd_engine := Some e;
    e
    >> (fun st ->
	  Plasma_client.close_cluster c;
	  to_fd_engine := None;
	  st
       )

  method private fill_buffers i fakeinit =
    assert (fakeinit || !lazy_init);

    let bgrd =
      match !buf_bgrd with
        | Some bgrd -> bgrd
	| None ->
	    let bgrd =
	      Mapred_io_bgreader.create
		fs name i !semi_end_index (!buf_num/2) in
	    buf_bgrd := Some bgrd;
	    bgrd in

    let next_block =
      Int64.add
	(Mapred_io_bgreader.buffer_block bgrd) 
	(Int64.of_int (Mapred_io_bgreader.buffer_len bgrd)) in
    assert (next_block = i);

    if i >= !semi_end_index then
      Mapred_io_bgreader.extend bgrd !hard_end_index;

    let t0 = Unix.gettimeofday() in
    let len_blocks = Mapred_io_bgreader.read bgrd in
    let t1 = Unix.gettimeofday() in

    bufs := Mapred_io_bgreader.buffer bgrd;
    assert(Mapred_io_bgreader.buffer_block bgrd = i);

    self # book_buffers i len_blocks (t1 -. t0);

    
(*
  method private fill_buffers i =
    Plasma_client.sync (self#fill_buffers_e i) false 
    *)


  method private fill_buffers_e i fakeinit c inode esys =
    (** Fill buffers so that the first one has index i *)
    
    assert(fakeinit || !lazy_init);

    if Bigarray.Array1.dim !bufs = 0 then
      bufs := Netsys_mem.alloc_memory_pages !buf_dim;

    (** [len_blocks] is the number of blocks we read here, either until
	the buffers are filled, or the end of the overflow region is
	reached.

	Sophisticated: if i < semi_end_index, we read only until semi_end_index.
     *)

    let buf_numL = Int64.of_int !buf_num in
    let bsizeL = Int64.of_int !bsize in

    let len_blocks = 
      if i < !semi_end_index then
	Int64.to_int (min (Int64.sub !semi_end_index i) buf_numL)
      else
	Int64.to_int (min (Int64.sub !hard_end_index i) buf_numL) in
    let len = len_blocks * !bsize in  (* rounded up to blocks *)
    let pos = Int64.mul i bsizeL in

    dlogf
      "Mapred_io.read_file: copy_out inode=%Ld pos=%Ld len=%d"
      inode pos len;

    let t0 = Unix.gettimeofday() in
    Plasma_client.retry_e
      c name
      (fun () ->
	 Plasma_client.copy_out_to_buf_e c inode pos !bufs len
      )
      ()
    ++ (fun _ ->
	  let t1 = Unix.gettimeofday() in
	  self # book_buffers i len_blocks (t1 -. t0);
	  eps_e (`Done ()) esys
       )


  method private book_buffers i len_blocks t_read =
    st_blocks := Int64.add !st_blocks (Int64.of_int len_blocks);
    st_fs_time := !st_fs_time +. t_read;

    let bsizeL = Int64.of_int !bsize in
    let len = len_blocks * !bsize in  (* rounded up to blocks *)
    let lenL = Int64.of_int len in
    let pos = Int64.mul i bsizeL in
    let nL = max (min lenL (Int64.sub !file_eof pos)) 0L in
    let n = Int64.to_int nL in
    (** This are the number of bytes we've really read into the buffer.
	Note that [pos > !file_eof] is possible (resulting in n=0)
     *)
    st_bytes := Int64.add !st_bytes nL;

    (* end of requested region as file position *)
    let eorr = min (Int64.mul !end_index bsizeL) !file_eof in

    (* end of requested region relative to buffer start. This value
       can be zero or negative!
     *)
    let rel_eorr = Int64.sub eorr pos in

    (* If [rel_eorr > n] the end of the requested region is not yet
       reached. If [rel_eorr = n] the end of the requested region is
       at the end of the buffer. If [rel_eorr < n] the end is before 
       the buffer or in the buffer. If [rel_eorr < 0] the eorr is
       even _before_ the start of the buffer.
     *)

    buf_index := i;
    buf_end := n;
    buf_count := if len=0 then 0 else (len - 1) / !bsize + 1;
    buf_eof := (if rel_eorr > nL then None 
		else Some (Int64.to_int rel_eorr)); 
    buf_cursor := 0;

    dlogf
      "Mapred_io.read_file: new buffer: index=%Ld end=%d count=%d eof=%s"
      !buf_index !buf_end !buf_count
      (match !buf_eof with None -> "none" | Some n -> string_of_int n);


  method close_in() =
    if not !closed then (
      ( match !to_fd_engine with
	  | None -> ()
	  | Some e -> e # abort(); to_fd_engine := None
      );
      ( match !buf_bgrd with
          | None ->
	      if Bigarray.Array1.dim !bufs <> 0 then
		Netsys_mem.memory_unmap_file !bufs;
	  | Some bgrd ->
	      Mapred_io_bgreader.close bgrd;
	      Netsys_mem.memory_unmap_file (Mapred_io_bgreader.buffer bgrd);
      );
      !shm_undo();
      closed := true
    )

  method abort() =
    self # close_in()

  method filesystem =
    fs

  method record_config =
    rc

  method stats = 
    Mapred_stats.of_list
      [ "read_blocks", Int64.to_float !st_blocks;
	"read_lines", Int64.to_float !st_lines;
	"read_bytes", Int64.to_float !st_bytes;
	"read_fs_time", !st_fs_time
      ]
 
end


class ls_write_file fs shm_mng (rc:record_config) name =
  let () =
    dlogf "Mapred_io.ls_write_file name=%s" name in

  (* This block of variables is initialized in buf_init_e: *)
  let bsize = ref 0 in
  let bigblocksize = ref 0 in
  let buf_len = ref 0 in        (* length of bufs in bytes *)
  let bufs = ref zero_mem in    (* The buffer as bigarray *)
  let shm_undo = ref (fun () -> ()) in    (* deregisters the buffer in shm *)

  (* The following variables are created with their right initial value: *)
  let buf_end = ref 0 in        (* end position in bufs in bytes *)
  let buf_pos = ref 0L in       (* byte position in file of first buf byte *)

  let lazy_init = ref false in        (* whether lazy_init_e is done *)
  let closed = ref false in
  let from_fd_engine = ref None in

  let copy_in_from_buf = lazy(fs # copy_in_from_buf name) in

  let pos_out = ref 0 in

(*
  let rev_find_lf pos end_pos =
    assert(!lazy_init && pos >= 0 && end_pos >= 0 &&
	     pos <= !buf_len && end_pos <= !buf_len);
    Platform.rev_find_lf !bufs pos end_pos
  in 
 *)

  let qout = Queue.create() in

  (* stats *)
  let st_blocks = ref 0L in
  let st_lines = ref 0L in
  let st_bytes = ref 0L in
  let st_fs_time = ref 0.0 in

object(self)

  method private lazy_init() =
    self # buffer_init()

  method private buffer_init() =
    let bs = fs # blocksize name in
    bsize := bs;
    bigblocksize := round_up rc#bigblock_size bs;
    let mr_buffer_size = round_up rc#mr_buffer_size bs in
    let mr_buffer_size_tight = round_up rc#mr_buffer_size_tight bs in
    let shm_cond = shm_mng # condition `Buf in
    let shm_id = shm_mng # new_id `Buf in
    let mem_limit = 
      ( match shm_cond with
	  | `Normal -> mr_buffer_size
	  | `Tight -> mr_buffer_size_tight
      ) in
    shm_undo := (fun () -> shm_mng # book_allocation shm_id 0);
    
    let ch = fs # write [`Truncate] name in
    ch # close_out();

    dlogf "Mapred_io.write_file(%s)" name;
    let buf_num = max 1 (mem_limit / bs) in
    (* let buf_numL = Int64.of_int buf_num in *)
    buf_len := buf_num * bs;
    shm_mng # book_allocation shm_id !buf_len;
    bufs := Netsys_mem.alloc_memory_pages !buf_len;
    lazy_init := true;


  method output_record line =
    Queue.add line qout;
    self # output_records qout

  method output_records q =
    (* we do not protect against having LF in line. User's responsibility *)
    if !closed then failwith "Mapred_io.write_file: closed";
    if !from_fd_engine <> None then
      failwith "Mapred_io.write_file: The fd engine is still running";
    if not !lazy_init then self # lazy_init();
    assert (!lazy_init);
    let q' = Queue.create() in
    Queue.transfer q q';
    Queue.iter
      (fun line ->
	 if line <> "" && line.[0] = '\000' then
	   Netlog.logf `Warning
	     "Mapred_io.write_file: Line starts with NUL byte pos_out=%d" 
	     !pos_out;
	 let l = String.length line in
	 if l >= !bigblocksize then
	   failwith "Mapred_io.write_file: record too large";
	 let p = ref 0 in
	 while !p < l+1 do
	   let space = !buf_len - !buf_end in
	   let n =
	     if !p < l then (
	       let k = min (l - !p) space in
	       Netsys_mem.blit_string_to_memory line !p !bufs !buf_end k;
	       k
	     ) else (
	       !bufs.{ !buf_end } <- '\n';
	       1
	     ) in
	   buf_end := !buf_end + n;
	   p := !p + n;
	   if !buf_end = !buf_len then
	     self # flush()
	 done;
	 st_lines := Int64.succ !st_lines;
	 incr pos_out;
      )
      q'

  method output_records_fs reclen q =
    (* The version for fixed_size_format *)
    if !closed then failwith "Mapred_io.write_file: closed";
    if !from_fd_engine <> None then
      failwith "Mapred_io.write_file: The fd engine is still running";
    if not !lazy_init then self # lazy_init();
    assert (!lazy_init);
    let q' = Queue.create() in
    Queue.transfer q q';
    Queue.iter
      (fun line ->
	 let l = String.length line in
	 if l <> reclen then
	   failwith "Mapred_io.write_file: record has not the fixed size";
	 let p = ref 0 in
	 while !p < l do
	   let space = !buf_len - !buf_end in
	   let n = min (l - !p) space in
	   Netsys_mem.blit_string_to_memory line !p !bufs !buf_end n;
	   buf_end := !buf_end + n;
	   p := !p + n;
	   if !buf_end = !buf_len then
	     self # flush()
	 done;
	 st_lines := Int64.succ !st_lines;
	 incr pos_out;
      )
      q'

  method flush() =
    if !closed then failwith "Mapred_io.write_file: closed";
    if !from_fd_engine <> None then
      failwith "Mapred_io.write_file: The fd engine is still running";
    if not !lazy_init then self # lazy_init();
    dlogf "Mapred_io.write_file.flush: name=%s pos=%Ld len=%d"
      name !buf_pos !buf_end;

    assert(!lazy_init);

    let t0 = Unix.gettimeofday() in
    (Lazy.force copy_in_from_buf) !buf_pos !bufs !buf_end;
    let t1 = Unix.gettimeofday() in

    self # book_buffers (t1 -. t0)


  method private flush_e c inode =
    assert(!lazy_init);
    let t0 = Unix.gettimeofday() in
    let esys = Plasma_client.event_system c in
    Plasma_client.retry_e
      c name
      (fun () ->
	 Plasma_client.copy_in_from_buf_e
	   ~flags:[`Late_datasync]
	   c inode !buf_pos !bufs !buf_end `Star
      )
      ()
    ++ (fun _ ->
	  let t1 = Unix.gettimeofday() in
	  self # book_buffers (t1 -. t0);
	  eps_e (`Done()) esys
       )

  method private book_buffers t_delta =
    st_fs_time := !st_fs_time +. t_delta;
    let b = if !buf_end = 0 then 0 else (!buf_end - 1) / !bsize + 1 in
    st_blocks := Int64.add !st_blocks (Int64.of_int b);

    (* copy_in works only with multiples of bsize. So if we did not match
       this, retain the end of the buffer. (The requirement here is
       that !buf_pos must remain a multiple of bsize.)
     *)
    let tail = !buf_end mod !bsize in
    if tail > 0 then (
      let n = !buf_end - tail in
      Plasma_platform.blit_memory !bufs (!buf_end - tail) !bufs 0 tail;
      buf_pos := Int64.add !buf_pos (Int64.of_int n);
      buf_end := tail;
      st_bytes := Int64.add !st_bytes (Int64.of_int n);
    )
    else (
      buf_pos := Int64.add !buf_pos (Int64.of_int !buf_end);
      buf_end := 0;
      st_bytes := Int64.add !st_bytes (Int64.of_int !buf_end);
    );

  method from_fd_e fd esys =
    let io_dev =
      `Polldescr(Netsys.get_fd_style fd,fd,esys) in
    self # from_dev_nolimit_e io_dev esys
    >> (function
	  | `Done _ -> `Done()
	  | `Error e -> `Error e
	  | `Aborted -> `Aborted
       )

  method from_dev_e q dev size_lim_opt lines_lim_opt esys =
    if size_lim_opt = None && lines_lim_opt = None then
      self # from_dev_nolimit_e (dev :> Uq_io.in_device) esys
    else
      let size_lim =
	match size_lim_opt with
	  | None -> Int64.max_int
	  | Some n -> n in
      let lines_lim =
	match lines_lim_opt with
	  | None -> max_int
	  | Some n -> n in
      self # from_dev_limit_e q dev size_lim lines_lim esys
      
  method private from_dev_nolimit_e io_dev esys =
    if !from_fd_engine <> None then
      failwith "Mapred_io.write_file: The fd engine is still running";

    let ends_with_lf = ref true in

    let rec in_loop c inode () =
      assert (!lazy_init);
      if !buf_end = !buf_len then
	self # flush_e c inode ++ in_loop c inode
      else
	Uq_engines.meta_engine
	  (Uq_io.input_e io_dev (`Memory !bufs) !buf_end (!buf_len - !buf_end))
	++ (fun st ->
	      match st with
		| `Done n ->
		    if n > 0 then
		      ends_with_lf := !bufs.{ !buf_end + n - 1 } = '\n';
		    buf_end := !buf_end + n;
		    in_loop c inode ()
		| `Error End_of_file -> 
		    if not !ends_with_lf then (
		      (* add LF *)
		      !bufs.{ !buf_end } <- '\n';
		      incr buf_end;
		    );
		    eps_e (`Done true) esys
		| `Error error -> eps_e (`Error error) esys
		| `Aborted -> eps_e `Aborted esys
	   )
    in

    if not !lazy_init then self#lazy_init();

    let c = get_cluster fs name esys in
    let e =
      lookup_inode_e c name
      ++ (fun inode ->
	    in_loop c inode ()
	 ) in
    from_fd_engine := Some e;
    e
    >> (fun st ->
	  Plasma_client.close_cluster c;
	  from_fd_engine := None;
	  st
       )

  method private from_dev_limit_e q dev size_lim lines_lim esys =
    if !from_fd_engine <> None then
      failwith "Mapred_io.write_file: The fd engine is still running";

    dlogf
      "Mapred_io.write_file(%s): from_fd_limit_e starting" name;

    let c = get_cluster fs name esys in

    let bpos = ref 0L in
    let lpos = ref 0 in

    let rec write_queue_e inode () =
      try
	let line = Queue.peek q in
	let len = String.length line in
	let lenL = Int64.of_int len in
	if len >= !bigblocksize then
	  failwith "Mapred_io.write_file: record too large";
	if Int64.add !bpos lenL <= size_lim && !lpos+1 <= lines_lim then (
	  ignore(Queue.take q);
	  write_line_e inode line 0 len
	  ++ write_queue_e inode
	)
	else
	  eps_e (`Done false) esys
      with
	| Queue.Empty ->
	    (Uq_io.input_lines_e dev 
	     >> Uq_io.eof_as_none
	    )
	    ++ (fun lines_opt ->
		  match lines_opt with
		    | Some lines ->
			List.iter (fun s -> Queue.add s q) lines;
			write_queue_e inode ()
		    | None ->
			eps_e (`Done true) esys
	       )

    and write_line_e inode line p l =
      if p < l+1 then (
	let space = !buf_len - !buf_end in
	let n =
	  if p < l then (
	    let k = min (l - p) space in
	    Netsys_mem.blit_string_to_memory line p !bufs !buf_end k;
	    k
	  ) else (
	    !bufs.{ !buf_end } <- '\n';
	    1
	  ) in
	buf_end := !buf_end + n;
	let p' = p + n in
	if !buf_end = !buf_len then
	  self # flush_e c inode ++ (fun () -> write_line_e inode line p' l)
	else
	  write_line_e inode line p' l
      )
      else (
	st_lines := Int64.succ !st_lines;
	bpos := Int64.add !bpos (Int64.of_int l);
	incr lpos;
	eps_e (`Done ()) esys
      )
    in

    let e =
      lookup_inode_e c name
      ++ (fun inode ->
	    write_queue_e inode ()
	 ) in
    from_fd_engine := Some e;
    e
    >> (fun st ->
	  Plasma_client.close_cluster c;
	  from_fd_engine := None;
	  st
       )
    
  method close_out() =
    if not !closed then (
      self # flush();
      if Bigarray.Array1.dim !bufs <> 0 then
	Netsys_mem.memory_unmap_file !bufs;
      !shm_undo();
      closed := true
    )

  method abort() =
    if not !closed then (
      (* no flush here *)
      ( match !from_fd_engine with
	  | None -> ()
	  | Some e -> e # abort(); from_fd_engine := None
      );
      if Bigarray.Array1.dim !bufs <> 0 then
	Netsys_mem.memory_unmap_file !bufs;
      !shm_undo();
      closed := true
    )

  method filesystem =
    (fs : Mapred_fs.filesystem)

  method record_config =
    rc

  method stats = 
    Mapred_stats.of_list
      [ "write_blocks", Int64.to_float !st_blocks;
	"write_lines", Int64.to_float !st_lines;
	"write_bytes", Int64.to_float !st_bytes;
	"write_fs_time", !st_fs_time
      ]
end


let get_bof_opt flags =
  try
    match
      List.find (function `Bof_block _ -> true | _ -> false) flags
    with
      | `Bof_block n -> Some n
      | _ -> assert false
  with
    | Not_found -> None

let get_eof_opt flags =
  try
    match
      List.find (function `Eof_pos _ -> true | _ -> false) flags
    with
      | `Eof_pos n -> Some n
      | _ -> assert false
  with
    | Not_found -> None


let line_structured_format() =
object
  method read_file flags fs shm_mng rc name block len =
    let bof_opt = get_bof_opt flags in
    let eof_opt = get_eof_opt flags in
    ls_read_file
      ?force_bof:bof_opt ?force_eof:eof_opt fs shm_mng rc name block len

  method write_file fs shm_mng rc name =
    ( new ls_write_file fs shm_mng rc name :> record_writer )

  method write_suffix = ""
end

(**********************************************************************)
(* fixed size                                                         *)
(**********************************************************************)

let fs_read_file ?(force_bof=0L) ?(force_eof=Int64.max_int)
                 reclen
                 fs shm_mng rc name index len : record_reader =
  let () =
    if index < 0L || index < force_bof || len < 0L || reclen <= 0
    then invalid_arg "Mapred_io.read_file" in

  let () =
    dlogf "Mapred_io.fs_read_file name=%s index=%Ld len=%Ld force_bof=%Ld \
           force_eof=%Ld reclen=%d"
      name index len force_bof force_eof reclen in

  (* For an explanation of the following variables see ls_read_file
     above, which uses the same system.
   *)

  (* This block of variables is initialized in buf_init: *)
  let bsize = ref 0 in
  let bigblocksize = ref 0 in
  let file_eof = ref force_eof in  (* the end of the file *)
  let end_index = ref 0L in     (* the end of the requested region as index *)
  let hard_end_index = ref 0L in(* the end of the overflow region as index *)
  let semi_end_index = ref 0L in(* the preliminary end of the overflow reg *)
  let buf_num = ref 0 in        (* length of bufs in number of blocks *)
  let buf_dim = ref 0 in        (* length of bufs in bytes *)
  let bufs = ref zero_mem in    (* The buffer as bigarray *)
  let shm_undo = ref (fun () -> ()) in    (* deregisters the buffer in shm *)

  (* These are initialized by the first fill_buffers call: *)
  let buf_end = ref 0 in        (* end position in bufs in bytes *)
  let buf_count = ref 0 in      (* how many blocks we have *)
  let buf_eof = ref None in     (* the (logical) eof position if reached *)
  let buf_index = ref index in  (* the block the first buffer holds *)
  let buf_cursor = ref 0 in     (* as byte position in bufs *)
  let buf_eof_reached = ref false in
  let buf_bgrd = ref None in

  let lazy_init = ref false in        (* whether lazy_init is done *)
  let pos_in = ref 0 in               (* number of lines read so far *)
  let cur_rec = Queue.create() in     (* currently peeked lines *)
  let cur_rec_len = ref 0 in          (* length of cur_rec in bytes *)
  let closed = ref false in           (* whether closed *)

  (** [cur_rec] may also buffer lines up. This is mostly intended to speed
      up reading when the lines are very short.
   *)

  (* stats *)
  let st_blocks = ref 0L in
  let st_lines = ref 0L in
  let st_bytes = ref 0L in
  let st_fs_time = ref 0.0 in


object(self)

  method private lazy_init() =
    self # buffer_init();
    self # fill_buffers index true;
    let m1 = Int64.of_int(!bigblocksize mod reclen) in
    let bigblockmult = !bigblocksize / !bsize in
    let bigblockmultL = Int64.of_int bigblockmult in
    let offs_blocks = Int64.sub index force_bof in
    let offs_bb = Int64.div offs_blocks bigblockmultL in
    let m2 = 
      Int64.to_int(Int64.rem (Int64.mul m1 offs_bb) (Int64.of_int reclen)) in
    let m3 = if m2=0 then 0 else reclen - m2 in (* i.e. (-m2) mod reclen *)
    (* It is now possible that we have m3 >= !buf_end because the bigblock
       was not completely loaded.
     *)
    let c = ref m3 in
    while !c >= !buf_end && !buf_count > 0 do
      let e = !buf_end in
      self # fill_buffers (Int64.add !buf_index (Int64.of_int !buf_count)) true;
      c := !c - e;
    done;
    buf_cursor := min !c !buf_end;
    buf_eof_reached := (len = 0L || !buf_cursor = !buf_end);
    dlogf "Mapred_io.read_file(%s): init done" name;
    lazy_init := true;

  method private buffer_init() =
    let bs = fs#blocksize name in
    bsize := bs;
    let bsizeL = Int64.of_int bs in
    bigblocksize := round_up rc#bigblock_size bs;
    let bigblockmult = !bigblocksize / bs in
    let bigblockmultL = Int64.of_int bigblockmult in
    (* bigblockmult: how many blocks fit into a bigblock *)

    let mr_buffer_size = round_up rc#mr_buffer_size bs in
    let mr_buffer_size_tight = round_up rc#mr_buffer_size_tight bs in
    let shm_cond = shm_mng # condition `Buf in
    let shm_id = shm_mng # new_id `Buf in
    let mem_limit = 
      ( match shm_cond with
	  | `Normal -> mr_buffer_size
	  | `Tight -> mr_buffer_size_tight
      ) in
    shm_undo := (fun () -> shm_mng # book_allocation shm_id 0);
    let size = fs # size [] name in
    file_eof := min size force_eof;
    let max_index = 
      if !file_eof = 0L then
	(-1L) 
      else
	Int64.div (Int64.pred !file_eof) bsizeL in
    let eof_index =
      Int64.succ max_index in
    (** [max_index] is the index of the last block of the file.
	[eof_index] is the following block.
     *)
    dlogf
      "Mapred_io.read_file(%s,index=%Ld,len=%Ld,eofidx=%Ld)" 
      name index len eof_index;
    if index > Int64.sub eof_index len then
      failwith "Mapred_io.read_file: cannot read beyond eof";

    end_index := Int64.add index len;
    semi_end_index := min (Int64.succ !end_index) eof_index;
    hard_end_index := 
      min (Int64.add !end_index bigblockmultL) eof_index;
    dlogf
      "Mapred_io.read_file(%s,index=%Ld,len=%Ld,endidx=%Ld,hendidx=%Ld)" 
      name index len !end_index !hard_end_index;
    let buf_limit0 = max 1 (mem_limit / bs) in
    let buf_limit =
      if buf_limit0 mod 2 = 1 then buf_limit0+1 else buf_limit0 in
    (* buf_limit is divisible by 2 *)
    let buf_numL = (* min hard_len *) (Int64.of_int buf_limit) in
    buf_num := Int64.to_int buf_numL;
    buf_dim := !buf_num * bs;
    if len = 0L then (
      buf_eof_reached := true;
      semi_end_index := !end_index;
      hard_end_index := !end_index;
    );
    shm_mng # book_allocation shm_id !buf_dim;
    

  method private end_of_file() =
    dlogf
      "Mapred_io.read_file(%s,EOF,pos_in=%d)" name !pos_in;
    raise End_of_file

  method pos_in = !pos_in

  method peek_record() =
    if Queue.is_empty cur_rec then self # refill();
    try
      Queue.peek cur_rec
    with
      | Queue.Empty -> raise End_of_file

  method input_record() =
    if Queue.is_empty cur_rec then self # refill();
    try
      let line = Queue.take cur_rec in
      cur_rec_len := !cur_rec_len - String.length line - 1;
      incr pos_in;
      line
    with
      | Queue.Empty -> raise End_of_file

  method input_records q =
    if Queue.is_empty cur_rec then self # refill();
    if Queue.is_empty cur_rec then raise End_of_file;
    pos_in := !pos_in + Queue.length cur_rec;
    cur_rec_len := 0;
    Queue.transfer cur_rec q
    
  method private refill() =
    assert(Queue.is_empty cur_rec && !cur_rec_len = 0);
    if !closed then failwith "Mapred_io.read_file: closed";
    if not !lazy_init then self#lazy_init();
    assert(!lazy_init && !buf_cursor <= !buf_end);
    try
      let input_record_start = self # input_record_start in (* method lookup *)
      (* Just queue up a few lines *)
      while !cur_rec_len < 16384 do
	let line = input_record_start () in
	let len = String.length line in
	Queue.add line cur_rec;
	cur_rec_len := !cur_rec_len + len + 1
      done;
      st_lines := Int64.add !st_lines (Int64.of_int (Queue.length cur_rec))
    with
      | End_of_file -> 
	  ()

  method private input_record_start() =
    if !buf_eof_reached then self # end_of_file();
    (* if we have reclen bytes in the buffer, just return them *)
    let r = String.create reclen in
    if !buf_end - !buf_cursor >= reclen then (
      Netsys_mem.blit_memory_to_string !bufs !buf_cursor r 0 reclen;
      buf_cursor := !buf_cursor + reclen;
    )
    else (
      (* Otherwise, extract the part of the record falling into this
	 buffer, and load the next part from the file
       *)
      let m = !buf_end - !buf_cursor in
      Netsys_mem.blit_memory_to_string !bufs !buf_cursor r 0 m;
      self # input_record_next r m;
    );
    (* Can we have another record starting in this buffer? *)
    ( match !buf_eof with
	| None -> ()
	| Some eof ->
	    if !buf_cursor >= eof then
	      buf_eof_reached := true
    );
    r

  method private input_record_next r k =
    self # fill_buffers 
      (Int64.add !buf_index (Int64.of_int !buf_count)) false;
    if !buf_count = 0 then (
      buf_eof_reached := true;
      if k > 0 then
	Netlog.logf `Warning
	  "Mapred_io.fixed_size_format: incomplete last record (dropped)";
      self # end_of_file()
    )
    else (
      let m = min (reclen - k) (!buf_end - !buf_cursor) in
      Netsys_mem.blit_memory_to_string !bufs !buf_cursor r k m;
      buf_cursor := !buf_cursor + m;
      if k + m < reclen then
	(* again hit the buffer boundary *)
	self # input_record_next r (k+m)
    );
		    
  method to_fd_e fd esys =
    failwith "not implemented: to_fd_e"

  method to_dev_e io_dev esys =
    failwith "not implemented: to_dev_e"

  method to_any_e
         (dest : Netsys_mem.memory -> int -> int -> unit Uq_engines.engine) 
         esys =
    failwith "not implemented: to_any_e"

  method private fill_buffers i fakeinit =
    assert (!lazy_init || fakeinit);

    let bgrd =
      match !buf_bgrd with
        | Some bgrd -> bgrd
	| None ->
	    let bgrd =
	      Mapred_io_bgreader.create
		fs name i !semi_end_index (!buf_num/2) in
	    buf_bgrd := Some bgrd;
	    bgrd in

    let next_block =
      Int64.add
	(Mapred_io_bgreader.buffer_block bgrd) 
	(Int64.of_int (Mapred_io_bgreader.buffer_len bgrd)) in
    assert (next_block = i);

    if i >= !semi_end_index then
      Mapred_io_bgreader.extend bgrd !hard_end_index;

    let t0 = Unix.gettimeofday() in
    let len_blocks = Mapred_io_bgreader.read bgrd in
    let t1 = Unix.gettimeofday() in

    bufs := Mapred_io_bgreader.buffer bgrd;
    assert(Mapred_io_bgreader.buffer_block bgrd = i);

    self # book_buffers i len_blocks (t1 -. t0);


  method private book_buffers i len_blocks t_read =
    st_blocks := Int64.add !st_blocks (Int64.of_int len_blocks);
    st_fs_time := !st_fs_time +. t_read;

    let bsizeL = Int64.of_int !bsize in
    let len = len_blocks * !bsize in  (* rounded up to blocks *)
    let lenL = Int64.of_int len in
    let pos = Int64.mul i bsizeL in
    let nL = max (min lenL (Int64.sub !file_eof pos)) 0L in
    let n = Int64.to_int nL in
    (** This are the number of bytes we've really read into the buffer.
	Note that [pos > !file_eof] is possible (resulting in n=0)
     *)
    st_bytes := Int64.add !st_bytes nL;

    (* end of requested region as file position *)
    let eorr = min (Int64.mul !end_index bsizeL) !file_eof in

    (* end of requested region relative to buffer start. This value
       can be zero or negative!
     *)
    let rel_eorr = Int64.sub eorr pos in

    (* If [rel_eorr > n] the end of the requested region is not yet
       reached. If [rel_eorr = n] the end of the requested region is
       at the end of the buffer. If [rel_eorr < n] the end is before 
       the buffer or in the buffer. If [rel_eorr < 0] the eorr is
       even _before_ the start of the buffer.
     *)

    buf_index := i;
    buf_end := n;
    buf_count := if len=0 then 0 else (len - 1) / !bsize + 1;
    buf_eof := (if rel_eorr > nL then None 
		else Some (Int64.to_int rel_eorr)); 
    buf_cursor := 0;

    dlogf
      "Mapred_io.read_file: new buffer: index=%Ld end=%d count=%d eof=%s"
      !buf_index !buf_end !buf_count
      (match !buf_eof with None -> "none" | Some n -> string_of_int n);


  method close_in() =
    if not !closed then (
      ( match !buf_bgrd with
          | None ->
	      if Bigarray.Array1.dim !bufs <> 0 then
		Netsys_mem.memory_unmap_file !bufs;
	  | Some bgrd ->
	      Mapred_io_bgreader.close bgrd;
	      Netsys_mem.memory_unmap_file (Mapred_io_bgreader.buffer bgrd);
      );
      !shm_undo();
      closed := true
    )

  method abort() =
    self # close_in()

  method filesystem =
    fs

  method record_config =
    rc

  method stats = 
    Mapred_stats.of_list
      [ "read_blocks", Int64.to_float !st_blocks;
	"read_lines", Int64.to_float !st_lines;
	"read_bytes", Int64.to_float !st_bytes;
	"read_fs_time", !st_fs_time
      ]
 
end


class fs_write_file reclen fs shm_mng rc name =
  let () =
    dlogf "Mapred_io.fs_write_file name=%s reclen=%d" name reclen in
object (self)

  inherit ls_write_file fs shm_mng rc name
    (* This is practically the same, only that we do not add a LF
       byte at the end of the records!
     *)

  method output_records =
    self # output_records_fs reclen

  method from_fd_e fd esys =
    failwith "unimplemented: from_fd_e"

  method from_dev_e q dev size_lim_opt lines_lim_opt esys =
    failwith "unimplemented: from_dev_e"
end


let fixed_size_format reclen =
object
  method read_file flags fs shm_mng rc name block len =
    let bof_opt = get_bof_opt flags in
    let eof_opt = get_eof_opt flags in
    fs_read_file 
      ?force_bof:bof_opt ?force_eof:eof_opt reclen fs shm_mng rc name block len

  method write_file fs shm_mng rc name =
    ( new fs_write_file reclen fs shm_mng rc name :> record_writer )

  method write_suffix =
    ".fixed" ^ string_of_int reclen

end

(**********************************************************************)
(* Variable size                                                      *)
(**********************************************************************)

(* This impl of vs_read_file can read files using any chunk length,
   so far the bigblock length is a whole multiple of the chunk length.
 *)

let vs_read_file ?(force_bof=0L) ?(force_eof=Int64.max_int)
                 fs shm_mng rc name index len : record_reader =
  let () =
    if index < 0L || index < force_bof || len < 0L
    then invalid_arg "Mapred_io.read_file" in

  let () =
    dlogf "Mapred_io.vs_read_file name=%s index=%Ld len=%Ld force_bof=%Ld \
           force_eof=%Ld"
      name index len force_bof force_eof in

  (* For an explanation of the following variables see ls_read_file
     above, which uses the same system.
   *)

  (* This block of variables is initialized in buf_init: *)
  let bsize = ref 0 in
  let bigblocksize = ref 0 in
  let file_eof = ref force_eof in  (* the end of the file *)
  let end_index = ref 0L in     (* the end of the requested region as index *)
  let hard_end_index = ref 0L in(* the end of the overflow region as index *)
  let semi_end_index = ref 0L in(* the preliminary end of the overflow reg *)
  let buf_num = ref 0 in        (* length of bufs in number of blocks *)
  let buf_dim = ref 0 in        (* length of bufs in bytes *)
  let bufs = ref zero_mem in    (* The buffer as bigarray *)
  let shm_undo = ref (fun () -> ()) in    (* deregisters the buffer in shm *)

  (* These are initialized by the first fill_buffers call: *)
  let buf_end = ref 0 in        (* end position in bufs in bytes *)
  let buf_count = ref 0 in      (* how many blocks we have *)
  let buf_eof = ref None in     (* the (logical) eof position if reached *)
  let buf_index = ref index in  (* the block the first buffer holds *)
  let buf_cursor = ref 0 in     (* as byte position in bufs *)
  let buf_eof_reached = ref false in
  let buf_bgrd = ref None in

  (* Chunk management *)
  let buf_chunks = Queue.create() in
  (* Contains chunks as [(start,end,rs)] pairs, where [start] and [end]
     are positions in the buffer. While reading the pairs are popped from
     the queue. We expect [start <= buf_cursor <= end]. [rs] is the
     recstart value from the header.

     It can happen that [end > buf_end].
   *)

  let lazy_init = ref false in        (* whether lazy_init is done *)
  let pos_in = ref 0 in               (* number of lines read so far *)
  let cur_rec = Queue.create() in     (* currently peeked lines *)
  let cur_rec_len = ref 0 in          (* length of cur_rec in bytes *)
  let closed = ref false in           (* whether closed *)

  (** [cur_rec] may also buffer lines up. This is mostly intended to speed
      up reading when the lines are very short.
   *)

  (* stats *)
  let st_blocks = ref 0L in
  let st_lines = ref 0L in
  let st_bytes = ref 0L in
  let st_fs_time = ref 0.0 in


object(self)

  method private lazy_init() =
    let fail() =
      failwith "Mapred_io.var_size_format: cannot find start of record" in
    self # buffer_init();
    self # fill_buffers index true None;
    if Queue.length buf_chunks > 0 then (
      (* Init buf_cursor so it points to the first record. It is possible
	 that the first chunk has no [rs] value (i.e. -1). In this case
	 iterate over the chunks until we find an [rs] value.
       *)
      let found = ref false in
      let search_end = ref !buf_end in
      while not !found do
	let (chunk_start, chunk_end, rs) = Queue.peek buf_chunks in
	if rs >= 0 then (
	  buf_cursor := chunk_start + rs;
	  found := true;
	  (* Note that buf_cursor may now point to a position past
	     the current buffer when the chunk is incomplete. This
	     needs to be fixed below.
	   *)
	)
	else (
	  (* Switch to the next chunk. If this is the last one, we have
	     to reload the buffer.
	   *)
	  ignore(Queue.take buf_chunks);
	  if Queue.length buf_chunks = 0 then (
	    if !search_end < !bigblocksize then (
	      search_end := !search_end + !buf_end;
	      let index' =
		Int64.add !buf_index (Int64.of_int !buf_count) in
	      if index' >= !end_index then fail();
	      self # fill_buffers 
		index'
		true
		(Some(chunk_start - !buf_end, chunk_end - !buf_end, rs));
	      if !buf_count = 0 then fail()
	    )
	    else
	      fail()
	  )
	)
      done;
      (* Now do the fixup if buf_cursor points beyond the end of the buffer *)
      while !buf_cursor > !buf_end do
	assert(Queue.length buf_chunks = 1);
	let (chunk_start, chunk_end, rs) = Queue.peek buf_chunks in
	let index' =
	  Int64.add !buf_index (Int64.of_int !buf_count) in
	if index' >= !end_index then fail();
	buf_cursor := !buf_cursor - !buf_end;
	let save_cursor = !buf_cursor in
	self # fill_buffers 
	  index'
	  true
	  (Some(chunk_start - !buf_end, chunk_end - !buf_end, rs));
	if !buf_count = 0 then fail();
	buf_cursor := save_cursor
      done;
      (* If buf_cursor is past the requested region, we have nothing to
	 read
       *)
      ( match !buf_eof with
	  | None -> ()
	  | Some eof ->
	      if !buf_cursor >= eof then
		buf_eof_reached := true
      );
    ) 
    else
      buf_eof_reached := true;

    if Queue.length buf_chunks > 0 then (
      let (chunk_start, chunk_end, rs) = Queue.peek buf_chunks in
      dlogf "Mapred_io.read_file: first chunk: index=%Ld \
             chunk_start=%d chunk_end=%d \
             rs=%d buf_end=%d buf_cursor=%d buf_eof_reached=%B"
	!buf_index chunk_start chunk_end rs !buf_end !buf_cursor
	!buf_eof_reached;
    );

    dlogf "Mapred_io.read_file(%s): init done" name;
    lazy_init := true;

  method private buffer_init() =
    let bs = fs#blocksize name in
    bsize := bs;
    let bsizeL = Int64.of_int bs in
    bigblocksize := round_up rc#bigblock_size bs;
    let bigblockmult = !bigblocksize / bs in
    let bigblockmultL = Int64.of_int bigblockmult in
    (* bigblockmult: how many blocks fit into a bigblock *)

    let mr_buffer_size = round_up rc#mr_buffer_size bs in
    let mr_buffer_size_tight = round_up rc#mr_buffer_size_tight bs in
    let shm_cond = shm_mng # condition `Buf in
    let shm_id = shm_mng # new_id `Buf in
    let mem_limit = 
      ( match shm_cond with
	  | `Normal -> mr_buffer_size
	  | `Tight -> mr_buffer_size_tight
      ) in
    shm_undo := (fun () -> shm_mng # book_allocation shm_id 0);
    let size = fs # size [] name in
    file_eof := min size force_eof;
    let max_index = 
      if !file_eof = 0L then
	(-1L) 
      else
	Int64.div (Int64.pred !file_eof) bsizeL in
    let eof_index =
      Int64.succ max_index in
    (** [max_index] is the index of the last block of the file.
	[eof_index] is the following block.
     *)
    dlogf
      "Mapred_io.read_file(%s,index=%Ld,len=%Ld,eofidx=%Ld)" 
      name index len eof_index;
    if index > Int64.sub eof_index len then
      failwith "Mapred_io.read_file: cannot read beyond eof";

    end_index := Int64.add index len;
    semi_end_index := min (Int64.succ !end_index) eof_index;
    hard_end_index := 
      min (Int64.add !end_index bigblockmultL) eof_index;
    dlogf
      "Mapred_io.read_file(%s,index=%Ld,len=%Ld,endidx=%Ld,hendidx=%Ld)" 
      name index len !end_index !hard_end_index;
    let buf_limit0 = max 1 (mem_limit / bs) in
    let buf_limit =
      if buf_limit0 mod 2 = 1 then buf_limit0+1 else buf_limit0 in
    (* buf_limit is divisible by 2 *)
    let buf_numL = (* min hard_len *) (Int64.of_int buf_limit) in
    buf_num := Int64.to_int buf_numL;
    buf_dim := !buf_num * bs;
    if len = 0L then (
      buf_eof_reached := true;
      semi_end_index := !end_index;
      hard_end_index := !end_index;
    );
    shm_mng # book_allocation shm_id !buf_dim;
    

  method private end_of_file() =
    dlogf
      "Mapred_io.read_file(%s,EOF,pos_in=%d)" name !pos_in;
    raise End_of_file

  method pos_in = !pos_in

  method peek_record() =
    if Queue.is_empty cur_rec then self # refill();
    try
      Queue.peek cur_rec
    with
      | Queue.Empty -> raise End_of_file

  method input_record() =
    if Queue.is_empty cur_rec then self # refill();
    try
      let line = Queue.take cur_rec in
      cur_rec_len := !cur_rec_len - String.length line;
      incr pos_in;
      line
    with
      | Queue.Empty -> raise End_of_file

  method input_records q =
    if Queue.is_empty cur_rec then self # refill();
    if Queue.is_empty cur_rec then raise End_of_file;
    pos_in := !pos_in + Queue.length cur_rec;
    cur_rec_len := 0;
    Queue.transfer cur_rec q
    
  method private refill() =
    assert(Queue.is_empty cur_rec && !cur_rec_len = 0);
    if !closed then failwith "Mapred_io.read_file: closed";
    if not !lazy_init then self#lazy_init();
    assert(!lazy_init && !buf_cursor <= !buf_end);
    try
      let input_record_start = 
	if !Plasma_util.debug && !more_logging then
	  self # input_record_start_log
	else
	  self # input_record_start false in (* method lookup *)
      (* Just queue up a few lines *)
      while !cur_rec_len < 16384 do
	let line = input_record_start () in
	let len = String.length line in
	Queue.add line cur_rec;
	cur_rec_len := !cur_rec_len + len
      done;
      st_lines := Int64.add !st_lines (Int64.of_int (Queue.length cur_rec))
    with
      | End_of_file -> 
	  ()

  method private input_record_start_log() =
    let (chunk_start, chunk_end, _) = Queue.peek buf_chunks in
    dlogf "input_record_start chunk_start=%d chunk_end=%d buf_end=%d \
            buf_cursor=%d"
      chunk_start chunk_end !buf_end !buf_cursor;
    let r = self # input_record_start true () in
    if !Plasma_util.debug && !more_logging then
      dlogf "input_record_start #return=%d" (String.length r);
    r

  method private input_record_start log_flag () =
    if !buf_eof_reached then self # end_of_file();
    let (chunk_start, chunk_end, _) = Queue.peek buf_chunks in
    let eff_end = min chunk_end !buf_end in
    let bc = !buf_cursor in
    let bufs = !bufs in
    let r =
      if bc = eff_end then
	self # input_record_next "" None 0
      else (
	(* Only test here for the frequent cases *)
	let n0 = Char.code (Bigarray.Array1.unsafe_get bufs bc) in
	if n0 < 255 then (
	  if log_flag then
	    dlogf "input_record_start n0=%d" n0;
	  if eff_end - bc >= n0+1 then (
	    let bc1 = bc+1 in
	    let r = String.create n0 in
	    Netsys_mem.blit_memory_to_string bufs bc1 r 0 n0;
	    buf_cursor := bc1 + n0;
	    r
	  ) else (
	    let p = eff_end - bc in
	    let firstpart = String.create p in
	    Netsys_mem.blit_memory_to_string bufs bc firstpart 0 p;
	    buf_cursor := bc + p;
	    self # input_record_next firstpart (Some n0) 1
	  )
	)
	else
	  self # input_record_decode_header ""
      ) in
    (* Can we have another record starting in this buffer? *)
    ( match !buf_eof with
	| None -> ()
	| Some eof ->
	    if !buf_cursor >= eof then
	      buf_eof_reached := true
    );
    r

  method private input_record_next firstpart reclen_opt offset =
    (* We have already read [firstpart], but it is incomplete.
       If reclen_opt=None, we could not even decode the length
       of the record. If reclen_opt=Some n, the record has a length
       of n bytes, and offset is the "length of the length" in bytes.
     *)
    let (chunk_start, chunk_end, rs) = Queue.take buf_chunks in
    if !Plasma_util.debug && !more_logging then
      dlogf "input_record_next chunk_start=%d chunk_end=%d #firstpart=%d"
	chunk_start chunk_end (String.length firstpart);
    if Queue.length buf_chunks = 0 then (
      self # fill_buffers 
	(Int64.add !buf_index (Int64.of_int !buf_count))
	false
	(Some (chunk_start - !buf_end, chunk_end - !buf_end, rs));
      if !buf_count = 0 || Queue.length buf_chunks = 0 then (
	buf_eof_reached := true;
	if String.length firstpart > 0 then
	  Netlog.logf `Warning
	    "Mapred_io.var_size_format: incomplete last record (dropped)";
	self # end_of_file()
      )
    )
    else (
      let (chunk_start, _, _) = Queue.peek buf_chunks in
      buf_cursor := chunk_start;
    );
    self # input_record_decode firstpart reclen_opt offset

  method private input_record_decode firstpart reclen_opt offset =
    let k = String.length firstpart in
    let (chunk_start, chunk_end, _) = Queue.peek buf_chunks in
    let eff_end = min chunk_end !buf_end in
    if !Plasma_util.debug && !more_logging then
      dlogf "input_record_decode chunk_start=%d chunk_end=%d buf_end=%d \
             buf_cursor=%d"
	chunk_start chunk_end !buf_end !buf_cursor;
    match reclen_opt with
      | Some reclen ->
	  if !Plasma_util.debug && !more_logging then
	    dlogf "input_record_decode #firstpart=%d reclen=%d offset=%d"
	      (String.length firstpart) reclen offset;
	  let totlen = reclen + offset in
	  let m = min (totlen - k) (eff_end - !buf_cursor) in
	  let r = String.create (k + m) in
	  String.blit firstpart 0 r 0 k;
	  Netsys_mem.blit_memory_to_string !bufs !buf_cursor r k m;
	  buf_cursor := !buf_cursor + m;
	  if k + m < totlen then
	    (* again hit the buffer boundary *)
	    self # input_record_next r reclen_opt offset
	  else
	    String.sub r offset reclen
      | None ->
	  if !Plasma_util.debug && !more_logging then
	    dlogf "input_record_decode #firstpart=%d reclen=None"
	      (String.length firstpart);
	  self # input_record_decode_header firstpart

  method private input_record_decode_header firstpart =
    let (chunk_start, chunk_end, _) = Queue.peek buf_chunks in
    let eff_end = min chunk_end !buf_end in

    let rec check b =
      let l = String.length b in
      if l = 0 then
	extend b
      else
	let b0 = Char.code b.[0] in
	if b0 < 255 then
	  self # input_record_decode b (Some b0) 1
	else
	  if l < 9 then
	    extend b
	  else (
	    let nL =
	      Netnumber.int64_of_int8 (Netnumber.BE.read_int8 b 1) in
	    if nL < 0L || nL > Int64.of_int !bigblocksize then
	      failwith "Mapred_io.var_size_format: record too large";
	    let n = Int64.to_int nL in
	    self # input_record_decode b (Some n) 9
	  )

    and extend b =
      (* Extend by another char, and check again. We do this at most 9
	 times, so performance is not too important here.
       *)
      if !buf_cursor < eff_end then (
	let c = (!bufs).{ !buf_cursor } in
	incr buf_cursor;
	check (b ^ String.make 1 c)
      )
      else
	self # input_record_next b None 0 in
	
    check firstpart
		    
  method to_fd_e fd esys =
    failwith "not implemented: to_fd_e"

  method to_dev_e io_dev esys =
    failwith "not implemented: to_dev_e"

  method to_any_e
         (dest : Netsys_mem.memory -> int -> int -> unit Uq_engines.engine)  
         esys =
    failwith "not implemented: to_any_e"

  method private fill_buffers i fakeinit prev_chunk_end_opt =
    assert (!lazy_init || fakeinit);

    let bgrd =
      match !buf_bgrd with
        | Some bgrd -> bgrd
	| None ->
	    let bgrd =
	      Mapred_io_bgreader.create
		fs name i !semi_end_index (!buf_num/2) in
	    buf_bgrd := Some bgrd;
	    bgrd in

    let next_block =
      Int64.add
	(Mapred_io_bgreader.buffer_block bgrd) 
	(Int64.of_int (Mapred_io_bgreader.buffer_len bgrd)) in
    assert (next_block = i);

    if i >= !semi_end_index then
      Mapred_io_bgreader.extend bgrd !hard_end_index;

    let t0 = Unix.gettimeofday() in
    let len_blocks = Mapred_io_bgreader.read bgrd in
    let t1 = Unix.gettimeofday() in

    bufs := Mapred_io_bgreader.buffer bgrd;
    assert(Mapred_io_bgreader.buffer_block bgrd = i);

    self # book_buffers i len_blocks (t1 -. t0);
    self # find_chunks prev_chunk_end_opt;

  method private book_buffers i len_blocks t_read =
    st_blocks := Int64.add !st_blocks (Int64.of_int len_blocks);
    st_fs_time := !st_fs_time +. t_read;

    let bsizeL = Int64.of_int !bsize in
    let len = len_blocks * !bsize in  (* rounded up to blocks *)
    let lenL = Int64.of_int len in
    let pos = Int64.mul i bsizeL in
    let nL = max (min lenL (Int64.sub !file_eof pos)) 0L in
    let n = Int64.to_int nL in
    (** This are the number of bytes we've really read into the buffer.
	Note that [pos > !file_eof] is possible (resulting in n=0)
     *)
    st_bytes := Int64.add !st_bytes nL;

    (* end of requested region as file position *)
    let eorr = min (Int64.mul !end_index bsizeL) !file_eof in

    (* end of requested region relative to buffer start. This value
       can be zero or negative!
     *)
    let rel_eorr = Int64.sub eorr pos in

    (* If [rel_eorr > n] the end of the requested region is not yet
       reached. If [rel_eorr = n] the end of the requested region is
       at the end of the buffer. If [rel_eorr < n] the end is before 
       the buffer or in the buffer. If [rel_eorr < 0] the eorr is
       even _before_ the start of the buffer.
     *)

    buf_index := i;
    buf_end := n;
    buf_count := if len=0 then 0 else (len - 1) / !bsize + 1;
    buf_eof := (if rel_eorr > nL then None 
		else Some (Int64.to_int rel_eorr)); 
    buf_cursor := 0;

    dlogf
      "Mapred_io.read_file: new buffer: index=%Ld end=%d count=%d eof=%s"
      !buf_index !buf_end !buf_count
      (match !buf_eof with None -> "none" | Some n -> string_of_int n);


  method private find_chunks prev_chunk_end_opt =
    (* Initialize buf_chunks for a newly filled buffer.

       prev_chunk_end_opt: If [Some(start,end,rs)], this is the previous
       chunk (i.e. the last of the prev buffer). It might be continued in the
       current buffer.
     *)
    Queue.clear buf_chunks;
    let bbsizeL = Int64.of_int !bigblocksize in
    let k = ref 0 in
    ( match prev_chunk_end_opt with
	| None -> ()
	| Some(chunk_start,chunk_end,rs) ->
	    dlogf "find_chunks prev: chunk_start=%d chunk_end=%d rs=%d"
	      chunk_start chunk_end rs;
	    (* These positions are already relative to the new buffer *)
	    if chunk_end > 0 then (
	      Queue.add (chunk_start,chunk_end,rs) buf_chunks;
	      k := chunk_end;
	    )
    );

    (* Get [offs], the index of the chunk in the sequence *)
    let buf_pos = Int64.mul !buf_index (Int64.of_int !bsize) in
    let chunk_pos = Int64.add buf_pos (Int64.of_int !k) in
    let bof_pos = Int64.mul force_bof (Int64.of_int !bsize) in
    let delta_pos = ref(Int64.sub chunk_pos bof_pos) in

    dlogf "find_chunks index=%Ld k_start=%d buf_end=%d"
      !buf_index !k !buf_end;

(*
    let d = Bigarray.Array1.dim !bufs in
    let b = String.create d in
    Netsys_mem.blit_memory_to_string !bufs 0 b 0 d;
    dlogf "bufs=%S" b;
 *)

    let h = String.create 32 in
    let count = ref 0 in
    while !k + 32 <= !buf_end do
      if !Plasma_util.debug && !more_logging then
	dlogf "find_chunks k=%d" !k;
      Netsys_mem.blit_memory_to_string !bufs !k h 0 32;
      let h1 = String.sub h 0 28 in
      let chunksize =
	Netnumber.int64_of_int8 (Netnumber.BE.read_int8 h 0) in
      if chunksize < 64L || Int64.rem bbsizeL chunksize <> 0L then
	failwith (sprintf 
		    "Mapred_io.var_size_format: cannot decode chunk - \
                     bad chunk size (chunksize=%Ld bigblock=%Ld index=%Ld \
                     pos=%d)"
		    chunksize bbsizeL !buf_index !k);
      let offs = Int64.div !delta_pos chunksize in
      let h2 = Digest.string (h1 ^ Int64.to_string offs) in
      if String.sub h2 0 4 <> String.sub h 28 4 then
	failwith (sprintf
		    "Mapred_io.var_size_format: cannot decode chunk header \
                     (index=%Ld pos=%d)" !buf_index !k);
    let datasize =
	Netnumber.int64_of_int8 (Netnumber.BE.read_int8 h 8) in
      if datasize < 0L || datasize > Int64.sub chunksize 32L then
	failwith "Mapred_io.var_size_format: cannot decode chunk - bad data size";
(*
      if Int64.to_int datasize > !buf_end - !k - 32 then
	failwith "Mapred_io.var_size_format: cannot decode chunk - data exceeds buffer";
 *)
      let recstart =
	Netnumber.int64_of_int8 (Netnumber.BE.read_int8 h 16) in
      if recstart >= datasize then
	failwith (sprintf
		    "Mapred_io.var_size_format: cannot decode chunk - \
                     bad record start (recstart=%Ld datasize=%Ld index=%Ld \
                     pos=%d)"
		    recstart datasize !buf_index !k);
      let flags =
	Netnumber.int32_of_int4 (Netnumber.BE.read_int4 h 24) in
      if flags <> 0l then
	failwith "Mapred_io.var_size_format: unsupported flags found in file";

      if datasize > 0L then
	Queue.add
	  (!k + 32,
	   !k + 32 + Int64.to_int datasize,
	   Int64.to_int recstart)
	  buf_chunks;
      k := !k + Int64.to_int chunksize;
      delta_pos := Int64.add !delta_pos chunksize;
      incr count;
    done;

    if Queue.length buf_chunks > 0 then (
      let (chunk_start, chunk_end, _) = Queue.peek buf_chunks in
      buf_cursor := max 0 chunk_start
    );

    dlogf "find_chunks: count=%d total=%d k_end=%d"
      !count (Queue.length buf_chunks) !k


  method close_in() =
    if not !closed then (
      ( match !buf_bgrd with
          | None ->
	      if Bigarray.Array1.dim !bufs <> 0 then
		Netsys_mem.memory_unmap_file !bufs;
	  | Some bgrd ->
	      Mapred_io_bgreader.close bgrd;
	      Netsys_mem.memory_unmap_file (Mapred_io_bgreader.buffer bgrd);
      );
      !shm_undo();
      closed := true
    )

  method abort() =
    self # close_in()

  method filesystem =
    fs

  method record_config =
    rc

  method stats = 
    Mapred_stats.of_list
      [ "read_blocks", Int64.to_float !st_blocks;
	"read_lines", Int64.to_float !st_lines;
	"read_bytes", Int64.to_float !st_bytes;
	"read_fs_time", !st_fs_time
      ]
 
end


class vs_write_file ?(chunklen=65536) fs shm_mng rc name : record_writer =
  let () =
    dlogf "Mapred_io.vs_write_file name=%s chunklen=%d" name chunklen in

  (* This block of variables is initialized in buf_init_e: *)
  let bsize = ref 0 in
  let bigblocksize = ref 0 in
  let buf_len = ref 0 in        (* length of bufs in bytes *)
  let bufs = ref zero_mem in    (* The buffer as bigarray *)
  let shm_undo = ref (fun () -> ()) in    (* deregisters the buffer in shm *)

  (* The following variables are created with their right initial value: *)
  let buf_end = ref 0 in        (* end position in bufs in bytes *)
  let buf_pos = ref 0L in       (* byte position in file of first buf byte *)
  let chunk_start = ref 0 in    (* where the chunk starts in the buffer *)
  let chunk_end = ref 0 in      (* where the chunk ends in the buffer *)
  let recstart = ref (-1) in    (* start of first record *)

  let lazy_init = ref false in        (* whether lazy_init_e is done *)
  let closed = ref false in

  let pos_out = ref 0 in
  let qout = Queue.create() in
  let qout_len = ref 0 in

  let copy_in_from_buf = lazy(fs # copy_in_from_buf name) in

  (* stats *)
  let st_blocks = ref 0L in
  let st_lines = ref 0L in
  let st_bytes = ref 0L in
  let st_fs_time = ref 0.0 in

object(self)

  method private lazy_init() =
    self # buffer_init()

  method private buffer_init() =
    let bs = fs # blocksize name in
    bsize := bs;
    bigblocksize := round_up rc#bigblock_size bs;
    if (chunklen > !bigblocksize || !bigblocksize mod chunklen <> 0) then
      failwith "Mapred_io.var_size_format: big block not a multiple of 64K";
    let mr_buffer_size = round_up rc#mr_buffer_size bs in
    let mr_buffer_size_tight = round_up rc#mr_buffer_size_tight bs in
    let shm_cond = shm_mng # condition `Buf in
    let shm_id = shm_mng # new_id `Buf in
    let mem_limit = 
      ( match shm_cond with
	  | `Normal -> mr_buffer_size
	  | `Tight -> mr_buffer_size_tight
      ) in
    shm_undo := (fun () -> shm_mng # book_allocation shm_id 0);
	  
    let ch = fs # write [`Truncate] name in
    ch # close_out();

    dlogf "Mapred_io.write_file(%s)" name;
    let buf_num = ref(max 1 (mem_limit / bs)) in
    (* Ensure that buf_len is a multiple of chunklen. We need that because
       the header of a chunk is first created when the chunk is complete.
       So the chunk must fit completely into the buffer.
     *)
    while (!buf_num * bs) mod chunklen > 0 do
      incr buf_num
    done;
    buf_len := !buf_num * bs;
    shm_mng # book_allocation shm_id !buf_len;
    bufs := Netsys_mem.alloc_memory_pages !buf_len;
    buf_end := 32; (* pretend the chunk header would exist already *)
    chunk_end := chunklen;
    dlogf "vs buffer_init bs=%d bigblock=%d chunklen=%d buf_num=%d"
      bs !bigblocksize chunklen !buf_num;
    lazy_init := true;


  method output_record line =
    Queue.add line qout;
    qout_len := !qout_len + String.length line;
    if !qout_len > 16384 then
      self # output_records qout

  method output_records q =
    if !closed then failwith "Mapred_io.write_file: closed";
    if not !lazy_init then self # lazy_init();
    assert (!lazy_init);
    consume
      (fun line ->
	 let l = String.length line in
	 if l >= !bigblocksize then (* FIXME: take chunk overhead into account *)
	   failwith "Mapred_io.write_file: record too large";
	 let rhlen = if l < 255 then 1 else 9 in
	 let rh = String.create rhlen in
	 if l < 255 then
	   String.unsafe_set rh 0 (Char.unsafe_chr l)
	 else (
	   String.unsafe_set rh 0 '\xFF';
	   Netnumber.BE.write_int8_unsafe rh 1 (Netnumber.int8_of_int l)
	 );
	 let p = ref 0 in
	 while !p < rhlen do
	   let space = !chunk_end - !buf_end in
	   let n = min (rhlen - !p) space in
	   Netsys_mem.blit_string_to_memory rh !p !bufs !buf_end n;
	   if !recstart < 0 && !p=0 && n>0 then
	     recstart := !buf_end - !chunk_start - 32;
	   buf_end := !buf_end + n;
	   p := !p + n;
	   if !buf_end = !chunk_end then
	     self # finish_chunk false
	 done;
	 while !p < l+rhlen do
	   let space = !chunk_end - !buf_end in
	   let p1 = !p - rhlen in
	   let n = min (l - p1) space in
	   Netsys_mem.blit_string_to_memory line p1 !bufs !buf_end n;
	   buf_end := !buf_end + n;
	   p := !p + n;
	   if !buf_end = !chunk_end then
	     self # finish_chunk false
	 done;
	 st_lines := Int64.succ !st_lines;
	 incr pos_out;
	 qout_len := !qout_len - l;
      )
      q

  method private finish_chunk eof_flag =
    if not !lazy_init then self # lazy_init();

    let ds = !buf_end - !chunk_start - 32 in
    self # chunk_flush();

    if ds = 0 then (
      (* Avoid to write an empty chunk: *)
      buf_end := !chunk_start;
      chunk_end := !chunk_start;
    );

    if !chunk_end < !buf_len && not eof_flag then (
      chunk_start := !chunk_end;
      chunk_end := !chunk_start + chunklen;
      buf_end := !chunk_start + 32;
    )
    else (
      if !buf_end > 0 then (
	self # buf_flush();
	self # buf_advance();
      );
      chunk_start := 0;
      chunk_end := chunklen;
      buf_end := 32
    );
    recstart := (-1)

  method flush () =
    if !closed then failwith "Mapred_io.write_file: closed";
    if not !lazy_init then self # lazy_init();
    self # output_records qout;
    self # chunk_flush();
    if !buf_end > 0 then
      self # buf_flush()
    (* but no buf_advance - otherwise the current chunk could not be
       extended
     *)

  method private chunk_flush() =
    dlogf "finish_chunk chunk_start=%d chunk_end=%d buf_end=%d recstart=%d"
      !chunk_start !chunk_end !buf_end !recstart;
    let ds = !buf_end - !chunk_start - 32 in
    let h1 = String.create 28 in
    Netnumber.BE.write_int8 h1 0 (Netnumber.int8_of_int chunklen);
    Netnumber.BE.write_int8 h1 8 (Netnumber.int8_of_int ds);
    Netnumber.BE.write_int8 h1 16 (Netnumber.int8_of_int !recstart);
    Netnumber.BE.write_int4 h1 24 (Netnumber.int4_of_int 0);
    let chunk_pos = Int64.add !buf_pos (Int64.of_int !chunk_start) in
    let offs = Int64.div chunk_pos (Int64.of_int chunklen) in
    let h2 = String.sub (Digest.string (h1 ^ Int64.to_string offs)) 0 4 in
    
    Netsys_mem.blit_string_to_memory h1 0 !bufs !chunk_start 28;
    Netsys_mem.blit_string_to_memory h2 0 !bufs (!chunk_start+28) 4;


  method private buf_flush() =
    dlogf "Mapred_io.write_file.flush: name=%s pos=%Ld len=%d"
      name !buf_pos !buf_end;
    let t0 = Unix.gettimeofday() in
    (Lazy.force copy_in_from_buf) !buf_pos !bufs !buf_end;
    let t1 = Unix.gettimeofday() in
    st_fs_time := !st_fs_time +. (t1 -. t0);

  method private buf_advance() =
    let b = if !buf_end = 0 then 0 else (!buf_end - 1) / !bsize + 1 in
    st_blocks := Int64.add !st_blocks (Int64.of_int b);

    (* copy_in works only with multiples of bsize. So if we did not match
       this, retain the end of the buffer. (The requirement here is
       that !buf_pos must remain a multiple of bsize.)
     *)
    let tail = !buf_end mod !bsize in
    if tail > 0 then (
      let n = !buf_end - tail in
      Plasma_platform.blit_memory !bufs (!buf_end - tail) !bufs 0 tail;
      buf_pos := Int64.add !buf_pos (Int64.of_int n);
      buf_end := tail;
      st_bytes := Int64.add !st_bytes (Int64.of_int n);
    )
    else (
      buf_pos := Int64.add !buf_pos (Int64.of_int !buf_end);
      buf_end := 0;
      st_bytes := Int64.add !st_bytes (Int64.of_int !buf_end);
    );
    

  method close_out() =
    if not !closed then (
      self # output_records qout;
      self # finish_chunk true;
      if Bigarray.Array1.dim !bufs <> 0 then
	Netsys_mem.memory_unmap_file !bufs;
      !shm_undo();
      closed := true
    )

  method abort() =
    if not !closed then (
      (* no flush here *)
      if Bigarray.Array1.dim !bufs <> 0 then
	Netsys_mem.memory_unmap_file !bufs;
      !shm_undo();
      closed := true
    )

  method filesystem =
    fs

  method record_config =
    rc

  method stats = 
    Mapred_stats.of_list
      [ "write_blocks", Int64.to_float !st_blocks;
	"write_lines", Int64.to_float !st_lines;
	"write_bytes", Int64.to_float !st_bytes;
	"write_fs_time", !st_fs_time
      ]

  method from_fd_e fd esys =
    failwith "unimplemented: from_fd_e"

  method from_dev_e q dev size_lim_opt lines_lim_opt esys =
    failwith "unimplemented: from_dev_e"
end


let var_size_format () =
object
  method read_file flags fs shm_mng rc name block len =
    let bof_opt = get_bof_opt flags in
    let eof_opt = get_eof_opt flags in
    vs_read_file
      ?force_bof:bof_opt ?force_eof:eof_opt fs shm_mng rc name block len

  method write_file fs shm_mng rc name =
    new vs_write_file fs shm_mng rc name

  method write_suffix =
    ".var"
end


let fixed_re = Pcre.regexp ".*\\.fixed([0-9]+)$"

let auto_input_format () =
object
  method read_file flags fs shm_mng rc name block len =
    let factory =
      if Filename.check_suffix name ".var" then
	var_size_format()
      else
	try
	  match Pcre.extract ~rex:fixed_re ~full_match:false name with
	    | [| n_str |] ->
		let n = int_of_string n_str in
		fixed_size_format n
	    | _ ->
		raise Not_found
	with
	  | Not_found ->
	      line_structured_format() in
    factory # read_file flags fs shm_mng rc name block len

  method write_file fs shm_mng rc name =
    failwith "Mapred_io.auto_input_format.write_file: \
              writing is not supported for auto_input_format"

  method write_suffix = ""

end


(**********************************************************************)
(* Generic                                                            *)
(**********************************************************************)


type sync_readers =
    (unit -> record_reader) list

type async_readers =
    (unit -> record_reader Uq_engines.engine) list


let read_multiple fs rc
                  ~(readers :[`Sync of sync_readers | `Async of async_readers])
                  () =
  let readers = ref readers in
  let pos_in = ref 0 in
  let cur_rec = Queue.create() in
  let cur_rd = ref None in

  let stats = ref Mapred_stats.empty in

  let rec refill() =
    assert(Queue.is_empty cur_rec);
    match !cur_rd with
      | Some rd ->
	  ( try
	      rd # input_records cur_rec
	    with
	      | End_of_file ->
		  if Queue.is_empty cur_rec then (
		    rd # close_in();
		    cur_rd := None;
		    stats := Mapred_stats.add !stats rd#stats;
		    next_reader()
		  )
	  )
      | None ->
	 next_reader()

  and next_reader() =
    match !readers with
      | `Sync(first :: rest) ->
	  readers := `Sync rest;
	  let rd = first() in
	  cur_rd := Some rd;
	  refill()
      | `Async(first :: rest) ->
	  readers := `Async rest;
	  let rd = Plasma_client.sync first () in
	  cur_rd := Some rd;
	  refill()
      | `Sync [] | `Async [] ->
	  raise End_of_file
  in

object(self)
  method pos_in = !pos_in

  method peek_record() =
    if Queue.is_empty cur_rec then refill();
    try
      Queue.peek cur_rec
    with
      | Queue.Empty ->
	  raise End_of_file

  method input_record() =
    if Queue.is_empty cur_rec then refill();
    try
      let line = Queue.take cur_rec in
      incr pos_in;
      line
    with
      | Queue.Empty ->
	  raise End_of_file

  method input_records q =
    if Queue.is_empty cur_rec then refill();
    pos_in := !pos_in + Queue.length cur_rec;
    Queue.transfer cur_rec q

  method to_fd_e fd esys =
    let io_dev =
      `Polldescr(Netsys.get_fd_style fd,fd,esys) in
    self # to_dev_e io_dev esys

  method to_dev_e io_dev esys =
    self # to_any_e
      (fun m pos len ->
	 Uq_io.really_output_e
	   io_dev
	   (`Memory m)
	   pos
	   len
      )
      esys

  method to_any_e
         (dest : Netsys_mem.memory -> int -> int -> unit Uq_engines.engine)
         esys =
    dlogf
      "Mapred_io.read_multiple: to_fd_e start";
    let rec out_loop() =
      if not (Queue.is_empty cur_rec) then (
	let l = Queue.fold (fun acc line -> line::acc) [] cur_rec in
	let s = String.concat "\n" (List.rev ("" :: l)) in
	Queue.clear cur_rec;
	output_string_e dest s ++ out_loop
      )
      else  ( 
	match !cur_rd with
	  | Some rd ->
	      cur_rd := None;
	      rd # to_any_e dest esys
	      ++ (fun () -> rd # close_in(); out_loop())
	  | None ->
	      ( match !readers with
		  | `Async(first :: rest) ->
		      readers := `Async rest;
		      first()
		      ++ (fun rd ->
			    cur_rd := Some rd;
			    out_loop()
			 )
		  | `Sync(first :: rest) ->
		      readers := `Sync rest;
		      cur_rd := Some(first());
		      out_loop()
		  | `Async [] | `Sync [] ->
		      dlogf
			"Mapred_io.read_multiple: to_fd_e end";
		      eps_e (`Done()) esys
	      )
      )
    in
    out_loop()

  method close_in() =
    match !cur_rd with
      | Some rd ->
	  stats := Mapred_stats.add !stats rd#stats;
	  rd # close_in()
      | None ->
	  ()

  method abort() =
    match !cur_rd with
      | Some rd ->
	  stats := Mapred_stats.add !stats rd#stats;
	  rd # abort()
      | None ->
	  ()

  method filesystem =
    fs

  method record_config =
    rc

  method stats = !stats
end


let write_multiple fs shm_mng rc prefix size_limit lines_limit
                   ~create_sync ~create_async
		   (rwf : record_writer_factory) : record_writer =
  let n = ref 0 in
  let current = ref None in
  let cur_pos_out = ref 0 in    (* only for checking the limit *)
  let cur_bpos_out = ref 0L in  (* only for checking the limit *)
  let stats = ref Mapred_stats.empty in

  let switch() = 
    dlogf
      "Mapred_io.write_multiple(%s): switch n=%d" prefix !n;
    let name = create_sync prefix !n in
    incr n;
    dlogf
      "Mapred_io.write_multiple(%s): switch sub-writer: %s" prefix name;
    let w = rwf # write_file fs shm_mng rc name in
    current := Some w;
    cur_pos_out := 0;
    cur_bpos_out := 0L;
    w in

  let switch_e esys =
    match create_async with
      | None -> 
	  eps_e (`Done (switch())) esys
      | Some create_e ->
	  dlogf
	    "Mapred_io.write_multiple(%s): switch n=%d" prefix !n;
	  let k = !n in
	  incr n;
	  create_e prefix k
	  ++ (fun name ->
		dlogf
		  "Mapred_io.write_multiple(%s): switch sub-writer: %s"
		  prefix name;
		let w = rwf # write_file fs shm_mng rc name in
		current := Some w;
		cur_pos_out := 0;
		cur_bpos_out := 0L;
		eps_e (`Done w) esys
	     ) in
object(self)

  method output_records q =
    let out = self#output_record in
    Queue.iter out q

  method output_record line =
    let w =
      match !current with
	| None ->
	    switch()
	| Some w ->
	    let p = 
	      Int64.add !cur_bpos_out (Int64.of_int (String.length line))  in
	    let l =
	      !cur_pos_out + 1 in
	    if p > size_limit || l > lines_limit then (
	      w # close_out();
	      stats := Mapred_stats.add !stats w#stats;
	      switch()
	    )
	    else (
	      cur_bpos_out := p;
	      cur_pos_out := l;
	      w
	    ) in
    w # output_record line

  method from_dev_e q bdev size_limit lines_limit esys =
    failwith "from_dev_e: not implemented in write_multiple"

  method from_fd_e fd esys =
    dlogf
      "Mapred_io.write_multiple(%s): from_fd_e starting" prefix;

    let dev =
      `Polldescr(Netsys.get_fd_style fd,fd,esys) in
    let bdev =
      `Buffer_in(Uq_io.create_in_buffer dev) in
    let q = 
      Queue.create() in

    let rec loop w =
      dlogf
	"Mapred_io.write_multiple(%s): from_fd_e loop #prepend=%d"
	prefix (Queue.length q);
      w # from_dev_e q bdev (Some size_limit) (Some lines_limit) esys
      ++ (fun eof->
	    if eof then
	      eps_e (`Done ()) esys
	    else (
	      stats := Mapred_stats.add !stats w#stats;
	      w # abort();  (* non-blocking. w is already flushed *)
	      switch_e esys
	      ++ (fun w' -> loop w')
	    )	    
	 )
    in
    ( match !current with
	| None -> switch_e esys
	| Some w -> eps_e (`Done w) esys
    )
    ++ (fun w -> loop w)

  method flush() =
    match !current with
      | None -> ()
      | Some w -> w # flush()

  method close_out () =
    match !current with
      | None -> ()
      | Some w -> 
	  stats := Mapred_stats.add !stats w#stats;
	  w # close_out()

  method abort() =
    match !current with
      | None -> ()
      | Some w -> 
	  stats := Mapred_stats.add !stats w#stats;
	  w # abort()

  method filesystem =
    fs

  method record_config =
    rc

  method stats = !stats
end


let create_file ?(repl=0) ?(pref_nodes=[]) fs name =
  let ch = 
    fs # write [`Create; `Exclusive; `Repl repl; `Location pref_nodes] name in
  ch # close_out()


let create_dir fs name =
  fs # mkdir [] name


let delete_file fs name =
  fs # remove [] name

let delete_rec fs name =
  fs # remove [`Recursive] name


let file_blocks fs name =
  let bsize = fs # blocksize name in
  let bsizeL = Int64.of_int bsize in
  let eof = fs # size [] name in
  let blocks =
    if eof = 0L then 0L 
    else
      Int64.succ (Int64.div (Int64.pred eof) bsizeL) in
  blocks


type fileinfo =
    [ `Regular of int64
    | `Directory
    | `Other
    ]

(*
  (* Old version using Plasma_cluster. Leave this here, because
     it has the nice feature to be safe against rename races
 *)

let scan_dir c rootdir deeply =

  let rec scan_inode dir_inode depth =
    if depth > 50 then
      failwith "Mapred_io.scan: too deeply nested (symlink loop?)";
    let files0 =
      Plasma_client.with_trans c
	(fun trans ->
	   Plasma_client.list_inode trans dir_inode
	) in
    let files1 =
      List.filter
	(fun (n,_) ->
	   n <> "" && n.[0] <> '.' && n.[0] <> '_'
	) 
	files0 in
    let files2 =
      Plasma_client.with_trans c
	(fun trans ->
	   List.map
	     (fun (n,inode0) ->
		let ii0 = Plasma_client.get_cached_inodeinfo c inode0 false in
		(* if the file is a symlink, do a lookup *)
		if ii0.Plasma_rpcapi_aux.filetype = `ftype_symlink then
		  let inode1 = 
		    Plasma_client.dir_lookup trans dir_inode n false in
		  let ii1 = Plasma_client.get_cached_inodeinfo c inode1 false in
		  (n,inode1,ii1)
		else
		  (n,inode0,ii0)
	     )
	     files1
	) in
    let regular_files =
      List.filter
	(fun (_,_,ii) ->
	   ii.Plasma_rpcapi_aux.filetype = `ftype_regular
	)
	files2 in
    let subtrees =
      if deeply then (
	let subdirs =
	  List.filter
	    (fun (_,_,ii) ->
	       ii.Plasma_rpcapi_aux.filetype = `ftype_directory
	    )
	    files2 in
	List.map
	  (fun (d,d_inode,_) ->
	     let l = scan_inode d_inode (depth+1) in
	     List.map
	       (fun (n,inode,ii) ->
		  (d ^ "/" ^ n, inode, ii)
	       )
	       l
	  )
	  subdirs
      )
      else [] in
    regular_files @ List.flatten subtrees in
  
  let dir_inode =
    Plasma_client.with_trans c
      (fun trans ->
	 Plasma_client.lookup trans rootdir false
      ) in
  scan_inode dir_inode 0
 *)


let scan_file fs name =
  match fs # test_list [] name [`D;`F;`N] with
    | [ is_dir; is_reg; is_ex ] ->
	if is_dir then
	  `Directory
	else
	  if is_reg then
	    let n = fs # size [] name in
	    `Regular n
	  else
	    if is_ex then
	      `Other
	    else
	      raise(Unix.Unix_error(Unix.ENOENT, "Mapred_io.scan_file",
				    name))
    | _ -> assert false
	

let scan_dir fs rootdir deeply =

  let rec scan_dir dir depth =
    if depth > 50 then
      failwith "Mapred_io.scan: too deeply nested (symlink loop?)";
    let files0 = fs # readdir [] dir in
    let files1 =
      List.filter
	(fun n ->
	   n <> "" && n.[0] <> '.' && n.[0] <> '_'
	) 
	files0 in
    let files2 =
      List.map
	(fun n ->
	   let full_n = (dir ^ "/" ^ n) in
	   try
	     (n, scan_file fs full_n)
	   with
	     | Unix.Unix_error(_,_,_) -> (n, `Other)
	)
	files1 in
    let regular_files =
      List.filter
	(fun (_,info) ->
	   match info with `Regular _ -> true | _ -> false
	)
	files2 in
    let subtrees =
      if deeply then (
	let subdirs =
	  List.filter
	    (fun (_,info) ->
	       info = `Directory
	    )
	    files2 in
	List.map
	  (fun (d,_) ->
	     let l = scan_dir (dir ^ "/" ^ d) (depth+1) in
	     List.map
	       (fun (n,info) ->
		  (d ^ "/" ^ n, info)
	       )
	       l
	  )
	  subdirs
      )
      else [] in
    regular_files @ List.flatten subtrees in
  
  scan_dir rootdir 0


let file_exists fs name =
  try ignore(scan_file fs name); true
  with _ -> false


let is_directory fs name =
  try
    let info = scan_file fs name in
    info = `Directory
  with _ -> false



let divide rc d =
  let bigblock_size = rc # bigblock_size in
  let mr_buffer_size = rc # mr_buffer_size / d in
  let mr_buffer_size_tight = rc # mr_buffer_size_tight / d in
object
  method bigblock_size = bigblock_size
  method mr_buffer_size = mr_buffer_size
  method mr_buffer_size_tight = mr_buffer_size_tight
end
