(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Registered functions (low-level) *)

(** Registered functions get a unique ID, and can be invoked remotely
    on the task nodes. On this low-level, the registration procedure
    is unsafe. See {!Mapred_toolkit} for how to use it safely.
 *)

type 't rfun constraint 't = _ -> _ ;;
  (** A registered function has this type. The argument must be a
      function type, e.g [(int -> string) rfun].

      An [rfun] is marshallable.
   *)

exception Rfun_not_found of string
  (** The named registered function is not found *)

val register : string -> ('a -> 'b) -> ('a -> 'b) rfun
  (** [register name f]: Registers the function [f] under [name]. The
      name must be unique, and must not start with an underscore.

      Registrations are only possible in the initialization phase of
      the program, because they must be done identically in all
      running instances of the program (i.e. on all task nodes).
   *)

val finish : unit -> unit
  (** Disables further registrations (at the end of the initialization). *)

val lookup : ('a -> 'b) rfun -> ('a -> 'b)
  (** Returns a registered function *)

val get_id : _ rfun -> string
  (** Returns the ID *)

val apply_partially : ('a -> 'b -> 'c) rfun -> 'a -> ('b -> 'c) rfun
  (** Create a new rfun by applying the first argument only. Note that
      the argument needs to be marshallable!
   *)

val p4_new_file : (unit -> unit) -> unit
  (** Used in [mapred.mlp] to announce a new source file *)

val p4_register : (unit -> unit) -> ('a -> 'b) -> ('a -> 'b) rfun
  (** Used in [mapred.mlp] to register a function with an automatic name *)

