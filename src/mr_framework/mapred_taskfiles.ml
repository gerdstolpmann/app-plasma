(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

class type taskfile_manager =
object
  method local_directory : string
  method log_directory : string
  method copy_to_local_directory : string list -> unit
  method delete_local_directory : unit -> unit
  method task_save_log_directory : Mapred_fs.filesystem -> string list -> unit
  method task_files_log_directory : unit -> string list
  method task_delete_log_directory : unit -> unit
end


let ssh_cmd host cmd =
  let addr =
    (Uq_resolver.get_host_by_name host).Unix.h_addr_list.(0) in
  if addr = Unix.inet_addr_loopback || addr = Unix.inet6_addr_loopback then
    cmd
  else
    ("ssh -q -a -k -x " ^ Filename.quote host ^ " " ^ cmd)


let scp_to_cmd localfiles host remotedest =
  let addr =
    (Uq_resolver.get_host_by_name host).Unix.h_addr_list.(0) in
  if addr = Unix.inet_addr_loopback || addr = Unix.inet6_addr_loopback then
    ("scp -q -p " ^ String.concat " " (List.map Filename.quote localfiles) ^ 
       " " ^ Filename.quote remotedest)
  else
    ("scp -q -p " ^ String.concat " " (List.map Filename.quote localfiles) ^ 
       " " ^ Filename.quote host ^ ":" ^ Filename.quote remotedest)



let scp_taskfile_manager mc jc =
  let local_dir =
    mc#mr_task_tmpdir ^ "/" ^ jc#job_id ^ "_" ^ "local" in
  let log_dir =
    mc#mr_task_tmpdir ^ "/" ^ jc#job_id ^ "_" ^ "log" in
  ( object
      method local_directory = local_dir
      method log_directory = log_dir

      method copy_to_local_directory files =
	List.iter
	  (fun f ->
	     if not (Sys.file_exists f) then
	       failwith ("File does not exist: " ^ f)
	  )
	  files;
	List.iter
	  (fun node ->
	     (* Ensure the local directory exists *)
	     let cmd = 
	       ssh_cmd node
		 (sprintf "mkdir -p %s"
		    (Filename.quote local_dir)) in
	     let code = Sys.command cmd in
	     if code <> 0 then
	       failwith ("Exec failed: " ^ cmd);
	     (* Copy the files to the node: *)
	     let files' =
	       List.map
		 (fun f -> if f <> "" && f.[0] <> '/' then "./" ^ f else f)
		 files in
	     let cmd = scp_to_cmd files' node (local_dir ^ "/")in
	     let code = Sys.command cmd in
	     if code <> 0 then
	       failwith ("Exec failed: " ^ cmd);
	  )
	  mc#mr_task_nodes

      method delete_local_directory () =
	assert (local_dir <> "/");
	List.iter
	  (fun node ->
	     let cmd = 
	       ssh_cmd node
		 (sprintf "rm -rf %s"
		    (Filename.quote local_dir)) in
	     let code = Sys.command cmd in
	     if code <> 0 then
	       failwith ("Exec failed: " ^ cmd);
	  )
	  mc#mr_task_nodes

      method task_save_log_directory fs files =
	let hostname = Unix.gethostname() in
	let abs_files =
	  List.map
	    (fun f ->
	       if f <> "" && f.[0] = '/' then (f,f) else
		 (f, log_dir ^ "/" ^ f)
	    )
	    files in
	List.iter
	  (fun (_,f) ->
	     if not (Sys.file_exists f) then
	       failwith ("File does not exist: " ^ f)
	  )
	  abs_files;
	(* First ensure that the PlasmaFS log directory exists *)
	( try
	    Mapred_io.create_dir fs jc#log_dir
	  with
	    | Plasma_client.Plasma_error `eexist -> ()
	    | Plasma_client.Plasma_error `eaccess -> ()
	    | Unix.Unix_error(Unix.EEXIST,_,_) -> ()
	    | Unix.Unix_error(Unix.EACCES,_,_) -> ()
	);
	(* Copy file by file *)
	List.iter
	  (fun (f,abs) ->
	     let p_name = jc#log_dir ^ "/" ^ hostname ^ "_" ^ f in
	     let st = Unix.LargeFile.stat abs in
	     if st.Unix.LargeFile.st_kind = Unix.S_REG then (
	       fs # write_file
		 [`Create; `Truncate; `Binary]
		 p_name
		 ( object
		     method filename = abs
		     method close() = ()
		   end
		 )
	     )
	  )
	  abs_files;
	(* Finally delete files *)
	List.iter
	  (fun (_,f) ->
	     Sys.remove f
	  )
	  abs_files;
	
      method task_files_log_directory () =
	let files =
	  try
	    Array.to_list (Sys.readdir log_dir)
	  with Sys_error _ -> [] in
	List.filter
	  (fun name ->
	     let st = Unix.stat (log_dir ^ "/" ^ name) in
	     st.Unix.st_kind = Unix.S_REG
	  )
	  files

      method task_delete_log_directory () =
	let cmd =
	  sprintf "rm -rf %s" (Filename.quote log_dir) in
	let code = Sys.command cmd in
	if code <> 0 then
	  failwith ("Exec failed: " ^ cmd)	

    end
  )
