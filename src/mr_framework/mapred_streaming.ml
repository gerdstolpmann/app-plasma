(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf
open Uq_engines.Operators

let get_env (me:Mapred_def.mapred_env) tm (jc:Mapred_def.mapred_job_config) 
            (ti:Mapred_def.task_info) bigblock_size =
  let env = Shell_sys.current_env() in
  Shell_sys.set_env_var env "PLASMAMR_LOCAL_DIR" tm#local_directory;
  Shell_sys.set_env_var env "PLASMAMR_LOCAL_LOG_DIR" tm#log_directory;
  Shell_sys.set_env_var env "PLASMAMR_TASK_PREFIX" ti#task_prefix;
  Shell_sys.set_env_var env "PLASMAMR_REQ_ID" (string_of_int ti#req_id);
  Shell_sys.set_env_var env "PLASMAMR_NAME" jc#name;
  Shell_sys.set_env_var env "PLASMAMR_JOB_ID" jc#job_id;
  Shell_sys.set_env_var env "PLASMAMR_INPUT_DIR" jc#input_dir;
  Shell_sys.set_env_var env "PLASMAMR_OUTPUT_DIR" jc#output_dir;
  Shell_sys.set_env_var env "PLASMAMR_WORK_DIR" jc#work_dir;
  Shell_sys.set_env_var env "PLASMAMR_LOG_DIR" jc#log_dir;
  Shell_sys.set_env_var env "PLASMAMR_BIGBLOCK_SIZE" 
    (string_of_int bigblock_size);
  Shell_sys.set_env_var env "PLASMAMR_PARTITIONS" (string_of_int jc#partitions);
  Shell_sys.set_env_var env "PLASMAMR_CONF" me#config_file#filename;
  Shell_sys.set_env_var env "PLASMAFS_CLUSTER" me#config#nn_clustername;
  Shell_sys.set_env_var env "PLASMAFS_NAMENODES" 
    (String.concat "," me#config#nn_nodes);
  Shell_sys.set_env_var env "PLASMAFS_AUTH_TICKET" me#auth_ticket;
  env


let exec env local_dir cmd 
         (r:Mapred_io.record_reader) (w:Mapred_io.record_writer) esys =
  ( try
      Unix.mkdir local_dir 0o777
    with Unix.Unix_error(Unix.EEXIST,_,_) -> ()
  );

  let (r_fd_child, r_fd_parent) = Unix.pipe() in
  let (w_fd_parent, w_fd_child) = Unix.pipe() in

  Unix.set_nonblock r_fd_parent;
  Unix.set_nonblock w_fd_parent;

  let call_e = 
    new Shell_uq.call_engine
      ~stdin:(Shell.from_fd r_fd_child)
      ~stdout:(Shell.to_fd w_fd_child)
      [ Shell.cmd
	  ~chdir:local_dir
	  ~environment:env
	  "/bin/sh"
	  [ "-c"; cmd ]
      ]
      esys in
  Unix.close r_fd_child;
  Unix.close w_fd_child;

  let rd_e = 
    r # to_fd_e r_fd_parent esys
    ++ (fun () -> Unix.close r_fd_parent; eps_e (`Done()) esys) in
  let wr_e = w # from_fd_e w_fd_parent esys in
  
  let drop_result _ = eps_e (`Done ()) esys in

  let join_e() =
    Uq_engines.msync_engine 
      [ call_e ++ drop_result; rd_e; wr_e ]
      (fun () () -> ())
      ()
      esys in
  
  Plasma_client.sync join_e ();

  Unix.close w_fd_parent;
  w # flush()


let get_map_exec jc = 
  try jc # custom "map_exec"
  with Not_found ->
    failwith "map: parameter 'map_exec' is not defined"

let get_extract_mode jc =
  let m =
    try jc # custom "extract_mode"
    with Not_found -> "key" in
  match m with
    | "key" -> `Key
    | "key_tab_value" -> `Key_tab_value
    | "key_tab_partition_tab_value" -> `Key_tab_partition_tab_value
    | _ -> failwith "bad value in parameter 'extract_mode'"

let get_reduce_exec jc = 
  try jc # custom "reduce_exec"
  with Not_found ->
    failwith "reduce: parameter 'reduce_exec' is not defined"


let map me jc ti r w =
  let map_exec = get_map_exec jc in
  let tm = Mapred_taskfiles.scp_taskfile_manager me#config jc in
  let fs = me#filesystem in
  let bigblock_size = 
    Mapred_io.bigblock_size fs jc#input_dir jc#bigblock_size in
  let env = get_env me tm jc ti bigblock_size in
  let esys = Unixqueue.create_unix_event_system() in
  exec env tm#local_directory map_exec r w esys


let extract_key me jc = 
  let extract_mode = get_extract_mode jc in
  match extract_mode with
    | `Key -> (fun line -> (0, String.length line))
    | `Key_tab_value -> Mapred_split.tab_split_key_range
    | `Key_tab_partition_tab_value -> 
	Mapred_split.tab2_split_key_partition_range


let partition_of_key me jc =
  let extract_mode = get_extract_mode jc in
  let n = jc#partitions in
  match extract_mode with
    | `Key -> 
	(fun s p l -> (Hashtbl.hash (String.sub s p l)) mod n)
    | `Key_tab_value -> 
	(fun s p l -> 
	   let key = String.sub s p l in
	   (Hashtbl.hash key) mod n
	)
    | `Key_tab_partition_tab_value -> 
	(fun s p l ->
	   Mapred_split.tab2_get_partition (String.sub s p l)
	)


let reduce me jc ti r w =
  let reduce_exec = get_reduce_exec jc in
  let tm = Mapred_taskfiles.scp_taskfile_manager me#config jc in
  let fs = me#filesystem in
  let bigblock_size = 
    Mapred_io.bigblock_size fs jc#input_dir jc#bigblock_size in
  let env = get_env me tm jc ti bigblock_size in
  Shell_sys.set_env_var env "PLASMAMR_PARTITION" (string_of_int ti#partition);
  let esys = Unixqueue.create_unix_event_system() in
  exec env tm#local_directory reduce_exec r w esys


let job() =
  ( object 
      method custom_params = [ "map_exec"; "reduce_exec"; "extract_mode" ]
      method check_config me jc =
	ignore(get_map_exec jc);
	ignore(get_reduce_exec jc);
	ignore(get_extract_mode jc)
      method input_record_io me jc =
	Mapred_io.line_structured_format()
	  (* Note that auto_input_format would not work here, because only
	     line_structured_format supports the engine-based read methods
	   *)
      method internal_record_io me jc =
	Mapred_io.line_structured_format()
      method output_record_io me jc =
	Mapred_io.line_structured_format()
      method pre_job_start me jc = ()
      method post_job_finish me jc = ()
      method map = map
      method extract_key = extract_key
      method partition_of_key = partition_of_key
      method reduce = reduce
      method sorter = 
	Mapred_sorters.generic_sorter
	  ~hash:Mapred_sorters.String_asc.hash
	  ~cmp:Mapred_sorters.String_asc.cmp
      method combine _ _ _ = None
    end
  )
