(* $Id$ *)

let text_field_range ?(pos=0) ?len ?(sep='\t') ?(field=1) ?(num=1) s =
  let len =
    match len with
      | None -> String.length s - pos
      | Some n -> n in
  if pos < 0 || len < 0 || pos > String.length s - len || field < 1 ||
     num < 1
  then
    invalid_arg "Mapred_fields.text_field_range";
  let p_end = pos + len in
  let rec cut fld p0 p =
    if p < p_end then (
      let c = String.unsafe_get s p in
      if c = sep then (
	let fld' = fld+1 in
	let p' = p+1 in
	if fld' = field then
	  cut fld' p' p'
	else
	  if fld' = field+num then
	    (p0, p-p0)
	  else
	    cut fld' p0 p'
      )
      else
	cut fld p0 (p+1)
    )
    else (
      if fld = field+num-1 then
	(p0, p-p0)
      else
	failwith "Mapred_fields.text_field_range: no such field(s)"
    ) in
  cut 1 pos pos


let text_field ?pos ?len ?sep ?field ?num s =
  let (p,l) = text_field_range ?pos ?len ?sep ?field ?num s in
  String.sub s p l


let text_fields_range ?(pos=0) ?len ?(sep='\t') ?(field=1) ?(num=1) s =
  let len =
    match len with
      | None -> String.length s - pos
      | Some n -> n in
  if pos < 0 || len < 0 || pos > String.length s - len || field < 1 ||
     num < 0
  then
    invalid_arg "Mapred_fields.text_field_range";
  let p_end = pos + len in
  let rec cut fld p0 p =
    if p < p_end then (
      let c = String.unsafe_get s p in
      if c = sep then (
	let fld' = fld+1 in
	let p' = p+1 in
	if fld >= field then
	  if fld' = field+num then
	    [p0, p-p0]
	  else
	    (p0, p-p0) :: cut fld' p' p'
	else
	  cut fld' p' p'
      )
      else
	cut fld p0 (p+1)
    )
    else (
      if fld = field+num-1 then
	[p0, p-p0]
      else
	failwith "Mapred_fields.text_fields_range: no such field(s)"
    ) in
  if num = 0 then
    []
  else
    cut 1 pos pos


let text_fields ?pos ?len ?sep ?field ?num s =
  List.map
    (fun (p,l) -> String.sub s p l)
    (text_fields_range ?pos ?len ?sep ?field ?num s)


let var_concat l =
  let n = 
    List.fold_left 
      (fun acc s -> 
	 let l = String.length s in
	 if l < 255 then acc+l+1 else acc+l+9
      )
      0 l in
  let u = String.create n in
  let k = ref 0 in
  List.iter
    (fun s ->
       let l = String.length s in
       if l < 255 then (
	 u.[ !k ] <- Char.chr l;
	 String.blit s 0 u (!k+1) l;
	 k := !k + l + 1
       )
       else (
	 u.[ !k ] <- '\255';
	 Netnumber.BE.write_int8 u (!k+1) (Netnumber.int8_of_int l);
	 String.blit s 0 u (!k+9) l;
	 k := !k + l + 9
       )
    )
    l;
  u

  
let var_field_range ?(pos=0) ?len ?(field=1) s =
  let len =
    match len with
      | None -> String.length s - pos
      | Some n -> n in
  if pos < 0 || len < 0 || pos > String.length s - len || field < 1 then
    invalid_arg "Mapred_fields.var_field_range";
  let p_end = pos + len in

  let rec cut fld p =
    let n, q =
      if p < p_end then
	let c = s.[p] in
	if c = '\255' then
	  if p+9 <= p_end then
	    (Netnumber.int_of_int8(Netnumber.BE.read_int8 s (p+1)), (p+9))
	  else
	    raise Not_found
	else
	  (Char.code c, p+1)
      else
	raise Not_found in
    if fld < field then
      cut (fld+1) (q+n)
    else
      ( q, n ) in

  try
    cut 1 pos
  with
    | _ ->
	failwith "Mapred_fields.var_field_range: no such field"


let var_field ?pos ?len ?field s =
  let (p,l) =
    var_field_range ?pos ?len ?field s in
  String.sub s p l
  

let var_fields_range ?(pos=0) ?len ?(field=1) ?(num=1) s =
  let len =
    match len with
      | None -> String.length s - pos
      | Some n -> n in
  if pos < 0 || len < 0 || pos > String.length s - len || field < 1 || num < 0
  then
    invalid_arg "Mapred_fields.var_fields_range";
  let p_end = pos + len in

  let rec cut fld p =
    let n, q =
      if p < p_end then
	let c = s.[p] in
	if c = '\255' then
	  if p+9 <= p_end then
	    (Netnumber.int_of_int8(Netnumber.BE.read_int8 s (p+1)), (p+9))
	  else
	    raise Not_found
	else
	  (Char.code c, p+1)
      else
	raise Not_found in
    if fld < field then
      cut (fld+1) (q+n)
    else
      if fld < field+num-1 then
	( q, n ) :: cut (fld+1) (q+n)
      else
	[ q, n ] in

  try
    if num = 0 then [] else
      cut 1 pos
  with
    | _ ->
	failwith "Mapred_fields.var_fields_range: no such field"


let var_fields ?pos ?len ?field ?num s =
  List.map
    (fun (p,l) -> String.sub s p l)
    (var_fields_range ?pos ?len ?field ?num s)


let escape s = String.escaped s

let int_of_hex c1 c2 =
  try
    int_of_string ("0x" ^ String.make 1 c1  ^ String.make 1 c2)
  with
    | _ ->
	failwith "Mapred_fields.unescape: bad string"


let int_of_dec c1 c2 c3 =
  try
    int_of_string
      ("0" ^ String.make 1 c1  ^ String.make 1 c2 ^ String.make 1 c3)
  with
    | _ ->
	failwith "Mapred_fields.unescape: bad string"


let unescape s =
  let l = String.length s in
  let b = Buffer.create l in
  
  let rec loop p =
    if p < l then (
      let c = String.unsafe_get s p in
      if c = '\\' then (
	if p+1 < l then (
	  let c = s.[p+1] in
	  match c with
	    | 'n' ->
		Buffer.add_char b '\n';
		loop (p+2)
	    | 't' ->
		Buffer.add_char b '\t';
		loop (p+2)
	    | 'b' ->
		Buffer.add_char b '\b';
		loop (p+2)
	    | 'r' ->
		Buffer.add_char b '\r';
		loop (p+2)
	    | '"' ->
		Buffer.add_char b '"';
		loop (p+2)
	    | '\\' ->
		Buffer.add_char b '\\';
		loop (p+2)
	    | 'x' ->
		if p+3 < l then (
		  let c1 = s.[p+2] in
		  let c2 = s.[p+3] in
		  let x = int_of_hex c1 c2 in
		  Buffer.add_char b (Char.chr x);
		  loop (p+4)
		)
		else
		  failwith "Mapred_fields.unescape: bad string"
	    | '0'..'9' ->
		if p+3 < l then (
		  let c1 = s.[p+2] in
		  let c2 = s.[p+3] in
		  let c3 = s.[p+4] in
		  let x = int_of_dec c1 c2 c3 in
		  Buffer.add_char b (Char.chr x);
		  loop (p+5)
		)
		else
		  failwith "Mapred_fields.unescape: bad string"
	    | _ ->
		failwith "Mapred_fields.unescape: bad string"
	)
	else
	  failwith "Mapred_fields.unescape: bad string"
      )
      else (
	Buffer.add_char b c;
	loop (p+1)
      )
    ) in
  loop 0;
  Buffer.contents b


let alphabet =
  [| '0'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'; '9'; '='; '@';
     'A'; 'B'; 'C'; 'D'; 'E'; 'F'; 'G'; 'H'; 'I'; 'J'; 'K'; 'L'; 'M';
     'N'; 'O'; 'P'; 'Q'; 'R'; 'S'; 'T'; 'U'; 'V'; 'W'; 'X'; 'Y'; 'Z';
     'a'; 'b'; 'c'; 'd'; 'e'; 'f'; 'g'; 'h'; 'i'; 'j'; 'k'; 'l'; 'm';
     'n'; 'o'; 'p'; 'q'; 'r'; 's'; 't'; 'u'; 'v'; 'w'; 'x'; 'y'; 'z' |]

let pad = '-' ;;


let encode_alt64 s =
  let l = String.length s in
  let n = if l=0 then 0 else ((l-1)/3)*4 + 4 in
  let u = String.create n in

  (* ######--
     ----++++
     ++======
   *)

  for k = 0 to l/3-1 do
    let p = k+k+k in
    let x2 = Char.code (String.unsafe_get s p) in
    let x1 = Char.code (String.unsafe_get s (p+1)) in
    let x0 = Char.code (String.unsafe_get s (p+2)) in
    let y3 = x2 lsr 2 in
    let y2 = ((x2 land 3) lsl 4) lor (x1 lsr 4) in
    let y1 = ((x1 land 15) lsl 2) lor (x0 lsr 6) in
    let y0 = x0 land 63 in
    let q = p+k in
    String.unsafe_set u q alphabet.(y3);
    String.unsafe_set u (q+1) alphabet.(y2);
    String.unsafe_set u (q+2) alphabet.(y1);
    String.unsafe_set u (q+3) alphabet.(y0);
  done;

  ( match l mod 3 with
      | 0 -> ()
      | 1 ->
	  let p = (l/3) * 3 in
	  let q = (l/3) * 4 in
	  let x0 = Char.code (String.unsafe_get s p) in
	  let y3 = x0 lsr 2 in
	  let y2 = (x0 land 3) lsl 4 in
	  String.unsafe_set u q alphabet.(y3);
	  String.unsafe_set u (q+1) alphabet.(y2);
	  String.unsafe_set u (q+2) pad;
	  String.unsafe_set u (q+3) pad;
      | 2 ->
	  let p = (l/3) * 3 in
	  let q = (l/3) * 4 in
	  let x1 = Char.code (String.unsafe_get s p) in
	  let x0 = Char.code (String.unsafe_get s (p+1)) in
	  let y3 = x1 lsr 2 in
	  let y2 = ((x1 land 3) lsl 4) lor (x0 lsr 4) in
	  let y1 = ((x0 land 15) lsl 2) in
	  String.unsafe_set u q alphabet.(y3);
	  String.unsafe_set u (q+1) alphabet.(y2);
	  String.unsafe_set u (q+2) alphabet.(y1);
	  String.unsafe_set u (q+3) pad;
      | _ ->
	  assert false
  );
  u


let ralphabet = Array.make 256 (-1)

let () =
  for k = 0 to 63 do
    let c = alphabet.(k) in
    ralphabet.(Char.code c) <- k
  done


let decode_alt64 s =
  let l = String.length s in
  if l mod 4 <> 0 then
    failwith "Mapred_fields.decode_alt64: bad string";
  let offs =
    if l > 0 && s.[l-1] = pad then
      if l > 1 && s.[l-2] = pad then
	(-2)
      else
	(-1)
    else
      0 in
  let t = if offs = 0 then 0 else (-1) in

  let n = (l/4)*3 + offs in
  let u = String.create n in

  (* ######--
     ----++++
     ++======
   *)

  let code c =
    let x = ralphabet.(Char.code c) in
    if x >= 0 then x else
      failwith "Mapred_fields.decode_alt64: bad string" in

  for k = 0 to l/4 - 1 + t do
    let p = k+k+k+k in
    let x3 = code (String.unsafe_get s p) in
    let x2 = code (String.unsafe_get s (p+1)) in
    let x1 = code (String.unsafe_get s (p+2)) in
    let x0 = code (String.unsafe_get s (p+3)) in
    let y2 = (x3 lsl 2) lor (x2 lsr 4) in
    let y1 = ((x2 land 15) lsl 4) lor (x1 lsr 2) in
    let y0 = ((x1 land 3) lsl 6) lor x0 in
    let q = p-k in
    String.unsafe_set u q (Char.chr y2);
    String.unsafe_set u (q+1)(Char.chr y1);
    String.unsafe_set u (q+2) (Char.chr y0);
  done;

  if offs<>0 then (
    let p = l-4 in
    let x3 = code (String.unsafe_get s p) in
    let x2 = code (String.unsafe_get s (p+1)) in
    let c1 = String.unsafe_get s (p+2) in
    let x1 = if c1=pad then 0 else code c1 in
    let y2 = (x3 lsl 2) lor (x2 lsr 4) in
    let y1 = ((x2 land 15) lsl 4) lor (x1 lsr 2) in
    let q = (l/4-1)*3 in
    String.unsafe_set u q (Char.chr y2);
    if offs >= (-1) then
      String.unsafe_set u (q+1)(Char.chr y1);
  );
  u

