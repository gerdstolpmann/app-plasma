(* $Id$ *)

(* Word frequencies using the toolkit *)

open Mapred_toolkit

let str = string_of_int;;

let code_input =
  <:rfun< @ (k,v) -> k ^ "\t" ^ v >> ;;

let decode_input =
  <:rfun< @ s p l ->
             match Mapred_fields.text_fields ~pos:p ~len:l ~num:2 s with
	       | [ k; v ] -> (k, v)
	       | _ -> assert false
  >> ;;

let code_internal =
  <:rfun< @ (word,freq) ->
               Mapred_fields.var_concat [word; string_of_int freq]
  >> ;;

let decode_internal =
  <:rfun< @ s p l ->
     match Mapred_fields.var_fields ~pos:p ~len:l ~num:2 s with
       | [ word; freq_s ] ->
	   (word, int_of_string freq_s)
       | _ ->
	   assert false
  >> ;;

let code_output =
  <:rfun< @ (word,freq) -> word ^ "\t" ^ string_of_int freq
  >> ;;


let decode_output =
  <:rfun< @ s p l -> assert false >> ;;

let mapl =
  <:rfun< @ mi (k,v) -> [k, 1] >> ;;

let hash =
  <:rfun< @ mi (k,v) -> 0 >> ;;

let cmp =
  <:rfun< @ mi (k1,_) (k2,_) -> String.compare k1 k2 >> ;;

let initfold =
  <:rfun< @ mi _ -> None >> ;;

let initcombine =
  <:rfun< @ mi -> None >> ;;

let fold =
  <:rfun< @ mi st (k,m) ->
            match st with
              | Some(j,n) -> 
		  if j=k then (Some(j,n+m),[]) else (Some(k,m), [j, n])
              | None ->
		  (Some(k,m), [])
  >> ;;

let finfold =
  <:rfun< @ mi st ->
	    match st with
              | Some(j,n) -> [j, n]
              | None -> []
  >> ;;

let partition_of =
  <:rfun< @ mi ->
    let partitions = mi#mapred_job_config#partitions in
    fun k ->
      (Hashtbl.hash k) mod partitions
  >> ;;


let run_job me mj =
  let config =
    DSeq.create_config ~report:true ~keep_temp_files:true me in

  let codec_input = (code_input,decode_input) in
  let fs = me # filesystem in
  let rc = DSeq.get_rc config in
  let p2 = Place.from fs codec_input `Line_structured rc (`Flat_dir "/d2") in

  let code_output = (code_output,decode_output) in
  if Mapred_io.is_directory fs "/d3" then
    Mapred_io.delete_rec fs "/d3";
  let p3 = Place.create fs code_output `Line_structured rc (`Flat_dir "/d3") in

  let res =
if true then   
    DSeq.mapl_sort_fold
      ~mapl ~hash ~cmp ~initfold ~fold ~finfold ~partition_of
      ~initcombine ~combine:fold ~fincombine:finfold
      p2 p3
      config
      (code_internal, decode_internal)
else
    DSeq.mapl
      mapl p2 p3 config in
  ignore(DSeq.get_result res)
;;


Mapred_main.dispatch
  ["wf", "Count word frequencies", Mapred_toolkit.toolkit_job, run_job ]


