(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

type access =
    { users : (string * string * string * string * int) list;
    }

type client_auth =
    { pnobody_name : string;
      pnobody_pw : string;
      auth_ticket : string;
    }


let scram_ic = 4096

let extract_access_config cf addr =
  match cf#resolve_section addr "access" with
    | [] ->
	{ users = []
	}
    | [access] ->
	let user_sects =
	  cf#resolve_section access "user" in
	let users =
	  List.map
	    (fun user ->
	       let name =
		 try cf#string_param (cf#resolve_parameter user "name")
		 with Not_found ->
		   failwith ("Missing parameter: " ^ cf#print user ^ ".name") in
	       let pw =
		 try cf#string_param (cf#resolve_parameter user "password")
		 with Not_found ->
		   ( let file =
		       try 
			 cf#string_param 
			   (cf#resolve_parameter user "password_file")
		       with Not_found ->
			  failwith ("Missing parameter: " ^ cf#print user ^ 
				      ".password (or .password_file)") in
		     let file =
		       if Filename.is_implicit file then
			 Filename.dirname cf#filename ^ "/" ^ file
		       else
			 file in
		     try
		       let f = open_in file in
		       let pw = input_line f in
		       close_in f;
		       pw
		     with
		       | error ->
			   failwith ("Error reading file " ^ file ^ ": " ^ 
				       Netexn.to_string error)
		   ) in
	       let salt = Netmech_scram.create_salt() in
	       let spw = Netmech_scram.salt_password pw salt scram_ic in
	       (name, pw, spw, salt, scram_ic)
	    )
	    user_sects
	in
	{ users = users
	}
    | _ ->
	failwith ("Mapred_auth.extract_access_config: several sections " ^ 
		    cf#print addr ^ ".access")


let get_pw acc user =
   try
     let (_,pw,_,_,_) =
       List.find
	 (fun (u,_,_,_,_) -> u=user)
	 acc.users in
     pw
   with Not_found -> ""


let configure_rpc_server rpc acc =
  let server_key_verifier =
    ( object
        method scram_credentials username = (* may raise Not_found *)
	  try
	    let user =
	      List.find
		(fun (u,_,_,_,_) -> u=username)
		acc.users in
	    let (_,_,pw,salt,ic) = user in
	    (pw,salt,ic)
	  with
	    | Not_found ->
		Netlog.logf `Warning
		  "No password set for user: %s" username;
		raise Not_found
      end
    ) in
  let gss_api =
    new Netmech_scram_gssapi.scram_gss_api
      ~server_key_verifier
      (Netmech_scram.profile ~return_unknown_user:true `GSSAPI) in
  let m1 =
    Rpc_auth_gssapi.server_auth_method
      ~require_privacy:true
      ~user_name_format:`Plain_name
      gss_api
      Netmech_scram_gssapi.scram_mech in
  let null_proc =
    Rtypes.uint4_of_int 0 in
  let m2 = (* permit procedure 0 with AUTH_NONE *)
    ( object
	method name = Rpc_server.auth_none#name
	method flavors = Rpc_server.auth_none#flavors
	method peek = Rpc_server.auth_none#peek
	method authenticate srv conn_id details f =
	  if details#procedure = null_proc then
	    Rpc_server.auth_none#authenticate srv conn_id details f
	  else
	    f (Rpc_server.Auth_negative(Rpc.Auth_too_weak))
      end
    ) in
  let ml = [m1; m2] in
  if not(Rpc_server.is_dummy rpc) then
    Rpc_server.set_auth_methods rpc ml


let rpc_proxy_auth_methods auth_ticket =
  if auth_ticket = "unauthenticated" then
    [Rpc_client.auth_none]
  else
    let (rpc_user,rpc_pw) = Plasma_ug.rpc_login_of_auth_ticket auth_ticket in
    let client_key_ring =
      ( object
          method password_of_user_name n =
	    if n = rpc_user then
	      rpc_pw
	    else raise Not_found
          method default_user_name =
            Some rpc_user
	end
      ) in
    let gss_api =
      new Netmech_scram_gssapi.scram_gss_api
	~client_key_ring
	(Netmech_scram.profile `GSSAPI) in
    let m =
      Rpc_auth_gssapi.client_auth_method
	~privacy:`Required
	~user_name_interpretation:(`Plain_name Netgssapi.nt_user_name)
	gss_api
	Netmech_scram_gssapi.scram_mech in
    [m]

let get_client_auth c =
  let (user, _, _) = Plasma_client.current_user c in
  let auth_ticket = Plasma_client.get_auth_ticket c user in
  auth_ticket
