(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Sorter classes *)

open Mapred_def

val null_sorter : mapred_env -> mapred_job_config -> float -> sorter
  (** This "sorter" just copies the input unmodified to the output *)

val generic_sorter : 
      hash:(string -> int -> int -> int) ->
      cmp:(string -> int -> int -> string -> int -> int -> int) ->
      mapred_env -> mapred_job_config -> float -> sorter
  (** The sorter orders the records first by the hash of the key. If two
      hashes are equal, the keys are directly compared.

      The functions [hash], and [cmp] are defined as for
      {!Mapred_def.sorter}.
   *)

  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records with the same key are
      grouped together.
   *)
module Grouped : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      ascending way by key. The key is interpreted as string, and sorted
      as [String.compare] does.
   *)
module String_asc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      descending way by key. The key is interpreted as string, and sorted
      as [String.compare] does.
   *)
module String_desc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      ascending way by key. The key is interpreted as integer (digits
      0 to 9, possibly prefixed by [+] or [-]). Characters violating the
      integer syntax are silently ignored. (Note that the integer range
      is not restricted!)
   *)
module Integer_asc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      descending way by key. The key is interpreted as integer (digits
      0 to 9, possibly prefixed by [+] or [-]). Characters violating the
      integer syntax are silently ignored. (Note that the integer range
      is not restricted!)
   *)
module Integer_desc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      ascending way by key. The key is interpreted as hex integer (digits
      0 to 9, a to f, A to F, possibly prefixed by [+] or [-]). 
      Characters violating the
      hex syntax are silently ignored. (Note that the integer range
      is not restricted!)
   *)
module Hex_asc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      descending way by key. The key is interpreted as hex integer (digits
      0 to 9, a to f, A to F, possibly prefixed by [+] or [-]). 
      Characters violating the
      hex syntax are silently ignored. (Note that the integer range
      is not restricted!)
   *)
module Hex_desc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      ascending way by key. The key is interpreted as decimal representation
      of a float. Unparsable keys are treated as Nan.
   *)
module Float_asc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      descending way by key. The key is interpreted as decimal representation
      of a float. Unparsable keys are treated as Nan.
   *)
module Float_desc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      ascending way by key. The key is interpreted as binary representation
      of an int64 in big endian byte order.
   *)
module Int64_xdr_asc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      descending way by key. The key is interpreted as binary representation
      of an int64 in big endian byte order.
   *)
module Int64_xdr_desc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      ascending way by key. The key is interpreted as binary representation
      of a float following the (big-endian) XDR rules.
   *)
module Float_xdr_asc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


  (** Exports a [hash] and [cmp] function for {!Mapred_sorters.generic_sorter}.
      The functions are defined so that the records are sorted in an
      descending way by key. The key is interpreted as binary representation
      of a float following the (big-endian) XDR rules.
   *)
module Float_xdr_desc : sig
  val hash : string -> int -> int -> int
  val cmp : string -> int -> int -> string -> int -> int -> int
end


