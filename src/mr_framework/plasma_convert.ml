(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let convert ?repl fs bigblock 
            from_factory to_factory_opt from_file to_file print = 
  let rc =
    ( object
	method bigblock_size = bigblock
	method mr_buffer_size = bigblock
	method mr_buffer_size_tight = bigblock
      end
    ) in
  let shm_mng = new Plasma_shm.null_shm_manager() in
  let q = Queue.create() in
  let count = ref 0 in
  let len = Mapred_io.file_blocks fs from_file in
  if to_factory_opt <> None then
    Mapred_io.create_file ?repl fs to_file;
  if len > 0L then (
    let rd = from_factory # read_file [] fs shm_mng rc from_file 0L len in
    let wr_opt =
      match to_factory_opt with
	| None -> None
	| Some to_factory ->
	    Some(to_factory # write_file fs shm_mng rc to_file) in
    try
      while true do
	rd # input_records q;  (* or End_of_file *)
	if print then
	  Queue.iter print_endline q;
	count := !count + Queue.length q;
	match wr_opt with
	  | None -> Queue.clear q
	  | Some wr -> wr # output_records q
      done
    with
      | End_of_file ->
	  rd # close_in();
	  ( match wr_opt with
	      | None -> ()
	      | Some wr -> wr # close_out()
	  );
	  eprintf "Record count: %d\n" !count;
  )


let factory name =
  match name with
    | "text" -> Mapred_io.line_structured_format()
    | "var" -> Mapred_io.var_size_format()
    | _ ->
	( try
	    let p = String.index name ':' in
	    let prefix = String.sub name 0 p in
	    let suffix = String.sub name (p+1) (String.length name - p - 1) in
	    match prefix with
	      | "fixed" ->
		  let n =
		    try int_of_string suffix
		    with _ -> raise Not_found in
		  if n <= 0 then raise Not_found;
		  Mapred_io.fixed_size_format n
	      | _ -> raise Not_found
	  with
	    | Not_found ->
		failwith ("Invalid format name: " ^ name)
	)


let main() =
  let cluster = ref None in
  let namenodes = ref [] in
  let disable_cluster = ref false in
  let in_format = ref None in
  let out_format = ref None in
  let bigblock = ref (64 * 1024 * 1024) in
  let repl = ref 0 in
  let in_file = ref None in
  let out_file = ref None in
  let print = ref false in
  let debug = ref false in
  Arg.parse
    [ "-cluster", Arg.String (fun s -> cluster := Some s),
      "<name>    Set the cluster name";
      
      "-namenode", Arg.String (fun n -> namenodes := n :: !namenodes),
      "<host>:<port>   Also use this namenode (can be given several times)";

      "-disable-cluster", Arg.Set disable_cluster,
      "     Disable PlasmaFS entirely";

      "-in-format", Arg.String (fun s -> in_format := Some(factory s)),
      "<name>  Set the format of the input ('text', 'var', 'fixed:<N>')";

      "-out-format", Arg.String (fun s -> out_format := Some(factory s)),
      "<name>  Set the format of the output ('text', 'var', 'fixed:<N>')";

      "-bigblock", Arg.Set_int bigblock,
      "<n>   Set the bigblock size (max record size; default: 64M)";

      "-repl", Arg.Set_int repl,
      "<n>   Set the replication factor of the output explicitly";

      "-print", Arg.Set print,
      "    Print the records to stdout (with LF as record terminator)";

      "-debug", Arg.Set debug,
      "    Enable debug logging";
    ]
    (fun s ->
       match (!in_file, !out_file) with
	 | None, _ ->
	     in_file := Some s
	 | (Some _), None ->
	     out_file := Some s
	 | _ ->
	     failwith "Too many arguments"
    )
    (sprintf "usage: %s [options] plasma_input_file [plasma_output_file]"
       (Filename.basename Sys.argv.(0)));

  Netlog.current_logger := 
    Netlog.channel_logger stderr (if !debug then `Debug else `Info);
  Plasma_util.debug := !debug;
  Mapred_io.more_logging := !debug;

  let client_config =
    if !disable_cluster then 
      None
    else
      Some
	(Plasma_client_config.get_config
	   ?clustername:!cluster
	   ~nn_nodes:!namenodes ()
	) in 
  let fs =
    Mapred_fs.standard_fs_cc ?client_config () in

  let from_file =
    match !in_file with
      | None ->
	  failwith "Missing argument"
     | Some n ->
	  n in

  let from_factory =
    match !in_format with
      | None ->
	  failwith "Need -in-format"
      | Some p ->
	  p in

  let to_file, to_factory_opt =
    match !out_file, !out_format with
      | None, _ -> "", None
      | (Some n), None -> failwith "Need -out-format"
      | (Some n), (Some p) -> n, (Some p) in

  convert 
    ~repl:!repl 
    fs
    !bigblock
    from_factory to_factory_opt from_file to_file
    !print


let () =
  Printexc.record_backtrace true;
  try
    main()
  with
    | error ->
	let bt = Printexc.get_backtrace () in
	eprintf "Exception: %s\n%!" (Netexn.to_string error);
	eprintf "Backtrace: %s\n%!" bt;
	exit 1
