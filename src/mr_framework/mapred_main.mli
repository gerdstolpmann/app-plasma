(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

val main : Mapred_def.mapred_job -> unit
  (** Main program for the task server and job controller, for this
      single job only
   *)

val exec_job : 
      ?options:( (string*string)list ref -> 
                    (Arg.key * Arg.spec * Arg.doc) list) ->
      Mapred_def.mapred_env -> Mapred_def.mapred_job -> unit
  (** Command-line interface for running this job.

      By passing [options] it is possible to define additional command-line
      options as for the [Arg] module of the standard library.
      [options] is called with a list of keys and values as argument.
      By extending this list, additional custom job options can be
      set (as for -override).
   *)

type spec =
       string * 
       string * 
       (Mapred_def.mapred_env -> Mapred_def.mapred_job) *
       (Mapred_def.mapred_env -> Mapred_def.mapred_job -> unit)
  (** Specification of a command for [dispatch]. A command is described
      as 4-tuple [(name, usage, get_job, run_job)]:

      - [name] is the command name (taken from the command-line)
      - [usage] is a help text
      - [get_job]: must return the job definition
      - [run_job]: starts the job

      [run_job] can parse further command-line parameters with [Arg].
   *)

val dispatch : spec list -> unit
  (** Alternative main program for dispatching between several types
      of jobs. The list includes 4-tuples:

      {[ (command_name, usage, get_job, run_job) ]}

      See [spec] for more information.

      Note that [exec_job] is a valid [run_job] function, and that
      {!Mapred_toolkit.toolkit_job} can be used for [get_job].
   *)
