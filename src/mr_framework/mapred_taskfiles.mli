(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Copy files to and from the task nodes *)

class type taskfile_manager =
object
  method local_directory : string
    (** The directory path on the task nodes for files
	that are needed by tasks
     *)

  method log_directory : string
    (** The directory path on the task nodes where the tasks can put
	files, and these files are finally copied to PlasmaFS
     *)

  method copy_to_local_directory : string list -> unit
    (** Copies the mentioned file to the task nodes, and places them into
	the [local_directory]. The [local_directory] is created if missing.
	This method can be invoked on any node.
     *)

  method delete_local_directory : unit -> unit
    (** Deletes the local directory on the task nodes.
	This method can be invoked on any node.
     *)

  method task_save_log_directory : Mapred_fs.filesystem -> string list -> unit
    (** Copies these files of the log directory into PlasmaFS. The source
	files are deleted. The PlasmaFS name is prefixed with the hostname
	(i.e. [log_dir ^ "/" hostname ^ "_" ^ filename]).
	This method must be invoked on the task node.
     *)

  method task_files_log_directory : unit -> string list
    (** Returns the files that are still in the log directory *)

  method task_delete_log_directory : unit -> unit
    (** Deletes the entire log directory.
	This method must be invoked on the task node.
     *)

end


val scp_taskfile_manager : Mapred_config.mapred_config ->
                           Mapred_def.mapred_job_config ->
                             taskfile_manager
  (** A taskfile manager that uses [scp] to copy file to remote nodes.

      The [local_directory] is set to [mr_task_tmpdir]/[job_id]_"local".

      The [log_directory] is set to [mr_task_tmpdir]/[job_id]_"log".
   *)


val ssh_cmd : string -> string -> string
  (** [ssh_cmd host cmd]: Generates a shell command that runs [cmd] on
      [host]
   *)

val scp_to_cmd : string list -> string -> string -> string
  (** [scp_to_cmd localfiles host dest]: Generates a shell command that
      copies [localfiles] to [host] into the destination [dest]
   *)
