(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type mapred_config =
object
  method nn_disabled : bool
  method nn_clustername : string
  method nn_nodes : string list
  method file_root : string
  method plasma_root : string option
  method mr_task_nodes : string list
  method mr_task_port : int
  method mr_task_tmpdir : string
  method mr_users : string list option
  method mr_shm_low : int64 option
  method mr_shm_high : int64 option
  method mr_shm_max : int64 option
  method mr_buf_low : int64 option
  method mr_buf_high : int64 option
  method mr_buf_max : int64 option
  method mr_shm_low_factor : float
  method mr_shm_high_factor : float
  method mr_shm_max_factor : float
  method mr_buf_low_factor : float
  method mr_buf_high_factor : float
  method mr_buf_max_factor : float
  method mr_buffer_size : int
  method mr_buffer_size_tight : int
  method mr_sort_size : int
  method mr_pre_partitions_max : int
end


let extract_config cf =
  let namenodes =
    cf#resolve_section cf#root_addr "namenodes" in
  let nn = 
    match namenodes with
      | [] ->
          failwith "missing 'namenodes' config"
      | _ :: _ :: _ ->
          failwith "more than one 'namenodes' config"
      | [nn] ->
	  nn in

  let nn_disabled =
    try
      cf#bool_param(cf#resolve_parameter nn "disabled")
    with Not_found -> false in
  
  let nn_addrs = 
    if nn_disabled then [] else Plasma_util.node_list cf nn in
  
  let clustername =
    try
      let p = cf#resolve_parameter nn "clustername" in
      cf#string_param p
    with
      | Not_found ->
	  if nn_disabled then
	    "<none>"
	  else
            failwith "missing 'namenodes.clustername' parameter" in

  let file_root =
    try
      let file_sect = List.hd (cf # resolve_section cf#root_addr "file") in
      let p = cf # resolve_parameter file_sect "root" in
      cf#string_param p
    with
      | _ -> "/" in

  let plasma_root =
    try
      Some(cf#string_param (cf#resolve_parameter nn "root"))
    with
      | _ -> None in
  
  let mapreds =
    cf#resolve_section cf#root_addr "mapred" in

  let mr = 
    match mapreds with
      | [] ->
          failwith "missing 'mapred' config"
      | _ :: _ :: _ ->
          failwith "more than one 'mapred' config"
      | [mr] ->
	  mr in
  
  let mr_addrs = Plasma_util.node_list cf mr in
  let mr_hosts =
    List.map
      (fun s ->
	 fst (Plasma_util.parse_host_port s)
      )
      mr_addrs in
  if mr_hosts = [] then
    failwith "missing 'mapred.node' config";

  let mr_task_port =
    try
      let p = cf#resolve_parameter mr "port" in
      cf#int_param p
    with
      | Not_found ->
          failwith "missing 'mapred.port' parameter" in

  let mr_task_tmpdir =
    try
      let p = cf#resolve_parameter mr "tmpdir" in
      cf#string_param p
    with
      | Not_found ->
          failwith "missing 'mapred.tmpdir' parameter" in
  
  let mr_users =
    try
      let p = cf#resolve_parameter mr "users" in
      Some(Pcre.split(cf#string_param p))
    with
      | Not_found ->
          None in
  
  let mr_shm_low =
    try
      let p = cf#resolve_parameter mr "shm_low" in
      try Some(Int64.of_string (cf#string_param p))
      with _ -> Some(Int64.of_int (cf#int_param p))
    with
      | Not_found ->
	  None in

  let mr_shm_low_factor =
    try
      let p = cf#resolve_parameter mr "shm_low_factor" in
      cf#float_param p
    with
      | Not_found -> 0.25 in

  let mr_shm_high =
    try
      let p = cf#resolve_parameter mr "shm_high" in
      try Some(Int64.of_string (cf#string_param p))
      with _ -> Some(Int64.of_int (cf#int_param p))
    with
      | Not_found ->
	  None in

  let mr_shm_high_factor =
    try
      let p = cf#resolve_parameter mr "shm_high_factor" in
      cf#float_param p
    with
      | Not_found -> 0.5 in

  let mr_shm_max =
    try
      let p = cf#resolve_parameter mr "shm_max" in
      try Some(Int64.of_string (cf#string_param p))
      with _ -> Some(Int64.of_int (cf#int_param p))
    with
      | Not_found ->
	  None in

  let mr_shm_max_factor =
    try
      let p = cf#resolve_parameter mr "shm_max_factor" in
      cf#float_param p
    with
      | Not_found -> 0.75 in


  let mr_buf_low =
    try
      let p = cf#resolve_parameter mr "buf_low" in
      try Some(Int64.of_string (cf#string_param p))
      with _ -> Some(Int64.of_int (cf#int_param p))
    with
      | Not_found ->
	  None in

  let mr_buf_low_factor =
    try
      let p = cf#resolve_parameter mr "buf_low_factor" in
      cf#float_param p
    with
      | Not_found -> 0.25 in

  let mr_buf_high =
    try
      let p = cf#resolve_parameter mr "buf_high" in
      try Some(Int64.of_string (cf#string_param p))
      with _ -> Some(Int64.of_int (cf#int_param p))
    with
      | Not_found ->
	  None in

  let mr_buf_high_factor =
    try
      let p = cf#resolve_parameter mr "buf_high_factor" in
      cf#float_param p
    with
      | Not_found -> 0.5 in

  let mr_buf_max =
    try
      let p = cf#resolve_parameter mr "buf_max" in
      try Some(Int64.of_string (cf#string_param p))
      with _ -> Some(Int64.of_int (cf#int_param p))
    with
      | Not_found ->
          None in

  let mr_buf_max_factor =
    try
      let p = cf#resolve_parameter mr "buf_max_factor" in
      cf#float_param p
    with
      | Not_found -> 0.75 in

  let mr_buffer_size =
    try
      let p = cf#resolve_parameter mr "buffer_size" in
      cf#int_param p
    with
      | Not_found ->
          failwith "missing 'mapred.buffer_size' parameter" in

  let mr_buffer_size_tight =
    try
      let p = cf#resolve_parameter mr "buffer_size_tight" in
      cf#int_param p
    with
      | Not_found ->
          failwith "missing 'mapred.buffer_size_tight' parameter" in

  let mr_sort_size0 =
    try
      let p = cf#resolve_parameter mr "sort_size" in
      cf#int_param p
    with
      | Not_found ->
          failwith "missing 'mapred.sort_size' parameter" in
  let mr_sort_size =
    if mr_sort_size0 > Sys.max_string_length then (
      let n = Sys.max_string_length in
      Netlog.logf `Warning
	"Parameter mapred.sort_size is too large. Using %d instead" n;
      n
    )
    else
      mr_sort_size0 in

  let mr_pre_partitions_max =
    try
      let p = cf#resolve_parameter mr "pre_partitions_max" in
      cf#int_param p
    with
      | Not_found ->
	  mr_sort_size / (max mr_buffer_size_tight 1) in

  ( object
      method nn_disabled = nn_disabled
      method nn_clustername = clustername
      method nn_nodes = nn_addrs
      method file_root = file_root
      method plasma_root = plasma_root
      method mr_task_nodes = mr_hosts
      method mr_task_port = mr_task_port
      method mr_task_tmpdir = mr_task_tmpdir
      method mr_users = mr_users
      method mr_shm_low = mr_shm_low
      method mr_shm_high = mr_shm_high
      method mr_shm_max = mr_shm_max
      method mr_buf_low = mr_buf_low
      method mr_buf_high = mr_buf_high
      method mr_buf_max = mr_buf_max
      method mr_shm_low_factor = mr_shm_low_factor
      method mr_shm_high_factor = mr_shm_high_factor
      method mr_shm_max_factor = mr_shm_max_factor
      method mr_buf_low_factor = mr_buf_low_factor
      method mr_buf_high_factor = mr_buf_high_factor
      method mr_buf_max_factor = mr_buf_max_factor
      method mr_buffer_size = mr_buffer_size
      method mr_buffer_size_tight = mr_buffer_size_tight
      method mr_sort_size = mr_sort_size
      method mr_pre_partitions_max = mr_pre_partitions_max
    end
  )


let sixteen = 16 * 1024 * 1024
let sixtyfour = 64 * 1024 * 1024

let test_config ~clustername ~nodes
                ?(buffer_size=sixteen) ?(sort_size=sixtyfour) () =
  ( object
      method nn_disabled = false
      method nn_clustername = clustername
      method nn_nodes = nodes
      method file_root = "/"
      method plasma_root = None
      method mr_task_nodes = []
      method mr_task_port = 9999
      method mr_task_tmpdir = "/tmp/xxx"
      method mr_users = None
      method mr_shm_low = None
      method mr_shm_high = None
      method mr_shm_max = None
      method mr_buf_low = None
      method mr_buf_high = None
      method mr_buf_max = None
      method mr_shm_low_factor = 0.25
      method mr_shm_high_factor = 0.5
      method mr_shm_max_factor = 0.75
      method mr_buf_low_factor = 0.25
      method mr_buf_high_factor = 0.5
      method mr_buf_max_factor = 0.75
      method mr_buffer_size = buffer_size
      method mr_buffer_size_tight = buffer_size
      method mr_sort_size = sort_size
      method mr_pre_partitions_max = sort_size / buffer_size
    end
  )



let executable_version() =
  Digest.to_hex (Digest.file Sys.executable_name)
