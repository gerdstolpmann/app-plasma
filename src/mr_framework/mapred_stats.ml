(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

module StrMap = Map.Make(String)

type stats = float StrMap.t

let rec of_list l =
  match l with
    | (label, x) :: l' -> StrMap.add label x (of_list l')
    | [] -> StrMap.empty

let to_list st =
  StrMap.fold
    (fun label x acc ->
       (label,x) :: acc
    )
    st
    []


let empty = StrMap.empty

let add st1 st2 =
  StrMap.fold
    (fun label x acc ->
       try
	 let y = StrMap.find label acc in
	 StrMap.add label (x +. y) acc
       with Not_found ->
	 StrMap.add label x acc
    )
    st1
    st2

let addl l =
  List.fold_left add empty l
