(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let finish job =
  let rjc = Mapred_job_exec.get_rjc job in
  let p = Mapred_job_exec.message job in
  let tm = Mapred_job_exec.get_tm job in
  let mj = Mapred_job_exec.get_job job in
  let me = Mapred_job_exec.get_env job in
  let mjc = Mapred_job_exec.get_job_config rjc in
  let jc = Mapred_job_config.mapred_job_config mjc in
  let dump_plan = Mapred_job_exec.get_dump_plan_when_complete rjc in

  let status =
    (match Mapred_job_exec.status job with
       | `Running -> "running (hey, this is weird)"
       | `Successful -> "successful"
       | `Errors e -> "erroneous (" ^ String.concat "; " e ^ ")"
       | `Killed -> "killed"
    ) in
  p (sprintf "Job_status: %s\n%!" status);
  
  if Mapred_job_exec.status job = `Running && dump_plan then (
    p "NON-FINISHED PLAN:\n";
    if Mapred_job_exec.get_report rjc then
      let plan = Mapred_job_exec.get_plan job in
      Mapred_sched.print_plan (Mapred_job_exec.get_report_to rjc) plan
  );
  
  p "Cleanup...\n";
  Mapred_job_exec.cleanup job;
  if not (Mapred_job_exec.get_keep_temp_files rjc) then
    tm # delete_local_directory();  (* even if task_files = [] *)
  
  p "Post-finish hook...\n";
  mj # post_job_finish me jc;
  
  p (sprintf "Job_status: %s\n" status);
  p "Finished.\n"
    


let invoke ?(catch_sigint=false) ?(create_dirs=false) me rjc mj =
  let mjc = Mapred_job_exec.get_job_config rjc in
  let jc = Mapred_job_config.mapred_job_config mjc in
  let conf = me # config in

  let p = Mapred_job_exec.message_rjc rjc in
  
  p (sprintf "--------------------------------------------------------\n");
  p (sprintf "Job name:             %s\n" jc#name);
  p (sprintf "Job ID:               %s\n" jc#job_id);
  p (sprintf "Mapred_fs input dir:  %s\n" jc#input_dir);
  p (sprintf "Mapred_fs output dir: %s\n" jc#output_dir);
  p (sprintf "Mapred_fs work dir:   %s\n" jc#work_dir);
  p (sprintf "Mapred_fs log dir:    %s\n" jc#log_dir);
  p (sprintf "Number task nodes:    %d\n" (List.length conf#mr_task_nodes));
  p (sprintf "Task server dir:      %s\n" conf#mr_task_tmpdir);
  p (sprintf "--------------------------------------------------------\n%!");

  let fs = me#filesystem in
  let simulate = Mapred_job_exec.get_simulate rjc in

  if not simulate then (
    p "Checking...\n";
    List.iter
      (fun dir ->
	 try
	   try
	     let files = Mapred_io.scan_dir fs dir false in
	     if files <> [] then
	       failwith ("Directory not empty: " ^ dir)
	   with
	     ( Plasma_client.Plasma_error(`enoent) 
	     | Unix.Unix_error(Unix.ENOENT,_,_)
	     ) when create_dirs ->
	       Mapred_io.create_dir fs dir
	 with
 	   | error ->
	       failwith ("FS exception: " ^ Netexn.to_string error)
      )
      [ jc#output_dir; jc#work_dir; jc#log_dir ]
  );

  mj # check_config me jc;

  p "Planning...\n";

  let task_nodes_ip =
    List.map Plasma_util.ip_of_host conf#mr_task_nodes in
  let dn_info = fs # location_info jc#input_dir in
  let dn_info_ok =
    List.filter
      (fun (ip,_) ->
	 List.mem ip task_nodes_ip
      )
      dn_info in
  let dn_identities =
    List.map (fun (_,id) -> id) dn_info_ok in
  
  let job_id = jc # job_id in

  let mp = Mapred_job_exec.investigate_machines conf me#auth_ticket job_id in
  let planning_capacity = Mapred_job_exec.planning_capacity mp in

  let internal_suffix =
    (mj # internal_record_io me jc) # write_suffix in
  let output_suffix =
    (mj # output_record_io me jc) # write_suffix in

  let pc = 
    Mapred_sched.configure_plan
      ~keep_temp_files:(Mapred_job_exec.get_keep_temp_files rjc)
      ~planning_capacity
      ~internal_suffix
      ~output_suffix
      jc
      conf in

  let plan = Mapred_sched.create_plan ~dn_identities fs pc in
  Mapred_sched.add_inputs plan;
  Mapred_sched.complete_inputs plan;

  let dump_plan = Mapred_job_exec.get_dump_plan_when_complete rjc in
  if dump_plan then (
    p "INITIAL PLAN UP TO MAP:\n";
    if Mapred_job_exec.get_report rjc then
      Mapred_sched.print_plan (Mapred_job_exec.get_report_to rjc) plan
  );

  let tm = Mapred_taskfiles.scp_taskfile_manager conf jc in
  if jc#task_files <> [] then (
    p "Copying task files...\n";
    tm # copy_to_local_directory jc#task_files
  );

  p "Pre-start hook...\n";
  mj # pre_job_start me jc;

  p "Starting...\n";
  let esys = Unixqueue.create_unix_event_system() in
  let job = 
    Mapred_job_exec.start esys plan mj rjc mp tm me conf#mr_task_port
      (fun job ->
	 finish job
      )
  in
      
  let interrupt = ref false in

  if catch_sigint then (
    Netsys_signal.register_handler
      ~name:"ctrl-c" ~signal:Sys.sigint 
      ~callback:(fun _ -> interrupt := true)
      ();

    p "Job can be interrupted with CTRL-C!\n";
  
    let g = Unixqueue.new_group esys in
    let rec ctrlc() =
      Unixqueue.weak_once esys g 0.1
	(fun () ->
	   if !interrupt then (
	     p "CTRL-C seen. Killing job.\n";
	     Mapred_job_exec.kill job
	   ) else
	     ctrlc()
	) in
    ctrlc();
  );
  job
  
let run job =
  Unixqueue.run (Mapred_job_exec.event_system job);

  if Mapred_job_exec.status job = `Running then
    finish job


