(* $Id$ *)

open Printf

type read_flag =
    [ `Skip of int64 | `Binary | `Streaming | `Dummy ]

type read_file_flag =
    [ `Binary
    | `Temp of string * string | `Destination of string
    | `Dummy
    ]

type write_flag =
    [ `Create | `Exclusive | `Truncate | `Binary | `Streaming 
    | `Repl of int | `Location of string list
    | `Dummy
    ]

type write_file_flag =
    [ `Create | `Exclusive | `Truncate | `Binary | `Link
    | `Repl of int | `Location of string list
    | `Dummy
    ]

type write_common =
    [ `Create | `Exclusive | `Truncate | `Binary
    | `Repl of int | `Location of string list
    | `Dummy ]

type size_flag =
    [ `Dummy ]

type test_flag =
    [ `Link | `Dummy ]

type remove_flag =
    [ `Recursive | `Dummy ]

type rename_flag =
    [ `Dummy ]

type symlink_flag =
    [ `Dummy ]

type readdir_flag =
    [ `Dummy ]

type readlink_flag =
    [ `Dummy ]

type mkdir_flag =
    [ `Path | `Nonexcl | `Dummy ]

type rmdir_flag =
    [ `Dummy ]

type copy_flag =
    [ `Repl of int | `Location of string list | `Dummy ]

type link_flag =
    [ `Dummy ]

type test_type =
    [ `N | `E | `D | `F | `H | `R | `W | `X | `S ]

class type local_file =
object
  method filename : string

  method close : unit -> unit
end

class type filesystem =
object
  method path_encoding : Netconversion.encoding option
  method path_exclusions : (int * int) list
  method nominal_dot_dot : bool
  method read : read_flag list -> string -> Netchannels.in_obj_channel
  method read_file : read_file_flag list -> string -> local_file
  method write : write_flag list -> string -> Netchannels.out_obj_channel
  method write_file : write_file_flag list -> string -> local_file -> unit
  method size : size_flag list -> string -> int64
  method test : test_flag list -> string -> test_type -> bool
  method test_list : test_flag list -> string -> test_type list -> bool list
  method remove : remove_flag list -> string -> unit
  method rename : rename_flag list -> string -> string -> unit
  method symlink : symlink_flag list -> string -> string -> unit
  method readdir : readdir_flag list -> string -> string list
  method readlink : readlink_flag list -> string -> string
  method mkdir : mkdir_flag list -> string -> unit
  method rmdir : rmdir_flag list -> string -> unit
  method copy : copy_flag list -> string -> string -> unit
  method cancel : unit -> unit
  method open_cluster : string -> Unixqueue.event_system -> 
                            Plasma_client.plasma_cluster option
  method open_filesystem : unit -> filesystem
  method local_root : string -> string option
  method blocksize : string -> int
  method copy_out_to_buf : string -> int64 -> Netsys_mem.memory -> int -> unit
  method copy_in_from_buf : string -> int64 -> Netsys_mem.memory -> int -> unit
  method commit_files : (string * string) list -> unit
  method location_info : string -> (Unix.inet_addr * string) list
  method blocklist : string -> Plasma_rpcapi_aux.blockinfo list
  method have_link : string -> bool
  method link : link_flag list -> string -> string -> unit
  method set_aside : unit -> unit
  method close : unit -> unit
end


let rec extract f l d =
  match l with
    | x :: l' ->
	( match f x with
	    | None -> extract f l' d
	    | Some v -> v
	)
    | [] ->
	d


(* Assure that filesystem is a subtype of stream_fs *)
let _ =
  (fun (x:filesystem) -> (x:>Netfs.stream_fs)) ;;

let rec plasma_filesystem_i no_shm ?plasma_root pcc configure : filesystem =
  let open_cluster esys =
    let cluster =
      Plasma_client.open_cluster_cc pcc esys in
    configure cluster;
    (* We cannot share the shm manager with the parent if the new filesystem
       will be used in a different thread. So, disable it.
     *)
    if no_shm then
      Plasma_client.configure_shm_manager cluster 
	(new Plasma_shm.null_shm_manager());
    cluster in
  let cluster = open_cluster (Unixqueue.create_unix_event_system()) in
  let fs = Plasma_netfs.x_netfs cluster in
  ( object (self)
      method path_encoding = fs # path_encoding
      method path_exclusions = fs # path_exclusions
      method nominal_dot_dot = fs # nominal_dot_dot
      method read = fs # read
      method read_file = fs # x_read_file
      method size = fs # size
      method test = fs # test
      method test_list = fs # test_list
      method remove = fs # remove
      method rename = fs # rename
      method symlink = fs # symlink
      method readdir = fs # readdir
      method readlink = fs # readlink
      method mkdir = fs # mkdir
      method rmdir = fs # rmdir
      method cancel = fs # cancel
      
      method write = fs # x_write
      method write_file = fs # x_write_file
      method copy = fs # x_copy
      method link = fs # x_link
      method blocklist = fs # x_blocklist
      method copy_out_to_buf = fs # x_copy_out_to_buf
      method copy_in_from_buf = fs # x_copy_in_from_buf

      method commit_files l = fs # x_commit_files l
      method blocksize _ = fs # x_blocksize()
      method location_info _ = fs # x_location_info()

      method have_link _ = true
      method local_root path =
        match plasma_root with
          | None -> None
          | Some prefix ->
              Some(
                if path = "" || path.[0] = '/' then
                  prefix ^ path
                else
                  prefix ^ "/" ^ path
              )
                    
      method open_cluster _ esys = Some(open_cluster esys)
      method open_filesystem() = plasma_filesystem_i true pcc configure

      method set_aside() =
	Plasma_client.abort_cluster cluster

      method close() =
	Plasma_client.close_cluster cluster

    end
  )


let plasma_filesystem = plasma_filesystem_i false


let rec local_filesystem root : filesystem =
  let check_and_norm_path p =
    (* a lame emulation *)
    if p = "" || p.[0] <> '/' then
      raise(Unix.Unix_error(Unix.EINVAL, "Mapred_fs: not absolute", p));
    p in

  let blocksize = 65536 in
  let blocksizeL = Int64.of_int blocksize in

  let identity = sprintf "unixroot_%s" (Unix.gethostname()) in

  let fs = Netfs.local_fs ~root () in

  ( object(self)
      method path_encoding = fs # path_encoding
      method path_exclusions = fs # path_exclusions
      method nominal_dot_dot = fs # nominal_dot_dot
      method read = fs # read
      method size = fs # size
      method test = fs # test
      method test_list = fs # test_list
      method remove = fs # remove
      method rename = fs # rename
      method symlink = fs # symlink
      method readdir = fs # readdir
      method readlink = fs # readlink
      method mkdir = fs # mkdir
      method rmdir = fs # rmdir
      method cancel = fs # cancel

      method have_link _ = true

      method read_file flags file =
	let flags' =
	  List.map
	    (function
	       | #Netfs.read_file_flag as fl -> fl
	       | _ -> `Dummy
	    )
	    flags in
        let destination =
          extract
            (function
               | `Destination filename -> Some (Some filename)
               | _ -> None
            )
            flags
            None in
        match destination with
          | None ->
              fs # read_file flags' file
          | Some d ->
              let lf = fs # read_file flags' file in
              ( try Unix.unlink d with _ -> () );
              Unix.symlink lf#filename d;
              ( object
                  method filename = d
                  method close() = ()
                end
              )

      method write flags file =
	let flags' =
	  List.map
	    (function
	       | #Netfs.write_flag as fl -> fl
	       | _ -> `Dummy
	    )
	    flags in
	fs # write flags' file

      method write_file flags file =
	let flags' =
	  List.map
	    (function
	       | #Netfs.write_file_flag as fl -> fl
	       | _ -> `Dummy
	    )
	    flags in
	fs # write_file flags' file

      method copy flags file =
	let flags' =
	  List.map
	    (function
	       | #Netfs.copy_flag as fl -> fl
	       | _ -> `Dummy
	    )
	    flags in
	fs # copy flags' file

      method link flags oldname newname = 
	let fn_oldname = root ^ check_and_norm_path oldname in
	let fn_newname = root ^ check_and_norm_path newname in
	Unix.link fn_oldname fn_newname

      method local_root name =
	let fn = root ^ check_and_norm_path name in
        Some fn

      method blocklist name = 
	let fn = root ^ check_and_norm_path name in
	let st = Unix.LargeFile.stat fn in
	let len = st.Unix.LargeFile.st_size in
	if len=0L then 
	  []
        else 
	  let blocks =
	    Int64.succ (Int64.div (Int64.pred len) blocksizeL) in
	  [ { Plasma_rpcapi_aux.index = 0L;
	      node = "127.0.0.1:0";
	      identity = identity;
	      block = 0L;
	      length = blocks;
	      node_alive = true;
	      checksum = None;
	      inode_seqno = 0L;
	      inode_committed = true;
	      ticket = { Plasma_rpcapi_aux.range_start = 0L;
	  	         range_length = blocks;
		         safetrans_id = 0L;
		         safetrans_tmo = Int64.max_int;
		         safetrans_vfy = 0L;
		         read_perm = true;
		         write_perm = true
		       }
	    }
	  ]

      method copy_out_to_buf name pos buf len =
	let fn = root ^ check_and_norm_path name in
	let fd = Unix.openfile fn [Unix.O_RDONLY] 0 in
	try
	  ignore(Unix.LargeFile.lseek fd pos Unix.SEEK_SET);
	  let n = Netsys_mem.mem_read fd buf 0 len in
	  Bigarray.Array1.fill
	    (Bigarray.Array1.sub buf n (len-n))
	    '\000';
	  Unix.close fd
	with
	  | error ->
	      Unix.close fd;
	      raise error

      method copy_in_from_buf name pos buf len =
	let fn = root ^ check_and_norm_path name in
	let fd = Unix.openfile fn [Unix.O_WRONLY] 0o666 in
	try
	  ignore(Unix.LargeFile.lseek fd pos Unix.SEEK_SET);
	  ignore(Netsys_mem.mem_write fd buf 0 len);
	  Unix.close fd
	with
	  | error ->
	      Unix.close fd;
	      raise error

      method commit_files l =
	let created = ref [] in
	let renames = ref [] in
	try
	  List.iter
	    (fun (_,f) ->
	       let fd = 
		 Unix.openfile f [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_EXCL] 
		   0o666 in
	       Unix.close fd;
	       created := f :: !created
	    )
	    l;
	  List.iter
	    (fun (o,n) ->
	       Unix.rename o n;
	       renames := (o,n) :: !renames
	    )
	    l
	with
	  | error ->
	      ( try
		  List.iter (fun (o,n) -> Unix.rename n o) !renames;
		  List.iter (fun n -> Unix.unlink n) !created;
		with
		  | inner_error ->
		      Netlog.logf `Warning
			"Mapred_fs.commit_files: Exception while \
                         reverting after an exception: %s"
			(Netexn.to_string inner_error)
	      );
	      raise error

      method blocksize _ = blocksize

      method location_info _ = 
	[ Unix.inet_addr_of_string "127.0.0.1",
	  identity
	]

      method open_cluster _ esys = None

      method open_filesystem() = local_filesystem root

      method set_aside() =
	()

      method close() =
	()

    end
  )


let tree_re =
  Pcre.regexp "^([^:]+)::(.*)$"


let rec multi_tree (trees:(string*filesystem) list) default =
  let default_tree =
    try List.assoc default trees
    with Not_found ->
      failwith "Mapred_fs.multi_tree: the default tree is not defined" in

  let dest_name name =
    if name = "" || name.[0] = '/' then
      (name, default_tree)
    else
      try
	let p = Pcre.extract ~rex:tree_re ~full_match:false name in
	match p with
	  | [| tree_name; path_name |] ->
	      let tree =
		try List.assoc tree_name trees
		with Not_found ->
		  raise(Unix.Unix_error(Unix.ENOENT, "Mapred_fs.multi_tree",
					name)) in
	      (path_name, tree)
	  | _ -> raise Not_found
      with
	| Not_found ->
	    raise(Unix.Unix_error(Unix.EINVAL, "Mapred_fs.multi_tree", name)) in

  ( object(self)

      method path_encoding = None
      method path_exclusions = [ 0,0; 47,47 ]
      method nominal_dot_dot = false
      method read flags name =
	let (path,tree) = dest_name name in
	tree # read flags path
      method read_file flags name =
	let (path,tree) = dest_name name in
	tree # read_file flags path
      method write flags name =
	let (path,tree) = dest_name name in
	tree # write flags path
      method write_file flags name =
	let (path,tree) = dest_name name in
	tree # write_file flags path
      method size flags name =
	let (path,tree) = dest_name name in
	tree # size flags path
      method test flags name t  =
	let (path,tree) = dest_name name in
	tree # test flags path t
      method test_list flags name tl  =
	let (path,tree) = dest_name name in
	tree # test_list flags path tl
      method remove flags name =
	let (path,tree) = dest_name name in
	tree # remove flags path
      method rename flags oldname newname =
	let (oldpath,oldtree) = dest_name oldname in
	let (newpath,newtree) = dest_name newname in
	if oldtree <> newtree then
	  raise(Unix.Unix_error(Unix.EXDEV, "Mapred_fs.rename", oldname));
	oldtree # rename flags oldpath newpath
      method commit_files l =
	if l <> [] then (
	  let d = 
	    List.map
	      (fun (o,n) -> (dest_name o, dest_name n))
	      l in
	  let d_tree = 
	    List.flatten
	      (List.map
		 (fun ((_,o_tree),(_,n_tree)) -> [o_tree;n_tree])
		 d
	      ) in
	  let t1 = List.hd d_tree in
	  if List.exists (fun t -> t <> t1) d_tree then
	    raise(Unix.Unix_error
		    (Unix.EXDEV, "Mapred_fs.commit_files", "<various>"));
	  let d_path =
	    List.map 
	      (fun ((o_path,_),(n_path,_)) -> (o_path,n_path)) 
	      d in
	  t1 # commit_files d_path
	)
      method symlink flags dest name =
	let (path,tree) = dest_name name in
	tree # symlink flags dest path
      method readdir flags name =
	let (path,tree) = dest_name name in
	tree # readdir flags path
      method readlink flags name =
	let (path,tree) = dest_name name in
	tree # readlink flags path
      method mkdir flags name =
	let (path,tree) = dest_name name in
	tree # mkdir flags path
      method rmdir flags name =
	let (path,tree) = dest_name name in
	tree # rmdir flags path
      method copy flags oldname newname =
	let (oldpath,oldtree) = dest_name oldname in
	let (newpath,newtree) = dest_name newname in
	if oldtree <> newtree then
	  raise(Unix.Unix_error(Unix.EXDEV, "Mapred_fs.copy", oldname));
	oldtree # copy flags oldpath newpath
      method cancel () =
	List.iter
	  (fun (_,tree) -> tree#cancel())
	  trees
      method open_cluster name esys =
	try
	  let (path,tree) = dest_name name in
	  tree # open_cluster path esys
	with
	  | Unix.Unix_error(Unix.ENOENT,_,_) -> None
      method local_root name =
	let (path,tree) = dest_name name in
        tree # local_root path
      method blocksize name =
	let (path,tree) = dest_name name in
	tree # blocksize path
      method copy_out_to_buf name pos buf len =
	let (path,tree) = dest_name name in
	tree # copy_out_to_buf path pos buf len
      method copy_in_from_buf name pos buf len =
	let (path,tree) = dest_name name in
	tree # copy_in_from_buf path pos buf len
      method location_info name =
	let (path,tree) = dest_name name in
	tree # location_info name
      method blocklist name =
	let (path,tree) = dest_name name in
	tree # blocklist path
      method have_link name =
	let (path,tree) = dest_name name in
	tree # have_link path
      method link flags oldname newname =
	let (oldpath,oldtree) = dest_name oldname in
	let (newpath,newtree) = dest_name newname in
	if oldtree <> newtree then
	  raise(Unix.Unix_error(Unix.EXDEV, "Mapred_fs.link", oldname));
	oldtree # link flags oldpath newpath
      method set_aside() =
	List.iter
	  (fun (_,tree) -> tree#set_aside())
	  trees
      method close() =
	List.iter
	  (fun (_,tree) -> tree#close())
	  trees
      method open_filesystem() = 
	let trees' =
	  List.map
	    (fun (name,tree) -> (name, tree#open_filesystem()))
	    trees in
	multi_tree trees' default
    end
  )


let standard_fs_cc ?(custom=[]) ?default 
                   ?(configure_cluster = fun c ->
		       Plasma_client.configure_auth_daemon c;
		       Plasma_client.configure_buffer c 16
		    )
		   ?client_config
		   ?(file_root = "/")
                   ?plasma_root
		   () =
  match client_config with
    | None ->
	multi_tree
	  (custom @
	     [ "file", local_filesystem file_root ])
	  ( match default with
	      | None -> "file"
	      | Some d -> d
	  )
    | Some cc ->
	multi_tree
	  (custom @
	     [ "file", local_filesystem file_root;
	       "plasma", (plasma_filesystem ?plasma_root cc configure_cluster)
	     ]
	  )
	  ( match default with
	      | None -> "plasma"
	      | Some d -> d
	  )
	  

let standard_fs ?custom ?default ?configure_cluster conf =
  let client_config =
    if conf#nn_disabled then
      None
    else
      Some
	(Plasma_client_config.get_config
	   ~clustername:conf#nn_clustername
	   ~nn_nodes:conf#nn_nodes ()
	) in
  let file_root = conf # file_root in
  let plasma_root = conf # plasma_root in
  standard_fs_cc
    ?custom ?default ?configure_cluster ?client_config ~file_root 
    ?plasma_root ()
