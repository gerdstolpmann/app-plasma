(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Mapred_def

let management_overhead = 0.2
  (* how much additional space can be allocated for managing the
     records to sort. This is specified as fraction of the sort_limit.
   *)


module Array_builder : sig
  type ('a, 'b) t
  val create : ('a,'b) Bigarray.kind -> ('a,'b) t
  val reserve : ('a,'b) t -> int -> unit
  val add : ('a,'b) t -> 'a -> unit
  val length : ('a,'b) t -> int
  val return : ('a,'b) t -> ('a,'b, Bigarray.c_layout) Bigarray.Array1.t
  val iter : ('a -> unit) -> ('a, 'b) t -> unit
  val clear : ('a,'b) t -> unit
end = struct
  let chunk_bits = 16     (* number of bits of chunk_size *)
  let chunk_size = 1 lsl chunk_bits
  let chunk_mask = chunk_size - 1

  type ('a, 'b) t =
      { data : (int, ('a,'b, Bigarray.c_layout) Bigarray.Array1.t) Hashtbl.t;
	kind : ('a,'b) Bigarray.kind;
	mutable chunks :int;
	mutable length : int;
	mutable cur_chunk : ('a,'b, Bigarray.c_layout) Bigarray.Array1.t;
	mutable cur_msb : int;
      }

  let create kind =
    { data = Hashtbl.create 7;
      kind = kind;
      chunks = 0;
      length = 0;
      cur_chunk = Bigarray.Array1.create kind Bigarray.c_layout 0;
      cur_msb = (-1)
    }

  let clear ab =
    Hashtbl.clear ab.data;
    ab.chunks <- 0;
    ab.length <- 0;
    ab.cur_chunk <- Bigarray.Array1.create ab.kind Bigarray.c_layout 0;
    ab.cur_msb <- (-1)

  let space ab =
    ab.chunks * chunk_size - ab.length

  let reserve ab req_space =
    while space ab < req_space do
      let chunk = Bigarray.Array1.create ab.kind Bigarray.c_layout chunk_size in
      Hashtbl.add ab.data ab.chunks chunk;
      ab.chunks <- ab.chunks + 1
    done

  let load ab j = 
    if ab.cur_msb <> j then (
      ab.cur_chunk <- Hashtbl.find ab.data j;
      ab.cur_msb <- j
    )

  (* Best if this function is inlined! *)
  let add ab x =
    if space ab < 1 then reserve ab 1;
    let j = ab.length lsr chunk_bits in
    let k = ab.length land chunk_mask in
    load ab j;
    ab.cur_chunk.{ k } <- x;
    ab.length <- ab.length + 1

  let length ab = ab.length

  let return ab =
    let built = 
      Bigarray.Array1.create ab.kind Bigarray.c_layout ab.length in
    let j = ref 0 in
    let n = ref ab.length in
    while !n > 0 do
      let chunk = Hashtbl.find ab.data !j in
      let m = min !n chunk_size in
      Bigarray.Array1.blit 
	(Bigarray.Array1.sub chunk 0 m)
	(Bigarray.Array1.sub built (!j * chunk_size) m);
      incr j;
      n := !n - m
    done;
    built

  let iter f ab =
    let j = ref 0 in
    let n = ref ab.length in
    while !n > 0 do
      let chunk = Hashtbl.find ab.data !j in
      let m = min !n chunk_size in
      for k = 0 to m-1 do
	f chunk.{k}
      done;
      incr j;
      n := !n - m
    done
end


let int_size = float (Sys.word_size / 8)


let null_sorter me mjc factor =
  let sort_limit = 
    max 65536 (truncate (factor *. float me#config#mr_sort_size)) in
  let b_data = Netsys_mem.alloc_memory_pages sort_limit in
  let b_eof = ref 0 in
  let lines = Array_builder.create Bigarray.int in
  let closed = ref false in
object(self)
  method name = "Mapred_sorters.null_sorter"

  method set_key_extraction _ = ()

  method eff_sort_limit = sort_limit

  method sort_lines_limit size =
    truncate(float size *. management_overhead /. int_size)

  method put_records q =
    if !closed then
      failwith "Mapred_sorters.null_sorter: closed";
    let pos = ref !b_eof in
    Array_builder.reserve lines (Queue.length q);
    Queue.iter
      (fun line ->
	 let len = String.length line in
	 if !pos + len > sort_limit then
	   failwith
	     "Mapred_sorters.null_sorter: mount of data exceeds sort limit";
	 Netsys_mem.blit_string_to_memory line 0 b_data !pos len;
	 Array_builder.add lines !pos;
	 pos := !pos + len
      )
      q;
    Queue.clear q;
    b_eof := !pos


  method sort writer =
    if !closed then
      failwith "Mapred_sorters.null_sorter: closed";
    Array_builder.add lines !b_eof;
    let first = ref true in
    let q = ref 0 in
    Array_builder.iter
      (fun p ->
	 if !first then
	   first := false
	 else (
	   let len = p - !q in
	   let s = String.create len in
	   Netsys_mem.blit_memory_to_string b_data !q s 0 len;
	   writer # output_record s
	 );
	 q := p
      )
      lines;
     Array_builder.clear lines;
    b_eof := 0
    
  method close() =
    if not !closed then (
      Array_builder.clear lines;
      Netsys_mem.memory_unmap_file b_data;
      closed := true
    )
    
  method sort_time = 0.0
  method hash _ _ _ = 0
  method cmp _ _ _ _ _ _ = 0
end


let generic_sorter ~hash ~cmp me mjc factor =
  let sort_limit = 
    max 65536 (truncate (factor *. float me#config#mr_sort_size)) in
  let b_data = Netsys_mem.alloc_memory_pages (sort_limit+16) in
  let b_offs, _ = Netsys_mem.init_string b_data 0 sort_limit in
  let b_str = (Netsys_mem.as_value b_data b_offs : string) in
  let key_extractor = 
    ref (fun _ -> failwith "Missing config: set_key_extraction") in
  let b_eof = ref 0 in
  let lines = Array_builder.create Bigarray.int in
  let hashes = Array_builder.create Bigarray.int32 in
  let keys = Array_builder.create Bigarray.int32 in
  (* [keys] stores a pair [(key_start,key_len)] for each line. The two
     numbers are encoded (via bit shifting) in a single int32
   *)
  let keys_extra = Hashtbl.create 7 in
  (* [keys_extra] stores all keys that cannot be encoded as int32 and put
     into [keys]. There cannot be many such keys, because this occurs only
     for very long lines or very long keys
   *)
  let closed = ref false in
  let last_hash = ref 0 in
  let last_key = ref (0,0) in
  let still_sorted = ref true in
  let bits20 = 0b1111_1111_1111_1111_1111 in   (* 20 bits *)
  let bits11 = 0b111_1111_1111 in              (* 11 bits *)
  let sort_time = ref 0.0 in
object(self)
  method name = "Mapred_sorters.generic_sorter"

  method set_key_extraction ex =
    key_extractor := ex

  method eff_sort_limit = sort_limit

  method sort_lines_limit size =
    truncate(float size *. management_overhead /. 
	       (2.0 *. int_size +. 8.0))


  method put_records q =
    if !closed then
      failwith "Mapred_sorters.null_sorter: closed";
    let pos = ref !b_eof in
    Array_builder.reserve lines (Queue.length q);
    let key_extractor = !key_extractor in
    Queue.iter
      (fun line ->
	 let len = String.length line in
	 if !pos + len > sort_limit then
	   failwith
	     "Mapred_sorters.generic_sorter: amount of data exceeds sort limit";
	 String.blit line 0 b_str !pos len;
	 Array_builder.add lines !pos;

	 let (key_start, key_len) = key_extractor line in
	 if key_start < 0 || key_len < 0 || key_start > len - key_len then
	   failwith "Mapred_sorters.generic_sorter: extractor returns \
                     key range outside the bounds of the record";

	 let key_abs_start = !pos + key_start in
	 let h = hash line key_start key_len in

	 (* Check whether the keys are already sorted *)
	 if !pos > 0 && !still_sorted then (
	   let p1 = !last_hash - h in
	   let p =
	     if p1 <> 0 then p1 else
	       cmp
		 b_str (fst !last_key) (snd !last_key)
		 line key_start key_len in
	   if p > 0 then
	     still_sorted := false;
	 );
	 last_hash := h;
	 last_key := (key_abs_start, key_len);

	 (* Also fill the [hashes] and [keys] arrays *)
	 Array_builder.add hashes (Int32.of_int h);
	 let enc_key =
	   if key_start > bits20 || key_len > bits11 then
	     (-1l)
	   else
	     Int32.logor
	       (Int32.shift_left (Int32.of_int key_start) 20)
	       (Int32.of_int key_len) in
	 if enc_key < 0l then
	   Hashtbl.add
	     keys_extra (Array_builder.length keys) (key_start,key_len);
	 Array_builder.add keys enc_key;
	 
	 pos := !pos + len;
      )
      q;
    Queue.clear q;
    b_eof := !pos


  method sort writer =
    if !closed then
      failwith "Mapred_sorters.null_sorter: closed";
    let length = Array_builder.length lines in
    (* add a guard at the end *)
    Array_builder.add lines !b_eof;

    let lines_a = Array_builder.return lines in
    Array_builder.clear lines;

    let hashes_a = Array_builder.return hashes in
    Array_builder.clear hashes;

    let keys_a = Array_builder.return keys in
    Array_builder.clear keys;

    let index = 
      Bigarray.Array1.create Bigarray.int Bigarray.c_layout length in
    for k = 0 to length-1 do
      index.{ k } <- k
    done;

    let t0 = Unix.gettimeofday() in
    let u = ref 0 in
    let v = ref 0 in
    if not !still_sorted then (
      Mapred_sort.ba_merge_sort
	(fun j1 j2 ->
	   incr v;
	   let h1 = Int32.to_int (Bigarray.Array1.unsafe_get hashes_a j1) in
	   let h2 = Int32.to_int (Bigarray.Array1.unsafe_get hashes_a j2) in
	   if h1 <> h2 then
	     h1 - h2
	   else (
	     incr u;
	     let q1 = lines_a.{ j1 } in
	     let q2 = lines_a.{ j2 } in
	     let ek1 = keys_a.{ j1 } in
	     let (ks1, kl1) = 
	       if ek1 >= 0l then
		 (Int32.to_int (Int32.shift_right ek1 20),
		  Int32.to_int (Int32.logand ek1 (Int32.of_int bits11)))
	       else
		 try Hashtbl.find keys_extra j1
		 with Not_found -> assert false in
	     let ek2 = keys_a.{ j2 } in
	     let (ks2, kl2) = 
	       if ek2 >= 0l then
		 (Int32.to_int (Int32.shift_right ek2 20),
		  Int32.to_int (Int32.logand ek2 (Int32.of_int bits11)))
	       else
		 try Hashtbl.find keys_extra j2
		 with Not_found -> assert false in
	     cmp b_str (q1+ks1) kl1 b_str (q2+ks2) kl2
	   )
	)
	index;
    );
    let t1 = Unix.gettimeofday() in
    sort_time := t1 -. t0;
(*
Printf.printf "#callbacks: %d\n" !v;
Printf.printf "#cmp: %d\n%!" !u;
 *)

    for k = 0 to length-1 do
      let j = index.{ k } in
      let q = lines_a.{ j } in
      let p = lines_a.{ j+1 } in
      let len = p - q in
      let s = String.sub b_str q len in
      writer # output_record s
    done;

    (* reinit: the builders are already cleared, so the only remaining
       part:
     *)
    b_eof := 0;
    Hashtbl.clear keys_extra;
    still_sorted := true


  method close() =
    if not !closed then (
      Array_builder.clear lines;
      Array_builder.clear hashes;
      Array_builder.clear keys;
      Hashtbl.clear keys_extra;
      Netsys_mem.memory_unmap_file b_data;
      closed := true
    )
    
  method sort_time = !sort_time
  method hash = hash
  method cmp = cmp

end



module Grouped = struct
  let hash s p l = Hashtbl.hash (String.sub s p l)
  let cmp = Platform.str_cmp
end


module String_asc = struct
  let hash s p l =
    (* We copy the first up to 3.75 bytes into the "hash" *)
    let x3 =
      if l=0 then 0 else
	(Char.code s.[p]) lsl 22 in
    let x2 =
      if l<2 then 0 else
	(Char.code s.[p+1]) lsl 14 in
    let x1 =
      if l<3 then 0 else
	(Char.code s.[p+2]) lsl 6 in
    let x0 =
      if l<4 then 0 else
	(Char.code s.[p+3]) lsr 2 in
    x3 lor x2 lor x1 lor x0

  let cmp = Platform.str_cmp
end


let max_hash = 0x3fff_ffff


module String_desc = struct
  let hash s p l = max_hash - String_asc.hash s p l

  let cmp s1 p1 l1 s2 p2 l2 =
    Platform.str_cmp s2 p2 l2 s1 p1 l1
end


module Any_integer_asc = struct
  let hash is_digit digit_val s p l =
    (* Basically we use the number of digits as hash, plus the first four 
       digits *)
    let negative = ref false in
    let sign = ref false in
    let digits = ref 0 in
    let nonzero = ref false in
    let d1 = ref 0 in
    let d2 = ref 0 in
    let d3 = ref 0 in
    let d4 = ref 0 in
    for k = p to p+l-1 do
      match s.[k] with  
	| '+' ->
	    sign := true
	| '-' ->
	    if not !sign then negative := true;
	    sign := true
	| '0' ->
	    if !nonzero then incr digits;
	    sign := true
	| c when is_digit c ->
	    nonzero := true;
	    incr digits;
	    if !digits = 1 then d1 := digit_val c;
	    if !digits = 2 then d2 := digit_val c;
	    if !digits = 3 then d3 := digit_val c;
	    if !digits = 4 then d4 := digit_val c;
	    sign := true
	| _ ->
	    ()
    done;
    (* up to 13 bits *)
    if !digits > 8191 then (
      digits := 8181;
      d1 := 15; d2 := 15; d3 := 15; d4 := 15;
    );
    let p1 =
      (!digits lsl 16) lor
	(!d1 lsl 12) lor (!d2 lsl 8) lor (!d3 lsl 4) lor !d4 in
    let p2 =
      if !negative && !digits > 0 then -p1 else p1 in
    p2 + 0b10_0000_0000_0000_0000_0000_0000_0000
    
  let first_digit is_digit s p l =
    (* Returns the position of the first non-zero digit, plus the sign *)
    let negative = ref false in
    let sign = ref false in
    let digit_seen = ref false in
    let k = ref p in
    while not !digit_seen && !k < p+l do
      match s.[!k] with  
	| '+' ->
	    sign := true;
	    incr k
	| '-' ->
	    if not !sign then negative := true;
	    sign := true;
	    incr k
	| '0' ->
	    sign := true;
	    incr k
	| c when is_digit c ->
	    digit_seen := true
	| _ ->
	    incr k
    done;
    (!k, !negative)

  let cmp is_digit digit_val s1 p1 l1 s2 p2 l2 =
    let e1 = p1+l1 in
    let e2 = p2+l2 in

    let (q1, neg1) = first_digit is_digit s1 p1 l1 in
    let (q2, neg2) = first_digit is_digit s2 p2 l2 in
    
    let z1 = (q1 = e1) in   (* whether zero *)
    let z2 = (q2 = e2) in   (* whether zero *)

    let neg1 = neg1 && not z1 in   (* fixup: -0 *)
    let neg2 = neg2 && not z2 in   (* fixup: -0 *)

    (* Look at the sign. Maybe we can already conclude something *)
    if neg1 && not neg2 then
      (-1)
    else if not neg1 && neg2 then
      1
    else (
      (* Now walk over both numbers simultaneously. We count the number
	 of digits, and we remember whether one number was bigger for
	 the case that they have the same number of digits
       *)
      let dig1 = ref (if q1 < e1 then 1 else 0) in
      let dig2 = ref (if q2 < e2 then 1 else 0) in
      
      let big1 = ref false in
      let big2 = ref false in

      let j1 = ref q1 in
      let j2 = ref q2 in

      while !dig1 = !dig2 && !j1 < e1 && !j2 < e2 do
	(* !j1 and !j2 point to digits *)
	if not !big1 && not !big2 then (
	  let d1 = digit_val s1.[!j1] in
	  let d2 = digit_val s2.[!j2] in
	  big1 := d1 > d2;
	  big2 := d2 > d1;
	);
	(* advance j1: *)
	incr j1;
	while !j1 < e1 && not(is_digit s1.[!j1]) do incr j1 done;
	if !j1 < e1 then incr dig1;
	(* advance j2: *)
	incr j2;
	while !j2 < e2 && not(is_digit s2.[!j2]) do incr j2 done;
	if !j2 < e2 then incr dig2;
      done;

      let r1 =
	if !dig1 = !dig2 then (
	  if !big1 then 1 else if !big2 then (-1) else 0
	)
	else
	  if !dig1 < !dig2 then
	    (-1)
	  else
	    1 in
      if neg1 then -r1 else r1
    )
end


module Integer_asc = struct
  let is_digit =
    function
      | '0'..'9' -> true
      | _ -> false
      

  let digit_val c = Char.code c - 48

  let hash = Any_integer_asc.hash is_digit digit_val

  let cmp = Any_integer_asc.cmp is_digit digit_val

end


module Integer_desc = struct
  let hash s p l = max_hash - Integer_asc.hash s p l

  let cmp s1 p1 l1 s2 p2 l2 =
    Integer_asc.cmp s2 p2 l2 s1 p1 l1
end


module Hex_asc = struct
  let is_digit =
    function
      | '0'..'9' -> true
      | 'a'..'f' -> true
      | 'A'..'F' -> true
      | _ -> false
      

  let digit_val =
    function
      | '0'..'9' as c -> Char.code c - 48
      | 'a'..'f' as c -> Char.code c - 87
      | 'A'..'F' as c -> Char.code c - 55
      | _ -> 0

  let hash = Any_integer_asc.hash is_digit digit_val

  let cmp = Any_integer_asc.cmp is_digit digit_val

end


module Hex_desc = struct
  let hash s p l = max_hash - Hex_asc.hash s p l

  let cmp s1 p1 l1 s2 p2 l2 =
    Hex_asc.cmp s2 p2 l2 s1 p1 l1
end


module Float_asc = struct
  let hashx x =
    match classify_float x with
      | FP_nan -> 0
      | FP_infinite -> if x < 0.0 then 1 else 4251
      | FP_zero -> 2127
      | _ ->
	  let (_, exp) = frexp x in  (* exp is in the range -1073..1024 *)
	  let exp1 = exp + 1100 in   (* range: ..2124 *)
	  let exp2 = if x < 0.0 then -exp1 else exp1 in (* range: -2124..2124 *)
	  exp2 + 2126  (* 2..4250 *)

  let hash s p l =
    (* Values:
       0 = nan
       1 = -infinity
       2..2124: negative finite values
       2127: zero
       2128..4250: positive finite values
       4251: +infinity
     *)
    let x =
      try float_of_string (String.sub s p l)
      with _ -> nan in
    hashx x
	  
  let hashcmp h1 h2 = h1-h2

  let cmp s1 p1 l1 s2 p2 l2 =
    let x1 =
      try float_of_string (String.sub s1 p1 l1)
      with _ -> nan in
    let x2 =
      try float_of_string (String.sub s2 p2 l2)
      with _ -> nan in
    compare x1 x2
end


module Float_desc = struct
  let hash s p l = max_hash - Float_asc.hash s p l

  let cmp s1 p1 l1 s2 p2 l2 =
    Float_asc.cmp s2 p2 l2 s1 p1 l1
end


let msbyteL = 0xFF00_0000_0000_0000L

module Int64_xdr_asc = struct
  let hash s p l =
    if l <> 8 then
      failwith "Mapred_sorters.Int64_xdr_asc.hash: not 8 bytes";
    let i8 =
      Netnumber.BE.read_int8 s p in
    let i64 =
      Netnumber.int64_of_int8 i8 in
    if i64 < 0L then (
      let x = ref i64 in
      let n = ref 0 in
      while !n < 7 && Int64.logand !x msbyteL = msbyteL do
	incr n;
	x := Int64.shift_left !x 8
      done;
      let b0 = 
	Int64.to_int(Int64.logand (Int64.shift_right_logical !x 56) 0xFFL) in
      let b1 = 
	Int64.to_int(Int64.logand (Int64.shift_right_logical !x 48) 0xFFL) in
      let b2 = 
	Int64.to_int(Int64.logand (Int64.shift_right_logical !x 40) 0xFFL) in
      (!n lsl 24) lor (b0 lsl 16) lor (b1 lsl 8) lor b2
    )
    else (
      let x = ref i64 in
      let n = ref 0 in
      while !n < 7 && Int64.logand !x msbyteL = 0L do
	incr n;
	x := Int64.shift_left !x 8
      done;
      let b0 = 
	Int64.to_int(Int64.logand (Int64.shift_right_logical !x 56) 0xFFL) in
      let b1 = 
	Int64.to_int(Int64.logand (Int64.shift_right_logical !x 48) 0xFFL) in
      let b2 = 
	Int64.to_int(Int64.logand (Int64.shift_right_logical !x 40) 0xFFL) in
      0b1000_0000_0000_0000_0000_0000_0000 lor
	((7 - !n) lsl 24) lor (b0 lsl 16) lor (b1 lsl 8) lor b2
    )

  let hashcmp h1 h2 = h1-h2

  let cmp s1 p1 l1 s2 p2 l2 =
    let x1 = String.sub s1 p1 l1 in
    let x2 = String.sub s2 p2 l2 in
    (* Invert the first bit (sign) *)
    x1.[0] <- Char.chr ((Char.code x1.[0]) lxor 0b1000_0000);
    x2.[0] <- Char.chr ((Char.code x2.[0]) lxor 0b1000_0000);
    String.compare x1 x2
end


module Int64_xdr_desc = struct
  let hash s p l = max_hash - Int64_xdr_asc.hash s p l

  let cmp s1 p1 l1 s2 p2 l2 =
    Int64_xdr_asc.cmp s2 p2 l2 s1 p1 l1
end


module Float_xdr_asc = struct
  let hash s p l =
    if l <> 8 then
      failwith "Mapred_sorters.Float_xdr_asc.hash: not 8 bytes";
    let fp8 =
      Netnumber.BE.read_fp8 s p in
    let x =
      Netnumber.float_of_fp8 fp8 in
    Float_asc.hashx x

  let hashcmp h1 h2 = h1 - h2

  let cmp s1 p1 l1 s2 p2 l2 =
    let x1 = Netnumber.float_of_fp8(Netnumber.BE.read_fp8 s1 p1) in
    let x2 = Netnumber.float_of_fp8(Netnumber.BE.read_fp8 s2 p2) in
    compare x1 x2
end


module Float_xdr_desc = struct
  let hash s p l = max_hash - Float_xdr_asc.hash s p l

  let cmp s1 p1 l1 s2 p2 l2 =
    Float_xdr_asc.cmp s2 p2 l2 s1 p1 l1
end


