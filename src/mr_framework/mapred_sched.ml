(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** A plan is a directed graph. We are building the graph here iteratively -
   not all information are available at the beginning.

   The vertices are either:
    - files 
    - tasks

   Edges go from files to tasks (i.e. the files are inputs of the task), 
   and from tasks to files (i.e. the files are outputs of the task).

   The files may get tags:
   - [`Input]: The file is an input file for the whole workflow
   - [`Map_out]: The file is the result of a map job
   - [`Sort_out]: The file is the result of a sort job
   - [`Emap_out]: the file is the result of an emap job
   - [`Shuffle_out]: The file is the result of a shuffle job
   - [`Output]: The file is an output file for the whole workflow

   We provide an index mapping tags to vertices.

   During the process of graph construction, vertexes may be added to or
   removed from these sets. Additions happen when more inputs become
   available, and removals when files get connected to tasks processing
   them.

   There also booleans: [input_complete], [map_out_complete], etc which
   are true when it is known that no more additions can occur.

 *)

open Mapred_tasks
open Plasma_rpcapi_aux
open Printf

let dlogf = Plasma_util.dlogf
let dlogr = Plasma_util.dlogr


type tag_type =
    [ `Input
    | `Map_out
    | `Sort_out
    | `Emap_out of int * int   (* pmin, pmax *)
    | `Shuffle_out of int * int * int  (* round, pmin, pmax *)
    | `Output
    ]

type tag =
    [ `Input of int
	(* arg: the ordinal number of the input file, 0..n-1 *)
    | `Map_out of int
	(* arg: the ordinal number of the map output file, 0..m-1 *)
    | `Sort_out of int
	(* arg: the ordinal number of the sort output file, 0..m-1 *)
    | `Emap_out of int * int * int
	(* arg (part_min, part_max, file_id) *)
    | `Shuffle_out of int * int * int * int * int
	(* arg (round, part_min, part_max, file_min, file_max) *)
    | `Output of int
	(* arg: partition *)
    ]


let type_of_tag =
  function
    | `Input _ -> `Input
    | `Map_out _ -> `Map_out
    | `Sort_out _ -> `Sort_out
    | `Emap_out(pmin,pmax,_) -> `Emap_out(pmin,pmax)
    | `Shuffle_out(round,pmin,pmax,_,_) -> `Shuffle_out(round,pmin,pmax)
    | `Output _ -> `Output

module Tag = struct
  type t = tag

  let rank_of_tt =
    function
      | `Input -> 0
      | `Map_out -> 1
      | `Sort_out -> 2
      | `Emap_out _ -> 3
      | `Shuffle_out _ -> 4
      | `Output -> 5

  let ( ||| ) x y =
    if x = 0 then y else x

  let compare_tt xt yt =
    match xt, yt with
      | `Shuffle_out(xround,xpmin,xpmax), `Shuffle_out(yround,ypmin,ypmax) -> 
	  (xround - yround) ||| (xpmin - ypmin) ||| (xpmax - ypmax)
      | `Emap_out(xpmin,xpmax), `Emap_out(ypmin,ypmax) ->
	  (xpmin - ypmin) ||| (xpmax - ypmax)
      | _ -> rank_of_tt xt - rank_of_tt yt

  let compare (x:t) (y:t) =
    match x, y with
      | `Input p, `Input q -> p - q
      | `Map_out p, `Map_out q -> p - q
      | `Sort_out p, `Sort_out q -> p - q
      | `Emap_out(pmin,pmax,p), `Emap_out(qmin,qmax,q) ->
	  (pmin - qmin) ||| (pmax - qmax) ||| (p - q)
      | `Shuffle_out (xround, xpmin, xpmax, xfmin, xfmax), 
	  `Shuffle_out (yround, ypmin, ypmax, yfmin, yfmax) ->
	  (xround - yround) ||| (xpmin - ypmin) ||| (xpmax - ypmax) |||
	      (xfmin - yfmin) ||| (xfmax - yfmax)
      | `Output p, `Output q -> p - q
      | _ ->
	  let xt = type_of_tag x in
	  let yt = type_of_tag y in
	  rank_of_tt xt - rank_of_tt yt
    
end


module TagSet = Set.Make(Tag)

module V = struct
  type t =
      [ `File of Mapred_tasks.file
      | `Task of Mapred_tasks.task
      ]
  let compare (x:t) (y:t) = 
    match x, y with
      | `File _, `Task _ -> (-1)
      | `Task _, `File _ -> 1
      | `File fx, `File fy -> Pervasives.compare fx fy
      | `Task tx, `Task ty -> Mapred_tasks.Ord.compare tx ty

  let equal (x:t) (y:t) =
    compare x y = 0

  let hash (x:t) = 
    match x with
      | `File f -> 
	  Hashtbl.hash f
      | `Task (`Map m) -> 
	  15001 + Hashtbl.hash m.map_id
      | `Task (`Emap m) -> 
	  30005 + Hashtbl.hash m.emap_map.map_id
      | `Task (`Sort m) -> 
	  60056 + Hashtbl.hash m.sort_id
      | `Task (`Shuffle m) -> 
	  90345 + Hashtbl.hash (m.shuffle_coverage, m.shuffle_partitions)
      | `Task `Cleanup ->
	  0

end


(** Graph module: *)
module G = Graph.Imperative.Digraph.ConcreteBidirectional(V)

module TaskSet = Set.Make(Mapred_tasks.Ord)


type plan_config =
    { keep_temp_files : bool;
      task_servers : string list;
      task_servers_ip : Unix.inet_addr list;
      jc : Mapred_def.mapred_job_config;
      mr_config : Mapred_config.mapred_config;
      planning_capacity : float;
      internal_suffix : string;
      output_suffix : string;
    }


type runinfo =
    { task_id : int;
      task_node : Unix.inet_addr;
      task_misalloc : bool
    }


type plan =
    { config : plan_config;
      fs : Mapred_fs.filesystem;
      bigblock_size : int;  (* the real one *)
      graph : G.t;
      tagidx : (tag, V.t list) Hashtbl.t;
        (** Maps tag to the vertexes *)
      vtxtag : (V.t, tag) Hashtbl.t;
        (** Maps vertex to tag *)
      set : (tag_type, TagSet.t) Hashtbl.t;
        (** The mentioned sets per [tag_type] *)
      complete : (tag_type, bool) Hashtbl.t;
        (** Whether the sets are complete *)
      max : (tag_type, tag) Hashtbl.t;
        (** The maximum tag per tag type, but only for consumed files *)
      map_tasks : (int, task) Hashtbl.t;
        (** Maps map_id to task (map/emap) *)
      pre_partitions_ht : (file_tag, int*int) Hashtbl.t;
        (** Pre-partitions for enhanced mapping. This hashtbl maps tags
	    to (pmin,pmax).
	 *)
      pre_partitions : (file_tag * int * int * locality) list;
        (** Same as list *)
      mutable plan_complete : bool;
        (** Whether the plan is fully complete *)
      mutable next_input_id : int;
      mutable next_map_out_id : int;
      mutable next_emap_out_id : (int*int, int) Hashtbl.t; 
      (* per prepartition *)
      mutable next_shuffle_id : int;
      mutable next_reduce_id : int;
      mutable vertex_added : bool;
        (** Used to determine whether the graph has been extended *)
      mutable runnable : TaskSet.t;
      mutable running : TaskSet.t;
      mutable finished : TaskSet.t;
      mutable period_start : float;   (* timestamp of measurement period *)
      mutable stat_running : float;
      mutable stat_runnable : float;
      mutable stat_period : float;
        (* so that stat_running/stat_period is the avg number of running tasks *)
      mutable missing_output : TaskSet.t;
        (** Map/emap tasks where we did not yet got all output files *)
      mutable runinfo : (task, runinfo) Hashtbl.t;
      delayed_map_outputs : (Unix.inet_addr, (unit->unit) list) Hashtbl.t;
        (** delayed really_add_map_output invocations, grouped by
	    hosts
	 *)
      mutable delayed_map_count : int;
      avg_part_width : (int, float) Hashtbl.t;

      loc_dn_assoc : (int*int, string) Hashtbl.t;
      loc_dn_avail : string Queue.t;
    }

(**********************************************************************)

(* debug *)

let string_of_tag_type =
  function
    | `Input -> "Input"
    | `Map_out -> "Map_out"
    | `Sort_out -> "Sort_out"
    | `Emap_out(pmin,pmax) ->
	sprintf "Emap_out(%d,%d)" pmin pmax
    | `Shuffle_out(round,pmin,pmax) ->
	sprintf "Shuffle_out(%d,%d,%d)" round pmin pmax
    | `Output -> "Output"

let string_of_tag =
  function
    | `Input n -> sprintf "Input(%d)" n
    | `Map_out(n) -> sprintf "Map_out(%d)" n
    | `Sort_out n -> sprintf "Sort_out(%d)" n
    | `Emap_out(pmin,pmax,n) ->
	sprintf "Emap_out(%d,%d,%d)" pmin pmax n
    | `Shuffle_out(round,pmin,pmax,fmin,fmax) ->
	sprintf "Shuffle_out(%d,%d,%d,%d,%d)" round pmin pmax fmin fmax
    | `Output p -> sprintf "Output(%d)" p


let string_of_file (f:file) =
  String.concat ","
    (List.map
       (fun (n,p,l) -> 
	  sprintf "%s@%Ld/%Ld" n p l
       )
       f
    )

(**********************************************************************)

(** {2 Determining runnable tasks} *)

let stat_save plan =
  let now = Unix.gettimeofday() in
  if plan.period_start > 0.0 then (
    let p = now -. plan.period_start in
    let n_running = TaskSet.cardinal plan.running in
    let n_runnable = TaskSet.cardinal plan.runnable in
    plan.stat_running <- 
      plan.stat_running +. (p *. float n_running);
    plan.stat_runnable <- 
      plan.stat_runnable +. (p *. float n_runnable);
    plan.stat_period <-
      plan.stat_period +. p;
  );
  plan.period_start <- now

let mark_as_started plan task ip run_id misalloc =
  stat_save plan;
  plan.runnable <- TaskSet.remove task plan.runnable;
  plan.running <- TaskSet.add task plan.running;
  plan.finished <- TaskSet.remove task plan.finished;
  let ri =
    { task_id = run_id;
      task_node = ip;
      task_misalloc = misalloc
    } in
  Hashtbl.replace plan.runinfo task ri


let debug_marking = true


let maybe_mark_as_runnable plan task =
  (* Netlog.logf `Info
    "maybe_mark_as_runnable task=%s" (string_of_task_id task);
   *)

  let l1 = G.pred plan.graph (`Task task) in
  let l12 = 
    List.flatten
      (List.map
	 (fun v -> 
	    let tpp = G.pred plan.graph v in
	    List.map (fun t -> (v,t)) tpp)
	 l1) in
  let l2 = List.map snd l12 in
  (* Netlog.logf `Info
    "maybe_mark_as_runnable #l2=%d" (List.length l2);
   *)
  let old_runnable = TaskSet.mem task plan.runnable in
  let finished = TaskSet.mem task plan.finished in
  let running = TaskSet.mem task plan.running in
  let runnable =
    not running &&
    not finished &&
      (List.for_all
	 (fun v ->
	    match v with
	      | `Task t ->
		  TaskSet.mem t plan.finished
	      | `File _ -> assert false
	 )
	 l2) in
  (* Netlog.logf `Info
    "maybe_mark_as_runnable runnable=%B" runnable;
   *)

  if debug_marking then (
    dlogf "  succ -> %s: was_runnable=%B new_runnable=%B finished=%B"
      (Mapred_tasks.string_of_task_id task) old_runnable runnable finished;
    List.iter
      (fun (file, task) ->
	 let s_file =
	   match file with
	     | `Task _ -> assert false
	     | `File f -> string_of_file f in
	 match task with
	   | `Task t ->
	       dlogf "    pred -> %s: finished=%B (via %s)"
		 (Mapred_tasks.string_of_task_id t) 
		 (TaskSet.mem t plan.finished)
		 s_file
	   | `File _ -> assert false
      )
      l12
  );

  if runnable then
    plan.runnable <- TaskSet.add task plan.runnable
  

let mark_as_finished plan task =
  stat_save plan;
  let again = TaskSet.mem task plan.finished in
  plan.runnable <- TaskSet.remove task plan.runnable;
  plan.running <- TaskSet.remove task plan.running;
  plan.finished <- TaskSet.add task plan.finished;
  if debug_marking then (
    dlogf "Mapred_sched.mark_as_finished (again=%B)" again;
    dlogf "%s:" (Mapred_tasks.string_of_task_id task);
    let is_reduce =
      match task with
	| `Shuffle t -> t.shuffle_reduce
	| _ -> false in
    if is_reduce then
      dlogf "  is a reduce task"
  );
  try
    let l1 = G.succ plan.graph (`Task task) in
    let l2 = List.flatten (List.map (fun v -> G.succ plan.graph v) l1) in
    (*Netlog.logf `Info
      "mark_as_finished task=%s #l2=%d"
      (string_of_task_id task) (List.length l2); *)
    List.iter
      (fun v ->
	 match v with
	   | `Task t ->
	       maybe_mark_as_runnable plan t
		 (* t is runnable if all predecessors are finished *)
	   | `File _ -> ()
      )
      l2
  with
    | Invalid_argument _ -> ()
    

let maybe_mark_as_finished_again plan task =
  (** If successors of [task] are later added, and [task] is already finished,
     we have to check again whether the successors are runnable.
   *)
  if TaskSet.mem task plan.finished then
    mark_as_finished plan task


let check_marks_for_new_task plan task =
  (** After adding [task], check whether there are predecessors that are
      now runnable
   *)
  let l1 = G.pred plan.graph (`Task task) in
  let l2 = List.flatten (List.map (fun v -> G.pred plan.graph v) l1) in
  List.iter
    (function
       | `Task t ->
	   maybe_mark_as_finished_again plan t
       | `File _ -> assert false
    )
    l2


let remove_marks plan task =
  stat_save plan;
  plan.runnable <- TaskSet.remove task plan.runnable;
  plan.running <- TaskSet.remove task plan.running;
  plan.finished <- TaskSet.remove task plan.finished;
  maybe_mark_as_runnable plan task


let plan_finished plan =
  plan.plan_complete &&
  plan.runnable = TaskSet.empty && 
  plan.running = TaskSet.empty (* && 
  plan.finished <> TaskSet.empty *)

let n_running plan =
  TaskSet.cardinal plan.running

let n_finished plan =
  TaskSet.cardinal plan.finished

let n_total plan =
  G.fold_vertex
    (fun v acc ->
       match v with
	 | `Task _ -> acc+1
	 | _ -> acc
    )
    plan.graph
    0

let round_points plan t =
  match t with
    | `Map _ | `Emap _ -> 0.0
    | `Sort _ -> 1000.0
    | `Shuffle sh -> 2000.0 +. 1000.0 *. float sh.shuffle_round
    | _ -> 0.0

let greediness_points plan t =
  (* First get s2 = all successor tasks of t *)
  let s1 = G.succ plan.graph (`Task t) in
  let s2 = List.flatten (List.map (fun v -> G.succ plan.graph v) s1) in
  (* For each member of s2 calculate the fraction r/n *)
  let fractions =
    List.map
      (fun u ->
	 (* Now get the predecessors of s2. Of course, [t] is among them *)
	 let p1 = G.pred plan.graph u in
	 let p2 = List.flatten (List.map (fun v -> G.pred plan.graph v) p1) in
	 let n = List.length p2 in
	 let p2_would_be_finished =
	   List.filter
	     (fun w ->
		let w_task =
		  match w with
		    | `Task wt -> wt
		    | _ -> assert false in
		(Mapred_tasks.Ord.compare w_task t = 0) ||
		  TaskSet.mem w_task plan.finished
	     )
	     p2 in
	 let r = List.length p2_would_be_finished in
	 if n=0 then 0.0 else (float r /. float n)
      )
      s2 in
  (* Sum up *)
  List.fold_left ( +. ) 0.0 fractions

(**********************************************************************)

let get_loc loc_dn_assoc loc_dn_avail (pmin,pmax) =
  try `Dn_identity(Hashtbl.find loc_dn_assoc (pmin,pmax))
  with Not_found ->
    try
      let id = Queue.take loc_dn_avail in
      Queue.push id loc_dn_avail;
      Hashtbl.add loc_dn_assoc (pmin,pmax) id;
      `Dn_identity id
    with
      | Queue.Empty ->
	  `Any

(**********************************************************************)

(** {2 Graph construction in general} *)

let get_set plan tt =
  try Hashtbl.find plan.set tt with Not_found -> TagSet.empty

let get_complete plan tt =
  try Hashtbl.find plan.complete tt with Not_found -> false

let find_vertex plan tag =
  (** Find a _single_ vertex by tag *)
  try
    match Hashtbl.find plan.tagidx tag with
      | [v] -> v
      | _ -> raise Not_found 
  with
    | Not_found ->
	assert false

let find_file_vertex plan tag =
  (** Find a single file vertex by tag *)
  match find_vertex plan tag with
    | `File file -> file
    | _ -> assert false
  


let add_task plan task =
  let v = `Task task in
  G.add_vertex plan.graph v;
  plan.vertex_added <- true;
  v



let add_file plan (f:file) (t:tag) =
  dlogr
    (fun () ->
       sprintf "add_file tag=%s file=%s"
	 (string_of_tag t)
	 (string_of_file f)
    );
  let v = (`File f) in
  G.add_vertex plan.graph v;
  Hashtbl.replace plan.vtxtag v t;
  Hashtbl.replace plan.tagidx t [v];
    (* right now the tags are unique per file *)
  let tt = type_of_tag t in
  let s = get_set plan tt in
  let s' = TagSet.add t s in
  Hashtbl.replace plan.set tt s';
  plan.vertex_added <- true;
  v

let add_edge plan (v1:V.t) (v2:V.t) =
  (** Add the edge, and update [set] *)
  G.add_edge plan.graph v1 v2;
  ( try
      let t = Hashtbl.find plan.vtxtag v1 in
      let tt = type_of_tag t in
      let s = get_set plan tt in
      let s' = TagSet.remove t s in
      Hashtbl.replace plan.set tt s';
      ( try
	  let m = Hashtbl.find plan.max tt in
	  if Tag.compare t m > 0 then raise Not_found
	with
	  | Not_found ->
	      Hashtbl.replace plan.max tt t
      );
    with
      | Not_found -> ()
  )


module DFS = Graph.Traverse.Dfs(G)

let print_plan out plan =
  DFS.prefix
    (fun v ->
       match v with
	 | `Task t ->
	     out # output_string "Task:\n";
	     Mapred_tasks.print_task out t 4;
	     out # output_string  "    Inputs:\n";
	     List.iter
	       (fun vp ->
		  match vp with
		    | `Task _ -> assert false
		    | `File f ->
			print_file out f 8
	       )
	       (G.pred plan.graph v);
	     out # output_string "    Outputs:\n";
	     List.iter
	       (fun vp ->
		  match vp with
		    | `Task _ -> assert false
		    | `File f ->
			print_file out f 8
	       )
	       (G.succ plan.graph v)
	     
	 | `File _ -> ()
    )
    plan.graph;

  Hashtbl.iter
    (fun t l ->
       out # output_string (sprintf "Tag %s:\n" (string_of_tag t));
       List.iter
	 (fun vp ->
	    match vp with
	      | `Task _ -> ()
	      | `File f ->
		  print_file out f 8
	 )
	 l
    )
    plan.tagidx;

  Hashtbl.iter
    (fun tt set ->
       out # output_string (sprintf "Tag type %s: set size = %d\n"
			      (string_of_tag_type tt)
			      (TagSet.cardinal set));
    )
    plan.set;

  Hashtbl.iter
    (fun tt flag ->
       out # output_string 
	 (sprintf "Tag type %s: complete = %B\n"
	    (string_of_tag_type tt)
	    flag)
    )
    plan.complete


(**********************************************************************)

(** {2 Adding [`Input] files to the plan} *)


let ranked_hosts dead bset =
  (** Get the list of hosts storing [bset] as list of pairs [(p,ident)]
      where [p] is the share (range 0 to 1), and [ident] is the datanode
      identity. The best node is returned first.
   *)
  let open Plasma_blocks in
  let ht = Hashtbl.create 5 in
  let n = ref 0L in
  Plasma_blocks.Bset.iter
    (fun _ br ->
       Plasma_blocks.StrMap.iter
	 (fun identity _ ->
	    let is_dead = Hashtbl.mem dead identity in
	    if not is_dead then (
	      let k = try Hashtbl.find ht identity with Not_found -> 0L in
	      Hashtbl.replace ht identity (Int64.add k br.br_length);
	      n := Int64.add !n br.br_length
	    )
	 )
	 br.br_datanodes
    )
    bset;
  let nn = Int64.to_float !n in
  let l = Hashtbl.fold (fun ident k acc -> (ident,k)::acc) ht [] in
  let l' = List.map (fun (ident,k) -> (Int64.to_float k /. nn, ident)) l in
  List.sort
    (fun (p1,_) (p2,_) -> Pervasives.compare p2 p1)
    l'


let add_inputs plan =
  (** For every host we determine the number of map tasks it should get. 

      We iterate over the files and the blocklists. Look for sequences
      that are on the same host. When we have identified such a sequence,
      add it to the map task list of this host. If this list is already
      full, take another host randomly.
   *)
  let pc = plan.config in
  let fs = plan.fs in

  let n_hosts = List.length pc.task_servers in

  let servertab = Hashtbl.create 5 in   (** all task servers *)
  List.iter
    (fun ip -> Hashtbl.replace servertab ip ())
    pc.task_servers_ip;

  let input_dir = plan.config.jc#input_dir in
  let blocksize = fs # blocksize input_dir in
  let blocksizeL = Int64.of_int blocksize in
  let prepend_input_dir l =
    List.map
      (fun (n,info) -> (input_dir ^ "/" ^ n, info))
      l in
  let files_ii0 = 
    match plan.config.jc#input_dir_designation with
      | `File ->
	  let info = Mapred_io.scan_file fs input_dir in
	  [ input_dir, info ]
      | `Flat_dir ->
	  prepend_input_dir(Mapred_io.scan_dir fs input_dir false)
      | `Deep_dir ->
	  prepend_input_dir(Mapred_io.scan_dir fs input_dir true) in
  let files_ii =
    List.filter
      (fun (_,info) ->
         match info with
           | `Regular 0L -> false
           | `Regular _ -> true
           | _ -> false
      )
      files_ii0 in
  let total_blocksL =
    List.fold_left
      (fun acc (_,info) ->
	 match info with
	   | `Regular eof ->
	       Int64.add
		 acc
		 (Int64.succ (Int64.div (Int64.pred eof) blocksizeL))
	   | _ ->
	       acc
      )
      0L
      files_ii in

  let map_tasks =
    if pc.jc#map_tasks = 0 then (
      if pc.jc#map_whole_files then
        List.length files_ii
      else
        let mt_for_max_load =
	  truncate
	    (float
	       (List.length pc.task_servers) *. pc.planning_capacity *. 1.5) in
        let bb_multiple =
	  plan.bigblock_size / blocksize in
        let mt_minimum =
	  max
	    (Int64.to_int
	       (Int64.div total_blocksL (Int64.of_int bb_multiple)))
	    1 in
        min mt_minimum mt_for_max_load
    )
    else
      pc.jc#map_tasks in

  dlogf
    "Mapred_sched.add_inputs: map_tasks=%d" map_tasks;

  let n_tasks_per_host = map_tasks / n_hosts + 1 in
  let n_blocks_per_taskL = 
    Int64.div total_blocksL (Int64.of_int map_tasks) in

  let tasks_per_host = Hashtbl.create 5 in
  (** number of tasks assigned to each host *)

  let cur_task = Hashtbl.create 5 in
  (** this is the current task: [(size, list)]. [size] in blocks.
      [list] is a list of tuples [(filename,pos,len)].
   *)

  let add_map_task best_hosts (f:file) =
    (** Adds the input file vertex for [f], plus the map task vertex.
	There is an edge from the input file to the task.
     *)
    let id = plan.next_input_id in
    plan.next_input_id <- id+1;
    let v_in_file = add_file plan f (`Input id) in
    let dir =
      if plan.config.jc#phases = `Map then
	plan.config.jc#output_dir
      else
	plan.config.jc#work_dir in
    let prefix =
      if plan.pre_partitions = [] || plan.config.jc#phases = `Map then
	"mapped" 
      else
	"sorted" in
    let suffix =
      if plan.config.jc#phases = `Map then
	plan.config.output_suffix
      else
	plan.config.internal_suffix in
    let m =
      { map_input = f;
	map_output_prefix = 
	  sprintf "%s/%s_%06d_" dir prefix id;
	map_output_suffix = suffix;
	map_id = id;
	map_best_hosts = best_hosts;
      } in
    let task =
      if plan.pre_partitions = [] then
	`Map m
      else
	`Emap { emap_map = m;
		emap_output = plan.pre_partitions
	      } in
    Hashtbl.replace plan.map_tasks id task;
    let v_task = add_task plan task in
    plan.runnable <- TaskSet.add task plan.runnable;
    plan.missing_output <- TaskSet.add task plan.missing_output;
    add_edge plan v_in_file v_task;
    (** The output files for this map task cannot yet be added, as they
	are not yet known
     *)
  in

  (** analyze in groups of bigblocks *)
  let sched_limit = plan.bigblock_size in
  let sched_limitL = Int64.of_int sched_limit in
  let sched_size = sched_limit / blocksize in
  let sched_sizeL = Int64.of_int sched_size in

  let dead = Hashtbl.create 5 in
  let ip_of_ident = Hashtbl.create 5 in

  List.iter
    (fun (filename,info) ->
       let eof =
	 match info with
	   | `Regular eof -> eof
	   | _ -> 0L in
       let blocklimit =
	 if eof=0L then 
	   0L
	 else
	   Int64.succ (Int64.div (Int64.pred eof) blocksizeL) in
       let n_groupsL = 
	 if eof = 0L then
	   0L
	 else
           if pc.jc#map_whole_files then
             1L
           else
	     Int64.succ (Int64.div (Int64.pred eof) sched_limitL) in
       if n_groupsL > Int64.of_int max_int then
	 failwith "Mapred_sched: input file has too many blocks";
       (* well, that limit is really high, on 32 bit systems:
	  max_int = 1G - 1, so the max size is roughly 1G * blocksize,
	  e.g. 1P for blocksize=1M. Also, you run out of memory way
	  before that!
	*)

       let blocks = fs # blocklist filename in
       List.iter
	 (fun b -> 
	    if not b.node_alive then Hashtbl.replace dead b.identity ();
	    if not (Hashtbl.mem ip_of_ident b.identity) then (
	      let (h,p) = Plasma_util.parse_host_port b.node in
	      let ip =
		try Unix.inet_addr_of_string h 
		with _ ->
		  failwith ("Mapred_sched.add_inputs: not an IP address: " 
			    ^ h) in
	      Hashtbl.replace ip_of_ident b.identity ip
	    )
	 )
	 blocks;
       let bset = Plasma_blocks.bset_of_blocklist blocks in
       
       let n_groups = Int64.to_int n_groupsL in
       for g = 0 to n_groups - 1 do
	 let pos0 = Int64.mul (Int64.of_int g) sched_sizeL in
	 let pos1 = 
           if pc.jc#map_whole_files then
             blocklimit
           else
             min (Int64.add pos0 sched_sizeL) blocklimit in
	 let lenL = Int64.sub pos1 pos0 in
	 let group =
	   Plasma_blocks.Bset.sub (pos0, Int64.pred pos1) bset in
	 (** Get a ranking of the nodes (note that nodes0 may also
	     include nodes that are no taskservers):
	  *)
	 let nodes0 = ranked_hosts dead group in
	 (** nodes0 may be empty for two reasons:
	    - all nodes dead
	    - a hole in the file
	    We check here only for the first. Holes will also be rejected
	    by the record reader, and it is much simpler to find out there.
	  *)
	 if nodes0 = [] then (
	   if group <> Plasma_blocks.Bset.empty then
	     failwith ("File inaccessible (no live data nodes): " ^
			 filename)
	 )
	 else (
	   let nodes = 
	     List.map (fun (p,ident) -> (p,ident,false)) nodes0 @
	       [ let (p,ident) = List.hd nodes0 in (p,ident,true) ] in
	   (** Try to assign the task: *)
	   ( try
	       (** iterate over nodes. Skip nodes when tasks_per_host is
		  >= n_tasks_per_hosts. Otherwise add the task to cur_task.
		  If cur_task is >= blocks_per_task, create the graph node.
		*)
	       List.iter
		 (fun (p,ident,is_last) ->
		    let ip = 
		      try Hashtbl.find ip_of_ident ident 
		      with Not_found -> assert false in
		    let n = 
		      try Hashtbl.find tasks_per_host ip 
		      with Not_found -> 0 in
		    if n < n_tasks_per_host || is_last then (
		      let ct_n, ct_l =
			try Hashtbl.find cur_task ip 
			with Not_found -> (0L, []) in
		      let ct_n' = Int64.add ct_n lenL in
		      let ct_l' = (filename,pos0,lenL) :: ct_l in
		      if ct_n' >= n_blocks_per_taskL then (
			Hashtbl.remove cur_task ip;
			let best_hosts =
			  if Hashtbl.mem servertab ip then [ip] else [] in
			add_map_task best_hosts (List.rev ct_l');
			Hashtbl.replace tasks_per_host ip (n+1)
		      )
		      else
			Hashtbl.replace cur_task ip (ct_n', ct_l');
		      raise Exit
		    )
		 )
		 nodes
	     with
	       | Exit -> ()
	   )
	 );
       done
       
    )
    files_ii;

  (* Special case empty input directory *)
  if files_ii = [] then
    add_map_task [] [];   (* A map task without input files... *)

  (** Create the remaining tasks from [cur_task]: *)
  Hashtbl.iter
    (fun ip (ct_n, ct_l) ->
       let best_hosts =
	 if Hashtbl.mem servertab ip then [ip] else [] in
       add_map_task best_hosts (List.rev ct_l);
    )
    cur_task

(**********************************************************************)

(** {2 Adding sort tasks} *)

let try_add_sort_tasks plan =
  (** If there is a [`Map_out] tag in [set], we can create a sort 
      task for it. Also check whether we have all sort tasks.
   *)
  assert(plan.pre_partitions = []); 
    (* sort tasks are only added if we do not create emap tasks *)
  let s = get_set plan `Map_out in
  let is_complete = get_complete plan `Map_out in
 
  TagSet.iter
    (fun tag ->
       let id =
	 match tag with
	   | `Map_out id -> id
	   | _ -> assert false in
       
       let map_out = find_file_vertex plan tag in
       let v_map_out = `File map_out in

       let dir =
	 if plan.config.jc#phases = `Map_sort then
	   plan.config.jc#output_dir
	 else
	   plan.config.jc#work_dir in
       let suffix =
	 if plan.config.jc#phases = `Map_sort then
	   plan.config.output_suffix
	 else
	   plan.config.internal_suffix in
       let sort_out_name =
	 sprintf "%s/sorted_%06d%s" dir id suffix in
       let st =
	 { sort_input = map_out;
	   sort_input_del = not plan.config.keep_temp_files;
	   sort_output = sort_out_name;
	   sort_output_locality = `Any;
	   sort_id = id
	 } in
       let sort_out = [ sort_out_name, 0L, 0L ] in
       let v_sort = add_task plan (`Sort st) in
       let v_sort_out = add_file plan sort_out (`Sort_out id) in
       add_edge plan v_map_out v_sort;
       add_edge plan v_sort v_sort_out;
       check_marks_for_new_task plan (`Sort st)
    )
    s;

  if is_complete then (
    Hashtbl.replace plan.complete `Sort_out true;
    if plan.config.jc#phases = `Map_sort then
      plan.plan_complete <- true
  )

(**********************************************************************)

(** {2 Adding shuffle/reduce tasks} *)


let split_range n pmin pmax =
  (** splits the range of numbers pmin...pmax into n groups
     (g1min, g1max), (g2min, g2max), ..., (gnmin,gnmax)
     so that g(k)max = g(k+1)min, and the groups have approx the same size
     e.g.
     # split_range 4 0 10;;
     - : (int * int) list = [(0, 2); (3, 5); (6, 8); (9, 10)]
   *)
  let r = pmax-pmin+1 in   (* number of elements in total *)
  let n = min n r in
  let gsize = r/n in
  let gmod = r mod n in
  let l = ref [] in
  let p = ref pmin in
  for k = 0 to n-1 do
    let size = gsize + (if k < gmod then 1 else 0) in
    l := (!p, !p+size-1) :: !l;
    p := !p + size;
  done;
  List.rev !l


let new_shuffle_out_filename plan =
  let id = plan.next_shuffle_id in
  plan.next_shuffle_id <- id+1;
  let suffix = plan.config.internal_suffix in
  sprintf "%s/shuffle_%09d%s" plan.config.jc#work_dir id suffix


let log n =
  let rec mul k p =
    if p >= n then
      k
    else
      mul (k+1) (p*10) in
  mul 0 1


let new_reduce_out_filename plan p =
(*
  let id = plan.next_reduce_id in
  plan.next_reduce_id <- id+1;
 *)
  let width = log plan.config.jc#partitions in
  let suffix = plan.config.output_suffix in
  sprintf "%s/partition_%0*d%s" plan.config.jc#output_dir width p suffix


let rec get_avg_part_width plan round =
  if not (Hashtbl.mem plan.avg_part_width round) then (
    if round=0 then (
      if plan.pre_partitions = [] then (
	let p = plan.config.jc # partitions in
	Hashtbl.add plan.avg_part_width round (float p);
	float p
      )
      else (
	let p1 = plan.config.jc # partitions in
	let p2 = (float p1) /. (float (List.length plan.pre_partitions)) in
	Hashtbl.add plan.avg_part_width round p2;
	p2
      )
    )
    else (
      let p = get_avg_part_width plan (round-1) in
      let n_split = plan.config.jc # split_limit in
      let p' = p /. (float n_split) in
      Hashtbl.add plan.avg_part_width round p';
      p'
    )
  )
  else
    Hashtbl.find plan.avg_part_width round


let try_add_first_round_shuffle_tasks plan =
  (** Shuffle tasks of the first round take the output of sort tasks.

      We can do something here when we have the next [n_merge] sort output
      files.

      This function is only meaningful if we do not use emap tasks.
   *)
  assert(plan.pre_partitions = []);

  let n_merge = plan.config.jc # merge_limit in
  let n_split = plan.config.jc # split_limit in

  let s = get_set plan `Sort_out in
  let is_complete = get_complete plan `Sort_out in

  let pmin = 0 in
  let pmax = plan.config.jc # partitions - 1 in

  let id_of_sort_tag =
    function
      | `Sort_out id -> id
      | _ -> assert false in

  let rec collect id id_max =
    if id > id_max then
      []
    else
      let tag = (`Sort_out id) in
      if TagSet.mem tag s then
	tag :: collect (id+1) id_max
      else
	if is_complete then 
	  []
	else
	  raise Not_found
  in

  if s <> TagSet.empty then (
    let tag0 = TagSet.min_elt s in
    let id0 = id_of_sort_tag tag0 in
    (** We can only go on if [tag0] is really the first tag we need, i.e.
	the immediate predecessor (if any) is already processed.
     *)
    if id0=0 || Hashtbl.mem plan.tagidx (`Sort_out (id0-1)) then (
      (** Now check whether we even have the first [n_merge] tags *)
      try
	let tags = collect id0 (id0 + n_merge - 1) in
	  (** [collect] may raise [Not_found] *)
	let id_max = id_of_sort_tag (List.hd (List.rev tags)) in
	
	let shuffle_input =
	  List.map
	    (fun tag ->
	       let id = id_of_sort_tag tag in
	       let sort_out = find_file_vertex plan tag in
	       (sort_out, id, id)
	    )
	    tags in
	let output_parts = split_range n_split pmin pmax in
	let shuffle_output =
	  List.map
	    (fun (qmin,qmax) ->
	       let name = new_shuffle_out_filename plan in
	       let loc = 
		 get_loc plan.loc_dn_assoc plan.loc_dn_avail (qmin,qmax) in
	       (name, qmin, qmax, loc)
	    )
	    output_parts in
	
	let t =
	  { shuffle_input = shuffle_input;
	    shuffle_input_del = not plan.config.keep_temp_files;
	    shuffle_output = shuffle_output;
	    shuffle_partitions = (pmin,pmax);
	    shuffle_coverage = (id0,id_max);
	    shuffle_reduce = false;  (* never in the first round *)
	    shuffle_round = 0;
	    shuffle_avg_part_width = get_avg_part_width plan 0;
	  } in

	let v_task = add_task plan (`Shuffle t) in
	List.iter
	  (fun tag ->
	     let v_sort_out = `File(find_file_vertex plan tag) in
	     add_edge plan v_sort_out v_task
	  )
	  tags;
	List.iter
	  (fun (name,qmin,qmax,_) ->
	     let file = [name, 0L, 0L] in
	     let tag = `Shuffle_out(0, qmin, qmax, id0, id_max) in
	     let v_shuffle_out = add_file plan file tag in
	     add_edge plan v_task v_shuffle_out
	  )
	  shuffle_output;
       check_marks_for_new_task plan (`Shuffle t);

       let s' = get_set plan `Sort_out in
       if is_complete && s' = TagSet.empty then
	 List.iter
	   (fun (qmin,qmax) ->
	      Hashtbl.replace plan.complete (`Shuffle_out(0,qmin,qmax)) true
	   )
	   output_parts

      with Not_found -> ()       (* from [collect] *)
    );

  )

  
let try_add_these_shuffle_tasks plan round pmin pmax =
  (** Shuffle tasks of the following rounds take the output of previous
      shuffle tasks.

      round, pmin, pmax: these values identify the previous round!
      round=(-1) means that the previous round was an emap round.
   *)

  let n_merge = plan.config.jc # merge_limit in
  let n_split = plan.config.jc # split_limit in  

  let tt =
    if round >= 0 then
      `Shuffle_out(round,pmin,pmax) 
    else (
      assert(plan.pre_partitions <> []);
      `Emap_out(pmin,pmax)
    ) in
  let s = get_set plan tt in
  let is_complete = get_complete plan tt in

  let input_is_complete = get_complete plan `Map_out in
  (* certain tests work only if input_is_complete *)

  let ids_of_shuffle_tag =
    function
      | `Shuffle_out(_,_,_,fmin,fmax) -> (fmin,fmax)
      | `Emap_out(_,_,n) -> (n,n)
      | _ -> assert false in

  let rec collect fmin_exp u n =
    if n = 0 then
      []
    else
      let tag_opt = try Some(TagSet.min_elt u) with Not_found -> None in
      match tag_opt with
	| Some tag ->
	    let (fmin_next, fmax_next) = ids_of_shuffle_tag tag in
	    if fmin_next = fmin_exp then
	      tag :: collect (fmax_next+1) (TagSet.remove tag u) (n-1)
	    else
	      raise Not_found
	| None ->
	    if is_complete then 
	      []
	    else
	      raise Not_found
  in

  dlogf
    "try_add_these_shuffle_tasks round=%d pmin=%d pmax=%d is_compl=%B #s=%d"
    round pmin pmax is_complete (TagSet.cardinal s);

  if s <> TagSet.empty then (
    let tag0 = TagSet.min_elt s in
    let (fmin0,fmax0) = ids_of_shuffle_tag tag0 in
    (** We can only go on if [tag0] is really the first tag we need, i.e.
	the immediate predecessor (if any) is already processed.
     *)
    let pred_mem = (
      try
	let m = Hashtbl.find plan.max tt in
	let (fminm, fmaxm) = ids_of_shuffle_tag m in
	fmaxm+1 = fmin0
      with Not_found -> false
    ) in
    dlogf "fmin0=%d pred_mem=%B" fmin0 pred_mem;
    if fmin0=0 || pred_mem then (
      (** Now check whether we even have the first [n_merge] tags *)
      try
	let tags = collect fmin0 s n_merge in
	  (** [collect] may raise [Not_found] *)

	let (_,fmaxx) = ids_of_shuffle_tag (List.hd (List.rev tags)) in
	
	dlogf "#tags=%d fmaxx=%d" (List.length tags) fmaxx;

	let next_map_out_id =
	  if plan.pre_partitions = [] then
	    plan.next_map_out_id
	  else
	    Hashtbl.fold
	      (fun (rmin,rmax) id acc ->
		 if pmin >= rmin && pmax <= rmax then
		   id
		 else
		   acc
	      )
	      plan.next_emap_out_id
	      (-1) in
	assert(next_map_out_id >= 0);

	let encompasses_all_files = 
	  fmin0 = 0 && fmaxx+1 = next_map_out_id in
	(* if not input_is_complete, plan.next_map_out_id may not be the
	   final value, as further outputs can still be added. If we
	   nevertheless find the no_merges_in_next_round condition
	   evaluates to "true", we have to stop adding tasks here,
	   as we cannot recognize reduce tasks anymore.
	 *)
	(* CHECK
	if encompasses_all_files && not input_is_complete then
	  raise Not_found;
	 *)
	let no_merges_in_next_round =
	  encompasses_all_files && input_is_complete in
	(* reasoning: if not input_is_complete then next_map_out_id will
	   be increased (by the next map task), and thus we will have
	   merges
	 *)

	(* We can set [n_split_extra] only > 0 if there will be no merges in
	   the next round. Otherwise this would break merges of the next
	   round. (The way of splitting must be predictable as long as
	   merges can occur.)
	 *)
	let n_split_extra =
	  if no_merges_in_next_round then
	    n_merge
	  else 0 in

	let shuffle_input =
	  List.map
	    (fun tag ->
	       let (fmin,fmax) = ids_of_shuffle_tag tag in
	       let file = find_file_vertex plan tag in
	       (file, fmin, fmax)
	    )
	    tags in

	let output_parts = split_range (n_split+n_split_extra) pmin pmax in


	(* Old:
	   let is_reduce = (pmin = pmax) && no_merges_in_next_round
	 *)
	let is_reduce = 
	  no_merges_in_next_round &&
	  List.length output_parts = 1 &&
	    ( List.for_all
		(fun (qmin,qmax) -> qmin=qmax)
		output_parts
	    )  in
	
	let shuffle_output =
	  if is_reduce then
	    [ new_reduce_out_filename plan pmin, pmin, pmax, `Any ]
	  else
	    List.map
	      (fun (qmin,qmax) ->
		 let name = new_shuffle_out_filename plan in
		 (name, qmin, qmax, `Any)
	      )
	      output_parts in

	let t =
	  { shuffle_input = shuffle_input;
	    shuffle_input_del = not plan.config.keep_temp_files;
	    shuffle_output = shuffle_output;
	    shuffle_partitions = (pmin,pmax);
	    shuffle_coverage = (fmin0,fmaxx);
	    shuffle_reduce = is_reduce;
	    shuffle_round = round+1;
	    shuffle_avg_part_width = get_avg_part_width plan (round+1);
	  } in

	let v_task = add_task plan (`Shuffle t) in
	List.iter
	  (fun tag ->
	     let v_file = `File(find_file_vertex plan tag) in
	     add_edge plan v_file v_task
	  )
	  tags;
	List.iter
	  (fun (name,qmin,qmax,_) ->
	     let file = [name, 0L, 0L] in
	     let tag = 
	       if is_reduce then 
		 `Output qmin
	       else
		 `Shuffle_out(round+1, qmin, qmax, fmin0, fmaxx) in
	     let v_shuffle_out = add_file plan file tag in
	     add_edge plan v_task v_shuffle_out
	  )
	  shuffle_output;
       check_marks_for_new_task plan (`Shuffle t);

       if is_complete then (
	 let s' = get_set plan tt in
	 dlogf "complete check: #s'=%d" (TagSet.cardinal s');
	 if s' = TagSet.empty then
	   List.iter
	     (fun (qmin,qmax) ->
		Hashtbl.replace 
		  plan.complete (`Shuffle_out(round+1,qmin,qmax)) true
	     )
	     output_parts;
       );

       dlogf "try_add_these_shuffle_tasks return"

      with Not_found ->  (* from [collect] *)
	dlogf "try_add_these_shuffle_tasks not_found";
	assert(not is_complete);
	()        
    );

  )


let card_one s =
  (** quickly determine whether the cardinality of s is 1 *)
  let n = ref 0 in
  ( try
      TagSet.iter (fun e -> incr n; if !n > 1 then raise Exit) s
    with Exit -> ()
  );
  !n = 1


let try_add_shuffle_tasks plan =
  Hashtbl.iter
    (fun tt set ->
       match tt with
	 | `Emap_out(pmin,pmax) ->
	     try_add_these_shuffle_tasks plan (-1) pmin pmax
	 | `Shuffle_out(round,pmin,pmax) ->
	     try_add_these_shuffle_tasks plan round pmin pmax
	 | `Output ->
	     (* FIXME: [cardinal] is O(n) *)
	     if TagSet.cardinal set = plan.config.jc # partitions then
	       plan.plan_complete <- true
	 | _ ->
	     ()
    )
    plan.set

(**********************************************************************)

(** {2 Adding any task} *)

let add_tasks plan =
  let phases = plan.config.jc#phases in
  let cont = ref true in
  while !cont do
    plan.vertex_added <- false;
    if plan.pre_partitions = [] then (
      if phases = `Map_sort || phases = `Map_sort_reduce then
	try_add_sort_tasks plan;
      if phases = `Map_sort_reduce then
	try_add_first_round_shuffle_tasks plan;
    );
    if phases = `Map_sort_reduce then
      try_add_shuffle_tasks plan;
    cont := plan.vertex_added
  done;

  if get_complete plan `Map_out && not plan.plan_complete then (
    (* This is an error condition *)
    Netlog.logf `Err 
      "Plan is fully computed but not marked as complete! \
       (diagnostics to stderr)";
    let ech = new Netchannels.output_channel stderr in
    ech # output_string "PLAN SO FAR:\n";
    print_plan ech plan;
    ech # output_string
      (sprintf "next_reduce_id = %d\n" plan.next_reduce_id);
    ech # output_string
      (sprintf "#missing_output = %d\n" (TaskSet.cardinal plan.missing_output));
    ech # flush()
  )


let complete_map_outputs plan =
  let phases = plan.config.jc#phases in
  Hashtbl.replace plan.complete `Map_out true; (* even if we do not use map *)
  List.iter
    (fun (_,pmin,pmax,_) ->
       Hashtbl.replace plan.complete (`Emap_out(pmin,pmax)) true;
    )
    plan.pre_partitions;
  if phases = `Map || plan.next_input_id = 0 then
    plan.plan_complete <- true


let complete_inputs plan =
  (** Mark the inputs as complete *)
  Hashtbl.replace plan.complete `Input true;
  if plan.missing_output = TaskSet.empty then (
    complete_map_outputs plan;
    add_tasks plan
  )


let really_add_map_output plan (id:int) (files:(file_tag * file) list) =
  dlogf "add_map_output id=%d files=[%s]"
    id
    (String.concat "; "
       (List.map
	  (fun (`Tag tag, file) ->
	     tag ^  ":" ^  string_of_file file
	  )
	  files
       )
    );

  let task =
    try Hashtbl.find plan.map_tasks id
    with Not_found ->
      failwith ("Mapred_sched.add_map_output: map id not found: " ^
		  string_of_int id) in
  let v_task = `Task task in

  if plan.pre_partitions = [] then (
    (* classic map tasks *)
    List.iter
      (fun (_,file) ->
	 let id = plan.next_map_out_id in
	 plan.next_map_out_id <- id+1;
	 let t = `Map_out id in
	 let v = add_file plan file t in
	 add_edge plan v_task v
      )
      files;
  )
  else (
    (* emap *)
    let by_tag = Hashtbl.create 18 in
    List.iter
      (fun (tag,file) ->
	 let l =
	   try Hashtbl.find by_tag tag
	   with Not_found -> [] in
	 Hashtbl.replace by_tag tag (file :: l)
      )
      files;
    Hashtbl.iter
      (fun tag tag_files ->
	 let `Tag str_tag = tag in
	 let tag_files = List.rev tag_files in
	 let (pmin,pmax) =
	   try Hashtbl.find plan.pre_partitions_ht tag
	   with Not_found ->
	     failwith ("Mapred_sched.add_map_output: tag not found: " ^
			 str_tag) in
	 List.iter
	   (fun file ->
	      let id = 
		try Hashtbl.find plan.next_emap_out_id (pmin,pmax)
		with Not_found -> assert false in
	      Hashtbl.replace plan.next_emap_out_id (pmin,pmax) (id+1);
	      let t = `Emap_out(pmin,pmax,id) in
	      let v = add_file plan file t in
	      add_edge plan v_task v;
	   )
	   tag_files;
      )
      by_tag;
  );
  plan.missing_output <- TaskSet.remove task plan.missing_output;
  let input_complete =
    try Hashtbl.find plan.complete `Input with Not_found -> false in
  if input_complete && plan.missing_output = TaskSet.empty then
    complete_map_outputs plan;
  add_tasks plan


let add_map_output plan (id:int) (files:(file_tag * file) list) ip =
  really_add_map_output plan id files

(* The following block replaces the call of really_add_map_output.
   It is disabled because there seems to be no effect according to
   tests. It only complicates things :-(
 *)

(*
  (* If we called really_map_output immediately, we would construct
     shuffle tasks (round 0) that would have to read inputs from several hosts.
     By delaying this construction, we can ensure that better
     shuffles can be created
   *)
  let l =
    try Hashtbl.find plan.delayed_map_outputs ip
    with Not_found -> [] in
  let rmo () =
    really_add_map_output plan id files in
  let l' = rmo :: l in
  Hashtbl.replace plan.delayed_map_outputs ip l';
  plan.delayed_map_count <- plan.delayed_map_count + 1;

  let input_complete =
    try Hashtbl.find plan.complete `Input with Not_found -> false in
  let map_complete =
    input_complete &&
      plan.delayed_map_count = TaskSet.cardinal plan.missing_output in
  let n_merge = plan.config.jc # merge_limit in
  if map_complete then (
    Hashtbl.iter
      (fun _ rmo_l -> 
	 List.iter (fun f -> f()) (List.rev rmo_l)
      )
      plan.delayed_map_outputs;
    Hashtbl.clear plan.delayed_map_outputs;
    plan.delayed_map_count <- 0;
  )
  else if List.length l' = n_merge then (
    List.iter (fun f -> f()) (List.rev l');
    Hashtbl.replace plan.delayed_map_outputs ip [];
    plan.delayed_map_count <- plan.delayed_map_count - n_merge
  )

 *)

(**********************************************************************)

(** {2 Interface} *)

let configure_plan ?(keep_temp_files=false) ~planning_capacity
                   ~internal_suffix ~output_suffix
                   jc mr_config =
  if jc#bigblock_size <= 0 then
    invalid_arg "Mapred_sched.configure_plan: bad bigblock_size";
  let servers = mr_config # mr_task_nodes in
  let ips =
    List.map Plasma_util.ip_of_host servers in
  { keep_temp_files = keep_temp_files;
    internal_suffix;
    output_suffix;
    task_servers = servers;
    task_servers_ip = ips;
    jc = jc;
    mr_config = mr_config;
    planning_capacity;
  }


let create_plan ?(dn_identities=[]) fs pc =
  let input_dir = pc.jc#input_dir in
  let blocksize = fs # blocksize input_dir in
  let bigblock_size = 
    ((pc.jc#bigblock_size - 1) / blocksize + 1) * blocksize in
  let loc_dn_assoc = Hashtbl.create 17 in
  let loc_dn_avail = Queue.create() in
  List.iter (fun id -> Queue.add id loc_dn_avail) dn_identities;
  let ppmax =
    min pc.jc#partitions pc.mr_config#mr_pre_partitions_max in
  let n_pre_partitions =
    min ppmax pc.jc#enhanced_mapping in
  if n_pre_partitions <> pc.jc#enhanced_mapping then
    Netlog.logf `Warning
      "Limiting number of pre-partitions to %d"
      ppmax;
  let pre_partitions =
    if n_pre_partitions = 0 then
      []
    else
      List.map
	(fun (pmin,pmax) ->
	   let file_tag = sprintf "p%d-%d" pmin pmax in
	   let loc = get_loc loc_dn_assoc loc_dn_avail (pmin,pmax) in
	   (`Tag file_tag,pmin,pmax,loc)
	)
	(split_range n_pre_partitions 0 (pc.jc#partitions-1)) in
  let pre_partitions_ht = Hashtbl.create 59 in
  List.iter
    (fun (file_tag,pmin,pmax,_) ->
       Hashtbl.replace pre_partitions_ht file_tag (pmin,pmax)
    )
    pre_partitions;
  let next_emap_out_id = Hashtbl.create 17 in
  List.iter
    (fun (_,pmin,pmax,_) ->
       Hashtbl.replace next_emap_out_id (pmin,pmax) 0
    )
    pre_partitions;

  { config = pc;
    fs = fs;
    bigblock_size = bigblock_size;
    graph = G.create ~size:1000 ();
    tagidx = Hashtbl.create 1000;
    vtxtag = Hashtbl.create 50;
    set = Hashtbl.create 50;
    complete = Hashtbl.create 50;
    max = Hashtbl.create 50;
    map_tasks = Hashtbl.create 50;
    pre_partitions = pre_partitions;
    pre_partitions_ht = pre_partitions_ht;
    plan_complete = false;
    next_input_id = 0;
    next_map_out_id = 0;
    next_emap_out_id;
    next_shuffle_id = 0;
    next_reduce_id = 0;
    vertex_added = false;
    runnable = TaskSet.empty;
    running = TaskSet.empty;
    finished = TaskSet.empty;
    period_start = 0.0;
    stat_running = 0.0;
    stat_runnable = 0.0;
    stat_period = 0.0;
    missing_output = TaskSet.empty;
    runinfo = Hashtbl.create 50;
    delayed_map_outputs = Hashtbl.create 37;
    delayed_map_count = 0;
    avg_part_width = Hashtbl.create 7;
    loc_dn_assoc;
    loc_dn_avail;
  }

let bigblock_size plan =
  plan.bigblock_size

let executable_tasks plan =
  TaskSet.elements plan.runnable


let plan_complete plan =
  plan.plan_complete

let hosts plan =
  List.map2
    (fun n ip -> (n,ip))
    plan.config.task_servers
    plan.config.task_servers_ip

let task_depends_on_list plan task =
  let l =
    List.flatten
      (List.map (G.pred plan.graph) (G.pred plan.graph (`Task task))) in
  let l' =
    List.map
      (function
	 | `Task t -> t
	 | _ -> assert false
      )
      l in
  let s = List.fold_left (fun acc t -> TaskSet.add t acc) TaskSet.empty l' in
  TaskSet.elements s
  


let avg_running plan =
  plan.stat_running /. plan.stat_period

let avg_runnable plan =
  plan.stat_runnable /. plan.stat_period

let avg_runqueue plan =
  (plan.stat_runnable +. plan.stat_running) /. plan.stat_period


let task_stats plan t =
  let p =
    G.pred plan.graph (`Task t) in
  let ri = Hashtbl.find plan.runinfo t in
  let p_non_local =
    try
      List.filter
	(fun v ->
	   let pp = G.pred plan.graph v in
	   match pp with
	     | [ `Task t_p ] ->
		 let ri_p = Hashtbl.find plan.runinfo t_p in
		 ri.task_node <> ri_p.task_node
	     | _ -> raise Not_found
	)
	p 
    with Not_found -> [] in

  (List.length p,
   List.length p_non_local
  )


(**********************************************************************)
(* Visualization                                                      *)
(**********************************************************************)

let structure_violation n =
  failwith ("Mapred_sched.generate_svg: structure violation [" ^ 
	      string_of_int n ^ "]")


let tags_with plan cond =
  Hashtbl.fold
    (fun tag _ acc ->
       if cond tag then
	 TagSet.add tag acc
       else
	 acc
    )
    plan.tagidx
    TagSet.empty


let files_with plan cond =
  let tags = tags_with plan cond in
  List.flatten
    (List.map
       (fun tag ->
	  let files =
	    try Hashtbl.find plan.tagidx tag with Not_found -> [] in
	  List.map
	    (fun file -> (tag,file))
	    files
       )
       (TagSet.elements tags)
    )


let generate_svg plan =
  (* We proceed in these steps:
     1. Position the objects representing files
     2. Position the objects representing tasks
     3. Connect with edges
     4. Output as XML, also apply attributes
   *)

  let width = 1.0 in
  let row_height = 0.05 in

  let svg_list = ref [] in
  let pos_of_vertex = Hashtbl.create 79 in

  (* 1. *)
  let row = ref 0.0 in
  let factor = ref 0 in

  let position_files cond descr_of =
    let files = 
      files_with plan cond in
    let n_files = List.length files in
    if n_files > 0 then (
      factor := max !factor n_files;
      let p = ref 0 in
      List.iter
	(fun (tag,file) ->
	   let x =
	     (float !p) *. (width /. float (n_files-1)) in
	   let item =
	     `Rect(x, !row, descr_of !p tag file, file) in
	   svg_list := item :: !svg_list;
	   Hashtbl.replace pos_of_vertex file (x,!row);
	   incr p
	)
	files;
      row := !row +. 2. *. row_height
    ) in
  
  (* 1.1: Position input files *)
  position_files
    (function `Input _ -> true | _ -> false)
    (fun id tag file -> "m" ^ string_of_int id);

  (* 1.2: Position Map_out files *)
  position_files 
    (function `Map_out _ -> true | _ -> false)
    (fun id tag file -> "s" ^ string_of_int id);

  (* 1.3: Position Sort_out files *)
  position_files
    (function `Sort_out _ -> true | _ -> false)
    (fun id tag file -> "s" ^ string_of_int id);

  (* 1.4: Position Emap_out files *)
  position_files
    (function `Emap_out(_,_,_) -> true | _ -> false)
    (fun id tag file ->
       match tag with
	 | `Emap_out(pmin,pmax,s) ->
	     sprintf "s%d/p%d-%d" s pmin pmax
	 | _ ->
	     assert false
    );

  (* 1.5: Position Shuffle_out files *)
  let round = ref 0 in
  let found = ref true in
  while !found do
    found := false;
    position_files
      (function `Shuffle_out(r,_,_,_,_) -> r = !round | _ -> false)
      (fun id tag file ->
	 found := true;
	 match tag with
	   | `Shuffle_out(_,pmin,pmax,smin,smax) ->
	       sprintf "s%d-%d/p%d-%d" smin smax pmin pmax
	   | _ ->
	       assert false
      );
    incr round
  done;

  (* 1.6: Position output files *)
  position_files
    (function `Output _ -> true | _ -> false)
    (fun id tag file ->
       sprintf "p%d" id
    );

  (* 2. Position tasks: Tasks are drawn in the rows following the processed
     files. The x pos is simply the average of all inputs
   *)
  
  G.iter_vertex
    (fun v ->
       match v with
	 | `Task t ->
	     let ri_opt =
	       try Some(Hashtbl.find plan.runinfo t)
	       with Not_found -> None in
	     let l = G.pred plan.graph v in
	     let row =
	       List.fold_left
		 (fun acc v' ->
		    let (_,y) = 
		      try Hashtbl.find pos_of_vertex v'
		      with Not_found -> (0.0,0.0) in
		    max acc y
		 )
		 0.0
		 l +. row_height in
	     let x_sum =
	       List.fold_left
		 (fun acc v' ->
		    let (x,_) = 
		      try Hashtbl.find pos_of_vertex v'
		      with Not_found -> (0.0,0.0) in
		    x +. acc
		 )
		 0.0
		 l in
	     let x = x_sum /. float(List.length l) in
	     let task_type =
	       match t with
		 | `Map _ -> "M"
		 | `Emap _ -> "EM"
		 | `Sort _ -> "SO"
		 | `Shuffle _ -> "SH"
		 | _ -> "?" in
	     let task_ann =
	       match ri_opt with
		 | None -> task_type ^ " -"
		 | Some ri -> task_type ^ " #" ^ string_of_int ri.task_id in
	     let item = `Circle(x, row, task_ann, v) in
	     svg_list := item :: !svg_list;
	     Hashtbl.replace pos_of_vertex v (x,row)
	 | `File _ ->
	     ()
    )
    plan.graph;

  (* 3. Add edges *)
  
  G.iter_edges
    (fun v_from v_to ->
       let (x_from, y_from) =
	 try Hashtbl.find pos_of_vertex v_from with Not_found -> (0.0,0.0) in
       let (x_to, y_to) =
	 try Hashtbl.find pos_of_vertex v_to with Not_found -> (0.0,0.0) in
       let item =
	 `Line(x_from,y_from,x_to,y_to,v_from,v_to) in
       svg_list := item :: !svg_list
    )
    plan.graph;

  if !factor < 8 then factor := 8;

  (* 4. Output SVG *)
  let rect_width = 200.0 in
  let rect_height = 50.0 in
  let circ_radius = 50.0 in
  let font_size = 18 in

  let factor = float !factor *. rect_width *. 1.2 in

  let b = Buffer.create 1000 in
  bprintf
    b "<?xml version='1.0'?>\n\
       <!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 20010904//EN' \
          'http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd'>\n\
       <svg viewBox='-%d -%d %d %d' preserveAspectRatio='meet' \
            xmlns='http://www.w3.org/2000/svg'>\n"
    (truncate (0.05 *. factor))
    (truncate (row_height *. factor))
    (truncate (1.05 *. factor +. rect_width))
    (truncate (!row *. factor));

  let esc_xml =
    Netencoding.Html.encode
      ~in_enc:`Enc_utf8
      ~out_enc:`Enc_utf8
      () in

  let colors =
    [| "blue";
       "darkgreen";
       "brown";
       "blueviolet";
       "chocolate";
       "olive";
       "orangered";
       "salmon";
       "dimgray";
       "magenta";
       "royalblue";
       "lime";
       "goldenrod";
       "steelblue";
       "lightgreen";
       "lightpink"
    |] in
  let next_color = ref 0 in
  let color_of_node = Hashtbl.create 20 in

  let get_color_of_node ip =
    try Hashtbl.find color_of_node ip
    with Not_found ->
      if !next_color < Array.length colors then (
	let k = !next_color in
	incr next_color;
	let c = colors.(k) in
	Hashtbl.add color_of_node ip c;
	c
      )
      else
	raise Not_found in
  
  let rec vertex_color v =
    match v with
      | `Task t ->
	  ( try
	      let ri = Hashtbl.find plan.runinfo t in
	      let color = get_color_of_node ri.task_node in
	      color
	    with Not_found -> "white"
	  )
      | `File _ ->
	  (* Just take the color of the task that created this file *)
	  ( try
	      let l = G.pred plan.graph v in
	      match l with
		| [] -> raise Not_found
		| [v'] ->
		    vertex_color v'
		| _ -> raise Not_found
	    with
	      | Not_found -> "white"
	  ) in

  let is_misalloc v =
    match v with
      | `Task t ->
	  ( try
	      let ri = Hashtbl.find plan.runinfo t in
	      ri.task_misalloc
	    with Not_found -> false
	  )
      | `File _ -> false in
  
  List.iter
    (fun item ->
       match item with
	 | `Rect(x,y,ann,v) ->
	     let x = x *. factor and y = y *. factor in
	     bprintf b
	       "<rect x='%f' y='%f' width='%f' height='%f' \
                   fill='%s' stroke='black' stroke-width='2'/>\n"
	       (x -. 0.5 *. rect_width)
	       (y -. 0.5 *. rect_height)
	       rect_width
	       rect_height
	       (vertex_color v);
	     bprintf b
	       "<text x='%f' y='%f' \
                   text-anchor='middle' font-size='%d'>%s</text>\n"
	       x y font_size
	       (esc_xml ann)
	 | `Circle(x,y,ann,v) ->
	     let x = x *. factor and y = y *. factor in
	     bprintf b
	       "<circle cx='%f' cy='%f' r='%f' \
                   fill='%s' stroke='black' stroke-width='2'/>\n"
	       x y circ_radius (vertex_color v);
	     if is_misalloc v then 
	       bprintf b
		 "<circle cx='%f' cy='%f' r='%f' \
                     fill='none' stroke='red' stroke-width='5'/>\n"
		 x y (circ_radius *. 1.5);
	     bprintf b
	       "<text x='%f' y='%f' \
                   text-anchor='middle' font-size='%d'>%s</text>\n"
	       x y font_size
	       (esc_xml ann)
	 | `Line(x1,y1,x2,y2,v1,v2) ->
	     let x1 = x1 *. factor and y1 = y1 *. factor in
	     let x2 = x2 *. factor and y2 = y2 *. factor in
	     let c1 = vertex_color v1 in
	     let c2 = vertex_color v2 in
	     let is_red =
	       (c1 <> "white" && c2 <> "white" && c1 <> c2) in
	     bprintf b
	       "<line x1='%f' y1='%f' x2='%f' y2='%f' \
                   fill='white' stroke='%s' stroke-width='%d'/>\n"
	       x1 y1 x2 y2
	       (if is_red then "red" else "black")
	       (if is_red then 5 else 2)
    )
    !svg_list;

  bprintf b "</svg>\n";
  Buffer.contents b
