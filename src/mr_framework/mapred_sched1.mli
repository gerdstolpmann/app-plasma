(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Scheduler *)

open Mapred_tasks

type plan_config

val configure_plan : ?keep_temp_files:bool ->
                     string list -> plan_config
  (** [configure_plan hostlist] *)

type plan
  (** A plan contains:
      - tasks
      - dependencies between tasks
      - whether a task is done or not done
   *)

val create_plan : Plasma_client.plasma_cluster -> 
                  Mapred_def.mapred_job -> 
                  plan_config -> 
                    plan
  (** Creates a plan. This involves the analysis of the block layout of
      the input files.
   *)

val executable_tasks : plan -> task list
  (** Returns all tasks that are executable. This list can be quite long! *)

(*
val best_hosts : Plama_client.plasma_cluster -> plan -> task -> 
                   Unix.inet_addr list
  (** Calculates the best hosts for this task. This may involve the analysis
      of the block layout of the input files. The output may be empty
      when there are no clear winners. If there are several hosts, these
      are seen as roughly equivalent with a slight advantage for the first
      ones.
   *)
 *)

val hosts : plan -> (string * Unix.inet_addr) list
  (** returns all configured task server hosts *)

val mark_as_completed : plan -> task -> unit
  (** Marks this task as completed *)

val mark_as_started : plan -> task -> unit
  (** Marks this task as started *)

val task_depends_on_list : plan -> task -> task list
  (** Returns the list of tasks a given task depends on *)

val plan_finished : plan -> bool
  (** Whether everything is done *)

val cluster : plan -> Plasma_client.plasma_cluster
  (** Just return the cluster passed in *)

val print_plan : plan -> unit
  (** Debug printing to stdout *)
