(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Mapred_rfun
open Printf

let invoke = lookup

type format =
    [ `Line_structured
    | `Fixed_size of int
    | `Var_size
    | `Auto_input
    ]

let factory format =
  match format with
    | `Line_structured ->
	Mapred_io.line_structured_format()
    | `Fixed_size n ->
	Mapred_io.fixed_size_format n
    | `Var_size ->
	Mapred_io.var_size_format()
    | `Auto_input ->
	Mapred_io.auto_input_format()
	  


module Notebook : sig
  (* Note that notebooks are a hybrid between a functional and a
     mutable structure: [tail] just returns the tail of the same internal
     list. [add], however, modifies the last element of the list in-place.
   *)
  type 'a t

  val create : unit -> 'a t
  val add : 'a -> 'a t -> unit

  val tl : 'a t -> 'a t
  val hd : 'a t -> 'a
  val is_empty : 'a t -> bool

  val length : 'a t -> int
  val tail_end : 'a t -> 'a t
end = struct
  type 'a cell =
      { mutable content : 'a option;
	mutable next : 'a cell;
      }
  (* The last element has [content=None], and [next] points to itself *)

  type 'a t =
      { start : 'a cell;
	mutable guessed_end : 'a cell;
	mutable guessed_len : int;   (* until guessed_end *)
      }

  let create() =
    let rec empty =
      { content = None; next = empty } in
    { start = empty;
      guessed_end = empty;
      guessed_len = 0;
    }
	
  let rec find_end c k =
    match c.content with
      | None -> (c,k)
      | Some _ -> find_end c.next (k+1)

  let add x nb =
    let (real_end,real_len) = find_end nb.guessed_end nb.guessed_len in
    let rec empty =
      { content = None; next = empty } in
    real_end.content <- Some x;
    real_end.next <- empty;
    nb.guessed_end <- empty;
    nb.guessed_len <- real_len + 1

  let tl nb =
    if nb.start.content = None then
      failwith "Mapred_toolkit.Notebook.tl";
    { start = nb.start.next;
      guessed_end = nb.guessed_end;
      guessed_len = nb.guessed_len
    }

  let hd nb =
    match nb.start.content with
      | None -> failwith "Mapred_toolkit.Notebook.hd"
      | Some x -> x

  let is_empty nb =
    nb.start.content = None

  let length nb =
    let (real_end,real_len) = find_end nb.guessed_end nb.guessed_len in
    nb.guessed_end <- real_end;
    nb.guessed_len <- real_len;
    real_len

  let tail_end nb =
    let (real_end,real_len) = find_end nb.guessed_end nb.guessed_len in
    nb.guessed_end <- real_end;
    nb.guessed_len <- real_len;
    { start = real_end;
      guessed_end = real_end;
      guessed_len = 0
    }
    

end


module Place = struct
  type 'a codec = ('a -> string) rfun * (string -> int -> int -> 'a) rfun

  type ext_location = 
     [ `File of string | `Flat_dir of string | `Deep_dir of string 
     ]

  type location = [ ext_location | `Notebook ]

  type 'a t =
      { fs : Mapred_fs.filesystem option;
	codec : 'a codec option;
	format : format;
	location : location;
	prefix : string;
	repl : int;
	rc : Mapred_io.record_config option;
	mutable files : string list;
	mutable notebooks : 'a Notebook.t list;
      }

  let create ?(prefix="") ?(repl=0) fs codec format rc xloc =
    ( match xloc with
	| `File name ->
	    Mapred_io.create_file ~repl fs name
	| `Flat_dir name | `Deep_dir name ->
	    Mapred_io.create_dir fs name
    );
    { fs = Some fs;
      codec = Some codec;
      format;
      location = (xloc :> location);
      prefix;
      repl;
      rc = Some rc;
      files = [];
      notebooks = [];
    }

  let from ?(prefix="") ?(repl=0) fs codec format rc xloc =
    let files =
      match xloc with
	| `File name ->
	    (* only check that the file exists *)
	    ignore(Mapred_io.scan_file fs name);
	    [ name ]
	| `Flat_dir dir ->
	    let l = Mapred_io.scan_dir fs dir false in
	    List.map
	      (fun (n,_) -> dir ^ "/" ^ n)
	      l
	| `Deep_dir dir ->
	    let l = Mapred_io.scan_dir fs dir true in
	    List.map
	      (fun (n,_) -> dir ^ "/" ^ n)
	      l in
    { fs = Some fs;
      codec = Some codec;
      format;
      location = (xloc :> location);
      files = List.rev files;
      prefix;
      repl;
      rc = Some rc;
      notebooks = [];
    }


  let notebook_place () =
    { fs = None;
      codec = None;
      format = `Var_size;
      location = `Notebook;
      files = [];
      prefix = "";
      repl = 0;
      rc = None;
      notebooks = [];
    }

  let is_file_place place =
    place.location <> `Notebook

  let files place =
    List.rev place.files

  let location place =
    place.location

  let get_format place =
    place.format

  let get_fs place =
    match place.fs with
      | None -> failwith "Mapred_toolkit.Place: not a file place"
      | Some fs -> fs

  let get_codec place =
    match place.codec with
      | None -> failwith "Mapred_toolkit.Place: not associated to a codec"
      | Some c -> c

  let get_rc place =
    match place.rc with
      | None -> failwith "Mapred_toolkit.Place: not a file place"
      | Some rc -> rc
  
  let create_file ?filename place =
    let fs = get_fs place in
    let suffix = 
      match place.format with
	| `Line_structured -> ""
	| `Var_size -> ".var"
	| `Fixed_size n -> ".fixed" ^ string_of_int n 
	| `Auto_input ->
	    failwith "Mapred_toolkit.Place.create_file: not supported for }
                      auto input format" in

    let rec attempt k =
      if k > 20 then
	failwith "Mapred_toolkit.Place.create_file";
      try
	let name =
	  match place.location with
	    | `File n ->
		if place.files <> [] then
		  failwith "Mapred_toolkit.Place.create_file: already created";
		n
	    | `Flat_dir d | `Deep_dir d ->
		( match filename with
		    | None ->
			let rnd = Plasma_rng.random_int64() in
			let n = 
			  sprintf "%s/%s%08Lx%s" d place.prefix rnd suffix in
			n
		    | Some fragment ->
			let n1 = 
			  sprintf "%s/%s%s%s" d place.prefix fragment suffix in
			if Mapred_io.file_exists fs n1 then
			  let rnd = Plasma_rng.random_int64() in
			  sprintf "%s/%s%s_%08Lx%s" 
			    d place.prefix fragment rnd suffix
			else
			  n1
		)

	    | `Notebook ->
		failwith "Mapred_toolkit.Place.create_file: not a file place"
	in
	Mapred_io.create_file ~repl:place.repl fs name;
	name
      with
	| Plasma_client.Plasma_error `eexist
	| Unix.Unix_error(Unix.EEXIST,_,_) ->
	    attempt (k+1) in

    let name = attempt 0 in
    place.files <- name :: place.files;
    name

  let notebooks place =
    List.rev place.notebooks

  let create_notebook place =
    let nb = Notebook.create() in
    place.notebooks <- nb :: place.notebooks;
    nb

  let clear place = 
    match place.location with
      | `Notebook ->
	  place.notebooks <- []
      | `File path ->
	  let fs = get_fs place in
	  Mapred_io.delete_file fs path;
	  place.files <- []
      | `Flat_dir _ | `Deep_dir _ ->
	  let fs = get_fs place in
	  while place.files <> [] do
	    let path = List.hd place.files in
	    Mapred_io.delete_file fs path;
	    place.files <- List.tl place.files
	  done

end


module Store = struct
  type 'a store =
      { place : 'a Place.t;
	case : 'a case;
	mutable length : int64 option;
	mutable cursor : int64;
	mutable close_hooks : (unit -> unit) list
      }

  and 'a case =
    | Nbook of 'a Notebook.t
    | Reader of string * int64 * Mapred_fs.filesystem * 
	        Mapred_io.record_config 
    | Writer of string * Mapred_io.record_writer

  let flush st =
    match st.case with
      | Writer(_,wr) -> wr # flush()
      | _ -> ()

  let notebook() =
    let place = Place.notebook_place() in
    let nb = Place.create_notebook place in
    { place;
      case = Nbook nb;
      length = Some 0L;
      cursor = 0L;
      close_hooks = [];
    }

  let file_name place =
    match place.case with
      | Nbook _ ->
	  failwith "Mapred_toolkit.Store.file_name: notebooks do not have names"
      | Reader(n,_,_,_) ->
	  n
      | Writer(n,_) ->
	  n

  let read_place place =
    if Place.is_file_place place then
      let fs = Place.get_fs place in
      let rc = Place.get_rc place in
      List.map
	(fun file ->
	   let len = Mapred_io.file_blocks fs file in
	   { place = place;
	     case = Reader(file,len,fs,rc);
	     length = None;
	     cursor = 0L;
	     close_hooks = [];
	   }
	)
	(Place.files place)
    else
      List.map
	(fun nb ->
	   { place = place;
	     case = Nbook nb;
	     length = Some(Int64.of_int(Notebook.length nb));
	     cursor = 0L;
	     close_hooks = [];
	   }
	)
	(Place.notebooks place)

  let write_place ?filename place =
    if Place.is_file_place place then
      let fs = Place.get_fs place in
      let rc = Place.get_rc place in
      let file = Place.create_file ?filename place in
      let fac = factory (Place.get_format place) in
      let shm_mng = new Plasma_shm.null_shm_manager() in
      let wr = fac # write_file fs shm_mng rc file in
      { place = place;
	case = Writer(file,wr);
	length = Some 0L;
	cursor = 0L;
	close_hooks = [];
      }
    else
      let nb = Place.create_notebook place in
      { place = place;
	case = Nbook nb;
	length = Some(Int64.of_int(Notebook.length nb));
	cursor = 0L;
	close_hooks = [];
      }

  let read st =
    match st.case with
      | Nbook nb ->
	  { st with close_hooks = [] }
      | Reader (file,len,fs,rc) ->
	  { st with close_hooks = [] }
      | Writer (file, wr) ->
	  wr # flush();
	  let fs = Place.get_fs st.place in
	  let rc = Place.get_rc st.place in
	  let len = Mapred_io.file_blocks fs file in
	  { st with
	      case = Reader(file,len,fs,rc);
	      close_hooks = []
	  }


  let close st =
    List.iter (fun f -> f()) st.close_hooks;
    st.close_hooks <- [];
    match st.case with
      | Nbook _ -> ()
      | Reader _ -> ()
      | Writer(n,wr) -> wr # close_out()

  let place st =
    st.place

  let incr_length st =
    match st.length with
      | None -> ()
      | Some n ->
	  st.length <- Some(Int64.succ n)

  let length64 st =
    match st.length with
      | Some len -> len
      | None ->
	  ( match st.case with
	      | Reader(name,len,fs,rc) ->
		  let n = ref 0L in
		  let fac = factory (Place.get_format st.place) in
		  let shm_mng = new Plasma_shm.null_shm_manager() in
		  let rd = fac # read_file [] fs shm_mng rc name 0L len in
		  let q = Queue.create() in
		  ( try
		      while true do
			rd # input_records q;
			n := Int64.add !n (Int64.of_int (Queue.length q));
			Queue.clear q
		      done
		    with 
		      | End_of_file -> rd # close_in()
		      | error -> rd # close_in(); raise error
		  );
		  st.length <- Some !n;
		  !n
	      | _ -> assert false
	  )

  let length st =
    let n = length64 st in
    if n > Int64.of_int max_int then
      failwith "Mapred_toolkit.Store.length: out of int range";
    Int64.to_int n

end


module Seq = struct
  exception End_of_sequence

  type ('a,'b) seq =
      { st : 'a Store.store;
	n : int64;
	case : 'a case;
      }

  and 'a case =
    | Nbook of 'a Notebook.t
    | Writer of ('a -> string)
    | Reader of 'a rd_case * (string -> int -> int -> 'a)

  and 'a rd_case =
      { rd : Mapred_io.record_reader;
	nb : 'a Notebook.t;
      }

  let read st =
    { st;
      n = ( match st.Store.length with
	      | Some n -> n
	      | None -> Int64.max_int
	  );
      case = ( match st.Store.case with
		 | Store.Nbook nb -> 
		     Nbook nb
		 | Store.Reader(name,len,fs,rc) ->
		     let place = Store.place st in
		     let fac = factory (Place.get_format place) in
		     let shm_mng = new Plasma_shm.null_shm_manager() in
		     let rd = 
		       fac # read_file [] fs shm_mng rc name 0L len in
		     st.Store.close_hooks <-
		       (fun () -> rd # close_in()) :: st.Store.close_hooks;
		     let codec = Place.get_codec place in
		     let (_,decoder_rfun) = codec in
		     let decoder = invoke decoder_rfun in
		     Reader ( { rd; nb = Notebook.create() }, decoder)
		 | Store.Writer _ ->
		     failwith "Mapred_toolkit.Seq.read"
	     )
    }


  let extend st =
    { st;
      n = 0L;
      case = ( match st.Store.case with
		 | Store.Nbook nb -> 
		     Nbook (Notebook.tail_end nb)
		 | Store.Reader(name,len,fs,rc) ->
		     Nbook (Notebook.create())
		 | Store.Writer(n,wr) ->
		     let place = Store.place st in
		     let codec = Place.get_codec place in
		     let (coder_rfun,_) = codec in
		     let coder = invoke coder_rfun in
		     Writer coder
	     )
    }

  let reread seq =
    read(Store.read seq.st)

  let store seq = seq.st

  let flush seq =
    Store.flush seq.st

  let notebook() =
    extend(Store.notebook())

  let is_empty seq =
    seq.n = 0L || (
      match seq.case with
	| Nbook _ -> false
	| Reader (r,decoder) ->
	    Notebook.is_empty r.nb && (
	      try
		let s = r.rd # input_record() in
		let x = decoder s 0 (String.length s) in
		Notebook.add x r.nb;
		false
	      with End_of_file -> 
		r.rd # close_in();
		true
	    )
      | Writer _ -> false
    )

  let hd seq =
    if seq.n = 0L then raise End_of_sequence;
    match seq.case with
      | Nbook nb ->
	  if Notebook.is_empty nb then
	    raise End_of_sequence;
	  Notebook.hd nb
      | Reader (r,decoder) ->
	  if not (Notebook.is_empty r.nb) then
	    Notebook.hd r.nb
	  else (
	    try
	      let s = r.rd # input_record() in
	      let x = decoder s 0 (String.length s) in
	      Notebook.add x r.nb;
	      x
	    with End_of_file -> 
	      r.rd # close_in();
	      raise End_of_sequence
	  )
      | Writer _ ->
	  failwith "Mapred_toolkit.Seq.hd: write-only sequence"


  let tl seq =
    if seq.n = 0L then raise End_of_sequence;
    match seq.case with
      | Nbook nb ->
	  if Notebook.is_empty nb then
	    raise End_of_sequence;
	  { seq with
	      n = Int64.pred seq.n;
	      case = Nbook(Notebook.tl nb)
	  }
      | Reader (r,decoder) ->
	  if Notebook.is_empty r.nb then (
	    try
	      let s = r.rd # input_record() in
	      let x = decoder s 0 (String.length s) in
	      Notebook.add x r.nb;
	    with
	      | End_of_file ->
		  r.rd # close_in();
		  raise End_of_sequence
	  );
	  { seq with
	      n = Int64.pred seq.n;
	      case = Reader ({ r with nb = Notebook.tl r.nb }, decoder)
	  }
      | Writer _ ->
	  failwith "Mapred_toolkit.Seq.tl: write-only sequence"


  let add x seq =
    ( match seq.st.Store.case with
	| Store.Nbook nb ->
	    if seq.n <> Store.length64 seq.st then
	      failwith "Mapred_toolkit.Seq.add: sequence view is read-only";
	    Notebook.add x nb;
	    Store.incr_length seq.st
	| Store.Writer(_,wr) ->
	    if seq.n <> Store.length64 seq.st then
	      failwith "Mapred_toolkit.Seq.add: sequence view is read-only";
	    let coder =
	      match seq.case with
		| Writer f -> f
		| _ -> assert false in
	    wr # output_record (coder x);
	    Store.incr_length seq.st
	| Store.Reader _ ->
	    failwith "Mapred_toolkit.Seq.add: cannot add to this type of store"
    );
    { seq with
	n = if seq.n = Int64.max_int then Int64.max_int else Int64.succ seq.n;
    }

  let addl l s1 =
    List.fold_left
      (fun s2 x -> add x s2)
      s1
      l


  let rec iter f seq =
    if not(is_empty seq) then (
      let x = hd seq in
      f x;
      iter f (tl seq)
    )

  let rec fold f acc0 seq =
    if is_empty seq then
      acc0
    else
      fold f (f acc0 (hd seq)) (tl seq)

  let hdl n seq =
    let l = ref [] in
    let k = ref 0 in
    ( try
	iter (fun x -> l := x :: !l; incr k; if !k >= n then raise Exit) seq
      with
	| Exit -> ()
    );
    List.rev !l


  let append s1 s2 =
    fold (fun acc x -> add x acc) s1 s2

  let length64 seq =
    match seq.case with
      | Reader(r,_) -> 
	  Int64.add
	    (Int64.sub (Store.length64 seq.st) seq.st.Store.cursor)
	    (Int64.of_int (Notebook.length r.nb))
      | _ -> seq.n

  let length seq = 
    let n = length64 seq in
    if n > Int64.of_int max_int then
      failwith "Mapred_toolkit.Seq.length: out of int range";
    Int64.to_int n

  let map f s1 s2 =
    fold (fun acc x -> add (f x) acc) s2 s1

  let mapl f s1 s2 =
    fold
      (fun acc x -> 
	 let l = f x in
	 List.fold_left (fun acc y -> add y acc) acc l
      )
      s2 
      s1


  let sort ~hash ~cmp s1 s2 =
    let l1 =
      fold (fun acc x -> x :: acc) [] s1 in
    let l2 =
      List.sort
	(fun x y ->
	   let hx = hash x in
	   let hy = hash y in
	   if hx <> hy then
	     hx - hy
	   else
	     cmp x y
	)
	l1 in
    List.fold_left
      (fun acc x ->
	 add x acc
      )
      s2
      l2

  let mapl_sort_fold_i f_mapl f_hash f_cmp f_initfold f_fold f_finfold 
                       f_partition_of partitions inplace outplace =
    let in_stores = Store.read_place inplace in
    
    let mapout_container = notebook() in
    let mapout =
      List.fold_left
	(fun acc in_store ->
	   let in_seq = read in_store in
	   mapl f_mapl in_seq acc
	)
	mapout_container
	in_stores in

    let sortout_container = notebook() in
    let sortout =
      sort ~hash:f_hash ~cmp:f_cmp mapout sortout_container in
    
    let partout =
      Array.init partitions (fun i -> notebook()) in
    iter
      (fun x ->
	 let i = f_partition_of x in
	 partout.(i) <- add x partout.(i)
      )
      sortout;

    let width = Mapred_sched.log partitions in

    let out_seq i =
      let filename = sprintf "partition_%0*d" width i in
      extend (Store.write_place ~filename outplace) in
    let redout =
      Array.init partitions out_seq in
    
    let rec reduce i =
      if i < partitions then (
	let acc_init = f_initfold i in
	let acc_res =
	  fold
	    (fun acc x ->
	       let (acc', l) = f_fold acc x in
	       List.iter 
		 (fun y -> 
		    redout.(i) <- add y redout.(i)
		 )
		 l;
	       acc'
	    )
	    acc_init
	    partout.(i) in
	let l = f_finfold acc_res in
	List.iter 
	  (fun y -> 
	     redout.(i) <- add y redout.(i)
	  )
	  l;
	reduce (i+1)
      ) in

    reduce 0;
    Array.iter
      (fun s -> Store.close s.st)
      redout;
    Array.to_list redout


  let mapl_sort_fold ~mapl ~hash ~cmp ~initfold ~fold 
                     ?(finfold = fun _ -> [])
                     ~partition_of ~partitions inplace outplace =
    mapl_sort_fold_i
      mapl hash cmp initfold fold finfold partition_of partitions
      inplace outplace

end



exception Toolkit_exit

class type mr_info =
object
  method mapred_env : Mapred_def.mapred_env
  method mapred_job_config : Mapred_def.mapred_job_config
end


let mapred_info me jc : mr_info =
  ( object
      method mapred_env = me
      method mapred_job_config = jc
    end
  )

	  

class toolkit_job _ : Mapred_def.mapred_job =
  ( object
      method custom_params =
	[ "input_format";
	  "output_format";
	  "mapl_rfun";
	  "hash_rfun";
	  "cmp_rfun";
	  "initfold_rfun";
	  "fold_rfun";
	  "finfold_rfun";
	  "part_of_rfun";
	  "initcombine_rfun";
	  "combine_rfun";
	  "fincombine_rfun";
	  "decoder_rfun_a";
	  "coder_rfun_b";
	  "decoder_rfun_b";
	  "coder_rfun_d";
	]

      method check_config _ _ = ()
      method pre_job_start _ _ = ()
      method post_job_finish _ _ = ()
	
      method input_record_io _ jc =
	let s = jc # custom "input_format" in
	let format = (Marshal.from_string s 0 : format) in
	factory format

      method output_record_io _ jc =
	let s = jc # custom "output_format" in
	let format = (Marshal.from_string s 0 : format) in
	factory format

      method internal_record_io _ _ =
	Mapred_io.var_size_format()

      method map me jc =
	let mi = mapred_info me jc in
	let s1 = jc # custom "mapl_rfun" in
	let mapl_rfun = 
	  (Marshal.from_string s1 0 : (mr_info -> 'a -> 'b list) rfun) in
	let mapl = Mapred_rfun.lookup mapl_rfun in
	let s2 = jc # custom "decoder_rfun_a" in
	let decoder_rfun_a = 
	  (Marshal.from_string s2 0 : (string -> int -> int -> 'a) rfun) in
	let decoder_a = Mapred_rfun.lookup decoder_rfun_a in
	let s3 = jc # custom "coder_rfun_b" in
	let coder_rfun_b = (Marshal.from_string s3 0 : ('b -> string) rfun) in
	let coder_b = Mapred_rfun.lookup coder_rfun_b in
	let mapl_mi = mapl mi in
	(fun ti rd wr ->
	   try
	     while true do
	       let r_str = 
		 try rd # input_record()
		 with End_of_file -> raise Toolkit_exit in
	       let r = decoder_a r_str 0 (String.length r_str) in
	       let l = mapl_mi r in
	       List.iter
		 (fun x ->
		    let y = coder_b x in
		    wr # output_record y
		 )
		 l
	     done
	   with
	     | Toolkit_exit ->
		 wr # flush()
	)

      method sorter me jc factor =
	let mi = mapred_info me jc in
	let s1 = jc # custom "hash_rfun" in
	let hash_rfun = 
	  (Marshal.from_string s1 0 : (mr_info -> 'b -> int) rfun) in
	let hash = Mapred_rfun.lookup hash_rfun in
	let s2 = jc # custom "cmp_rfun" in
	let cmp_rfun = 
	  (Marshal.from_string s2 0 : (mr_info -> 'b -> 'b -> int) rfun) in
	let cmp = Mapred_rfun.lookup cmp_rfun in
	let s3 = jc # custom "decoder_rfun_b" in
	let decoder_rfun_b = 
	  (Marshal.from_string s3 0 : (string -> int -> int -> 'b) rfun) in
	let decoder_b = Mapred_rfun.lookup decoder_rfun_b in
	let hash_mi = hash mi in
	let cmp_mi = cmp mi in
	Mapred_sorters.generic_sorter
	  ~hash:(fun buf p l ->
		   hash_mi (decoder_b buf p l)
		)
	  ~cmp:(fun buf1 p1 l1 buf2 p2 l2 ->
		  cmp_mi (decoder_b buf1 p1 l1) (decoder_b buf2 p2 l2)
	       )
	  me jc factor

      method extract_key me jc r =
	(0, String.length r)

      method partition_of_key me jc =
	let mi = mapred_info me jc in
	let s1 = jc # custom "part_of_rfun" in
	let part_of_rfun = 
	  (Marshal.from_string s1 0 : (mr_info -> 'b -> int) rfun) in
	let part_of = Mapred_rfun.lookup part_of_rfun in
	let s2 = jc # custom "decoder_rfun_b" in
	let decoder_rfun_b = 
	  (Marshal.from_string s2 0 : (string -> int -> int -> 'b) rfun) in
	let decoder_b = Mapred_rfun.lookup decoder_rfun_b in
	let part_of_mi = part_of mi in
	(fun buf p l ->
	   let r = decoder_b buf p l in
	   part_of_mi r
	)
	
      method reduce me jc =
	let mi = mapred_info me jc in
	let s1 = jc # custom "initfold_rfun" in
	let initfold_rfun = 
	  (Marshal.from_string s1 0 : (mr_info -> int -> 'c) rfun) in
	let initfold = Mapred_rfun.lookup initfold_rfun in
	let s2 = jc # custom "fold_rfun" in
	let fold_rfun =
	  (Marshal.from_string s2 0 :
	     (mr_info -> 'c -> 'b -> 'c * 'd list) rfun) in
	let fold = Mapred_rfun.lookup fold_rfun in
	let s3 = jc # custom "finfold_rfun" in
	let finfold =
	  if s3 = "" then
	    (fun _ _ -> [])
	  else
	    let finfold_rfun = 
	      (Marshal.from_string s3 0 : (mr_info -> 'c -> 'd list) rfun) 
	    in
	    Mapred_rfun.lookup finfold_rfun in
	let s4 = jc # custom "decoder_rfun_b" in
	let decoder_rfun_b = 
	  (Marshal.from_string s4 0 : (string -> int -> int -> 'b) rfun) in
	let decoder_b = Mapred_rfun.lookup decoder_rfun_b in
	let s5 = jc # custom "coder_rfun_d" in
	let coder_rfun_d = (Marshal.from_string s5 0 : ('d -> string) rfun) in
	let coder_d = Mapred_rfun.lookup coder_rfun_d in
	let fold_mi = fold mi in
	(fun ti rd wr ->
	   let acc = ref (initfold mi ti#partition) in
	   try
	     while true do
	       let r_str =
		 try rd # input_record()
		 with End_of_file -> raise Toolkit_exit in
	       let r =
		 decoder_b r_str 0 (String.length r_str) in
	       let (acc', out) = fold_mi !acc r in
	       List.iter
		 (fun x ->
		    wr # output_record (coder_d x)
		 )
		 out;
	       acc := acc'
	     done
	   with
	     | Toolkit_exit ->
		 let out = finfold mi !acc in
		 List.iter
		   (fun x ->
		      wr # output_record (coder_d x)
		   )
		   out;
		 wr # flush()
	)

      method combine me jc =
	let mi = mapred_info me jc in
	let s1 = jc # custom "initcombine_rfun" in
	let s2 = jc # custom "combine_rfun" in
	let s3 = jc # custom "fincombine_rfun" in
	if s2 = "" then
	  (fun _ -> None)
	else (
	  let initcombine_rfun = 
	    (Marshal.from_string s1 0 : (mr_info -> 'e) rfun) in
	  let initcombine = Mapred_rfun.lookup initcombine_rfun in
	  let combine_rfun =
	    (Marshal.from_string s2 0 :
	       (mr_info -> 'e -> 'b -> 'e * 'b list) rfun) in
	  let combine = Mapred_rfun.lookup combine_rfun in
	  let fincombine =
	    if s3 = "" then
	      (fun _ _ -> [])
	    else
	      let fincombine_rfun = 
		(Marshal.from_string s3 0 : (mr_info -> 'e -> 'b list) rfun) 
	      in
	      Mapred_rfun.lookup fincombine_rfun in
	  
	  let s4 = jc # custom "decoder_rfun_b" in
	  let decoder_rfun_b = 
	    (Marshal.from_string s4 0 : (string -> int -> int -> 'b) rfun) in
	  let decoder_b = Mapred_rfun.lookup decoder_rfun_b in
	  let s5 = jc # custom "coder_rfun_b" in
	  let coder_rfun_b = (Marshal.from_string s5 0 : ('b -> string) rfun) in
	  let coder_b = Mapred_rfun.lookup coder_rfun_b in
	  let combine_mi = combine mi in

	  (fun ti ->
	     Some
	       (fun rd wr ->
		  let acc = ref (initcombine mi) in
		  try
		    while true do
		      let r_str =
			try rd # input_record()
			with End_of_file -> raise Toolkit_exit in
		      let r =
			decoder_b r_str 0 (String.length r_str) in
		      let (acc', out) = combine_mi !acc r in
		      List.iter
			(fun x ->
			   wr # output_record (coder_b x)
			)
			out;
		      acc := acc'
		    done
		  with
		    | Toolkit_exit ->
			let out = fincombine mi !acc in
			List.iter
			  (fun x ->
			     wr # output_record (coder_b x)
			  )
			  out;
			wr # flush()
	       )

	  )
	) 
    end
  )


let toolkit_job = new toolkit_job



module DSeq = struct

  type config =
      { me : Mapred_def.mapred_env;
	jc : Mapred_def.mapred_job_config;
	mjc : Mapred_job_config.m_job_config;
	report : bool;
	report_to : Netchannels.out_obj_channel;
	keep_temp_files : bool;
      }

  class type mapred_info = mr_info

  let create_config ?name ?task_files ?bigblock_size ?map_tasks
                    ?merge_limit ?split_limit ?partitions 
		    ?enhanced_mapping ?phases
		    ?(report=false)
		    ?(report_to=new Netchannels.output_channel stderr)
		    ?(keep_temp_files=false)
		    me =
    let mjc0 =
      snd(Mapred_job_config.extract_job_config me#config_file [] []) in
    let mjc =
      Mapred_job_config.update_job_config
	?name ?task_files ?bigblock_size ?map_tasks 
	?merge_limit ?split_limit ?partitions ?enhanced_mapping
	?phases mjc0 in
    { me;
      mjc;
      jc = Mapred_job_config.mapred_job_config mjc;
      report;
      report_to;
      keep_temp_files;
    }

  let get_rc config =
    Mapred_def.get_rc config.me config.jc#bigblock_size

  type 'a result =
      { mutable res_lz : 'a Lazy.t;
	rj : Mapred_job_exec.running_job option;
	config : config
      }

  let get_result r =
    Lazy.force r.res_lz

  let stats r =
    match r.rj with
      | None ->
	  Mapred_stats.empty
      | Some rj ->
	  Mapred_job_exec.stats rj

  let job_id c =
    c.jc # job_id

  let mapl_sort_fold_i f_mapl f_hash f_cmp f_initfold f_fold f_finfold_opt
                       f_part_of f_initcomb_opt f_comb_opt f_fincomb_opt
                       pl_a pl_d config codec_b =
    if f_comb_opt <> None && f_initcomb_opt = None then
      invalid_arg "Mapred_toolkit.DSeq.mapl_sort_fold: missing initcombine";

    let marshal v =
      Marshal.to_string v [] in
    let params =
      [ "input_format",   marshal (Place.get_format pl_a);
	"output_format",  marshal (Place.get_format pl_d);
	"mapl_rfun",      marshal f_mapl;
	"hash_rfun",      marshal f_hash;
	"cmp_rfun",       marshal f_cmp;
	"initfold_rfun",  marshal f_initfold;
	"fold_rfun",      marshal f_fold;
	"finfold_rfun",   (match f_finfold_opt with
			     | None -> ""
			     | Some f ->  marshal f
			  );

	"initcombine_rfun", (match f_initcomb_opt with
			     | None -> ""
			     | Some f ->  marshal f
			  );
	"combine_rfun",   (match f_comb_opt with
			     | None -> ""
			     | Some f ->  marshal f
			  );
	"fincombine_rfun",(match f_fincomb_opt with
			     | None -> ""
			     | Some f ->  marshal f
			  );
	"part_of_rfun",   marshal f_part_of;
	"decoder_rfun_a", marshal (snd (Place.get_codec pl_a));
	"coder_rfun_b",   marshal (fst codec_b);
	"decoder_rfun_b", marshal (snd codec_b);
	"coder_rfun_d",   marshal (fst (Place.get_codec pl_d));
      ] in
    let (input_dir_ds, input_dir) =
      match Place.location pl_a with
	| `Notebook -> assert false
	| `File name -> `File, name
	| `Flat_dir name -> `Flat_dir, name
	| `Deep_dir name -> `Deep_dir, name in
    let output_dir =
      match Place.location pl_d with
	| `Notebook -> assert false
	| `File name -> 
	    failwith
	      "Mapred_toolkit.DSeq.mapl_sort_fold: need an output directory"
	| `Flat_dir name -> name
	| `Deep_dir name -> name in
    let work_dir = output_dir ^ "/_work" in
    let log_dir = output_dir ^ "/_log" in
    let mjc =
      Mapred_job_config.update_job_config 
	~custom:params
	~input_dir
	~input_dir_designation:input_dir_ds
	~output_dir
	~work_dir
	~log_dir
	config.mjc in
    let jc = Mapred_job_config.mapred_job_config mjc in
    let rjc =
      Mapred_job_exec.create_runtime_job_config_from_mapred_config
	~report:config.report
	~report_to:config.report_to
	~keep_temp_files:config.keep_temp_files
	mjc
	config.me#config in
    let rj =
      Mapred_job.invoke
	~catch_sigint:true
	~create_dirs:true
	config.me
	rjc
	(toolkit_job config.me) in
    let res_lz =
      lazy (
	Mapred_job.run rj;
	match Mapred_job_exec.status rj with
	  | `Running ->
	      failwith "Internal error (job still running but function retrurned)"
	  | `Errors l ->
	      failwith ("Errors: " ^ 
			  String.concat ", and " l)
	  | `Killed ->
	      failwith "Job killed"
	  | `Successful ->
	      let xloc =
		match Place.location pl_d with
		  | #Place.ext_location as xloc -> xloc
		  | _ -> assert false in
	      let pl_res =
		Place.from
		  (Place.get_fs pl_d)
		  (Place.get_codec pl_d)
		  (Place.get_format pl_d)
		  (Place.get_rc pl_d)
		  xloc in
	      (* Files should now have format "partition_#". We return
		 those files first, in the right order. If the job
		 creates other files, these will follow in any order
		 (e.g. mapped_#).
	       *)
	      let pl_st1 = Store.read_place pl_res in
	      let pl_st2 =
		List.map
		  (fun st ->
		     let name = Store.file_name st in
		     try
		       let p =
			 Scanf.sscanf
			   (Filename.basename name)
			   "partition_%d"
			   (fun p -> p) in
		       `Partition(st,p)
		     with
		       | _ -> `Other st
		  )
		  pl_st1 in
	      let pl_st3 =
		List.sort
		  (fun f1 f2 ->
		     match f1,f2 with
		       | `Partition(_,p), `Partition(_,q) -> p-q
		       | `Partition(_,_), `Other _ -> (-1)
		       | `Other _, `Partition(_,_) -> 1
		       | `Other o1, `Other o2 -> 
			   let n1 = Store.file_name o1 in
			   let n2 = Store.file_name o2 in
			   String.compare n1 n2
		  )
		  pl_st2 in
	      if jc#phases = `Map_sort_reduce then (
		try
		  let k = ref 0 in
		  List.iter
		    (function
		       | `Other _ -> raise Not_found
		       | `Partition(_,p) ->
			   if p <> !k then raise Not_found;
			   incr k
		    )
		    pl_st3;
		  if !k <> jc#partitions then raise Not_found;
		with
		  | _ ->
		      failwith "Mapred_toolkit.DSeq.mapl_sort_fold: \
                                cannot interpret output directory";

	      );
	      let seq_list =
		List.map 
		  (function
		     | `Partition(st,_) -> Seq.read st
		     | `Other st -> Seq.read st
		  )
		  pl_st3 in
	      seq_list
      ) in
    { rj = Some rj;
      config;
      res_lz
    }

  let mapl_sort_fold ~mapl ~hash ~cmp ~initfold ~fold ?finfold
                     ~partition_of
                     ?initcombine ?combine ?fincombine
                      pl_a pl_d config codec_b =
    if Place.is_file_place pl_a && Place.is_file_place pl_d then
      mapl_sort_fold_i
	mapl hash cmp initfold fold finfold partition_of
	initcombine combine fincombine
	pl_a pl_d config codec_b
    else (
      if config.report then
	config.report_to # output_string
	  ("Warning: Running map/reduce non-distributed\n");
      let mi = mapred_info config.me config.jc in
      let seq_l =
	Seq.mapl_sort_fold
	  ~mapl:(invoke mapl mi)
	  ~hash:(invoke hash mi)
	  ~cmp:(invoke cmp mi)
	  ~initfold:(invoke initfold mi)
	  ~fold:(invoke fold mi)
	  ~finfold:(match finfold with
		      | None -> (fun _ -> [])
		      | Some f -> invoke f mi
		   )
	  ~partition_of:(invoke partition_of mi)
	  ~partitions:config.jc#partitions
	  pl_a pl_d in
      { rj = None;
	config;
	res_lz = lazy seq_l;
      }
    )

  let dummy_rfun =
    Mapred_rfun.register
      "_Mapred_toolkit.DSeq.dummy_rfun"
      (fun (mi:mapred_info) -> if true then assert false; ())

  (* NB: pure "assert false" results in a warning *)


  let mapl f_mapl pl_a pl_b config =
    let mjc' = 
      Mapred_job_config.update_job_config ~phases:`Map config.mjc in
    let config' =
      { config with
	  mjc = mjc';
	  jc = Mapred_job_config.mapred_job_config mjc'
      } in
    mapl_sort_fold
      ~mapl:f_mapl
      ~hash:(Obj.magic dummy_rfun)
      ~cmp:(Obj.magic dummy_rfun)
      ~initfold:(Obj.magic dummy_rfun)
      ~fold:(Obj.magic dummy_rfun)
      ~partition_of:(Obj.magic dummy_rfun)
      pl_a
      pl_b
      config'
      (Place.get_codec pl_b)


end
