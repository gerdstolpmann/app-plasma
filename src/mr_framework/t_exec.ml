(* Run an individual task (perf) *)

open Mapred_tasks

let () =
  Printexc.record_backtrace true

let conf_cf =
  Netplex_config.read_config_file "t_exec.conf"

let conf =
  Mapred_config.extract_config conf_cf

let job_conf =
  fst(Mapred_job_config.extract_job_config conf_cf [] [])

let env =
  (object
     method open_cluster esys =
       Plasma_client.open_cluster 
         "office"
         [ "office3", 2730; "office4", 2730 ]
         esys 
     method config = conf
     method config_file = conf_cf
   end
  )

let job =
  (object
     method custom_params = []
     method check_config _ _ = ()
     method pre_job_start _ _ = ()
     method post_job_finish _ _ = ()
     method map e jc ti rr rw =
       try
	 while true do
           let r = rr#input_record() in
           let words = Pcre.split r in
           List.iter (fun word -> rw # output_record word) words
	 done
       with End_of_file ->
	 rw # flush()
     method extract_key e jc line =
       line
     method partition_of_key e jc key =
       (Hashtbl.hash key) mod jc#partitions
     method reduce e jc ti rr rw =
       assert false
   end
  )

let task =
  `Sort {
    sort_input = [ "/input/dewiki.words_4times", 0L, 64L ];
    sort_input_del = false;
    sort_output = "/testout";
    sort_id = 0;
  }
(*
  `Map {
      map_input = [ "/input/dewiki.words_4times", 0L, 64L ];
      map_output_prefix = "/testout";
      map_id = 0;
      map_best_hosts = []
    }
 *)
(*
  `Emap {
    emap_map = {
      map_input = [ "/input/dewiki.words_4times", 0L, 64L ];
      map_output_prefix = "/testout";
      map_id = 0;
      map_best_hosts = []
    };
    emap_output = [ `Tag "all", 0, job_conf#partitions-1 ]
  }
 *)

let ti =
  (object
     method req_id = 0
     method task_prefix = "/testlog"
     method partition = 0
   end
  )

let r =
  Mapred_task_exec.execute env job_conf job task ti

let () =
  match r with
    | `Ok _ ->
	print_endline "OK"
    | `Error s ->
	print_endline s
    | _ ->
	print_endline "not ok"
