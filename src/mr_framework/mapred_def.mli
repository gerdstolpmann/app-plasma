(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Defining the map/reduce job *)

class type mapred_env =
object
  method filesystem : Mapred_fs.filesystem
    (** Access to the cluster filesystem *)

  method shm_manager : Plasma_shm.shm_manager
    (** The manager for shared memory and large buffers *)

  method config : Mapred_config.mapred_config
    (** The map/reduce server configuration *)

  method config_file : Netplex_types.config_file
    (** The config file containing [config] *)

  method auth_ticket : string
    (** The authentication ticket *)

  method command_name : string
    (** The name of the started command *)

  method accumulate : Mapred_stats.stats -> unit
    (** Adds the passed counters to the current set of counters, e.g.
	to the task counters
     *)
end


type designation =
    [ `File | `Flat_dir | `Deep_dir ]
  (** How to determine the files of [input_dir]:

      - [`File]: Interpret [input_dir] as a single file (not as directory)
      - [`Flat_dir]: Take the files in [input_dir], except those that start
        with a dot or an underscore
      - [`Deep_dir]: Take the files in [input_dir] and all inner directories.
        Again files are ignore when they start with a dot or underscore.
        Directories starting with dot or underscore are completely ignored.

      In the future, there will be more types of designation, such as
      regular expression-based ones.
   *)


type phases =
    [ `Map | `Map_sort | `Map_sort_reduce ]
  (** Which phases are enabled:

      - [`Map]: Only the map phase is executed. The output directory will
        contain files [mapped_#_#] where "#" is a number.
      - [`Map_sort]: The mapped files are sorted. This generates files
        [sorted_#] where "#" is a number.
      - [`Map_sort_reduce]: The sorted files are shuffled and reduced.
        This generates files [partition_#] where "#" is a number.
        This is the default.
   *)


class type mapred_job_config =
object
  method name : string
    (** A name for identifying this job, e.g. in log files *)

  method job_id : string
    (** An automatically generated name. This can be considered as
	unique. It is not possible to override this name.
     *)

  method input_dir : string
    (** This plasma directory contains the input files *)

  method input_dir_designation : designation
    (** How [input_dir] is interpreted *)

  method output_dir : string
    (** This plasma directory will get output files. It should exist,
	and it should be empty
     *)

  method work_dir : string
    (** This plasma directory is used for temporary files. It should
	exist, and it should be empty
     *)

  method log_dir : string
    (** This plasma directory is used for log files. It should exist,
	and it should be empty
     *)

  method task_files : string list
    (** These files are copied at job start to the "local" directory
	for this job on all task nodes. This should be regular files
	only. The location of this local directory can be queried with
        {!Mapred_def.get_job_local_dir}.
     *)

  method bigblock_size : int
    (** Map/reduce processes files in units of bigblocks. The size of
	bigblocks can be chosen as multiples of the size of filesystem
	blocks. The value [bigblock_size] is in bytes. The maximum
	size of records (line length) is also [bigblock_size]. Reasonable
	values are in the multi-megabyte range, e.g. 16M.

	Certain file formats also require that bigblocks are multiples
	of 64K.
     *)

  method map_tasks : int
    (** The number of map tasks that should not be exceeded. It is tried
	to hit this number, but it may be possible that not enough map
	tasks can be generated.
     *)

  method merge_limit : int
    (** How many files are merged at most by a shuffle task *)

  method split_limit : int
    (** How many files are created by a shuffle task. This is not a
	strict limit - actually the scheduler plans with slightly
	larger split limits near the end of the job.  
     *)

  method partitions : int
    (** The number of partitions = number of reduce tasks *)

  method enhanced_mapping : int
    (** If >0, enhanced map tasks are created. This type of tasks
	also sorts and pre-partitions data. The int is the number
	of pre-partitions (must be <= [partitions]). 

	Increasing the number of pre-partitions reduces network
	traffic but makes it more likely that random disk seeks
	are needed to read data from disk.
     *)

  method phases : phases
    (** Which phases are enabled *)

  method map_whole_files : bool
    (** Whether only whole files are passed to a map job. If false, files
        to map can be split into parts
     *)

  method custom : string -> string
    (** Get a custom parameter or raise [Not_found] *)
end


(** Sorters are filled with records, and when the sorter is full, the
    records are sorted by key, and written to a destination.

    The amount of data that can be put into a sorter is limited by
    [max_sort_size]. It is the task of the caller to stick to this
    limit.
 *)
class type sorter =
object
  (** Informational methods *)

  method name : string
    (** The name of the sorter, for debugging *)

  method eff_sort_limit : int
    (** The sort limit after applying the factor, in bytes *)

  method sort_lines_limit : int -> int
    (** Maximum number of lines for a given sort limit in bytes *)

  (** Configuration *)

  method set_key_extraction : (string -> int*int) -> unit
    (** Sets the function for extracting the key. This is normally set
	to what [extract_key] returns, see below.
     *)

  (** Controlling methods *)

  method put_records : string Queue.t -> unit
    (** Hands over the records in the queue to the sorter. The queue is
	empty when the method returns.
     *)

  method sort : Mapred_io.record_writer -> unit
    (** Sorts the records, and writes them to the passed writer.
	After [sort], the sorter is cleared, and can be used for new
	data.
     *)

  method sort_time : float
    (** Returns the duration of the last sort (w/o writing) in seconds *)

  method close : unit -> unit
    (** Deallocates resources *)

  (** Exposition of the ordering *)

  method hash : string -> int -> int -> int
    (** [hash record key_index key_len]: Takes
	[key = String.sub record index key_len] as key, and returns
	the hash value. (If the sorter does not work with hashes, the
	method must return 0 instead.)

	Hash values must be between 0 and 2^30-1 (i.e. 30 bits max).
     *)

  method cmp : string -> int -> int -> string -> int -> int -> int
    (** [cmp s1 i1 l1 s2 i2 l2]: Compare two keys as the sorter does.
	The first key is passed in by the string [s1] and the start index
	[i1] and the length [l1]. 
	The other key is made available via [s2], [i2], and [l2].

	[cmp] is only called if [cmphash] returns 0.

	It is essential that [cmp] does not store the passed strings [s1]
	and [s2] anywhere.
     *)

end


class type task_info =
object
  method req_id : int
    (** The request ID. This is different for each task of a job *)

  method task_prefix : string
    (** This filename prefix is used for log files *)

  method partition : int
    (** The partition. Only available at reduce time *)
end


class type mapred_job =
object
  method custom_params : string list
    (** The list of allowed custom parameters *)

  method check_config : mapred_env -> mapred_job_config -> unit
    (** Check the config. If not ok, this method can raise exceptions to
	stop everything
     *)

  method pre_job_start : mapred_env -> mapred_job_config -> unit
    (** This is run by the job process before the first task is started *)

  method post_job_finish : mapred_env -> mapred_job_config -> unit
    (** This is run by the job process after the last task is finished *)

  method input_record_io : mapred_env -> mapred_job_config -> 
                             Mapred_io.record_rw_factory
    (** How to split the input file into records *)

  method output_record_io : mapred_env -> mapred_job_config -> 
                             Mapred_io.record_rw_factory
    (** How to write the output file from records. This includes all files
	in the output directory.
     *)

  method internal_record_io : mapred_env -> mapred_job_config -> 
                                Mapred_io.record_rw_factory
    (** How to represent records for internal files *)

  method map : mapred_env -> mapred_job_config ->
               task_info -> Mapred_io.record_reader ->
                 Mapred_io.record_writer -> unit
    (** The mapper reads records, maps them, and writes them into a
	second file.
     *)

  method sorter : mapred_env -> mapred_job_config -> float -> sorter
    (** This is normally set to one of the sorters defined in
	{!Mapred_sorters}. The float is the factor for the sort buffer,
	and it should be between 0.0 and 1.0.
     *)

  method extract_key : mapred_env -> mapred_job_config -> string -> int*int
    (** Extracts the key from a record, and returns the position as
	pair [(index,len)]. Here, [index] is the byte in the record
	where the key starts, and [len] is the length of the key in bytes.

	This method is always called by
	first evaluating [let f = job#extract_key me jc], and then
	calling [f line] for each input line. Because of this, it is
	possible to factor initializations out as in

	{[
	   method extract_key me jc =
              ...; (* init stuff *)
              (fun line -> ...  (* real extraction *) )
	]}

	Before Plasma-0.6, [extract_key] returned the key directly as
	string.
     *)

  method partition_of_key : mapred_env -> mapred_job_config -> string -> int -> int -> int
    (** [partition_of_key me jc s p l]:
       Determines the partition of a key (which is supposed to occupy the
	range [p] to [p+l-1] of [s]). Can be something simple like
	[(Hashtbl.hash key) mod partitions], or something more
	elaborated. This method is always called by
	first evaluating [let f = job#partition_of_key me jc], and then
	calling [f s p l] for each input line. Because of this, it is
	possible to factor initializations out as in

	{[
	   method partition_of_key me jc =
              ...; (* init stuff *)
              (fun s p l -> ...  (* real extraction *) )
	]}

     *)

  method reduce : mapred_env ->  mapred_job_config ->
                 task_info -> Mapred_io.record_reader -> 
                    Mapred_io.record_writer -> unit
    (** The reducer reads all the records of one partition, and puts them
	into an output file.
     *)

  method combine : mapred_env ->  mapred_job_config -> task_info ->
                   (Mapred_io.record_reader -> Mapred_io.record_writer -> unit)
		       option
    (** The optional combiner is called for the internal shuffle passes.
	The reader gets the already merged input records (i.e. it reads
	the records in sorted order). The combiner can now shrink the
	data if possible, and writer them to the writer.

	Note that Plasma allows it that the combiner gets data from
	several partitions!

	If no combiner is needed, just define this method as
	{[ method combine _ _ _ = None ]}

	In this case, the internal shuffles just copy the input to the
	output.
     *)

end


val get_rc : mapred_env -> int -> Mapred_io.record_config
  (** [get_rc me bigblock_size]: Returns a record config for the given
      environment and the suggested size of the bigblock
   *)

val get_job_local_dir : mapred_env -> mapred_job_config -> string
  (** Returns the directory in the local filesystem where the files
      configured in [task_files] can be found. The task implementations
      can use this directory also for other purposes, e.g. temporary
      files. The directory exists for the lifetime of the job.
      Note that this directory is only created when needed.
      Same as {!Mapred_taskfiles.taskfile_manager.local_directory}.
   *)

val get_job_log_dir : mapred_env -> mapred_job_config -> string
  (** Returns the directory in the local filesystem where
      log files can be placed. These files are automatically moved
      to the [log_dir] in PlasmaFS when the job is finished.
      Note that this directory is only created when needed.
      Same as {!Mapred_taskfiles.taskfile_manager.log_directory}.
   *)
