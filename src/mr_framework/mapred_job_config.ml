(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

let dlogf = Plasma_util.dlogf

type m_job_config =
    { std : (string * Netplex_types.param_value) list;
      custom : (string * string) list
    }

let lookup_string mjc n =
  try 
    match List.assoc n mjc.std with
      | `String s -> s
      | _ -> assert false
  with Not_found -> assert false


let lookup_int mjc n =
  try 
    match List.assoc n mjc.std with
      | `Int s -> s
      | _ -> assert false
  with Not_found -> assert false


let lookup_bool mjc n =
  try 
    match List.assoc n mjc.std with
      | `Bool b -> b
      | _ -> assert false
  with Not_found -> assert false


let designation_of_string s =
  match String.lowercase s with
    | "file" -> `File
    | "flat_dir" -> `Flat_dir
    | "deep_dir" -> `Deep_dir
    | _ -> 
	failwith 
	  ("Mapred_job_config.designation_of_string: Unknown designation: " 
	     ^ s)

let string_of_designation d =
  match d with
    | `File -> "file"
    | `Flat_dir -> "flat_dir"
    | `Deep_dir -> "deep_dir"


let phases_of_string s =
  match String.lowercase s with
    | "map" -> `Map
    | "map_sort" -> `Map_sort
    | "map_sort_reduce" -> `Map_sort_reduce
    | _ ->
	failwith 
	  ("Mapred_job_config.phases_of_string: Unknown phases: " 
	     ^ s)
	

let string_of_phases =
  function
    | `Map -> "map"
    | `Map_sort -> "map_sort"
    | `Map_sort_reduce -> "map_sort_reduce"




let mapred_job_config mjc : Mapred_def.mapred_job_config =
  let name = lookup_string mjc "name" in
  let job_id = lookup_string mjc "job_id" in
  let input_dir = lookup_string mjc "input_dir" in
  let input_dir_designation = 
    designation_of_string(lookup_string mjc "input_dir_designation") in
  let output_dir = lookup_string mjc "output_dir" in
  let work_dir = lookup_string mjc "work_dir" in
  let log_dir = lookup_string mjc "log_dir" in
  let task_files = Pcre.split (lookup_string mjc "task_files")  in
  let bigblock_size = lookup_int mjc "bigblock_size" in
  let map_tasks = lookup_int mjc "map_tasks" in
  let merge_limit = lookup_int mjc "merge_limit" in
  let split_limit = lookup_int mjc "split_limit" in
  let partitions = lookup_int mjc "partitions" in  
  let map_whole_files = lookup_bool mjc "map_whole_files" in
  let enhanced_mapping = lookup_int mjc "enhanced_mapping" in
  let phases = 
    phases_of_string(lookup_string mjc "phases") in
  (object
     method name = name
     method job_id = job_id
     method input_dir = input_dir
     method input_dir_designation = input_dir_designation
     method output_dir = output_dir
     method work_dir = work_dir
     method log_dir = log_dir
     method task_files = task_files
     method bigblock_size = bigblock_size
     method map_tasks = map_tasks
     method merge_limit = merge_limit
     method split_limit = split_limit
     method partitions = partitions
     method enhanced_mapping = enhanced_mapping
     method map_whole_files = map_whole_files
     method phases = phases
     method custom n = List.assoc n mjc.custom
   end
  )

let params =
  [ "name", `String;
    "job_id", `String;
    "input_dir", `String;
    "input_dir_designation", `String;
    "output_dir", `String;
    "work_dir", `String;
    "log_dir", `String;
    "task_files", `String;   (* space-separated *)
    "bigblock_size", `Int;
    "map_tasks", `Int;
    "merge_limit", `Int;
    "split_limit", `Int;
    "partitions", `Int;
    "enhanced_mapping", `Int;
    "map_whole_files", `Bool;
    "phases", `String
  ]

let n_params = List.length params

let value_of_string n t s =
  try
    match t with
      | `String -> `String s
      | `Int -> `Int (int_of_string s)
      | `Float -> `Float (float_of_string s)
      | `Bool -> `Bool (bool_of_string s)
  with
    | _ ->
	failwith ("Job config parameter '" ^ n ^ "' is passed with wrong " ^ 
		    "type")

let extract_job_config (cf:Netplex_types.config_file) args custom_params =
  let name = "mr_" ^ (Digest.to_hex
			(Digest.string
			   (Unix.gethostname() ^ "/" ^ 
			      string_of_int (Unix.getpid()) ^ "/" ^ 
			      string_of_float (Unix.gettimeofday())))) in
  dlogf
    "Mapred_job_config: file=%s" cf#filename;
  let args =
    [ "job_id", name ] @ args in

  let defaults =
    [ "name", name;
      "task_files", "";
      "bigblock_size", string_of_int (16 * 1024 * 1024);
      "map_tasks", string_of_int 0;
      "merge_limit", string_of_int 4;
      "split_limit", string_of_int 4;
      "enhanced_mapping", "0";
      "partitions", "1";
      "input_dir", "[uninitialized input_dir]";
      "input_dir_designation", "flat_dir";
      "output_dir", "[uninitialized output_dir]";
      "log_dir", "[uninitialized log_dir]";
      "work_dir", "[uninitialized work_dir]";
      "map_whole_files", "false";
      "phases", "map_sort_reduce";
    ] in
  
  let section_lz = lazy (
    match cf#resolve_section cf#root_addr "mapredjob" with
      | [sect] -> sect
      | [] ->
	  failwith "Missing section 'mapredjob' in job config file"
      | _ ->
	  failwith "Ambiguous section 'mapredjob' in job config file" 
  ) in
  
  let mjc_std =
    List.map
      (fun (n,t) ->
	 try
	   let v = List.assoc n args in
	   (n, value_of_string n t v)
	 with
	   | Not_found ->
	       let sect = Lazy.force section_lz in
	       try
		 let addr = cf#resolve_parameter sect n in
		 match t with
		   | `String -> (n, `String (cf#string_param addr))
		   | `Int -> (n, `Int (cf#int_param addr))
		   | `Float -> (n, `Float (cf#float_param addr))
		   | `Bool -> (n, `Bool (cf#bool_param addr))
	       with
		 | Not_found ->
		     try
		       let v = List.assoc n defaults in
		       (n, value_of_string n t v)
		     with
		       | Not_found ->
			   failwith
			     ("Job config parameter '" ^ n ^ "' not found")
      )
      params in

  let mjc_custom =
    List.flatten
      (List.map
	 (fun n ->
	    try
	      let v = List.assoc n args in
	      [n, v]
	    with
	      | Not_found ->
		  let sect = Lazy.force section_lz in
		  try
		    let addr = cf#resolve_parameter sect n in
		    [n, cf#string_param addr]
		  with
		    | Not_found -> []
	 )
	 custom_params) in

  let mjc =
    { std = mjc_std;
      custom = mjc_custom;
    } in

  let jc = mapred_job_config mjc in

  (jc, mjc)


let test_job_config() =
  let cf = 
    Netplex_config.repr_config_file 
      "internal_test_config" 
      (`Section("root", [ `Section("mapredjob", [])])) in
  snd
    (extract_job_config 
       cf 
       [ "input_dir", "/input_test";
	 "output_dir", "/output_test";
	 "work_dir", "/work_test";
	 "log_dir", "/log_test";
	 "partitions", "1";
       ]
       [])
      

let update_job_config ?name ?input_dir ?input_dir_designation
                      ?output_dir ?work_dir ?log_dir
                      ?task_files ?bigblock_size ?map_tasks ?merge_limit
                      ?split_limit ?partitions ?enhanced_mapping ?phases
		      ?custom ?map_whole_files
                      mjc =
  let upd_string n v_opt =
    match v_opt with
      | None -> (n, List.assoc n mjc.std)
      | Some x -> (n, `String x) in

  let upd_int n v_opt =
    match v_opt with
      | None -> (n, List.assoc n mjc.std)
      | Some x -> (n, `Int x) in

  let upd_bool n v_opt =
    match v_opt with
      | None -> (n, List.assoc n mjc.std)
      | Some x -> (n, `Bool x) in

  let task_files_s =
    match task_files with
      | None -> None
      | Some l -> Some(String.concat " " l) in

  let input_dir_ds =
    match input_dir_designation with
      | None -> None
      | Some d -> Some(string_of_designation d) in

  let phases_s =
    match phases with
      | None -> None
      | Some ph -> Some(string_of_phases ph) in

  let new_std =
    try
      [ upd_string "name" name;
	upd_string "job_id" None;
	upd_string "input_dir" input_dir;
	upd_string "input_dir_designation" input_dir_ds;
	upd_string "output_dir" output_dir;
	upd_string "work_dir" work_dir;
	upd_string "log_dir" log_dir;
	upd_string "task_files" task_files_s;
	upd_int "bigblock_size" bigblock_size;
	upd_int "map_tasks" map_tasks;
	upd_int "merge_limit" merge_limit;
	upd_int "split_limit" split_limit;
	upd_int "partitions" partitions;
	upd_int "enhanced_mapping" enhanced_mapping;
        upd_bool "map_whole_files" map_whole_files;
	upd_string "phases" phases_s
      ] 
    with Not_found -> assert false in
  assert(List.length new_std = n_params);

  let new_custom =
    match custom with
      | None -> mjc.custom
      | Some l -> l in

  { std = new_std;
    custom = new_custom
  }


let marshal mjc =
  Marshal.to_string mjc [Marshal.No_sharing]

let unmarshal s =
  Marshal.from_string s 0
