(* $Id$ *)

(* This adds the following quotation:
 
   <:rfun< p1 p2 p3 ... @ q1 q2 q3 ... -> expr >>

   This registers the function under an automatically generated name.
   The function is [fun p1 p2 p3 ... q1 q2 q3 ... -> expr].
   The parameters before "@" are local parameters, and the parameters
   after "@" are remote parameters. The function will have type

   P1 -> P2 -> P3 -> ... (Q1 -> Q2 -> Q3 -> ... T) rfun

   when Pi is the type of pi, and Qi the type of qi.

   Note that the function cannot be polymorphic!

 *)

open Camlp4.PreCast
open Syntax

let rfun_def = Gram.Entry.mk "rfun_def"

(* Original grammar entry for "fun":

	 fun_def:
	      [ [ p = labeled_ipatt; (w, e) = fun_def_cont ->
		    <:expr< fun [ $p$ when $w$ -> $e$ ] >> ] ]
	    ;
	 fun_def_cont:
	      [ RIGHTA
		[ p = labeled_ipatt; (w,e) = SELF -> 
                    (<:expr<>>, <:expr< fun [ $p$ when $w$ -> $e$ ] >>)
		| "when"; w = expr; "->"; e = expr -> 
                    (w, e)
		| "->"; e = expr -> 
                    (<:expr<>>, e) 
                ]
              ]
	    ;
 *)


EXTEND Gram
  GLOBAL: rfun_def;

  rfun_def:
    [ [ lp = labeled_ipatt; (lp_list, rp_list, e) = rfun_def ->
         (lp :: lp_list, rp_list, e)
      | "@"; (rp_list, e) = rfun_def_1 ->
         ([], rp_list, e)
      ]
    ];

  rfun_def_1:
    [ [ rp = labeled_ipatt; (rp_list,e) = rfun_def_2 ->
        (rp :: rp_list, e)
      ]
    ];

  rfun_def_2:
    [ [ rp = labeled_ipatt; (rp_list,e) = rfun_def_2 ->
         (rp :: rp_list, e)
      | "->"; e = expr ->
         ([], e)
      ]
    ];
END


let n = ref 0

let expand _loc _  s =
  let (lp_list, rp_list, e) =
    Gram.parse_string rfun_def _loc s in

  let file = Loc.file_name _loc in
  let line = Loc.start_line _loc in

  let name =
    "_" ^ 
    Digest.to_hex
      (Digest.string
	 ( (string_of_float (Unix.gettimeofday())) ^ 
	     Marshal.to_string _loc [] ^ "/" ^ string_of_int !n)) ^ "_" ^ 
      file ^ "_line_" ^ string_of_int line  in
  incr n;

  let rfun1_code =
    List.fold_right
      (fun pat acc ->
	 <:expr< fun $pat$ -> $acc$ >>
      )
      (lp_list @ rp_list)
      e in

  let rfun2_code =
    <:expr< Mapred_rfun.register $str:name$ $rfun1_code$ >> in

  let rec mk_rfun3_code lp_list =
    match lp_list with
      | [] ->
	  <:expr< f >>
      | lp :: lp_list' ->
	  let subexpr = mk_rfun3_code lp_list' in
	  <:expr< 
             fun x -> 
                let f = Mapred_rfun.apply_partially f x in
                $subexpr$
          >> in
  
  let rfun3_code = mk_rfun3_code lp_list in

  <:expr<
    let f = $rfun2_code$ in
    $rfun3_code$
  >>




let () =
  Quotation.add
    "rfun" Syntax.Quotation.DynAst.expr_tag expand
