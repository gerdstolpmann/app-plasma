(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

let tab_split s =
  try
    let p = String.index s '\t' in
    (String.sub s 0 p, String.sub s (p+1) (String.length s - p - 1))
  with
    | Not_found -> (s, "")


let tab_split_key s =
  try
    let p = String.index s '\t' in
    String.sub s 0 p
  with
    | Not_found -> s


let tab_split_key_range s =
  try
    let p = String.index s '\t' in
    (0, p)
  with
    | Not_found -> (0, String.length s)


let tab2_split s =
  try
    let p1 = String.index s '\t' in
    let p2 = String.index_from s (p1+1) '\t' in
    let key = String.sub s 0 p1 in
    let p = int_of_string (String.sub s (p1+1) (p2-p1-1)) in
    let value = String.sub s (p2+1) (String.length s - p2 - 1) in
    (key,p,value)
  with
    | _ ->
	failwith "Mapred_split.tab2_split"

let tab2_split_key s =
  try
    let p1 = String.index s '\t' in
    let key = String.sub s 0 p1 in
    key
  with
    | _ ->
	failwith "Mapred_split.tab2_split_key"


let tab2_split_key_range s =
  try
    let p1 = String.index s '\t' in
    (0, p1)
  with
    | _ ->
	failwith "Mapred_split.tab2_split_key_range"


let tab2_split_key_partition s =
  try
    let p1 = String.index s '\t' in
    let p2 = String.index_from s (p1+1) '\t' in
    let key = String.sub s 0 p1 in
    let p = int_of_string (String.sub s (p1+1) (p2-p1-1)) in
    (key,p)
  with
    | _ ->
	failwith "Mapred_split.tab2_split_key_partition"


let tab2_split_key_partition_range s =
  try
    let p1 = String.index s '\t' in
    let p2 = String.index_from s (p1+1) '\t' in
    (0, p2)
  with
    | _ ->
	failwith "Mapred_split.tab2_split_key_partition_range"


let tab2_get_partition s =
  try
    let p1 = String.index s '\t' in
    let l = String.length s in
    int_of_string (String.sub s (p1+1) (l - p1 - 1))
  with
    | _ ->
	failwith "Mapred_split.tab2_get_partition"


