(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

type bgreader =
    { fs : Mapred_fs.filesystem;
      name : string;
      copy_out_to_buf : int64 -> Netsys_mem.memory -> int -> unit;
      mutex : Mutex.t;
      cond : Condition.t;
      bsize : int;

      (* These fields are about the fg buffer, and must not be touched by
	 the bg thread,
       *)
      mutable fgbuf_start : int64;
      mutable fgbuf : Netsys_mem.memory;
      mutable fgbuf_len : int;
      mutable thread : Thread.t;

      (* These fields are protected by the mutex *)
      mutable enabled : bool;
      mutable end_block : int64;
      mutable bgbuf : Netsys_mem.memory;
      mutable bgbuf_start : int64;
      mutable bgbuf_len : int;
      mutable bgbuf_state : [ `Idle | `Reading | `Valid | `Error of exn ];
    }


let thread_body bgrd =
  Mutex.lock bgrd.mutex;

  while bgrd.enabled do
    while bgrd.enabled && bgrd.bgbuf_state <> `Reading do
      Condition.wait bgrd.cond bgrd.mutex
    done;
    if bgrd.enabled then (
      let buflen = Bigarray.Array1.dim bgrd.bgbuf in
      let bufnum = buflen / bgrd.bsize in
      let end_block = bgrd.end_block in
      let len_blocks = 
	Int64.to_int
	  (min
	     (Int64.of_int bufnum)
	     (Int64.sub end_block bgrd.bgbuf_start)) in
      let len = len_blocks * bgrd.bsize in
      let pos = Int64.mul bgrd.bgbuf_start (Int64.of_int bgrd.bsize) in
      Mutex.unlock bgrd.mutex;

      try
	ignore(bgrd.copy_out_to_buf pos bgrd.bgbuf len);
	Mutex.lock bgrd.mutex;
	if bgrd.end_block = end_block then (
	  bgrd.bgbuf_len <- len_blocks;
	  bgrd.bgbuf_state <- `Valid;
	  Condition.signal bgrd.cond;
	)
	(* else: [extend] was called in the meantime, so redo *)
      with
	| error ->
	    Mutex.lock bgrd.mutex;
	    bgrd.bgbuf_len <- 0;
	    bgrd.bgbuf_state <- `Error error;
	    Condition.signal bgrd.cond;
    )
  done;

  bgrd.fs # close();
  Netsys_mem.memory_unmap_file bgrd.bgbuf;
  Mutex.unlock bgrd.mutex


let read bgrd =
  if not bgrd.enabled then
    failwith "Mapred_io_bgreader: no longer enabled";

  Mutex.lock bgrd.mutex;

  if bgrd.bgbuf_state = `Idle then (
    bgrd.bgbuf_state <- `Reading;
    Condition.signal bgrd.cond;
  );

  while bgrd.bgbuf_state = `Reading || bgrd.bgbuf_state = `Idle do
    Condition.wait bgrd.cond bgrd.mutex
  done;
   
  match bgrd.bgbuf_state with
    | `Valid ->
	let bgbuf = bgrd.bgbuf in
	let fgbuf = bgrd.fgbuf in
	bgrd.fgbuf_start <- bgrd.bgbuf_start;
	bgrd.fgbuf_len <- bgrd.bgbuf_len;
	bgrd.fgbuf <- bgbuf;
	bgrd.bgbuf_start <- 
	  Int64.add bgrd.bgbuf_start (Int64.of_int bgrd.bgbuf_len);
	bgrd.bgbuf_len <- 0;
	bgrd.bgbuf <- fgbuf;
	bgrd.bgbuf_state <- (* `Idle *) `Reading;
	Condition.signal bgrd.cond;
	Mutex.unlock bgrd.mutex;
	bgrd.fgbuf_len
    | `Error error ->
	Mutex.unlock bgrd.mutex;
	raise error
    | `Reading | `Idle ->
	assert false


let extend bgrd new_end =
  Mutex.lock bgrd.mutex;
  bgrd.end_block <- new_end;
  (* Condition.signal bgrd.cond; *)
  Mutex.unlock bgrd.mutex


let buffer bgrd = bgrd.fgbuf
let buffer_block bgrd = bgrd.fgbuf_start
let buffer_len bgrd = bgrd.fgbuf_len


let close bgrd =
  Mutex.lock bgrd.mutex;
  bgrd.enabled <- false;
  Condition.signal bgrd.cond;
  Mutex.unlock bgrd.mutex;
  Thread.join bgrd.thread


let create fs name startblock endblock bufnum =
  let fs_copy = fs#open_filesystem() in
  let bsize = fs_copy#blocksize name in
  let fgbuf = Netsys_mem.alloc_memory_pages (bufnum * bsize) in
  let bgbuf = Netsys_mem.alloc_memory_pages (bufnum * bsize) in

  let bgrd =
    { fs = fs_copy;
      name;
      copy_out_to_buf = fs_copy # copy_out_to_buf name;
      mutex = Mutex.create();
      cond = Condition.create();
      bsize;
      fgbuf_start = startblock;
      fgbuf;
      fgbuf_len = 0;
      enabled = true;
      end_block = endblock;
      bgbuf;
      bgbuf_start = startblock;
      bgbuf_len = 0;
      bgbuf_state = `Reading;
      thread = Thread.self()
    } in
  let t =
    Thread.create thread_body bgrd in
  bgrd.thread <- t;
  bgrd

