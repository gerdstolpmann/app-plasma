(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type mapred_env =
object
  method filesystem : Mapred_fs.filesystem
  method shm_manager : Plasma_shm.shm_manager
  method config : Mapred_config.mapred_config
  method config_file : Netplex_types.config_file
  method auth_ticket : string
  method command_name : string
  method accumulate : Mapred_stats.stats -> unit
end

type designation =
    [ `File | `Flat_dir | `Deep_dir ]

type phases =
    [ `Map | `Map_sort | `Map_sort_reduce ]

class type mapred_job_config =
object
  method name : string
  method job_id : string
  method input_dir : string
  method input_dir_designation : designation
  method output_dir : string
  method work_dir : string
  method log_dir : string
  method task_files : string list
  method bigblock_size : int
  method map_tasks : int
  method merge_limit : int
  method split_limit : int
  method partitions : int
  method enhanced_mapping : int
  method phases : phases
  method map_whole_files : bool
  method custom : string -> string
end


class type sorter =
object
  method name : string
  method eff_sort_limit : int
  method sort_lines_limit : int -> int
  method set_key_extraction : (string -> int*int) -> unit
  method put_records : string Queue.t -> unit
  method sort : Mapred_io.record_writer -> unit
  method sort_time : float
  method close : unit -> unit
  method hash : string -> int -> int -> int
  method cmp : string -> int -> int -> string -> int -> int -> int
end


class type task_info =
object
  method req_id : int
  method task_prefix : string
  method partition : int
end


class type mapred_job =
object
  method custom_params : string list
  method check_config : mapred_env -> mapred_job_config -> unit
  method pre_job_start : mapred_env -> mapred_job_config -> unit
  method post_job_finish : mapred_env -> mapred_job_config -> unit
  method input_record_io : mapred_env -> mapred_job_config -> 
                             Mapred_io.record_rw_factory
  method output_record_io : mapred_env -> mapred_job_config -> 
                             Mapred_io.record_rw_factory
  method internal_record_io : mapred_env -> mapred_job_config -> 
                                Mapred_io.record_rw_factory
  method map : mapred_env -> mapred_job_config ->
               task_info -> Mapred_io.record_reader ->
                 Mapred_io.record_writer -> unit
  method sorter : mapred_env -> mapred_job_config -> float -> sorter
  method extract_key : mapred_env -> mapred_job_config -> string -> int*int
  method partition_of_key : mapred_env -> mapred_job_config -> 
                              string -> int -> int -> int
  method reduce : mapred_env ->  mapred_job_config ->
                 task_info -> Mapred_io.record_reader -> 
                    Mapred_io.record_writer -> unit
  method combine : mapred_env ->  mapred_job_config -> task_info ->
                   (Mapred_io.record_reader -> Mapred_io.record_writer -> unit)
		       option
end


let get_rc me bb : Mapred_io.record_config =
  ( object
      method bigblock_size = bb
      method mr_buffer_size = me#config#mr_buffer_size
      method mr_buffer_size_tight = me#config#mr_buffer_size_tight
    end
  )


(* Also see Mapred_taskfiles for the following *)

let get_job_local_dir me mjc =
  me#config#mr_task_tmpdir ^ "/" ^ mjc#job_id ^ "_local"

let get_job_log_dir me mjc =
  me#config#mr_task_tmpdir ^ "/" ^ mjc#job_id ^ "_log"
