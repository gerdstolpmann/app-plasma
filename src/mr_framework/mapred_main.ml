(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

type spec =
       string * 
       string * 
       (Mapred_def.mapred_env -> Mapred_def.mapred_job) *
       (Mapred_def.mapred_env -> Mapred_def.mapred_job -> unit)


let dlogf = Plasma_util.dlogf

let this =
  Filename.basename Sys.argv.(0)

let usage spec =
  eprintf "usage: %s [-conf <configfile>] <mode> [-help|options]\n" this;
  prerr_endline "";
  prerr_endline "  available modes:";
  prerr_endline "  - start_task_servers: install and start task servers";
  prerr_endline "  - stop_task_servers: stop task servers";
  List.iter
    (fun (cmd,usg,_,_) ->
       if cmd <> "" && cmd.[0] <> '_' then
	 prerr_endline ("  - " ^ cmd ^ ": " ^ usg)
    )
    spec


let open_cluster conf esys =
  Plasma_client.open_cluster 
    conf#nn_clustername
    (List.map Plasma_util.parse_host_port conf#nn_nodes)
    esys


let with_admin_client tmpdir f =
  let sockdir = tmpdir ^ "/sockdir" in
  let socket =
    Filename.concat sockdir "netplex.controller/admin" in
  let conn =
    Netplex_sockserv.any_file_client_connector socket in
  let client =
    Netplex_ctrl_clnt.Admin.V2.create_client 
      conn
      Rpc.Tcp in
  try
    let r = f client in
    Rpc_client.shut_down client;
    r
  with
    | error ->
	Rpc_client.shut_down client;
	raise error


let kill tmpdir =
  (* First try a Netplex-level shutdown: *)
  ( try
      with_admin_client tmpdir
	(fun client ->
	   ignore(Netplex_ctrl_clnt.Admin.V2.system_shutdown client ())
	)
    with
      | _error ->
	  ()
  );
  let n = ref 0 in
  let ok = ref false in
  while not !ok && !n < 600 do
    ( try
	with_admin_client tmpdir
	  (fun client ->
	     Netplex_ctrl_clnt.Admin.V2.ping client ();
	  )
      with _ -> ok := true
    );
    if not !ok then (
      Netsys.sleep 0.1;
      incr n
    )
  done;
  if not !ok then (
    (* Kill the process: *)
    try
      let f = open_in (tmpdir ^ "/task_server.pid") in
      let pid_s = input_line f in
      close_in f;
      let pid = int_of_string pid_s in
      Unix.kill pid 15;
      n := 0;
      while not !ok && !n < 600 do
	( try
	    with_admin_client tmpdir
	      (fun client ->
		 Netplex_ctrl_clnt.Admin.V2.ping client ();
	  )
	  with _ -> ok := true
	);
	if not !ok then (
	  Netsys.sleep 0.1;
	  incr n
	)
  done;
    with
      | _error ->
	  () 
  );
  if not !ok then
    Netlog.logf `Warning
      "Could not terminate task server"


let start_task_servers cf_file =
  let level = ref `Info in
  let nodeploy = ref false in
  Arg.parse
    [ "-debug", Arg.Unit (fun () -> level := `Debug),
      "    Enable Plasma debug messages in task server";

      "-nodeploy", Arg.Set nodeploy,
      "    Do not install new files";
    ]
    (fun s -> raise(Arg.Bad ("Extra argument: "  ^ s)))
    (sprintf "usage: %s start_task_servers [options]" this);

  let cf = Netplex_config.read_config_file cf_file in
  let conf = Mapred_config.extract_config cf in

  let ttmp = conf # mr_task_tmpdir in
  let qttmp = Filename.quote ttmp in
      
  (* Get RPC auth info: *)
  let (rpc_user,rpc_pw) =
    if conf#nn_disabled then
      ("anybody", "insecure")
    else
      let c = open_cluster conf (Unixqueue.create_unix_event_system()) in
      try
	Plasma_client.configure_auth_daemon c;
	let a = Mapred_auth.get_client_auth c in
	let (u,p) = Plasma_ug.rpc_login_of_auth_ticket a in
	Plasma_client.close_cluster c;
	(u,p)
      with
	| error ->
	    Plasma_client.close_cluster c;
	    raise error in
  
  (* Kill first *)
  List.iter
    (fun hostname ->
       let cmd =
	 Mapred_taskfiles.ssh_cmd
	   hostname
	   (sprintf "%s/task_server \
                       -log %s/task_server.stderr \
                       _plasma_kill_task_server -tmpdir %s"
	      qttmp qttmp qttmp) in
       let code = Sys.command cmd in
       if code <> 0 then
	 prerr_endline ("Exec failed: " ^ cmd);
    )
    conf#mr_task_nodes;
  
  let cleanup = ref [] in
  ( try

      if not !nodeploy then (

	let bind_addr =
	  if conf#nn_disabled then
	    "127.0.0.1"
	  else
	    "0.0.0.0" in

	(* Generate config file of the task server: *)
	let n1, f = Filename.open_temp_file "plasmamr_task" ".conf" in
	cleanup := (fun () -> close_out f; Sys.remove n1) :: !cleanup;
	
	fprintf f "netplex {\n";
	fprintf f "  controller {\n";
	fprintf f "    socket_directory = \"%s/sockdir\";\n" ttmp;
	fprintf f "    max_level = \"%s\";\n" (Netlog.string_of_level !level);
	fprintf f "    logging {\n";
	fprintf f "       type = \"multi_file\";\n";
	fprintf f "       directory = \"%s\";\n" ttmp;
	fprintf f "       file { file = \"task_server_debug.log\"; max_level = \"debug\" };\n";
	fprintf f "       file { file = \"task_server_info.log\"; max_level = \"info\" };\n";
	fprintf f "       file { file = \"task_server_crit.log\"; max_level = \"crit\" }\n";
	fprintf f "    }\n";
	fprintf f "  };\n";
	fprintf f "  service {\n";
	fprintf f "    name = \"Mapred_task_server\";\n";
	fprintf f "    protocol {\n";
	fprintf f "      name = \"RPC\";\n";
	fprintf f "      address {\n";
	fprintf f "        type = \"internet\";\n";
	fprintf f "        bind = \"%s:%d\"\n" bind_addr conf#mr_task_port;
	fprintf f "      }\n";
	fprintf f "    };\n";
	fprintf f "    processor { \n";
	fprintf f "      type = \"mapred_task_server\"\n";
	fprintf f "    };\n";
	fprintf f "    workload_manager {\n";
	fprintf f "      type = \"dynamic\";\n";
	fprintf f "      max_jobs_per_thread = 1;\n";
	fprintf f "      min_free_jobs_capacity = 1;\n";
	fprintf f "      max_free_jobs_capacity = 1;\n";
	fprintf f "      max_threads = 100\n";
	fprintf f "    }\n";
	fprintf f "  };\n";
	fprintf f "  access {\n";
	fprintf f "    user { name = \"%s\"; password_file = \"task_server.pw\" };\n" rpc_user;
	fprintf f "  }\n";
	fprintf f "}\n";
	close_out f;
	
	let n2, f2 = Filename.open_temp_file "plasmamr_task" ".pw" in
	cleanup := (fun () -> close_out f2; Sys.remove n2) :: !cleanup;
	Unix.fchmod (Unix.descr_of_out_channel f2) 0o600;
	output_string f2 rpc_pw;
	output_string f2 "\n";
	close_out f2;
	
	List.iter
	  (fun hostname ->
	     let cmd = 
	       Mapred_taskfiles.ssh_cmd
		 hostname
		 (sprintf "mkdir -p %s" 
		    (Filename.quote ttmp)) in
	     let code = Sys.command cmd in
	     if code <> 0 then
	       failwith ("Exec failed: " ^ cmd);
	  )
	  conf#mr_task_nodes;
	
	let files_to_copy =
	  [ n1,           "task_server.conf";
	    n2,           "task_server.pw";
	    Sys.argv.(0), "task_server";
	    cf_file,      "mapred.conf"
	  ] in
	List.iter
	  (fun hostname ->
	     List.iter
	       (fun (localname, remotename) ->
		  let cmd = 
		    Mapred_taskfiles.scp_to_cmd
		      [localname]
		      hostname
		      (ttmp ^ "/" ^ remotename) in
		  let code = Sys.command cmd in
		if code <> 0 then
		  failwith ("Exec failed: " ^ cmd);
	       )
	       files_to_copy;
	  )
	  conf#mr_task_nodes;
      );	

      (* NB the following command must be both executable as local command
	 (vis sh) and as remote command (via ssh)
       *)
      List.iter
	(fun hostname ->
	   let cmd =
	     Mapred_taskfiles.ssh_cmd
	       hostname
	       (sprintf "%s/task_server \
                         -conf %s/task_server.conf \
                         -log %s/task_server.stderr \
                         _plasma_task_server -mapred-conf %s/mapred.conf \
                         -pid %s/task_server.pid"
		  qttmp qttmp qttmp qttmp qttmp) in
	   let code = Sys.command cmd in
	   if code <> 0 then
	     failwith ("Exec failed: " ^ cmd);
	)
	conf#mr_task_nodes;
      print_endline "Servers started"
    with
      | error ->
	  List.iter (fun f -> f()) !cleanup;
	  raise error
  );
  List.iter (fun f -> f()) !cleanup


let stop_task_servers cf_file =
  Arg.parse
    [ ]
    (fun s -> raise(Arg.Bad ("Extra argument: "  ^ s)))
    (sprintf "usage: %s stop_task_servers [options]" this);

  let cf = Netplex_config.read_config_file cf_file in
  let conf = Mapred_config.extract_config cf in
  let ttmp = conf # mr_task_tmpdir in
  let qttmp = Filename.quote ttmp in
  
  List.iter
    (fun hostname ->
       let cmd =
	 Mapred_taskfiles.ssh_cmd
	   hostname
	   (sprintf "%s/task_server \
                     -log %s/task_server.stderr \
                     _plasma_kill_task_server -tmpdir %s"
	      qttmp qttmp qttmp) in
       let code = Sys.command cmd in
       if code <> 0 then
	 prerr_endline ("Exec failed: " ^ cmd);
    )
    conf#mr_task_nodes;
  print_endline "Servers killed"

       
let kill_task_server cf_file =
  let tmpdir = ref "/unset"   in
  Arg.parse
    [ "-tmpdir", Arg.Set_string tmpdir,
      "path    Set the mapred tmpdir";
    ]
    (fun s -> raise(Arg.Bad ("Extra argument: "  ^ s)))
    (sprintf "usage: %s _plasma_kill_task_server [options]" this);

  kill !tmpdir

       
let task_server cf_file spec =
  let netplex_args, netplex_cmdconf = Netplex_main.args () in
  let mapred_conf = ref "mapred.conf" in
  Arg.parse
    ( [ "-mapred-conf", Arg.Set_string mapred_conf,
	"<file>   Set the mapred conf file";
      ] @
	netplex_args
    )
    (fun s -> raise(Arg.Bad ("Extra argument: "  ^ s)))
    (sprintf "usage: %s _plasma_task_server [options]" this);

  (* Especially close stuff from ssh! *)
  for fd = 3 to Netsys_posix.sysconf_open_max()-1 do
    try Unix.close (Netsys_posix.file_descr_of_int fd)
    with _ -> ()
  done;

  let mapred_cf = Netplex_config.read_config_file !mapred_conf in

  let parallelizer =
    Netplex_mp.mp() in
  
  prerr_endline "Startup";
  Netlog.logf `Notice "Startup";

  Plasma_util.init_debug netplex_cmdconf;

  let mj_list =
    List.map (fun (cmd,_,get_job,_) -> (cmd,get_job)) spec in

  Netplex_main.startup
    parallelizer
    Netplex_log.logger_factories
    Netplex_workload.workload_manager_factories
    [ Mapred_task_server.factory mj_list mapred_cf ]
    netplex_cmdconf


let get_mapred_env cf_file cmd_name =
  let cf = Netplex_config.read_config_file cf_file in
  let conf = Mapred_config.extract_config cf in
  let fs = Mapred_fs.standard_fs conf in
  let esys = Unixqueue.create_unix_event_system() in

  let auth_ticket =
    match fs # open_cluster "plasma::/" esys  with
      | None -> "unauthenticated"
      | Some c ->
	  let (user, _, _) = Plasma_client.current_user c in
	  let auth_ticket = Plasma_client.get_auth_ticket c user in
	  Plasma_client.close_cluster c;
	  auth_ticket in

  let shm_mng =
    new Plasma_shm.null_shm_manager() in

  ( object
      method filesystem = fs
      method shm_manager = shm_mng
      method config = conf
      method config_file = cf
      method auth_ticket = auth_ticket
      method command_name = cmd_name
      method accumulate (st:Mapred_stats.stats) = ()
    end
  )


let exec_job ?(options = fun _ -> []) me mj =
  let dump_plan = ref false in
  let dump_svg = ref None in
  let keep_temp_files = ref false in
  let net_limit = ref None in
  let simulate = ref false in
  let pre_sort_algo = ref "DEFAULT" in
  let args = ref [] in

  let proc_override s =
    try
      match Pcre.extract ~pat:"^([a-zA-Z0-9_]+)=(.*)$" ~full_match:false s with
	| [| n; v |] -> args := !args @ [ n,v ]
	| _ -> raise Not_found
    with
      | Not_found ->
	  raise (Arg.Bad ("Cannot parse this override: " ^ s)) in

  let level = ref `Info in

  let user_opts = options args in

  Arg.parse
    (user_opts @
       [ "-override", Arg.String proc_override,
         "<name>=<value>    Override a parameter from the 'mapredjob' section";

         "-keep-temp-files", Arg.Set keep_temp_files,
         "    do not delete intermediate files";

         "-simulate", Arg.Set simulate,
         "    only simulate map/reduce (no files written)";
         
         "-dump-plan", Arg.Set dump_plan,
         "    Dump the plan before mapping and after mapping";
         
         "-dump-svg", Arg.String (fun s -> dump_svg := Some s),
         "<file>   Dump the plan after execution as SVG for analysis";

         "-pre-sort-algo", Arg.Set_string pre_sort_algo,
         "<name>   Set the task pre-sort algorithm";

         "-net-limit", Arg.Int (fun n -> net_limit := Some n),
         "<n>   The max allowed number of misallocated tasks per node";

         "-debug", Arg.Unit (fun () -> level := `Debug; dump_plan := true),
         "    Enable Plasma debug messages";

         "-on-debug", Arg.String (fun s -> Netlog.Debug.enable_module s),
         "<module>  Enable debug messages for Ocamlnet <module>";

         "-on-debug-all", Arg.Unit (fun () -> Netlog.Debug.enable_all()),
         "  Enable all Ocamlnet debug messages";

         "-on-debug-list", Arg.Unit (fun () -> 
                                       List.iter print_endline (Netlog.Debug.names());
                                 exit 0),
         "  Show possible modules for -debug, then exit"
           
       ]
    )
    (fun s -> raise(Arg.Bad ("Extra argument: "  ^ s)))
    (sprintf "usage: %s stop_task_servers [options]" this);

  Netlog.current_logger := Netlog.channel_logger stderr !level;
  Plasma_util.debug := !level = `Debug;

  let (jc,mjc) = 
    Mapred_job_config.extract_job_config 
      me#config_file !args mj#custom_params in
  let rjc =
    Mapred_job_exec.create_runtime_job_config_from_mapred_config
      ~dump_plan_when_complete:!dump_plan
      ?dump_plan_as_svg:!dump_svg
      ~simulate:!simulate
      ~pre_sort_algo:!pre_sort_algo
      ~keep_temp_files:!keep_temp_files
      ~report:true
      ?net_limit:!net_limit
      mjc
      me#config in

  let job =
    Mapred_job.invoke 
      ~catch_sigint:true
      me
      rjc
      mj in
  Mapred_job.run job


let dispatch spec =
  Printexc.record_backtrace true;
  try
    let cf_file = ref (Sys.argv.(0) ^ ".conf") in
    if (Array.length Sys.argv >= 3 && Sys.argv.(!Arg.current+1) = "-conf") then (
      cf_file := Sys.argv.(!Arg.current+2);
      Arg.current := !Arg.current + 2;
    );
    if (Array.length Sys.argv >= 3 && Sys.argv.(!Arg.current+1) = "-log") then (
      let fn = Sys.argv.(!Arg.current+2) in
      Arg.current := !Arg.current + 2;
      let fd = 
	Unix.openfile fn [Unix.O_APPEND; Unix.O_CREAT; Unix.O_WRONLY] 0o666 in
      Unix.dup2 fd Unix.stdout;
      Unix.dup2 fd Unix.stderr;
      Unix.close fd;
    );
    
    if !Arg.current+1 >= Array.length Sys.argv then
      failwith "Not enough arguments";

    incr Arg.current;
    let mode_str = Sys.argv.(!Arg.current) in
    
    match mode_str with
      | "start_task_servers" | "start-task-servers" ->
	  start_task_servers !cf_file
      | "stop_task_servers" | "stop-task-servers" ->
	  stop_task_servers !cf_file
      | "_plasma_task_server" ->
	  task_server !cf_file spec
      | "_plasma_kill_task_server" ->
	  kill_task_server !cf_file
      | "help" | "-help" | "--help" -> usage spec
      | _ -> 
	  let (_,_,get_job,run_job) =
	    try
	      List.find (fun (name,_,_,_) -> name = mode_str) spec
	    with
	      | Not_found ->
		  failwith ("No such command: " ^ mode_str) in
	  let me = get_mapred_env !cf_file mode_str in
	  let job = get_job me in
	  run_job me job
  with	  
    | Failure m ->
        prerr_endline (this ^ ": " ^ m);
        usage spec;
        exit 2


let main mj =
  dispatch
    [ "exec_job", "Run the map/red job", (fun _ -> mj), exec_job ?options:None;
    ]
