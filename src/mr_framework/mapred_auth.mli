(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Authentication in server context *)

(** {2 Server Configuration} *)

type access =
    { users : (string * string * string * string * int) list;
	(** The RPC users with passwords. Incoming connections can log in
	    as any of these users.

	    The tuple is:
	    [ (user, password, salted_password, salt, iteration_count) ]
	 *)
    }

val extract_access_config : 
      Netplex_types.config_file -> Netplex_types.address -> access
    (** Extracts a config section from a Netplex config file like:
	
	{[
	  access {
	    user { name = "name"; password = "pw" };
	    (* more users possible *)
	  }
	]}

	If this section is missing, the empty list of users is reported.

	It is possible to use [password_file] instead of [password] to
	read the password from the first line of a given file (relative
	to the config file).
     *)

val configure_rpc_server : Rpc_server.t -> access -> unit
  (** Configures the RPC server so the passed level of access protection
      is required.

      The procedure 0 is handled specially here and never requires
      authentication.
   *)


(** {2 Client Configuration} *)

val get_client_auth : Plasma_client.plasma_cluster -> string
  (** Requests an authentication ticket *)

val rpc_proxy_auth_methods : string -> Rpc_client.auth_method list
  (** Returns a value suitable for the [mclient_auth_methods] field.
      The arg is the authentication ticket.
   *)

      
