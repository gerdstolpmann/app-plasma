(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

type file_fragment =
    (string * int64 * int64)

type file = file_fragment list

type file_tag = [ `Tag of string ]

type locality = [ `Dn_identity of string | `Any ]

type map_task =
    { map_input : file;
      map_output_prefix : string; 
      map_output_suffix : string;
      map_id : int;
      map_best_hosts : Unix.inet_addr list;
    }

type sort_task =
    { sort_input : file;
      sort_input_del : bool;
      sort_output : string;
      sort_output_locality : locality;
      sort_id : int;
    }

type shuffle_task =
    { shuffle_input : (file * int * int) list;
      shuffle_input_del : bool;
      shuffle_output : (string * int * int * locality) list;
      shuffle_partitions : int * int;
      shuffle_coverage : int * int;
      shuffle_reduce : bool;
      shuffle_round : int;
      shuffle_avg_part_width : float;
    }

type emap_task =
    { emap_map : map_task;
      emap_output : (file_tag * int * int * locality) list;
    }

type task =
    [ `Map of map_task
    | `Emap of emap_task
    | `Sort of sort_task
    | `Shuffle of shuffle_task
    | `Cleanup
    ]

type task_ok =
    { ok_files : (file_tag * file) list;
      ok_counters : Mapred_stats.stats
    }

type task_result =
    [ `Ok of task_ok
    | `Retry_later
    | `Corrupt_input of file list
    | `Error of string
    ]


module Ord = struct
  type t = task
      
  let task_type_rank =
    function
      | `Map _ -> 0
      | `Emap _ -> 1
      | `Sort _ -> 2
      | `Shuffle _ -> 3
      | `Cleanup -> 4
	  
  let delta (a,b) = abs (a-b)
    

  let ( ||| ) a b = if a=0 then b() else a
    
    
  let compare (x:t) (y:t) = 
    let x_ttr = task_type_rank x in
    let y_ttr = task_type_rank y in
    (x_ttr - y_ttr)
    ||| (fun () ->
	   match x, y with
	     | `Map x_m, `Map y_m -> 
		 x_m.map_id - y_m.map_id
	     | `Sort x_s, `Sort y_s ->
		 x_s.sort_id - y_s.sort_id
	     | `Emap x_e, `Emap y_e ->
		 x_e.emap_map.map_id - y_e.emap_map.map_id
	     | `Shuffle x_s, `Shuffle y_s ->
		 (x_s.shuffle_round - y_s.shuffle_round)
		 ||| (fun () ->
			let x_ci = fst x_s.shuffle_coverage in
			let y_ci = fst y_s.shuffle_coverage in
			(x_ci - y_ci)
			||| (fun () ->
			       let x_ri = fst x_s.shuffle_partitions in
			       let y_ri = fst y_s.shuffle_partitions in
			       (x_ri - y_ri)
			       ||| (fun () ->
				      (* just to be sure *)
				      (* Pervasives.compare x y *)
				      0
				   )
			    )
		     )
	     | `Cleanup, `Cleanup -> 
		 0
	     | _ -> assert false
	)
end      

let encode_task t = Marshal.to_string t [Marshal.No_sharing]
let decode_task s = Marshal.from_string s 0

let encode_task_result t = Marshal.to_string t [Marshal.No_sharing]
let decode_task_result s = Marshal.from_string s 0

let string_of_task_id t =
  match t with
    | `Map mt ->
	sprintf "Map %d" 
	  mt.map_id

    | `Sort st ->
	sprintf "Sort %d"
	  st.sort_id

    | `Emap et ->
	sprintf "Emap %d"
	  et.emap_map.map_id

    | `Shuffle st ->
	let (kmin,kmax) = st.shuffle_coverage in
	let (pmin,pmax) = st.shuffle_partitions in
	sprintf "Shuffle s%d-%d/p%d-%d"
	  kmin kmax pmin pmax

    | `Cleanup ->
	"Cleanup"

let lock_name_of_task_id t =
  match t with
    | `Map mt ->
	sprintf "map_%d" 
	  mt.map_id

    | `Sort st ->
	sprintf "sort_%d"
	  st.sort_id

    | `Emap et ->
	sprintf "emap_%d"
	  et.emap_map.map_id

    | `Shuffle st ->
	let (kmin,kmax) = st.shuffle_coverage in
	let (pmin,pmax) = st.shuffle_partitions in
	sprintf "shuffle_s%d-%d_p%d-%d"
	  kmin kmax pmin pmax

    | `Cleanup ->
	"cleanup"

let print_task_id out t i =
  let indent = String.make i ' ' in
  out # output_string (indent ^ string_of_task_id t);
  out # output_string "\n"

let print_file ?(tag="fragment") out f i =
  let indent = String.make i ' ' in
  List.iter
    (fun (name,pos,len) ->
       out # output_string 
	 (sprintf "%s%s %s @ %Ld / %Ld\n"
	    indent tag name pos len)
    )
    f

let print_task out t i =
  let indent = String.make i ' ' in
  print_task_id out t i;
  match t with
    | `Map mt ->
	print_file ~tag:"input" out mt.map_input (i+4);
	out # output_string 
	  (sprintf "%s    output_prefix %s\n" indent mt.map_output_prefix);
	List.iter
	  (fun ip ->
	     out # output_string 
	       (sprintf "%s    best_host %s\n"
		  indent (Unix.string_of_inet_addr ip))
	  )
	  mt.map_best_hosts

    | `Emap et ->
	let mt = et.emap_map in
	print_file ~tag:"input" out mt.map_input (i+4);
	out # output_string 
	  (sprintf "%s    output_prefix %s\n" indent mt.map_output_prefix);
	List.iter
	  (fun ip ->
	     out # output_string 
	       (sprintf "%s    best_host %s\n"
		  indent (Unix.string_of_inet_addr ip))
	  )
	  mt.map_best_hosts;
	out # output_string 
	  (sprintf "%s    #prepartitions %d\n" 
	     indent (List.length et.emap_output));
	out # output_string 
	  (sprintf "%s    prepartitions %s\n" indent
	     (String.concat ","
		(List.map
		   (fun (_,pmin,pmax,_) -> sprintf "%d-%d" pmin pmax)
		   et.emap_output
		)
	     )
	  )

    | `Sort st ->
	print_file ~tag:"input" out st.sort_input (i+4);
	out # output_string 
	  (sprintf "%s    output %s\n" indent st.sort_output)

    | `Shuffle st ->
	List.iter
	  (fun (file,kmin,kmax) ->
	     out # output_string 
	       (sprintf "%s    input s%d-%d\n" indent kmin kmax);
	     print_file out file (i+8);
	  )
	  st.shuffle_input;
	List.iter
	  (fun (file, pmin,pmax,_) ->
	     out # output_string 
	       (sprintf "%s    output %s p%d-%d\n" indent file pmin pmax)
	  )
	  st.shuffle_output;
	out # output_string 
	  (sprintf "%s    reduce %b\n" indent st.shuffle_reduce)

    | `Cleanup ->
	()
