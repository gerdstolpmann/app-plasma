(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Where to run which task:
   - map task: is specified in the task
   - sort task: same node as preceding map
   - first round of shuffling: same node as preceding sort
   - further rounds of shuffling: any node
   - reduce task: same node as preceding shuffling task if this task
     produced only one file; otherwise any node
 *)

open Mapred_tasks
open Mapred_def
open Printf

module Mr_proxy = Mapred_rpc_clnt.Make'Mapred_task(Rpc_proxy.ManagedClient)

module TaskSet = Set.Make(Mapred_tasks.Ord)
module TaskMap = Map.Make(Mapred_tasks.Ord)


type runtime_job_config =
    { map_weight : float * float;
      sort_weight : float * float;
      emap_weight : float * float;
      shuffle_weight : float * float;
      reduce_weight : float * float;
      dump_plan_when_complete : bool;
      dump_plan_as_svg : string option;
      simulate : bool;
      pre_sort_algo : string;
      keep_temp_files : bool;
      net_limit : int;
      report : bool;
      report_to : Netchannels.out_obj_channel;
      res_conf : Mapred_rpc_aux.resource_config;
      mjc : Mapred_job_config.m_job_config;
    }

let dlogf = Plasma_util.dlogf

let create_runtime_job_config 
      ?(map_weight=(0.8, 0.2))     (* (io, cpu) *) 
      ?(emap_weight=(0.1, 0.9))     (* (io, cpu) *) 
      ?(sort_weight=(0.1, 0.9)) 
      ?(shuffle_weight=(0.7, 0.3))
      ?(reduce_weight=(0.5, 0.5)) 
      ?(dump_plan_when_complete=false)
      ?dump_plan_as_svg
      ?shm_low
      ?shm_high
      ?shm_max
      ?buf_low
      ?buf_high
      ?buf_max
      ?(shm_low_factor=0.25)
      ?(shm_high_factor=0.5)
      ?(shm_max_factor=0.75)
      ?(buf_low_factor=0.25)
      ?(buf_high_factor=0.5)
      ?(buf_max_factor=0.75)
      ?(simulate=false)
      ?(pre_sort_algo = "DEFAULT")
      ?(keep_temp_files=false)
      ?(net_limit=1)
      ?(report=false)
      ?(report_to=new Netchannels.output_channel stderr)
      mjc =
  let res_conf =
    { Mapred_rpc_aux.rc_shm_low = shm_low;
      rc_shm_high = shm_high;
      rc_shm_max = shm_max;
      rc_buf_low = buf_low;
      rc_buf_high = buf_high;
      rc_buf_max = buf_max;
      rc_shm_low_p = shm_low_factor;
      rc_shm_high_p = shm_high_factor;
      rc_shm_max_p = shm_max_factor;
      rc_buf_low_p = buf_low_factor;
      rc_buf_high_p = buf_high_factor;
      rc_buf_max_p = buf_max_factor;
    } in
  { map_weight;
    emap_weight;
    sort_weight;
    shuffle_weight;
    reduce_weight;
    dump_plan_when_complete;
    dump_plan_as_svg;
    simulate;
    pre_sort_algo;
    keep_temp_files;
    net_limit;
    report;
    report_to;
    mjc = mjc;
    res_conf;
  }

let create_runtime_job_config_from_mapred_config
      ?dump_plan_when_complete
      ?dump_plan_as_svg
      ?simulate
      ?pre_sort_algo
      ?keep_temp_files
      ?net_limit
      ?report
      ?report_to
      mjc
      conf =
  create_runtime_job_config
    ?shm_low:conf#mr_shm_low
    ?shm_high:conf#mr_shm_high
    ?shm_max:conf#mr_shm_max
    ?buf_low:conf#mr_buf_low
    ?buf_high:conf#mr_buf_high
    ?buf_max:conf#mr_buf_max
    ~shm_low_factor:conf#mr_shm_low_factor
    ~shm_high_factor:conf#mr_shm_high_factor
    ~shm_max_factor:conf#mr_shm_max_factor
    ~buf_low_factor:conf#mr_buf_low_factor
    ~buf_high_factor:conf#mr_buf_high_factor
    ~buf_max_factor:conf#mr_buf_max_factor
    ?dump_plan_when_complete
    ?dump_plan_as_svg
    ?simulate
    ?pre_sort_algo
    ?keep_temp_files
    ?net_limit
    ?report
    ?report_to
    mjc


module MCSet = Set.Make(Rpc_proxy.ManagedClient)

type status =
    [ `Running | `Successful | `Errors of string list | `Killed ]

type running_job =
    { config : runtime_job_config;
      plan : Mapred_sched.plan;
      mutable started_tasks : task list;  (* for cleanup *)
      mutable killed : bool;
      mutable running : (int, task * Unix.inet_addr) Hashtbl.t;
        (* key: request id *)
      mutable clients : MCSet.t;
      mutable status : status;
      esys : Unixqueue.event_system;
      port : int;
      work_dir : string;
      mutable next_req_id : int;
      me : Mapred_def.mapred_env;
      tm : Mapred_taskfiles.taskfile_manager;
      mj : Mapred_def.mapred_job;
      mutable counters : Mapred_stats.stats;
    }

type machine_params =
    { capacity : (Unix.inet_addr, Mapred_rpc_aux.capacity) Hashtbl.t;
    }


let get_simulate rjc = rjc.simulate
let get_report rjc = rjc.report
let get_report_to rjc = rjc.report_to
let get_job_config rjc = rjc.mjc
let get_dump_plan_when_complete rjc = rjc.dump_plan_when_complete
let get_keep_temp_files rjc = rjc.keep_temp_files

let get_rjc j = j.config
let get_tm j = j.tm
let get_plan j = j.plan
let get_env j = j.me
let get_job j = j.mj

let event_system j = j.esys
let stats j = j.counters

let message_rjc rjc s =
  if rjc.report then (
    rjc.report_to # output_string s;
    rjc.report_to # flush();
  )

let message j s =
  message_rjc j.config s


let misalloc_penalty = 4.0
  (** Misallocated tasks have this factor bigger weight. The idea is to
     prevent misallocations as long as good allocations are possible.
     (A misallocated task is run on a non-preferred host.)
   *)

let real_version =
  Mapred_config.executable_version()

let new_client_no_job auth_ticket saddr esys =
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Mapred_rpc_clnt.Mapred_task.V1._program ]
      ~initial_ping:true
      ~auth_methods:(Mapred_auth.rpc_proxy_auth_methods auth_ticket)
      () in
  (* TODO: no timeout so far - this is not possible with current RPC API *)
  Rpc_proxy.ManagedClient.create_mclient
    mclient_config
    (Plasma_util.connector_of_sockaddr saddr)
    esys


let new_client job saddr =
  let mclient = new_client_no_job job.me#auth_ticket saddr job.esys in
  job.clients <- MCSet.add mclient job.clients;
  mclient


let new_configured_client job saddr onlimit_opt when_ready =
  let auth_ticket = job.me#auth_ticket in
  let mclient = new_client job saddr in
  let pending = ref 0 in
  let check_ready() =
    if !pending = 0 then
      when_ready mclient in
  Mr_proxy.V1.configure'async 
    mclient 
    (job.config.res_conf, auth_ticket)
    (fun get_reply -> 
       ( try let () = get_reply() in ()
	 with error ->
	   let ip = Plasma_util.ip_of_sockaddr saddr in
	   Netlog.logf `Err "Exception from configure to %s: %s"
	     (Unix.string_of_inet_addr ip)
	     (Netexn.to_string error)
       );
       decr pending;
       check_ready());
  incr pending;
  ( match onlimit_opt with
      | None -> ()
      | Some onlimit ->
	  Mr_proxy.V1.job_capacity'async
	    mclient
	    ( (Mapred_job_config.mapred_job_config job.config.mjc)#job_id,
	      auth_ticket
	    )
	    (fun get_reply ->
	       ( try
		   let res = get_reply() in
		   onlimit res
		 with error ->
		   let ip = Plasma_util.ip_of_sockaddr saddr in
		   Netlog.logf `Err "Exception from job_capacity to %s: %s"
		     (Unix.string_of_inet_addr ip)
		     (Netexn.to_string error)
	       );
	       decr pending;
	       check_ready()
	    );
	  incr pending;
  )



let dispose_client job c =
  job.clients <- MCSet.remove c job.clients


let kill job =
  Unix.sleep 1;  (* give servers time to log errors *)
  Netlog.logf `Info "Killing job";
  job.killed <- true;
  Hashtbl.iter
    (fun req_id (task,ip) ->
       let task_s = Mapred_tasks.string_of_task_id task in
       Netlog.logf `Info "Submitting kill request of %s to %s"
	 task_s (Unix.string_of_inet_addr ip);
       let saddr = Unix.ADDR_INET(ip,job.port) in
       let c = new_client job saddr in
       Mr_proxy.V1.kill'async
	 c
	 (req_id, job.me#auth_ticket)
	 (fun get_reply ->
	    Rpc_proxy.ManagedClient.shut_down c;
	    try
	      let () = get_reply() in
	      Netlog.logf `Info "Kill request for %s was accepted" task_s
	    with
	      | error ->
		  Netlog.logf `Err
		    "Exception after kill request for %s to %s: %s"
		    task_s (Unix.string_of_inet_addr ip)
		    (Netexn.to_string error)
	 )
    )
    job.running;
  Hashtbl.clear job.running;
  if job.status = `Running then
    job.status <- `Killed
    

let print_logs job req_id logs =
  List.iter
    (fun msg ->
       message job 
	 (sprintf "    #%06d: %s\n" req_id msg)
    )
    logs


let max_errors = 1

let start esys plan mj rjc mp tm me port when_done =
  let t0 = Unix.gettimeofday() in

  let all_hosts = 
    List.map snd (Mapred_sched.hosts plan) in

  let load = Hashtbl.create 5 in
  (** (io_load,cpu_load,net_load) per IP addr *)

  (* NB. net_load is 0.99 for a misalloc entry and 0.0 otherwise.
     The limit is always 1
   *)

  let sum_io_load = ref 0.0 in   (* the sum of the load value on all nodes *)
  let sum_cpu_load = ref 0.0 in

  let n_hosts = List.length (Mapred_sched.hosts plan) in

  let load_limit = Hashtbl.create 5 in
  (** dynamic load limit per IP addr *)

  let load_limit_warning = ref false in

  let retry_later = ref TaskSet.empty in
  (** contains tasks that responded recently with `Retry_later. This should
      not be immediately retried, but first when some resources were freed
      again.
   *)

  let retry_later_nodes = Hashtbl.create 5 in
  (** The corresponding full nodes *)

  let task_err_count = ref TaskMap.empty in
  (** counts errors per task *)

  let shuffle_pref_node = Hashtbl.create 5 in
  (** For pairs (pmin,pmax) this hashtbl contains a preferred node. *)

  let eff_node = Hashtbl.create 5 in
  (** Maps task to the node that really executed it *)

  let plan_dumped = ref false in

  let pp_round = ref None in
  let pp_nodes = Queue.create() in
  let () =
    List.iter (fun (_,ip) -> Queue.add ip pp_nodes) (Mapred_sched.hosts plan) in

  let jc = Mapred_job_config.mapred_job_config rjc.mjc in
  let job =
    { config = rjc;
      plan = plan;
      started_tasks = [];
      killed = false;
      running = Hashtbl.create 7;
      clients = MCSet.empty;
      status = `Running;
      esys = esys;
      port = port;
      work_dir = jc#work_dir;
      next_req_id = 0;
      me;
      tm;
      mj;
      counters = Mapred_stats.empty;
    } in

  let stat_tasks = ref 0 in
  let stat_misalloc_tasks = ref 0 in
  let stat_inputs = ref 0 in
  let stat_inputs_nonlocal = ref 0 in

  let get_load ip =
    try Hashtbl.find load ip with Not_found -> (0.0, 0.0, 0.0, 0) in

  let update_load_limit ip cap =
    (* lim: number of cores that are assigned for this job. We double the
       number of tasks we send to the machine, just to be sure we max it
       out. Also, add 0.5 to avoid rounding problems, and ensure we can
       at least execute one task.
     *)
    let lim = cap.Mapred_rpc_aux.avail_capacity in
    if lim > 0.0 then (
      let lim' =
	max 1.0 (2.0 *. lim) +. 0.5 in
      Hashtbl.replace load_limit ip lim'
    ) in

  let emit_load_limit_warning() =
    if not !load_limit_warning then (
      Netlog.logf `Warning
	"The capacity cannot be determined at runtime from some or all task nodes";
      load_limit_warning := true
    ) in

  let underutilized_nodes() =
    (* A node counts as underutilized if less than half of the load_limit
       tasks runs on a node
     *)
    List.fold_left
      (fun acc ip ->
	 let (_,_,_,tasks) = get_load ip in
	 let lim = try Hashtbl.find load_limit ip with Not_found -> 0.0 in
	 if float tasks < 0.5*.lim then
	   acc+1
	 else
	   acc
      )
      0
      all_hosts in

  let t_underutil = ref t0 in
  let p_underutil = ref 0.0 in   (* current underutil seconds *)
  let m_underutil = ref 0.0 in   (* theoretical max *)

  let update_underutil() =
    let t = Unix.gettimeofday() in
    let delta = t -. !t_underutil in
    let n = underutilized_nodes() in
    p_underutil := !p_underutil +. float n *. delta;
    m_underutil := !m_underutil +. float (List.length all_hosts) *.delta;
    t_underutil := t
  in

  let submit task pref_hosts allow_misalloc (io_weight,cpu_weight) onsuccess =
    (** Try to start [task] on the [pref_hosts]. If there are no preferences,
       tries to start on any host. Returns [true] if possible, else [false].

	The [pref_hosts] get just a better factor! This allows it to
	use a non-preferred host when the preferred one is very loaded,
	but the chosen alternative is not loaded.
     *)
    let well_allocated ip =
      pref_hosts = [] || List.mem ip pref_hosts in
    if all_hosts = [] then
      failwith "List of hosts is empty";
    let hosts_with_factor =
      List.map
	(fun ip ->
	   let factor = 
	     if well_allocated ip then 1.0 else misalloc_penalty in
	   (factor, ip)
	)
	all_hosts in
    let hosts_sorted_wf =
      List.sort
	(fun (f1,ip1) (f2,ip2) ->
	   let (io_ld1, cpu_ld1, net_id1, tasks_id1) = get_load ip1 in
	   let (io_ld2, cpu_ld2, net_id2, tasks_id2) = get_load ip2 in
	   compare (f1 *. (io_ld1 +. cpu_ld1)) (f2 *. (io_ld2 +. cpu_ld2))
	)
	hosts_with_factor in
    let hosts_sorted =
      List.map snd hosts_sorted_wf in
    let ip_opt =
      try
	Some
	  (List.find
	     (fun ip ->
		let net_weight =
		  if well_allocated ip then 0.0 else 
		    if allow_misalloc then 0.99 else 2.0 in
		let limit =
		  try Hashtbl.find load_limit ip
		  with Not_found -> 
		    emit_load_limit_warning(); 1.0 in
		let (ip_io_load, ip_cpu_load, ip_net_load, _) = get_load ip in
		ip_io_load +. io_weight +.
		  ip_cpu_load +. cpu_weight <= limit &&
		  ip_net_load +. net_weight <= float rjc.net_limit
		  (* We add the new load to the existing load, and check
		     whether we are below the bounds
		   *)
	     )
	     hosts_sorted)
      with Not_found -> None in

    let exec_response ip net_weight req_id task_s c get_reply =
      let res, logs =
	try Mapred_tasks.decode_task_result(get_reply())
	with error ->
	  Netlog.logf `Err "Exception from exec to %s: %s"
	    (Unix.string_of_inet_addr ip)
	    (Netexn.to_string error);
	  (`Error("RPC error: " ^ Netexn.to_string error), []) in
      Rpc_proxy.ManagedClient.shut_down c;
      dispose_client job c;
      update_underutil();
      let (old_ip_io_load, old_ip_cpu_load, old_ip_net_load, old_tasks) = 
	get_load ip in
      let new_ip_io_load = old_ip_io_load -. io_weight in
      let new_ip_cpu_load = old_ip_cpu_load -. cpu_weight in
      let new_ip_net_load = old_ip_net_load -. net_weight in
      Hashtbl.replace load 
	ip (new_ip_io_load, new_ip_cpu_load, new_ip_net_load, old_tasks-1);
      sum_io_load := !sum_io_load -. io_weight;
      sum_cpu_load := !sum_cpu_load -. cpu_weight;
      Hashtbl.remove job.running req_id;
      print_logs job req_id logs;
      match res with
	| `Ok ok ->
	    message job (sprintf "Finished task %s\n" task_s);
	    let files = ok.ok_files in
	    job.counters <- Mapred_stats.add job.counters ok.ok_counters;
	    let misalloc =  not(well_allocated ip) in
	    incr stat_tasks;
	    if misalloc then incr stat_misalloc_tasks;
	    ( try
		let (inputs, inputs_nonlocal) = 
		  Mapred_sched.task_stats plan task in
		stat_inputs := !stat_inputs + inputs;
		stat_inputs_nonlocal := !stat_inputs_nonlocal + inputs_nonlocal;
	      with Not_found -> ()
	    );
	    ( match task with
		| `Map mt ->
		    Mapred_sched.add_map_output
		      plan mt.map_id files ip
		| `Emap et ->
		    Mapred_sched.add_map_output
		      plan et.emap_map.map_id files ip
		| _ -> ()
	    );
	    if rjc.dump_plan_when_complete && 
	      Mapred_sched.plan_complete plan && not !plan_dumped
	    then (
	      message job "COMPLETE PLAN:\n";
	      if job.config.report then
		Mapred_sched.print_plan job.config.report_to plan;
	      job.config.report_to # flush();
	      plan_dumped := true
	    );
	    Mapred_sched.mark_as_finished plan task;
	    Hashtbl.remove retry_later_nodes ip;
	    retry_later := TaskSet.empty;
	    onsuccess()
	| `Retry_later ->
	    if Hashtbl.length job.running = 0 then (
	      let msg = 
		"Got Retry_later code for the only running task" in
	      Netlog.logf `Warning "%s" msg;
	      (* keep retry_later as is - so all other tasks remain
		 blocked
	       *)
	      Hashtbl.clear retry_later_nodes;
	      Mapred_sched.remove_marks plan task;
	      onsuccess()   (* i.e. not a fatal error *)
	    )
	    else (
	      message job (sprintf "Delaying task %s\n" task_s);
	      retry_later := TaskSet.add task !retry_later;
	      Hashtbl.replace retry_later_nodes ip true;
	      Mapred_sched.remove_marks plan task;
	      onsuccess()   (* i.e. not a fatal error *)
	    )
	| `Corrupt_input _ ->
	    assert false
	| `Error msg ->
	    Netlog.logf `Err "Error for task %s: %s" task_s msg;
	    message job "Erroneous task:\n";
	    if job.config.report then
	      Mapred_tasks.print_task job.config.report_to task 4;
	    job.config.report_to # flush();
	    let ec = 
	      try TaskMap.find task !task_err_count
	      with Not_found -> 0 in
	    task_err_count := TaskMap.add task (ec+1) !task_err_count;
	    if ec < max_errors then (
	      message job (sprintf "Going to retry task %s\n" task_s);
	      retry_later := TaskSet.add task !retry_later;
	      Hashtbl.replace retry_later_nodes ip true;
	      Mapred_sched.remove_marks plan task;
	      onsuccess()   (* i.e. not a fatal error *)
	    )
	    else (
	      let x_msg = sprintf "Task %s: %s" task_s msg in
	      job.status <- `Errors [ x_msg ];
	      if not job.killed then
		kill job
	    ) in
 
    let exec ip net_weight req_id task_s c =
      let f =
	if rjc.simulate then
	  Mr_proxy.V1.simulate'async
	else
	  Mr_proxy.V1.exec'async in
      f
	c
	(job.me#command_name,
	 req_id, 
	 job.me#auth_ticket,
	 Mapred_job_config.marshal rjc.mjc, 
	 Mapred_tasks.encode_task task)
	(exec_response ip net_weight req_id task_s c)
    in

    update_underutil();
    match ip_opt with
      | Some ip ->
	  let (ip_io_load, ip_cpu_load, ip_net_load, ip_tasks) = get_load ip in
	  let misalloc =  not(well_allocated ip) in
	  let net_weight = if misalloc then 0.99 else 0.0 in
	  let saddr = Unix.ADDR_INET(ip,port) in
	  let onlimit_opt = Some (update_load_limit ip) in
	  let req_id = job.next_req_id in
	  job.next_req_id <- job.next_req_id + 1;
	  let task_s = 
	    sprintf "%s #%06d" (Mapred_tasks.string_of_task_id task) req_id in
	  new_configured_client job saddr onlimit_opt
	    (exec ip net_weight req_id task_s);

	  let new_ip_io_load = ip_io_load +. io_weight in
	  let new_ip_cpu_load = ip_cpu_load +. cpu_weight in
	  let new_ip_net_load = ip_net_load +. net_weight in
	  Hashtbl.replace load
	    ip (new_ip_io_load, new_ip_cpu_load, new_ip_net_load, ip_tasks+1);
	  sum_io_load := !sum_io_load +. io_weight;
	  sum_cpu_load := !sum_cpu_load +. cpu_weight;
	  Hashtbl.replace job.running req_id (task,ip);
	  Mapred_sched.mark_as_started plan task ip req_id misalloc;
	  job.started_tasks <- task :: job.started_tasks;
	  
	  Hashtbl.replace eff_node task ip;
	  message job
	    (sprintf 
	       "Submitted task %s to %s\n"
	       task_s
	       (Unix.string_of_inet_addr ip));
	  true
      | None ->
	  false
  in

  let most_frequent l =
    let ht = Hashtbl.create 5 in
    List.iter
      (fun x ->
	 let n = try Hashtbl.find ht x with Not_found -> 0 in
	 Hashtbl.replace ht x (n+1)
      )
      l;
    let best_x, _ =
      Hashtbl.fold
	(fun x n (best_x,best_n) ->
	   if n > best_n then
	     ([x],n)
	   else
	     if n = best_n then
	       (x :: best_x, n)
	     else
	       (best_x,best_n)
	)
	ht
	([], 0) in
    best_x in


  let rec determine_pref_hosts task =
    (** see comment above *)
    try
      [Hashtbl.find eff_node task]
    with
      | Not_found ->
	  match task with
	    | `Map mt -> 
		mt.map_best_hosts
	    | `Emap et -> 
		et.emap_map.map_best_hosts
	    | `Sort st ->
		most_frequent
		  (List.flatten
		     (List.map
			determine_pref_hosts
			(Mapred_sched.task_depends_on_list plan task)))
	    | `Shuffle st ->
		( try
		    [Hashtbl.find shuffle_pref_node st.shuffle_partitions]
		  with Not_found -> 
		    (* Check whether we try to influence node allocation *)
		    if !pp_round = None then (
		      (* Whether we have enough different (pmin,pmax) pairs
			 in this shuffle round so that we can keep all
			 task nodes busy
		       *)
		      let n =
			(float jc#partitions) /. st.shuffle_avg_part_width in
		      if n > float n_hosts then
			pp_round := Some (st.shuffle_round)
		    );
		    if !pp_round = Some st.shuffle_round then (
		      (* So this is a shuffle task of the round where we
			 actively try to allocate by (pmin,pmax). In
			 pp_nodes all task nodes are queued up, and we
			 take the next node in a round-robin fashion.
		       *)
		      let next_pp_node = Queue.take pp_nodes in
		      Queue.add next_pp_node pp_nodes;
		      Hashtbl.replace 
			shuffle_pref_node
			st.shuffle_partitions
			next_pp_node;
		      [next_pp_node]
		    ) else 
		      most_frequent
			(List.flatten
			   (List.map
			      determine_pref_hosts
			      (Mapred_sched.task_depends_on_list plan task)))
		)
	    | `Cleanup ->
		assert false
  in

  let determine_weight task =
    let (w_io, w_cpu) =
      match task with
	| `Cleanup -> (0.0, 0.0)
	| `Map _ -> rjc.map_weight
	| `Emap _ -> rjc.emap_weight
	| `Sort _ -> rjc.sort_weight
	| `Shuffle t -> 
	    if t.shuffle_reduce then
	      rjc.reduce_weight
	    else
	      rjc.shuffle_weight in
    (w_io, w_cpu)
  in

  let node_is_ok ip =
    not (Hashtbl.mem retry_later_nodes ip)
  in

  let pre_sort_by_utilization tasks =
    (* Select tasks so that both IO load and CPU load are maxed out as often
       as possible. Well, we don't do a real optimization here, but only
       follow some rules of thumb. If the total IO load < CPU load, we
       select the task next with the highest IO load. Otherwise we
       select the task with the highest CPU load. I.e. the idea is to
       keep IO and CPU balanced at the moment.

       We do not do this if it is more important to get quick progress in
       plan execution. The criterion is: the number of runnable tasks
       is quite low. The quickest progress is made if we leave the order
       unchanged (Mapred_sched returns the tasks in this order).
       "Quick progress" means here to execute those tasks first that
       will enable other dependent tasks (i.e. so
       that the number of runnable tasks grows).
     *)
    let total_limit =
      Hashtbl.fold (fun _ lim acc -> lim +. acc) load_limit 0.0 in
    if float(List.length tasks) < 2. *. total_limit then
      tasks
    else (
      let tasks_with_load =
	List.map (fun t -> (t, determine_weight t)) tasks in
      let by_io =
	ref
	  (List.sort
	     (fun (_,(io1,_)) (_,(io2,_)) -> -(Pervasives.compare io2 io1))
	     tasks_with_load) in
      let by_cpu =
	ref
	  (List.sort
	     (fun (_,(_,cpu1)) (_,(_,cpu2)) -> -(Pervasives.compare cpu2 cpu1))
	     tasks_with_load) in
      let prospective_io = ref !sum_io_load in
      let prospective_cpu = ref !sum_cpu_load in
      let result = ref [] in
      let set = ref TaskSet.empty in
      let n = List.length tasks in
      let k = ref 0 in
      while !k < n do
	(* FIXME: We probably need not n results here! *)
	let (t, (io,cpu)) =
	  if !prospective_io < !prospective_cpu then (
	    match !by_io with
	      | (t, (io,cpu)) :: l' ->
		  by_io := l';
		  (t, (io,cpu))
	      | [] -> assert false
	  )
	  else (
	    match !by_cpu with
	      | (t, (io,cpu)) :: l' ->
		  by_cpu := l';
		  (t, (io,cpu))
	      | [] -> assert false
	  ) in
	if not (TaskSet.mem t !set) then (
	  result := t :: !result;
	  set := TaskSet.add t !set;
	  prospective_io := !prospective_io +. io;
	  prospective_cpu := !prospective_cpu +. cpu;
	  incr k
	)
      done;
      List.rev !result
    )
  in

  let pre_sort_by_greediness ?(enable_round_points=false) tasks =
    (* Sort by greediness points *)
    let gp_map =
      List.fold_left
	(fun acc t ->
	   TaskMap.add t 
	     (Mapred_sched.greediness_points plan t +.
		(if enable_round_points then Mapred_sched.round_points plan t
		 else 0.0)
	     ) acc
	)
	TaskMap.empty
	tasks in
(*
    message job "GREEDINESS: ";
    TaskMap.iter
      (fun t gp ->
	 let s = Mapred_tasks.string_of_task_id t in
	 message job "%s: %.1f " s gp
      )
      gp_map;
    message job "\n%!";
 *)
    List.stable_sort
      (fun t1 t2 ->
	 let gp1 =
	   try TaskMap.find t1 gp_map
	   with Not_found -> assert false in
	 let gp2 =
	   try TaskMap.find t2 gp_map
	   with Not_found -> assert false in
	 Pervasives.compare gp2 gp1
      )
      tasks
  in

  let pre_sort_by_rev_rounds tasks =
    (* Sort by negative round points, i.e. first round is sorted to front *)
    let gp_map =
      List.fold_left
	(fun acc t ->
	   TaskMap.add t 
	     (-. (Mapred_sched.round_points plan t))
	     acc
	)
	TaskMap.empty
	tasks in
    List.stable_sort
      (fun t1 t2 ->
	 let gp1 =
	   try TaskMap.find t1 gp_map
	   with Not_found -> assert false in
	 let gp2 =
	   try TaskMap.find t2 gp_map
	   with Not_found -> assert false in
	 Pervasives.compare gp2 gp1
      )
      tasks
  in

  let pre_sort_mode = ref false in

  let pre_sort_default tasks =
    (* sort in 50% of the cases by greediness, and in 50% by rev rounds *)
    let tasks1 =
      pre_sort_by_utilization tasks in
    let tasks2 =
      if !pre_sort_mode then
	pre_sort_by_greediness tasks1
      else
	pre_sort_by_rev_rounds tasks1 in
    pre_sort_mode := not !pre_sort_mode;
    tasks2 in


  let pre_sort tasks =
    (* First key is greediness, then utilization. Note that the greediness
       sort is stable
     *)
    let algo = rjc.pre_sort_algo in
    match algo with
      | "NONE" -> tasks
      | "UTILIZATION" -> 
	  pre_sort_by_utilization tasks
      | "GREEDINESS" ->
	  pre_sort_by_greediness (pre_sort_by_utilization tasks)
      | "DEFAULT" ->
	  pre_sort_default tasks
      | "ROUNDS" ->
	  pre_sort_by_greediness
	    ~enable_round_points:true
	    (pre_sort_by_utilization tasks)
      | _ ->
	  failwith ("Unknown pre_sort_algo: " ^ algo)
  in

  let rec loop () =
    let tasks0 = Mapred_sched.executable_tasks plan in
    let tasks1 = 
      List.filter (fun t -> not (TaskSet.mem t !retry_later)) tasks0 in
    let tasks2 =
      if tasks1 = [] then tasks0 else tasks1 in
    let tasks =
      pre_sort tasks2 in
    message job
      (sprintf
	 "Tasks: runnable=%d running=%d finished=%d total=%d complete=%B\n"
	 (List.length tasks)
	 (Mapred_sched.n_running plan)
	 (Mapred_sched.n_finished plan)
	 (Mapred_sched.n_total plan)
	 (Mapred_sched.plan_complete plan));
    message job
      (sprintf "Load: I/O=%f CPU=%f\n"
	 !sum_io_load !sum_cpu_load);
    if not job.killed && tasks <> [] then (
      (** Get the pref_hosts for each task. Also, split into tasks with
	  preference and in tasks without. The tasks with pref will be
	  tried first for getting them allocated
       *)
      let tasks_prefs =
	List.map
	  (fun t ->
	     let ph0 = determine_pref_hosts t in
	     let ph1 = List.filter node_is_ok ph0 in
	     let ph2 = if ph1=[] then ph0 else ph1 in
	     (t, ph2)
	  )
	  tasks in
      let tasks_with, tasks_without =
	List.partition
	  (fun (t, prefs) -> prefs <> [])
	  tasks_prefs in
      (* TODO (perf): in the following, avoid to check for further job 
	 submissions if all nodes are already maxed out
       *)
      (** First try to start tasks with preferred hosts. If we reach
	  the load limit, tasks may not be startable, so some rest
	  will be left
       *)
      let tasks_with_nonstart =
	List.filter
	  (fun (t, prefs) ->
	     let weight = determine_weight t in
 	     let is_started = submit t prefs false weight loop in
	     not is_started
	  )
	  tasks_with in
      (** Now try to start any other tasks *)
      List.iter
	(fun (t, _) ->
	   let weight = determine_weight t in
	   ignore(submit t [] true weight loop)
	)
	tasks_without;
      List.iter
	(fun (t, prefs) ->
	   let weight = determine_weight t in
	   ignore(submit t prefs true weight loop)
	)
	tasks_with_nonstart;
    )
    else
      if (Mapred_sched.plan_finished plan && 
	    Mapred_sched.n_finished plan = Mapred_sched.n_total plan)
      then (
	message job "Finished job\n";
	let t1 = Unix.gettimeofday() in
	job.status <- `Successful;
	( match rjc.dump_plan_as_svg with
	    | None -> ()
	    | Some file ->
		let svg = Mapred_sched.generate_svg plan in
		let f = open_out_bin file in
		output_string f svg;
		close_out f;
		message job (sprintf "Dumped SVG: %s" file)
	);
	job.counters <-
	  Mapred_stats.add
	    (Mapred_stats.of_list
	       [ "num_tasks_successful", float !stat_tasks;
		 "num_tasks_misalloc",   float !stat_misalloc_tasks;
		 "num_input_files",      float !stat_inputs;
		 "num_input_files_nonlocal", float !stat_inputs_nonlocal;
		 "avg_running_tasks", Mapred_sched.avg_running plan;
		 "avg_runqueue_tasks", Mapred_sched.avg_runqueue plan;
		 "time_underutil", (!p_underutil /. !m_underutil *. 100.0);
		 "runtime", t1-.t0;
	       ]
	    )
	    job.counters;
	List.iter
	  (fun (label, v) ->
	     message job (sprintf "Counter %-21s %.2f\n" label v)
	  )
	  (Mapred_stats.to_list job.counters);

	message job (sprintf "Wallclock runtime:            %f seconds\n"
		       (t1 -. t0));
	when_done job
      )
  in

  message job "Checking executable versions\n";
  List.iter
    (fun (node,ip) ->
       let saddr = Unix.ADDR_INET(ip,job.port) in
       let client = new_client job saddr in
       let ok = 
	 Mr_proxy.V1.check_version client (real_version,job.me#auth_ticket) in
       if not ok then
	 failwith ("Node runs wrong task server executable: " ^ node);
       Rpc_proxy.ManagedClient.shut_down client;
       dispose_client job client;
    )
    (Mapred_sched.hosts plan);

  (* initialize load limit: *)
  Hashtbl.iter
    (fun ip cap ->
       update_load_limit ip cap
    )
    mp.capacity;

  message job "Starting job\n";
  loop();
  job


let investigate_machines config auth_ticket job_id =
  let port = config # mr_task_port in
  let cap = Hashtbl.create 27 in
  let servers = config # mr_task_nodes in
  let ips = List.map Plasma_util.ip_of_host servers in
  let esys = Unixqueue.create_unix_event_system() in
  List.iter
    (fun ip ->
       let saddr = Unix.ADDR_INET(ip,port) in
       let client = new_client_no_job auth_ticket saddr esys in
       try
	 let c = Mr_proxy.V1.job_capacity client (job_id,auth_ticket) in
	 Hashtbl.replace cap ip c;
	 Rpc_proxy.ManagedClient.shut_down client;
       with
	 | error ->
	     Rpc_proxy.ManagedClient.shut_down client;
	     raise error
    )
    ips;
  { capacity = cap }


let planning_capacity mp =
  Hashtbl.fold
    (fun _ cap acc ->
       acc +. (max cap.Mapred_rpc_aux.max_capacity 1.0)
    )
    mp.capacity
    0.0


let status job = job.status


let files_to_keep task =
  (** This only looks at *_del flags *)
  match task with
    | `Map mt ->
	[]
    | `Emap mt ->
	[]
    | `Sort st ->
	if not st.sort_input_del then
	  List.map (fun (n,_,_) -> n) st.sort_input
	else
	  []
    | `Shuffle st ->
	if not st.shuffle_input_del then
	  List.flatten
	    (List.map 
	       (fun (f,_,_) -> 
		  List.map (fun (n,_,_) -> n) f
	       )
	       st.shuffle_input
	    )
	else
	  []
    | `Cleanup -> []

let cleanup job =
  message job "Sending cleanup tasks\n";
  let sjob = 
    { job with esys = Unixqueue.create_unix_event_system() } in
  List.iter
    (fun (node,ip) ->
       let saddr = Unix.ADDR_INET(ip,job.port) in
       let c = new_client sjob saddr in
       let req_id = job.next_req_id in
       job.next_req_id <- job.next_req_id + 1;
       let rs =
	 Mr_proxy.V1.exec
	   c
	   (job.me#command_name,
	    req_id,
	    job.me#auth_ticket,
	    Mapred_job_config.marshal job.config.mjc, 
	    Mapred_tasks.encode_task `Cleanup) in
       Rpc_proxy.ManagedClient.shut_down c;
       let r, logs = Mapred_tasks.decode_task_result rs in
       print_logs job req_id logs;
       match r with
	 | `Ok _ -> ()
	 | `Retry_later ->
	     Netlog.logf `Err "Got Retry_later from cleanup task"
	 | `Corrupt_input _ ->
	     Netlog.logf `Err "Got Corrupt_input from cleanup task"
	 | `Error msg ->
	     Netlog.logf `Err "Error for task Cleanup: %s" msg
    )
    (Mapred_sched.hosts job.plan);

  if not job.config.simulate then (
    message job "Cleaning rest up\n";
    let to_keep = Hashtbl.create 5 in
    List.iter
      (fun task ->
	 let keep = files_to_keep task in
	 List.iter (fun f -> Hashtbl.replace to_keep f ()) keep;
      )
      job.started_tasks;
    let fs = job.me # filesystem in
    let to_delete = fs # readdir [] job.work_dir in
    
    List.iter
      (fun filebase ->
	 let file = job.work_dir ^ "/" ^ filebase in
	 if filebase <> "." && filebase <> ".." && 
	   not (Hashtbl.mem to_keep file) then (
	     fs # remove [] file;
	   )
      )
      to_delete;
  );

  message job "Done clean-up\n"
