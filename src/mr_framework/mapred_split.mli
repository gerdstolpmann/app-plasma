(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

val tab_split : string -> (string * string)
  (** Splits a line into key and value. The separator is the first TAB
      character.
   *)

val tab_split_key : string -> string
  (** Same as [fst (tab_split_key s)] *)

val tab_split_key_range : string -> int * int
  (** Returns only the range of the key *)

val tab2_split : string -> (string * int * string)
  (** Splits a line into (key, partition, value). Separators are the first
      and second TAB characters
   *)

val tab2_split_key : string -> string
  (** Same as [let (key,_,_) = tab2_split s in key] *)

val tab2_split_key_range : string -> int * int
  (** Returns only the range of the key *)

val tab2_split_key_partition : string -> string * int
  (** Same as [let (key,p,_) = tab2_split s in (key,p)] *)

val tab2_split_key_partition_range : string -> int * int
  (** Returns the range of the key and the partition *)

val tab2_get_partition : string -> int
  (** Returns the partition of a string "<key> TAB <partition>" *)

