(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Run a map/red job *)

val invoke : ?catch_sigint:bool ->
             ?create_dirs:bool ->
             Mapred_def.mapred_env ->
             Mapred_job_exec.runtime_job_config ->
             Mapred_def.mapred_job ->
               Mapred_job_exec.running_job
  (** Prepare the job, but do not run yet any task.

      - [catch_sigint]: whether to catch the sigint signal (normally CTRL-C).
        If caught, the job is killed, and it is waiter for the termination.
      - [create_dirs]: whether to create the output directory, the log
        directory and the work directory if they do not exist yet.
   *)

val run : Mapred_job_exec.running_job -> unit
  (** Run the tasks, and wait until the job is done *)
