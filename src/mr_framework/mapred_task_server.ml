(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let dlogf = Plasma_util.dlogf

type sh_state =
    { pid_of_req_id : (int, int) Hashtbl.t;
      job_id_of_req_id : (int, string) Hashtbl.t;
      cont_oo_id_of_req_id : (int, 
			      int (* Oo.id of Netplex_types.container_id *)
			     ) Hashtbl.t;
    }

module State_shvar =
  Netplex_sharedvar.Make_var_type(struct type t = sh_state end)

let state_name = "mapred_task_server.state"
let mutex_name = state_name ^ ".mutex"

let m = ref None

let change_state op =
  let mutex = Netplex_mutex.access mutex_name in
  Netplex_mutex.lock mutex;
  try
    ( match op with
	| `Reg_req(req_id,pid,job_id,cont_id) ->
	    let st = State_shvar.get state_name in
	    Hashtbl.replace st.pid_of_req_id req_id pid;
	    Hashtbl.replace st.job_id_of_req_id req_id job_id;
	    Hashtbl.replace st.cont_oo_id_of_req_id req_id (Oo.id cont_id);
	    State_shvar.set state_name st
	| `Del_req req_id ->
	    let st = State_shvar.get state_name in
	    Hashtbl.remove st.pid_of_req_id req_id;
	    Hashtbl.remove st.job_id_of_req_id req_id;
	    Hashtbl.remove st.cont_oo_id_of_req_id req_id;
	    State_shvar.set state_name st
	| `Del_cont cont_oo_id ->
	    let st = State_shvar.get state_name in
	    let req_id_list =
	      Hashtbl.fold 
		(fun req_id c_id acc -> 
		   if c_id = cont_oo_id then req_id :: acc else acc
		)
		st.cont_oo_id_of_req_id
		[] in
	    List.iter
	      (fun req_id ->
		 Hashtbl.remove st.pid_of_req_id req_id;
		 Hashtbl.remove st.job_id_of_req_id req_id;
		 Hashtbl.remove st.cont_oo_id_of_req_id req_id;
	      )
	      req_id_list;
	    State_shvar.set state_name st
	| `Init ->
	    let st = 
	      { pid_of_req_id = Hashtbl.create 5;
		job_id_of_req_id = Hashtbl.create 5;
		cont_oo_id_of_req_id = Hashtbl.create 5;
	      } in
	    let ok =
	      Netplex_sharedvar.create_var ~enc:true state_name in
	    if ok then
	      State_shvar.set state_name st
    );
    Netplex_mutex.unlock mutex;
  with
    | error ->
	Netplex_mutex.unlock mutex;
	raise error


let count_jobs job_id_opt =
  (* Include job_id_opt if passed *)
  let st = State_shvar.get state_name in
  let ht = Hashtbl.create 5 in
  Hashtbl.iter
    (fun _ job_id ->
       Hashtbl.replace ht job_id ()
    )
    st.job_id_of_req_id;
  ( match job_id_opt with
      | None -> ()
      | Some id -> Hashtbl.replace ht id ()
  );
  Hashtbl.length ht

let real_stderr = ref (Unix.dup Unix.stderr)


let real_stderr_print s =
  Netsys.really_gwrite `Read_write !real_stderr s 0 (String.length s)



let redirect_log tm prefix msgq f arg =
  let log_dir = tm#log_directory in
  ( try Unix.mkdir log_dir 0o777
    with Unix.Unix_error (Unix.EEXIST,_,_) -> ()
  );
  let levels = [ `Debug; `Info; `Warning; `Crit ] in
  let files =
    List.map
      (fun level ->
	 let level_s = Netlog.string_of_level level in
	 let name = sprintf "%s/%s%s.log" log_dir prefix level_s in
	 open_out name
      )
      levels in
  let n_msgs = ref 0 in
  let logger lev msg =
    if lev <> `Debug && lev <> `Info && lev <> `Notice then (
      incr n_msgs;
      if !n_msgs < 5 then
	Queue.add msg msgq;
    );
    List.iter2
      (fun level ch ->
	 Netlog.channel_logger ch level lev msg
      )
      levels
      files in

  let new_stderr_name = sprintf "%s/%sstderr.log" log_dir prefix in
  let new_stderr = 
    Unix.openfile 
      new_stderr_name
      [Unix.O_CREAT; Unix.O_WRONLY; Unix.O_APPEND]
      0o666 in
  let old_logger = !Netlog.current_logger in
  let revert () =
    Netlog.current_logger := old_logger;
    Unix.dup2 !real_stderr Unix.stderr;
    List.iter (fun ch -> close_out ch) files;
    if !n_msgs > 5 then
      Queue.add (sprintf "  (and %d more log messages)" (!n_msgs -5)) msgq;
  in
  Netlog.current_logger := logger;
  Unix.dup2 new_stderr Unix.stderr;
  Unix.close new_stderr;
  try
    let r = f arg in
    revert();
    r
  with
    | error ->
	Queue.add (sprintf "Exception: %s" (Netexn.to_string error)) msgq;
	Netlog.logf `Err "Exception: %s" (Netexn.to_string error);
	revert();
	raise error


let upload_log me tm prefix filter_flag =
  try
    let files = tm # task_files_log_directory() in
    let prefix_l = String.length prefix in
    let files' =
      if filter_flag then
	List.filter
	  (fun name ->
	     String.length name >= prefix_l &&
	       String.sub name 0 prefix_l = prefix
	  )
	  files
      else
	files  in
    tm # task_save_log_directory me#filesystem files';
  with
    | error ->
	Netlog.logf `Err "Exception upload_log: %s" (Netexn.to_string error)	


(* needed config:
   - mapred_env:
     * cluster name, namenodes
     * config file
     * arguments (string array) --> exec!
 *)

let shm_mng_process = lazy(new Plasma_shm.netplex_shm_manager())

(* CHECK: call [finish] again from the close-connection handler? *)

let finish() =
  (* At the end of the request, GC all data. We have a few
     large arrays etc. that can usually be collected now. We run
     full_major several times because finalisers may delay when the GC
     can detect dead memory.
   *)
  for k = 1 to 5 do
    Gc.full_major()
  done;
  (* Maybe we can release memory now in our own bookkeeping *)
  let shm_mng = Lazy.force shm_mng_process in
  shm_mng # push();
  (* Debug *)
  let d =
    sprintf "TASK FINISH[pid %d]: %s \n"
      (Unix.getpid()) 
      shm_mng#dump in
  real_stderr_print d


let open_cluster conf esys =
  Plasma_client.open_cluster 
    conf#nn_clustername
    (List.map Plasma_util.parse_host_port conf#nn_nodes)
    esys


let open_fs conf shm_mng auth_ticket =
  Mapred_fs.standard_fs
    ~configure_cluster: (fun c ->
			   Plasma_client.configure_shm_manager c shm_mng;
			   Plasma_client.configure_auth_ticket c auth_ticket;
			   Plasma_client.configure_buffer c 16;
			)
    conf


let check_user conf auth_ticket =
  if not conf#nn_disabled then (
    (* Check whether auth_ticket is valid: *)
    let c = open_cluster conf (Unixqueue.create_unix_event_system()) in
    ( try
	Plasma_client.configure_auth_ticket c auth_ticket;
	let _ = Plasma_client.blocksize c in
	()
      with
	| Plasma_client.Plasma_error `eaccess ->
	    Plasma_client.close_cluster c;
	    failwith "Access denied (bad authentication ticket)"
	| error ->
	    Plasma_client.close_cluster c;
	    raise error
    );
    Plasma_client.close_cluster c;
    (* Check whether the user is permitted: *)
    match conf#mr_users with
      | None -> ()
      | Some ul ->
	  let (u,_,_) = Plasma_ug.principal_of_auth_ticket auth_ticket in
	  if not (List.mem u ul) then (
	    Netlog.logf `Err "User not permitted: %s" u;
	    failwith "Access denied (user not in mr_users)"
	  )
  )

let proc_exec (mj_list,conf,cf) 
              (cmd_name, req_id, auth_ticket, jobconfig_s, enc_task) =
  check_user conf auth_ticket;
  let shm_mng = Lazy.force shm_mng_process in
  let d =
    sprintf "TASK START[pid %d]: %s \n"
      (Unix.getpid()) 
      shm_mng#dump in
  real_stderr_print d;
  let wm_shm = shm_mng # wm `Shm in
  let wm_buf = shm_mng # wm `Buf in
  let wm_d = 
    sprintf
      "WATERMARKS: SHM low=%Ld high=%Ld max=%Ld BUF low=%Ld high=%Ld max=%Ld\n" 
      wm_shm.Plasma_shm.low wm_shm.Plasma_shm.high wm_shm.Plasma_shm.max
      wm_buf.Plasma_shm.low wm_buf.Plasma_shm.high wm_buf.Plasma_shm.max in
  real_stderr_print wm_d;
  let ucounters = ref Mapred_stats.empty in
  let fs = open_fs conf shm_mng auth_ticket in
  let me =
    ( object
	method filesystem = fs
	method shm_manager = shm_mng
	method config = conf
	method config_file = cf
	method auth_ticket = auth_ticket
	method command_name = cmd_name
	method accumulate st = 
	  ucounters := Mapred_stats.add !ucounters st
      end
    ) in
  let tm_opt = ref None in
  let task_prefix = sprintf "task_%06d_" req_id in
  let cont_id = (Netplex_cenv.self_cont() :> Netplex_types.container_id) in
  let step = ref "start" in
  let msgq = Queue.create() in
  let msgs() = List.rev(Queue.fold (fun acc x -> x::acc) [] msgq) in
  try
    let mj =
      try (List.assoc cmd_name mj_list) me
      with Not_found ->
	failwith ("Cannot find job definition for cmd: " ^ cmd_name) in
    step := "unmarshal_jobconfig";
    let mjc = Mapred_job_config.unmarshal jobconfig_s in
    let jc = Mapred_job_config.mapred_job_config mjc in
    step := "decode_task";
    let task = Mapred_tasks.decode_task enc_task in
    let tm = Mapred_taskfiles.scp_taskfile_manager me#config jc in
    tm_opt := Some tm;
    let job_id = jc#job_id in
    step := "change_state1";
    change_state (`Reg_req(req_id, Unix.getpid(), job_id, cont_id));
    let ti =
      ( object
	  method req_id = req_id
	  method task_prefix = task_prefix
	  method partition = 
	    match task with
	      | `Shuffle s when s.Mapred_tasks.shuffle_reduce -> 
		  fst s.Mapred_tasks.shuffle_partitions
	      | _ ->
		  failwith "partition: not in a reduce task"
	end
      ) in
    step := "redirect_log";
    let r = 
      redirect_log tm task_prefix msgq
	(fun () ->
	   step := "execute";
	   Mapred_task_exec.execute me jc mj task ti
	)
	() in
    step := "upload_log";
    upload_log me tm task_prefix (task <> `Cleanup);
    if task = `Cleanup then (
      step := "delete_log_directory";
      tm # task_delete_log_directory();
    );
    step := "change_state2";
    change_state (`Del_req req_id);
    step := "finish";
    finish();
    step := "fs#close";
    fs # close();
    let r1 =
      match r with
	| `Ok ok ->
	    Mapred_tasks.(
	      `Ok { ok with
		      ok_counters = Mapred_stats.add ok.ok_counters !ucounters
		  }
	    )
	| _ -> r in
    if shm_mng # have_local_allocations then (
      Netlog.logf `Warning
	"Shutting container down after failure to deallocate memory";
      (Netplex_cenv.self_cont()) # shutdown();
    );
    step := "encode_task_result";
    Mapred_tasks.encode_task_result (r1, msgs())
  with
    | error ->
	Netlog.logf `Err "Exception in exec (pid %d, step %s): %s"
	  (Unix.getpid()) !step (Netexn.to_string error);
	( match !tm_opt with
	    | None -> ()
	    | Some tm ->
		upload_log me tm task_prefix true
	);
	finish();
	( try
	    fs # close()
	  with
	    | close_error ->
		Netlog.logf `Err "Exception in fs#close (pid %d): %s"
		  (Unix.getpid()) (Netexn.to_string close_error)
	);
	(* Trigger the shutdown of the Netplex container after errors.
	   The Netplex controller will start a replacement process if
	   needed.
	 *)
	Netlog.logf `Warning
	  "Shutting container down after exception";
	(Netplex_cenv.self_cont()) # shutdown();
	Mapred_tasks.encode_task_result
	  (`Error (Netexn.to_string error), msgs())


let proc_simulate (mj_list,conf,cf) 
                  (cmd_name, req_id, auth_ticket, jobconfig_s, enc_task) =
  check_user conf auth_ticket;
  let shm_mng = new Plasma_shm.null_shm_manager() in
  let fs = open_fs conf shm_mng auth_ticket in
  let me =
    ( object
	method filesystem = fs
	method shm_manager = shm_mng
	method config = conf
	method config_file = cf
	method auth_ticket = auth_ticket
	method command_name = cmd_name
	method accumulate (st:Mapred_stats.stats) = ()
      end
    ) in
  let cont_id = (Netplex_cenv.self_cont() :> Netplex_types.container_id) in
  try
    let mj =
      try (List.assoc cmd_name mj_list) me
      with Not_found ->
	failwith ("Cannot find job definition for cmd: " ^ cmd_name) in
    let mjc = Mapred_job_config.unmarshal jobconfig_s in
    let jc = Mapred_job_config.mapred_job_config mjc in
    let task = Mapred_tasks.decode_task enc_task in
    let job_id = jc#job_id in
    change_state (`Reg_req(req_id, Unix.getpid(), job_id, cont_id));
    let r = 
      Mapred_task_sim.execute me jc mj task in
    change_state (`Del_req req_id);
    finish();
    fs # close();
    Mapred_tasks.encode_task_result (r,[])
  with
    | error ->
	Netlog.logf `Err "Exception in simulate (pid %d): %s"
	  (Unix.getpid()) (Netexn.to_string error);
	finish();
	fs # close();
	Mapred_tasks.encode_task_result
	  (`Error (Netexn.to_string error), [])


let proc_kill conf (req_id, auth_ticket) =
  check_user conf auth_ticket;
  let st = State_shvar.get state_name in
  try
    let pid = Hashtbl.find st.pid_of_req_id req_id in
    ( try
	Unix.kill pid Sys.sigterm
      with
	| error ->
	    Netlog.logf `Err "Cannot kill pid %d: %s"
	      pid
	      (Netexn.to_string error);
    );
    change_state (`Del_req req_id)
  with
    | Not_found ->
	()


let proc_kill_all conf auth_ticket =
  check_user conf auth_ticket;
  let st = State_shvar.get state_name in
  Hashtbl.iter
    (fun req_id pid ->
       ( try
	   Unix.kill pid Sys.sigterm
	 with
	   | error ->
	       Netlog.logf `Err "Cannot kill pid %d: %s"
		 pid
		 (Netexn.to_string error);
       );
       change_state (`Del_req req_id)
    )
    st.pid_of_req_id


let proc_configure conf (rc,auth_ticket) =
  let open Mapred_rpc_aux in
  check_user conf auth_ticket;
  let shm_max, shm_limit =
    match rc.rc_shm_max with
      | Some m -> (m, m)
      | None ->
	  ( match Platform.shm_limit () with
	      | None ->
		  Netlog.logf `Warning
		    "Cannot determine a good limit of shared mem for this system";
		  Netlog.logf `Warning
		    "You can override this by setting shm_max explicitly.";
		  (Int64.max_int, Int64.max_int)
	      | Some m ->
		  (Int64.of_float(Int64.to_float m *. rc.rc_shm_max_p),
		   m
		  )
	  ) in
  let shm_high =
    match rc.rc_shm_high with
      | Some h -> h
      | None ->
	  if shm_limit < Int64.max_int then
	    Int64.of_float(Int64.to_float shm_limit *. rc.rc_shm_high_p)
	  else
	    Int64.max_int in
  let shm_low =
    match rc.rc_shm_low with
      | Some h -> h
      | None ->
	  if shm_limit < Int64.max_int then
	    Int64.of_float(Int64.to_float shm_limit *. rc.rc_shm_low_p)
	  else
	    0L in

  let buf_max, buf_limit =
    match rc.rc_buf_max with
      | Some m -> (m, m)
      | None ->
	  ( match Platform.physical_ram () with
	      | None ->
		  Netlog.logf `Warning
		    "Cannot determine the size of physical RAM for this system";
		  Netlog.logf `Warning
		    "You can override this by setting shm_max explicitly.";
		  (Int64.max_int, Int64.max_int)
	      | Some m ->
		  (Int64.of_float(Int64.to_float m *. rc.rc_buf_max_p),
		   m
		  )
	  ) in
  let buf_high =
    match rc.rc_buf_high with
      | Some h -> h
      | None ->
	  if buf_limit < Int64.max_int then
	    Int64.of_float(Int64.to_float buf_limit *. rc.rc_buf_high_p)
	  else
	    Int64.max_int in
  let buf_low =
    match rc.rc_buf_low with
      | Some h -> h
      | None ->
	  if buf_limit < Int64.max_int then
	    Int64.of_float(Int64.to_float buf_limit *. rc.rc_buf_low_p)
	  else
	    0L in

  Plasma_shm.Netplex_shm_manager.init
    ~shm_wm: { Plasma_shm.low = shm_low;
	       high = shm_high;
	       max = shm_max
	     }
    ~buf_wm: { Plasma_shm.low = buf_low;
	       high = buf_high;
	       max = buf_max
	     }


let real_version =
  Mapred_config.executable_version()

let proc_check_version conf (expected_version,auth_ticket) =
  check_user conf auth_ticket;
  expected_version = real_version


let proc_job_capacity conf (job_id, auth_ticket) =
  check_user conf auth_ticket;
  let n_jobs = count_jobs (Some job_id) in
  let n_cores_opt = Platform.number_cores() in
  match n_cores_opt with
    | Some n_cores ->
	{ Mapred_rpc_aux.max_capacity = float n_cores;
	  avail_capacity = float n_cores /. float n_jobs 
	}
    | None ->
	{ Mapred_rpc_aux.max_capacity = (-1.0);
	  avail_capacity = (-1.0)
	}


let setup srv (mj_list,conf,cf,access) =
  if not conf#nn_disabled then
    Mapred_auth.configure_rpc_server srv access;
  Mapred_rpc_srv.Mapred_task.V1.bind
    ~proc_null:(fun () -> ())
    ~proc_exec:(proc_exec (mj_list,conf,cf))
    ~proc_simulate:(proc_simulate (mj_list,conf,cf))
    ~proc_kill:(proc_kill conf)
    ~proc_kill_all:(proc_kill_all conf)
    ~proc_configure:(proc_configure conf)
    ~proc_check_version:(proc_check_version conf)
    ~proc_job_capacity:(proc_job_capacity conf)
    srv;
  Rpc_server.set_exception_handler
    srv
    (fun e ->
       prerr_endline(Netexn.to_string e)
    );
  Rpc_server.set_onclose_action
    srv
    (fun _ ->
       ()
    )


let msg_helper_name = "mapred_task_server.msg_helper"

let msg_helper_factory ctrl =
  (* This is a helper process that just listens for broadcast messages,
     and executes the required actions in container context
   *)
  Netplex_kit.add_helper_service ctrl msg_helper_name
    (  object
         inherit Netplex_kit.empty_processor_hooks()

	 method receive_message cont name args =
	   match name with
	     | "del_cont" ->
		 let id = int_of_string args.(0) in
		 change_state (`Del_cont id)
	     | _ ->
		 ()
       end
    )


let factory mj_list job_cf =
  Rpc_netplex.rpc_factory
   ~configure:(fun cf cfaddr -> 
		 let access = 
		   Mapred_auth.extract_access_config cf cf#root_addr in
		 (mj_list, Mapred_config.extract_config job_cf, job_cf, access))
    ~hooks:(fun _ ->
              ( object
                  inherit Netplex_kit.empty_processor_hooks()
                    
                  method post_add_hook socksrv ctrl =
		    ctrl # add_plugin Netplex_mutex.plugin;
		    ctrl # add_plugin Netplex_sharedvar.plugin;

                  method post_start_hook cont =
		    real_stderr := Unix.dup Unix.stderr;
		    change_state `Init;

		    Rpc_server.Debug.enable := true;
		    Rpc_server.Debug.enable_ctrace := true;
		    Rpc_server.Debug.enable_ptrace := true;

		  method post_finish_hook _ ctrl cont_id =
		    (* We'd like to remove this container from sh_state.
		       We cannot do it immediately here because we are in
		       controller context, and access to shared state
		       is not allowed. So, we send a message to the 
		       helper process to execute this.
		     *)
		    ctrl # send_message
		      msg_helper_name
		      "del_cont"
		      [| string_of_int (Oo.id cont_id) |]

		end
	      )
	   )
    ~setup
    ~name:"mapred_task_server"
    ()
