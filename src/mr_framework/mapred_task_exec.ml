(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Mapred_def
open Mapred_tasks
open Plasma_rpcapi_aux
open Printf
open Uq_engines.Operators

let dlogf = Plasma_util.dlogf

let run_stack f s =
  while not (Stack.is_empty s) do
    f (Stack.pop s)
  done


let fragment_reader ?(divisor=1) factory me rc file =
  let fs = me # filesystem in
  let file' =
    List.map
      (fun (filename,block,len) ->
	 let len' =
	   if len = 0L then
	     Mapred_io.file_blocks fs filename
	   else
	     len in
	 (filename,block,len')
      )
      file in
  let rc1 = Mapred_io.divide rc divisor in
  Mapred_io.read_multiple
    fs rc
    ~readers:(`Sync
	       (List.map
		  (fun (filename,block,len) ->
		     (fun () ->
			dlogf
			  "fragment_reader(%s,%Ld,%Ld)" filename block len;
			let rd = 
			  factory # read_file
			    [] fs me#shm_manager rc1 filename block len in
			rd
		     )
		  )
		  file'
	       ))
    ()


let delete_file fs file =
  let ht = Hashtbl.create 5 in
  List.iter (fun (name,_,_) -> Hashtbl.replace ht name ()) file;
  Hashtbl.iter
    (fun name () ->
       Mapred_io.delete_file fs name
    )
    ht


let delete_files fs files =
  List.iter
    (fun file ->
       delete_file fs file
    )
    files


let get_pref_nodes loc =
  match loc with
    | `Any ->
	["_local"]
    | `Dn_identity s ->
	[s]


let handle_out_of_memory fs =
  try
    Unix.sleep 5;   (* hmm, give OS time to free memory *)
    `Retry_later
  with
    | error ->
	let bt = Printexc.get_backtrace() in
	let msg =
	  sprintf "Exception in handle_out_of_memory: %s%s"
	    (Netexn.to_string error)
	    (if Printexc.backtrace_status() then
	       " - backtrace: " ^ bt
	     else ""
	    ) in
	`Error msg


let record_config me jc =
  Mapred_def.get_rc me jc#bigblock_size


type workfile_manager =
    { prefix : string;
      mutable files : (string * string) list;
      fs : Mapred_fs.filesystem;
    }

let wf_manager fs =
  { prefix = "_" ^ Digest.to_hex (Plasma_rng.random_bytes 16) ^ "_";
    files = [];
    fs;
  }

let create_wf ?repl ?pref_nodes wfm final_name =
  let dir = Filename.dirname final_name in
  let final_base = Filename.basename final_name in
  let wf_name = dir ^ "/" ^ wfm.prefix ^ final_base in
  wfm.files <- (wf_name, final_name) :: wfm.files;
  Mapred_io.create_file ?repl ?pref_nodes wfm.fs wf_name;
  wf_name


let fail_too_late() =
  failwith "A previous attempt succeeded already"



let commit_wf wfm task =
  (* Ideally, this would be done in a single transaction. However, we
     don't have this in our filesystem abstraction, and many filesystems
     do not support this.

     We only rename back from wf_name to final_name if final_name
     does not exist yet. This is needed for speculative execution, i.e.
     the same task is started several times. The first task finishing
     execution wins.
   *)
  if wfm.files <> [] then (
    let final_name = snd (List.hd wfm.files) in
    let lock_name =
      (Filename.dirname final_name) ^ "/_lock_" ^ 
	Mapred_tasks.lock_name_of_task_id task in
    ( try
	Mapred_io.create_file wfm.fs lock_name;
	wfm.fs # commit_files wfm.files;
	Mapred_io.delete_file wfm.fs lock_name
      with
	  Unix.Unix_error(Unix.EEXIST,_,_) -> fail_too_late()
    );
    wfm.files <- []
  )

let remove_wf wfm =
  try
    List.iter
      (fun (wf_name, final_name) ->
	 try
	   wfm.fs # remove [] wf_name
	 with Unix.Unix_error(Unix.ENOENT,_,_) -> ()
      )
      wfm.files;
    wfm.files <- []
  with
    | error ->
	Netlog.logf `Warning
	  "Exception in remove_fs: %s"
	  (Netexn.to_string error)


(**********************************************************************)
(* map                                                                *)
(**********************************************************************)

let exec_map_task me jc mj (t:map_task) ti =
  let cleanup = Stack.create() in
  let wfiles = ref [] in
  let fs = me # filesystem in
  let wfm = wf_manager fs in
  try
    (* The output of a map task should only be written to the local node.
       Also, be careful and set replication to 1.
     *)
    let pref_nodes = [ "_local" ] in
    let repl =
      if jc#phases = `Map then 0 else 1 in

    let rc = record_config me jc in
    let rd_factory = mj#input_record_io me jc in
    let reader = fragment_reader rd_factory me rc t.map_input in
    Stack.push (fun () -> reader # close_in()) cleanup;

(*    
    let create_async prefix k =
      let name = sprintf "%s%05d" prefix k in
      Mapred_io.create_file_e ~repl ~pref_nodes fs name
      ++ (fun () ->
	    (* Plasma_client.abort_cluster c; - impossible in async context *)
	    wfiles := (`Tag "out", [name, 0L, 0L]) :: !wfiles;
	    eps_e (`Done name) esys
	 ) in
 *)
    let create_sync prefix k =
      let suffix = t.map_output_suffix in
      let name = sprintf "%s%05d%s" prefix k suffix in
      let wf_name = create_wf ~repl ~pref_nodes wfm name in
      wfiles := (`Tag "out", [name, 0L, 0L]) :: !wfiles;
      wf_name
    in

    let dummy_sorter = 
      if jc#phases = `Map then
	Mapred_sorters.null_sorter me jc 0.0
      else
	mj # sorter me jc 0.0 in
    let lines_limit = 
      dummy_sorter # sort_lines_limit me#config#mr_sort_size  in
    let size_limit =
      Int64.of_int (me#config#mr_sort_size - me#config#mr_sort_size/64) in
    (* subtract 1/64 for LF, length bytes, chunk headers etc. *)

    let wr_factory =
      if jc#phases = `Map then
	( mj#output_record_io me jc :> Mapred_io.record_writer_factory )
      else
	( mj#internal_record_io me jc :> Mapred_io.record_writer_factory ) in
    let writer =
      Mapred_io.write_multiple
	fs me#shm_manager rc t.map_output_prefix
	size_limit
	lines_limit
	~create_sync ~create_async:None (* (Some create_async) *)
	wr_factory in
    Stack.push (fun () -> writer # close_out()) cleanup;

    mj # map me jc ti reader writer;

    run_stack (fun f -> f()) cleanup;
    commit_wf wfm (`Map t);

    let stats =
      Mapred_stats.add reader#stats writer#stats in
    let ok =
      { ok_files = !wfiles;
	ok_counters = stats
      } in
    `Ok ok
  with
    | Plasma_util.Out_of_shared_memory
    | Plasma_shm.Shm_max_exceeded
    | Plasma_shm.Buf_max_exceeded ->
	Netlog.logf `Warning "Out_of_shared_memory";
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	handle_out_of_memory fs
    | error ->
	let bt = Printexc.get_backtrace() in
	let msg =
	  sprintf "Exception %s%s"
	    (Netexn.to_string error)
	    (if Printexc.backtrace_status() then
	       " - backtrace: " ^ bt
	     else ""
	    ) in
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	`Error msg

(**********************************************************************)
(* sort                                                               *)
(**********************************************************************)

let ba_substring ba pos len =
  let s = String.create len in
  Netsys_mem.blit_memory_to_string ba pos s 0 len;
  s
	
let exec_sort_task me jc mj (t:sort_task) =
  (* very dumb impl: read everything into an array, sort it, write it.
     This works for every record reader
   *)
  let cleanup = Stack.create() in
  let fs = me # filesystem in
  let wfm = wf_manager fs in
  try
    dlogf "Start sort %s"
      (Mapred_tasks.string_of_task_id (`Sort t));

    (* The output of a sort task should only be written to the local node.
       Also, be careful and set replication to 1.
     *)
    let pref_nodes = get_pref_nodes t.sort_output_locality in
    let repl =
      if jc#phases = `Map_sort then 0 else 1 in

    let wf_name =
      create_wf ~repl ~pref_nodes wfm t.sort_output in
    
    let rc = record_config me jc in
    let rd_factory = mj#internal_record_io me jc in
    let reader = fragment_reader rd_factory me rc t.sort_input in
    Stack.push (fun () -> reader # close_in()) cleanup;

    let sorter = mj # sorter me jc 1.0 in
    sorter # set_key_extraction (mj # extract_key me jc);

    let q = Queue.create() in
    ( try
	while true do
	  reader # input_records q;
	  sorter # put_records q;
	done
      with
	| End_of_file -> ()
    );

    reader#close_in();

    let wr_factory = 
      if jc#phases = `Map_sort then
	mj # output_record_io me jc
      else
	mj # internal_record_io me jc in
    let writer =
      wr_factory # write_file fs me#shm_manager rc wf_name in
    Stack.push (fun () -> writer # close_out()) cleanup;

    sorter # sort writer;

    let sort_stats =
      Mapred_stats.of_list
	[ "sort_time", sorter#sort_time ] in

    dlogf "Done sort %s"
      (Mapred_tasks.string_of_task_id (`Sort t));

    reader # close_in();

    run_stack (fun f -> f()) cleanup;
    commit_wf wfm (`Sort t);

    if t.sort_input_del then
      delete_file fs t.sort_input;

    let stats =
      Mapred_stats.addl [ reader#stats; writer#stats; sort_stats ] in
    let ok =
      { ok_files = [`Tag "out", [t.sort_output, 0L, 0L]];
	ok_counters = stats
      } in
    `Ok ok
  with
    | Plasma_util.Out_of_shared_memory
    | Plasma_shm.Shm_max_exceeded
    | Plasma_shm.Buf_max_exceeded ->
	Netlog.logf `Warning "Out_of_shared_memory";
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	handle_out_of_memory fs
    | error ->
	let bt = Printexc.get_backtrace() in
	let msg =
	  sprintf "Exception %s%s"
	    (Netexn.to_string error)
	    (if Printexc.backtrace_status() then
	       " - backtrace: " ^ bt
	     else ""
	    ) in
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	`Error msg


(**********************************************************************)
(* enhanced map                                                       *)
(**********************************************************************)

let exec_emap_task me jc mj (t:emap_task) ti =
  let mt = t.emap_map in
  let cleanup = Stack.create() in
  let wfiles = ref [] in
  let windex = ref 0 in
  let stats = ref Mapred_stats.empty in
  let fs = me#filesystem in
  let wfm = wf_manager fs in
  try
    let rc = record_config me jc in
    let rd_factory = mj#input_record_io me jc in
    let reader = fragment_reader rd_factory me rc mt.map_input in
    Stack.push (fun () -> reader # close_in()) cleanup;

    let sort_limit = me#config#mr_sort_size in
    let extract =
      mj # extract_key me jc in
    let part_key =
      mj # partition_of_key me jc in

    let dummy_sorter = mj # sorter me jc 0.0 in

    let shm_mng = me # shm_manager in
    let buf_id = shm_mng # new_id `Buf in
    Stack.push (fun () -> shm_mng # book_allocation buf_id 0) cleanup;
    shm_mng # book_allocation buf_id sort_limit;

    let line_id = shm_mng # new_id `Buf in
    Stack.push (fun () -> shm_mng # book_allocation line_id 0) cleanup;
    
    (* manage the pre-partitioning *)
    let pp_buffers = Hashtbl.create 7 in
    let pp_index = 
      Array.make jc#partitions (`Tag "", dummy_sorter, Queue.create(), `Any, 
				ref 0, 0, ref 0, 0) in
    let n_outputs = List.length t.emap_output in
    let factor = 1.0 /. float n_outputs in

    let pp_init() =
      List.iter
	(fun (tag,pmin,pmax,loc) ->
	   let sorter =
	     if jc#phases = `Map then
	       Mapred_sorters.null_sorter me jc factor
	     else
	       mj # sorter me jc factor in
	   sorter # set_key_extraction extract;
	   let q = Queue.create() in
	   let length = ref 0 in
	   let length_limit =
	     (sorter#eff_sort_limit - sorter#eff_sort_limit/64) in
	   (* subtract 1/64 for LF, length bytes, chunk headers etc. *)
	   let lines = ref 0 in
	   let lines_limit = sorter # sort_lines_limit sorter#eff_sort_limit in
	   for p = pmin to pmax do
	     pp_index.(p) <- (tag, sorter, q, loc, 
			      length, length_limit,
			      lines, lines_limit
			     )
	   done;
	   Hashtbl.replace pp_buffers tag (sorter,q,loc)
	)
	t.emap_output in
    pp_init();

    let sort_and_write_this tag sorter q loc =
      let `Tag str_tag = tag in
      dlogf "sort_and_write for tag %s" str_tag;
      sorter # put_records q;
      assert(Queue.length q = 0);

      let wr_name = sprintf "%s%05d" mt.map_output_prefix !windex in
      incr windex;

      let pref_nodes = get_pref_nodes loc in
      let repl =
	if jc#phases = `Map then 0 else 1 in
      let wf_wr_name =
	create_wf ~repl ~pref_nodes wfm wr_name in
      wfiles := (tag, [ wr_name, 0L, 0L ]) :: !wfiles;

      let wr_factory = 
	if jc#phases = `Map || jc#phases = `Map_sort then
	  mj # output_record_io me jc
	else
	  mj # internal_record_io me jc in
      let writer = wr_factory # write_file fs shm_mng rc wf_wr_name in
      Stack.push (fun () -> writer # close_out()) cleanup;
	   
      sorter # sort writer;

      let sort_stats =
	Mapred_stats.of_list
	  [ "sort_time", sorter#sort_time ] in
      stats := Mapred_stats.add !stats sort_stats;

      writer # close_out();
      stats := Mapred_stats.add !stats writer#stats;
	   
      let _ = Stack.pop cleanup in ();  (* undo: writer#close_out *)
      dlogf "Done sort_and_write tag %s" str_tag
    in

    let sort_and_write() =
      (* iterate over the buffers, sort them, and write them to files *)
      dlogf "sort_and_write";
      Hashtbl.iter
	(fun tag (sorter,q,loc) ->
	   sort_and_write_this tag sorter q loc
	)
	pp_buffers
    in
      
    (* We need a special writer *)
    let closed = ref false in
    let wr : Mapred_io.record_writer =
      ( object(self)

	  method output_records q =
	    let out = self#output_record in
	    Queue.iter out q

	  method output_record line =
	    let n = String.length line in

	    let (key_start,key_len) = extract line in
	    let p = part_key line key_start key_len in
	    let (tag, sorter, q, loc, 
		 length, limit, lines, lines_limit) = pp_index.(p) in

	    if !length + n > limit || !lines >= lines_limit then (
	      sorter # put_records q;      
	      sort_and_write_this tag sorter q loc;
	      length := 0;
	      lines := 0;
	      for n=1 to 10 do Gc.full_major() done;
	    );
	    
	    Queue.add line q;
	    if Queue.length q > 20 then
	      sorter # put_records q;

	    length := !length + n;
	    incr lines

	  method from_fd_e fd esys = 
	    let fd_style = Netsys.get_fd_style fd in
	    let fd_buf = 
	      Uq_io.create_in_buffer (`Polldescr (fd_style,fd,esys)) in
	    let dev = `Buffer_in fd_buf in
	    
	    let read_lines prev_lines =
	      List.iter
		(fun prev_line -> self # output_record prev_line)
		prev_lines;
	      Uq_io.input_lines_e dev in

	    let read_stream =
	      Stream.from (fun _ -> Some read_lines) in

	    let stream_e =
	      Uq_engines.stream_seq_engine [] read_stream esys
	      >> (function
		    | `Done _ -> assert false
		    | `Error End_of_file -> `Done ()
		    | `Error e -> `Error e
		    | `Aborted -> `Aborted
		 ) in

	    stream_e

	  method from_dev_e q bdev _ _ _ =
	    failwith "from_dev_e not implemented";

	  method flush() = ()

	  method close_out() =
	    if not !closed then (
	      Hashtbl.iter
		(fun _ (sorter,q,_) -> sorter#put_records q)
		pp_buffers;
	      sort_and_write();
	      Hashtbl.iter 
		(fun _ (sorter,_,_) -> sorter#close()) 
		pp_buffers;
	      closed := true
	    )
	  method abort() = ()
	  method filesystem = fs
	  method record_config = rc
	  method stats = assert false
	end
      ) in
    (* no need to add this writer to cleanup *)

    mj # map me jc ti reader wr;

    wr#close_out();
    run_stack (fun f -> f()) cleanup;
    commit_wf wfm (`Emap t);
    
    stats := Mapred_stats.add !stats reader#stats;
    let ok =
      { ok_files = (List.rev !wfiles);
	ok_counters = !stats
      } in
    `Ok ok
  with
    | Plasma_util.Out_of_shared_memory
    | Plasma_shm.Shm_max_exceeded
    | Plasma_shm.Buf_max_exceeded ->
	Netlog.logf `Warning "Out_of_shared_memory";
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	handle_out_of_memory fs
    | error ->
	let bt = Printexc.get_backtrace() in
	let msg =
	  sprintf "Exception %s%s"
	    (Netexn.to_string error)
	    (if Printexc.backtrace_status() then
	       " - backtrace: " ^ bt
	     else ""
	    ) in
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	`Error msg


(**********************************************************************)
(* shuffle                                                            *)
(**********************************************************************)

type shuffle_in =
    { in_index : int;   (* which input file *)
      in_rec : string;  (* current input record *)
      in_key_start : int;  (* key position in in_rec *)
      in_key_len : int;
      in_hash : int;    (* hash of the key *)
      in_cmp : shuffle_in -> shuffle_in -> int;    
                        (* compares the key with another one *)
    }


module Rd = struct
  type t = shuffle_in

  (* This _must_ be the same criterion as for sorting! *)
  let compare (r1:t) (r2:t) =
    if r1.in_hash = r2.in_hash then (
      let d = r1.in_cmp r1 r2 in
      if d = 0 then
	r1.in_index - r2.in_index
      else
	d
    )
    else
      r1.in_hash - r2.in_hash
end

module RdSet0 = Set.Make(Rd)


(* This is only faster for very small sets (maybe < 4 elements) *)
(*
module RdSet = struct
  type t = shuffle_in list

  let empty = []

  let min_elt s =
    match s with
      | [] -> raise Not_found
      | x :: _ -> x

  let remove x s =
    let rec rm s =
      match s with
	| [] -> []
	| y :: s' -> 
	    let p = Rd.compare x y in
	    if (* x > y *) p > 0 then y :: rm s' else 
	      if p = 0 then s' else s
    in
    rm s

  let add x s =
    let rec ins s =
      match s with
	| [] -> [x]
	| y :: s' ->
	    let p = Rd.compare x y in
	    if (* x > y *) p > 0 then y :: ins s' else
	      if p = 0 then x :: s' else x :: s
    in
    ins s
end
 *)

module RdSet = struct
  type t =
    | Empty
    | S1 of shuffle_in
    | S2 of shuffle_in * shuffle_in
    | S3 of shuffle_in * shuffle_in * shuffle_in
    | S4 of shuffle_in * shuffle_in * shuffle_in * shuffle_in
    | Sn of RdSet0.t

  let empty = Empty

  let min_elt =
    function
      | Empty -> raise Not_found
      | S1 x -> x
      | S2(x,_) -> x
      | S3(x,_,_) -> x
      | S4(x,_,_,_) -> x
      | Sn s -> RdSet0.min_elt s

  let remove x s =
    match s with
      | Empty -> Empty
      | S1 y0 -> if Rd.compare x y0 = 0 then Empty else s
      | S2(y0,y1) -> 
	  let p0 = Rd.compare x y0 in
	  if p0 = 0 then S1 y1 
	  else if p0 > 0 && Rd.compare x y1 = 0 then S1 y0 
	  else s
      | S3(y0,y1,y2) ->
	  let p1 = Rd.compare x y1 in
	  if p1 = 0 then S2(y0, y2) else 
	    if p1 < 0 then (
	      if Rd.compare x y0 = 0 then S2(y1,y2) else s
	    )
	    else (
	      if Rd.compare x y2 = 0 then S2(y0,y1) else s
	    )
      | S4(y0,y1,y2,y3) ->
	  let p1 = Rd.compare x y1 in
	  if p1 = 0 then S3(y0, y2, y3) else 
	    if p1 < 0 then (
	      if Rd.compare x y0 = 0 then S3(y1,y2,y3) else s
	    )
	    else (
	      let p2 = Rd.compare x y2 in
	      if p2 = 0 then S3(y0,y1,y3) else
		if p2 < 0 then s else 
		  if Rd.compare x y3 = 0 then
		    S3(y0,y1,y2)
		  else s
	    )
      | Sn u -> 
	  let u' = RdSet0.remove x u in
	  (* we do not downgrade to the tuple representation. But we need
	     to check for emptiness (for tests against empty)
	   *)
	  if u' = RdSet0.empty then
	    Empty
	  else
	    Sn u'

  let upgrade l =
    Sn (List.fold_left (fun acc x -> RdSet0.add x acc) RdSet0.empty l)

  let add x s =
    match s with
      | Empty -> S1 x
      | S1 y0 -> 
	  let p0 = Rd.compare x y0 in
	  if p0 = 0 then S1 x else 
	    if p0 < 0 then S2(x,y0) else S2(y0,x)
      | S2(y0,y1) -> 
	  let p0 = Rd.compare x y0 in
	  if p0 = 0 then S2(x, y1) else
	    if p0 < 0 then S3(x,y0,y1) else (
	      let p1 = Rd.compare x y1 in
	      if p1 = 0 then S2(y0,x) else
		if p1 < 0 then S3(y0,x,y1) else S3(y0,y1,x)
	    )
      | S3(y0,y1,y2) ->
	  let p1 = Rd.compare x y1 in
	  if p1 = 0 then S3(y0, x, y2) else
	    if p1 < 0 then (
	      let p0 = Rd.compare x y0 in
	      if p0 = 0 then S3(x,y1,y2) else
		if p0 < 0 then S4(x,y0,y1,y2) else S4(y0,x,y1,y2)
	    )
	    else (
	      let p2 = Rd.compare x y2 in
	      if p2 = 0 then S3(y0,y1,x) else
		if p2 < 0 then S4(y0,y1,x,y2) else S4(y0,y1,y2,x)
	    )
      | S4(y0,y1,y2,y3) ->
	  let up() = upgrade [x; y0; y1; y2; y3] in
	  let p1 = Rd.compare x y1 in
	  if p1 = 0 then S4(y0,x,y2,y3) else
	    if p1 < 0 then (
	      let p0 = Rd.compare x y0 in
	      if p0 = 0 then S4(x,y1,y2,y3) else
		up()
	    )
	    else (
	      let p2 = Rd.compare x y2 in
	      if p2 = 0 then S4(y0,y1,x,y3) else
		if p2 < 0 then
		  up()
		else
		  if Rd.compare x y3 = 0 then
		    S4(y0,y1,y2,x)
		  else 
		    up()
	    )
      | Sn u -> 
	  let u' = RdSet0.add x u in
	  Sn u'
end


let merging_reader input_array extract hash cmp : Mapred_io.record_reader =
  let input_q =
    Array.map
      (fun rd ->
	 let q = Queue.create() in
	 (try rd # input_records q with End_of_file -> ());
	 q
      )
      input_array in
  let peek intr k = (* peek a record from file k *)
    let q = input_q.(k) in
    if Queue.is_empty q then (
      intr := true;
      let rd = input_array.(k) in
      try rd # input_records q with End_of_file -> ()
    );
    Queue.peek q in
  let inprec k = (* input a record from file k *)
    let q = input_q.(k) in
    if Queue.is_empty q then (
      let rd = input_array.(k) in
      try rd # input_records q with End_of_file -> ()
    );
    Queue.take q in

  let in_cmp x y =
    cmp 
      x.in_rec x.in_key_start x.in_key_len
      y.in_rec y.in_key_start y.in_key_len in

  let input_set = ref RdSet.empty in
  let () =
    let dummy = ref false in
    for k = 0 to Array.length input_array - 1 do
      try
	let line = peek dummy k in  (* or Queue.Empty *)
	let (key_start,key_len) = extract line in
	let si =
	  { in_index = k;
	    in_rec = line;
	    in_key_start = key_start;
	    in_key_len = key_len;
	    in_hash = hash line key_start key_len;
	    in_cmp = in_cmp;
	  } in
	input_set := RdSet.add si !input_set
      with
	| Queue.Empty -> ()
    done in

  let pos_in = ref 0 in
  let current = Queue.create() in

  let advance() =
    assert(Queue.is_empty current);
    if !input_set <> RdSet.empty then (
      let si = RdSet.min_elt !input_set in
      let si_line = si.in_rec in
      let si_key_start = si.in_key_start in
      let si_key_len = si.in_key_len in
      let k = si.in_index in
      let line = inprec k in
      (* This inprec cannot raise Queue.Empty because we have already
	 peeked the line (this is an invariant for all [si] in [input_set])
       *)
      Queue.add line current;
      ( try
	  let intr = ref false in
	  while not !intr do
	    let next_line = peek intr k in (* or Queue.Empty *)
	    let (next_key_start,next_key_len) = extract next_line in
	    let d =
	      cmp
		si_line si_key_start si_key_len
		next_line next_key_start next_key_len in
	    if d = 0 then (
	      if not !intr then (
		ignore(inprec k);
		Queue.add next_line current;
	      )
	    )
	    else (
	      input_set := RdSet.remove si !input_set;
	      let si' =
		{ si with 
		    in_rec = next_line;
		    in_key_start = next_key_start;
		    in_key_len = next_key_len;
		    in_hash = hash next_line next_key_start next_key_len
		} in
	      input_set := RdSet.add si' !input_set;
	      intr := true
	    )
	  done
	with
	  | Queue.Empty ->
	      input_set := RdSet.remove si !input_set
      )
    ) in

  let () = 
    advance() in

  ( object(self)
      method pos_in = !pos_in
      method input_record() =
 	if Queue.is_empty current then advance();
	try
	  let line = Queue.take current in
	  incr pos_in;
	  line
	with
	  | Queue.Empty -> raise End_of_file

      method input_records q =
	if Queue.is_empty current then advance();
	if Queue.is_empty current then raise End_of_file;
	pos_in := !pos_in + Queue.length current;
	Queue.transfer current q

      method peek_record() =
 	if Queue.is_empty current then advance();
	try
	  Queue.peek current
	with
	  | Queue.Empty -> raise End_of_file
	      
      method close_in() =
	Array.iter (fun rd -> rd # close_in()) input_array

      method abort() =
	Array.iter (fun rd -> rd # abort()) input_array

      method to_any_e dest esys =
	assert false
	
      method to_fd_e fd esys = 
	let io_dev =
	  `Polldescr(Netsys.get_fd_style fd,fd,esys) in
	self # to_dev_e io_dev esys

      method to_dev_e io_dev esys = 
	let input_many_records () =
	  let l = ref [] in
	  let n = ref 0 in
	  ( try
	      while !n < 1_000_000 do
		let line = self#input_record() in
		l := line :: !l;
		n := !n + String.length line + 1;
	      done;
	    with End_of_file -> ()
	  );
	  !n, String.concat "\n" (List.rev !l) in

	dlogf
	  "Mapred_task_exec.exec_shuffle_task: reader to_fd_e";
	let stream =
	  Stream.from
	    (fun _ ->
	       let n, lines = input_many_records() in
	       dlogf
		 "Mapred_task_exec.exec_shuffle_task: reader to_fd_e n=%d" n;
	       (* Unixqueue.Debug.enable := true; *)
	       if n > 0 then (
		 let e() = 
		   Uq_io.output_string_e io_dev lines
		   ++ (fun () -> Uq_io.output_string_e io_dev "\n") in
		 Some e
	       )
	       else (
		 dlogf
		   "Mapred_task_exec.exec_shuffle_task: reader to_fd_e EOF";
		 None
	       )
	    ) in
	Uq_engines.stream_seq_engine () stream esys

      method filesystem = input_array.(0) # filesystem
      method record_config = input_array.(0) # record_config
      method stats =
	Mapred_stats.addl
	  (Array.to_list
	     (Array.map (fun rd -> rd#stats) input_array))
    end
  )


let splitting_writer output_array pmin pmax extract part_key fs rc
                     : Mapred_io.record_writer =
object(self)

  method output_record line =
    let (key_start,key_len) = extract line in
    let p = part_key line key_start key_len in
              
    if p < pmin || p > pmax then
      failwith 
        (sprintf "Shuffle: Partition outside allowed range \
                  (key=%s p=%d)" (String.sub line key_start key_len) p
        );
              
    ( match output_array.(p - pmin) with
        | None ->
            failwith "Shuffle: output partition missing"
        | Some writer ->
            writer # output_record line
    )

  method output_records q =
    while not(Queue.is_empty q) do
      let line = Queue.take q in
      self # output_record line
    done

  method flush() =
    Array.iter
      (function
	 | None -> ()
	 | Some writer -> writer#flush()
      )
      output_array

  method close_out () =
    Array.iter
      (function
	 | None -> ()
	 | Some writer -> writer#close_out()
      )
      output_array

  method abort() =
    Array.iter
      (function
	 | None -> ()
	 | Some writer -> writer#abort()
      )
      output_array

  method filesystem = fs

  method record_config = rc

  method stats =
    Array.fold_left
      (fun acc wropt ->
	 match wropt with
	   | None -> acc
	   | Some wr -> Mapred_stats.add acc wr#stats
      )
      Mapred_stats.empty
      output_array

  method from_fd_e _ _ =
    assert false

  method from_dev_e _ _ _ _ _ =
    assert false
end


let exec_shuffle_task me jc mj (t:shuffle_task) ti =
  let cleanup = Stack.create() in
  let stats = ref Mapred_stats.empty in
  let fs = me # filesystem in
  let wfm = wf_manager fs in
  try
    let sorter = mj # sorter me jc 0.0 in
    let rc = record_config me jc in

    let (pmin,pmax) = t.shuffle_partitions in
    if pmin > pmax then failwith "Shuffle: empty set of output partitions";

    (* Maybe we can shuffle with [link]: *)
    let link_possible =
       (match t.shuffle_input with         (* exactly one input *)
	  | [ [_, 0L, (*0L*) _], _, _ ] -> true
	      (* FIXME: not checking the end block is wrong. However, we
		 never get a 0 here
	       *)
	  | _ -> false
       ) && 
	 ( match t.shuffle_output with     (* exactly one output *)
	     | [ _, _, _, _ ] -> true
	     | _ -> false
	 ) &&
	 not t.shuffle_reduce in          (* and no reduce! *)
    
    let output_files =
      if link_possible then (
	let in_file, _, _ = List.hd t.shuffle_input in
	let in_name, _, _ = List.hd in_file in
	let out_name, _, _, _ = List.hd t.shuffle_output in
	fs # link [] in_name out_name;
	[ `Tag "out", [ out_name, 0L, 0L ] ]
      )
      else (
	let inputs =
	  let divisor = List.length t.shuffle_input in
	  let rd_factory = mj#internal_record_io me jc in
	  List.map
	    (fun (file,_,_) ->
	       let reader = 
		 fragment_reader ~divisor rd_factory me rc file in
	       Stack.push (fun () -> reader # close_in()) cleanup;
	       reader
	    )
	    t.shuffle_input in
	let input_array = Array.of_list inputs in
	
	let extract =
	  mj # extract_key me jc in
	let part_key =
	  mj # partition_of_key me jc in
	
	let input_files = Hashtbl.create 5 in
	List.iter2
	  (fun (file,_,_) rd ->
	     let s =
	       String.concat "+"
		 (List.map 
		    (fun (name,pos,len) -> sprintf "%s@%Ld/%Ld" name pos len)
		    file) in
	     Hashtbl.add input_files rd ("[" ^ s ^ "]")
	  )
	  t.shuffle_input
	  inputs;
	
	let output_part = 
	  Array.make (pmax+1-pmin) None in

	let wr_factory =
	  if t.shuffle_reduce then
	    mj # output_record_io me jc
	  else
	    mj # internal_record_io me jc in
	
	let outputs =
	  let divisor = List.length t.shuffle_output in
	  let rc1 = Mapred_io.divide rc divisor in
	  List.map
	    (fun (filename,fpmin,fpmax,loc) ->
	       if fpmin < pmin || fpmin > fpmax || fpmax > pmax then
		 failwith "Shuffle: bad output partitions";
	       let repl = if t.shuffle_reduce then 0 else 1 in
	       let pref_nodes = get_pref_nodes loc in
	       let wf_name =
		 create_wf ~repl ~pref_nodes wfm filename in
	       let writer = 
		 wr_factory # write_file fs me#shm_manager rc1 wf_name in
	       Stack.push (fun () -> writer # close_out()) cleanup;
	       for p = fpmin to fpmax do
		 output_part.(p - pmin) <- Some writer
	       done;
	       writer
    	    )
	    t.shuffle_output in
	
	let output_files =
	  List.map
	    (fun (filename,fpmin,fpmax,_) -> (`Tag "out", [filename, 0L, 0L]))
	    t.shuffle_output in
	
	let reader =
	  if Array.length input_array > 1 then
	    merging_reader input_array extract sorter#hash sorter#cmp
	  else
	    input_array.(0) in
	
	if t.shuffle_reduce then (
	  (** this is a merge+reduce pass *)
	  (* assert(pmin = pmax); - not true anymore *)
	  assert(List.length outputs=1);
	  let writer = List.hd outputs in
	  mj # reduce me jc ti reader writer;
	)
	else (
	  (** this is a merge+split pass *)
	  let writer =
	    splitting_writer 
	      output_part pmin pmax extract part_key fs rc in
	  let combiner =
	    match mj # combine me jc ti with
	      | None ->
		  (fun rd wr ->
		     let input_record = rd # input_record in
		     let output_record = wr # output_record in
		     try
		       while true do
			 let line = input_record() in
			 output_record line
		       done
		     with End_of_file -> ()
		  )
	      | Some combiner -> combiner in
	  combiner reader writer
	);
	
	List.iter (fun reader -> reader # close_in()) inputs;
	List.iter (fun writer -> writer # close_out()) outputs;
	stats := Mapred_stats.addl ( [ !stats; reader#stats ] @
				       List.map (fun w -> w#stats) outputs );
	output_files
      ) in

    run_stack (fun f -> f()) cleanup;
    commit_wf wfm (`Shuffle t);
    
    if t.shuffle_input_del then (
      List.iter
	(fun (file,_,_) -> delete_file fs file)
	t.shuffle_input
    );

    let ok =
      { ok_files = output_files;
	ok_counters = !stats
      } in
    `Ok ok
  with
    | Plasma_util.Out_of_shared_memory 
    | Plasma_shm.Shm_max_exceeded
    | Plasma_shm.Buf_max_exceeded ->
	Netlog.logf `Warning "Out_of_shared_memory";
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	handle_out_of_memory fs 
    | error ->
	let bt = Printexc.get_backtrace() in
	let msg =
	  sprintf "Exception %s%s"
	    (Netexn.to_string error)
	    (if Printexc.backtrace_status() then
	       " - backtrace: " ^ bt
	     else ""
	    ) in
	run_stack (fun f -> try f() with _ -> ()) cleanup;
	remove_wf wfm;
	`Error msg


(**********************************************************************)


let execute (me:Mapred_def.mapred_env) 
            (jc:Mapred_def.mapred_job_config)
            (mj:Mapred_def.mapred_job) t ti
    : task_result =
  Netlog.logf `Notice "Start task %s"
    (Mapred_tasks.string_of_task_id t);
  let r =
    match t with
      | `Map t' -> 
	  exec_map_task me jc mj t' ti
      | `Emap t' -> 
	  exec_emap_task me jc mj t' ti
      | `Sort t' ->
	  exec_sort_task me jc mj t'
      | `Shuffle t' ->
	  exec_shuffle_task me jc mj t' ti
      | `Cleanup ->
	  (* no cleanup here. This is done in mapred_task_server *)
	  `Ok
	    { ok_files = [];
	      ok_counters = Mapred_stats.empty
	    }
  in
  Netlog.logf `Notice "End task %s"
    (Mapred_tasks.string_of_task_id t);
  r
