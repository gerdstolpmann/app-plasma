(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(***********************************************************************)
(*                                                                     *)
(*                           Objective Caml                            *)
(*                                                                     *)
(*            Xavier Leroy, projet Cristal, INRIA Rocquencourt         *)
(*                                                                     *)
(*  Copyright 1996 Institut National de Recherche en Informatique et   *)
(*  en Automatique.  All rights reserved.  This file is distributed    *)
(*  under the terms of the GNU Library General Public License, with    *)
(*  the special exception on linking described in file ../LICENSE      *)
(*  of the Objective Caml distribution.                                *)
(*                                                                     *)
(***********************************************************************)

(* A slightly optimized version of Array.stable_sort. This is
   copyright INRIA (see copyright notice in array.ml)
 *)

let cutoff = 10;;
let merge_sort cmp (a : int array) =
  let merge src1ofs src1len src2 src2ofs src2len dst dstofs =
    let src1r = src1ofs + src1len and src2r = src2ofs + src2len in
    let rec loop i1 s1 i2 s2 d =
      if cmp s1 s2 <= 0 then begin
        Array.unsafe_set dst d s1;
        let i1 = i1 + 1 in
        if i1 < src1r then
          loop i1 (Array.unsafe_get a i1) i2 s2 (d + 1)
        else
          Array.blit src2 i2 dst (d + 1) (src2r - i2)
      end else begin
        Array.unsafe_set dst d s2;
        let i2 = i2 + 1 in
        if i2 < src2r then
          loop i1 s1 i2 (Array.unsafe_get src2 i2) (d + 1)
        else
          Array.blit a i1 dst (d + 1) (src1r - i1)
      end
    in 
    loop 
      src1ofs (Array.unsafe_get a src1ofs) 
      src2ofs (Array.unsafe_get src2 src2ofs) 
      dstofs;
  in
  let isortto srcofs dst dstofs len =
    for i = 0 to len - 1 do
      let e = (Array.unsafe_get a (srcofs + i)) in
      let j = ref (dstofs + i - 1) in
      while (!j >= dstofs && cmp (Array.unsafe_get dst !j) e > 0) do
        Array.unsafe_set dst (!j + 1) (Array.unsafe_get dst !j);
        decr j;
      done;
      Array.unsafe_set dst (!j + 1) e;
    done;
  in
  let rec sortto srcofs dst dstofs len =
    if len <= cutoff then isortto srcofs dst dstofs len else begin
      let l1 = len / 2 in
      let l2 = len - l1 in
      sortto (srcofs + l1) dst (dstofs + l1) l2;
      sortto srcofs a (srcofs + l2) l1;
      merge (srcofs + l2) l1 dst (dstofs + l1) l2 dst dstofs;
    end;
  in
  let l = Array.length a in
  if l <= cutoff then isortto 0 a 0 l else begin
    let l1 = l / 2 in
    let l2 = l - l1 in
    let t = Array.make l2 (Array.unsafe_get a 0) in
    sortto l1 t 0 l2;
    sortto 0 a l2 l1;
    merge l2 l1 t 0 l2 a 0;
  end;
;;


let cutoff = 5;;  (* fewer cmp callbacks for 10 *)
let ba_merge_sort
      cmp (a : (int,Bigarray.int_elt,Bigarray.c_layout) Bigarray.Array1.t)=
  let open Bigarray.Array1 in

  let qblit src srcoffs dst dstoffs len =
    if len <= 2 then (
      if len = 1 then
	unsafe_set dst dstoffs (unsafe_get src srcoffs)
      else
	if len = 2 then (
	  let x0 = unsafe_get src srcoffs in
	  let x1 = unsafe_get src (srcoffs+1) in
	  unsafe_set dst dstoffs x0;
	  unsafe_set dst (dstoffs+1) x1;
	)
    )
    else
      blit (sub src srcoffs len) (sub dst dstoffs len)
  in

  let merge src1ofs src1len src2 src2ofs src2len dst dstofs =
    let src1r = src1ofs + src1len and src2r = src2ofs + src2len in
    let rec loop i1 s1 i2 s2 d =
      if cmp s1 s2 <= 0 then begin
        unsafe_set dst d s1;
        let i1 = i1 + 1 in
        if i1 < src1r then
          loop i1 (unsafe_get a i1) i2 s2 (d + 1)
        else
	  let l = src2r - i2 in
	  qblit src2 i2 dst (d+1) l
      end else begin
        unsafe_set dst d s2;
        let i2 = i2 + 1 in
        if i2 < src2r then
          loop i1 s1 i2 (unsafe_get src2 i2) (d + 1)
        else
	  let l = src1r - i1 in
	  qblit a i1 dst (d+1) l
      end
    in 
    loop 
      src1ofs (unsafe_get a src1ofs) 
      src2ofs (unsafe_get src2 src2ofs) 
      dstofs;
  in
  let isortto srcofs dst dstofs len =
    (* In the outer loop we iterate over the elements of dst. In each
       iteration we search the next smallest element. The inner loops
       start with k_start and actually search this element. In [flags]
       we remember which elements have already been inserted to dst.
       These elements are skipped. [flags] is a bitmask (which is
       possible because len <= cutoff).
     *)
    let flags = ref 0 in
    let k_start = ref srcofs in
    let m_start = ref 1 in
    let m = ref 0 in
    let p = ref 0 in
    let s = ref 0 in
    let x = ref 0 in
    let pmax = srcofs + len in
    for i = 0 to len - 1 do
      (* Search the right element for dst.(dstofs+i) *)
(*
      assert(!k_start < pmax);
      assert(!flags land !m_start = 0);
 *)
      m := !m_start;
      p := !k_start;
      s := !m_start;
      while !p < pmax do
	m := !s;
	x := unsafe_get a !p;
	incr p;
	s := !s lsl 1;
	while 
	  !p < pmax && (!flags land !s <> 0 ||
			 cmp (unsafe_get a !p) !x >= 0)
	do
	  incr p;
	  s := !s lsl 1;
	done;
      done;
      unsafe_set dst (dstofs+i) !x;
      flags := !flags lor !m;
      if !m = !m_start then (
	while !flags land !m_start <> 0 do
	  incr k_start;
	  m_start := !m_start lsl 1
	done
      )
    done
  in
(*
  let isortto srcofs dst dstofs len =
    for i = 0 to len - 1 do
      let e = (unsafe_get a (srcofs + i)) in
      let j = ref (dstofs + i - 1) in
      while (!j >= dstofs && cmp (unsafe_get dst !j) e > 0) do
        unsafe_set dst (!j + 1) (unsafe_get dst !j);
        decr j;
      done;
      unsafe_set dst (!j + 1) e;
    done;
  in
 *)
  let rec sortto srcofs dst dstofs len =
    if len <= cutoff then isortto srcofs dst dstofs len else begin
      let l1 = len / 2 in
      let l2 = len - l1 in
      sortto (srcofs + l1) dst (dstofs + l1) l2;
      sortto srcofs a (srcofs + l2) l1;
      merge (srcofs + l2) l1 dst (dstofs + l1) l2 dst dstofs;
    end;
  in
  let l = dim a in
  if l <= cutoff then isortto 0 a 0 l else begin
    let l1 = l / 2 in
    let l2 = l - l1 in
    let t = create Bigarray.int Bigarray.c_layout l2 in
    sortto l1 t 0 l2;
    sortto 0 a l2 l1;
    merge l2 l1 t 0 l2 a 0;
  end;
;;
