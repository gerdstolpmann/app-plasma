# Portable script to determine amount of phys RAM
# Output in number of bytes! Float output is OK.

case `uname` in
    Linux)
	grep MemTotal /proc/meminfo | tail -1 | awk '{print $2*1024}' ;;
    SunOS)
	/usr/sbin/prtconf | grep 'Memory size' | awk '{print $3*1024*1024}' ;;
    AIX)
	lsattr -E -l mem0 | tail -1 | awk '{print $2*1024*1024}' ;;
    FreeBSD)
	sysctl -n hw.realmem ;;
    NetBSD)
	sysctl -n hw.physmem64 ;;
    OpenBSD)
	sysctl -n hw.physmem ;;
    Darwin)
	sysctl -n hw.memsize ;;

    # Also looked for a HP-UX solution, but seems to be difficult to
    # find out as non-root user
esac
