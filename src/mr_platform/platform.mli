(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Platform-specific functions *)

external find_lf : Netsys_mem.memory -> int -> int -> int 
  = "mapred_find_lf"
(** Looks for the first LF char from [mem[pos]] to [mem[endpos-1]] and returns
    the position. [Not_found] if not found.
 *)

external rev_find_lf : Netsys_mem.memory -> int -> int -> int 
  = "mapred_rev_find_lf"
(** Looks for the last LF char from [mem[pos]] to [mem[endpos-1]] and returns 
    the position. [Not_found] if not found.
 *)


external str_cmp_unsafe : string -> int -> int -> string -> int -> int -> int
  = "mapred_str_cmp_byte" "mapred_str_cmp"
(** See [str_cmp] *)

val str_cmp : string -> int -> int -> string -> int -> int -> int
(** [str_cmp s1 p1 l1 s2 p2 l2]: The same as
    [String.compare (String.sub s1 p1 l1) (String.sub s2 p2 l2)], only
    faster
 *)

module Line_splitter : sig
  type engine
    (** The engine runs in a separate thread (C level, so this thread can
	run in parallel with Ocaml code)
     *)

  val create : Netsys_mem.memory -> engine
    (** Creates the line splitter thread *)

  val next_lf : engine -> int
    (** Looks for the next LF char. [Not_found] if not found.  
     *)

  val stop : engine -> unit
    (** Stops the thread. *)
end


val physical_ram : unit -> int64 option
  (** Tries to get the amount of physical RAM, in bytes. This is not
      available on all OS (but on a lot).
   *)

val shm_limit : unit -> int64 option
  (** Returns a limit on the amount of POSIX shm.
      Right now, this is just 1/8 of physical RAM
   *)

val number_cores : unit -> int option
  (** Returns the number of cores (or virtual cores) on the system.
   *)
