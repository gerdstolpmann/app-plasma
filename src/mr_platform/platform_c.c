/* $Id$ */

/* Helpers for mapred performance */

#define _GNU_SOURCE

#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <caml/fail.h>
#include <caml/bigarray.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/custom.h>
#include <caml/unixsupport.h>

#define Nothing ((value) 0)

/* external find_lf :  Netsys_mem.memory -> int -> int -> int
  = "mapred_find_lf"

   Looks for LF char from mem[pos] to mem[endpos-1] and returns the
   position. Not_found if not found.
 */

CAMLprim value mapred_find_lf (value memv, value posv, value endposv) {
    struct caml_bigarray *mem = Bigarray_val(memv);
    long pos = Long_val(posv);
    long endpos = Long_val(endposv);
    char *m = (char *) mem->data;
    char *found;

    found = (char *) memchr(m + pos, '\n', endpos-pos);
    if (found == NULL) caml_raise_not_found();

    return (Val_long (found - m));
}


CAMLprim value mapred_str_cmp(value s1, value p1, value l1,
			      value s2, value p2, value l2)
{
    int d;
    size_t n1, n2, n;

    n1 = Long_val(l1);
    n2 = Long_val(l2);
    n = (n1 < n2) ? n1 : n2;

    d = memcmp(String_val(s1) + Long_val(p1),
	       String_val(s2) + Long_val(p2),
	       n);

    if (d == 0)  d = n1 - n2;

    return (Val_int(d));
}


CAMLprim value mapred_str_cmp_byte(value *argv, int argn)
{
    return mapred_str_cmp(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
}


#ifdef __linux__
#define HAVE_MEMRCHR
#endif

#ifdef __FreeBSD__
#define HAVE_MEMRCHR
#endif

#ifdef __OpenBSD__
#define HAVE_MEMRCHR
#endif

CAMLprim value mapred_rev_find_lf (value memv, value posv, value endposv) {
    struct caml_bigarray *mem = Bigarray_val(memv);
    long pos = Long_val(posv);
    long endpos = Long_val(endposv);
    char *m = (char *) mem->data;
    char *found;

#ifdef HAVE_MEMRCHR
    found = (char *) memrchr(m + pos, '\n', endpos-pos);
#else
    found = NULL;
    while (endpos > pos && m[endpos-1] != '\n') endpos--;
    if (endpos > pos) found = m+endpos;
#endif
    if (found == NULL) caml_raise_not_found();

    return (Val_long (found - m));
}



#define N_MAX 800
#define N_MAX_MIN 400

struct split_engine {
    pthread_mutex_t  m;
    pthread_cond_t   c;
    int              enabled;
    int              running;
    char            *start;
    char            *cursor;
    char            *end;
    char           **ringbuf;
    int              ringbuf_head;
    int              ringbuf_len;
};


/* must be called with lock */
static void body_split_pass(struct split_engine *se)
{
    char *found;
    int p;

    char *cursor;
    char *end;
    char **ringbuf;
    int   ringbuf_head;
    int   ringbuf_len;

    cursor = se->cursor;
    end = se->end;
    ringbuf = se->ringbuf;
    ringbuf_head = se->ringbuf_head;
    ringbuf_len = se->ringbuf_len;

    p = ringbuf_head + ringbuf_len;
    while (p >= N_MAX) p -= N_MAX;

    while (cursor < end && ringbuf_len < N_MAX) {
	found = (char *) memchr(cursor, '\n', end - cursor);
	if (found == NULL)
	    cursor = end;
	else {
	    cursor = found+1;
	    ringbuf[p] = found;
	    ringbuf_len++;
	    p++;
	    if (p == N_MAX) p=0;
	}
    }

    se->cursor = cursor;
    se->ringbuf_len = ringbuf_len;
}


static void *body_split_engine(void *arg)
{
    struct split_engine *se;

    se = (struct split_engine *) arg;
    pthread_mutex_lock(&(se->m));
    while (se->enabled && se->cursor < se->end) {
	/* fprintf(stderr, "Loop 1\n"); */
	while (se->enabled &&
	       se->cursor < se->end &&
	       se->ringbuf_len >= N_MAX_MIN
	       ) {
	    /* fprintf(stderr, "Loop 2\n"); */
	    pthread_cond_wait(&(se->c), &(se->m));
	};
	/* still locked! */
	if (se->enabled)
	    body_split_pass(se);
	if (se->ringbuf_len > 0) {
	    /* fprintf(stderr, "Signal\n"); */
	    pthread_cond_signal(&(se->c));
	}
    }
    se->running = 0;
    pthread_cond_signal(&(se->c));
    pthread_mutex_unlock(&(se->m));
    pthread_exit(NULL);
    return NULL;
}


static void init_split_engine(struct split_engine *se,
			      value ba)
{
    pthread_t thr;
    int code;

    se->enabled = 1;
    se->running = 1;
    se->cursor = (char *) Data_bigarray_val(ba);
    se->start = se->cursor;
    se->end = se->cursor + Bigarray_val(ba)->dim[0];
    code = pthread_mutex_init(&(se->m), NULL);  /* code is the error code */
    if (code != 0) unix_error(code, "pthread_mutex_init", Nothing);
    code = pthread_cond_init(&(se->c), NULL);   /* code is the error code */
    if (code != 0) unix_error(code, "pthread_cond_init", Nothing);
    se->ringbuf = (char **) stat_alloc(N_MAX * sizeof(char *));
    se->ringbuf_head = 0;
    se->ringbuf_len = 0;
    /* Do the first pass immediately, so we don't have to synchronize with
       the thread for the first chunk of data
    */
    body_split_pass(se);
    code = pthread_create(&thr, NULL, &body_split_engine, se);
    if (code != 0) unix_error(code, "pthread_create", Nothing);
}


static struct custom_operations splitter_ops = {
    "",
    custom_finalize_default,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
};


#define Engine_val(v) ((struct split_engine **) (Data_custom_val(v)))


CAMLprim value mapred_splitter_create(value ba)
{
    struct split_engine *se;
    value r;

    se = (struct split_engine *) stat_alloc(sizeof(struct split_engine));
    init_split_engine(se, ba);
    r = caml_alloc_custom(&splitter_ops, sizeof(void *), 0, 0);
    *(Engine_val(r)) = se;
    return r;
}


CAMLprim value mapred_splitter_stop(value eng)
{
    struct split_engine *se;

    se = *(Engine_val(eng));
    if (se != NULL) {
	/* Signal the thread to stop */
	pthread_mutex_lock(&(se->m));
	se->enabled = 0;
	pthread_mutex_unlock(&(se->m));
	pthread_cond_signal(&(se->c));
	/* Wait until the thread really stops */
	pthread_mutex_lock(&(se->m));
	while (se->running)
	    pthread_cond_wait(&(se->c), &(se->m));
	pthread_mutex_unlock(&(se->m));
	/* Free resources. */
	pthread_cond_destroy(&(se->c));
	pthread_mutex_destroy(&(se->m));
	stat_free(se->ringbuf);
    stat_free(se);
	*(Engine_val(eng)) = NULL;
    };
    return Val_unit;
}


CAMLprim value mapred_splitter_next(value eng, value camlbuf)
{
    struct split_engine *se;
    int p, k, n, n1;

    char **ringbuf;
    char  *start;

    se = *(Engine_val(eng));
    ringbuf = se->ringbuf;
    start = se->start;

    /* Wait until there are elements in the ring buffer, or we have EOF */
    pthread_mutex_lock(&(se->m));
    while (se->ringbuf_len == 0 && se->cursor < se->end && se->running)
	pthread_cond_wait(&(se->c), &(se->m));

    /* Copy now as many as possible elements to camlbuf */
    n = Wosize_val(camlbuf);
    if (se->ringbuf_len < n) n=se->ringbuf_len;
    p = se->ringbuf_head;
    n1 = N_MAX - p;
    if (n < n1) n1=n;
    for (k=0; k<n1; k++) {
	Field(camlbuf, k) = Val_long(ringbuf[p] - start);
	p++;
    }
    if (p == N_MAX) p = 0;
    for (k=n1; k<n; k++) {
	Field(camlbuf, k) = Val_long(ringbuf[p] - start);
	p++;
    }
    se->ringbuf_head = p;
    se->ringbuf_len = se->ringbuf_len - n;
    /* fprintf(stderr, "n=%d len=%d\n", n, se->ringbuf_len);*/
    /* If this is now below the threshold, signal the thread to collect more */
    if (se->ringbuf_len < N_MAX_MIN && se->cursor < se->end) {
	/* fprintf(stderr, "Signal\n");*/
	pthread_cond_signal(&(se->c));
    }
    pthread_mutex_unlock(&(se->m));
    return Val_int(n);
}
