(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

external find_lf : Netsys_mem.memory -> int -> int -> int 
  = "mapred_find_lf"

external rev_find_lf : Netsys_mem.memory -> int -> int -> int 
  = "mapred_rev_find_lf"

external str_cmp_unsafe : string -> int -> int -> string -> int -> int -> int
  = "mapred_str_cmp_byte" "mapred_str_cmp"

let str_cmp s1 p1 l1 s2 p2 l2 =
  if (p1 < 0 || l1 < 0 || p1 > String.length s1 - l1 ||
	p2 < 0 || l2 < 0 || p2 > String.length s2 - l2)
  then invalid_arg "Platform.str_cmp";
  str_cmp_unsafe s1 p1 l1 s2 p2 l2


module Line_splitter = struct
  type c_engine
  type engine =
      { ls : c_engine;
	buf : int array;
	mutable cur : int;
	mutable bufend : int;
      }

  external splitter_create : Netsys_mem.memory -> c_engine 
    = "mapred_splitter_create"

  external splitter_stop : c_engine -> unit
    = "mapred_splitter_stop"

  external splitter_next : c_engine -> int array -> int
    = "mapred_splitter_next"

  let create mem =
    let ls = splitter_create mem in
    Gc.finalise splitter_stop ls;
    { ls;
      buf = Array.make 800 0;
      cur = 0;
      bufend = 0;
    }

  let stop ls =
    splitter_stop ls.ls

  let refill ls =
    let n = splitter_next ls.ls ls.buf in
    ls.bufend <- n;
    ls.cur <- 0;
    if n = 0 then raise Not_found

  let next_lf ls =
    if ls.cur = ls.bufend then
      refill ls;
    let k = ls.cur in
    ls.cur <- k+1;
    Array.unsafe_get ls.buf k

end


let get_number_from_script script =
  try
    let ch = Unix.open_process_in script in
    let ch_open = ref true in
    try
      let n = input_line ch in
      let st = Unix.close_process_in ch in
      ch_open := false;
      if st <> Unix.WEXITED 0 then raise Not_found;
      Some(Int64.of_float(float_of_string n))
    with
      | error ->
	  if !ch_open then
	    ignore(Unix.close_process_in ch);
	  None
  with
    | error ->
	None


let physical_ram_script = include_file "physical_ram.sh"

let physical_ram() =
  get_number_from_script physical_ram_script

let number_cores_script = include_file "number_cores.sh"

let number_cores() =
  match get_number_from_script number_cores_script with
    | None -> None
    | Some n -> Some(Int64.to_int n)

let shm_limit() =
  match physical_ram() with
    | None -> None
    | Some n -> Some(Int64.div n 8L)
