# Return the number of cores

case `uname` in
    Linux)
	grep processor /proc/cpuinfo |wc -l ;;
    SunOS)
        # This is the number of "virtual CPUs" 
        # = number of cores * number of threads per core
	uname -X | grep "NumCPU" | awk '{ print $3 }' ;;
    FreeBSD|NetBSD|OpenBSD|Darwin)
        sysctl -n hw.ncpu ;;
    AIX)
	LC_ALL=C bindprocessor -q | sed -e 's/The available processors are://' | wc -w ;;
    *)
	echo 1;;
esac
