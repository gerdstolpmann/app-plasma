(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Detailed analysis of block lists *)

(* NB this module is very close to the client module Plasma_blocks, but
   not exactly identical.
 *)

open Plasma_util

type block_cluster =
    { bc_index : int64;
      bc_length : int64;
      bc_identities : StrSet.t;
    }
  (** Describes blocks from an unspecified inode: The block range starting
      at [bc_index] with [bc_length >= 1] blocks is stored on the nodes
      in [bc_identities].
   *)

module Block_cluster :
  Mergable with type key = int64 and type t = block_cluster

module Block_report :
  RangeMapType with type point = int64 and type value = block_cluster

type block_report = Block_report.t


val create_report : Nn_blocklist.blocklist -> block_report
  (** Creates a report. Note that holes are not specially marked.
   *)

val under_replicated : block_report -> int -> block_report
  (** Returns the "sub report" of all underreplicated blocks, given a 
      certain replication factor
   *)

val zero_replicated : (int64 * int64) -> block_report -> bool
  (** [zero_replicated (min,max) rep]:
      Whether there are indexes without blocks in the given range *)

val sub : (int64 * int64) -> block_report -> block_report
  (** [sub (min,max) rep]: Returns a sub report for the given range *)

val iter : (block_cluster -> unit) -> block_report -> unit
  (** [iter f rep]: iterates over all clusters (elementary ranges)
      of the report
   *)

val remove_identity : string -> block_report -> block_report
  (** Remove this identity from the report *)
