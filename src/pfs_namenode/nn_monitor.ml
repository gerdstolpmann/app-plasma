(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Nn_datastores
open Plasma_util.Operators
open Printf

module IntSet = Plasma_util.IntSet

let dlogf = Plasma_util.dlogf

module Bool_var =
  Netplex_cenv.Make_var_type(struct type t = bool end)

module Node_config_var =
  Netplex_cenv.Make_var_type(struct type t = Nn_config.nn_node_config end)

type errors =
    (string, int) Hashtbl.t  (* maps identity to error count *)

module Errors_var =
  Netplex_cenv.Make_var_type(struct type t = errors end)

module Rpc_client_var =
  Netplex_cenv.Make_var_type(struct type t = Rpc_client.t end)




let dsname = "current_datastores"
let pubname = "current_is_publishing"
let cfgname = "current_node_config"
let errname = "current_errors"
let disclientname = "current_discovery_client"

let is_publishing() =
  try Bool_var.get pubname
  with _ -> false


let error_table() =
  Errors_var.get errname


let list() =
  Datastores_var.get dsname

let get idstr =
  List.find 
    (fun ds -> ds.identity = idstr)
    (list())

let get_by_id id =
  List.find 
    (fun ds -> ds.id = id)
    (list())

let set_list l =
  (** Put the value into the container variable [dsname]: *)
  Datastores_var.set dsname l;
  if is_publishing() then (
    (** If we are publishing, put the value also in the shared Netplex
       variable [shdsname], and notify the receivers
     *)
    Datastores_shvar.set shdsname l;
    (** The receiver of this message is {!Nn_datastore_news.receive_message} *)
    Netplex_cenv.send_message "Nn_*" shdsname [| "have_monitor_news" |];
    Netlog.logf `Info "Notifying news channel about status update (n=%d)"
      (List.length l);
  )

let add ds =
  let l = list() in
  let p_conflict =
    List.exists
      (fun ds1 -> ds1.id = ds.id || ds1.identity = ds.identity)
      l in
  if p_conflict then
    failwith "Nn_monitor.add: conflicting IDs";
  set_list (ds :: l)
  
let delete idstr =
  let l = list() in
  let l' =
    List.filter (fun ds -> ds.identity <> idstr) l in
  set_list l'

let delete_by_id id =
  let l = list() in
  let l' =
    List.filter (fun ds -> ds.id <> id) l in
  set_list l'

let update f idstr =
  (** Updates the list of datastores by applying [f] to the element [idstr] *)
  let l = list() in
  let found = ref false in
  let l' =
    List.map
      (fun ds -> 
	 if ds.identity = idstr then (
	   found := true;
	   f ds
	 )
	 else
	   ds
      )
      l in
  if !found then
    set_list l'
  else
    failwith ("Nn_monitor: identity not found: " ^ idstr)
  
let enable idstr =
  update
    (fun ds -> { ds with enabled = true })
    idstr

let disable idstr =
  update
    (fun ds -> { ds with enabled = false })
    idstr

let update_from_db ?(when_done = fun _ -> ()) id =
  (** Start a new db transaction, and read the table of datastores. Look for
      the row for [id], and update the in-memory version of this row. We have
      to be careful to always finish the transaction (rolling it back on
      error). Call [when_done] when the in-memory version is changed.
   *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let t = Nn_db.transact esys 0 in  (* FIXME: commit_id *)
  let e = Nn_db.datastore_list_e t#connection esys in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun l ->
		( try
		    let ds =
		      List.find (fun ds1 -> ds1.id = id) l in (*or Not_found *)
		    try
		      update
			(fun ds1 ->
			   { ds1 with
			       size = ds.size;
			       enabled = ds.enabled;
			       node = 
			       ( if not ds.enabled then None else ds1.node );
			   })
			ds.identity
		    with
		      | Failure _ -> (** identity not found  *)
			add ds
		  with
		    | Not_found -> (** If not found in db, delete the row *)
			delete_by_id id
		);
		dlogf "update_from_db: return";
		t#return();
		dlogf "update_from_db: when_done";
		when_done();
	     )
    ~is_error:(fun error ->
		 Netlog.logf `Err
		   "Nn_monitor.update_from_db: exception %s"
		   (Netexn.to_string error);
		 t#return();
	      )
    ~is_aborted:(fun () ->
		   Netlog.logf `Err
		     "Nn_monitor.update_from_db: aborted";
		   t#return();
		)
    e


let init_from_db ?(when_done = fun () -> ()) () =
  (** Start a new transaction, read the table of datastores, and set the
      in-memory version of it to this list.
       Call [when_done] when the in-memory version is changed.
   *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let t = Nn_db.transact esys 0 in  (* FIXME: commit_id *)
  let e = Nn_db.datastore_list_e t#connection esys in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun l ->
		set_list l;
		t # return();
		when_done();
	     )
    ~is_error:(fun error ->
		 Netlog.logf `Err
		   "Nn_monitor.init_from_db: exception %s"
		   (Netexn.to_string error);
		 t # return();
	      )
    ~is_aborted:(fun () ->
		   Netlog.logf `Err
		     "Nn_monitor.init_from_db: aborted";
		   t # return();
		)
    e


let is_alive idstr = 
  let errors = error_table() in
  let conf = Node_config_var.get cfgname in
  try
    let n = Hashtbl.find errors idstr in
    n > conf#dn_max_errors
  with
    | Not_found -> true


let activate_node saddr ds cauth =
  (** Check what the datanode is reporting about itself, and if it is good
      set the [node] component of the [datastore] to the socket address
   *)
  let conf = Node_config_var.get cfgname in
  let blocksize = conf#dn_blocksize in
  let (h,p) =
    Plasma_util.host_port_of_sockaddr saddr in
  dlogf "discover at %s:%d" h p;
  let conn =
    Plasma_util.connector_of_sockaddr saddr in
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let client =
    Pfs_rpcapi_clnt.Datanode.V1.create_client2
      ~esys
      (`Socket(Rpc.Tcp, conn, Rpc_client.default_socket_config)) in
  Rpc_client.configure client 0 conf#dn_node_timeout;
  Rpc_client.set_auth_methods client 
    (Pfs_auth.rpc_proxy_auth_methods cauth false);

  let report_error n e =
    Netlog.logf `Err
      "Error in RPC call %s @ %s: %s"
      n
      (Netsys.string_of_sockaddr saddr)
      (Netexn.to_string e);
    Rpc_client.shut_down client;
  in

  try
    Pfs_rpcapi_clnt.Datanode.V1.identity'async
      client
      conf#nn_clustername
      (fun get_reply ->
	 try
	   let id = get_reply() in
	   if id <> ds.identity then
	     failwith (sprintf "Wrong identity: %s" id);
	   Pfs_rpcapi_clnt.Datanode.V1.blocksize'async
	     client
	     ()
	     (fun get_reply ->
		try
		  let bs = get_reply() in
		  if bs <> blocksize then
		    failwith "Node has wrong blocksize";
		  Pfs_rpcapi_clnt.Datanode.V1.size'async
		    client
		    ()
		    (fun get_reply ->
		       try
			 let size = get_reply() in
			 if size < ds.size then
			   failwith "Node has fewer blocks than expected";
			 Rpc_client.shut_down client;
			 (* If still inactive, activate now *)
			 let do_activate =
			   try (get ds.identity).node = None
			   with Not_found -> false in
			 if do_activate then (
			   update 
			     (fun ds -> { ds with node = Some saddr; alive = true })
			     ds.identity;
			   Netlog.logf `Info
			     "Discovered datanode %s at %s:%d with size %Ld %s"
			     ds.identity h p size 
			     (if ds.enabled then "(enabled)" 
			      else "disabled")
			 )
		       with
			 | error ->
			     report_error "Datanode.V1.size" error
		    )
		with
		  | error ->
		      report_error "Datanode.V1.blocksize" error
	     )
	 with
	   | error ->
	       report_error "Datanode.V1.identity" error
      )
  with
    | error ->
	report_error "Datanode.V1.identity" error
	  



let start_publishing() =
  if not(is_publishing()) then (
    if Netplex_sharedvar.create_var ~enc:true shdsname then (
      Bool_var.set pubname true;
      set_list (list());
      Netlog.logf `Info "Publishing news about datanode status";
    )
    else
      Netlog.logf `Err "Cannot create shared variable: %s" shdsname
  )


let mcast_discovery conf cauth =
  let round = ref 0 in
  let responses = Hashtbl.create 17 in
  let xids = ref [] in
  let errors = error_table() in
  
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let getsock() =
    let udp_sock = Unix.socket Unix.PF_INET Unix.SOCK_DGRAM 0 in
    Netsys.mcast_set_loop udp_sock true;
    Netsys.mcast_set_ttl udp_sock conf#dn_multicast_ttl;
    udp_sock in
  let mk_client() =
    let conn = Rpc_client.Dynamic_descriptor getsock in
    let client =
      Pfs_rpcapi_clnt.Datanode_discover.V1.create_client2
	~esys
	(`Socket(Rpc.Udp, conn, Rpc_client.default_socket_config)) in
    Rpc_client.set_exception_handler 
      client (fun _ -> ()); 
    Rpc_client_var.set disclientname client;
    client in

  let client = ref(mk_client()) in
  let max_age = 5 in
  (* we accept discovery responses from max_age rounds (seconds) ago, but
     not older responses
   *)

  let discovery_round is_first =
    try
      if not is_first then (
	(* Forget xids of messages that are too old and that have not been
	   responded
	 *)
	xids :=
	  List.filter
	    (fun (client,xid,r) -> 
	       if r < !round - max_age then (
		 Rpc_client.abandon_call client xid;
		 false
	       )
	       else true
	    ) 
	    !xids;
	
	(* Check the results of previous round *)
	let ds_list = list() in
	List.iter
	  (fun ds ->
	     if ds.enabled then (
	       try
		 (* we raise Not_found if there is no adequate response for
		    ds.identity
		  *)
		 let (addr,port,r) = Hashtbl.find responses ds.identity in
		 if r < !round-max_age then raise Not_found;
		 let saddr = Unix.ADDR_INET(addr,port) in
		 let old_n =
		   try Hashtbl.find errors ds.identity with Not_found -> 0 in
		 Hashtbl.remove errors ds.identity;
		 if old_n > conf#dn_max_errors then (
		   Netlog.logf `Notice
		     "Datanode identity %s again alive at %s:%d"
		     ds.identity (Unix.string_of_inet_addr addr) port;
		 );
		 match ds.node with
		   | None ->
		       (* Discovered a new node *)
		       activate_node saddr ds cauth
		   | Some ds_saddr ->
		       let (ds_h, ds_p) = 
			 Plasma_util.host_port_of_sockaddr ds_saddr in
		       if saddr <> ds_saddr then (
			 Netlog.logf `Alert
			   "Datanode identity %s is found at %s:%d \
                            but used to be at %s:%d"
			   ds.identity
			   (Unix.string_of_inet_addr addr)
			   port
			   ds_h
			   ds_p
		       );
		       if old_n > conf#dn_max_errors then (
			 update 
			   (fun ds -> 
			      { ds with alive = true })
			   ds.identity;
		       )
	       with 
		 | Not_found ->
		     let n = 
		       try Hashtbl.find errors ds.identity 
		       with Not_found -> 0 in
		     Hashtbl.replace errors ds.identity (n+1);
		     if n = conf#dn_max_errors+1 then (
		       Netlog.logf `Err
			 "Datanode identity %s is UNREACHABLE"
			 ds.identity;
		       update 
			 (fun ds -> 
			    { ds with alive = false })
			 ds.identity;
		     );
	     )
	  )
	  ds_list;

	(* We are not interested in responses not occurring in ds_list *)

	start_publishing()
      );
      
      incr round;
      let this_round = !round in

      List.iter
	(fun addr ->
	   (* addr can be both a unicast or a multicast address *)
	   let is_mcast =
	     Plasma_util.is_multicast_inet_addr addr in
	   Rpc_client.set_dgram_destination !client
	     (Some (Unix.ADDR_INET(addr,conf#dn_port)));
	   Pfs_rpcapi_clnt.Datanode_discover.V1.identity'async
	     !client
	     conf#nn_clustername
	     (fun get_result ->
		(* It is possible that there is a delay between sending
		   the RPC request and getting the response, i.e.
		   this_round <> !round
		 *)
		( try
		    let identity = get_result() in
		    let r_saddr = 
		      Rpc_client.get_sender_of_last_response !client in
		    let r_addr =
		      match r_saddr with
			| Unix.ADDR_INET(a,_) -> a
			| _ -> assert false in
		    (* check the old response. The entry in [responses] is
		       only updated when the current response is newer
		     *)
		    ( try
			let (a,p,r) = Hashtbl.find responses identity in
			if (a <> r_addr || p <> conf#dn_port) &&
			   r >= !round - max_age && 
			   this_round >= !round - max_age
			then
			  Netlog.logf `Alert
			    "Datanode identity %s is reported by %s:%d and \
                             by %s:%d"
			    identity
			    (Unix.string_of_inet_addr a)
			    p
			    (Unix.string_of_inet_addr r_addr)
			    conf#dn_port;
			if this_round > r then
			  raise Not_found
		      with
			| Not_found ->
			    Hashtbl.replace
			      responses 
			      identity (r_addr,conf#dn_port,this_round)
		    )
		  with
		    | Not_found -> ()
		    | Rpc_client.Communication_error
			(Unix.Unix_error
			   ((Unix.ENETUNREACH | Unix.EHOSTUNREACH), _, _))
			as error when is_mcast -> (
			  Netlog.logf `Alert
			    "Datanode discovery: Multicast request to %s \
                             cannot be routed. This seems \
                             to be an error in the multicast configuration of \
                             this host"
			    (Unix.string_of_inet_addr addr);
			  Netlog.logf `Crit
			    "Discovery exception: %s" (Netexn.to_string error)
			)
		    |  Rpc_client.Communication_error
			(Unix.Unix_error
			   ((Unix.ENETUNREACH | Unix.EHOSTUNREACH), _, _))
			as error -> (
			  Netlog.logf `Alert
			    "Datanode discovery: Unicast request to %s \
                             cannot be routed"
			    (Unix.string_of_inet_addr addr);
			  Netlog.logf `Crit
			    "Discovery exception: %s" (Netexn.to_string error)
			)
		    | error ->
			Netlog.logf `Crit
			  "Discovery exception: %s" (Netexn.to_string error)
		);
		raise Rpc_client.Keep_call
	     );
	   let xid = Rpc_client.get_xid_of_last_call !client in
	   xids := (!client,xid,this_round) :: !xids
	)
	conf#dn_discovery_addrs

    with
      | Rpc_client.Client_is_down ->
	  client := mk_client()
      | error ->
	  Netlog.logf `Err
	    "Exception in datanode discovery: %s"
	    (Netexn.to_string error)
  in


  (* Emit repeatedly discovery messages. In each round, check which nodes
     responded in the previous round
   *)
  discovery_round true;
  let _tm =
    Netplex_cenv.create_timer
      (fun tm -> discovery_round false; true)
      conf#dn_discovery_period in
  ()


let post_add_hook _ ctrl =
  ctrl # add_plugin Netplex_sharedvar.plugin

let receive_message cauth cont msg args =
  if msg = shdsname then (
    match args with
      | [| "have_db_news"; id_s |] ->
	  Netlog.logf `Info "Got notification about db update";
	  let id = int_of_string id_s in
	  update_from_db id
      | [| "have_db_news_discover"; id_s; hp |] ->
	  Netlog.logf `Info 
	    "Got notification about db update; starting discovery at %s" hp;
	  let id = int_of_string id_s in
	  let (h,p) = Plasma_util.parse_host_port hp in
	  let saddr = Unix.ADDR_INET(Unix.inet_addr_of_string h, p) in
	  ( try
	      update_from_db
		~when_done:(fun () -> 
			      try
				let ds = get_by_id id in
				activate_node saddr ds cauth
			      with Not_found -> ()
			   )
		id
	    with Not_found ->
	      Netlog.logf `Crit
		"Announced db id not found: %d" id
	  )
      | _ -> 
	  ()
  )

let post_start_hook _ conf cauth =
  Node_config_var.set cfgname conf;
  if Netplex_sharedvar.create_var ~enc:true errname then (
    let et = Hashtbl.create 17 in
    Errors_var.set errname et;
  );
  Netlog.logf `Info "Reading datastore table";
  init_from_db
    ~when_done:(fun () ->
		  Netlog.logf `Info "Discovering datanodes";
		  mcast_discovery conf cauth
	       )
    ()


let shutdown () =
  try
    let client = Rpc_client_var.get disclientname in
    Rpc_client.shut_down client
  with
    | _ -> ()

