(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* N.B. If we ever support hot failover of the coordinator role to other 
   namenodes, the authentication tickets need to be handled specially.
   They are not stored in the db, and hence not automatically available
   in the new coordinator. How to solve:
   - Each ticket is pushed to the slaves (WHEN? HOW FREQUENTLY?)
     Probably we need this only for long-living tickets, i.e. those that
     are used in several sessions. If a ticket is created for a single
     session only, the death of the original coordinator will cause
     that this session fails, and hence it is not required that the ticket
     is available in the replacement coordinator. (Short living tickets
     are those requested by the authentication daemon.)

   Also, if we ever support the attachment of new slaves, we have to
   ensure that the new slaves also get the tickets (e.g. by just copying
   them over).
 *)

type principal = 
    Unix.passwd_entry * string * Plasma_util.StrSet.t

type header =
    { mutable mutex : Netmcore_mutex.mutex
    }

type auth_local =
    { shared : (int64, float * principal, header) Netmcore_hashtbl.t;
    }

module Auth_local_var =
  Netplex_cenv.Make_var_type(struct type t = auth_local end)

let auth_local_name = "current_auth"

module Shared_var =
  Netplex_sharedvar.Make_var_type
    (struct 
       type t = (int64, float * principal, header) Netmcore_hashtbl.t_descr
     end )

let shared_var_name = "plasma.namenode.authticket.shm"

let lifetime = 86400.0


let rec create_p auth max_time p =
  let ticket = Plasma_rng.random_int64() in
  let h = Netmcore_hashtbl.header auth.shared in
  Netmcore_mutex.lock h.mutex;
  if Netmcore_hashtbl.mem_ro auth.shared ticket then (
    Netmcore_mutex.unlock h.mutex;
    create_p auth max_time p
  )
  else (
    Netmcore_hashtbl.add auth.shared ticket (max_time, p);
    Netmcore_mutex.unlock h.mutex;
    ticket
  )


let create user =
  let ug = Nn_ug.get_ug_admin() in
  let pw_e = ug#getpwnam user in
  let gr_e = ug#getgrgid pw_e.Unix.pw_gid in
  let main_group = gr_e.Unix.gr_name in
  let groups = ug#getgroups user in
  let p = (pw_e, main_group, groups) in
  let auth = Auth_local_var.get auth_local_name in
  let max_time = Unix.time() +. lifetime in
  create_p auth max_time p


let renew ticket =
  let auth = Auth_local_var.get auth_local_name in
  let h = Netmcore_hashtbl.header auth.shared in
  Netmcore_mutex.lock h.mutex;
  if Netmcore_hashtbl.mem_ro auth.shared ticket then (
    Netmcore_hashtbl.find_p auth.shared ticket
      (fun (_, p) ->
	 let max_time = Unix.time() +. lifetime in
	 Netmcore_hashtbl.replace auth.shared ticket (max_time, p);
      )
  );
  Netmcore_mutex.unlock h.mutex


let delete ticket =
  let auth = Auth_local_var.get auth_local_name in
  let h = Netmcore_hashtbl.header auth.shared in
  Netmcore_mutex.lock h.mutex;
  Netmcore_hashtbl.remove auth.shared ticket;
  Netmcore_mutex.unlock h.mutex


let principal ticket =
  let auth = Auth_local_var.get auth_local_name in
  let h = Netmcore_hashtbl.header auth.shared in
  Netmcore_mutex.lock h.mutex;
  try
    let (_, p) = Netmcore_hashtbl.find_c auth.shared ticket in
    Netmcore_mutex.unlock h.mutex;
    p
  with
    | Not_found ->
	Netmcore_mutex.unlock h.mutex;
	raise Not_found


let housekeeping _ =
  ( try
      let auth = Auth_local_var.get auth_local_name in
      let h = Netmcore_hashtbl.header auth.shared in
      Netmcore_mutex.lock h.mutex;
      let timed_out = ref [] in
      let now = Unix.time() in
      let n = Netmcore_hashtbl.length auth.shared in
      Netmcore_hashtbl.iter
	(fun ticket (t, _) ->
	   if now > t then 
	     let ticket_c = Netmcore_heap.copy ticket in
	     timed_out := ticket_c :: !timed_out
	)
	auth.shared;
      List.iter
	(fun ticket ->
	   Netmcore_hashtbl.remove auth.shared ticket
	)
	!timed_out;
      Netmcore_mutex.unlock h.mutex;
      let p = List.length !timed_out in
      Netlog.logf `Info
	"Authentication tickets: deleting %d, keeping %d" p (n-p)
    with
      | error ->
	  Netlog.logf `Crit
	    "Exception in Nn_authticket.housekeeping: %s"
	    (Netexn.to_string error)
  );
  true


let post_start_hook pool =
  let shm =
    if Netplex_sharedvar.create_var ~enc:true shared_var_name then (
      let h0 =
        { mutex = Netmcore_mutex.dummy();
        } in
      let shm = Netmcore_hashtbl.create pool h0 in
      Netmcore_heap.modify
        (Netmcore_hashtbl.heap shm)
        (fun mut ->
           let h = Netmcore_hashtbl.header shm in
           h.mutex <- Netmcore_mutex.create mut `Normal
        );
      let descr = Netmcore_hashtbl.descr_of_hashtbl shm in
      Shared_var.set shared_var_name descr;
      shm
    )
    else (
      ignore(Netplex_sharedvar.wait_for_enc_value shared_var_name);
      let descr = Shared_var.get shared_var_name in
      Netmcore_hashtbl.hashtbl_of_descr pool descr
    ) in
  
  Auth_local_var.set auth_local_name
    { shared = shm
    };

  let _tmr =
    Netplex_cenv.create_timer housekeeping 60.0 in

  ()
