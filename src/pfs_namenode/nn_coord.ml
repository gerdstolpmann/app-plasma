(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** The main module for the coordinator (master). Defines RPC services
    for [Filesystem], [Coordination], and [Dn_admin].

    The [Nameslave] program is also "implemented", but all calls 
    (except [null]) fail.
 *)

open Printf
open Plasma_util.Operators

let dlog = Plasma_util.dlog
let dlogr = Plasma_util.dlogr

module Rpcapi_clnt = Pfs_rpcapi_clnt
module Rpcapi_srv = Pfs_rpcapi_srv
module Rpcapi_aux = Pfs_rpcapi_aux
module A = Pfs_rpcapi_aux

module StrSet = Plasma_util.StrSet

module Node_config_var =
  Netplex_cenv.Make_var_type(struct type t = Nn_config.nn_node_config end)

module Shared_state_var =
  Netplex_cenv.Make_var_type(struct type t = Nn_state.shared_state_t end)

module State_shm_var =
  Netplex_sharedvar.Make_var_type(struct type t = Nn_state.sh_state_shm end)

let cfgname = "current_node_config"
let shstname = "current_shared_state"
let stshmname = "plasma.namenode.coordinator.shm"

let get_shared() =
  Shared_state_var.get shstname


(**********************************************************************)
(* Revisions                                                          *)
(**********************************************************************)

let wait_until_slaves_are_ready_e() =
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in

  let rec loop_e l =
    match l with
      | (hp, _, enabled) :: l' ->
	  if enabled then
	    let conn = 
	      Plasma_util.connector_of_sockaddr
		(Plasma_util.sockaddr_of_host_port
		   (Plasma_util.parse_host_port hp)) in
	    Nn_push.wait_until_ready_e conn esys
	    ++ (fun () -> loop_e l')
	  else
	    loop_e l'

      | [] ->
	  eps_e (`Done()) esys
  in
  loop_e sh#slaves


let revision_transaction_e() =
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  let v = new Nn_state.revclear_view in
  Nn_commit.trans_e 
    sh [ (v :> Nn_state.view_t) ]
    ( eps_e (`Done()) esys )
    "revision_transaction"
  ++ (fun st -> eps_e (st :> _ Uq_engines.engine_state) esys)

(**********************************************************************)
(* Dn_admin                                                           *)
(**********************************************************************)

(** Implementation of [Dn_admin]: *)

let proc_add_datanode sess (identity,size) emit =
  (** Adds the [datastore] row to the db *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  sh # incr_count `Transactions_total;
  let v = new Nn_state.ds_view sh in
  let e = 
    Nn_commit.trans_e 
      sh [ (v :> Nn_state.view_t) ]
      (	v#datastore_new_id_e()
	++ (fun id ->
	      v#datastore_upd_e ~id ~identity ~size ~enabled:false
	      ++ (fun _ ->
		    eps_e (`Done id) esys
		 )
	   )
      ) "proc_add_datanode" in
  let emit1 id =
    (** Notify [nn_monitor]: *)
    Nn_datastore_news.signal_db_change id None;
    emit () in
  Plasma_util.rpc_srv_reply "Dn_admin.add_datanode" sess emit1 e


let proc_enable_or_disable procname enable sess (identity,hp) emit =
  (** Modifies the [datastore] db row *)
  if enable then ( (** syntax check only *)
    ignore(Plasma_util.parse_host_port hp)
  );
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  sh # incr_count `Transactions_total;
  let v = new Nn_state.ds_view sh in
  let e = 
    Nn_commit.trans_e
      sh [(v :> Nn_state.view_t)]
      ( v#datastore_list_e()
	++ (fun stores ->
	      try
		let ds =
		  try 
		    List.find 
		      (fun ds -> ds.Nn_datastores.identity = identity) 
		      stores
		  with Not_found -> 
		    failwith (procname ^ ": identity not found") in
		let id = ds.Nn_datastores.id in
		let size = ds.Nn_datastores.size in
		v#datastore_upd_e ~id ~identity ~size ~enabled:enable
		++ (fun _ ->
		      eps_e (`Done id) esys
		   )
	      with
		| error ->
		    eps_e (`Error error) esys
	   )
      )
      "proc_enable_or_disable" in
  let emit1 id =
    (** Notify [nn_monitor]: *)
    let node_opt =
      if enable then
	let (h,p) = Plasma_util.parse_host_port hp in
	let addr = Plasma_util.sockaddr_of_host_port (h,p) in
	Some addr
      else
	None in
    Nn_datastore_news.signal_db_change id node_opt;
    (** Notify Nn_datanode_ctrl: *)
    if not enable then (
      Nn_datanode_ctrl.disconnect id
    );
    emit () in
  Plasma_util.rpc_srv_reply procname sess emit1 e


let proc_enable_datanode =
  proc_enable_or_disable "Dn_admin.enable_datanode" true

let proc_disable_datanode sess identity emit =
  proc_enable_or_disable
    "Dn_admin.disable_datanode" false sess (identity,"") emit

let proc_is_enabled sess (identity) emit =
  (** Get from the [Monitor] service whether this datastore is enabled. *)
  (** First force that our version of the datastore list is updated from
      the [Monitor] process. This is ok here because this is an admin 
      interface, and we want to be as exact as possible
   *)
  Nn_datastore_news.force();
  (** Get the information: *)
  try
    let ds = Nn_datastore_news.get identity in (* or Not_found *)
    emit ds.Nn_datastores.enabled
  with
    | Not_found ->
	Netlog.logf `Err
	  "Dn_admin.is_enabled: Unknown datastore: %s" identity;
	emit false


let proc_is_alive sess (identity) emit =
  (* Similar to proc_is_enabled *)
  Nn_datastore_news.force();
  try
    let ds = Nn_datastore_news.get identity in (* or Not_found *)
    emit ds.Nn_datastores.alive
  with
    | Not_found ->
	Netlog.logf `Err
	  "Dn_admin.is_alive: Unknown datastore: %s" identity;
	emit false


let proc_lookup sess (identity) emit =
  (** Do the lookup (similar to [proc_is_enabled]): *)
  Nn_datastore_news.force();
  try
    let ds = Nn_datastore_news.get identity in (* or Not_found *)
    let str =
      if ds.Nn_datastores.enabled then
	match ds.Nn_datastores.node with
	  | Some addr -> 
	      ( match addr with
		  | Unix.ADDR_INET(ip,p) ->
		      sprintf "%s:%d"
			(Unix.string_of_inet_addr ip) p
		  | _ ->
		      assert false
	      )
	  | None -> ""
      else
	"" in
    emit str
  with
    | Not_found ->
	Netlog.logf `Err
	  "Dn_admin.is_enabled: Unknown datastore: %s" identity;
	Rpc_server.reply_error sess Rpc.System_err


let proc_datanodes sess () emit =
  (** Do the lookup (similar to [proc_is_enabled]): *)
  Nn_datastore_news.force();
  emit
    (Array.of_list
       (List.map
	  (fun ds -> ds.Nn_datastores.identity)
	  (Nn_datastore_news.list())
       )
    )

let proc_destroy_datanode sess (identity) emit =
  (** Deletes the [datastore] db row *)
  let procname = "destroy_datanode" in
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  sh # incr_count `Transactions_total;
  let v = new Nn_state.ds_view sh  in
  let e = 
    Nn_commit.trans_e
      sh [(v :> Nn_state.view_t)]
      ( v#datastore_list_e()
	++ (fun stores ->
	      try
		let ds =
		  try 
		    List.find 
		      (fun ds -> ds.Nn_datastores.identity = identity) 
		      stores
		  with Not_found -> 
		    failwith (procname ^ ": identity not found") in
		let id = ds.Nn_datastores.id in
		v#datastore_del_e ~id
		++ (fun _ ->
		      eps_e (`Done id) esys
		   )
	      with
		| error ->
		    eps_e (`Error error) esys
	   )
      ) "proc_destroy_datanode" in
  let emit1 id =
    (** Notify [nn_monitor]: *)
    Nn_datastore_news.signal_db_change id None;
    (** Notify Nn_datanode_ctrl: *)
    Nn_datanode_ctrl.disconnect id;
    emit () in
  Plasma_util.rpc_srv_reply procname sess emit1 e


let proc_fsstat sess identity emit =
  dlogr (fun () -> sprintf "proc_fsstat");
  let sh = get_shared() in
  let esys = (Netplex_cenv.self_cont()) # event_system in

  let e =
    sh # blockmap_get_e ~identity
    ++ (fun bm ->
	  let (n_used,n_trans,n_free) = bm#stats in
	  eps_e (`Done(n_used,n_trans,n_free)) esys
       ) in
  Plasma_util.rpc_srv_reply
    "fsstat"
    sess emit 
    (Uq_engines.meta_engine
       (e ++
	  (fun (n_used,n_trans,n_free) -> 
	     let r =
	       { Pfs_rpcapi_aux.total_blocks = 
		   Int64.add n_used (Int64.add n_trans n_free);
		 used_blocks = n_used;
		 trans_blocks = n_trans;
		 enabled_datanodes = 0;
		 alive_datanodes = 0;
		 dead_datanodes = [| |]
	       } in
	     
	     eps_e (`Done r) esys
	  )))


(**********************************************************************)
(* Filesystem                                                         *)
(**********************************************************************)

(** Implementation of [Filesystem]: *)

type fs_user =
    { user : string;
      group : string;
      supp_groups : Plasma_util.StrSet.t
    }

type fs_creds =
    [ `User of fs_user
    | `Nobody
    | `Root
    ]

type fs_transaction =
    { bm_view : Nn_state.bm_view_t;
      inode_view : Nn_state.inode_view_t;
      ticket_verifier : Nn_datanode_ctrl.ticket_verifier;
      mutable running : bool;   (* whether an operation is running *)
      mutable pending_abort : bool;
        (* a pending abort is done first when the current operation is
	   finished
	 *)
      creds : fs_creds;
    }

type fs_trans_tbl =
    (Rpc_server.connection_id * int64, fs_transaction) Hashtbl.t

module Fs_trans_tbl_var =
  Netplex_cenv.Make_var_type(struct type t = fs_trans_tbl end)

type fs_user_tbl =
    (Rpc_server.connection_id, fs_user) Hashtbl.t

module Fs_user_tbl_var =
  Netplex_cenv.Make_var_type(struct type t = fs_user_tbl end)

module Int64_var =
  Netplex_cenv.Make_var_type(struct type t = int64 end)

let fs_trans_tbl_name = "current_transtbl"

let fs_user_tbl_name = "current_usertbl"

let get_trans_tbl() =
  try
    Fs_trans_tbl_var.get fs_trans_tbl_name
  with
    | _ ->
	let ht = Hashtbl.create 7 in
	Fs_trans_tbl_var.set fs_trans_tbl_name ht;
	ht

let get_user_tbl() =
  try
    Fs_user_tbl_var.get fs_user_tbl_name
  with
    | _ ->
	let ht = Hashtbl.create 7 in
	Fs_user_tbl_var.set fs_user_tbl_name ht;
	ht

let get_creds sess =
  let cid = Rpc_server.get_connection_id sess in
  let creds_tbl = get_user_tbl() in
  try
    `User(Hashtbl.find creds_tbl cid)
  with
    | Not_found ->
	(* No impersonation has happened yet. *)
	if Rpc_server.get_user sess = Pfs_auth.privileged_user then
	  `Root
	else
	  `Nobody

let set_creds sess user group supp_groups =
  let cid = Rpc_server.get_connection_id sess in
  let creds_tbl = get_user_tbl() in
  Hashtbl.replace creds_tbl cid 
    { user = user;
      group = group;
      supp_groups = Plasma_util.StrSet.add group supp_groups
    }


let sess_peer sess =
  try Netsys.string_of_sockaddr
    (Rpc_server.get_peer_name sess)
  with _ -> "n/a"

let unmanaged_transaction creds =
  let sh = get_shared() in
  let bm_v = new Nn_state.bm_view sh in
  let inode_v = new Nn_state.inode_view sh in
  { bm_view = bm_v;
    inode_view = inode_v;
    ticket_verifier = Nn_datanode_ctrl.new_ticket_verifier();
      (* N.B. new_ticket_verifier does not raise Datanode_ctrl exceptions *)
    running = false;
    pending_abort = false;
    creds = creds;
  }

let new_transaction_e sess tid =
  let sh = get_shared() in
  sh # incr_count `Transactions_total;
  try
    let cid = Rpc_server.get_connection_id sess in
    let transtbl = get_trans_tbl() in
    let fstr = unmanaged_transaction (get_creds sess) in
    Hashtbl.replace transtbl (cid,tid) fstr;
    dlogr
      (fun () ->
	 sprintf 
	   "New transaction %Ld (peer %s)"
	   tid (sess_peer sess));
    eps_e (`Done fstr) sh#event_system
  with error ->
    eps_e (`Error error) sh#event_system


let del_transaction cid tid =
  let transtbl = get_trans_tbl() in
  Hashtbl.remove transtbl (cid,tid)

let fs_rpc_srv_reply procname sess emit te =
  (** Handle a server reply. When a [Transaction_error] is found, set
      [errno] to this exception. For unknown exceptions, set [errno] to
      [`efailed], the generic error.

      The engine [te] must follow the conventions for transactional
      engines. Such engines never transition to [`Error] or [`Abort],
      but always to [`Done] (corresponding to the fact that transactions
      handle errors so they do not propagate to the "outside"). The [`Done]
      state has the original state value as argument, i.e. we see
      [`Done(`Done v)], [`Done(`Error e)], and [`Done `Aborted], so
      the value of the inner engine enclosed by the transaction handler is
      still visible.
   *)
  let te' =
    te 
    >> (function
	  | `Done(`Done x) ->
	      `Done(`Done(`ok x :> [Nn_state.errno | `ok of _] ))
	  | `Done(`Error(Nn_state.Transaction_error code as err)) ->
	      Netlog.logf `Err
		"Error in %s: %s" procname (Netexn.to_string err);
	      `Done(`Done(code :> [Nn_state.errno | `ok of _] ))
	  | `Done(`Error err) ->
	      Netlog.logf `Crit
		"Error in %s: %s" procname (Netexn.to_string err);
	      `Done(`Done `efailed)
	  | `Done `Aborted ->
	      `Done `Aborted
	  | `Done (`Working _) ->
	      assert false
	  | `Error err ->
	      `Error err
	  | `Aborted ->
	      `Aborted
       ) in
  Plasma_util.rpc_srv_reply
    procname sess emit te'

let fs_rpc_srv_reply_unit ?(anyway=fun()->()) procname sess emit te =
  (** A variant of the former for unit/void replies *)
  let te' =
    te 
    >> (function
	  | `Done(`Done ()) ->
	      `Done(`Done(`ok :> [Nn_state.errno | `ok ] ))
	  | `Done(`Error(Nn_state.Transaction_error code as err)) ->
	      Netlog.logf `Err
		"Error in %s: %s" procname (Netexn.to_string err);
	      `Done(`Done(code :> [Nn_state.errno | `ok ] ))
	  | `Done(`Error err) ->
	      Netlog.logf `Crit
		"Error in %s: %s" procname (Netexn.to_string err);
	      `Done(`Done `efailed)
	  | `Done `Aborted ->
	      `Done `Aborted
	  | `Done (`Working _) ->
	      assert false
	  | `Error err ->
	      `Error err
	  | `Aborted ->
	      `Aborted
       ) in
  Plasma_util.rpc_srv_reply
    ~onerror:(fun _ -> anyway())
    ~onaborted:(fun _ -> anyway())
    ~ondone:(fun _ -> anyway())
    procname sess emit te'


let abort_trans cid tid trans =
  del_transaction cid tid;
  trans.bm_view # rollback();
  trans.inode_view # rollback();
  ignore(Nn_datanode_ctrl.disable_ticket_verifier
	   trans.ticket_verifier
	   trans.bm_view#release
	)
    


let with_transaction_1 rpc_srv_reply procname sess emit tid f =
  (** Ensure [f] is run within a transaction. If the transaction is already
      started everything is fine. 

     [sess], [emit]: from the RPC. [procname] is the name of the RPC
     for messages
   *)

  let esys = (Netplex_cenv.self_cont()) # event_system in
  let transtbl = get_trans_tbl() in
  let cid = Rpc_server.get_connection_id sess in
  let trans_opt =
    try Some(Hashtbl.find transtbl (cid,tid))
    with Not_found -> None in
  let run_f (g : _ Uq_engines.engine -> _ Uq_engines.engine) trans =
    trans.running <- true;
    let te = 
      try
	(** [g] is for mapping results so the result is always [`Done].
            Also for commit/abort in autocommit mode.
	 *)
	g(f trans) 
      with
	| error ->
	    eps_e (`Done (`Error error)) esys in
    let te' =
      te
      ++ (fun r ->
	    trans.running <- false;
	    if trans.pending_abort then (
	      Netlog.logf `Info
		"Executing pending rollback of zombie transaction %Ld" tid;
	      abort_trans cid tid trans;
	      trans.pending_abort <- false;
	      eps_e (`Done (`Error (Nn_state.Transaction_error `efailed))) esys
	    )
	    else 
	      eps_e (`Done r) esys
	 ) in
    rpc_srv_reply procname sess emit te'
  in
  match trans_opt with
    | Some trans ->
	(** The transaction is already started. *)
	if trans.running then
	  emit `etbusy
	else
	  run_f 
	    (fun e1 ->
	       map_e
		 ~map_done:(fun v -> `Done(`Done v))
		 ~map_error:(fun e -> `Done(`Error e))
		 ~map_aborted:(fun _ ->`Done `Aborted)
		 e1
	    )
	    trans
    | None -> (
	(** There is no transaction yet *)
	dlogr
	  (fun () ->
	     sprintf
	       "ENOTRANS for transaction %Ld (peer %s)"
	       tid (sess_peer sess));
	emit `enotrans
      )

let with_transaction procname =
  with_transaction_1 fs_rpc_srv_reply procname

let with_transaction_unit =
  with_transaction_1 fs_rpc_srv_reply_unit

let fake_trans e =
  (** Wrap [`Done] in another [`Done], so it looks like as if the value
      came from a transactional engine
   *)
  map_e
    ~map_done:(fun v -> `Done(`Done v))
    e

let trans_error errno =
  Nn_state.Transaction_error errno


let blockmap_dealloc_e ~inode bmv blist =
  let bm_dealloc =
    Nn_blocklist.fold
      (fun identity index_range bi acc ->
	 let tuple =
	   ( bi.Rpcapi_aux.identity, 
	     bi.Rpcapi_aux.block,
	     bi.Rpcapi_aux.length ) in
	 tuple :: acc
      )
      blist
      [] in
  let total =
    List.fold_left
      (fun acc (_,_,l) -> Int64.add l acc) 0L bm_dealloc in
  dlogr
    (fun () ->
       sprintf "blockmap_dealloc_e: #blocks=%Ld" total);
  bmv # blockmap_dealloc_e ~inode bm_dealloc


let delete_inode_e trans id =
  (** Sequence of actions:
      1. Get inodeinfo, and blocklimit
      2. Get list of blocks [old_bis] in the FULL range (can be long)
      3. Deallocate the [old_bis] blocks in the blockmap
      4. Remove inode
   *)
  (* TODO: for very large files it can be a problem that step 2 returns
     the complete list. This is not fixable by performing 2./3. in a 
     loop, because the blocks are also cached. Maybe delete the cache
     when it becomes too full? 
   *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  trans.inode_view # inode_get_with_lock_e ~id
    (* lock needed by inode_dealloc_e anyway *)
  ++ (fun ii_opt ->
	match ii_opt with
	  | None ->
	      eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	  | Some ii ->
	      dlog "delete_inode: found inode";
	      eps_e (`Done ii) esys)
  ++ (fun ii ->
	trans.inode_view # inode_get_blocks_e
	  ~id ~blkidx:0L ~len:ii.A.blocklimit ~pin_flag:false trans.bm_view)
  ++ (fun old_bis ->
	blockmap_dealloc_e ~inode:id trans.bm_view old_bis)
  ++ (fun () ->
	trans.inode_view # inode_dealloc_e ~id
     )


let delete_inodes_e trans idlist =
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let q = Queue.create() in
  List.iter (fun id -> Queue.add id q) idlist;
  Plasma_util.q_seq_engine q esys
    (fun id -> delete_inode_e trans id)

    
    
let proc_begin_transaction sess tid emit =
  (** When a transaction is started, the datanodes are notified of it
      (the [Nn_datanode_ctrl] call below), so they can accept block writes
      for this transaction.
   *)
  (* sec *)
  dlogr (fun () -> sprintf "proc_begin_transaction tid=%Ld" tid);
  (get_shared()) # incr_count `RPC_filesystem;
  if get_creds sess = `Nobody then
    emit `eperm
  else (
    let transtbl = get_trans_tbl() in
    let cid = Rpc_server.get_connection_id sess in
    let trans_opt =
      try
	Some(Hashtbl.find transtbl (cid,tid))
      with Not_found -> None in
    match trans_opt with
      | None ->
	  fs_rpc_srv_reply_unit
	    "begin_transaction"
	    sess emit 
	    (fake_trans
	       (new_transaction_e sess tid
		++ (fun trans ->
		      let tv = trans.ticket_verifier in
		      Nn_datanode_ctrl.enable_ticket_verifier_e tv
		      >> (fun st ->
			    dlogr (fun () ->
				     sprintf
				       "transaction tid=%Ld uses safetrans=%Ld"
				       tid
				       tv.Nn_datanode_ctrl.tv_safetrans_id
				  );
			    st
			 )
		      >> (fun st ->
			    match st with
			      | `Error(Nn_datanode_ctrl.Datanode_error(_,_)) ->
				  (* If we cannot contact a datanode to
				     start the transaction, map this to
				     EIO (better than System_err)
				   *)
				  `Error(Nn_state.Transaction_error `eio)
			      | _ -> st
			 )
		   )
	       )
	    )
      | Some _ ->
	  Netlog.logf `Err
	    "Error: begin_transaction but transaction %Ld already active" tid;
	  emit `efailed
  )

let proc_commit_transaction sess tid emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_commit_transaction tid=%Ld" tid);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let transtbl = get_trans_tbl() in
  let cid = Rpc_server.get_connection_id sess in
  let trans_opt =
    try
      Some(Hashtbl.find transtbl (cid,tid))
    with Not_found -> None in
  match trans_opt with
    | None ->
	Netlog.logf `Err
	  "Error: ENOTRANS - commit_transaction outside transaction";
	emit `enotrans

    | Some trans ->
	if trans.running then
	  emit `etbusy
	else (
	  trans.running <- true;
	  let sh = get_shared() in
	  fs_rpc_srv_reply_unit
	    ~anyway:(fun () -> 
		       trans.running <- false;
		       (* Disable tv at last (otherwise the bm release could
			  come before the bm commit)
			*)
		       Nn_datanode_ctrl.disable_ticket_verifier
			 trans.ticket_verifier
			 trans.bm_view#release;
		       if trans.pending_abort then (
			 let exists = Hashtbl.mem transtbl (cid,tid) in
			 if exists then
			   abort_trans cid tid trans;
			 Netlog.logf `Info
			   "Zombie transaction %Ld has been %s" 
			   tid
			   (if exists then "rolled back" else "committed");
			 trans.pending_abort <- false;
		       )
		    )
	    "commit_transaction"
	    sess 
	    emit
	    ( let (inodes_to_del, inodes_to_mark) =
		trans.inode_view#delayed_inode_deletes in
	      delete_inodes_e trans inodes_to_del
	      ++ (fun () ->
		    List.iter
		      (fun id -> trans.inode_view#inode_set_anonymous ~id)
		      inodes_to_mark;
		    Nn_commit.trans_e
		      sh 
		      [ (trans.bm_view :> Nn_state.view_t); 
			(trans.inode_view :> Nn_state.view_t) ]
		      (eps_e (`Done()) esys)
		      (sprintf "%Ld (peer %s)" tid (sess_peer sess))
		 )
	      >> (fun st ->
		    del_transaction cid tid;
		    ( match st with
			| `Done _ ->
			    if inodes_to_mark <> [] then
			      sh#set_zombie_inodes true;
			    dlogr
			      (fun () ->
				 sprintf
				   "Committed transaction %Ld (peer %s)"
				   tid (sess_peer sess));
			| _ ->
			    dlogr
			      (fun () ->
				 sprintf
				   "Fatal error in commit %Ld (peer %s)"
				   tid (sess_peer sess));
		    );
		    st
		 )
	    )
	)


let proc_abort_transaction sess tid emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_abort_transaction tid=%Ld" tid);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let transtbl = get_trans_tbl() in
  let cid = Rpc_server.get_connection_id sess in
  let trans_opt =
    try
      Some(Hashtbl.find transtbl (cid,tid))
    with Not_found -> None in
  match trans_opt with
    | None ->
	Netlog.logf `Err
	  "Error: ENOTRANS - abort_transaction outside transaction";
	emit `enotrans;

    | Some trans ->
	if trans.running then
	  (* FIXME: better we could allow this *)
	  emit `etbusy
	else (
	  dlogr
	    (fun () ->
	       sprintf "Aborted transaction %Ld (peer %s)"
		 tid (sess_peer sess));
	  trans.running <- true;
	  del_transaction cid tid;
	  trans.bm_view # rollback();
	  trans.inode_view # rollback();
	  
	  Nn_datanode_ctrl.disable_ticket_verifier
	    trans.ticket_verifier
	    trans.bm_view#release;

	  fs_rpc_srv_reply_unit
	    "abort_transaction"
	    sess 
	    (fun r -> 
	       trans.running <- false; 
	       if trans.pending_abort then (
		 Netlog.logf `Info
		   "Zombie transaction %Ld has been rolled back" tid;
		 trans.pending_abort <- false
	       );
	       emit r)
	    (eps_e (`Done (`Done ())) esys)
	)


let fs_onclose srv cid =
  (** If the TCP connection is closed, implicitly roll all transactions back *)
  let transtbl = get_trans_tbl() in
  let trans_l =
    Hashtbl.fold 
      (fun (cid1,tid1) wtrans acc ->
	 if cid1 = cid then
	   (tid1,wtrans) :: acc
	 else
	   acc
      )
      transtbl
      [] in
  List.iter
    (fun (tid,trans) ->
       let peer =
	 try Netsys.string_of_sockaddr
	   (Rpc_server.get_conn_peer_name cid)
	 with _ -> "n/a" in
       if trans.running then (
	 if not trans.pending_abort then (
	   Netlog.logf `Info
	     "Scheduling termination of zombie transaction %Ld (peer %s)"
	     tid peer;
	   trans.pending_abort <- true
	 )
       )
       else (
	 Netlog.logf `Info
	   "Rolling transaction %Ld back after TCP close (peer %s)" 
	   tid peer;
	 abort_trans cid tid trans
       )
    )
    trans_l;
  let ut = get_user_tbl() in
  Hashtbl.remove ut cid

(** The following RPC implementations are just transaction-wrapped
    incovations of the right methods in [Nn_state].
 *)

let have_permission mask creds ii =
  let module A = Pfs_rpcapi_aux in
  match creds with
    | `Root -> true
    | `Nobody -> false
    | `User cr ->
	if cr.user = ii.A.usergroup.A.user then (
	  (ii.A.mode land (mask lsl 6)) <> 0
	)
	else
	  if StrSet.mem ii.A.usergroup.A.group cr.supp_groups then (
	    (ii.A.mode land (mask lsl 3)) <> 0
	  )
	  else
	    (ii.A.mode land mask) <> 0

let have_read_permission = have_permission 4
let have_write_permission = have_permission 2
let have_exec_permission = have_permission 1

let have_owner_permission creds ii =
  match creds with
    | `Root -> true
    | `Nobody -> false
    | `User cr -> cr.user = ii.A.usergroup.A.user


let owner_restriction creds ii =
  if (ii.A.mode land 0o1000) <> 0 then
    match creds with
      | `Root -> None
      | `Nobody -> None
      | `User cr -> Some cr.user
  else
    None


let perm_ii creds ii =
  (* whether we have permission to create a new inode with this ii *)
  match creds with
    | `Nobody ->
	raise(Nn_state.Transaction_error `eperm)
    | `Root ->
	()
    | `User c ->
	if ii.A.usergroup.A.user <> c.user then
	  raise(Nn_state.Transaction_error `eperm);
	if not (StrSet.mem ii.A.usergroup.A.group c.supp_groups) then
	  raise(Nn_state.Transaction_error `eperm)


let perm_upd_ii creds old_ii new_ii =
  (* whether we have permission to change the inode *)
  let module A = Pfs_rpcapi_aux in
  match creds with
    | `Nobody ->
	raise(Nn_state.Transaction_error `eperm)
    | `Root ->
	()
    | `User c ->
	(* Nobody can change the owner or the type: *)
	if (old_ii.A.usergroup.A.user <> new_ii.A.usergroup.A.user ||
	      old_ii.A.filetype <> new_ii.A.filetype) then
	  raise(Nn_state.Transaction_error `eperm);
	(* Check for changes only the file owner can do: *)
	if (old_ii.A.usergroup.A.group <> new_ii.A.usergroup.A.group ||
	      old_ii.A.mode <> new_ii.A.mode ||
	      (old_ii.A.mtime <> new_ii.A.mtime && new_ii.A.mtime.A.tsecs >= 0L) ||
	      (old_ii.A.ctime <> new_ii.A.ctime && new_ii.A.ctime.A.tsecs >= 0L) ||
	      old_ii.A.replication <> new_ii.A.replication ||
	      old_ii.A.field1 <> new_ii.A.field1)
	then
	  perm_ii creds new_ii;
	(* Check for changes for which write access is sufficient *)
	if (old_ii.A.eof <> new_ii.A.eof ||
	      (old_ii.A.mtime <> new_ii.A.mtime && new_ii.A.mtime.A.tsecs < 0L) ||
	      (old_ii.A.ctime <> new_ii.A.ctime && new_ii.A.ctime.A.tsecs < 0L) ||
	      old_ii.A.create_verifier <> new_ii.A.create_verifier) then (
	  if not (have_write_permission (`User c) new_ii) then
	    raise(Nn_state.Transaction_error `eaccess)
	)


let fixup_ug_ii creds ii =
  match creds with
    | `Root ->
	if ii.A.usergroup.A.user = "" || ii.A.usergroup.A.group = "" then
	  raise(Nn_state.Transaction_error `einval);
	ii
    | `Nobody ->
	raise(Nn_state.Transaction_error `eperm)
    | `User u ->
	{ ii with
	    A.usergroup = { A.user = (if ii.A.usergroup.A.user = "" then
					u.user
				      else
					ii.A.usergroup.A.user);
			    A.group = (if ii.A.usergroup.A.group = "" then
					 u.group
				       else
					 ii.A.usergroup.A.group);
			  }
	}


let fixup_ii ii =
  let module A = Pfs_rpcapi_aux in
  let sh = get_shared() in
  if ii.A.mtime.A.tnsecs < 0 || ii.A.mtime.A.tnsecs >= 1_000_000_000 then
    raise(Nn_state.Transaction_error `einval);
  if ii.A.ctime.A.tnsecs < 0 || ii.A.ctime.A.tnsecs >= 1_000_000_000 then
    raise(Nn_state.Transaction_error `einval);
  if ii.A.replication < 0 then
    raise(Nn_state.Transaction_error `einval);
  if ii.A.eof < 0L then
    raise(Nn_state.Transaction_error `einval);
  let t0_lz = lazy(Unix.gettimeofday()) in
  { ii with
      A.mode = (if ii.A.filetype = `ftype_symlink then 0o777 else ii.A.mode);
      A.mtime = (if ii.A.mtime.A.tsecs < 0L then
		   Nn_state.encap_time (Lazy.force t0_lz)
		 else
		   ii.A.mtime);
      A.ctime = (if ii.A.ctime.A.tsecs < 0L then
		   Nn_state.encap_time (Lazy.force t0_lz)
		 else
		   ii.A.ctime);
      A.replication = (if ii.A.replication = 0 then
			 sh#node_config#nn_replication
		       else
			 ii.A.replication);
      A.seqno = 1L;   (* Used for inode creation only. For updates this value
			 is ignored anyway
		       *)
  }

let proc_allocate_inode sess (tid,ii) emit =
  (* sec *)
  let module A = Pfs_rpcapi_aux in
  dlogr
    (fun () -> sprintf "proc_allocate_inode tid=%Ld seqno=%Ld" tid ii.A.seqno);
  (get_shared()) # incr_count `RPC_filesystem;
  with_transaction 
    "allocate_inode" sess emit tid
    (fun trans ->
       if ii.A.seqno <> 0L then raise(Nn_state.Transaction_error `einval);
       let ii1 = fixup_ug_ii trans.creds ii in
       perm_ii trans.creds ii1;
       let ii2 = fixup_ii ii1 in
       trans.inode_view # inode_alloc_e ii2
    )

let proc_delete_inode sess (tid,id) emit =
  (* sec *)
  (* This operation is always permitted. The point is that you need the right
     to remove all file names in order to delete the inode. All permissions
     were already checked in conjunction with the file name removal.
   *)
  dlogr (fun () -> sprintf "proc_delete_inode tid=%Ld" tid);
  (get_shared()) # incr_count `RPC_filesystem;
  with_transaction_unit
    "delete_inode" sess emit tid
    (fun trans ->
       delete_inode_e trans id
    )

let proc_update_inodeinfo sess (tid,id,ii) emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_update_inodeinfo tid=%Ld inode=%Ld" tid id);
  (get_shared()) # incr_count `RPC_filesystem;
  with_transaction_unit
    "update_inodeinfo" sess emit tid
    (fun trans ->
       let ii1 = fixup_ug_ii trans.creds ii in
       let permission old_ii =
	 try
	   perm_upd_ii trans.creds old_ii ii1; None
	 with
	   | Nn_state.Transaction_error code -> Some code in 
       let ii2 = fixup_ii ii1 in
       trans.inode_view # inode_upd_e
	 ~id ~keep_blocklimit:true ~permission ii2
    )

let proc_get_inodeinfo sess (tid,id) emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_get_inodeinfo tid=%Ld inode=%Ld" tid id);
  (get_shared()) # incr_count `RPC_filesystem;
  with_transaction 
    "get_inodeinfo" sess emit tid
    (fun trans ->
       new Uq_engines.map_engine
	 ~map_done:(function
		      | None ->
			  `Error(Nn_state.Transaction_error `estale)
		      | Some ii ->
			  if have_owner_permission trans.creds ii ||
			     have_read_permission trans.creds ii
			  then
			    `Done ii
			  else
			    `Error (Nn_state.Transaction_error `eaccess)
		   )
	 (trans.inode_view # inode_get_e ~id)
    )

let proc_get_blocks sess (tid,inode,blkidx,len,req_seqno,pin_flag) emit = 
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_get_blocks tid=%Ld inode=%Ld idx=%Ld len=%Ld pin=%B"
	     tid inode blkidx len pin_flag);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "get_blocks" sess emit tid
    (fun trans ->
       if len < 0L then
	 raise(Nn_state.Transaction_error `einval);
       let len = min len (Int64.sub Int64.max_int blkidx) in
       trans.inode_view # inode_get_e 
	 ~id:inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	       | Some ii ->
		   (* don't use this ii - it may already be outdated! 
		      We check the permissions with this ii, though.
		      (CHECK: is this really acceptable?)
		    *)
		   if req_seqno > 0L && ii.A.seqno <> req_seqno then
		     raise(Nn_state.Transaction_error `econflict);
		   if not(have_read_permission trans.creds ii) then
		     raise(Nn_state.Transaction_error `eaccess);
		   eps_e (`Done()) esys
	  )
       ++ (fun () ->
	     trans.inode_view # inode_get_blocks_e 
	       ~id:inode ~blkidx ~len ~pin_flag trans.bm_view
	  )
       ++ (fun bis ->
	     if req_seqno > 0L then
	       (* Check whether ii changed in the meantime *)
	       trans.inode_view # inode_get_e ~id:inode
	       ++ (fun ii_opt ->
		     match ii_opt with
		       | None ->
			   eps_e
			     (`Error(Nn_state.Transaction_error `estale)) esys
		       | Some ii ->
			   if ii.A.seqno <> req_seqno then
			     eps_e
			       (`Error(Nn_state.Transaction_error `econflict))
			       esys
			   else
			     eps_e (`Done bis) esys
		  )
	     else
	       eps_e (`Done bis) esys
	  )
       ++ (fun bis ->
	     (* finally set node_alive and ticket *)
	     let tv = trans.ticket_verifier in
	     assert(tv.Nn_datanode_ctrl.tv_enabled);
	     let datanodes = Nn_datastore_news.list ~wait:true() in
	     let good_datanodes =
	       List.fold_left
		 (fun acc id ->
		    (* It is theoretically possible that the datanode just
		       came up, and the ticket is not yet valid for the node.
		       Drop just nodes. Also check whether the node is alive
		     *)
		    try
		      let ds =
			List.find
			  (fun ds -> ds.Nn_datastores.id = id) datanodes in
		      let idstr = ds.Nn_datastores.identity  in
		      if Nn_datastore_news.is_alive ~wait:true idstr then
			StrSet.add idstr acc
		      else 
			acc
		    with Not_found -> acc
		 )
		 StrSet.empty
		 tv.Nn_datanode_ctrl.tv_datanodes in
	     eps_e
	       (let bis' =
		  Array.map
		    (fun bi -> 
		       let node_alive =
			 StrSet.mem bi.Rpcapi_aux.identity good_datanodes in
		       if node_alive then (
			 if pin_flag then (
			   let vfy =
			     Plasma_util.compute_verifier 
			       tv.Nn_datanode_ctrl.tv_safetrans_id
			       tv.Nn_datanode_ctrl.tv_safetrans_sec
			       bi.Rpcapi_aux.block
			       bi.Rpcapi_aux.length
			       true
			       false in
			   dlogr
			     (fun () ->
				sprintf 
				  "compute_verifier id=%Ld sec=%Ld range=%Ld+%Ld rd=%B wr=%B v=%Ld"
				  tv.Nn_datanode_ctrl.tv_safetrans_id
				  tv.Nn_datanode_ctrl.tv_safetrans_sec
				  bi.Rpcapi_aux.block
				  bi.Rpcapi_aux.length
				  true
				  false
				  vfy);
			   let ticket =
			     { Rpcapi_aux.range_start = bi.Rpcapi_aux.block;
			       range_length = bi.Rpcapi_aux.length;
			       safetrans_id = tv.Nn_datanode_ctrl.tv_safetrans_id;
			       safetrans_tmo = tv.Nn_datanode_ctrl.tv_safetrans_tmo;
			       safetrans_vfy = vfy;
			       read_perm = true;
			       write_perm = false
			     } in
			   { bi with 
			       Rpcapi_aux.node_alive = true;
			       ticket = ticket;
			   }
			 )
			 else
			   { bi with 
			       Rpcapi_aux.node_alive = true;
			   }
		       )
		       else
			 { bi with 
			     Rpcapi_aux.node_alive = false
			 }
		    )
		    (Array.of_list
		       (Nn_blocklist.to_blockinfo_list bis)) in
		`Done bis'
	       )
	       esys
	  )
    )

let proc_allocate_blocks sess (tid,inode,blkidx,len,set_mtime,pref) emit = 
  (* sec *)
  dlogr (fun () -> sprintf "proc_allocate_blocks tid=%Ld inode=%Ld" tid inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "allocate_blocks" sess emit tid
    (fun trans ->
       if len < 0L || blkidx > Int64.sub Int64.max_int len then
	 raise(Nn_state.Transaction_error `einval);
       if len > Int64.of_int max_int then
	 raise(Nn_state.Transaction_error `einval);
       let tv = trans.ticket_verifier in
       let td =
	 { Nn_alloc_types.td_idlist = tv.Nn_datanode_ctrl.tv_datanodes;
	   td_safetrans_id = tv.Nn_datanode_ctrl.tv_safetrans_id;
	   td_safetrans_secret = tv.Nn_datanode_ctrl.tv_safetrans_sec;
	   td_safetrans_tmo = tv.Nn_datanode_ctrl.tv_safetrans_tmo;
	 } in
       (** Sequence of actions:
	   1. Get the inode (needed for repfactor)
	   2. Get list of blocks [old_bis] in the range (blkidx,len)
           3. Deallocate the [old_bis] blocks in the blockmap
           4. Allocate new blocks [new_bis] from the blockmap
           5. Remove [old_bis] from the inode list, add [new_bis]
	   6. Update inode if blocklimit has changed or [set_mtime]

           We need to lock early in step 1. Otherwise bad things can happen:
            - A competing transaction could also allocate blocks in the same
	      range, and if the transaction is committed between steps 2 and 3
	      we would deallocate the wrong blocklist
	    - A competing transaction could change the inode, and we
	      return the wrong seqno's. This confuses caches on the client
	      side.
	*)
       trans.inode_view # inode_get_with_lock_e ~id:inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	       | Some ii ->
		   dlog "allocate_blocks: found inode";
		   let r =
		     if have_write_permission trans.creds ii
		     then
		       `Done ii
		     else
		       `Error (Nn_state.Transaction_error `eaccess) in
		   eps_e r esys)
	 ++ (fun ii ->
	       trans.inode_view # inode_get_blocks_e
		 ~id:inode ~blkidx ~len ~pin_flag:false trans.bm_view
	       ++ (fun old_bis ->
		     blockmap_dealloc_e ~inode trans.bm_view old_bis
		  )
	       ++ (fun () ->
		     dlog "allocate_blocks: old blocks deallocated";
		     trans.inode_view # allocinfo_get_e ~id:inode
		  )
	       ++ (fun ai_blocks ->
		     dlog "allocate_blocks: got allocinfo";
		     let ri =
		       { Nn_blockmap.ri_blocks = ai_blocks
		       } in
		     trans.bm_view # blockmap_alloc_e 
		       ~inode
		       ~rpolicy:(`Balanced ii.Rpcapi_aux.replication)
		       ~index:blkidx
		       ~number:(Int64.to_int len)
		       ~pref:(Array.to_list pref)
		       td ri
		  )
	       ++ (fun new_bis ->
		     dlog "allocate_blocks: new blocks allocated";
		     let new_limit =
		       max 
			 ii.Rpcapi_aux.blocklimit
			 (Int64.add blkidx len) in
		     trans.inode_view # inode_upd_blocks_e 
		       ~id:inode
		       ~del:(blkidx, len)
		       ~add:new_bis
		       ~set_blocklimit:new_limit
		       ~set_mtime
		       ~permission:(fun _ -> None) (* already checked *)
		     ++ (fun _ ->
			   dlog "allocate_blocks: new blocks inserted";
			   let blocks_l =
			     Nn_blocklist.to_blockinfo_list new_bis in
			   let blocks_a =
			     Array.map
			       (fun bi ->
				  { bi with A.inode_seqno = ii.A.seqno } )
			       (Array.of_list blocks_l) in
			   eps_e (`Done blocks_a) esys
			)
		  )
	    )
    )


let proc_free_blocks sess (tid,inode,blkidx,len,set_mtime) emit = 
  (* sec *)
  dlogr (fun () -> sprintf "proc_free_blocks tid=%Ld inode=%Ld" tid inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "free_blocks" sess emit tid
    (fun trans ->
       if len < 0L then
	 raise(Nn_state.Transaction_error `einval);
       let len = min len (Int64.sub Int64.max_int blkidx) in
       (** Sequence of actions:
	   1. Get list of blocks [old_bis] in the range (blkidx,len)
           2. Deallocate the [old_bis] blocks in the blockmap
           3. Remove [old_bis] from the inode list
	*)
       trans.inode_view # inode_get_with_lock_e ~id:inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	       | Some ii ->
		   let r =
		     if have_write_permission trans.creds ii
		     then
		       `Done ii
		     else
		       `Error (Nn_state.Transaction_error `eaccess) in
		   eps_e r esys
	  )
       ++ (fun ii ->
	     trans.inode_view # inode_get_blocks_e
	       ~id:inode ~blkidx ~len ~pin_flag:false trans.bm_view)
       ++ (fun old_bis ->
	     blockmap_dealloc_e ~inode trans.bm_view old_bis)
       ++ (fun () ->
	     trans.inode_view # inode_upd_blocks_e 
	       ~id:inode
	       ~del:(blkidx, len)
	       ~add:Nn_blocklist.empty
	       ~set_blocklimit:(-1L)
	       ~set_mtime
	       ~permission:(fun _ -> None)   (* already checked *)
	  )
    )
  

let proc_get_fsstat sess _ emit =
  dlogr (fun () -> sprintf "proc_get_fsstat");
  let sh = get_shared() in
  sh # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in

  if get_creds sess = `Nobody then
    emit `eperm
  else (
    let ds_list = Nn_datastore_news.list ~wait:true () in
    let ds_enabled = 
      List.filter (fun ds -> ds.Nn_datastores.enabled) ds_list in
    let ds_alive =
      List.filter (fun ds -> ds.Nn_datastores.alive) ds_enabled in
    let ds_dead =
      List.filter (fun ds -> not ds.Nn_datastores.alive) ds_enabled in
    let idlist =
      List.map (fun ds -> ds.Nn_datastores.identity) ds_alive in
    let dead_list =
      List.map
	(fun ds -> 
	   sprintf "%s (%s)"
	     ds.Nn_datastores.identity
	     ( match ds.Nn_datastores.node with
		 | None -> "n/a"
		 | Some sa -> 
		     let (h,p) = Plasma_util.host_port_of_sockaddr sa in
		     sprintf "%s:%d" h p
	     )
	)
	ds_dead in
  
    let total = ref 0L in
    let used = ref 0L in
    let trans = ref 0L in
    
    let bm_view = new Nn_state.bm_view sh in
    let e =
      Uq_engines.msync_engine
	(List.map (fun id -> bm_view # blockmap_get_e ~identity:id) idlist)
	(fun bm () ->
	   if bm#active then (
	     total := Int64.add !total bm#size;
	     let (n_used,n_trans,n_free) = bm#stats in
	     used := Int64.add !used n_used;
	     trans := Int64.add !trans n_trans
	   )       
	)
      ()
	esys in
    fs_rpc_srv_reply
    "get_fsstat"
      sess emit 
      (fake_trans
	 (e ++
	    (fun () -> 
	       let r =
		 { Pfs_rpcapi_aux.total_blocks = !total;
		   used_blocks = !used;
		   trans_blocks = !trans;
		   enabled_datanodes = List.length ds_enabled;
		   alive_datanodes = List.length ds_alive;
		   dead_datanodes = Array.of_list dead_list;
		 } in
	       
	       eps_e (`Done r) esys
	    )))
  )

let proc_rereplicate sess _ emit = assert false


let slash_re = Pcre.regexp "[/]+"

let split_path ?(strip_trailing_slashes=false) p =
  (* empty string: []
     "/": [""]
     absolute path: first element of list is ""
   *)
  if p = "" then
    []
  else
    let max =
      if strip_trailing_slashes then 0 else (-1) in
    let l =
      Pcre.split ~rex:slash_re ~max p in
    if l = [] then  (* "/" *)
      [ "" ]
    else
      l


let rec lookup_path_e trans inode ii path loops symbolic =
  (* walks along relative path starting at inode *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  match path with
    | [] ->
	eps_e (`Done(inode, ii)) esys
    | fn :: path' ->
	if ii.Pfs_rpcapi_aux.filetype <> `ftype_directory then
	  eps_e (`Error (trans_error `ebadpath)) esys
	else (
	  match fn with
	    | "." | "" ->
		(* stay in the same directory *)
		lookup_path_e trans inode ii path' loops symbolic
	    | ".." ->
		(* get the parent directory *)
		trans.inode_view # filename_parent_dir_e ~inode
		++ (fun parent_inode_opt ->
		      match parent_inode_opt with
			| None ->
			    eps_e (`Error (trans_error `enoent)) esys
			| Some parent_inode ->
			    trans.inode_view # inode_get_e ~id:parent_inode
			    ++ (fun parent_ii_opt ->
				  match parent_ii_opt with
				    | None ->
					eps_e 
					  (`Error (trans_error `enoent)) esys
				    | Some parent_ii ->
					lookup_path_e 
					  trans parent_inode parent_ii
					  path' loops symbolic
			       )
		   )
	    | _ ->
		(* lookup the path component *)
		trans.inode_view # filename_get_e ~dir_inode:inode ~name:fn
		++ (fun sub_inode_opt ->
		      match sub_inode_opt with
			| None ->
			    eps_e (`Error (trans_error `enoent)) esys
			| Some sub_inode ->
			    trans.inode_view # inode_get_e ~id:sub_inode
			    ++ (fun sub_ii_opt ->
				  match sub_ii_opt with
				    | None ->
					eps_e
					  (`Error (trans_error `enoent)) esys
				    | Some sub_ii ->
					(match sub_ii.Pfs_rpcapi_aux.filetype with
					   | `ftype_directory | `ftype_regular->
					       lookup_path_e
						 trans sub_inode sub_ii
						 path' loops symbolic
					   | `ftype_symlink ->
					       if symbolic && path' = [] then
						 eps_e
						   (`Done(sub_inode, sub_ii))
						   esys
					       else
						 lookup_symlink_e
						   trans inode ii
						   sub_inode sub_ii path'
						   loops symbolic
					)
			       )
		   )
	)

and lookup_abs_e trans path loops symbolic =
  (* "/" has the fixed inode 1 *)
  trans.inode_view # inode_get_e ~id:1L
  ++ (fun root_ii_opt ->
	match root_ii_opt with
	  | None ->
	      assert false
	  | Some root_ii ->
	      lookup_path_e trans 1L root_ii path loops symbolic 
     )

and lookup_symlink_e trans inode ii sym_inode sym_ii path' loops symbolic =
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let target =
    split_path sym_ii.Pfs_rpcapi_aux.field1 in
  if target = [] then
    eps_e (`Error (trans_error `enoent)) esys
  else
    if !loops >= 100 then
      eps_e (`Error (trans_error `eloop)) esys
    else (
      incr loops;
      if List.hd target = "" then
	lookup_abs_e trans (List.tl target @ path') loops symbolic
      else
	lookup_path_e trans inode ii (target @ path') loops symbolic 
    )
	
let lookup_e trans inode pathname symbolic =
  (* returns (inode,ii), or ENOENT *)
  (* inode is only used if pathname is non-absolute *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let path = split_path pathname in
  match path with
    | "" :: path' ->
	lookup_abs_e trans path' (ref 0) symbolic
    | _ ->
	trans.inode_view # inode_get_e ~id:inode
	++ (fun ii_opt ->
	      match ii_opt with
		| None ->
		    eps_e (`Error (trans_error `enoent)) esys
		| Some ii ->
		    lookup_path_e trans inode ii path (ref 0) symbolic
	   )

let no_slash_slash n =
  not(Pcre.pmatch ~pat:"//" n)

let no_trailing_slash n =
  n = "" || n = "/" || n.[ String.length n - 1 ] <> '/'

let check_name n =
  (** Check whether [n] is a good filename *)
  n <> "" && n.[0] = '/' && no_slash_slash n && no_trailing_slash n

let get_parent n =
  (** Return the parent name *)
  try
    if n = "/" then raise Not_found;
    let p = String.rindex n '/' in
    if p=0 then "/" else String.sub n 0 p
  with
    | Not_found ->
	failwith "Nn_coord.get_parent"


let proc_fs_lookup sess (tid,dir_inode,path,symbolic) emit = 
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_fs_lookup tid=%Ld dir_inode=%Ld path=%s sym=%B" 
	     tid dir_inode path symbolic);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "lookup" sess emit tid
    (fun trans ->
       lookup_e trans dir_inode path symbolic
       ++ (fun (inode, ii) ->
	     dlogr (fun () -> 
		      sprintf "result proc_fs_lookup tid=%Ld dir_inode=%Ld path=%s sym=%B inode=%Ld" 
			tid dir_inode path symbolic inode);
	     eps_e (`Done inode) esys
	  )
    )


let rec rev_lookup_e trans dir_inode name acc =
  (* Translates the file identified by the dir_inode and the name member
     into an absolute path. Call with acc=[]
   *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  if dir_inode = 1L then (
    let path =
      "/" ^ String.concat "/" (name :: acc) in
    eps_e (`Done (Some path)) esys
  )
  else
    trans.inode_view # inode_get_e ~id:dir_inode
    ++ (fun ii_opt ->
	  match ii_opt with
	    | None ->
		eps_e (`Error (trans_error `estale)) esys
	    | Some ii ->
		if not(have_read_permission trans.creds ii) then
		  eps_e (`Error (trans_error `eaccess)) esys
		else
		  trans.inode_view # filename_rev_get_e ~inode:dir_inode
		  ++ (fun dir_names ->
			match dir_names with
			  | [] ->
			      eps_e (`Done None) esys
			  | [dir_dir_inode, dir_name] ->
			      rev_lookup_e 
				trans dir_dir_inode dir_name (name :: acc)
			  | _ ->
			      eps_e (`Error (trans_error `efailed)) esys
		     )
       )



let proc_rev_lookup sess (tid,inode) emit = 
  (* sec *)
  dlogr (fun () -> sprintf "proc_rev_lookup tid=%Ld inode=%Ld" tid inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "rev_lookup" sess emit tid
    (fun trans ->
       if inode=1L then
	 eps_e (`Done [| "/" |]) esys
       else
	 trans.inode_view # filename_rev_get_e ~inode
	 ++ (fun spots ->
	       Uq_engines.msync_engine
		 (List.map
		    (fun (dir_inode,name) ->
		       rev_lookup_e trans dir_inode name []
		    )
		    spots)
		 (fun path_opt acc ->
		    match path_opt with
		      | None -> acc
		      | Some path -> path :: acc )
		 []
		 esys
	       ++ (fun paths ->
		     eps_e (`Done(Array.of_list paths)) esys
		  )
	    )
    )


let proc_rev_lookup_dir sess (tid,inode) emit = 
  (* sec *)
  dlogr (fun () -> sprintf "proc_rev_lookup_dir tid=%Ld inode=%Ld" tid inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "rev_lookup_dir" sess emit tid
    (fun trans ->
       if inode=1L then
	 eps_e (`Done "/" ) esys
       else
	 trans.inode_view # filename_rev_get_dir_e ~inode
	 ++ (fun (dir_inode,name,_) ->
	       rev_lookup_e trans dir_inode name []
	       ++ (fun path_opt ->
		     match path_opt with
		       | None ->
			   eps_e (`Error (trans_error `enoent)) esys
		       | Some path ->
			   eps_e (`Done path) esys
		  )
	    )
    )
    

let proc_link_count sess (tid,inode) emit = 
  (* sec: no checks right now - the number of links is "public" *)
  dlogr (fun () -> sprintf "proc_link_count tid=%Ld inode=%Ld" tid inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "link_count" sess emit tid
    (fun trans ->
       trans.inode_view # filename_count_e ~inode
       ++ (fun n ->
	     eps_e (`Done n) esys
	  )
    )


let proc_list sess (tid,inode) emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_list tid=%Ld inode=%Ld" tid inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction 
    "list" sess emit tid
    (fun trans ->
       trans.inode_view # inode_get_e ~id:inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	       | Some ii ->
		   if not(have_read_permission trans.creds ii) then
		     eps_e (`Error (trans_error `eaccess)) esys
		   else
		     let ft = ii.Pfs_rpcapi_aux.filetype in
		     match ft with
		       | `ftype_regular | `ftype_symlink ->
			   eps_e
			     (`Error(Nn_state.Transaction_error `enotdir)) esys
		       | `ftype_directory ->
			   trans.inode_view # filename_list_e ~dir_inode:inode
			   ++ (fun entries ->
				 let rentries =
				   List.map
				     (fun (n,i) ->
					{ Pfs_rpcapi_aux.entry_name = n;
					  entry_inode = i
					}
				     )
				     entries in
				 eps_e (`Done (Array.of_list rentries)) esys
			      )
	  )
    )


let update_mtime trans inode =
  (** Update the mtime of the parent by adding a special
      db instruction: (This avoids that we have to lock the
      parent inode)
   *)
  let now = Unix.gettimeofday() in
  trans.inode_view # inode_upd_time
    ~id:inode
    ~mtime:(Some(Nn_state.encap_time now))
    ~ctime:None


let proc_link_at sess (tid,dir_inode,name,inode) emit =
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_link_at tid=%Ld dir_inode=%Ld name=%s inode=%Ld" 
	     tid dir_inode name inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "link_at" sess emit tid
    (fun trans ->
       if String.contains name '/' then
	 raise(Nn_state.Transaction_error `einval);
       trans.inode_view # inode_get_e ~id:dir_inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	       | Some ii ->
		   if not(have_write_permission trans.creds ii) then
		     eps_e (`Error (trans_error `eaccess)) esys
		   else
		     trans.inode_view # filename_link_e ~dir_inode ~name ~inode
		     ++ (fun () ->
			   update_mtime trans dir_inode;
			   eps_e (`Done ()) esys
			)
	  )
    )


let rec split_last_off_r path =
  match path with
    | [] ->
	assert false
    | [last] ->
	[], last
    | x :: path' ->
	let (p,last) = split_last_off_r path' in
	(x :: p), last

let split_last_off path =
  (* For an absolute path <> "/" this function returns (dir,name) where
     name is the last component, and dir is the preceding part of the path.
     Raises Transaction_error in case of errors.
   *)
  match path with
    | [] ->
	raise(Nn_state.Transaction_error `einval)
    | "" :: _ :: _ ->
	split_last_off_r path
    | _ ->
	raise(Nn_state.Transaction_error `einval)


let proc_link sess (tid,path,inode) emit =
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_link tid=%Ld path=%s inode=%Ld" tid path inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "link" sess emit tid
    (fun trans ->
       (* FIXME: Be more POSIX-compatible: allow the trailing slash when
	  linking a directory
	*)
       let p = split_path path in
       let dir_p, name = split_last_off p in
       if name = "" then raise(Nn_state.Transaction_error `einval);
       let dir_path =
	 if dir_p = [ "" ] then "/" else String.concat "/" dir_p in
       lookup_e trans (-1L) dir_path false
       ++ (fun (dir_inode, dir_ii) ->
	     if not(have_write_permission trans.creds dir_ii) then
	       eps_e (`Error (trans_error `eaccess)) esys
	     else
	       trans.inode_view # filename_link_e ~dir_inode ~name ~inode
	       ++ (fun () ->
		     update_mtime trans dir_inode;
		     eps_e (`Done ()) esys
		  )
	  )
    )


let proc_unlink_at sess (tid, dir_inode, name) emit =
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_unlink_at tid=%Ld dir_inode=%Ld name=%s"
	     tid dir_inode name);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "unlink_at" sess emit tid
    (fun trans ->
       trans.inode_view # inode_get_e ~id:dir_inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error(Nn_state.Transaction_error `estale)) esys
	       | Some ii ->
		   if not(have_write_permission trans.creds ii) then
		     eps_e (`Error (trans_error `eaccess)) esys
		   else
		     let owner_restr = owner_restriction trans.creds ii in
		     trans.inode_view # filename_unlink_e
		       ~dir_inode ~name ~owner_restr
		     ++ (fun () ->
			   update_mtime trans dir_inode;
			   eps_e (`Done ()) esys
			)
	  )
    )


let proc_unlink sess (tid,path) emit = 
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_unlink tid=%Ld path=%s" tid path);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "unlink" sess emit tid
    (fun trans ->
       (* FIXME: Be more POSIX-compatible: allow the trailing slash when
	  unlinking a directory
	*)
       let p = split_path path in
       let dir_p, name = split_last_off p in
       if name = "" then raise(Nn_state.Transaction_error `einval);
       let dir_path =
	 if dir_p = [ "" ] then "/" else String.concat "/" dir_p in
       lookup_e trans (-1L) dir_path false
       ++ (fun (dir_inode, dir_ii) ->
	     if not(have_write_permission trans.creds dir_ii) then
	       eps_e (`Error (trans_error `eaccess)) esys
	     else
	       let owner_restr = owner_restriction trans.creds dir_ii in
	       trans.inode_view # filename_unlink_e
		 ~dir_inode ~name ~owner_restr
	       ++ (fun () ->
		     update_mtime trans dir_inode;
		     eps_e (`Done ()) esys
		  )
	  )
    )


let proc_rename_at sess (tid,o_dir_inode,o_name,n_dir_inode,n_name) emit =
  (* sec *)
  dlogr (fun () -> 
	   sprintf
	     "proc_rename_at tid=%Ld old_dir_inode=%Ld old_name=%s new_dir_inode=%Ld new_name=%s" 
	     tid o_dir_inode o_name n_dir_inode n_name);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "rename_at" sess emit tid
    (fun trans ->
       if String.contains n_name '/' then
	 raise(Nn_state.Transaction_error `einval);
       trans.inode_view # inode_get_e ~id:o_dir_inode
       ++ (fun o_ii_opt ->
	     trans.inode_view # inode_get_e ~id:n_dir_inode
	     ++ (fun n_ii_opt ->
		   match (o_ii_opt, n_ii_opt) with
		     | (Some o_ii, Some n_ii) ->
			 if not(have_write_permission trans.creds o_ii) ||
			   not(have_write_permission trans.creds n_ii) 
			 then
			   eps_e (`Error (trans_error `eaccess)) esys
			 else
			   let owner_restr = 
			     owner_restriction trans.creds o_ii in
			   trans.inode_view # filename_rename_e
			     ~from_dir_inode:o_dir_inode
			     ~from_name:o_name
			     ~to_dir_inode:n_dir_inode
			     ~to_name:n_name
			     ~owner_restr
			   ++ (fun () ->
				 update_mtime trans o_dir_inode;
				 update_mtime trans n_dir_inode;
				 eps_e (`Done ()) esys
			      )
		     | _ ->
			 eps_e (`Error (trans_error `estale)) esys
		)
	  )
    )


let proc_rename sess (tid,o_path,n_path) emit = 
  (* sec *)
  dlogr (fun () -> 
	   sprintf "proc_rename tid=%Ld old_path=%s new_path=%s" 
	     tid o_path n_path);
  (get_shared()) # incr_count `RPC_filesystem;
  let esys = (Netplex_cenv.self_cont()) # event_system in
  with_transaction_unit
    "rename" sess emit tid
    (fun trans ->
       (* FIXME: Be more POSIX-compatible: allow the trailing slash when
	  renaming a directory (in o_path only)
	*)
       let o_p = split_path o_path in
       let o_dir_p, o_name = split_last_off o_p in
       if o_name = "" then raise(Nn_state.Transaction_error `einval);
       let o_dir_path =
	 if o_dir_p = [ "" ] then "/" else String.concat "/" o_dir_p in
       let n_p = split_path n_path in
       let n_dir_p, n_name = split_last_off n_p in
       if n_name = "" then raise(Nn_state.Transaction_error `einval);
       let n_dir_path =
	 if n_dir_p = [ "" ] then "/" else String.concat "/" n_dir_p in
       lookup_e trans (-1L) o_dir_path false
       ++ (fun (o_dir_inode, o_ii) ->
	     lookup_e trans (-1L) n_dir_path false
	     ++ (fun (n_dir_inode, n_ii) ->
		   if not(have_write_permission trans.creds o_ii) ||
		     not(have_write_permission trans.creds n_ii) 
		   then
		     eps_e (`Error (trans_error `eaccess)) esys
		   else
		     let owner_restr = owner_restriction trans.creds o_ii in
		     trans.inode_view # filename_rename_e
		       ~from_dir_inode:o_dir_inode
		       ~from_name:o_name
		       ~to_dir_inode:n_dir_inode
		       ~to_name:n_name
		       ~owner_restr
		     ++ (fun () ->
			   update_mtime trans o_dir_inode;
			   update_mtime trans n_dir_inode;
			   eps_e (`Done ()) esys
			)
		)
	  )
    )
    

let proc_namelock sess (tid,dir_inode,name) emit =
  dlogr (fun () -> 
	   sprintf "proc_namelock tid=%Ld dir_inode=%Ld name=%s" 
	     tid dir_inode name);
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  sh # incr_count `RPC_filesystem;
  with_transaction_unit
    "namelock" sess emit tid
    (fun trans ->
       if String.contains name '/' then
	 raise(Nn_state.Transaction_error `einval);
       trans.inode_view # inode_get_e ~id:dir_inode
       ++ (fun ii_opt ->
	     match ii_opt with
	       | None ->
		   eps_e (`Error (trans_error `estale)) esys
	       | Some ii ->
		   if not(have_read_permission trans.creds ii) then
		     raise(Nn_state.Transaction_error `eaccess);
		   let owner = trans.inode_view # as_owner in
		   match sh # lock_filename (dir_inode,name) `Exists owner  with
		     | Some unlock ->
			 trans.inode_view # filename_get_e ~dir_inode ~name
			 ++ (fun inode_opt ->
			       match inode_opt with
				 | None -> 
				     eps_e (`Error (trans_error `enoent)) esys
				 | Some inode -> eps_e (`Done ()) esys
			    )
			   >> (fun st ->
				 ( match st with
				     | `Error _ | `Aborted -> unlock()
				     | _ -> ()
				 );
				 st
			      )
		     | None ->
			 raise(Nn_state.Transaction_error `econflict)
	  )
    )

    
let proc_set_block_checksum sess _ emit = assert false
let proc_iterate sess _ emit = assert false


let impersonate_without_ticket sess user group supp_groups_set =
  match get_creds sess with
    | `Nobody ->
	(* nobody is not allowed to impersonate without ticket *)
	false
    | `Root ->
	(* root may do anything *)
	set_creds sess user group supp_groups_set;
	true
    | `User old_creds ->
	(* it is only possible to change the groups *)
	(user = old_creds.user) &&
	  (StrSet.mem group old_creds.supp_groups) &&
	  (StrSet.subset supp_groups_set old_creds.supp_groups) &&
	  (set_creds sess user group supp_groups_set;
	   true
	  )


let impersonate_with_ticket sess user group supp_groups_set ticket =
  let open Unix in
  try
    let verifier = Plasma_ug.verifier_of_auth_ticket ticket in
    let (p_pw,p_group,p_supp_groups) = Nn_authticket.principal verifier in
    let p_supp_groups' = StrSet.add p_group p_supp_groups in
    dlog "principal found";
    (user = p_pw.pw_name) &&
      (StrSet.mem group p_supp_groups') &&
      (StrSet.subset supp_groups_set p_supp_groups') &&
      (set_creds sess user group supp_groups_set;
       true
      )
  with
    | Not_found ->
	dlog "principal not found";
	false
    | error ->
	Netlog.logf `Warning
	  "Exception in proc_impersonate: %s"
	  (Netexn.to_string error);
	false


let proc_impersonate sess (user,group,supp_groups,auth_ticket_opt,delete_flag)
                     emit =
  (* sec *)
  dlogr (fun () ->
	   sprintf "proc_impersonate user=%s" user);
  (get_shared()) # incr_count `RPC_filesystem;
  let supp_groups_set = 
    List.fold_left
      (fun acc g -> StrSet.add g acc)
      StrSet.empty
      (Array.to_list supp_groups) in
  ( match auth_ticket_opt with
      | None ->
	  let ok = 
	    impersonate_without_ticket sess user group supp_groups_set in
	  emit ok
      | Some ticket ->
	  let ok = 
	    impersonate_with_ticket sess user group supp_groups_set ticket in
	  if delete_flag then (
	    try
	      let verifier = Plasma_ug.verifier_of_auth_ticket ticket in
	      Nn_authticket.delete verifier;
	    with _ -> ()
	  );
	  emit ok;
  )
  

let proc_get_auth_ticket sess user emit =
  (* sec *)
  let sh = get_shared() in
  sh # incr_count `RPC_filesystem;
  try
    ( match get_creds sess with
	| `Nobody -> raise Not_found
	| `User u -> if u.user <> user then raise Not_found
	| `Root -> ()
    );
    let verifier = Nn_authticket.create user in  (* or Not_found *)
    let (pw, mg, sg) = Nn_authticket.principal verifier in
    let ticket =
      Plasma_ug.serialize_auth_ticket
	~rpc_user:Pfs_auth.bottom_user
	~rpc_password:(sh#cauth).Pfs_auth.pnobody_pw
	~user
	~group:mg
	~supp_groups:sg
	~verifier in
    emit (Some ticket)
  with
    | Not_found -> emit None
    

let proc_renew_auth_ticket sess auth_ticket emit =
  (* sec *)
  (get_shared()) # incr_count `RPC_filesystem;
  try
    let verifier = Plasma_ug.verifier_of_auth_ticket auth_ticket in
    Nn_authticket.renew verifier;
    emit ()
  with
    | error ->
	Netlog.logf `Warning
	  "Exception in Nn_coord.proc_renew_auth_ticket: %s"
	  (Netexn.to_string error);
	emit ()


let proc_read_admin_table sess key emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_read_admin_table key=%s" key);
  (get_shared()) # incr_count `RPC_filesystem;
  if get_creds sess = `Nobody then
    emit `eperm
  else 
    fs_rpc_srv_reply
      "read_admin_table"
      sess emit
      (Uq_engines.meta_engine
	 (Nn_db.admin_table_get_ro_e ~key)
      )

let proc_write_admin_table sess (key,contents) emit =
  (* sec *)
  dlogr (fun () -> sprintf "proc_write_admin_table key=%s" key);
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  sh # incr_count `RPC_filesystem;
  if get_creds sess <> `Root then
    emit `eperm
  else 
    let old_uga = Nn_ug.get_ug_files() in
    let new_uga =
      List.map
	(fun (k,c) -> if k=key then (k,contents) else (k,c))
	old_uga in
    try
      let v = new Nn_state.ds_view sh in
      let e = 
	Nn_commit.trans_e 
	  sh [ (v :> Nn_state.view_t) ]
	  (v#admin_table_put_e ~key ~contents)
	  "proc_write_admin_table"
	++ (fun _ ->
	      Nn_ug.set_ug_files new_uga;
	      eps_e (`Done(`Done ())) esys
	   ) in
      fs_rpc_srv_reply_unit "write_admin_table" sess emit e
    with
      | error ->
	  Netlog.logf `Crit
	    "Exception in proc_write_admin_table: %s"
	    (Netexn.to_string error);
	  emit `efailed


let proc_is_up_to_date sess (inode,ii) emit =
  dlogr (fun () -> sprintf "proc_is_up_to_date(%Ld)" inode);
  (get_shared()) # incr_count `RPC_filesystem;
  let creds = get_creds sess in
  try
    let ii_cache = Nn_inodecache.lookup inode in
    if not (have_owner_permission creds ii_cache) &&
       not (have_read_permission creds ii_cache)
    then
      raise Not_found;
    emit (ii_cache = ii)
  with
    | Not_found ->
	emit false


let proc_is_up_to_date_seqno sess (inode,seqno) emit =
  dlogr (fun () -> sprintf "proc_is_up_to_date inode=%Ld seqno=%Ld" 
	   inode seqno);
  (get_shared()) # incr_count `RPC_filesystem;
  let creds = get_creds sess in
  try
    let ii_cache = Nn_inodecache.lookup inode in
    if not (have_owner_permission creds ii_cache) &&
       not (have_read_permission creds ii_cache)
    then
      raise Not_found;
    emit (ii_cache.A.seqno = seqno)
  with
    | Not_found ->
	emit false


let proc_get_params sess () emit =
  dlogr (fun () -> "proc_get_params");
  (get_shared()) # incr_count `RPC_filesystem;
  let _creds = get_creds sess in
  let conf = Node_config_var.get cfgname in
  let params =
    [| { A.name = "clustername";
	 A.value = conf#nn_clustername;
       };
       { A.name = "coordinator";
	 A.value = conf#nn_hostname;
       };
       { A.name = "blocksize";
	 A.value = string_of_int conf#dn_blocksize;
       };
       { A.name = "lock_timeout";
	 A.value = string_of_int (truncate conf#nn_lock_timeout);
       };
       { A.name = "replication";
	 A.value = string_of_int conf#nn_replication;
       };
       { A.name = "data_security_level";
	 A.value = Pfs_auth.string_of_level conf#dn_security_level;
       };
       { A.name = "data_timeout";
	 A.value = string_of_int(truncate(conf#dn_node_timeout)) ;
       };
    |] in
  emit params



let proc_get_dn_info sess () emit =
  (** Do the lookup (similar to [proc_is_enabled]): *)
  dlogr (fun () -> "proc_get_dn_info");
  (get_shared()) # incr_count `RPC_filesystem;
  let _creds = get_creds sess in
  Nn_datastore_news.force();
  emit
    (Array.of_list
       (List.flatten
	  (List.map
	   (fun ds -> 
	      let open Nn_datastores in
	      match ds.node with
		| None -> []
		| Some node ->
		    if ds.enabled && ds.alive then
		      [ { A.dn_identity = ds.identity;
			  dn_size = ds.size;
			  dn_node = Plasma_util.string_of_sockaddr node;
			}
		      ]
		    else
		      []
	   )
	   (Nn_datastore_news.list())
	  )
       )
    )

let proc_get_blocksize nn_conf sess () emit =
  dlogr (fun () -> "proc_get_blocksize");
  (get_shared()) # incr_count `RPC_filesystem;
  emit nn_conf#dn_blocksize


(**********************************************************************)
(* Scanner for zombie inodes                                          *)
(**********************************************************************)

let delete_zombie_inodes_e() =
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let sh = get_shared() in
  sh # incr_count `Transactions_total;
  let trans = unmanaged_transaction `Root in
  sh#set_zombie_inodes false;
  Nn_db.inode_get_anonymous_ro_e ~n:max_int
  ++ (fun l ->
	if l = [] then (
	  eps_e (`Done()) esys
	) else (

	  sh#set_zombie_inodes true;  (* there could be more to do... *)

	  let l' =
	    List.filter
	      (fun inode -> 
		 try sh#inode_use_counts inode = 0
		 with Not_found -> true
	      )
	      l in

	  if l' = [] then (
	    eps_e (`Done()) esys
	  ) else (
	    List.iter
	      (fun inode ->
		 trans.inode_view # inode_set_used inode
	      )
	      l';
	    
	    delete_inodes_e trans l'
	    ++ (fun () ->
		  Nn_commit.trans_e
		    sh 
		    [ (trans.bm_view :> Nn_state.view_t); 
		      (trans.inode_view :> Nn_state.view_t) ]
		    (eps_e (`Done()) esys)
		    "INTERNAL (delete_zombie_inodes_e)"
		  >> (fun st ->
			match st with
			  | `Done st' -> st'
			  | `Error err -> `Error err
			  | `Aborted -> `Aborted
		     )
	       )
	    >> (fun st ->
		  match st with
		    | `Done _ -> st
		    | _ ->
			trans.bm_view # rollback();
			trans.inode_view # rollback();
			st
	       )
	  )
	)
     )

(**********************************************************************)
(* Namesync                                                           *)
(**********************************************************************)

let pack_dump = Nn_sync.pack_dump
let unpack_dump = Nn_sync.unpack_dump

let proc_create_checkpoint sess () emit =
  dlogr (fun () -> "proc_create_checkpoint");
  let conf = Node_config_var.get cfgname in
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let code = Nn_sync.checkpoint conf esys in
  match code with
    | `Running -> emit `econflict
    | `Blocked -> emit `eperm
    | `Dump d ->
        let d1 = pack_dump d in
        emit (`ok d1)


let proc_get_checkpointing_status sess dump emit =
  dlogr (fun () -> "proc_get_checkpointing_status");
  let d = unpack_dump dump in
  let conf = Node_config_var.get cfgname in
  let st = Nn_sync.get_dump_status conf d in
  match st with
    | `Ok -> emit `ok
    | `Running -> emit `etbusy
    | `Error -> emit `efailed
    | `Stale -> emit `estale
    | `Not_found -> emit `enoent


let proc_block_checkpointing sess ts emit =
  dlogr (fun () -> "proc_block_checkpointing");
  assert false (* TODO *)


let proc_create_log_dump sess (cp_dump, min_id) emit =
  dlogr (fun () -> "proc_create_log_dump");
  assert false (* TODO *)


let proc_get_log_dumping_status sess (dump) emit =
  dlogr (fun () -> "proc_get_log_dumping_status");
  assert false (* TODO *)


let proc_list_dumps sess (pat_dump) emit =
  dlogr (fun () -> "proc_list_dumps");
  let conf = Node_config_var.get cfgname in
  let l1 = Nn_dumps.list_dumps conf in
  let l2 = List.map pack_dump l1 in
  let l3 =
    List.filter
      (fun d ->
         let open Pfs_rpcapi_aux in
         (pat_dump.dump_prefix = "" ||
            pat_dump.dump_prefix = d.dump_prefix) &&
         (pat_dump.dump_ref = "" ||
            pat_dump.dump_ref = d.dump_ref) &&
         (pat_dump.dump_suffix = "" ||
            pat_dump.dump_suffix = d.dump_suffix) &&
         (pat_dump.dump_ts = 0L ||
            (pat_dump.dump_ts > 0L && d.dump_ts >= pat_dump.dump_ts) ||
            (pat_dump.dump_ts < 0L && d.dump_ts < Int64.neg pat_dump.dump_ts))
      )
      l2 in
  emit (Array.of_list l3)


let proc_list_dump_files sess (dump) emit =
  dlogr (fun () -> "proc_list_dump_files");
  let conf = Node_config_var.get cfgname in
  let d = unpack_dump dump in
  let files = Nn_dumps.get_dump_files conf d in
  let l =
    List.map
      (fun (n,sz) ->
         { Pfs_rpcapi_aux.dumpfile_name = n;
           dumpfile_size = sz
         }
      )
      files in
  emit (Array.of_list l)


let one_meg = 1024 * 1024

let proc_read_dump_file sess (dump, dump_file, pos, len) emit =
  dlogr (fun () -> "proc_read_dump_file");
  let conf = Node_config_var.get cfgname in
  let d = unpack_dump dump in
  let dir = Nn_dumps.dump_directory conf d in
  if Sys.file_exists (dir ^ "/dump_succeeded") then 
    let full_file = dir ^ "/" ^ dump_file in
    try
      if len > one_meg then failwith "length too large";
      let data = String.create len in
      let f = Unix.openfile full_file [Unix.O_RDONLY] 0 in
      try
        ignore(Unix.LargeFile.lseek f pos Unix.SEEK_SET);
        Netsys.really_gread `Read_write f data 0 len;
        emit (`ok data);
        Unix.close f
      with error ->
        Unix.close f;
        raise error
    with
      | error ->
           dlogr
             (fun () -> 
                sprintf "proc_read_dump_file: error reading file: %s"
                        (Netexn.to_string error)
             );
           emit `efailed
  else (
    dlogr (fun () -> "proc_read_dump_file: no such dump");
    emit `enoent
  )


let proc_attach1 sess (clustername,hostport) emit =
  dlogr (fun () -> "proc_attach1");
  assert false (* TODO *)


let proc_attach2 sess (clustername,hostport,max_revid,cksum_revids) emit =
  dlogr (fun () -> "proc_attach2");
  assert false (* TODO *)


(**********************************************************************)
(* Setup                                                              *)
(**********************************************************************)

(** Binding RPC servers: *)

let setup srv nn_conf =
  (** Bind the RPC procedure implementations. Note that this is called once
      outside the container context to discover the bound RPC programs. We
      can ignore this here because this discovery is only meaningful for
      portmapper registrations.
   *)
  try
    let conf = Node_config_var.get cfgname in
    Rpcapi_srv.Coordination.V1.bind_async
      ~proc_null:(fun sess () emit -> emit ())
      ~proc_find_coordinator:(fun sess cname emit -> 
				if cname=conf#nn_clustername then
				  let sh = get_shared() in
				  emit(Some sh#master)
				else
				  emit None)
      ~proc_find_namenodes:(fun sess cname emit ->
			      if cname=conf#nn_clustername then
				emit(Array.of_list conf#nn_nodes)
			      else
				emit [| |]
			   )
      ~proc_is_coordinator:(fun sess cname emit -> 
			      if cname=conf#nn_clustername then
				emit(Nn_elect.get_role() = `Coordinator)
			      else
				emit false
			   )
      ~proc_clustername:(fun sess () emit -> emit conf#nn_clustername)
      ~proc_local_identities:(fun sess cname emit ->
				try
				  if cname <> conf#nn_clustername then
				    raise Not_found;
				  let addr = Rpc_server.get_peer_name sess in
				  let ip = Plasma_util.ip_of_sockaddr addr in
				  let dslist =
				    Nn_datastore_news.list ~wait:true () in
				  let locallist =
				    List.filter
				      (fun ds ->
					 match ds.Nn_datastores.node with
					   | None -> false
					   | Some addr -> 
					       ip = 
					         Plasma_util.ip_of_sockaddr addr
				      )
				      dslist in
				  let identities =
				    List.map 
				      (fun ds -> ds.Nn_datastores.identity)
				      locallist in
				  emit(Array.of_list identities)
				with
				  | _ ->
				      emit [| |]
			     )
      srv;
    
    Rpcapi_srv.Dn_admin.V1.bind_async
      ~proc_null:(fun _ _ emit -> emit())
      ~proc_add_datanode
      ~proc_enable_datanode
      ~proc_disable_datanode
      ~proc_is_enabled
      ~proc_is_alive
      ~proc_fsstat
      ~proc_lookup
      ~proc_datanodes
      ~proc_destroy_datanode
      srv;

    Rpcapi_srv.Filesystem.V1.bind_async
      ~proc_null:(fun _ _ emit -> emit())
      ~proc_begin_transaction
      ~proc_commit_transaction
      ~proc_abort_transaction
      ~proc_get_inodeinfo
      ~proc_allocate_inode
      ~proc_update_inodeinfo
      ~proc_delete_inode
      ~proc_get_blocks
      ~proc_allocate_blocks
      ~proc_free_blocks
      ~proc_get_fsstat
      ~proc_rereplicate
      ~proc_lookup:(proc_fs_lookup)
      ~proc_rev_lookup
      ~proc_rev_lookup_dir
      ~proc_link_count
      ~proc_link
      ~proc_link_at
      ~proc_unlink
      ~proc_unlink_at
      ~proc_list
      ~proc_rename
      ~proc_rename_at
      ~proc_namelock
      ~proc_set_block_checksum
      ~proc_iterate
      ~proc_get_blocksize:(proc_get_blocksize nn_conf)
      ~proc_impersonate
      ~proc_get_auth_ticket
      ~proc_renew_auth_ticket
      ~proc_read_admin_table
      ~proc_write_admin_table
      ~proc_is_up_to_date
      ~proc_is_up_to_date_seqno
      ~proc_get_params
      ~proc_get_dn_info
      srv;
    Rpc_server.set_onclose_action srv (fs_onclose srv);

    Rpcapi_srv.Namesync.V1.bind_async
      ~proc_null:(fun _ _ emit -> emit())
      ~proc_create_checkpoint
      ~proc_get_checkpointing_status
      ~proc_block_checkpointing
      ~proc_create_log_dump
      ~proc_get_log_dumping_status
      ~proc_list_dumps
      ~proc_list_dump_files
      ~proc_read_dump_file
      ~proc_attach1
      ~proc_attach2
      srv;
    
    let no_nameslave name _ =
      failwith ("Nameslave." ^ name ^ ": Not available on coordinator node") in
    
    Rpcapi_srv.Nameslave.V1.bind
      ~proc_null:(fun () -> ())
      ~proc_begin_transaction:(no_nameslave "begin_transaction")
      ~proc_prepare_commit:(no_nameslave "prepare_commit")
      ~proc_commit:(no_nameslave "commit")
      ~proc_set_ds_cache:(no_nameslave "set_ds_cache")
      ~proc_push:(no_nameslave "push")
      srv;
  with
    | Netplex_cenv.Not_in_container_thread 
    | Netplex_cenv.Container_variable_not_found _ ->
	(** [setup] was run outside the container context *)
	()

(**********************************************************************)
(* Startup                                                            *)
(**********************************************************************)

let post_start_hook _ conf cauth pool index rev =
  (* This function may be called several times! *)
  ( try
      ignore(get_shared())
    with
      | _ ->
	  let esys = (Netplex_cenv.self_cont()) # event_system in
	  dlog "Nn_coord: creating shared state";
	  let sh_shm =
	    if Netplex_sharedvar.create_var ~enc:true stshmname then (
	      let sh_shm = Nn_state.sh_state_shm pool index rev in
	      State_shm_var.set stshmname sh_shm;
	      sh_shm
	    )
	    else (
	      ignore(Netplex_sharedvar.wait_for_enc_value stshmname);
	      State_shm_var.get stshmname
	    ) in
	  dlog "Nn_coord: creating shared state object";
	  let sh = new Nn_state.shared_state esys conf cauth sh_shm in
	  sh # set_zombie_inodes true; (* Initially scan for such inodes *)
	  dlog "Nn_coord: saving";
	  Node_config_var.set cfgname conf;
	  Shared_state_var.set shstname sh;
	()
  )

let activate_timers() =
  let sh = get_shared() in
  let scan_running = ref false in

  let _tm =
    Netplex_cenv.create_timer
      (fun _tm ->
	 if not !scan_running && sh#zombie_inodes_exist then (
	   scan_running := true;
	   let e =
	     delete_zombie_inodes_e()
	     >> (fun st -> scan_running := false; st) in
	   Uq_engines.when_state (* OK *)
	     ~is_error:(fun err ->
			  Netlog.logf `Crit
			    "Exception while deleting zombie inodes: %s"
			    (Netexn.to_string err)
		       )
	     e;
	   ()
	 );
	 true
      )
      1.0 in
  let _tm =
    Netplex_cenv.create_timer
      (fun _tm ->
	 let counters =
	   List.map
	     (fun name -> 
		(Nn_state.string_of_counter_name name, 
		 sh # reset_count name)
	     )
	     Nn_state.counters in
	 let s =
	   String.concat " "
	     (List.map
		(fun (name,value) -> sprintf "%s=%d" name value) 
		counters
	     ) in
	 Netlog.logf `Info
	   "Counts: %s" s;
	 true
      )
      1.0 in
  ()
