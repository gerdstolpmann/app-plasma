(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Uq_engines.Operators
open Printf

let dlogr = Plasma_util.dlogr

module I64Set = Plasma_util.I64Set

module BRowSet =
  Set.Make
    (struct
       type t = int * int64
       let compare (x_id, x_idx) (y_id, y_idx) =
	 Pervasives.compare (x_id, x_idx) (y_id, y_idx)
     end
    )

type update_characteristics =
    { upd_ds : bool;
      upd_inodes : I64Set.t;
      upd_blocks : BRowSet.t;
    }

type commit_monitor_shm =
    { mutable cm_m : Netmcore_mutex.mutex;
      mutable cm_c : Pfs_condition.condition;
      mutable cm_ws : Pfs_condition.wait_set;
      mutable cm_active : update_characteristics;
      mutable cm_count : int;
      mutable cm_entry_ser_descr : Pfs_condition.ser Netmcore_ref.sref_descr;
    }

type commit_monitor =
    { cm_heap : commit_monitor_shm Netmcore_ref.sref;
      cm_unused : Pfs_condition.wait_entry Queue.t;
      cm_entry_ser : unit Uq_engines.serializer_t;
      cm_exit_ser : unit Uq_engines.serializer_t;
      cm_esys : Unixqueue.event_system;
    }



let empty =
  { upd_ds = false;
    upd_inodes = I64Set.empty;
    upd_blocks = BRowSet.empty;
  }


let union uc1 uc2 =
  { upd_ds = uc1.upd_ds || uc2.upd_ds;
    upd_inodes = I64Set.union uc1.upd_inodes uc2.upd_inodes;
    upd_blocks = BRowSet.union uc1.upd_blocks uc2.upd_blocks;
  }

let diff uc1 uc2 =
  { upd_ds = uc1.upd_ds && not uc2.upd_ds;
    upd_inodes = I64Set.diff uc1.upd_inodes uc2.upd_inodes;
    upd_blocks = BRowSet.diff uc1.upd_blocks uc2.upd_blocks;
  }

let is_compatible uc1 uc2 =
  not (uc1.upd_ds && uc2.upd_ds)
  && I64Set.is_empty (I64Set.inter uc1.upd_inodes uc2.upd_inodes)
  && BRowSet.is_empty (BRowSet.inter uc1.upd_blocks uc2.upd_blocks)


let get_update_characteristics ql =
  List.fold_left
    (fun uc q ->
       Queue.fold
	 (fun uc (m : Nn_db.modification) ->
	    match m with
	      | `Admin_table_put(_,_)    (* just to avoid any locking issues *)
	      | `Datastore_upd _
	      | `Datastore_del _ ->
		  { uc with upd_ds = true }
	      | `Blockalloc_upd(ds,index,_) ->
		  { uc with upd_blocks = BRowSet.add (ds,index) uc.upd_blocks }
	      | `Inode_upd_time(inode,_,_)
	      | `Inode_ins(inode,_) 
	      | `Inode_upd(inode,_)
	      | `Inode_del inode
	      | `Inodeblocks_ins(inode,_)
	      | `Inodeblocks_ins_abstract(inode,_)
	      | `Inodeblocks_del(inode,_,_)
	      | `Inode_set_anonymous inode ->
		  { uc with upd_inodes = I64Set.add inode uc.upd_inodes }
	      | `Names_ins(_,_,_)        (* these inodes do not count *)
	      | `Names_del(_,_)          (* these inodes do not count *)
	      | `Revision_clear
	      | `Revision_upd _ ->
		  uc
	 )
	 uc
	 q
    )
    empty
    ql


let monitor_next_e cm uc f =
  (* This is called with the next candidate. The call of this function
     is serialized, so only one candidate is checked at a time. If
     the operation is compatible with the already running commits,
     it is immediately scheduled. Otherwise, we wait until the running
     commits are done, and do our operation first then.
   *)
  let commit_id = Unix.getpid() in
  let shm = Netmcore_ref.deref_ro cm.cm_heap in

  let get_we() =
    try Queue.take cm.cm_unused
    with Queue.Empty ->
      Netmcore_heap.modify 
        (Netmcore_ref.heap cm.cm_heap)
        (fun mut -> 
           let we = Pfs_condition.alloc_wait_entry mut shm.cm_ws in
           Pfs_pmanage.register_file (Pfs_condition.pipe_file we);
           we
        )  in

  let del_we we =
    Queue.add we cm.cm_unused in

  let rec wait_loop_e we cont () =
    (* Wait until shm.cm_active is compatible with cm. This recursion is
       entered and left with locked mutex cm_m
     *)
    if is_compatible uc shm.cm_active then
      cont()
    else
      Pfs_condition.wait_e 
	we shm.cm_c shm.cm_m cm.cm_esys
	(wait_loop_e we cont)
  in

  let we = get_we() in
  Netmcore_mutex.lock shm.cm_m;
  wait_loop_e we
    (fun () ->
       del_we we;
       let n =
	 Netmcore_heap.modify
	   (Netmcore_ref.heap cm.cm_heap)
	   (fun mut ->
	      shm.cm_active <- Netmcore_heap.add mut (union uc shm.cm_active);
	      let n = shm.cm_count + 1 in
	      shm.cm_count <- n;
	      n
	   ) in
       dlogr
	 (fun () ->
	    sprintf "Nn_commit_monitor: %d running commits (+1)" n
	 );
       Netmcore_mutex.unlock shm.cm_m;
       
       (* The exit serializer is process-local only. Its purpose is to
	  serialize the commit operations [f] that are now allowed to
	  run.
	*)
       let r = ref None in
       let e =
	 cm.cm_exit_ser # serialized
	   (fun _ ->
	      let e1 =
		try f commit_id
		with err ->
		  eps_e (`Error err) cm.cm_esys in
	      Uq_engines.when_state (* OK *)
		~is_error:(fun err ->
			     Netlog.logf `Crit
			       "Exception in commit: %s" (Netexn.to_string err)
			  )
		e1;
	      let e2 = Uq_engines.meta_engine e1 in
	      e2 ++
		(fun x -> 
		   r := Some x;
		   eps_e (`Done()) cm.cm_esys
		)
	   )
	 ++ (fun () ->
	       Netmcore_mutex.lock shm.cm_m;
	       let n =
		 Netmcore_heap.modify
		   (Netmcore_ref.heap cm.cm_heap)
		   (fun mut ->
		      shm.cm_active <- 
			Netmcore_heap.add mut (diff shm.cm_active uc);
		      let n = shm.cm_count - 1 in
		      shm.cm_count <- n;
		      n
		   ) in
	       dlogr
		 (fun () ->
		    sprintf "Nn_commit_monitor: %d running commits (-1)" n
		 );
	       Netmcore_mutex.unlock shm.cm_m;
	       (* It could be that the waiter can now be served, so signal: *)
	       Pfs_condition.signal shm.cm_c;
	       (* There is at most one waiter! *)
	       match !r with
		 | None -> assert false
		 | Some x ->
		     eps_e (x :> _ Uq_engines.engine_state) cm.cm_esys
	    ) in
       eps_e (`Done e) cm.cm_esys
    )
    ()

let monitor_e cm uc f =
  let r = ref None in
  cm.cm_entry_ser # serialized
    (fun _ ->
       monitor_next_e cm uc f
       ++ (fun e ->
	     r := Some e;
	     eps_e (`Done ()) cm.cm_esys
	  )
    )
  ++
    (fun () ->
       match !r with
	 | None -> assert false
	 | Some e -> 
	     e
    )

let create_commit_monitor_heap pool =
  let entry_ser_shm =
    Pfs_condition.serializer_heap pool in
  let s0 =
    { cm_m = Netmcore_mutex.dummy();
      cm_c = Pfs_condition.dummy_condition();
      cm_ws = Pfs_condition.dummy_wait_set();
      cm_active = empty;
      cm_count = 0;
      cm_entry_ser_descr = Netmcore_ref.descr_of_sref entry_ser_shm;
    } in
  let heap = Netmcore_ref.sref pool s0 in
  Netmcore_heap.modify
    (Netmcore_ref.heap heap)
    (fun mut ->
       let s = Netmcore_ref.deref_ro heap in
       s.cm_m <- Netmcore_mutex.create mut `Errorcheck;
       s.cm_c <-  Pfs_condition.create_condition mut;
       s.cm_ws <-  Pfs_condition.create_wait_set mut "/tmp/plasmacond-";  (* FIXME *)
    );
  heap


let create_commit_monitor pool heap esys =
  let entry_ser_heap =
    Netmcore_ref.deref_p heap
      (fun s -> 
	 Netmcore_ref.sref_of_descr pool s.cm_entry_ser_descr
      ) in
  { cm_heap = heap;
    cm_unused = Queue.create();
    cm_entry_ser = 
      Pfs_condition.serializer entry_ser_heap esys "Nn_commit_monitor";
    cm_exit_ser = Uq_engines.serializer esys;
    cm_esys = esys;
  }

