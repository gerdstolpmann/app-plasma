(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

type datastore =
    { id : int;
      identity : string;
      size : int64;
      enabled : bool;
      node : Unix.sockaddr option;
      alive : bool;
    }

type datastores = datastore list
  (** Keep it simple. The [id] and [identity] components must be unique in
      the list
   *)
  (* TODO: use better data structure *)

type discovered =
    { d_identity : string;
      d_size : int64
    }


module Datastores_var =
  Netplex_cenv.Make_var_type(struct type t = datastores end)

module Datastores_shvar =
  Netplex_sharedvar.Make_var_type(struct type t = datastores end)

let shdsname = "plasma.namenode.monitor.datastores"
  (** The name of the [Netplex_sharedvar] variable storing the current
      list of datastores (to be used with [Datastores_shvar])
   *)

let null_datastore =
  { id = 0;
    identity = "";
    size = 0L;
    enabled = false;
    node = None;
    alive = false
  }


let to_xdr ds =
  { Pfs_rpcapi_aux.ds_id = ds.id;
    ds_identity = ds.identity;
    ds_size = ds.size;
    ds_enabled = ds.enabled;
    ds_node = ( match ds.node with
		  | None -> None
		  | Some (Unix.ADDR_INET(ip,p)) ->
		      Some (sprintf "%s:%d" (Unix.string_of_inet_addr ip) p)
		  | Some _ ->
		      failwith "Nn_datastores.to_xdr"
	      );
    ds_alive = ds.alive
  }


let of_xdr ds =
  { id = ds.Pfs_rpcapi_aux.ds_id;
    identity = ds.Pfs_rpcapi_aux.ds_identity;
    size = ds.Pfs_rpcapi_aux.ds_size;
    enabled = ds.Pfs_rpcapi_aux.ds_enabled;
    node = ( match ds.Pfs_rpcapi_aux.ds_node with
	       | None -> None
	       | Some s ->
		   let (h,p) = Plasma_util.parse_host_port s in
		   Some (Plasma_util.sockaddr_of_host_port (h,p))
	   );
    alive = ds.Pfs_rpcapi_aux.ds_alive
    }
