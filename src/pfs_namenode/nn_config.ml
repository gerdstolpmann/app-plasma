(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type nn_node_config =
object
  method nn_clustername : string
  method nn_hostname : string
  method nn_nodes : string list
  method nn_node_timeout : float
  method nn_node_alive_min : int
  method nn_node_alive_min_startup : int
  method nn_rank : string
  method nn_elect_timeout : float
  method nn_inodecaches : string list
  method nn_replication : int
  method nn_lock_timeout : float
  method nn_pool_size : int
  method nn_dump_dir : string
  method nn_consistency_checks : bool
  method dn_blocksize : int
  method dn_port : int
  method dn_node_timeout : float
  method dn_security_level : Pfs_auth.level
  method dn_discovery_addrs : Unix.inet_addr list
  method dn_discovery_period : float
  method dn_multicast_ttl : int
  method dn_max_errors : int
end


class type nn_min_config =
object
  method nn_clustername : string
  method nn_nodes : string list
  method nn_node_timeout : float
end


let extract_node_config (cf:Netplex_types.config_file) =
  let datanodes =
    cf#resolve_section cf#root_addr "datanodes" in
  let (dn_port, dn_node_timeout, dn_blocksize, dn_sec_level,
       dn_discovery_addrs, dn_discovery_period,  dn_mcast_ttl,
       dn_max_errors) =
    match datanodes with
      | [] ->
	  failwith "missing 'datanodes' config"
      | _ :: _ :: _ ->
	  failwith "more than one 'datanodes' config"
      | [dn] ->
	  let port =
	    try
	      let p = cf#resolve_parameter dn "port" in
	      cf#int_param p
	    with
	      | Not_found -> 
		  failwith "missing 'datanodes.port' parameter" in
	  let timeout =
	    try
	      let p = cf#resolve_parameter dn "timeout" in
	      cf#float_param p
	    with
	      | Not_found -> 60.0 in
	  let blocksize =
	    try
	      let p = cf#resolve_parameter dn "blocksize" in
	      cf#int_param p
	    with
	      | Not_found ->
		  failwith "missing 'datanodes.blocksize' parameter" in
	  let dn_sec_level =
	    try
	      let p = cf#resolve_parameter dn "security_level" in
	      Pfs_auth.level_of_string(cf#string_param p)
	    with
	      | Not_found ->
		  `Authenticated in
	  let dn_discovery_names =
	    Plasma_util.node_list
	      ~node_param:"discovery" ~empty_ok:true 
	      cf dn in
	  let dn_discovery_addrs =
	    if dn_discovery_names = [] then
	      [ Unix.inet_addr_of_string "224.0.0.1" ]
	    else
	      List.map
		(fun n ->
		   let (h,p) = Plasma_util.parse_host_port n in
		   Plasma_util.ip_of_host h
		)
		dn_discovery_names in
	  let dn_discovery_period =
	    try
	      let p = cf#resolve_parameter dn "discovery_period" in
	      cf#float_param p
	    with
	      | Not_found -> 1.0 in
	  let dn_mcast_ttl =
	    try
	      let p = cf#resolve_parameter dn "multicast_ttl" in
	      cf#int_param p
	    with
	      | Not_found -> 1 in
	  let dn_max_errors =
	    try
	      let p = cf#resolve_parameter dn "max_errors" in
	      cf#int_param p
	    with
	      | Not_found -> 5 in
	  (port, timeout, blocksize, dn_sec_level, dn_discovery_addrs,
	   dn_discovery_period, dn_mcast_ttl, dn_max_errors) in
  let namenodes =
    cf#resolve_section cf#root_addr "namenodes" in
  let (nn_nodes, nn_clustername, nn_hostname, nn_node_timeout, 
       nn_node_alive_min, nn_node_alive_min_startup, nn_rank,
       nn_elect_timeout, nn_inodecaches, nn_replication, nn_lock_timeout,
       nn_pool_size, nn_consistency_checks, nn_dump_dir) =
    match namenodes with
      | [] ->
	  failwith "missing 'namenodes' config"
      | _ :: _ :: _ ->
	  failwith "more than one 'namenodes' config"
      | [nn] ->
	  let addrs = Plasma_util.node_list cf nn in
	  let timeout =
	    try
	      let p = cf#resolve_parameter nn "timeout" in
	      cf#float_param p
	    with
	      | Not_found -> 60.0 in
	  let elect_timeout =
	    try
	      let p = cf#resolve_parameter nn "elect_timeout" in
	      cf#float_param p
	    with
	      | Not_found -> 300.0 in
	  let lock_timeout =
	    try
	      let p = cf#resolve_parameter nn "lock_timeout" in
	      cf#float_param p
	    with
	      | Not_found -> 300.0 in
	  let pool_size =
	    try
	      let p = cf#resolve_parameter nn "pool_size" in
	      cf#int_param p
	    with
	      | Not_found -> 50 * 1024 * 1024 in
	  let alive_min =
	    try
	      let p = cf#resolve_parameter nn "alive_min" in
	      if (try cf#string_param p = "all" with _ -> false) then
		List.length addrs
	      else
		cf#int_param p
	    with
	      | Not_found -> 1 in
	  let alive_min_startup =
	    try
	      let p = cf#resolve_parameter nn "alive_min_startup" in
	      if (try cf#string_param p = "all" with _ -> false) then
		List.length addrs
	      else
		cf#int_param p
	    with
	      | Not_found -> alive_min in
	  let rank =
	    try
	      let p = cf#resolve_parameter nn "rank" in
	      ( try string_of_int (cf#int_param p)
		with _ ->
		  cf # string_param p
	      )
	    with
	      | Not_found -> 
		  ( try
		      let p = cf#resolve_parameter nn "rank_script" in
		      let script = cf#string_param p in
		      let b = Buffer.create 80 in
		      Shell.call
			~stdout:(Shell.to_buffer b)
			[ Shell.cmd "/bin/sh" [ "-c"; script ]];
		      Buffer.contents b
		    with
		      | Not_found ->
			  failwith "missing 'namenodes.rank' or \
                                    'namenodes.rank_script' parameters"
		  ) in
	  let hostname =
	    try
	      let p = cf#resolve_parameter nn "hostname" in
	      cf#string_param p
	    with
	      | Not_found ->
		  Unix.gethostname() in
	  let clustername =
	    try
	      let p = cf#resolve_parameter nn "clustername" in
	      cf#string_param p
	    with
	      | Not_found ->
		  failwith "missing 'namenodes.clustername' parameter" in
	  let replication =
	    try
	      let p = cf#resolve_parameter nn "replication" in
	      ( try
		  match cf#string_param p with
		    (* | "max" -> List.length dn_nodes *)
		    | _ -> raise Not_found
		with _ ->
		  cf#int_param p
	      )
	    with
	      | Not_found -> 1 in
	  let inodecaches =
	    cf#resolve_section nn "inodecache" in
	  let ic_addrs =
	    List.map
	      (fun n ->
		 try
		   let p = cf#resolve_parameter n "port" in
		   hostname ^ ":" ^ string_of_int (cf#int_param p)
		 with
		   | Not_found ->
		       failwith "missing 'port' parameter in 'namenodes.inodecache'"
	      )
	      inodecaches in
	  List.iter
	    (fun addr ->
	       try ignore(Plasma_util.parse_host_port addr)
	       with _ ->
		 failwith ("bad 'addr' parameter in 'namenodes.inodecache': "
			   ^ addr)
	    )
	    ic_addrs;
	  let consistency_checks =
	    try
	      let p = cf#resolve_parameter nn "consistency_checks" in
	      cf#bool_param p
	    with
	      | Not_found ->
		  false in
	  let dump_dir =
	    try
	      let p = cf#resolve_parameter nn "dump_dir" in
	      cf#string_param p
	    with
	      | Not_found ->
		  failwith "missing 'namenodes.dump_dir' parameter" in
	  (addrs, clustername, hostname, timeout, alive_min, alive_min_startup,
	   rank, elect_timeout, ic_addrs, replication, lock_timeout, pool_size,
	   consistency_checks, dump_dir
	  )
  in
  ( object
      method nn_clustername = nn_clustername
      method nn_hostname = nn_hostname
      method nn_nodes = nn_nodes
      method nn_node_timeout = nn_node_timeout
      method nn_node_alive_min = nn_node_alive_min
      method nn_node_alive_min_startup = nn_node_alive_min_startup
      method nn_rank = nn_rank
      method nn_elect_timeout = nn_elect_timeout
      method nn_inodecaches = nn_inodecaches
      method nn_replication = nn_replication
      method nn_lock_timeout = nn_lock_timeout
      method nn_pool_size = nn_pool_size
      method nn_consistency_checks = nn_consistency_checks
      method nn_dump_dir = nn_dump_dir
      method dn_port = dn_port
      method dn_node_timeout = dn_node_timeout
      method dn_blocksize = dn_blocksize
      method dn_security_level = dn_sec_level
      method dn_discovery_addrs = dn_discovery_addrs
      method dn_discovery_period = dn_discovery_period
      method dn_multicast_ttl = dn_mcast_ttl
      method dn_max_errors = dn_max_errors
    end
  )


let extract_min_config (cf:Netplex_types.config_file) =
  let namenodes =
    cf#resolve_section cf#root_addr "namenodes" in
  let (nn_nodes, nn_clustername, nn_node_timeout) =
    match namenodes with
      | [] ->
	  failwith "missing 'namenodes' config"
      | _ :: _ :: _ ->
	  failwith "more than one 'namenodes' config"
      | [nn] ->
	  let addrs = Plasma_util.node_list cf nn in
	  let timeout =
	    try
	      let p = cf#resolve_parameter nn "timeout" in
	      cf#float_param p
	    with
	      | Not_found -> 60.0 in
	  let clustername =
	    try
	      let p = cf#resolve_parameter nn "clustername" in
	      cf#string_param p
	    with
	      | Not_found ->
		  failwith "missing 'namenodes.clustername' parameter" in
	  (addrs, clustername, timeout) in
  ( object
      method nn_clustername = nn_clustername
      method nn_nodes = nn_nodes
      method nn_node_timeout = nn_node_timeout
    end
  )  

let bmaprowsize = 1024
