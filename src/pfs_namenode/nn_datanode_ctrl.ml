(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Uq_engines.Operators
open Printf

let dlog = Plasma_util.dlog
let dlogr = Plasma_util.dlogr

module Rpcapi_clnt = Pfs_rpcapi_clnt
module Rpcapi_srv = Pfs_rpcapi_srv
module Rpcapi_aux = Pfs_rpcapi_aux
module A = Pfs_rpcapi_aux

module Dnctrl_proxy = Rpcapi_clnt.Make'Datanode_ctrl(Rpc_proxy.ManagedClient)

exception Datanode_error of Nn_datastores.datastore * exn

type client = Rpc_proxy.ManagedClient.mclient

type ticket_verifier =
    { tv_safetrans_id : int64;
      tv_safetrans_tmo : int64;
      tv_safetrans_sec : int64;
      tv_datanodes : int list;
      mutable tv_enabled : bool;
    }

(* This is put into shared memory *)
type ctrl_shared =
    { mutable mutex : Netmcore_mutex.mutex;
      (** Protects accesses to this record *)

      mutable reset_datastores : int list;
      (** The DB identities of already reset datastores *)

      mutable next_id : int64;
      (** Next verifier ID *)
    }


type ctrl =
    { clients : (int, Nn_datastores.datastore * client) Hashtbl.t;
      (** maps datastore ID to client *)

      cauth : Pfs_auth.client_auth;
      (** Authentication data *)

      preallocated : ticket_verifier Queue.t;
      (** the queue of preallocated verifiers *)

      preallocated_size : int ref;
      (** the size to which preallocated is filled regularly *)

      preallocated_incr : bool ref;
      (** whether preallocated_size was increased *)

      disable : (ticket_verifier * (unit -> unit)) Queue.t;
      (** the queue of verifiers to disable, w/ post functions *)

      datastores : Nn_datastores.datastore list ref;
      (** the current list of datastores *)

      hkeep : bool ref;
      (** whether housekeeping is in progress *)

      shared : ctrl_shared Netmcore_ref.sref;
      (** Pointer to shared part *)

      reset_ser : Pfs_condition.ser Netmcore_ref.sref;
      (** The shared part of the serializer *)

      reset_serializer : unit Uq_engines.serializer_t;
      (** Serializes the reset action *)
    }

module Ctrl_var = 
  Netplex_cenv.Make_var_type(struct type t = ctrl end)

let ctrlname = "current_datanode_ctrl"

let get_ctrl() =
  Ctrl_var.get ctrlname


module Shared_var =
  Netplex_sharedvar.Make_var_type
    (struct type t = ctrl_shared Netmcore_ref.sref_descr end )

let sharedname = "plasma.namenode.dn_ctrl.shm"


module Reset_ser_var =
  Netplex_sharedvar.Make_var_type
    (struct type t =  Pfs_condition.ser Netmcore_ref.sref_descr end )

let reset_ser_name = "plasma.namenode.dn_ctrl.reset_ser"



let get_next_id() =
  let ctrl = get_ctrl() in
  Netmcore_ref.deref_p ctrl.shared
    (fun sh -> Netmcore_mutex.lock sh.mutex);
  let id =
    Netmcore_heap.modify
      (Netmcore_ref.heap ctrl.shared)
      (fun mut ->
	 let sh = Netmcore_ref.deref_ro ctrl.shared in
	 let next_id = Netmcore_heap.copy sh.next_id in
	 sh.next_id <- Netmcore_heap.add mut (Int64.succ next_id);
	 next_id
      ) in
  Netmcore_ref.deref_p ctrl.shared
    (fun sh -> Netmcore_mutex.unlock sh.mutex);
  id


let () =
  let open Nn_datastores in
  Netexn.register_printer
    (Datanode_error(null_datastore,Not_found))
    (function
       | Datanode_error(ds, e) ->
	   sprintf "Nn_datanode_ctrl.Datanode_error(identity=%s%s%s%s,%s)"
	     ds.identity
	     (if ds.enabled then " enabled" else "")
	     (if ds.alive then " alive" else "")
	     (match ds.node with
		| None -> ""
		| Some addr -> " " ^ Netsys.string_of_sockaddr addr
	     )
	     (Netexn.to_string e)
       | _ -> assert false
    )


let create_client conn cauth esys =
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Rpcapi_clnt.Datanode_ctrl.V1._program ]
      ~msg_timeout:20.0  (* this should be quite short *)
      ~msg_timeout_is_fatal:true
      ~initial_ping:true
      ~auth_methods:(Pfs_auth.rpc_proxy_auth_methods cauth true)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  mclient


let live_datastores() =
  (** Returns the sorted list of live datastores *)
  let ds_list =
    Nn_datastore_news.list ~wait:true () in
  List.sort
    (fun ds1 ds2 ->
       compare ds1.Nn_datastores.id ds2.Nn_datastores.id
    )
    (List.filter
       (fun ds ->
	  ds.Nn_datastores.enabled && ds.Nn_datastores.alive
       )
       ds_list
    )


let noop() = ()

let upd_datastores ctrl =
  (** Updates the [datastores] component in ctrl, and resets [preallocated]
      if there is a change.
   *)
  let dlist = live_datastores() in
  if dlist <> !(ctrl.datastores) then (
    ctrl.datastores := dlist;
    while not (Queue.is_empty ctrl.preallocated) do
      let tv = Queue.take  ctrl.preallocated in
      Queue.add (tv, noop) ctrl.disable
    done
  )


let create_ticket_verifier ds_list =
  { tv_safetrans_id = get_next_id();
    tv_safetrans_tmo = Int64.add (Int64.of_float (Unix.time())) 3600L; (* 1h *)
    tv_safetrans_sec = Plasma_rng.random_int64();
    tv_datanodes = List.map (fun ds -> ds.Nn_datastores.id) ds_list;
    tv_enabled = false;
  }


let new_ticket_verifier() =
  let ctrl = get_ctrl() in
  upd_datastores ctrl;
  let now = Unix.time() in
  let ds_list = !(ctrl.datastores) in
  let ds_id_list = List.map (fun ds -> ds.Nn_datastores.id) ds_list in
  try
    let found = ref None in
    while !found = None do
      let tv = Queue.take ctrl.preallocated in
      let min_tmo = Int64.add (Int64.of_float now) 1800L (* 30 min *) in
      if tv.tv_safetrans_tmo >= min_tmo && tv.tv_datanodes = ds_id_list then
	found := Some tv
    done;
    match !found with
      | None -> assert false
      | Some tv -> tv
  with
    | Queue.Empty ->
	dlogr (fun () ->
		 "Nn_datanode_ctrl: ran out of pre-allocated tickets");
	if not !(ctrl.preallocated_incr) then (
	  ctrl.preallocated_size := min 65536 (2 * !(ctrl.preallocated_size));
	  ctrl.preallocated_incr := true;
	  dlogr (fun () ->
		   sprintf
		     "Nn_datanode_ctrl: prealloc_size=%d"
		     !(ctrl.preallocated_size));
	);
	create_ticket_verifier ds_list


let is_reset_complete ctrl id =
  Netmcore_ref.deref_p ctrl.shared
    (fun sh -> 
       Netmcore_mutex.lock sh.mutex;
       let r = List.mem id sh.reset_datastores in
       Netmcore_mutex.unlock sh.mutex;
       r
    )

let mark_reset_complete ctrl id =
  Netmcore_heap.modify 
    (Netmcore_ref.heap ctrl.shared)
    (fun mut ->
       let sh = Netmcore_ref.deref_ro ctrl.shared in
       Netmcore_mutex.lock sh.mutex;
       sh.reset_datastores <- 
	 Netmcore_heap.add mut (id :: sh.reset_datastores);
       Netmcore_mutex.unlock sh.mutex;
    )


let reset_new_datastores_e() =
  (** Create connections to newly added datastores, and send a reset to them *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let ctrl = get_ctrl() in
  upd_datastores ctrl;
  let ds_list = !(ctrl.datastores) in
  let new_datastores =
    List.filter
      (fun ds -> 
	 let id = ds.Nn_datastores.id in
	 not (Hashtbl.mem ctrl.clients id)
      )
      ds_list in
  let have_new_datastores =
    (new_datastores <> []) in
  let e_list() =
    List.map
      (fun ds ->
	 let id = ds.Nn_datastores.id in
	 if not (Hashtbl.mem ctrl.clients id) then (
	   let conn =
	     match ds.Nn_datastores.node with
	       | None -> assert false
	       | Some addr -> Plasma_util.connector_of_sockaddr addr in
	   let client = create_client conn ctrl.cauth esys in
	   Hashtbl.add ctrl.clients id (ds,client);
	   (* Check whether a reset has already been done: *)
	   if is_reset_complete ctrl id then
	     eps_e (`Done()) esys
	   else (
	     dlogr (fun () -> sprintf "Resetting %d" id);
	     Plasma_util.rpc_engine
	       client
	       Dnctrl_proxy.V1.reset_all_safetrans'async
	       ()
	     >> (function
		   | `Done () -> 
		       mark_reset_complete ctrl id;
		       `Done ()
		   | `Error error ->
		       Netlog.logf `Err
			 "reset_all_safetrans: Exception %s"
			 (Netexn.to_string error);
		       Rpc_proxy.ManagedClient.enforce_unavailability client;
		       Hashtbl.remove ctrl.clients id;
		       `Done ()
		   | `Aborted ->
		       `Done ()
		)
	   )
	 )
	 else eps_e (`Done()) esys
      )
      ds_list in

  if not have_new_datastores then
    eps_e (`Done()) esys
  else (
    (* this serializer works across processes! *)
    ctrl.reset_serializer # serialized
      (fun _ ->
	 dlog "Start reset block";
	 Plasma_util.msync_engine (e_list()) esys
	 >> (function st ->
	       dlog "End reset block";
	       st
	    )
      )
  )
  

let disconnect id =
  (** Look the client up, and terminate the connection: *)
  let ctrl = get_ctrl() in
  try
    let _, client = Hashtbl.find ctrl.clients id in
    Rpc_proxy.ManagedClient.trigger_shutdown client (fun () -> ());
    Hashtbl.remove ctrl.clients id
  with
    | Not_found -> ()


let reset() =
  (** First terminate all connections: *)
  let ctrl = get_ctrl() in
  Hashtbl.iter
    (fun id (_,client) ->
       Rpc_proxy.ManagedClient.trigger_shutdown client (fun () -> ())
    )
    ctrl.clients;
  Hashtbl.clear ctrl.clients
  (* As the housekeeping loop is constantly running, there is no need
     to call reset_new_datastores_e now
   *)


let enable_ticket_verifier_e tv =
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let ctrl = get_ctrl() in

  if tv.tv_enabled then
    eps_e (`Done()) esys
  else (
    dlogr (fun () ->
	     sprintf "Nn_datanode_ctrl: enabling safetrans %Ld" 
	       tv.tv_safetrans_id);
    reset_new_datastores_e()
    ++ (fun () ->
	  Plasma_util.msync_engine
	    ( List.map
		(fun id ->
		   try
		     let ds, client = 
		       Hashtbl.find ctrl.clients id in(* or Not_found *)
		     Plasma_util.rpc_engine
		       client
		       Dnctrl_proxy.V1.safetrans'async
		       [| { A.st_id = tv.tv_safetrans_id;
			    st_tmo = tv.tv_safetrans_tmo;
			    st_secret = tv.tv_safetrans_sec
			  }
		       |]
		     >> (function
			   | `Done _ ->
			       dlogr
				 (fun () ->
				    sprintf
				      "Datastore %d: enabled safetrans %Ld \
                                       secret %Ld"
				      id tv.tv_safetrans_id 
				      tv.tv_safetrans_sec);
			       `Done()
			   | `Error e ->
			       let e' = Datanode_error(ds,e) in
			       Netlog.logf `Err
				 "safetrans: Exception %s" 
				 (Netexn.to_string e');
			       `Error e'
			   | `Aborted -> `Aborted
			)
		   with
		     | Not_found ->
			 eps_e (`Error (Failure
					  (sprintf 
					     "Nn_datanode_ctrl: datanode %d is
                                        not available anymore - cannot enable \
                                        ticket"
					     id))) esys
		)
		tv.tv_datanodes
	    )
	    esys
       )
    ++ (fun () ->
	  (* even if tv_datastores=[] *)
	  tv.tv_enabled <- true;
	  eps_e (`Done()) esys
       )
  )

let disable_ticket_verifier tv post =
  (* We just add tv to the queue of verifiers to disable... *)
  let ctrl = get_ctrl() in
  tv.tv_enabled <- false;
  Queue.add (tv,post) ctrl.disable


let housekeeping() =
  (* This is called every second to
     - refill missing ticket verifiers
     - disable verifiers
   *)
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let ctrl = get_ctrl() in

  let rec cancel() =
    let l = 
      Queue.fold (fun acc (tv,f_post) -> (tv,f_post)::acc) [] ctrl.disable in
    Queue.clear ctrl.disable;
    let tv_list = List.map fst l in
    let f_post_list = List.map snd l in
    if l = [] then
      ( [], [] )
    else
      (cancel_tv tv_list (List.hd tv_list).tv_datanodes, f_post_list)

  and cancel_tv tv_list id_list =
    match id_list with
      | id :: id_list' ->
	  let arg =
	    Array.of_list
	      (List.map (fun tv -> tv.tv_safetrans_id) tv_list) in
	  let e =
	    try
	      let (ds,client) = Hashtbl.find ctrl.clients id in
	      Plasma_util.rpc_engine
		client
		Dnctrl_proxy.V1.cancel_safetrans'async
		arg
	      >> (function
		     | `Done _ ->
			 List.iter
			   (fun tv ->
			      dlogr
				(fun () ->
				   sprintf
				     "Datastore %d: cancelled safetrans %Ld \
                                      secret %Ld"
				     id tv.tv_safetrans_id tv.tv_safetrans_sec)
			   )
			   tv_list;
			 `Done()
		     | `Error e ->
			 Netlog.logf `Err
			   "cancelling safetrans: Exception %s"
			   (Netexn.to_string e);
			 `Done()   (* We ignore errors here *)
		     | `Aborted -> `Aborted
		  )
	    with
	      | Not_found ->
		  eps_e (`Done ()) esys in
	  e :: cancel_tv tv_list id_list' 
      | [] ->
	  []
  in

  let new_verifiers = Queue.create() in

  let rec refill ds_list =
    let n() = Queue.length ctrl.preallocated + Queue.length new_verifiers in
    while n() < !(ctrl.preallocated_size) do
      let tv = create_ticket_verifier ds_list in
      Queue.add tv new_verifiers;
    done;
    let tv_list =
      Queue.fold (fun acc tv -> tv :: acc) [] new_verifiers in
    if tv_list = [] then
      []
    else
      refill_tv tv_list ds_list

  and refill_tv tv_list ds_list =
    match ds_list with
      | ds :: ds_list' ->
	  let arg =
	    Array.of_list
	      (List.map
		 (fun tv ->
		    { A.st_id = tv.tv_safetrans_id;
		      st_tmo = tv.tv_safetrans_tmo;
		      st_secret = tv.tv_safetrans_sec
		    }
		 )
		 tv_list
	      ) in
	  let e =
	    try
	      let id = ds.Nn_datastores.id in
	      let (ds,client) = Hashtbl.find ctrl.clients id in
	      Plasma_util.rpc_engine
		client
		Dnctrl_proxy.V1.safetrans'async
		arg
	      >> (function
		     | `Done _ ->
			 List.iter
			   (fun tv ->
			      tv.tv_enabled <- true;
			      dlogr
				(fun () ->
				   sprintf
				     "Datastore %d: refilled safetrans %Ld \
                                      secret %Ld"
				     id tv.tv_safetrans_id tv.tv_safetrans_sec)
			   )
			   tv_list;
			 `Done()
		     | `Error e ->
			 let e' = Datanode_error(ds,e) in
			 Netlog.logf `Err
			   "refilling safetrans: Exception %s"
			   (Netexn.to_string e');
			 `Error e'
		     | `Aborted -> `Aborted
		  )
	    with
	      | Not_found ->
		  eps_e (`Done ()) esys in
	  e :: refill_tv tv_list ds_list' 
      | [] ->
	  []
  in

  dlogr
    (fun () ->
       sprintf "ticket housekeeping already_running=%B" (!(ctrl.hkeep)));
  if not !(ctrl.hkeep) then (
    ctrl.hkeep := true;
    let _e =
      reset_new_datastores_e()
      ++ (fun () ->
	    let ds_list = !(ctrl.datastores) in
	    let cancel_elist, f_post_list = cancel() in
	    let refill_elist = refill ds_list in
	    Plasma_util.msync_engine (cancel_elist @ refill_elist) esys
	    ++ (fun () ->
		  let threshold = !(ctrl.preallocated_size) * 3 / 4 in
		  if not !(ctrl.preallocated_incr) &&
		     Queue.length ctrl.preallocated > threshold &&
                     !(ctrl.preallocated_size) > 100
		  then
		    ctrl.preallocated_size := !(ctrl.preallocated_size) / 2;
		  ctrl.preallocated_incr := false;
		  Queue.transfer new_verifiers ctrl.preallocated;
		  ctrl.hkeep := false;
		  List.iter (fun f -> f()) f_post_list;
		  eps_e (`Done ()) esys
	       )
	 )
      >> (function
	    | `Error e ->
		Netlog.logf `Err
		  "Nn_datanode_ctrl: stopping housekeeping after exception: %s"
		  (Netexn.to_string e);
		ctrl.hkeep := false;
		`Done ()
	    | st -> st
	 ) in
      ()
  )

(* FIXME: if only one allocation/deallocation goes wrong, the whole
   housekeeping round fails
 *)


let post_start_hook pool cauth =
  let shm =
    if Netplex_sharedvar.create_var ~enc:true sharedname then (
      let s0 =
	{ mutex = Netmcore_mutex.dummy();
	  next_id = 1L;
	  reset_datastores = []
	} in
      let shm = Netmcore_ref.sref pool s0 in
      Netmcore_heap.modify
	(Netmcore_ref.heap shm)
	(fun mut ->
	   let s = Netmcore_ref.deref_ro shm in
	   s.mutex <- Netmcore_mutex.create mut `Normal
	);
      let descr = Netmcore_ref.descr_of_sref shm in
      Shared_var.set sharedname descr;
      shm
    )
    else (
      ignore(Netplex_sharedvar.wait_for_enc_value sharedname);
      let descr = Shared_var.get sharedname in
      Netmcore_ref.sref_of_descr pool descr
    ) in

  let reset_ser =
    if Netplex_sharedvar.create_var ~enc:true reset_ser_name then (
      let ser = Pfs_condition.serializer_heap pool in
      let descr = Netmcore_ref.descr_of_sref ser in
      Reset_ser_var.set reset_ser_name descr;
      ser
    )
    else (
      ignore(Netplex_sharedvar.wait_for_enc_value reset_ser_name);
      let descr = Reset_ser_var.get reset_ser_name in
      Netmcore_ref.sref_of_descr pool descr
    ) in

  let esys = (Netplex_cenv.self_cont()) # event_system in
  Ctrl_var.set ctrlname
    { clients = Hashtbl.create 7;
      cauth = cauth;
      preallocated = Queue.create();
      preallocated_size = ref 100;
      preallocated_incr = ref false;
      disable = Queue.create();
      datastores = ref [];
      hkeep = ref false;
      shared = shm;
      reset_ser = reset_ser;
      reset_serializer = Pfs_condition.serializer reset_ser esys "datanode_reset";
    };
  let _timer =
    Netplex_cenv.create_timer
      (fun _ -> 
	 (try 
	    housekeeping()
	  with
	    | error ->
		Netlog.logf `Crit
		  "Nn_datanode_ctrl: Exception during housekeeping: %s"
		  (Netexn.to_string error)
	 );
	 true
      ) 
      1.0 in
  ()



