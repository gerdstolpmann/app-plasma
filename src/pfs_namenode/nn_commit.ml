(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Plasma_util.Operators
open Printf

let dlog = Plasma_util.dlog


let slave_info (hp,mc,enabled) =
  let s =
    match Rpc_proxy.ManagedClient.mclient_state mc with
      | `Down -> "down"
      | `Connecting -> "connecting"
      | `Up None -> "up(n/a)"
      | `Up (Some addr) -> "up(" ^ Netsys.string_of_sockaddr addr ^ ")" in
  sprintf "slave(addr=%s,enabled=%B,serial=%d,state=%s)"
    hp enabled
    (Rpc_proxy.ManagedClient.mclient_serial mc) s


let disable_slave shared_state (hp,mclient,enabled) err =
  Netlog.logf `Alert
    "Disabling namenode %s after error: %s"
    (slave_info (hp,mclient,enabled)) (Netexn.to_string err);
  shared_state # disable_slave hp;
  (* Unix.sleep 2; *)  (* debug *)
  Nn_push.trigger_shutdown mclient
    (* The shutdown will be considered as rollback by the slave *)



let commit_phase1_write_locally journal shared_state t =
  (** Execute the db updates in the journal *)
  Netlog.logf `Debug "Writing changes to local database";
  let esys = shared_state#event_system in
  qseq_e (Queue.copy journal) esys
    (fun m ->
       Nn_db.exec_e m t#connection esys
    )
  ++ (fun _ ->
	Netlog.logf `Debug "Done writing changes to local database";
	eps_e (`Done ()) shared_state#event_system
     )
    
let commit_phase1_prepare_locally t =
  Netlog.logf `Debug "Preparing commit at local database";
  t#prepare()


let commit_phase1_write_remotely slave commit_id journal shared_state rev_id =
  (** Start remote transactions, and execute the updates from the journal
      there. Errors are suppressed (but logged) - individual slaves may
      fail. Later we check that the number of alive slaves is still at
      least the minimum.
   *)
  let (hp, mclient, enabled) = slave in
  let esys = shared_state#event_system in
  let cname = shared_state # node_config # nn_clustername in
   if enabled then (
    Netlog.logf `Debug "Pushing updates to slave %s" hp;
    (Nn_push.begin_transaction_e mclient commit_id cname rev_id)
    ++ (fun _ ->
	  qseq_e (Queue.copy journal) esys
	    (fun m ->
	       if shared_state # is_slave_disabled hp then (
		 Netlog.logf `Warning
		   "commit_phase1_write_remotely still running \
                    although slave disabled";
		 eps_e (`Done ()) esys
	       )
	       else
		 Nn_push.push_e m mclient commit_id
	    )
       )
    >> (function
	  | `Done _  -> `Done ()
	  | `Error e -> disable_slave shared_state slave e; `Done()
	  | `Aborted -> 
	      disable_slave shared_state slave (Failure "Aborted");
	      `Done()
       )
  ) else
    (* Skip by providing a "false positive" return value *)
    eps_e (`Done ()) esys
      

let commit_phase1_prepare_remotely slave commit_id shared_state =
  (** Prepare remote transactions for commit. Errors are handled as in
      commit_phase1_write_remotely
   *)
  let (hp, mclient, enabled) = slave in
  let esys = shared_state#event_system in
  if enabled then (
    Netlog.logf `Debug "Preparing commit at slave %s" hp;
    (Nn_push.prepare_commit_e mclient commit_id)
    ++ (fun b ->
	  if b then
	    eps_e (`Done()) esys
	  else
	    eps_e (`Error(Failure "Slave denied transaction")) esys
       )
    >> (function
	  | `Done _  -> `Done ()
	  | `Error e -> disable_slave shared_state slave e; `Done()
	  | `Aborted -> 
	      disable_slave shared_state slave (Failure "Aborted");
	      `Done()
       )
  ) else
    (* Skip by providing a "false positive" return value *)
    eps_e (`Done ()) esys


let commit_phase1 commit_id journal shared_state t rev_id =
  (** Phase 1: Write changes to database; Start transaction on
      remote hosts; Write changes to remote hosts. Prepare
      the db transactions on remote hosts. Be as parallel
      as possible. 
   *)
  let esys = shared_state#event_system in
  let e_local = 
    commit_phase1_write_locally journal shared_state t in
  let e_remote slave =
    (commit_phase1_write_remotely slave commit_id journal shared_state rev_id)
      ++ (fun _ ->
	    commit_phase1_prepare_remotely slave commit_id shared_state
	 ) in
  let e_remotes =
    List.map e_remote shared_state#slaves in
  List.iter
    (fun e ->
       Uq_engines.when_state (* OK *)
	 ~is_error:(fun err ->
		      Netlog.logf `Crit
			"Exception from slave: %s"
			(Netexn.to_string err
			)
		   )
	 e
    )
    e_remotes;
  (msync_e (e_local :: e_remotes) esys)
    ++ (fun _ -> 
	  (** Phase 1 end: Prepare the db transaction on the local db.
              Once it is prepared, the transaction will be committed on
              restart after failure!
	      
              We also check here whether there are enough non-disabled
              slaves.
	   *)
	  Netlog.logf `Debug "Commit phase 1 done";
	  if shared_state#enough_slaves then
	    commit_phase1_prepare_locally t
	  else (
	    eps_e 
	      (`Error(Failure "Number of slaves drops below required minimum. \
                              Aborting transaction."))
	      esys
	  )
       )


let commit_phase2_commit_remotely shared_state commit_id slave =
  (** Commit remote transactions. Errors are handled as in
      commit_phase1_write_remotely
   *)
  let esys = shared_state#event_system in
  let (hp, mclient, enabled) = slave in
  if enabled then (
    Netlog.logf `Debug "Doing commit at slave %s" hp;
    (Nn_push.commit_e mclient commit_id)
    >> (function
	  | `Done _ ->
	      Netlog.logf `Debug "Successful commit at slave %s" hp;
	      `Done ()
	  | `Error e ->
	      disable_slave shared_state slave e; 
	      `Done()
	  | `Aborted ->
	      disable_slave shared_state slave (Failure "Aborted");
	      `Done()
       )
  ) else
    (* Skip by providing a "false positive" return value *)
    eps_e (`Done ()) esys


let commit_phase2 commit_id shared_state dbmods =
  (** Commit all remote transactions, and wait for ACK from all.
      If anything fails, we only disable the slave, but we do not
      roll back anything (which is impossible anyway for more than
      1 slave).
   *)
  let esys = shared_state#event_system in
  let slaves = shared_state#slaves in
  msync_e
    ( (List.map
	 (commit_phase2_commit_remotely shared_state commit_id) 
	 slaves
      )
    )
    esys
    

let journal_inodes j =
  (* Return the list of inodes that are modified by this journal. Modifications
     of directories do not count here.
   *)
  let ht = Hashtbl.create 17 in
  Queue.iter
    (function
       | `Inode_ins(inode,_)
       | `Inode_upd(inode,_)
       | `Inode_upd_time(inode,_,_)
       | `Inode_del inode
       | `Inodeblocks_ins(inode,_)
       | `Inodeblocks_del(inode,_,_) ->
	   Hashtbl.replace ht inode ()
       | _ ->
	   ()
    )
    j;
  Hashtbl.fold (fun inode _ acc -> inode::acc) ht []


let commit_e get_journal get_journal_mon post_commit
             (shared_state:Nn_state.shared_state_t) name =
  let esys = shared_state#event_system in
  let pc_e inodes (flag:bool) =
    (** The [post_commit] call must be run inside this serializer. *)
    shared_state # blockmap_serialized
      inodes
      (fun _ ->
	 post_commit flag;
	 eps_e (`Done()) esys
      )
    ++ (fun () -> eps_e (`Done flag) esys) in

  (* get_journal returns all updates of the transaction except blockmap
     updates. Only get_journal_mon returns these blockmap updates.

     The point is here that we call get_journal_mon twice. Once before
     we enter the commit monitor, and once when we are in it. It is
     possible that the blockmap data to write changes while we wait for
     the monitor (when another transaction commits to the same rows).
     This cannot result in additional or fewer blockmap rows, but the
     contents of the rows can be different.

     Here, for the purpose of getting update_characteristics, we call
     get_journal_mon for the first time.
   *)
  
  let journal = get_journal() in
  let journal_mon_0 = get_journal_mon() in
  let update_characteristics = 
    Nn_commit_monitor.get_update_characteristics [journal;journal_mon_0] in

  dlog "Nn_commit.commit_e: waiting on monitor";
  Nn_commit_monitor.monitor_e 
    shared_state#commit_monitor
    update_characteristics
    (fun commit_id ->
       dlog "Nn_commit.commit_e: monitored execution block";
       let inodes = journal_inodes journal in
       (** Catch simple case: No writes. In this case, just commit the local
	   transaction. The slaves are not contacted at all.
	*)
       if Queue.is_empty journal then
	 pc_e inodes true
       else (
	 (* If we already have too few slaves do not even start: *)
	 if not(shared_state#enough_slaves) then (
	   Netlog.logf `Alert
	     "Transaction %s: Aborting commit because the number of slaves is \
              below the minimum" name;
	   pc_e inodes false
	 )
	 else (
	   shared_state # incr_count `Commits_total;
	   let t = shared_state # db_trans_for_commit() in
	   (* Now get the blockmap modifications again, and this time we
	      add them to the main journal
	    *)
	   let journal_mon_1 = get_journal_mon() in
	   Queue.transfer journal_mon_1 journal;
	   (** Generate new revision id and push the update to journal *)
	   let prev_id = shared_state # cur_rev_ident in
	   let rev_id = shared_state # new_rev_ident() in
	   Queue.push (`Revision_upd (commit_id, rev_id)) journal;
	   
	   Netlog.logf `Info "Transaction %s: Starting commit of revision %s" 
	     name rev_id;
	   
	   (** The commit is done in two phases. Phase 1 just writes the
               changes to the databases, and prepares the commit so it
               cannot fail anymore because of SQL errors or insufficient
               disk space. If phase 1 is not successful, all db 
               transactions (local and remote) are rolled back.
	       
               Otherwise we continue with phase 2. The transactions are
               now committed: First all remote transactions, and second -
               after getting ACKs from all slaves - the local transaction.
	       
               We generally implement the policy "once the transaction is
               prepared for commit, it will be committed". This is no problem
               as we have the revision identifier as protection against
               the consequences of losing the synchronization of the 
               databases. The identifier allows us to check that all
               databases are in the same state, and which state is the latest
               one.
	       
               The user will see success if at least the local db can
               finally commit the transaction. If slaves fail in phase 2,
               we just record that, and prevent any further writes.
	    *)
	   let dbmods = 
	     List.rev
	       (Queue.fold
		  (fun acc m -> m::acc)
		  [] 
		  journal) in
	   ( if shared_state#node_config#nn_consistency_checks then
	       Nn_db.consistency_check_e dbmods name t#connection esys
	     else
	       eps_e (`Done ()) esys
	   )
	   ++ (fun () ->
		 commit_phase1 commit_id journal shared_state t prev_id)
	   ++ (fun () ->
		 let r = ref false in
		 shared_state # blockmap_serialized
		   inodes
		   (fun _ ->
		      ( commit_phase2 commit_id shared_state dbmods
			>> (function
			      | `Done _ -> `Done true
			      | `Error e ->
				  Netlog.logf `Err
				    "Transaction %s: \
                                     Rolling transaction back after error: %s"
				    name (Netexn.to_string e);
				  `Done false
			      | `Aborted ->
				  Netlog.logf `Err
				    "Transaction %s: Rolling transaction back \
                                     after abortion" name;
				  `Done false
			   )
		      )
		      ++ (fun commit_flag ->
			    if commit_flag then (
			      (** Rest of phase 2: local commit *)
			      Netlog.logf `Debug
				"Doing commit at local database";
			      (* Any error will disable the master until
				 restart! *)
			      ( t#commit_and_continue()
				>> (function
				      | `Done _ ->
					  Netlog.logf `Info
					    "Successful commit of revision %s"
					    rev_id;
					  shared_state # set_rev_ident rev_id;
					  post_commit true;
					  `Done true
				      | `Error err ->
					  Netlog.logf `Alert
					    "Cannot commit transaction in \
                                             master db: %s"
					    (Netexn.to_string err);
					  shared_state # disable_master();
					  post_commit false;
					  `Done false
				      | `Aborted -> `Aborted
				   )
			      )
			    )
			    else (
			      Netlog.logf `Debug "Rolling back at local database";
			      t#rollback()
			      ++ (fun () ->
				    Netlog.logf `Info
				      "Aborted commit of revision %s" rev_id;
				    post_commit false;
				    eps_e (`Done false) esys
				 )
			    )
			 )
		      ++ (fun flag -> r := flag; eps_e (`Done()) esys)
		   )
		 ++ (fun () -> eps_e (`Done !r) esys)
	      )
	 )
       )
    )
    

let trans_e shared_state
            (vl:Nn_state.view_t list) 
            (arg_e: 'a Uq_engines.engine) 
            name
	    : _ Uq_engines.final_state Uq_engines.engine =
  dlog "Nn_commit.trans_e: starting";
  (arg_e >>
     (function
	| `Done x -> `Done (`Done x)
	| `Error err -> `Done (`Error err)
	| `Aborted -> `Done `Aborted
     )
  )
  ++ (fun st ->
	let is_ok =
	  match st with
	    | `Done _ -> true
	    | _ -> false in
	let finish_e =
	  if is_ok then (
	    dlog "Nn_commit.trans_e: committing";
	   commit_e
	     (fun () ->  (* get_journal *)
		let q = Queue.create() in
		List.iter
		  (fun v ->
		     Queue.transfer v#pre_commit q
		  )
		  vl;
		q
	     )
	     (fun () ->  (* get_journal_mon *)
		let q = Queue.create() in
		List.iter
		  (fun v ->
		     Queue.transfer v#pre_commit_mon q
		  )
		  vl;
		q
	     )
	     (fun trans_success ->  (* post_commit *)
		List.iter
		  (fun v -> v#post_commit trans_success)
		  vl
	     )
	     shared_state
	     name
	  )
	  else (
	    dlog "Nn_commit.trans_e: aborting";
	    List.iter
	     (fun v -> v#rollback())
	      vl;
	    eps_e (`Done false) arg_e#event_system
	  ) in
	finish_e
	++ (fun committed ->
	      if committed then (
		dlog "Nn_commit.trans_e: commit done";
		eps_e (`Done(st)) arg_e#event_system
	      )
	      else (
		dlog "Nn_commit.trans_e: abort done";
		(** Keep original error if possible *)
		let st' =
		  match st with
		   | `Done _ -> 
		       `Error(Failure "Nn_commit: Commit failed")
		   | _ ->
		       `Done st in
		eps_e st' arg_e#event_system
	      )
	   )
     )
    
    
