(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Nn_alloc_types
open Printf

let dlog = Plasma_util.dlog
let dlogr = Plasma_util.dlogr

module StrSet = Set.Make(String)


exception Skip_ds


let update_ri ri inode blocks =
  let ht =
    Nn_db.update_allocinfo inode ri.Nn_blockmap.ri_blocks blocks in
  { (* ri with *)
      Nn_blockmap.ri_blocks = Hashtbl.fold (fun _ ai acc -> ai::acc) ht []
  }


let allocate_enblock inode blockmaps td ownobj repl index number pref ri =
  (** Sort the datastores by free blocks. Try to get [number] blocks from
      a single datastore (with most free blocks). If this works, go on,
      and get further [number] blocks for the replicas from other datastores.

      If there is no datastore with enough blocks, we have to split the
      allocation request: We get as many as we can, and continue our
      algorithm with this allocation as above. For the missing blocks
      we also recurse.

      Note that this algorithm may run several times in parallel, so we
      don't have a consistent view on the blockmaps!
   *)

  (* TODO: This could be just one call to the blockmap server *)
  let stats =
    List.map
      (fun bm ->
	 bm#identity, bm#stats
      )
      blockmaps in
  let stats_ht = Hashtbl.create 37 in
  List.iter
    (fun (identity, stats) ->
       Hashtbl.add stats_ht identity stats
    )
    stats;

  (** First sort datastores by free blocks: *)
  let good_blockmaps =
    List.flatten
      (List.map
	 (fun bm ->
	    try
	      let idstr = bm#identity in
	      let (n_used,n_trans,n_free) = Hashtbl.find stats_ht idstr in
	      if n_free = 0L then (
		dlogr
		  (fun () ->
		     sprintf "allocate_enblock: store %s is full" idstr);
		raise Skip_ds;
		(* Note that this is no guarantee that another process
		   does not fill up the blockmap in parallel!
		 *)
	      );
	      if not (Nn_datastore_news.is_alive idstr) then (
		dlogr
		  (fun () ->
		     sprintf "allocate_enblock: store %s is not alive" idstr);
		raise Skip_ds
	      );
	      let ds = 
		try
		  Nn_datastore_news.get bm#identity
		with Not_found -> (
		  dlogr
		    (fun () ->
		       sprintf 
			 "allocate_enblock: store %s is not monitored" idstr);
		  raise Skip_ds
		) in
	      let (h,p) =
		match ds.Nn_datastores.node  with
		  | Some addr ->
		      Plasma_util.host_port_of_sockaddr addr
		  | None ->
		      dlogr
			(fun () ->
			   sprintf 
			     "allocate_enblock: \
                                store %s has weird address" idstr);
		      raise Skip_ds in
	      let node =
		sprintf "%s:%d" h p in
	      let is_preferred =
		List.mem idstr pref in
 	      let rfactor = 
		0.001 *. float (Plasma_rng.random_int 1000) in
	      [ bm, node, is_preferred, rfactor ]
	    with
	      | Skip_ds -> []
	 )
	 blockmaps) in

  let max_free =
    List.fold_left
      (fun acc (bm,_,_,_) -> 
	 let (n_used,n_trans,n_free) = Hashtbl.find stats_ht bm#identity in
	 max acc n_free
      ) 
      0L good_blockmaps in

  dlogr (fun () ->
	   sprintf "allocate_enblock: %d good blockmaps, max_free=%Ld"
	     (List.length good_blockmaps) max_free);

  let points bm rfactor =
    (** 50%: just sort by r(andom)factor
	25%: sort by percentage of free blocks
	25%: sort by absolute number of free blocks
     *)
    let (n_used,n_free,n_trans) = Hashtbl.find stats_ht bm#identity in
    0.5 *. rfactor +. 
      0.25 *. (Int64.to_float n_free /. Int64.to_float bm#size) +.
      0.25 *. (Int64.to_float n_free /. Int64.to_float max_free) in

    let sorted_blockmaps =
    List.sort
      (fun (bm1,_,pr1,rf1) (bm2,_,pr2,rf2) -> 
	 (** Sort preferred datastores to the beginning. Second criterion
	     is the percentage of free blocks (biggest first)
	     FIXME: this is bad when nodes have just been added. They get
	     everything.
	  *)
	 let q =
	   match pr1 with
	     | true -> if pr2 then 0 else (-1)
	     | false -> if pr2 then 1 else 0 in
	 match q with
	   | 0 -> compare (points bm2 rf2) (points bm1 rf1)
	   | _ -> q
      )
      good_blockmaps in

  let create_bi bm node index block number =
    let vfy =
      Plasma_util.compute_verifier 
	td.td_safetrans_id 
	td.td_safetrans_secret
	block
	number
	true  (* FIXME *)
	true in
    let ticket =
      { Pfs_rpcapi_aux.range_start = block;
	range_length = number;
	safetrans_id = td.td_safetrans_id;
	safetrans_tmo = td.td_safetrans_tmo;
	safetrans_vfy = vfy;
	read_perm = true; (* FIXME *)
	write_perm = true;
      } in
    { Pfs_rpcapi_aux.index = index;
      node = node;
      node_alive = true;
      identity = bm#identity;
      block = block;
      length = number;
      checksum = None;
      inode_seqno = 0L;  (* not known here *)
      inode_committed = false;
      ticket = ticket;
    } in

  let rec create_bi_list bm node index blocks =
    match blocks with
      | (block,number) :: blocks' ->
	  create_bi bm node index block number :: 
	    create_bi_list bm node (Int64.add index number) blocks'
      | [] ->
	  [] in

  let rec allocate_range used_set index number repl ri =
    (** Still have to get [repl] replicas for the range 
	[index] to [index+number-1]. The datastores in [used_set]
	have already replicas, and cannot be used.
     *)
    if repl = 0 then
      Nn_blocklist.empty
    else
      allocate_rest used_set index number repl ri index number

  and allocate_rest used_set index number repl ri this_index this_number =
    (** First get blocks for [this_index] to [this_index+this_number-1],
	then for the other [repl-1] replicas (as in [allocate_range])
     *)
    let avail_blockmaps =
      List.filter
	(fun (bm,_,_,_) ->
	   not (StrSet.mem bm#identity used_set)
	)
	sorted_blockmaps in
    match avail_blockmaps with
      | (bm,node,_,_) :: _ ->
	  let blocks = bm#reserve (Int64.to_int this_number) ownobj ri in
	  let n =
	    List.fold_left (fun acc (_,n) -> Int64.add acc n) 0L blocks in
	  (** [blocks] are pairs [(startblock,number)] *)
	  dlogr
	    (fun () ->
	       sprintf 
		 "bm reserve(inode=%Ld,owner=%s): %s"
		 inode 
		 (Nn_blockmap.string_of_owner ownobj)
		 (Nn_blockmap.string_of_trl blocks));
	  let bi_list = create_bi_list bm node this_index blocks in
	  let blist = Nn_blocklist.to_blocklist bi_list in
	  if n = this_number then (
	    (* done - go on with replicas *)
	    let used_set' = StrSet.add bm#identity used_set in
	    let blist_repl =
	      allocate_range used_set' index number (repl-1) ri in
	    Nn_blocklist.merge blist blist_repl
	  )
	  else
	    if n = 0L then (
	      (* don't try this bm again: *)
	      dlogr
		(fun () ->
		   sprintf
		     "allocate_enblock: store %s is full" bm#identity);
	      let used_set' = StrSet.add bm#identity used_set in
	      allocate_rest 
		used_set' index number repl ri this_index this_number
	    )
	    else (  (* 0 < n < number *)
	      let index1 =
		Int64.add this_index n in
	      let n1 = Int64.sub this_number n in
	      let ri' = update_ri ri inode blist in
	      let blist_rest =
		allocate_rest used_set index number repl ri' index1 n1 in
	      Nn_blocklist.merge blist blist_rest
	    )
      | [] ->
	  raise No_space
  in
  allocate_range StrSet.empty index number repl ri


let alloc_limit = 128 * 1024 * 1024
  (* see also copy_in_limit *)

let get_allocsize blocksize =
  let bmaprowsize = Nn_config.bmaprowsize in
  let rows_by_limit = max 1 (alloc_limit / blocksize) in
  min bmaprowsize rows_by_limit 


let rec allocate_balanced_strategy_1 inode blockmaps td ownobj repl index number 
                                     blocksize pref ri =
  (** Use allocate_enblock in units of:
      - no more blocks than bmaprowsize
      - as many blocks as fit in alloc_limit
      - at least one block
   *)
  let allocsize = get_allocsize blocksize in
  let allocsizeL = Int64.of_int allocsize in
  let numberL = Int64.of_int number in
  if number <=  allocsize then
    allocate_enblock inode blockmaps td ownobj repl index numberL pref ri
  else (
    let blocks =
      allocate_enblock inode blockmaps td ownobj repl index allocsizeL pref 
	ri in
    let index' = Int64.add index allocsizeL in
    let number' = number-allocsize in
    let ri' = update_ri ri inode blocks in
    let blist_rest =
      allocate_balanced_strategy_1
	inode blockmaps td ownobj repl index' number' blocksize pref ri' in
    Nn_blocklist.merge blocks blist_rest
  )

let allocate_balanced_strategy ~inode blockmaps td ownobj repl index number 
                               blocksize pref ri =
  let l = 
    allocate_balanced_strategy_1
      inode blockmaps td ownobj repl index number blocksize pref ri in
  let replL = Int64.of_int repl in
  let numberL = Int64.of_int number in
  dlogr
    (fun () ->
       sprintf 
	 "allocate_balanced_strategy(index=%Ld,n=%d,repl=%d)=%s"
	 index number repl (Nn_blocklist.to_string l));
  assert(Nn_blocklist.cardinal l = Int64.mul replL numberL);
  l


