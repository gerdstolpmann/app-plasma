(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Support module for blocklists *)

open Plasma_util

module Blockinfo : 
  Mergable with type key = int64 and type t = Pfs_rpcapi_aux.blockinfo
  (** Keys are indexes! *)

module BlockRM :
  RangeMapType with type point = int64 and type value = Pfs_rpcapi_aux.blockinfo
  (** Represents blocks from a single datanode as (index) ranges [start..end] *)

type blocklist = BlockRM.t StrMap.t
  (** Common representation of blocklists: This maps datanode identities
      to range sets as represented by [BlockRM]
   *)


val to_blocklist : Pfs_rpcapi_aux.blockinfo list -> blocklist
  (** Converts any list of blocks to the official [blocklist] representation *)

val to_blockinfo_list : blocklist -> Pfs_rpcapi_aux.blockinfo list
  (** Returns the blocks for a blocklist *)

val to_string : blocklist -> string
  (** String repr for debugging *)

val empty_ticket : Pfs_rpcapi_aux.ticket
  (** A dummy ticket (used during blocklist construction) *)

(** {2 Set/map operations on blocklists} *)

val empty : blocklist
  (** an empty blocklist *)

val sub : (int64 * int64) -> blocklist -> blocklist
  (** [sub (min,max) blist]: Returns the blocklist with the subset of
      [blist] that intersects with the interval [(min,max)]
   *)

val remove : (int64 * int64) -> blocklist -> blocklist
  (** [remove (min,max) blist]: Returns the blocklist where the interval
      [(min,max)] is deleted
   *)

val merge : blocklist -> blocklist -> blocklist
  (** [merge blist1 blist2]: Merges both blocklists. If both lists contain
      a blockinfo for the same datanode identity and the same block index,
      the data from [blist2] wins and is included in the resulting 
      blocklist only. Other than this special case, this operation just
      merges the blockinfos from both arguments.
   *)

val iter : (string -> (int64 * int64) -> Pfs_rpcapi_aux.blockinfo -> unit) -> 
              blocklist -> unit
  (** [iter f blist]: Calls [f identity (min,max) bi] for all elementary
      ranges that can be compressed to one blockinfo [bi]. The 
      [identity] is just the identity from [bi]. In [min] and [max] the
      covered range of indexes is passed.
   *)

val fold : (string -> (int64 * int64) -> Pfs_rpcapi_aux.blockinfo -> 'a -> 'a)
            -> blocklist -> 'a -> 'a
  (** Fold operator *)

val map : (Pfs_rpcapi_aux.blockinfo -> Pfs_rpcapi_aux.blockinfo) -> 
          blocklist -> blocklist
  (** [map f blist]: Returns a blocklist where each element is mapped
      via [f]
   *)

val domain : blocklist -> (int64*int64) list
  (** Returns the list of ranges covered by the blocklist (i.e. the
      union of all ranges for all datanode identities)
   *)

val cardinal : blocklist -> int64
  (** The number of covered blocks (incl. replicas) *)

(*
val fragmentation : blocklist -> float
  (** For a given identity, the fragmentation is the percentage of blocks
      where the successor is stored "too far away".

      For the block list, the maximum of the fragmentation numbers of the
      identities is taken.

      Result: between 0.0 and 1.0
   *)
 *)

(*
val identities_by_volume : blocklist -> string list
   (** Returns the identities occurring in the list. The identities are
       sorted by volume (the number of blocks).
    *)
 *)
      
