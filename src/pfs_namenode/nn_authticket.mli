(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Management of authentication tickets *)

val create : string -> int64
  (** Create a new authentication ticket (verifier) for this user ID.

      Raises [Not_found] if this user does not exist.
   *)

val renew : int64 -> unit
  (** Renew the ticket. (Does not fail if the ticket is invalid.) *)

val principal : int64 -> Unix.passwd_entry * string * Plasma_util.StrSet.t
  (** Describe the principal behind the ticket as pair
      [(pw_entry, main_group, groups)]. Raises [Not_found] if
      the ticket does not exist or has timed out.
   *)

val delete : int64 -> unit

val post_start_hook : Netmcore.res_id -> unit
  (** [pool] *)
