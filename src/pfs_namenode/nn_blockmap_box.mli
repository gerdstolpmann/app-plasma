(*
  Copyright 2011 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Fast RPC to the blockmap server via camlboxes *)

open Nn_blockmap

class type blockmap_client_t =
object
  method access : id:int -> identity:string -> size:int64 -> bool * blockmap_t
    (** Access a blockmap stored in DB row [id] and for datastore [identity],
	with an initial [size].

	The bool return flag is true if the blockmap has just been created
	as empty structure and needs now to be populated.
     *)

  method inactivate : id:int -> unit
    (** Inactivate *)

  method shutdown : unit -> unit
    (** Shut the process down *)

  method close : unit -> unit
    (** Close the client (deallocate the message box) *)
end


val client : unit -> blockmap_client_t 
  (** Create a client *)

val serve : unit -> unit
  (** Make this process a blockmap server.

      It is required that {!Nn_db} is already initialized, because the database
      needs to be accessed.

      The server is unable to detect the shutdown of the system. Because of
      this, one has to call [shutdown]
   *)

val init : unit -> unit
  (** Initializations to be run before the server is started.
      This must be called in container context, though
   *)

