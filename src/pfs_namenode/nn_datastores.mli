(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Datastore types *)

type datastore =
    { id : int;                (** ID in the database *)
      identity : string;       (** identity string *)
      size : int64;            (** size in blocks *)
      enabled : bool;          (** whether administratively enabled *)
      node : Unix.sockaddr option; (** whether a datanode has this store *)
      alive : bool;            (** Whether it is known to be alive *)
    }


type discovered =
    { d_identity : string;
      d_size : int64
    }

val null_datastore : datastore

val to_xdr : datastore -> Pfs_rpcapi_aux.ds_info
val of_xdr : Pfs_rpcapi_aux.ds_info -> datastore
  (** Convert [datastore] to/from the wire representation *)


(**/**)

(* internally used: *)
type datastores = datastore list

module Datastores_var : Netplex_cenv.VAR_TYPE with type t = datastores

module Datastores_shvar : Netplex_cenv.VAR_TYPE with type t = datastores

val shdsname : string
