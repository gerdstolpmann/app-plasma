(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf
open Nn_datastores

let dlogf = Plasma_util.dlogf

module Rpcapi_clnt = Pfs_rpcapi_clnt
module Rpcapi_srv = Pfs_rpcapi_srv
module Rpcapi_aux = Pfs_rpcapi_aux

exception Unavailable

module Bool_var =
  Netplex_cenv.Make_var_type(struct type t = bool end)

let up2datename = "current_is_up_to_date"
  (* false: got a notification, and current_datastores needs to be updated
     true: it is still up to date
     unset: never got a notification
   *)

let dsname = "current_datastores"

let force() =
  Bool_var.set up2datename false


let receive_message _ n arg =
  if n = shdsname then (
    match arg with
      | [| "have_monitor_news" |] ->
	  Bool_var.set up2datename false;
	  Netlog.logf `Info "Got notification about monitor status change"
      | _ ->
	  ()
  )

let rec list ?(wait=false) () =
  try
    let up2date = 
      try Bool_var.get up2datename with _ -> false in
    if up2date then (
      let l = Datastores_var.get dsname in
      (*
      dlogf
	"Nn_datastore_news.list: responding from cache (n=%d)"
	(List.length l);
       *)
      l
    )
    else (
      let l = Datastores_shvar.get shdsname in
      Datastores_var.set dsname l;
      Bool_var.set up2datename true;
      dlogf
	"Nn_datastore_news.list: fetching from shared variable (n=%d)"
	(List.length l);
      l
    )
  with
    | _ ->
	if wait then (
	  dlogf
	    "Nn_datastore_news.list: retrying in 1 second";
	  Unix.sleep 1;
	  list ~wait ()
	)
	else
	  raise Unavailable


let get ?(wait=false) idstr =
  List.find 
    (fun ds -> ds.identity = idstr)
    (list ~wait ())

let is_alive ?(wait=false) idstr = 
  try
    let ds = get ~wait idstr in
    ds.alive && ds.node <> None
  with
    | Not_found -> false

let post_add_hook _ ctrl =
  ctrl # add_plugin Netplex_sharedvar.plugin

let signal_db_change id node_opt =
  let msg =
    match node_opt with
      | None ->
	  [| "have_db_news"; string_of_int id |]
      | Some node ->
	  let (h,p) = Plasma_util.host_port_of_sockaddr node in
	  let hp = sprintf "%s:%d" h p in
	  [| "have_db_news_discover"; string_of_int id; hp |] in
  (** The receiver of this message is {!Nn_monitor.receiver_message} *)
  Netplex_cenv.send_message "Nn_*" shdsname msg;
  Netlog.logf `Info "Notifying monitor about db update"

