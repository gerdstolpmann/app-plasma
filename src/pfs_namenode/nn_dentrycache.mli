(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** The dentrycache checks whether directory metadata is still up to date.
 *)

open Pfs_rpcapi_aux

val lookup : int64 -> string -> int64
  (** [lookup dir_inode name]: Looks up the name in the cache, or raises
      [Not_found] *)

val update : int64 -> string -> int64 -> unit
  (** Updates the cache with this info *)

val delete : int64 -> string -> unit
  (** Deletes this entry from the cache *)

val post_start_hook : Netmcore.res_id -> unit
  (** [pool] *)
