(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Block allocation algorithm *)

val get_allocsize : int -> int
  (** Get the allocation size for the blocksize. The allocation size is the
      number of blocks that are allocated at once
   *)

val allocate_balanced_strategy : 
      inode:int64 ->
      Nn_blockmap.blockmap_t list ->
      Nn_alloc_types.transacted_datastores ->
      Nn_blockmap.owner ->
      int ->
      int64 ->
      int ->
      int ->
      string list ->
      Nn_blockmap.reserve_info ->
         Nn_blocklist.blocklist
  (** [allocate_balanced_strategy inode bmaps td owner repl index number bs pref]:
      Allocates blocks for the index range [index] to [index+number-1].
      The replication factor is [repl]. The blocks are allocated in the
      passed blockmaps [bmaps], and will be assigned to [owner].
      Tickets are generated for the transaction identified by [td].
      In [bs] the blocksize must be passed. The list [pref] can contain
      the identities of preferred datanodes; if possible blocks are
      allocated there.

      This algorithm allocates blocks in ranges, and tries to get a
      balanced allocation with respect to the datanodes.

      May raise {!Nn_alloc_types.No_space}.
   *)

(*
val repair_blocklist :
      inode:int64 ->
      Nn_blockmap.blockmap_t list ->
      Nn_alloc_types.transacted_datastores ->
      Nn_blockmap.owner ->
      int ->
      int64 ->
      int ->
      int ->
      string list ->
      string list ->
      Nn_blocklist.blocklist ->
         Nn_blocklist.blocklist
  (** [repair_blocklist inode bmaps td owner repl index number bs pref decom
       in_blist]
      Repair the existing block list [in_blist]:
       - find replacement blocks for dead nodes
       - find replacement blocks for identities in [decom], decommissioned nodes
       - find additonal blocks if the repliaction factor is increased

      The input block list [in_blist] must not have holes (these would be
      treated as zero-replicated regions, causing a failure).

      [repl] is the target replication factor. The output blocklist guarantees
      this factor (if impossible to reach this, the function will fail).

      New blocks are preferably allocated on the identities in [pref].

      Failures:
       - {!Nn_alloc_types.No_space]
       - {!Nn_alloc_types.Dead_block]

   *)

 *)
