(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Election of the coordinator *)

type role =
    [ `Coordinator | `Slave | `Excluded | `Unknown ]

val string_of_role : role -> string

val get_role : unit -> role
  (** Return the role. This is [`Unknown] if the election is not yet
      finished, and [`Coordinator] or [`Slave] otherwise.

      Can be called from any Netplex container process.
   *)

val get_coordinator : unit -> string
  (** Return the coordinator in "host:port" syntax. This is only known if
      the role is [`Coordinator] or [`Slave].

      Can be called from any Netplex container process.
   *)

val elect_e : Nn_config.nn_node_config -> 
              int -> 
              Pfs_auth.client_auth ->
              Unixqueue.event_system -> 
                (int option * string) Uq_engines.engine
  (** Do the election. The int arg is the port number of this server.

      Returns [(index_opt, rev_ident)]: 
      [index_opt] is the index of the coordinator in the [nn_nodes] list
      of the configuration object {!Nn_config.nn_node_config}, but only
      if this machine is the coordinator. [None] otherwise.
      [rev_ident] is the revision string.
   *)

val setup_elect_api : Rpc_server.t -> 
                      Nn_config.nn_node_config -> 
                        unit
  (** Set up the RPC server so that [Elect] procedures are bound *)

val stop_election : unit -> unit
  (** Stops any ongoing election (in the current process only) *)

val stop_election_msg : string
  (** The name of a parameterless message for [stop_election]. This message
      is sent around to call [stop_election] in every manager process.
   *)
