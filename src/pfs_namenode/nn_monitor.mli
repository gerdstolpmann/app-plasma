(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Management of datastores, i.e. which datastores are available on which
    datanodes

    This module is run inside the single [nn_monitor] process. Read access
    from other processes is possible via the {!Nn_datastore_news} module
    (using a pub/sub scheme).

    The module can also be notified to merge db changes into its in-core
    set of datastores.
 *)

open Nn_datastores

val list : unit -> datastore list
  (** Returns the list of all datastores *)

val get : string -> datastore
  (** Get the store record by identity string, or raise [Not_found] *)

val add : datastore -> unit
  (** Add the datastore record as-is (for initializing this module from db). *)

val update_from_db : ?when_done:(unit -> unit) -> int -> unit
  (** Read the db version of this record, and trigger the update of the
      module copy of it. The actual update will happen at some point in the
      future.
   *)

val delete : string -> unit
  (** [delete identity]: Remove this store *)

val enable : string -> unit
  (** [enable identity]: Sets [enabled] to true *)

val disable : string -> unit
  (** [disable identity]: Sets [enabled] to false *)

val is_alive : string -> bool
  (** [is_alive identity]: Returns whether the datanode having this store
      is good as seen by the monitor. Fails if the monitor is not started.
   *)

(*
val discover : ?when_done:(discovered option -> unit) -> 
                Unix.sockaddr -> Pfs_auth.client_auth ->
                  unit
  (** [discover addr bs]: Contacts this datanode and gets the identity. If
      the identity was added before, the store's location is set to this
      address.

      This function can only be called within a Netplex component. The
      discovery is actually only triggered. If the monitor is also running,
      the datanode is enabled for being monitored.

      In order to qualify, the datanode must have at least the size as
      configured in the [datastore] record. It is no error if the node is
      bigger. Also, the blocksize must be [bs].

      [when_done]: If passed, this function is called back when the
      discovery is done. If the argument is [None] it was not successful.
      If it is [Some d], the record [d] contains the discovered identity
      and the size as reported by the datanode.
   *)
 *)

val start_publishing : unit -> unit
  (** Updates on the datastores are published and can be retrieved from
      any other process using {!Nn_datastore_news}
   *)

(* val stop_publishing : unit -> unit *)
  (** Stops feeding the news module *)


(* Future extension:

   mcast_discover : Unix.sockaddr -> float -> unit

   Starts multicast discovery with a certain timeout value
 *)

val post_add_hook : Netplex_types.socket_service -> 
                    Netplex_types.controller -> unit
  (** Needs to be called from the [post_add_hook] *)

val receive_message : Pfs_auth.client_auth -> Netplex_types.container -> 
                      string -> string array -> unit
  (** Needs to be called from the [receive_message] hook in order to
      arrange notifications about db updates
   *)

val post_start_hook : Netplex_types.container -> Nn_config.nn_node_config -> 
                      Pfs_auth.client_auth ->
                      unit
  (** Reads the datastore table from the database, and starts discovering
      datanodes.

      It is required that {!Nn_db.init} was called before.
   *)

val shutdown : unit -> unit
  (** To be called at shutdown time *)
