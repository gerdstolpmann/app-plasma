(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** The main module for the slaves. Defines RPC services
    for [Nameslave], and [Coordination].

    The programs [Filesystem] and [Dn_admin] are also "implemented", but all
    calls  (except [null]) fail.
 *)

(** Implementation of [Nameslave] *)

open Uq_engines.Operators
open Printf

let dlogf = Plasma_util.dlogf
let dlogr = Plasma_util.dlogr

type slave_manager =
    { trans_tbl : (int, Nn_db.transaction * Rpc_server.connection_id) Hashtbl.t;
        (* maps [commit_id] to [(trans,conn_id)]. The entry exists only while
	   the transaction is pending
	 *)
      trans_unused : Nn_db.transaction Queue.t;
        (* unused DB "transactions" (actually connections to DB server) *)
    }

module Slave_manager_var =
  Netplex_cenv.Make_var_type(struct type t = slave_manager end)

let get_sm() =
  Slave_manager_var.get "current_slave_manager"


let set_sm t =
  Slave_manager_var.set "current_slave_manager" t


let rec get_new_db_trans commit_id =
  (* Get an unused or new Nn_db.transaction *)
  let sm = get_sm() in
  try
    let t = Queue.take sm.trans_unused in
    if not t#is_returned then
       get_new_db_trans commit_id
    else
      t
  with
    | Queue.Empty ->
	let esys = (Netplex_cenv.self_cont()) # event_system in
	let t = Nn_db.transact esys commit_id in
	t


let get_db_trans_of proc_name commit_id =
  let sm = get_sm() in
  try
    fst(Hashtbl.find sm.trans_tbl commit_id)
  with
    | Not_found ->
	failwith (proc_name ^ ": no committer with ID " ^ 
		    string_of_int commit_id)


let post_start_hook cont =
  set_sm
    { trans_tbl = Hashtbl.create 5;
      trans_unused = Queue.create()
    }


let proc_begin_transaction conf sess (commit_id,cname,exp_rev) emit =
  dlogr (fun () -> sprintf "proc_begin_transaction id=%d" commit_id);
  if cname <> conf#nn_clustername then
    failwith "Nameslave.begin_transaction: bad clustername";
  let conn_id = Rpc_server.get_connection_id sess in
  let sm = get_sm() in
  if Hashtbl.mem sm.trans_tbl commit_id then
    failwith "Nameslave.begin_transaction: Already in a transaction";
  (* let esys = (Netplex_cenv.self_cont()) # event_system in *)
  let t = get_new_db_trans commit_id in
  Hashtbl.add sm.trans_tbl commit_id (t, conn_id);
  (* FIXME: check revision ID again *)
(*
  let e = Nn_db.revision_get_e t#connection esys in
  Uq_engines.when_state   (* OK *)
    ~is_done:(fun rev ->
		if rev = exp_rev then (
		  set_conn_id conn_id;
		  emit ()
		)
		else (
		  Netlog.logf `Err
		    "Revision string does not have the expected value";
		  Netlog.logf `Err
		    "Refusing to start transaction";
		  t#return();
		  Rpc_server.reply_error sess Rpc.System_err
		)
	     )
    ~is_error:(fun err ->
		 Netlog.logf `Err
		   "begin_transaction: Error %s"
		   (Netexn.to_string err);
		 t#return();
		 Rpc_server.reply_error sess Rpc.System_err
	      )
    e
 *)
  emit ()


let proc_prepare_commit sess commit_id emit =
  dlogr (fun () -> sprintf "proc_prepare_commit id=%d" commit_id);
  let t = get_db_trans_of "Nameslave.prepare_commit" commit_id in
  let e =
    new Uq_engines.map_engine 
      ~map_done:(fun () -> `Done true)
      ~map_error:(fun error ->
		    Netlog.logf `Err
		      "Nameslave.prepare_commit: Error %s"
		      (Netexn.to_string error);
		    `Done false)
      ~map_aborted:(fun () ->
		      Netlog.logf `Err
			"Nameslave.prepare_commit: Aborted";
		      `Done false)
      (t#prepare()) in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun ok ->
		try emit ok
		with Rpc_server.Connection_lost -> ()
	     )
    e
    

let proc_commit sess commit_id emit =
  dlogr (fun () -> sprintf "proc_commit id=%d" commit_id);
  let t = get_db_trans_of "Nameslave.commit" commit_id in
  let e = t#commit_and_continue() in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun () ->
		let sm = get_sm() in
		Queue.add t sm.trans_unused;
		Hashtbl.remove sm.trans_tbl commit_id;
		try emit()
		with Rpc_server.Connection_lost -> ()
	     )
    ~is_error:(fun error ->
		 Netlog.logf `Err
		   "Nameslave.commit: Error %s"
		   (Netexn.to_string error);
		 Rpc_server.reply_error sess Rpc.System_err)
    ~is_aborted:(fun () ->
		   Netlog.logf `Err
		     "Nameslave.commit: Aborted";
		   Rpc_server.reply_error sess Rpc.System_err)
    e


let push procname commit_id f sess emit =
  dlogr (fun () -> sprintf "proc_%s id=%d" procname commit_id);
  let t = get_db_trans_of ("Nameslave." ^ procname) commit_id in
  let esys = (Netplex_cenv.self_cont()) # event_system in
  let e =
    f t#connection esys in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun () ->
		try emit ()
		with Rpc_server.Connection_lost -> ()
	     )
    ~is_error:(fun error ->
		 Netlog.logf `Err
		   "Nameslave.%s: Error %s"
		   procname (Netexn.to_string error);
		 Rpc_server.reply_error sess Rpc.System_err)
    ~is_aborted:(fun () ->
		   Netlog.logf `Err
		     "Nameslave.%s: Aborted" procname;
		   Rpc_server.reply_error sess Rpc.System_err)
    e


(* CHECK: maybe we can optimize here a bit - by sending the void result
   immediately. We would then synchronize first at prepare_commit time.
 *)


let proc_set_ds_cache sess l emit =
  Nn_db.ds_cache_init
    (`Datastores
       (List.map
	  Nn_datastores.of_xdr
	  (Array.to_list l)));
  emit ()

let proc_push sess (commit_id,m) emit =
  push
    "push"
    commit_id
    (Nn_db.exec_e (m :> Nn_db.modification))
    sess
    emit
    

let onclose_nameslave srv conn_id =
  (** If the TCP connection is closed, implicitly roll the transaction back *)
  let sm = get_sm() in
  let l = ref [] in
  Hashtbl.iter
    (fun commit_id (t,cid) ->
       if cid = conn_id then (
	  Netlog.logf `Info
	    "Rolling transaction back after TCP close";
	  ignore(t#rollback());
	 l := commit_id :: !l
       )
    )
    sm.trans_tbl;
  List.iter
    (fun commit_id -> Hashtbl.remove sm.trans_tbl commit_id)
    !l


(** Binding RPC servers: *)

let setup srv (conf:Nn_config.nn_node_config) =
  (* TODO: Pfs_rpcapi_srv.Filesystem.V1.bind_async ... *)
  Pfs_rpcapi_srv.Coordination.V1.bind
    ~proc_null:(fun () -> ())
    ~proc_find_coordinator:(fun cname ->
			      if cname = conf#nn_clustername then
				Some(Nn_elect.get_coordinator())
			      else
				None)
    ~proc_find_namenodes:(fun cname ->
			    if cname = conf#nn_clustername then
			      Array.of_list conf#nn_nodes
			    else
			      [| |])
    ~proc_is_coordinator:(fun cname -> false)
    ~proc_clustername:(fun () -> conf#nn_clustername)
    ~proc_local_identities:(fun _ -> [| |])  (* not supported here *)
    srv;
  let no_dn_admin name sess _ emit =
    failwith ("Dn_admin." ^ name ^ ": Not available on slave node") in

  Pfs_rpcapi_srv.Dn_admin.V1.bind_async
    ~proc_null:(fun sess () emit -> emit())
    ~proc_add_datanode:(no_dn_admin "add_datanode")
    ~proc_enable_datanode:(no_dn_admin "enable_datanode")
    ~proc_disable_datanode:(no_dn_admin "disable_datanode")
    ~proc_is_enabled:(no_dn_admin "is_enabled")
    ~proc_is_alive:(no_dn_admin "is_alive")
    ~proc_fsstat:(no_dn_admin "fsstat")
    ~proc_lookup:(no_dn_admin "lookup")
    ~proc_datanodes:(no_dn_admin "datanodes")
    ~proc_destroy_datanode:(no_dn_admin "destroy_datanode")
    srv;

  Pfs_rpcapi_srv.Nameslave.V1.bind_async
    ~proc_null:(fun _ _ emit -> dlogf "null"; emit ())
    ~proc_begin_transaction:(proc_begin_transaction conf)
    ~proc_prepare_commit
    ~proc_commit
    ~proc_set_ds_cache
    ~proc_push
    srv;

  Rpc_server.set_onclose_action srv (onclose_nameslave srv)
