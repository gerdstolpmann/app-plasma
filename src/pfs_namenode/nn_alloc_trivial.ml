(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Nn_alloc_types
open Printf

let dlogf = Plasma_util.dlogf

(** Trivial algorithm: Do every block separately. *)

exception Skip_ds

let allocate_trivial_strategy blockmaps td ownobj repl index number =
  let blocks = ref [] in
  for k = 0 to number-1 do
    dlogf
      "allocate_trivial_strategy: allocating block %d/%d" k number;
    let index_k =
      Int64.add index (Int64.of_int k) in
    (** So simply iterate until we have our stores... *)
    let d = ref 0 in
    ( try
	List.iter
	  (fun bm ->
	     let idstr = bm#identity in
	     dlogf 
	       "allocate_trivial_strategy: looking at store %s" idstr;
	     try
	       (** Check whether bm is ok. *)
	       if not (Nn_datastore_news.is_alive idstr) then (
		 dlogf 
		   "allocate_trivial_strategy: store %s is not alive" idstr;
		 raise Skip_ds
	       );
	       let ds = 
		 try
		   Nn_datastore_news.get bm#identity
		 with Not_found -> (
		 dlogf 
		   "allocate_trivial_strategy: store %s is not monitored" idstr;
		   raise Skip_ds
		 ) in
	       let ri = Nn_blockmap.create_reserve_info() in
	       let new_blocks = bm # reserve 1 ownobj ri in
	       if new_blocks = [] then (
		 dlogf 
		   "allocate_trivial_strategy: store %s is full" idstr;
		 raise Skip_ds;
	       );
	       let new_block,_ = List.hd new_blocks in
	       let node =
		 match ds.Nn_datastores.node  with
		   | Some addr ->
		       let (h,p) = Plasma_util.host_port_of_sockaddr addr in
		       sprintf "%s:%d" h p
		   | None ->
		       dlogf 
			 "allocate_trivial_strategy: \
                          store %s has weird address" idstr ;
		       raise Skip_ds in
	       let vfy =
		 Plasma_util.compute_verifier 
		   td.td_safetrans_id 
		   td.td_safetrans_secret
		   new_block
		   1L
		   true  (* FIXME *)
		   true in
	       let ticket =
		 { Pfs_rpcapi_aux.range_start = new_block;
		   range_length = 1L;
		   safetrans_id = td.td_safetrans_id;
		   safetrans_tmo = td.td_safetrans_tmo;
		   safetrans_vfy = vfy;
		   read_perm = true; (* FIXME *)
		   write_perm = true;
		 } in
	       let bi =
		 { Pfs_rpcapi_aux.index = index_k;
		   node = node;
		   node_alive = true;
		   identity = bm#identity;
		   block = new_block;
		   length = 1L;
		   checksum = None;
		   inode_seqno = 0L;  (* not known here *)
		   inode_committed = false;
		   ticket = ticket;
		 } in
	       blocks := bi :: !blocks;
	       incr d;
	       if !d >= repl then raise Exit
	     with
	       | Skip_ds -> ()
	       | Nn_blockmap.Inactive -> 
		   (** destroyed blockmap *)
		   dlogf 
		     "allocate_trivial_strategy: \
                      store %s is inactive" idstr ;
	  )
	  blockmaps
      with
	| Exit -> ()
    );
    if !d < repl then (
      dlogf
	"allocate_trivial_strategy: \
         found blocks in %d stores (%d expected) - ENOSPC"
	!d repl;
      raise No_space
    )
  done;
  Nn_blocklist.to_blocklist !blocks
