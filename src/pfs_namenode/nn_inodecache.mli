(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** The inodecache checks whether inode metadata is still up to date.
    It requests notifications from the main nameserver.
 *)

open Pfs_rpcapi_aux

val lookup : int64 -> inodeinfo
  (** Looks up the inode in the cache, or raises [Not_found] *)

val update : int64 -> inodeinfo -> unit
  (** Updates the inode with this inodeinfo *)

val delete : int64 -> unit
  (** Deletes this inode from the cache *)

val post_start_hook : Netmcore.res_id -> unit
  (** [pool] *)


(*
val factory : unit -> Netplex_types.processor_factory
  (** The factory for the processor "nn_inodecache". The protocol must be
      "RPC". There must be an address for this protocol with 
      [type = "container"].

      This service supports multiple processes.
   *)
 *)
