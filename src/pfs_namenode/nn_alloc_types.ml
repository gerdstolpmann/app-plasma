(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Types for allocation algorithms *)

type rpolicy =
    [ `Balanced of int ]
      (** - [`Balanced factor]: Blocks are allocated on all datanodes,
            so that the nodes fill up similarly. The int is the
            replication factor
       *)


type transacted_datastores =
    { td_idlist : int list;
        (** IDs of datastores participating in a transaction *)
      td_safetrans_id : int64;
        (** safetrans id *)
      td_safetrans_secret : int64;
        (** per transaction secret *)
      td_safetrans_tmo : int64;
        (** timeout for data transaction *)
    }


exception No_space
  (** No space in the filesystem - all blocks are used *)
