(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Plasma_util.Operators
open Printf

let dlogf = Plasma_util.dlogf

module Elect_proxy = Pfs_rpcapi_clnt.Make'Elect(Rpc_proxy.ManagedClient)

type role =
    [ `Coordinator | `Slave | `Excluded | `Unknown ]

let string_of_role =
  function
    | `Coordinator -> "coordinator"
    | `Slave       -> "slave"
    | `Excluded    -> "excluded"
    | `Unknown     -> "unknown"

module Role_var =
  Netplex_sharedvar.Make_var_type(struct type t = role end)

module String_var =
  Netplex_sharedvar.Make_var_type(struct type t = string end)

module Ann_rnd_var =
  Netplex_sharedvar.Make_var_type(struct type t = int64 array end)

(** For simplicity we use global variables to store the current state.
    The election is only started once, and there is absolutely no
    concurrency.
 *)

let role = ref (`Unknown : role)
  (** The role. This is also stored in a shared variable [rolename] *)

let rolename = "plasma.namenode.manager.current_role"

let revision = ref None
  (** The revision from the db *)

let coordinator = ref None
  (** Local copy of the coordinator address *)

let coordname = "plasma.namenode.manager.current_coordinator"
  (** The coordinator *)

let ann_rnd_name = "plasma.namenode.manager.ann_random"
  (** The random value for the election *)

let cancel_announcements = ref (fun () -> ())
  (** This function will be initialized after starting the elect engine,
      so that calling it cancels the announcements. This is done when
      the [set_coordinator] RPC of this process is called.
   *)

let stop_election() =
  !cancel_announcements()

let stop_election_msg = "plasma.namenode.manager.stop_election"

let get_ann_random() =
  (** Random numbers for self-identification during the election *)
  if Netplex_sharedvar.create_var ~enc:true ann_rnd_name then (
    let rnd = [| Plasma_rng.random_int64(); Plasma_rng.random_int64() |] in
    Ann_rnd_var.set ann_rnd_name rnd;
    rnd
  )
  else (
    ignore(Netplex_sharedvar.wait_for_enc_value ann_rnd_name);
    Ann_rnd_var.get ann_rnd_name
  )


let get_role() = 
  (** If the role is [`Unknown] we have to look into the shared variable.
      Otherwise just use our local copy (values other than [`Unknown] never
      change again)
   *)
  if !role = `Unknown then (
    let r =
      try
	Role_var.get rolename
      with _ -> `Unknown    (** the shared var is not yet initialized *)
    in
    role := r;
    r
  )
  else
    !role


let set_role r =
  role := r;
  ignore(Netplex_sharedvar.create_var ~enc:true rolename);
  Role_var.set rolename r


let get_coordinator() =
  match !coordinator with
    | None ->
	( try
	    let c = String_var.get coordname in
	    coordinator := Some c;
	    c
	  with
	    | _ ->
		failwith "Nn_elect.get_coordinator"
	)
    | Some c -> c


let get_revision () =
  match !revision with
    | None ->
	let esys = Unixqueue.create_unix_event_system() in
	let db = Nn_db.transact esys 0 in   (* FIXME: commit_id *)
	let e =
	  Nn_db.revision_get_e db#connection esys
	  ++ (fun rev ->
		Nn_db.revision_get_checksum_e db#connection esys
		++ (fun cksum ->
		      Netlog.logf `Info
			"Election: Using local revision %s, checksum %s"
			rev cksum;
		      revision := Some(rev,cksum);
		      db#commit()
		      ++ (fun () -> eps_e (`Done(rev,cksum)) esys)
		   )
	     ) in
	Unixqueue.run esys;
	( match e#state with
	    | `Done(rev,cksum) -> (rev,cksum)
	    | `Error err ->
		Netlog.logf `Crit
		  "Election: Exception %s" (Netexn.to_string err);
		db#return();
		raise err
	    | _ ->
		assert false
	)
    | Some(rev,cksum) ->
	(rev,cksum)


let rec send_announcements_stream_e count conn announcement cauth esys =
  (** Send the announcement to the remote server endlessly until a response
      comes back
   *)
  let rcache_config =
    Rpc_proxy.ReliabilityCache.create_rcache_config
      ~policy:`None () in
  let rcache =
    Rpc_proxy.ReliabilityCache.create_rcache rcache_config in
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Elect.V1._program ]
      ~msg_timeout:1.0
      ~msg_timeout_is_fatal:false
      ~initial_ping:false
      ~exception_handler:(fun err -> 
			    Netlog.logf `Warning
			      "non-critical RPC exception: %s"
			      (Netexn.to_string err))
      ~rcache
      ~auth_methods:(Pfs_auth.rpc_proxy_auth_methods cauth true)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  ( Plasma_util.rpc_engine
      mclient
      Elect_proxy.V1.announce'async
      announcement
    >> (function
	  | `Done ok -> 
	      `Done(Some ok)
	  | `Error err -> 
	      `Done None
	  | `Aborted ->
	      `Done None
       )
  )
  ++ (fun ok_opt ->
	let sa_opt =
	  match Rpc_proxy.ManagedClient.mclient_state mclient with
	    | `Up sa_opt -> sa_opt
	    | _ -> None in
	Rpc_proxy.ManagedClient.shut_down mclient;
	match ok_opt with
	  | None ->
	      (** Just repeat. We introduce here a delay to avoid
		  a busy wait
		   *)
	      Plasma_util.delay_engine
		1.0 
		(fun () ->
		   (** Note that this recursion is problematic when
                       run too often - after all we are wrapping engines
                       in engines endlessly
		    *)
		   send_announcements_stream_e 
		     (count+1) conn announcement cauth esys)
		esys
	  | Some ok ->
	      (** We have a final result *)
	      let sa =
		match sa_opt with
		  | None -> assert false
		  | Some sa -> sa in
	      eps_e (`Done (ok, sa)) esys
     )


let send_annocunements nn_config port cauth esys =
  (** port: namenode port of this server *)
  let (rev, _) = get_revision() in
  let announcement =
    { Pfs_rpcapi_aux.ann_clustername = nn_config#nn_clustername;
      ann_sender = sprintf "%s:%d" nn_config#nn_hostname port;
      ann_eligible = Array.of_list nn_config#nn_nodes;
      ann_revision = rev;
      ann_rank = nn_config # nn_rank;
      ann_random = get_ann_random()
    } in
  let nodes =
    List.map Plasma_util.parse_host_port nn_config#nn_nodes in
  (** Start sending announcements to all neighbor namenodes. *)
  let replying_nodes = ref [] in
  let all_ok = ref true in
  let canceled = ref false in
  let my_ip_self = ref None in    (* My IP when self-communicating *)
  let my_ip_others = ref None in  (* My IP seen by others *)
  let e =
    Plasma_util.msync_engine
      (List.map
	 (fun (h,p) ->
	    let conn = 
	      Plasma_util.connector_of_sockaddr
		(Plasma_util.sockaddr_of_host_port (h,p)) in
	    send_announcements_stream_e 0 conn announcement cauth esys
	    >> (function
		  | `Done (`ann_self, sa) ->
		      (* Sent this to myself... Take the chance and
			 extract my IP address
		       *)
		      let ip = Plasma_util.ip_of_sockaddr sa in
		      my_ip_self := Some ip;
		      `Done ()
		  | `Done ((`ann_accept | `ann_reject as ok), sa) ->
		      Netlog.logf `Info
			"Election: Namenode %s:%d %s"
			h p
			(match ok with
			   | `ann_accept -> "approves"
			   | `ann_reject -> "objects"
			);
		      all_ok := !all_ok && (ok = `ann_accept);
		      replying_nodes := (h,p) :: !replying_nodes;
		      let ip = Plasma_util.ip_of_sockaddr sa in
		      ( match !my_ip_others with
			  | None -> 
			      my_ip_others := Some ip
			  | Some ip' when ip' = ip ->
			      ()
			  | Some ip' ->
			      Netlog.logf `Warning
				"This machine has several IP addresses"
		      );
		      `Done ()
		  | (`Error _|`Aborted) as other ->
		      other
	       )
	 )
	 nodes
      )
      esys in
  (** We cancel [e] after some time - only nodes that responded until then
      will be considered. It is also possible that [e] is stopped because
      somebody called [cancel_announcements]
   *)
  let g = Unixqueue.new_group esys in
  Unixqueue.weak_once esys g nn_config#nn_elect_timeout
    (fun () -> e#abort() );
  cancel_announcements :=
    (fun () -> canceled := true; e#abort() );
  (** If [e] is finished (either fully successful, or aborted), and there
      was no cancellation, we return all nodes that responded and are
      alive.
   *)
  e >>
    (function
       | `Done () 
       | `Aborted ->
	   if not !canceled && !all_ok then (
	     let ip =
	       match !my_ip_others, !my_ip_self with
		 | Some ip, _ -> ip
		 | None, Some ip -> ip  (* maybe only one namenode *)
		 | None, None ->
		     Netlog.logf `Crit
		       "Could not determine my own IP address";
		     failwith "No IP address" in
	     `Done (Some (!replying_nodes, ip))
	   )
	   else
	     `Done None
       | `Error e ->
	   Netlog.logf `Err
	     "Nn_elect.send_announcements: Exception %s"
	     (Netexn.to_string e);
	   `Error e
    )


let set_coordinator_at nn_config cauth rev cksum pref_name (h,p) esys =
  dlogf "set_coordinator_at h=%s p=%d" h p;
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Elect.V1._program ]
      ~msg_timeout:60.0
      ~msg_timeout_is_fatal:true
      ~initial_ping:false
      ~auth_methods:(Pfs_auth.rpc_proxy_auth_methods cauth true)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      (Plasma_util.connector_of_sockaddr
	 (Plasma_util.sockaddr_of_host_port (h,p)))
      esys in
  Plasma_util.rpc_engine
    mclient
    Elect_proxy.V1.set_coordinator'async
    ( pref_name, 
      nn_config#nn_clustername,
      rev,
      cksum
    )
  >> (fun st ->
	Rpc_proxy.ManagedClient.shut_down mclient;
	dlogf "done set_coordinator_at h=%s p=%d" h p;
	st
     )


let set_coordinator_everywhere_e nn_config cauth rev cksum pref_name nodes esys =
  dlogf "set_coordinator_everywhere_e";
  Plasma_util.msync_engine
    (List.map
       (fun (h,p) ->
	  set_coordinator_at nn_config cauth rev cksum pref_name (h,p) esys
       )
       nodes
    )
    esys


let index_of_this_node nn_conf my_ip port =
  (* First search by hostname only: *)
  let hostname = nn_conf#nn_hostname in
  let nodes = Array.of_list nn_conf#nn_nodes in
  let rec host_search k =
    if k >= Array.length nodes then
      raise Not_found;
    let (h,p) = Plasma_util.parse_host_port nodes.(k) in
    if h = hostname && p = port then
      k
    else
      host_search(k+1) in
  let rec ip_search k =
    if k >= Array.length nodes then
      raise Not_found;
    let (h,p) = Plasma_util.parse_host_port nodes.(k) in
    let ip = Plasma_util.ip_of_host h in
    if ip = my_ip && p = port then
      k
    else
      ip_search(k+1) in
  try
    host_search 0
  with
    | Not_found ->
	try
	  ip_search 0
	with
	  | Not_found ->
	      failwith "Coordinator not found in node list"


let elect_e nn_config port cauth esys =
  (** First send out announcement. If we get to the point when the result
      is available, and it is positive for this process, this process becomes
      the coordinator.
   *)
  let (rev,cksum) = get_revision() in
  send_annocunements nn_config port cauth esys
  ++ (fun nodes_opt ->
	match nodes_opt with
	  | None ->
	      Netlog.logf `Info
		"Election: coordinator role was rejected by other nodes";
	      eps_e (`Done(None,rev)) esys
	  | Some (nodes,ip) ->
	      Netlog.logf `Info
		"Election: got responses from %d node(s)" (List.length nodes);
	      Netlog.logf `Info
		"Election: assuming my IP address is %s"
		(Unix.string_of_inet_addr ip);
	      if List.length nodes + 1 >= nn_config # nn_node_alive_min_startup
	      then (
		Netlog.logf `Info
		  "Election: becoming coordinator";
		set_role `Coordinator;
		let i = index_of_this_node nn_config ip port in
		let pref_name = List.nth nn_config#nn_nodes i in
		set_coordinator_everywhere_e 
		  nn_config cauth rev cksum pref_name nodes esys
		>> (function
		      | `Done () -> `Done (Some i,rev)
		      | `Aborted -> `Aborted
		      | `Error e -> `Error e
		   )
	      )
	      else (
		(* TODO: run a configurable script in this case *)
		Netlog.logf `Alert
		  "Election: Not enough live namenodes. Not starting up";
		eps_e (`Done (None,rev)) esys
	      )
     )


let proc_announce nn_config sess ann emit =
  let (my_rev,_) = get_revision() in
  let my_rank = nn_config # nn_rank in
  let ann_rev = ann.Pfs_rpcapi_aux.ann_revision in
  let ann_rank = ann.Pfs_rpcapi_aux.ann_rank in
  let ann_clustername = ann.Pfs_rpcapi_aux.ann_clustername in
  let ann_random = ann.Pfs_rpcapi_aux.ann_random in

  let my_random = get_ann_random() in
  
  emit
    ( if my_random = ann_random then
	`ann_self
      else (
	if !role = `Unknown && nn_config#nn_clustername = ann_clustername then ( 
	  if ann_rev > my_rev || (ann_rev = my_rev && ann_rank < my_rank) then
	    `ann_accept
	  else
	    `ann_reject
	)
	else
	  `ann_reject
      )
    )


let proc_set_coordinator nn_config sess (coord_hp, cname, rev, cksum) emit =
  if nn_config#nn_clustername <> cname then (
    Netlog.logf `Err
      "Election: set_coordinator was called with wrong clustername";
    failwith "proc_set_coordinator: wrong clustername";
  );
  let (my_rev, my_cksum) = get_revision() in
  let cont = Netplex_cenv.self_cont() in
  cont # send_message cont#socket_service_name stop_election_msg [| |];
  ( match !role with
      | `Unknown ->
	  Netlog.logf `Info
	    "Election: Coordinator node is %s (revision %s)"
	    coord_hp
	    rev;
	  ignore(Netplex_sharedvar.create_var ~enc:true coordname);
	  String_var.set coordname coord_hp;
	  if rev = my_rev && cksum = my_cksum then (
	    Netlog.logf `Info "Election: Becoming slave node";
	    set_role `Slave
	  )
	  else (
	    Netlog.logf `Alert "Election: This node is excluded from operation \
                              because the revision is out of date";
	    set_role `Excluded
	  )
      | _ ->
	  Netlog.logf `Alert
	    "Election: Got a second set_coordinator call. This is likely to be a \
             serious error!"
  );
  emit ()
	  


let setup_elect_api srv nn_config =
  Pfs_rpcapi_srv.Elect.V1.bind_async
    ~proc_null:(fun _ _ emit -> emit ())
    ~proc_announce:(proc_announce nn_config)
    ~proc_set_coordinator:(proc_set_coordinator nn_config)
    srv
