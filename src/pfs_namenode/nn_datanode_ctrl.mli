(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Manage tickets and push ticket secrets to the datanodes *)

exception Datanode_error of Nn_datastores.datastore * exn
  (** [Datanode_error(ds,e)]: The communication with datastore [ds] caused
      the exception [e]. Usually an RPC error, or a socket error.
   *)

val reset : unit -> unit
  (** Executes a ticket reset in the background *)

type ticket_verifier =
    { tv_safetrans_id : int64;
      tv_safetrans_tmo : int64;
      tv_safetrans_sec : int64;
      tv_datanodes : int list;
      mutable tv_enabled : bool;  (* whether already enabled by new_ticket_verifier *)
    }
  (** This is almost the same as {!Nn_alloc_types.transacted_datastores} *)

val new_ticket_verifier : unit -> ticket_verifier

val enable_ticket_verifier_e : ticket_verifier -> unit Uq_engines.engine
  (** Pushes a safetrans secret to all enabled datanodes. If this is the
      first invocation, the safetrans are reset beforehand.
      Returns the list of datastore ID's to which the new safetrans
      could be pushed (which may be empty!)

      May transition to [Datanode_error].
   *)
  (* old name: safetrans_e *)

val disable_ticket_verifier : ticket_verifier -> (unit -> unit) -> unit
  (** [disable_ticket_verifier tv f_post]: Schedules that [tv] will be 
      disabled soon. After this has happened the function [f_post] is called
      back.
   *)
  (* old name: cancel_safetrans_e *)

val disconnect : int -> unit
  (** Disconnects from this datanode *)

val post_start_hook : Netmcore.res_id -> Pfs_auth.client_auth -> unit
  (** [post_start_hook pool cauth] *)


(** Note that [disable_ticket_verifier] does not disable the verifier
    immediately, but delays this to the next round of housekeeping -
    which usually happens once every second.

    There is a security risk here: The verifier remains valid even after
    the transaction is closed. Although this is only for a very short
    period of time, this can be abused by a malicious user.

    This problem is handled in this way:
    The verifier can be used to access blocks. There is no problem
    when only the blocks are accessed for which the ticket was originally
    valid, i.e. the blocks of the accessed files. The security hole exists
    only when these blocks are reused for other files - the verifier
    would then grant access to these other files although there is
    actually no permission.

    So the idea is to prevent that blocks are reused for other files
    until the verifier is really cancelled. We already have a mechanism -
    the blocks need to be pinned in the blockmaps (method [pin]).
    Such blocks are not freed as long as the pinning remains.

    The {!Nn_blockmap} module has been changed so that the pinning remains
    intact even when a [commit] or [rollback] occurs. First when the special
    [release] method is called the pinning is revoked. We do this now right
    after the cancellation of the ticket, using the [f_post] callback
    of {!Nn_datanode_ctrl.disable_ticket_verifier}.
 *)
