(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Users and groups *)

val get_ug_admin : unit -> Plasma_ug.ug_admin_ro
  (** Get the current version of the user/group tables.

      Fails if not yet set.
   *)

val get_ug_files : unit -> (string * string) list

val set_ug_files : ?init:bool -> (string * string) list -> unit
  (** Set the current version of the user/group tables.

      [init]: If set, this function is only allowed to set the initial
      value, and any later attempt will fail.
   *)

val post_start_hook : Netmcore.res_id -> unit
  (** [pool] *)
