(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf
open Plasma_util.Operators

let dlogr = Plasma_util.dlogr
let dlog = Plasma_util.dlog

type isolevel = [ `Read_committed | `Repeatable_read ]

class type transaction =
object
  method connection : Pfs_db.rw_async_connection
  method ro_connection : Pfs_db.ro_async_connection
  method isolevel : isolevel -> unit Uq_engines.engine
  method prepare : unit -> unit Uq_engines.engine
  method commit : unit -> unit Uq_engines.engine
  method commit_and_continue : unit -> unit Uq_engines.engine
  method rollback : unit -> unit Uq_engines.engine
  method event_system : Unixqueue.event_system
  method return : unit -> unit
  method is_returned : bool
end

class type ro_transaction =
object
  method connection : Pfs_db.ro_async_connection
  method event_system : Unixqueue.event_system
  method return : unit -> unit
  method is_returned : bool
end

type inodeinfo = Pfs_rpcapi_aux.inodeinfo
type blockinfo = Pfs_rpcapi_aux.blockinfo


type allocinfo = 
    { ai_inode : int64;
      ai_index : int64;
      ai_identity : string;
      ai_block : int64
    }

type modifiction_ext =
    [ `Inodeblocks_ins_abstract of int64 * Nn_blocklist.blocklist
    ]

type modification =
    [ `Datastore_upd of int * string * int64 * bool
    | `Datastore_del of int
    | `Revision_upd of int * string
    | `Revision_clear
    | `Blockalloc_upd of int * int64 * string
    | `Inode_ins of int64 * Pfs_rpcapi_aux.inodeinfo
    | `Inode_upd of int64 * Pfs_rpcapi_aux.inodeinfo
    | `Inode_upd_time of int64 * Pfs_rpcapi_aux.time option * Pfs_rpcapi_aux.time option
    | `Inode_set_anonymous of int64
    | `Inode_del of int64
    | `Inodeblocks_ins of int64 * blockinfo array
    | `Inodeblocks_del of int64 * int64 * int64
    | `Names_ins of int64 * string * int64
    | `Names_del of int64 * string
    | `Admin_table_put of string * string
    | modifiction_ext
   ]

(* assert type equality: *)
let _ =
  (fun (x : Pfs_rpcapi_aux.modification) -> (x :> modification))


let config = ref (lazy (failwith "Nn_db: module not initialized"))
let db_esys = ref (lazy (failwith "Nn_db: module not initialized"))

let bmaprowsize = Nn_config.bmaprowsize
let bmaprowsizeL = Int64.of_int bmaprowsize

let ro_conns = Queue.create()
  (** inactive ro connections *)

let ro_conns_n = ref 0
  (** #active connectons *)

let ro_conns_e = ref None
  (** This engine exists only if [ro_conns] is empty and no additional
      connection can be created. This engine is done when [ro_conns]
      becomes non-empty.
   *)

let ro_conns_signal = ref (fun () -> ())
  (** This function is called when [ro_conns] becomes non-empty *)


(* TODO: inactive connections should be closed after some period of
   idleness
 *)

let init conf esys =
  dlogr (fun () -> "Nn_db.init");
  config := lazy conf;
  db_esys := lazy esys


let ro_esys() =
  Lazy.force !db_esys


let transact esys commit_id =
  let config = Lazy.force !config in
  let c = new Pfs_db.connect config in
  (* The default READ COMMITTED isolation level is just right for us *)
  let _e =
    new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] ~noprepare:true
      "START TRANSACTION" in
  let alive = ref true in
  let prepared = ref false in
  let tname =
    "plasma_" ^ (match config#db_dbname with Some n -> n | None -> "") ^ 
      "_" ^ string_of_int commit_id in

  ( object(self)
      method connection = 
	if not !alive then 
	  failwith "Nn_db: connection is already returned to pool";
	c
      method ro_connection = 
	if not !alive then 
	  failwith "Nn_db: connection is already returned to pool";
	(c :> Pfs_db.ro_async_connection)
      method isolevel level =
	let e =
	  new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] 
	    ~noprepare:true
	    ("SET TRANSACTION ISOLATION LEVEL " ^ 
               match level with
                 | `Read_committed -> "READ COMMITTED"
                 | `Repeatable_read -> "REPEATABLE READ") in
	e 
	>> (fun st ->
	      match st with
		| (`Error _ | `Aborted) as st' -> c # finish; st'
		| `Done _ -> `Done()
	   )

      method prepare() =
	if not !alive then 
	  failwith "Nn_db: cannot prepare as transaction is already finished";
	let e =
	  new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] 
	    ~noprepare:true
	    ("PREPARE TRANSACTION '" ^ tname ^ "'") in
	prepared := true;
	e 
	>> (fun st ->
	      match st with
		| (`Error _ | `Aborted) as st' -> c # finish; st'
		| `Done _ -> `Done()
	   )
      method commit_and_continue () =
	if not !alive then 
	  failwith "Nn_db: cannot commit as transaction is already finished";
	new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] 
	  ~noprepare:true
	  (if !prepared then
	     "COMMIT PREPARED '" ^ tname ^ "'"
	   else
	     "COMMIT")
	>> (function
	      | `Done _ -> 
		  let _e =
		    new Pfs_db.async_exec c esys
		      ~expect:[Postgresql.Command_ok] ~noprepare:true
		      "START TRANSACTION" in
		  `Done()
	      | `Error e -> c#finish; `Error e
	      | `Aborted -> c#finish; `Aborted
	   )
      method commit () =
	if not !alive then 
	  failwith "Nn_db: cannot commit as transaction is already finished";
	let e =
	  new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] 
	    ~noprepare:true
	    (if !prepared then
	       "COMMIT PREPARED '" ^ tname ^ "'"
	     else
	       "COMMIT") in
	alive := false;
	e
	>> (fun st ->
	      c # finish; 
	      match st with
		| (`Error _ | `Aborted) as st' -> st'
		| `Done _ -> `Done()
	   )
      method rollback () =
	if !alive then (
	  alive := false;  (* always pretend that from now on *)
	  let e =
	    new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] 
	      ~noprepare:true
	      (if !prepared then
		 "ROLLBACK PREPARED '" ^ tname ^ "'"
	       else
		 "ROLLBACK") in
	  e
	  >> (fun st ->
		c # finish; 
		match st with
		  | (`Error _ | `Aborted) as st' -> st'
		  | `Done _ -> `Done()
	     )
	)
	else 
	  new Uq_engines.epsilon_engine (`Done ()) esys
      method return () =
	if !alive then (
	  alive := false;
	  c # finish;
	)
      method is_returned = not !alive
      method event_system = esys
    end
  )


let ro_transact_for c =
  let esys = Lazy.force !db_esys in
  let alive = ref true in
  ( object
      method connection = 
	if not !alive then 
	  failwith "Nn_db: connection is already returned to pool";
	c
      method return () =
	if !alive then (
	  alive := false;  (* always pretend that from now on *)
	  if c#idle then (
	    dlogr
	      (fun () ->
		 sprintf
		   "Nn_db.ro_transact: Returning idle db conn %d" (Oo.id c)
	      );
	    Queue.push c ro_conns
	  )
	  else (
	    dlogr
	      (fun () ->
		 sprintf
		   "Nn_db.ro_transact: Dropping bad db conn %d" (Oo.id c));
	    c # finish;
	  );
	  decr ro_conns_n;
	  !ro_conns_signal()
	)
      method is_returned = not !alive
      method event_system = esys
    end
  )


let rec ro_transact() =
  let esys = Lazy.force !db_esys in
  let config = Lazy.force !config in
  let c_opt =
    if not (Queue.is_empty ro_conns) then (
      let c = Queue.take ro_conns in
      dlogr (fun () -> 
	       sprintf "Nn_db.ro_transact: reusing db conn %d" (Oo.id c));
      Some c
    )
    else
      if !ro_conns_n < config#db_ro_connections_max then
	let c = new Pfs_db.connect config in
	dlogr
	  (fun () -> sprintf "Nn_db.ro_transact: new db conn %d" (Oo.id c));
	(* The default READ COMMITTED isolation level is just right for us *)
	let _e =
	  new Pfs_db.async_exec c esys ~expect:[Postgresql.Command_ok] 
	    ~noprepare:true
	    "START TRANSACTION" in
	Some c
      else
	None in
  match c_opt with
    | Some c ->
	incr ro_conns_n;
	let t = ro_transact_for c in
	eps_e (`Done t) esys
    | None ->
	dlog "Nn_db.ro_transact: waiting";
	( match !ro_conns_e with
	    | Some e ->
		e ++ ro_transact
	    | None ->
		let (e,signal) = Uq_engines.signal_engine esys in
		ro_conns_e := Some e;
		ro_conns_signal := (fun () ->
				      ro_conns_e := None;
				      ro_conns_signal := (fun () -> ());
				      signal(`Done());
				   );
		e ++ ro_transact
	)


let with_ro_trans f =
  let esys = Lazy.force !db_esys in
  ro_transact()
  ++ (fun ro_db ->
	f ro_db#connection esys
	>> (fun st ->
	      ro_db#return();
	      st
	   )
     )


let commit_all_prepared_transactions() =
  (** There can be at most one prepared transaction for our database.
      Check whether it appears in the pg_prepared_xacts system view,
      and if so, commit it.
   *)
  let config = Lazy.force !config in
  let tname_pat =
    "plasma\\\\_" ^ (match config#db_dbname with Some n -> n | None -> "") 
    ^ "\\\\_%" in
  let c = new Pfs_db.connect config in
  let r =
    c # exec
      ~expect:[Postgresql.Tuples_ok]
      ("SELECT gid FROM pg_prepared_xacts WHERE gid LIKE E'" ^ tname_pat ^ "'") in
  if r # ntuples > 0 then (
    let names =
      List.map
	(fun row ->
	   match row with
	     | [| gid |] -> gid
	     | _ -> assert false
	)
	r#get_all_l in
    List.iter
      (fun tname ->
	 let _r =
	   c # exec 
	     ~expect:[Postgresql.Command_ok]
	     ("COMMIT PREPARED '" ^ tname ^ "'") in
	 ()
      )
      names
  )

(**********************************************************************)
(* cache for datastore ID/identity mapping                            *)
(**********************************************************************)

type ds_init_source =
    [ `From_datastore_news_channel
    | `Datastores of Nn_datastores.datastore list
    ]

let ds_source = ref None
let ds_contents = ref None
let ds_tables = ref None

let ds_cache_init s =
  ds_source := Some s;
  ds_contents := None

let ds_from_source() =
  match !ds_source with
    | None ->
	failwith "Nn_db: ds_cache_init has not been called"
    | Some `From_datastore_news_channel ->
	Nn_datastore_news.list ~wait:true () 
    | Some `Datastores l ->
	l

(* We only want to recompute ds_tables when ds_from_source returns a 
   (physically) new list
 *)

let get_ds_all() =
  let ds_list = ds_from_source() in
  let need_update =
    match !ds_contents with
      | None -> true
      | Some x -> x != ds_list   (* phys eq! *) in
  if need_update then (
    let fwd_tab = Hashtbl.create 7 in
    let rev_tab = Hashtbl.create 7 in
    List.iter
      (fun ds ->
	 Hashtbl.replace fwd_tab ds.Nn_datastores.id ds;
	 Hashtbl.replace rev_tab ds.Nn_datastores.identity ds;
      )
      ds_list;
    ds_contents := Some ds_list;
    ds_tables := Some (fwd_tab, rev_tab)
  );
  match !ds_tables with
    | None -> assert false
    | Some (fwd_tab, rev_tab) ->
	(ds_list, fwd_tab, rev_tab)


let ds_cache_contents() =
  let (ds_list, _, _) = get_ds_all() in
  ds_list


let get_ds_tables () =
  let (_, fwd_tab, rev_tab) = get_ds_all() in
  (fwd_tab, rev_tab)


(**********************************************************************)
(* datastores and blockmap                                            *)
(**********************************************************************)

let datastore_list_e conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 let l =
		   List.map
		     (fun row ->
			match row with
			  | [| id_s; identity; size_s; enabled_s |] ->
			      { Nn_datastores.id = int_of_string id_s;
				identity = identity;
				size = Int64.of_string size_s;
				enabled = (enabled_s = "t");
				node = None;
				alive = false;
			      }
			  | _ -> assert false
		     )
		     r#get_all_l in
		 `Done l
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       "SELECT id,identity,size,enabled FROM datastore"
    )


let datastore_list_ro_e() =
  with_ro_trans datastore_list_e


let datastore_find_e ~identity conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [] ->
		       `Done None
		   | [ [| id_s; size_s; enabled_s |] ] ->
		       let ds =
			 { Nn_datastores.id = int_of_string id_s;
			   identity = identity;
			   size = Int64.of_string size_s;
			   enabled = (enabled_s = "t");
			   node = None;
			   alive = false;
			 } in
		       `Done (Some ds)
		   | _ -> assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) identity |]
       "SELECT id,size,enabled FROM datastore WHERE identity = $1"
    )


let datastore_find_ro_e ~identity =
  with_ro_trans (datastore_find_e ~identity)


let blockalloc_enlarge_e ~id ~size ~old_size conn esys =
  (** Modifies the blockalloc table so that rows for [size-old_size] new
      blocks are added to it. The new blocks are marked as unallocated.
   *)
  assert(size > old_size);
  let delta = Int64.sub size old_size in
  let old_sizeH = Int64.div old_size bmaprowsizeL in
  let old_sizeL = Int64.rem old_size bmaprowsizeL in
  let sizeH = Int64.div size bmaprowsizeL in
  let sizeL = Int64.rem size bmaprowsizeL in
  
  (* First make a plan: *)
  let do_upd =
    (* whether to extend an existing row *)
    if old_sizeL > 0L then
      let blkidx = Int64.mul old_sizeH bmaprowsizeL in        (* which row *)
      let n = min (Int64.add old_sizeL delta) bmaprowsizeL in
              (* new size of blkmap *)
      Some(blkidx, Int64.to_int n)
    else
      None in
  let do_ins =
    (* list of rows to insert *)
    let first_blkidx =                 (* first row to insert *)
      if old_sizeL = 0L then 
	Int64.mul old_sizeH bmaprowsizeL
      else
	Int64.mul (Int64.succ old_sizeH) bmaprowsizeL in
    let last_blkidx =                  (* last row to insert *)
      if sizeL = 0L then
	Int64.mul (Int64.pred sizeH) bmaprowsizeL
      else
	Int64.mul sizeH bmaprowsizeL in
    let last_n =                       (* length of last row *)
      if sizeL = 0L then
	bmaprowsize
      else
	Int64.to_int sizeL in
    (first_blkidx, last_blkidx, last_n) in
  
  let exec_upd (blkidx,n) =
    new Uq_engines.seq_engine
       (new Pfs_db.async_exec conn esys 
	  ~expect:[Postgresql.Tuples_ok]
	  ~params:[| (* $1: *) string_of_int id;
	             (* $2: *) Int64.to_string blkidx
          |]
	  "SELECT blkmap FROM blockalloc WHERE datastore = $1 AND blkidx = $2"
       )
       (fun r ->
	  if r#ntuples <> 1 then
	    new Uq_engines.epsilon_engine
	      (`Error (Failure "Nn_db.blockalloc_enlarge_e: missing row"))
	      esys
	  else (
	    let blkmap = r # getvalue 0 0 in
	    let new_blkmap =
	      blkmap ^ 
		(String.make (n - String.length blkmap) '0') in
	    new Pfs_db.async_exec conn esys 
	      ~expect:[Postgresql.Command_ok]
	      ~params:[| (* $1: *) string_of_int id;
	                 (* $2: *) Int64.to_string blkidx;
                         (* $3: *) new_blkmap  (* hope no escaping req'ed *)
              |]
	      "UPDATE blockalloc SET blkmap = $3 \
               WHERE datastore = $1 AND blkidx = $2"
	  )
       )
  in

  let exec_single_ins blkidx n =
    let blkmap = String.make n '0' in
    new Uq_engines.map_engine
      ~map_done:(fun _ -> `Done())
      (new Pfs_db.async_exec conn esys 
	 ~expect:[Postgresql.Command_ok]
	 ~params:[| (* $1: *) string_of_int id;
	            (* $2: *) Int64.to_string blkidx;
                     (* $3: *) blkmap  (* hope no escaping req'ed *)
	 |]
	 "INSERT INTO blockalloc (datastore,blkidx,blkmap,blkcnt) \
          VALUES ($1, $2, $3, 0)"
      )
  in

  let rec exec_ins (first_blkidx, last_blkidx, last_n) =
    if first_blkidx < last_blkidx then
      new Uq_engines.seq_engine
	(exec_single_ins first_blkidx bmaprowsize)
	(fun _ ->
	   exec_ins (Int64.add first_blkidx bmaprowsizeL, last_blkidx, last_n)
	)
    else
      if first_blkidx = last_blkidx then
	exec_single_ins last_blkidx last_n
      else
	new Uq_engines.epsilon_engine (`Done ()) esys
  in

  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done())
    (match do_upd with
       | None ->
	   exec_ins do_ins
       | Some u ->
	   new Uq_engines.seq_engine
	     (exec_upd u)
	     (fun _ -> exec_ins do_ins)
    )
    


let datastore_set_e ~id ~identity ~size ~enabled conn esys =
   new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done())
     (let params =
	[| (* $1: *) string_of_int id;
	   (* $2: *) identity;
	   (* $3: *) Int64.to_string size;
	   (* $4: *) if enabled then "t" else "f"
	|] in
       new Pfs_db.async_exec conn esys 
	~expect:[Postgresql.Command_ok]
	~params
	 "UPDATE datastore \
          SET identity=$2, size=$3, enabled=$4 \
          WHERE id=$1"
     )


let datastore_new_e ~id ~identity ~size ~enabled conn esys =
  new Uq_engines.seq_engine
    (let params =
	[| (* $1: *) string_of_int id;
	   (* $2: *) identity;
	   (* $3: *) Int64.to_string size;
	   (* $4: *) if enabled then "t" else "f"
	|] in
       new Pfs_db.async_exec conn esys 
	~expect:[Postgresql.Command_ok]
	~params
	 "INSERT INTO datastore (id,identity,size,enabled) \
          VALUES ($1,$2,$3,$4)"
    )
    (fun _ ->
       blockalloc_enlarge_e ~id ~size ~old_size:0L conn esys
    )


let datastore_upd_e ~id ~identity ~size ~enabled conn esys =
  if size <= 0L then
    invalid_arg "Nn_db.datastore_upd_e: size is non-positive";
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Uq_engines.seq_engine
       (new Pfs_db.async_exec conn esys 
	  ~expect:[Postgresql.Tuples_ok]
	  ~params:[| (* $1: *) string_of_int id |]
	  "SELECT size FROM datastore WHERE id = $1"
       )
       (fun r ->
	  (* if no row was found by SELECT, the record is new.
             Otherwise check whether size has changed
	   *)
	  if r # ntuples = 0 then
	    (* case new row: *)
	    datastore_new_e ~id ~identity ~size ~enabled conn esys
	  else (
	    let db_size =
	      Int64.of_string (r # getvalue 0 0) in
	    if db_size > size then
	      new Uq_engines.epsilon_engine
		(`Error(Failure "Nn_db.datastore_upd_e: Cannot shrink size"))
		esys
	    else
	      if db_size < size then
		(* case enlarge datastore: also add new blockalloc rows *)
		new Uq_engines.seq_engine
		  (blockalloc_enlarge_e ~id ~size ~old_size:db_size conn esys)
		  (fun _ ->
		     (* followed by the datastore update *)
		     datastore_set_e ~id ~identity ~size ~enabled conn esys
		  )
	      else
		(* unchanged size: only overwrite the datastore row *)
		datastore_set_e ~id ~identity ~size ~enabled conn esys
	  )
       )
    )


let datastore_del_e ~id conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
      ~expect:[Postgresql.Command_ok]
      ~params:[| (* $1: *) string_of_int id |]
      "DELETE FROM datastore WHERE id = $1"
    )


let blockalloc_list_e ~datastore ~blkidx_min ~blkidx_max conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 let l =
		   List.map
		     (fun row ->
			match row with
			  | [| blkidx_s; blkmap |] ->
			      (Int64.of_string blkidx_s, blkmap)
			  | _ -> assert false
		     )
		     r#get_all_l in
		 `Done l
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) string_of_int datastore;
	          (* $2: *) Int64.to_string blkidx_min;
	          (* $3: *) Int64.to_string blkidx_max;
               |]
       "SELECT blkidx,blkmap FROM blockalloc \
        WHERE datastore = $1 AND blkidx >= $2 AND blkidx <= $3"
    )


let blockalloc_list_ro_e ~datastore ~blkidx_min ~blkidx_max =
  with_ro_trans (blockalloc_list_e ~datastore ~blkidx_min ~blkidx_max)


let blockalloc_upd_e ~datastore ~blkidx ~blkmap conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) string_of_int datastore; 
	          (* $2: *) Int64.to_string blkidx;
	          (* $3: *) blkmap
               |]
       "UPDATE blockalloc SET blkmap = $3 WHERE datastore = $1 AND blkidx = $2"
    )


(**********************************************************************)
(* revision                                                           *)
(**********************************************************************)

let revision_get_e conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [ [| rev |] ] ->
		       `Done rev
		   | _ ->
		       assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       "SELECT MAX(revstr) FROM revision"
    )


let revision_get_ro_e () =
  with_ro_trans revision_get_e


let revision_get_checksum_e conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 let all_revs =
		   List.map
		     (function
			| [| rev |] -> rev
			| _ -> assert false
		     )
		     r#get_all_l in
		 let cksum =
		   Digest.to_hex
		     (Digest.string
			(String.concat ","
			   (List.sort Pervasives.compare all_revs))) in
		 `Done cksum
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       "SELECT revstr FROM revision"
    )


let revision_upd_e ~revstr ~commit_id conn esys =
  new Pfs_db.async_exec conn esys 
    ~expect:[Postgresql.Command_ok]
    ~params:[| (* $1: *) revstr;
               (* $2: *) string_of_int commit_id;
    |]
    "UPDATE revision SET revstr = $1 WHERE committer = $2"
  ++ (fun pgres ->
	match pgres#cmd_tuples with
	  | "0" ->
	      (* nothing was updated - do an INSERT instead *)
	      new Pfs_db.async_exec conn esys 
		~expect:[Postgresql.Command_ok]
		~params:[| (* $1: *) revstr;
		           (* $2: *) string_of_int commit_id;
		|]
		"INSERT INTO revision(revstr,committer) VALUES ($1, $2)"
	      ++ (fun _ ->
		    eps_e (`Done()) esys
		 )
	  | _ ->
	      eps_e (`Done()) esys
     )


let revision_clear_e conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       "DELETE FROM revision"
    )


(**********************************************************************)
(* inode                                                              *)
(**********************************************************************)

let ftype_of_char =
  function
    | 'r' -> `ftype_regular
    | 'd' -> `ftype_directory
    | 'l' -> `ftype_symlink
    | _ -> failwith "Nn_db.ftype_of_char"

let char_of_ftype =
  function
    | `ftype_regular -> 'r'
    | `ftype_directory -> 'd'
    | `ftype_symlink -> 'l'

let create_time secs nsecs =
  if nsecs < 0 || nsecs >= 1_000_000_000 then
    invalid_arg "Nn_db.create_time";
  { Pfs_rpcapi_aux.tsecs = secs; tnsecs = nsecs }


let inode_listall_e conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 `Done(List.map
			 (function
			    | [| id_s |] ->
				Int64.of_string id_s
			    | _ ->
				assert false
			 )
			 r#get_all_l)
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| |]
       "SELECT id FROM inode"
    )


let inode_get_e ~id conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [] -> 
		       `Done None
		   | [ [| ftype_s; username; groupname; mode_s; eof_s; 
			  mtime1_s; mtime2_s; ctime1_s; ctime2_s; 
			  blimit_s; repfactor_s; field1; seqno_s; crverf_s;
			  anon_s;
		       |]
		     ] ->
		       let ii =
			 { Pfs_rpcapi_aux.
			   filetype = ftype_of_char ftype_s.[0];
			   usergroup = {
			     Pfs_rpcapi_aux.user = username;
			     group = groupname
			   };
			   mode = int_of_string mode_s;
			   eof = Int64.of_string eof_s;
			   mtime = 
			     create_time
			       (Int64.of_string mtime1_s)
			       (int_of_string mtime2_s);
			   ctime = 
			     create_time
			       (Int64.of_string ctime1_s)
			       (int_of_string ctime2_s);
			   blocklimit = Int64.of_string blimit_s;
			   replication = int_of_string repfactor_s;
			   field1 = field1;
			   seqno = Int64.of_string seqno_s;
			   committed = true;
			   create_verifier = Int64.of_string crverf_s;
			   anonymous = (anon_s <> "");
			 } in
		       `Done(Some ii)
		   | _ -> assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) Int64.to_string id |]
       "SELECT ftype,username,groupname,mode,eof,mtime1,mtime2,ctime1,ctime2, \
               blimit,repfactor,field1,seqno,crverf,anonymous \
        FROM inode WHERE id = $1"
    )


let inode_get_ro_e ~id =
  with_ro_trans (inode_get_e ~id)


let inode_maxid_e conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [ [| max_s |] ] ->
		       if max_s = "" then
			 `Done 0L
		       else
			 `Done(Int64.of_string max_s)
		   | _ -> 
		       assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       "SELECT MAX(id) FROM inode"
    )
  
let inode_maxid_ro_e () =
 with_ro_trans inode_maxid_e


let inode_ins_e ~id ii conn esys =
  let module A = Pfs_rpcapi_aux in
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) Int64.to_string id;
	          (* $2: *) String.make 1 (char_of_ftype ii.A.filetype);
	          (* $3: *) ii.A.usergroup.A.user;
	          (* $4: *) ii.A.usergroup.A.group;
	          (* $5: *) string_of_int ii.A.mode;
    	          (* $6: *) Int64.to_string ii.A.eof;
                  (* $7: *) Int64.to_string ii.A.mtime.A.tsecs;
                  (* $8: *) string_of_int ii.A.mtime.A.tnsecs;
                  (* $9: *) Int64.to_string ii.A.ctime.A.tsecs;
                  (* $10: *) string_of_int ii.A.ctime.A.tnsecs;
	          (* $11: *) Int64.to_string ii.A.blocklimit;
	          (* $12: *) string_of_int ii.A.replication;
	          (* $13: *) ii.A.field1;
	          (* $14: *) Int64.to_string ii.A.seqno;
	          (* $15: *) Int64.to_string ii.A.create_verifier;
	          (* $16: *) ( if ii.A.anonymous then
				 Int64.to_string id
			       else
				 Postgresql.null
			     )
               |]
       "INSERT INTO inode (id,ftype,username,groupname,mode,eof, \
                           mtime1,mtime2,ctime1,ctime2, \
                           blimit,repfactor,field1, seqno, crverf, \
                           anonymous) \
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, \
                $14, $15, $16)"
    )
    

let inode_upd_e ~id ii conn esys =
  let module A = Pfs_rpcapi_aux in
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) Int64.to_string id;
	          (* $2: *) ii.A.usergroup.A.user;
	          (* $3: *) ii.A.usergroup.A.group;
	          (* $4: *) string_of_int ii.A.mode;
    	          (* $5: *) Int64.to_string ii.A.eof;
	          (* $6: *) Int64.to_string ii.A.mtime.A.tsecs;
                  (* $7: *) string_of_int ii.A.mtime.A.tnsecs;
                  (* $8: *) Int64.to_string ii.A.ctime.A.tsecs;
                  (* $9: *) string_of_int ii.A.ctime.A.tnsecs;
	          (* $10: *) Int64.to_string ii.A.blocklimit;
	          (* $11: *) string_of_int ii.A.replication;
	          (* $12: *) ii.A.field1;
	          (* $13: *) Int64.to_string ii.A.seqno;
	          (* $14: *) Int64.to_string ii.A.create_verifier;
	          (* $15: *) ( if ii.A.anonymous then
				 Int64.to_string id
			       else
				 Postgresql.null
			     )
               |]
       "UPDATE inode \
          SET username = $2, groupname = $3, mode = $4, eof = $5, \
              mtime1 = $6, mtime2 = $7, ctime1 = $8, ctime2 = $9,
              blimit = $10, repfactor = $11, field1 = $12, seqno = $13, \
              crverf = $14, anonymous = $15 \
          WHERE id = $1"
    )


let inode_upd_time_e ~id ~mtime ~ctime conn esys =
  let module A = Pfs_rpcapi_aux in
  if mtime <> None || ctime <> None then (
    new Uq_engines.map_engine
      ~map_done:(fun _ -> `Done ())
      (new Pfs_db.async_exec conn esys 
	 ~expect:[Postgresql.Command_ok]
	 ~noprepare:true               (* FIXME *)
	 (let set_mtime_clause =
	    match mtime with
	      | None -> ""
	      | Some t ->
		  sprintf "mtime1 = %Ld, mtime2 = %d" 
		    t.A.tsecs t.A.tnsecs in
	  let set_ctime_clause =
	    match ctime with
	      | None -> ""
	      | Some t ->
		  sprintf "ctime1 = %Ld, ctime2 = %d" 
		    t.A.tsecs t.A.tnsecs in
	  let set_clause =
	    set_mtime_clause ^ 
	      (if set_mtime_clause <> "" && set_ctime_clause <> "" then
		 ", " else "") ^ 
	      set_ctime_clause in
	  sprintf
	    "UPDATE inode SET %s WHERE id = %Ld"
	    set_clause id
	 )
      )
  )
  else
    eps_e (`Done ()) esys


let inode_del_e ~id conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) Int64.to_string id;
               |]
       "DELETE FROM inode WHERE id = $1"
    )

(**********************************************************************)
(* anonymous inodes                                                   *)
(**********************************************************************)

let inode_get_anonymous_e ~n conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 `Done
		   (List.map
		      (fun row ->
			 match row with
			   | [| inode_s |] ->
			       Int64.of_string inode_s
			   | _ ->
			       assert false
		      )
		      (r#get_all_l)
		   )
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) string_of_int n |]
       "SELECT anonymous FROM inode WHERE anonymous IS NOT NULL LIMIT $1"
    )

let inode_get_anonymous_ro_e ~n =
 with_ro_trans (inode_get_anonymous_e ~n)


let inode_set_anonymous_e ~id conn esys =
  let module A = Pfs_rpcapi_aux in
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| Int64.to_string id |]
       "UPDATE inode SET anonymous = $1 WHERE id = $1"
    )

(**********************************************************************)
(* blocklist and allocinfo                                            *)
(**********************************************************************)

let allocinfo_get_e ~inode conn esys =
  let (fwd_tab, _) = get_ds_tables() in
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 `Done
		   (List.map
		      (fun row ->
			 match row with
			   | [| blkidx_s; ds_id_s; block_s |] ->
			       let ds_id = int_of_string ds_id_s in
			       let ds =
				 try Hashtbl.find fwd_tab ds_id
				 with Not_found -> assert false in
			       { ai_inode = inode;
				 ai_index = Int64.of_string blkidx_s;
				 ai_identity = ds.Nn_datastores.identity;
				 ai_block = Int64.of_string block_s
			       }
			   | _ ->
			       assert false
		      )
		      (r#get_all_l)
		   )
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) Int64.to_string inode |]
       "SELECT blkidx,datastore,block FROM allocinfo WHERE inode = $1"
    )

let allocinfo_get_ro_e ~inode =
 with_ro_trans (allocinfo_get_e ~inode)

let inodeblocks_getall_e ~inode ~dsid conn esys = (* for fsck *)
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 `Done
		   (List.map
		      (fun row ->
			 match row with
			   | [| blkidxmin_s; blkidxmax_s; block_s |] ->
			       let blkidxmin = Int64.of_string blkidxmin_s in
			       let blkidxmax = Int64.of_string blkidxmax_s in
			       let len =
				 Int64.succ (Int64.sub blkidxmax blkidxmin) in
			       (blkidxmin,
				Int64.of_string block_s,
				len
			       )
			   | _ ->
			       assert false
		      )
		      (r#get_all_l)
		   )
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) Int64.to_string inode;
         (* $2: *) string_of_int dsid
       |]
       "SELECT blkidxmin,blkidxmax,block \
        FROM inodeblocks WHERE inode = $1 AND datastore = $2 \
        ORDER BY blkidxmin"
    )
    
    

let inodeblocks_get_uncut_e ~inode ~blkidx ~len fwd_tab conn esys =
  (* The "uncut" version of [inodeblocks_get_e] returns the original DB rows 
     overlapping with the query, i.e. these rows are not intersected with
     the query
   *)
  if len < 0L then invalid_arg "inodeblocks_get_e";
  if blkidx > Int64.sub Int64.max_int len then invalid_arg "inodeblocks_get_e";
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 let l =
		   List.flatten
		     (List.map
			(fun row ->
			   match row with
			     | [| blkidxmin_s; blkidxmax_s; ds_id_s; block_s; 
				  seqno_s
			       |] ->
				 let ds_id = int_of_string ds_id_s in
				 let blkidxmin = Int64.of_string blkidxmin_s in
				 let blkidxmax = Int64.of_string blkidxmax_s in
				 let length = 
				   Int64.succ(Int64.sub blkidxmax blkidxmin) in
				 ( try
				     let ds = Hashtbl.find fwd_tab ds_id in
				     let node =
					match ds.Nn_datastores.node with
					  | None -> ""
					  | Some addr -> 
					      let (h,p) = 
						Plasma_util.host_port_of_sockaddr addr in
					      sprintf "%s:%d" h p in
				      [
					{ Pfs_rpcapi_aux.index = blkidxmin;
					  node;
					  node_alive = false; (* don't know *)
					  identity = ds.Nn_datastores.identity;
					  block = Int64.of_string block_s;
					  length;
					  checksum = None;
					  ticket = Nn_blocklist.empty_ticket;
					  inode_seqno = Int64.of_string seqno_s;
					  inode_committed = true;
					}
				      ]
				   with
				     | Not_found ->
					  Netlog.logf `Warning
					    "Lookup for datastore ID %s failed"
					    ds_id_s;
					 []
				 )
			     | _ -> assert false
			)
			(r#get_all_l)
		     ) in
		 let blist = Nn_blocklist.to_blocklist l in
		 `Done blist
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| (* $1: *) Int64.to_string inode;
                  (* $2: *) Int64.to_string blkidx;
                  (* $3: *) Int64.to_string (Int64.pred (Int64.add blkidx len));
               |]
       "SELECT b.blkidxmin,b.blkidxmax,b.datastore,b.block,i.seqno \
        FROM inodeblocks b JOIN inode i ON (b.inode = i.id) \
        WHERE b.inode = $1 AND b.blkidxmax >= $2 AND b.blkidxmin <= $3"
    )


let inodeblocks_get_e ~inode ~blkidx ~len conn esys =
  let (fwd_tab,_) = get_ds_tables() in
  inodeblocks_get_uncut_e ~inode ~blkidx ~len fwd_tab conn esys
  ++ (fun blist ->
	let blkidxmax = Int64.pred (Int64.add blkidx len) in
	let blist' = Nn_blocklist.sub (blkidx,blkidxmax) blist in
	eps_e (`Done blist') esys
     )


let inodeblocks_get_ro_e ~inode ~blkidx ~len =
 with_ro_trans (inodeblocks_get_e ~inode ~blkidx ~len)


let inodeblocks_del_rows_e ~inode ~blkidx ~len conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
      ~expect:[Postgresql.Command_ok]
      ~params:[| (* $1: *) Int64.to_string inode;
                 (* $2: *) Int64.to_string blkidx;
                 (* $3: *) Int64.to_string (Int64.pred (Int64.add blkidx len));
              |]
      "DELETE FROM inodeblocks \
       WHERE inode = $1 AND blkidxmax >= $2 AND blkidxmin <= $3"
    )

let inodeblocks_ins_rows_e ~inode blist rev_tab conn esys =
  let open Pfs_rpcapi_aux in
  let bi_list = Nn_blocklist.to_blockinfo_list blist in
  if bi_list = [] then
    eps_e (`Done ()) esys
  else
    try
      let copy_in_lines =
	List.map
	  (fun bi ->
	     let identity = bi.identity in
	     let ds = 
	       try Hashtbl.find rev_tab identity
	       with Not_found ->
		 failwith ("identity not found: " ^ identity) in
	     let blkidxmax =
	       Int64.pred (Int64.add bi.index bi.length) in
	     
	     sprintf "%Ld\t%Ld\t%Ld\t%d\t%Ld" 
	       inode 
	       bi.index
	       blkidxmax
	       ds.Nn_datastores.id
	       bi.block
	  )
	  bi_list in
      new Uq_engines.map_engine
	~map_done:(fun _ -> `Done ())
	(new Pfs_db.async_exec conn esys 
	   ~expect:[Postgresql.Command_ok]
	   ~copy_in_lines
	   ~noprepare:true
	   "COPY inodeblocks (inode,blkidxmin,blkidxmax,datastore,block) \
          FROM STDIN"
	)
    with
      | Not_found ->
	  new Uq_engines.epsilon_engine
	    (`Error(Failure "inodeblocks_ins_rows_e: ds identity not found"))
	    esys



let inodeblocks_modify_e ~inode ~blkidx ~len ds_tabs f conn esys =
  (* Retrieves the blocklist [blkidx] to [blkidx+len-1] from the DB
     (as [inodeblocks_get_uncut_e] would do), and calls
     [l' = f l] for this list [k] yielding a new list [l']. The old
     DB contents are removed, and [l'] is inserted
   *)
  let (fwd_tab, rev_tab) = ds_tabs in
  inodeblocks_get_uncut_e ~inode ~blkidx ~len fwd_tab conn esys
  ++ (fun blist ->
	let blist' = f blist in
	inodeblocks_del_rows_e ~inode ~blkidx ~len conn esys
	++ (fun () ->
	      inodeblocks_ins_rows_e ~inode blist' rev_tab conn esys
	   )
     )


let inodeblocks_del_e ~inode ~blkidx ~len conn esys =
  if len < 0L then invalid_arg "inodeblocks_del_e";
  if blkidx > Int64.sub Int64.max_int len then invalid_arg "inodeblocks_del_e";
  let ds_tabs = get_ds_tables() in
  inodeblocks_modify_e
    ~inode ~blkidx ~len ds_tabs
    (fun blist -> 
       let blkidxmax = Int64.pred (Int64.add blkidx len) in
       Nn_blocklist.remove (blkidx,blkidxmax) blist
    )
    conn esys


let ai_of_bi inode bi =
  let index = 
    Int64.pred(Int64.add bi.Pfs_rpcapi_aux.index bi.Pfs_rpcapi_aux.length) in
  let block = 
    Int64.pred(Int64.add bi.Pfs_rpcapi_aux.block bi.Pfs_rpcapi_aux.length) in
  { ai_inode = inode;
    ai_index = index;
    ai_identity = bi.Pfs_rpcapi_aux.identity;
    ai_block = block
  }


let update_allocinfo inode ai_list blist =
  let best = Hashtbl.create 10 in
  List.iter
    (fun ai ->
       Hashtbl.replace best ai.ai_identity ai
    )
    ai_list;
  Nn_blocklist.iter
    (fun identity (min,max) bi ->
       try
	 let old_best_ai = Hashtbl.find best bi.Pfs_rpcapi_aux.identity in
	 if max > old_best_ai.ai_index then
	   raise Not_found
       with
	 | Not_found ->
	     let ai = ai_of_bi inode bi in
	     Hashtbl.replace best bi.Pfs_rpcapi_aux.identity ai
    )
    blist;
  best


let allocinfo_upd_e ~inode bi_list rev_tab conn esys =
  (* Extract the relevant blocks: *)
  let best = update_allocinfo inode [] bi_list in
  let ds_id_list =
    Hashtbl.fold
      (fun _ ai acc ->
	 let ds = 
	   try Hashtbl.find rev_tab ai.ai_identity 
	   with Not_found -> assert false in
	 ds.Nn_datastores.id :: acc
      )
      best
      [] in
  let copy_in_lines =
    Hashtbl.fold
      (fun _ ai acc ->
	 let identity = ai.ai_identity in
	 let ds = 
	   try Hashtbl.find rev_tab identity
	   with Not_found -> assert false in
	 
	 (sprintf "%Ld\t%Ld\t%d\t%Ld" 
	    inode 
	    ai.ai_index
	    ds.Nn_datastores.id
	    ai.ai_block) :: acc
      )
      best
      [] in

  (* Delete old information: *)
  ( if ds_id_list <> [] then
      (new Pfs_db.async_exec conn esys 
	 ~expect:[Postgresql.Command_ok]
	 ~params:[| (* $1: *) Int64.to_string inode; |]
	 ~noprepare:true
	 (sprintf 
	    "DELETE FROM allocinfo WHERE inode = $1 AND datastore IN ( %s )"
	    (String.concat "," (List.map string_of_int ds_id_list))
	 )
      ) ++ (fun _ -> eps_e (`Done()) esys)
    else eps_e (`Done()) esys
  )
  ++ (fun _ ->
	(new Pfs_db.async_exec conn esys 
	   ~expect:[Postgresql.Command_ok]
	   ~copy_in_lines
	   ~noprepare:true
	   "COPY allocinfo (inode,blkidx,datastore,block) FROM STDIN"
	)
     )
  ++ (fun _ ->
	eps_e (`Done ()) esys
     )


let inodeblocks_ins_e ~inode blist conn esys =
  (* FIXME: there are some details in blist which may cause discontiguous
     ranges like tickets, inode_seqno etc. These details are irrelevant
     here. Think about mapping blist so that these details are set to
     always the same value => more compressed db representation
   *)
  let ds_tabs = get_ds_tables() in
  let (_, rev_tab) = ds_tabs in
  (* Determine the block range of [blist], extended by one block on the
     left and right side. (The extension is necessary to make merges
     possible with existing db entries.)
   *)
  if blist = Nn_blocklist.empty then
    eps_e (`Done()) esys
  else (
    let blkidxmin, blkidxmax =
      Nn_blocklist.fold
	(fun _ (idxmin,idxmax) _ (accmin,accmax) ->
	   (min idxmin accmin), (max idxmax accmax)
	)
	blist
	(Int64.max_int, 0L) in
    let blkidx = Int64.pred blkidxmin in
    let len = Int64.add (Int64.sub blkidxmax blkidxmin) 3L in
    inodeblocks_modify_e
      ~inode ~blkidx ~len ds_tabs
      (fun old_blist ->
	 Nn_blocklist.merge old_blist blist
      )
      conn esys
    ++ (fun () ->
	  allocinfo_upd_e ~inode blist rev_tab conn esys
       )
  )

(**********************************************************************)
(* filenames                                                          *)
(**********************************************************************)


let names_get_e ~dir_inode ~name conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [ [| inode_s |] ] ->
		       `Done(Some(Int64.of_string inode_s))
		   | [ ] ->
		       `Done None
		   | _ ->
		       assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| Int64.to_string dir_inode; name |]
       "SELECT inode FROM names WHERE inode_d = $1 AND name = $2"
    )


let names_get_ro_e ~dir_inode ~name =
  with_ro_trans (names_get_e ~dir_inode ~name)


let names_list_e ~dir_inode conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 `Done(List.map
			 (fun row ->
			    match row with
			      | [| name; inode_s |] ->
				  (name, Int64.of_string inode_s)
			      | _ ->
				  assert false
			 )
			 r#get_all_l)
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| Int64.to_string dir_inode |]
       "SELECT name, inode FROM names WHERE inode_d = $1"
    )


let names_list_ro_e ~dir_inode =
  with_ro_trans (names_list_e ~dir_inode)


let names_rev_get_e ~inode conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 `Done
		   (List.map
		      (fun row ->
			 match row with
			   | [| inode_d; name |] ->
			       (Int64.of_string inode_d, name)
			   | _ -> assert false
		      )
		      r#get_all_l)
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| Int64.to_string inode |]
       "SELECT inode_d, name FROM names WHERE inode = $1"
    )


let names_rev_get_ro_e ~inode =
 with_ro_trans (names_rev_get_e ~inode)


let names_get_parent_e ~inode conn esys =
  names_rev_get_e ~inode conn esys
  ++ (fun rows ->
	match rows with
	  | [ inode_d, _ ] ->
	      eps_e (`Done (Some inode_d)) esys
	  | _ ->
	      eps_e (`Done None) esys
     )

let names_get_parent_ro_e ~inode =
 with_ro_trans (names_get_parent_e ~inode)


let names_count_e ~inode conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [ [| count_s |] ] ->
		       `Done(int_of_string count_s)
		   | _ ->
		       assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| Int64.to_string inode |]
       "SELECT COUNT(*) FROM names WHERE inode = $1"
    )


let names_count_ro_e ~inode =
 with_ro_trans (names_count_e ~inode)


let names_ins_e ~dir_inode ~name ~inode conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) Int64.to_string dir_inode;
	          (* $2: *) name;
 	          (* $3: *) Int64.to_string inode;
               |]
       "INSERT INTO names (inode_d, name, inode) \
        VALUES ($1, $2, $3)"
    )


let names_del_e ~dir_inode ~name conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) Int64.to_string dir_inode;
	          (* $2: *) name
               |]
       "DELETE FROM names WHERE inode_d = $1 AND name = $2"
    )

let admin_table_get_e ~key conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [ [| contents |] ] ->
		       `Done contents
		   | [ ] ->
		       `Error(Failure "No such admin table")
		   | _ ->
		       assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| key |]
       "SELECT contents FROM admin_table WHERE key = $1"
    )

let admin_table_get_ro_e ~key =
 with_ro_trans (admin_table_get_e ~key)

let admin_table_exists_e ~key conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 match r#get_all_l with
		   | [ [| count_s |] ] ->
		       `Done(int_of_string count_s > 0)
		   | _ ->
		       assert false
	      )
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Tuples_ok]
       ~params:[| key |]
       "SELECT COUNT(*) FROM admin_table WHERE key = $1"
    )

let admin_table_exists_ro_e ~key =
  with_ro_trans (admin_table_exists_e ~key)


let admin_table_put_e ~key ~contents conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun _ -> `Done ())
    (new Pfs_db.async_exec conn esys 
       ~expect:[Postgresql.Command_ok]
       ~params:[| (* $1: *) key;
	          (* $2: *) contents
               |]
       "UPDATE admin_table SET contents = $2 WHERE key = $1"
    )


let read_ug_admin_e conn esys =
  let rec loop_e keys acc =
    match keys with
      | key :: keys' ->
	  admin_table_get_e ~key conn esys
	  ++ (fun data -> loop_e keys' ((key,data) :: acc))
      | [] ->
	  eps_e (`Done acc) esys in
  loop_e Plasma_ug.req_admin_tables []



(**********************************************************************)

let exec_e m conn esys =
  match m with
    | `Datastore_upd(id,identity,size,enabled) ->
	datastore_upd_e
	  ~id ~identity ~size ~enabled conn esys
    | `Datastore_del id ->
	datastore_del_e
	  ~id  conn esys
    | `Revision_upd(commit_id, rev_id) ->
	revision_upd_e
	  ~revstr:rev_id ~commit_id conn esys
    | `Revision_clear ->
	revision_clear_e conn esys
    | `Blockalloc_upd(datastore,blkidx,blkmap) ->
	blockalloc_upd_e
	  ~datastore ~blkidx ~blkmap conn esys
    | `Inode_ins(id,ii) ->
	inode_ins_e ~id ii conn esys
    | `Inode_upd(id,ii) ->
	inode_upd_e ~id ii conn esys
    | `Inode_upd_time(id,mtime,ctime) ->
	inode_upd_time_e ~id ~mtime ~ctime conn esys
    | `Inode_set_anonymous id ->
	inode_set_anonymous_e ~id conn esys
    | `Inode_del id ->
	inode_del_e ~id conn esys
    | `Inodeblocks_ins_abstract(inode,blist) ->
	inodeblocks_ins_e ~inode blist conn esys
    | `Inodeblocks_ins(inode,binfos) ->
        let blist = Nn_blocklist.to_blocklist (Array.to_list binfos) in
	inodeblocks_ins_e ~inode blist conn esys
    | `Inodeblocks_del(inode,blkidx,len) ->
	inodeblocks_del_e ~inode ~blkidx ~len conn esys 
    | `Names_ins(dir_inode,name,inode) ->
	names_ins_e ~dir_inode ~name ~inode conn esys
    | `Names_del(dir_inode,name) ->
	names_del_e ~dir_inode ~name conn esys
    | `Admin_table_put(key,contents) ->
	admin_table_put_e ~key ~contents conn esys


(**********************************************************************)

(* FIXME: fsync each file *)

let dump_e conn esys dir =
  let rec next file_acc l =
    match l with
      | [] ->
          eps_e (`Done (List.rev file_acc)) esys
      | (table, columns) :: l' ->
          let file = sprintf "%s.dump" table in
          let full_file = sprintf "%s/%s" dir file in
          let ch = new Netchannels.output_channel (open_out_bin full_file) in
          let e =
            Pfs_db.copy_out_e
              conn
              esys
              ch
              table
              columns in
          ignore(e >>
                   (fun st -> (try ch # close_out() with _ -> ()); st));
          e ++ (fun () -> 
                  next (file :: file_acc) l'
               ) in
  next [] Nn_db_schema.tables
  ++ (fun files ->
        let descr_file = sprintf "%s/dump_descriptor" dir in
        let ch = open_out_bin descr_file in
        try
          List.iter
            (fun (table, _) ->
               fprintf ch "%s\n" table
            )
            Nn_db_schema.tables;
          close_out ch;
          eps_e (`Done ("dump_descriptor" :: files)) esys
        with
          | error ->
              close_out_noerr ch;
              raise error
     )


let load conn dir =
  let descr_tables = 
    Netchannels.with_in_obj_channel
      (new Netchannels.input_channel
           (open_in (sprintf "%s/dump_descriptor" dir)))
      Netchannels.lines_of_in_obj_channel in
  List.iter
    (fun tab ->
       if not(List.mem_assoc tab Nn_db_schema.tables) then
         failwith ("Nn_db.load: unknown table: " ^ tab)
    )
    descr_tables;
  Pfs_db.truncate conn descr_tables;
  List.iter
    (fun tab ->
       let columns = List.assoc tab Nn_db_schema.tables in
       Pfs_db.copy_in conn (sprintf "%s/%s.dump" dir tab) tab columns
    )
    descr_tables 


(**********************************************************************)

module I64Set = Plasma_util.I64Set
module IntMap = Plasma_util.IntMap
module StrMap = Plasma_util.StrMap


let cc_get_inodeblock inode blkidx_opt conn esys =
  new Uq_engines.map_engine
    ~map_done:(fun r ->
		 let tuples =
		   List.map
		     (function
			| [| idxmin_s; idxmax_s; ds_s; block_s |] ->
			    (Int64.of_string idxmin_s,
			     Int64.of_string idxmax_s,
			     int_of_string ds_s,
			     Int64.of_string block_s
			    )
			| _ -> assert false
		     )
		     r#get_all_l in
		 `Done tuples
	      )
    ( match blkidx_opt with
	| None ->
	    (new Pfs_db.async_exec conn esys 
	       ~expect:[Postgresql.Tuples_ok]
	       ~params:[| (* $1: *) Int64.to_string inode |]
	       "SELECT b.blkidxmin,b.blkidxmax,b.datastore,b.block \
                FROM inodeblocks b \
                WHERE b.inode = $1"
	    )
	| Some blkidx ->
	    (new Pfs_db.async_exec conn esys 
	       ~expect:[Postgresql.Tuples_ok]
	       ~params:[| (* $1: *) Int64.to_string inode;
                          (* $2: *) Int64.to_string blkidx;
               |]
	       "SELECT b.blkidxmin,b.blkidxmax,b.datastore,b.block \
                FROM inodeblocks b \
                WHERE b.inode = $1 AND b.blkidxmax >= $2 AND b.blkidxmin <= $2"
	    )
    )

let cc_announced = ref false 

let consistency_check_e ml trans_name conn esys =
  if not !cc_announced then (
    Netlog.logf `Info
      "Nn_db.consistency_check is enabled";
    cc_announced := true
  );

  let (fwd_tab, rev_tab) = get_ds_tables() in

  (* Load the inodeblocks of the ranges that are deleted:
     inodeblock_tuples maps the inode to a list of tuples
     (idxmin,idxmax,dn_id,block). We need this in order to map
     indexes to blocks later.
   *)
  let inodeblock_tuples = Hashtbl.create 25 in
  
  let rec load_inodeblock_tuples_e ml =
    match ml with
      | `Inodeblocks_del(inode,idx,len) :: ml' ->
	  load_inodeblock_tuples_idx_len_e inode idx len
	  ++ (fun () ->
		load_inodeblock_tuples_e ml'
	     )
      | `Inode_del inode :: ml' ->
	  load_inodeblock_tuples_all_e inode
	  ++ (fun () ->
		 load_inodeblock_tuples_e ml'
	     )
      | _ :: ml' ->
	  load_inodeblock_tuples_e ml'
      | [] ->
	  eps_e (`Done ()) esys

  and load_inodeblock_tuples_idx_len_e inode idx len =
    if len > 0L then (
      let tuples =
	try Hashtbl.find inodeblock_tuples inode
	with Not_found -> [] in
      let covered =
	List.exists
	  (fun (idxmin,idxmax,_,_) -> idx >= idxmin && idx <= idxmax)
	  tuples in
      if covered then
	load_inodeblock_tuples_idx_len_e inode (Int64.succ idx) (Int64.pred len)
      else (
	cc_get_inodeblock inode (Some idx) conn esys
	++ (fun new_tuples ->
	      Hashtbl.replace inodeblock_tuples inode (new_tuples @ tuples);
	      load_inodeblock_tuples_idx_len_e 
		inode (Int64.succ idx) (Int64.pred len)
	   )
      )
    )
    else
      eps_e (`Done ()) esys

  and load_inodeblock_tuples_all_e inode =
    cc_get_inodeblock inode None conn esys
    ++ (fun new_tuples ->
	  Hashtbl.replace inodeblock_tuples inode new_tuples;
	  eps_e (`Done()) esys
       )
  in


  (* Determine all blocks that are finally allocated/freed:
     b_alloc maps dn identities to the sets of blocks that are allocated by
     this transaction
     b_free maps dn identities to the sets of blocks that are freed
     The sets are disjoint
   *)
  let b_alloc = ref (IntMap.empty : I64Set.t IntMap.t) in
  let b_free = ref (IntMap.empty : I64Set.t IntMap.t) in

  let track_blocks() =
    List.iter
      (function
	 | `Inodeblocks_ins_abstract(_,blist) ->
	     StrMap.iter
	       (fun dn_str range ->
		  let dn_id =
		    try (Hashtbl.find rev_tab dn_str).Nn_datastores.id
		    with Not_found -> assert false in
		  let set =
		    ref (try IntMap.find dn_id !b_alloc
			 with Not_found -> I64Set.empty) in
		  Nn_blocklist.BlockRM.iter
		    (fun (b_first,b_last) binfo ->
		       let k = ref b_first in
		       while !k <= b_last do
			 let open Pfs_rpcapi_aux in
			 assert(Int64.sub !k binfo.index < binfo.length);
			 let dn_block = 
			   Int64.add (Int64.sub !k binfo.index) binfo.block in
			 set := I64Set.add dn_block !set;
			 k := Int64.succ !k
		       done
		    )
		    range;
		  b_alloc := IntMap.add dn_id !set !b_alloc
	       )
	       blist
	       
	 | `Inodeblocks_del(inode,idx,len) ->
	     let tuples =
	       try Hashtbl.find inodeblock_tuples inode
	       with Not_found -> [] in
	     let k = ref 0L in
	     while !k < len do
	       let i = Int64.add idx !k in
	       let matches =
		 List.filter
		   (fun (idxmin,idxmax,_,_) -> i >= idxmin && i <= idxmax)
		   tuples in
	       List.iter
		 (fun (idxmin,_,dn,block) ->
		    let dn_block = Int64.add (Int64.sub i idxmin) block in
		    ( try
			let alloc_set =
			  IntMap.find dn !b_alloc in
			let alloc_set' =
			  I64Set.remove dn_block alloc_set in
			b_alloc := IntMap.add dn alloc_set' !b_alloc;
		      with Not_found -> ()
		    );
		    let free_set =
		      try IntMap.find dn !b_free 
		      with Not_found -> I64Set.empty in
		    let free_set' =
		      I64Set.add dn_block free_set in
		    b_free := IntMap.add dn free_set' !b_free
		 )
		 matches;
	       k := Int64.succ !k
	     done

	 | `Inode_del inode ->
	     let tuples =
	       try Hashtbl.find inodeblock_tuples inode
	       with Not_found -> [] in
	     List.iter
	       (fun (idxmin,idxmax,dn,block) ->
		  let i = ref idxmin in
		  while !i <= idxmax do
		    let dn_block = Int64.add (Int64.sub !i idxmin) block in
		    ( try
			let alloc_set =
			  IntMap.find dn !b_alloc in
			let alloc_set' =
			  I64Set.remove dn_block alloc_set in
			b_alloc := IntMap.add dn alloc_set' !b_alloc;
		      with Not_found -> ()
		    );
		    let free_set =
		      try IntMap.find dn !b_free 
		      with Not_found -> I64Set.empty in
		    let free_set' =
		      I64Set.add dn_block free_set in
		    b_free := IntMap.add dn free_set' !b_free;
		    i := Int64.succ !i
		  done
	       )
	       tuples
	 | _ -> 
	     ()
      )
      ml in

  (* Compute from b_alloc/b_free the rows in blockalloc that will be modified.
     Results in a list of pairs (dn,row).
   *)
  let bmaprowsizeL = Int64.of_int Nn_config.bmaprowsize in
  let modified_rows() =
    let map set =
      I64Set.fold
	(fun dn_block acc ->
	   let dn_row = Int64.div dn_block bmaprowsizeL in
	   I64Set.add dn_row acc
	)
	set
	I64Set.empty in
    let b_alloc_rows = IntMap.map map !b_alloc in
    let b_free_rows = IntMap.map map !b_free in
    let r =
      IntMap.merge
	(fun dn set1 set2 ->
	   match set1,set2 with
	     | None, None -> None
	     | Some s1, None -> Some s1
	     | None, Some s2 -> Some s2
	     | Some s1, Some s2 -> Some(I64Set.union s1 s2)
	)
	b_alloc_rows
	b_free_rows in
    IntMap.fold
      (fun dn set acc ->
	 let blocks = I64Set.elements set in
	 List.map (fun b -> (dn,b)) blocks @ acc
      )
      r
      []
  in

  let db_rows = Hashtbl.create 17 in
  (* maps (dn,row) to the bitv *)
  let rec load_rows_e rows =
    match rows with
      | [] ->
	  eps_e (`Done()) esys
      | (dn,row) :: rows' ->
	  let bx = Int64.mul row bmaprowsizeL in
	  blockalloc_list_e
	    ~datastore:dn ~blkidx_min:bx ~blkidx_max:bx
	    conn esys 
	  ++ (fun r ->
		match r with
		  | [ blkidx, m ] ->
		      assert(blkidx = bx);
		      let bv = Bitv.of_string m in
		      Hashtbl.replace db_rows (dn,row) bv;
		      load_rows_e rows'
		  | _ -> 
		      assert false
	     )
  in

  let apply_alloc_free() =
    (* Modify db_rows as demanded by b_alloc and b_free *)
    IntMap.iter
      (fun dn set ->
	 I64Set.iter
	   (fun block ->
	      let row = Int64.div block bmaprowsizeL in
	      let base = Int64.mul row bmaprowsizeL in
	      let bv =
		try Hashtbl.find db_rows (dn,row)
		with Not_found -> assert false in
	      Bitv.set bv (Int64.to_int (Int64.sub block base)) true
	   )
	   set
      )
      !b_alloc;
    IntMap.iter
      (fun dn set ->
	 I64Set.iter
	   (fun block ->
	      let row = Int64.div block bmaprowsizeL in
	      let base = Int64.mul row bmaprowsizeL in
	      let bv =
		try Hashtbl.find db_rows (dn,row)
		with Not_found -> assert false in
	      Bitv.set bv (Int64.to_int (Int64.sub block base)) false
	   )
	   set
      )
      !b_free in

  let check() =
    (* Finally check whether [db_rows] reflects exactly the blockmap update *)
    let crows =
      Hashtbl.copy db_rows in
    List.iter
      (function
	 | `Blockalloc_upd(dn,blkidx,m) ->
	     let row = Int64.div blkidx bmaprowsizeL in
	     let bv1 = 
	       try Hashtbl.find crows (dn,row)
	       with Not_found -> 
		 failwith 
		   (sprintf
		      "Superflous blockalloc upd on datanode %d block %Ld"
		      dn blkidx) in
	     let bv2 =
	       Bitv.of_string m in
	     if bv1 <> bv2 then 
	       failwith (sprintf "Mismatch on datanode %d block %Ld"
			   dn blkidx);
	     Hashtbl.remove crows (dn,row)
	 | _ ->
	     ()
      )
      ml;
    if Hashtbl.length crows > 0 then
      failwith
	("Missing blockalloc upd on: " ^
	   (String.concat ";"
	      (List.map
		 (fun (dn,blkidx) ->
		    sprintf "datanode %d block %Ld" dn blkidx
		 )
		 (Hashtbl.fold
		    (fun (dn,row) _ acc -> 
		       (dn, Int64.mul row bmaprowsizeL) :: acc)
		    crows
		    []
		 )
	      )
	   )
	)
  in

  let get_inodes ml =
    let ht = Hashtbl.create 17 in
    List.iter
      (function
	 | `Inode_del inode
	 | `Inodeblocks_ins(inode,_)
	 | `Inodeblocks_ins_abstract(inode,_)
	 | `Inodeblocks_del(inode,_,_) ->
             Hashtbl.replace ht inode ()
	 | _ ->
             ()
      )
      ml;
    Hashtbl.fold (fun inode _ acc -> inode::acc) ht [] in
      
  let strrev s =
    let l = String.length s in
    let u = String.copy s in
    for k = 0 to l-1 do
      u.[k] <- s.[l-1-k]
    done;
    u in

  let hex_of_bitv bv =
    (* This is also LSB first, like Bitv.to_string *)
    let b = Buffer.create 64 in
    let p = ref 0 in
    let k = ref (Bitv.length bv) in
    while !k > 0 do
      let dv = Bitv.sub bv !p (min 4 !k) in
      let d = int_of_string ("0b" ^ strrev(Bitv.to_string dv)) in
      bprintf b "%x" d;
      k := !k - 4;
      p := !p + 4;
    done;
    Buffer.contents b in

  load_inodeblock_tuples_e ml
  ++ (fun () ->
	track_blocks();
	let mrows = modified_rows() in
	load_rows_e mrows
	++ (fun () ->
	      apply_alloc_free();
	      check();
	      eps_e (`Done()) esys
	   )
     )
  >> (function
	| `Done () -> `Done ()
	| `Aborted -> `Aborted
	| `Error e ->
	    let inodes = get_inodes ml in
	    Netlog.logf `Crit 
	      "Failed Nn_db.consistency check for transaction %s"
	      trans_name;
	    Netlog.logf `Crit
	      "Diag: exception=%s"
	      (Netexn.to_string e);
	    Netlog.logf `Crit
	      "Diag: Inodes=%s"
	      (String.concat ","
		 (List.map Int64.to_string inodes));
	    List.iter
	      (function
		 | `Blockalloc_upd(dn,blkidx,m) ->
		     let row = Int64.div blkidx bmaprowsizeL in
		     let bv_s = 
		       try hex_of_bitv(Hashtbl.find db_rows (dn,row))
		       with Not_found -> "not found" in
		     Netlog.logf `Crit
		       "Diag: dn=%d block=%Ld upd=%s"
		       dn blkidx (hex_of_bitv(Bitv.of_string m));
		     Netlog.logf `Crit
		       "Diag: dn=%d block=%Ld exp=%s"
		       dn blkidx bv_s
		 | _ -> ()
	      )
	      ml;
   (*
	    Hashtbl.iter
	      (fun (dn,row) bv ->
		 let block = Int64.mul row bmaprowsizeL in
		 Netlog.logf `Crit
		   "Diag: dn=%d block=%Ld exp=%s"
		   dn block (hex_of_bitv bv)
	      )
	      crows;
    *)
	    `Error e
     )
