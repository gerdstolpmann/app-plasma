(*
  Copyright 2013 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Uq_engines.Operators

let dlogf = Plasma_util.dlogf

module Nsync = Pfs_rpcapi_clnt.Make'Namesync(Rpc_proxy.ManagedClient)
module Coord = Pfs_rpcapi_clnt.Make'Coordination(Rpc_proxy.ManagedClient)

type client = Rpc_proxy.ManagedClient.mclient

(* let cauth = Pfs_auth.extract_client_config cf cf#root_addr *)

let create_client config conn cauth esys =
  dlogf
    "Nn_sync.create_client";
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Namesync.V1._program;
                  Pfs_rpcapi_clnt.Coordination.V1._program;
                ]
      ~msg_timeout:config#nn_node_timeout
      ~msg_timeout_is_fatal:true
      ~initial_ping:true
      ~auth_methods:(Pfs_auth.rpc_proxy_auth_methods cauth true)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  mclient


let create_sync_client_for conf remote cauth =
  let (host,port) = Plasma_util.parse_host_port remote in
  let sockaddr = Plasma_util.sockaddr_of_host_port (host,port) in
  let conn = Rpc_client.connector_of_sockaddr sockaddr in
  let esys = Unixqueue.create_unix_event_system() in
  create_client conf conn cauth esys


let trigger_shutdown mclient =
  Rpc_proxy.ManagedClient.trigger_shutdown mclient (fun () -> ())

type status = [ `Running | `Ok | `Error ]

type cp_info =
    { cp_dump : Nn_dumps.dump_entry;
      cp_status : status;
    }

type cp_shared =
    { cp_infos : cp_info list;
      cp_running : bool;
      cp_blocked : float option;
    }

module Cp_var =
  Netplex_sharedvar.Make_var_type(struct type t = cp_shared end)

let cp_var_name = "plasma.namenode.namesync.checkpointing"
let cp_var_lock_name = "plasma.namenode.namesync.checkpointing.lock"

let init() =
  (* Call this once *)
  ignore(Netplex_sharedvar.create_var ~enc:true cp_var_name);
  Cp_var.set 
    cp_var_name
    { cp_infos = [];
      cp_running = false;
      cp_blocked = None
    }

let cp_timestamp t =
  (* get the timestamp string for nref *)
  let nd = Netdate.create ~zone:0 t in
  Netdate.format ~fmt:"%Y%m%d%H%M%S" nd

let cp_filter conf cp =
  (* filter out non-existent dumps *)
  let cp_infos =
    List.filter
      (fun cpi -> Nn_dumps.dump_exists conf cpi.cp_dump)
      cp.cp_infos in
  { cp with cp_infos }

let cp_update cp dump status =
  (* return cp with updated dump *)
  let cp_infos =
    List.map
      (fun cpi ->
         let open Nn_dumps in
         let d = cpi.cp_dump in
         let eq =
           d.dump_prefix = dump.dump_prefix &&
           d.dump_ref = dump.dump_ref &&
           d.dump_suffix = dump.dump_suffix in
         if eq then
           { cp_dump = dump;
             cp_status = status
           }
         else
           cpi
      )
      cp.cp_infos in
  { cp with cp_infos }


let modify_cp_var f =
  let m = Netplex_mutex.access cp_var_lock_name in
  Netplex_mutex.lock m;
  try
    let cp = Cp_var.get cp_var_name in
    let (cp_mod, r) = f cp in
    ( match cp_mod with
        | None -> ()
        | Some cp' ->  Cp_var.set cp_var_name cp'
    );
    Netplex_mutex.unlock m;
    r
  with
    | error ->
         Netplex_mutex.unlock m;
         raise error


let get_dump_status conf dump =
  (* No lock needed here because read accesses are atomic anyway *)
  let cp = Cp_var.get cp_var_name in
  try
    let cpi =
      List.find
        (fun cpi ->
           Nn_dumps.equal cpi.cp_dump dump
        )
        cp.cp_infos in
    ( cpi.cp_status :> [ status | `Stale | `Not_found ] )
  with
    | Not_found ->
         if Nn_dumps.dump_exists conf dump then
           `Stale
         else
           `Not_found


(* TODO: remove superflous rows from translog *)

let checkpoint_worker_e conf esys dump =
  let t = Nn_db.transact esys 0 in
  let e =
    t # isolevel `Repeatable_read
    ++ (fun () ->
          let d = Nn_dumps.dump_directory conf dump in
          Nn_db.dump_e t#ro_connection esys d
          ++ (fun files ->
                t # commit()
                ++ (fun () ->
                      (* there cannot be competing updates of cp *)
                      let dump' = Nn_dumps.finish_dump conf dump in
                      let cp = Cp_var.get cp_var_name in
                      let cp' = cp_update cp dump' `Ok in
                      let cp'' = { cp' with cp_running = false } in
                      Cp_var.set cp_var_name cp'';
                      eps_e (`Done()) esys
                   )
             )
       ) in
  e >> (fun st ->
          if st <> `Done () then (
            ignore(t # rollback());
            let cp = Cp_var.get cp_var_name in
            let cp' = cp_update cp dump `Error in
            let cp'' = { cp' with cp_running = false } in
            Cp_var.set cp_var_name cp'';
          );
          st
       )


let checkpoint conf esys =
  modify_cp_var
    (fun cp ->
       let t = Unix.gettimeofday() in
       if cp.cp_running then
         (None, `Running)
       else
         let is_blocked =
           match cp.cp_blocked with
             | None -> false
             | Some t0 -> t < t0 in
         if is_blocked then
           (None, `Blocked)
         else (
           let nref = cp_timestamp t in
           let dump = Nn_dumps.create_dump conf ("checkpoint", nref) in
           let cpi =
             { cp_dump = dump;
               cp_status = `Running
             } in
           let cp = cp_filter conf cp in
           let cp =
             { cp_infos = cpi :: cp.cp_infos; 
               cp_running = true;
               cp_blocked = None
             } in
           ignore(checkpoint_worker_e conf esys dump);
           (Some cp, `Dump dump)
         )
    )


let pack_dump d =
  { Pfs_rpcapi_aux.dump_prefix = Nn_dumps.(d.dump_prefix);
    dump_ref = Nn_dumps.(d.dump_ref);
    dump_suffix = Nn_dumps.(d.dump_suffix);
    dump_ts = Int64.of_float Nn_dumps.(d.dump_timestamp)
  }

let unpack_dump dump =
  { Nn_dumps.dump_prefix = Pfs_rpcapi_aux.(dump.dump_prefix);
    dump_ref = Pfs_rpcapi_aux.(dump.dump_ref);
    dump_suffix = Pfs_rpcapi_aux.(dump.dump_suffix);
    dump_timestamp = Int64.to_float Pfs_rpcapi_aux.(dump.dump_ts);
    dump_finished = false
  }


let check_remote ~req_coord clustername client =
  let remote_clustername =
    Coord.V1.clustername client () in
  if clustername <> remote_clustername then
    failwith "Nn_sync: unexpected name of remote cluster";
  if req_coord then (
    let remote_is_coord =
      Coord.V1.is_coordinator client clustername in
    if not remote_is_coord then
      failwith "Nn_sync: the remote node is not the coordinator";
  )


let latest_dump dumps =
  let best = ref None in
  let best_ts = ref 0.0 in
  List.iter
    (fun d ->
       if Nn_dumps.(d.dump_timestamp) > !best_ts then (
         best := Some d;
         best_ts := Nn_dumps.(d.dump_timestamp)
       )
    )
    dumps;
  !best


let determine_latest_checkpoint ?(req_coord=false) 
                                conf cauth clustername remote =
  (* Returns the best remote dump, or Not_found *)
  let client = create_sync_client_for conf remote cauth in
  try
    check_remote ~req_coord clustername client;
    let search_dump =
      { Nn_dumps.dump_prefix = "checkpoint";
        dump_ref = "";
        dump_suffix = "";
        dump_finished = false;
        dump_timestamp = 0.0
      } in
    let dumps =
      Nsync.V1.list_dumps client (pack_dump search_dump) in
    let best = latest_dump (List.map unpack_dump (Array.to_list dumps)) in
    Rpc_proxy.ManagedClient.sync_shutdown client;
    match best with
      | None ->
           raise Not_found
      | Some d ->
           d
  with
    | error ->
         Rpc_proxy.ManagedClient.sync_shutdown client;
         raise error


let one_megL =
  Int64.of_int (1024 * 1024)

let get_remote_dump ?(req_coord=false) conf cauth clustername remote dump =
  (* Copies a dump from a remote node to the local node, i.e. the files
     are copied to a local directory. The name of the local dump may have
     a different suffix.

     clustername: the cluster the remote node is expected to be part of

     remote: the address of the remote namenode, in host:port syntax

     req_coord: it is required that the remote node is a coordinator
   *)
  let client = create_sync_client_for conf remote cauth in
  try
    check_remote ~req_coord clustername client;
    let files =
      Nsync.V1.list_dump_files client (pack_dump dump) in
    let ldump =
      Nn_dumps.create_dump conf Nn_dumps.(dump.dump_prefix, dump.dump_ref) in
    let ldir =
      Nn_dumps.dump_directory conf ldump in
    Array.iter
      (fun df ->
         let (name, size) = 
           Pfs_rpcapi_aux.(df.dumpfile_name,df.dumpfile_size) in
         let lfull_file = ldir ^ "/" ^ name in
         let f = 
           Unix.openfile
             lfull_file [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC] 0o600 in
         try
           let pos = ref 0L in
           while !pos < size do
             let len =
               Int64.to_int (min (Int64.sub size !pos) one_megL) in
             let r = 
               Nsync.V1.read_dump_file client (pack_dump dump,name,!pos,len) in
             match r with
               | `ok data ->
                    Netsys.really_gwrite `Read_write f data 0 len;
                    pos := Int64.add !pos (Int64.of_int len)
               | #Plasma_util.errno as code ->
                    failwith ("Nn_sync.get_remote_dump: code " ^ 
                                Plasma_util.string_of_errno code)
           done;
           Unix.close f
         with
           | error -> Unix.close f; raise error
      )
      files;
    let odump = Nn_dumps.finish_dump conf ldump in
    Rpc_proxy.ManagedClient.sync_shutdown client;
    odump
  with error ->
    Rpc_proxy.ManagedClient.sync_shutdown client;
    raise error

      
