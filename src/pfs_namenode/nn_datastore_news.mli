(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Getting news about datastores and their availability

    The following functions assume an arbitrary Netplex container as
    environment (i.e. any process). The feed is first available when the first
    notification message has arrived.

    Due to the nature of a feed, the information about datastore updates
    is delayed.

 *)

open Nn_datastores

exception Unavailable
  (** Raised if the news feed is not yet available (try again later),
      and the [wait] option is not set.
   *)

val force : unit -> unit
  (** Forces that the new version is fetched *)

val list : ?wait:bool -> unit -> datastore list
  (** Returns the list of all datastores *)

val get : ?wait:bool -> string -> datastore
  (** Get the store record by identity string *)

val is_alive : ?wait:bool -> string -> bool
  (** [is_alive identity]: Returns whether the datanode having this store
      is good as seen by the monitor. Fails if the monitor is not started.
   *)

val signal_db_change : int -> Unix.sockaddr option -> unit
  (** [signal_db_change id node_opt]: Signal to the monitor that
      the db row with [id] has changed and must be reread. If [node_opt]
      is given, it is tried to discover this datanode, and to attach
      it to the system
   *)

val receive_message : Netplex_types.container -> string -> string array -> unit
  (** Needs to be called from the [receive_message] hook in order to
      subscribe to this news channel
   *)

val post_add_hook : Netplex_types.socket_service -> 
                    Netplex_types.controller -> unit
  (** Needs to be called from the [post_add_hook] *)
