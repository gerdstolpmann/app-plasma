(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Block allocation algorithm *)

val allocate_trivial_strategy : 
      Nn_blockmap.blockmap_t list ->
      Nn_alloc_types.transacted_datastores ->
      Nn_blockmap.owner ->
      int ->
      int64 ->
      int ->
         Nn_blocklist.blocklist
  (** [allocate_trivial_strategy bmaps td owner repl index number]:
      Allocates blocks for the index range [index] to [index+number-1].
      The replication factor is [repl]. The blocks are allocated in the
      passed blockmaps [bmaps], and will be assigned to [owner].
      Tickets are generated for the transaction identified by [td].

      This is the "trivial" algorithm. It iterates over the index range
      and tries to allocate each block separately, without taking advantage
      of the range. This algorithm should not be used anymore; it is
      kept in the code base purely for documentation purposes.
   *)
