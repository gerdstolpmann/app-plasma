(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** State manager: Provide the logical view of the state, and forward changes
    to the database and to the nameslaves (2-phase commit)
 *)

open Nn_config
open Nn_blockmap
open Nn_alloc_types


type errno =
    Plasma_util.errno

exception Transaction_error of errno

type counter_name =
    [ `Transactions_total | `Commits_total | `RPC_filesystem ]
    (** Performance counters:
	- [`Transactions_total]: number of transaction starts
	- [`Commits_total]: number of writing commits
	- [`RPC_filesystem]: number of RPC calls of the "Filesystem" program
     *)

val string_of_counter_name : counter_name -> string
val counters : counter_name list


type owner
val create_owner : unit -> owner
val string_of_owner : owner -> string

(** This object exists once in the [nn_master] container, and collects
    information from not yet committed views (such as which ID's are
    used), so that the views can avoid clashes between each other.
    Also caching of committed values.
 *)
class type shared_state_t =
object
  method event_system : Unixqueue.event_system
    (** The common event system *)
  method node_config : nn_node_config
    (** the configuration *)
  method cur_rev_ident : string
    (** Returns current revision identifier *)
  method new_rev_ident : unit -> string
    (** Creates a new revision identifier *)
  method set_rev_ident : string -> unit
    (** Sets the identitifer returned by [cur_rev_ident] *)
  method datastore_max_id : int option
    (** Maximum ID used in the datastore table (None if use_id was never called)
     *)
  method datastore_use_id : int -> unit
    (** Record that this ID is now also used in a transaction *)
  method inode_max_id : int64 option
    (** Maximum ID used in the inode table (None if use_id was never called)
     *)
  method inode_use_id : int64 -> unit
    (** Record that this ID is now also used in a transaction *)
  method inode_get_new_id_serialized :
           (Unixqueue.event_system -> int64 Uq_engines.engine) ->
           int64 Uq_engines.engine
    (** Serializer for getting a new ID from the inode table *)
  method commit_monitor : Nn_commit_monitor.commit_monitor
    (** The commit monitor controls which commits can be run in parallel
     *)
  method slaves : (string * Rpc_proxy.ManagedClient.mclient * bool) list
    (** Connections to the name slaves as triples (name,client,enabled). 
        These must only be used in the commit protocol.
     *)
  method disable_slave : string -> unit
    (** [disable_slave name] *)
  method is_slave_disabled : string -> bool
    (** Whether this slave is disabled *)
  method minimum_slaves : int
    (** How many enabled slaves we need at minimum *)
  method enough_slaves : bool
    (** Whether there are still enough enabled slaves *)
  method master : string
    (** The master in host:port syntax *)
  method master_enabled : bool
    (** Whether the master (i.e. this process) is still in sync with the rest *)
  method disable_master : unit -> unit
    (** Disable this master *)
(*
  method exists_blockmap : string -> bool
    (** Whether we have a blockmap for this identity *)
  method create_blockmap : blockmap_t -> unit
    (** [create_blockmap m]: Creates a map of free/used/reserved
        blocks.
     *)
 *)
  method blockmaps : blockmap_t list
    (** All blockmaps so far loaded *)
  method get_blockmap : string -> blockmap_t
    (** Get the blockmap for this identity, or [Not_found] *)
  method blockmap_get_e : identity:string -> 
                              blockmap_t Uq_engines.engine
    (** If the blockmap exists, return it. If it does not exist,
        load it from the db.
        Loading is serialized if several users need the blockmap.
     *)
(*
  method blockmap_prioritizer : unit Uq_engines.prioritizer
    (** Used to prioritize commits before other accesses to the blockmap.
	Commits run at priority 1, other accesses at priority 2.

	The blockmap commit is part of the [post_commit] actions.
	For simplicity the whole [post_commit] is prioritized this way.

	The important "other access" is setting the pin bits. This
	must not overlap with blockmap commits.
     *)
 *)
  method blockmap_serialized : 
           int64 list ->
           (Unixqueue.event_system -> unit Uq_engines.engine) ->
           unit Uq_engines.engine
    (** This is used to synchronize accesses to the in-memory blockmap
	with the per-inode blocklists in the db. The passed list are the
	inodes that are affected by the serialized operation.

	Currently we have here inode granularity (each inode has a separate
	lock). A possible improvement would be reader/writer locks.
     *)
  method forget_identity : string -> unit
    (** Forget everything about this datastore identity *)

  method lock_inode : int64 -> owner -> (unit -> unit) option
    (** Returns [Some unlock] if successful where calling [unlock] removes
        the lock, or reverts to the previous state (1-step undo)
     *)
  method unlock_inodes : owner -> unit
    (** unlock all inodes of this owner *)

  method lock_filename : (int64 * string) -> [`Link | `Unlink | `Exists] -> 
                         owner -> 
                         (unit -> unit) option
    (** [lock_filename (dir_inode,name) lock_type owner]: Locks the [name]
	member of directory [dir_inode].

        Lock types:
         - [`Link]: States that the file is created, and must not be
           created by another transaction. A [`Link] lock is exlusive. 
         - [`Unlink]: States that the file is removed, and must not be
	   removed by another transaction. An [`Unlink] lock is exclusive.
         - [`Exists:] States that the file exists. Several transactions can
           request this. (Used for ensuring that parent directories exist
           until the end of the transaction.)

	Certain lock type changes are allowed provided this is ok with the
        protected operations (e.g. from [`Unlink] to [`Link]). It is also 
        possible that a lock already exists with stronger guarantees (e.g.
        we have already [`Link] which implies [`Exists]).

        Returns [Some unlock] if successful where calling [unlock] removes
        the lock, or reverts to the previous state (1-step undo).
     *)
  method unlock_filenames : owner -> unit
    (** unlock all filenames of this owner *)
  method inode_modify_use_counts : 
            transactions:(int->int) -> int64 -> unit
    (** The use count for inodes:

	- transactions: number of open transactions (no matter whether
	  the transaction is reading or writing to the inode)

	This functions modifies these counts.

	If [transactions] drops to 0, the entry is automatically removed.
     *)
  method inode_use_counts : int64 -> int
    (** Returns [(transactions)] numbers (or raises [Not_found]) *)
  method zombie_inodes_exist : bool
    (** Whether zombie inodes exist (unused anonymous inodes) *)
  method set_zombie_inodes : bool -> unit
    (** Set whether zombie inodes exist *)
  method cauth : Pfs_auth.client_auth
    (** Return the cauth record *)
  method db_trans_for_commit : unit -> Nn_db.transaction
    (** Returns the db transaction that is used for commits *)
  method incr_count : counter_name -> unit
    (** Increases this shared counter *)
  method reset_count : counter_name -> int
    (** Resets this shared counter, and returns the value before resetting *)
end


type sh_state_shm
  (** The shm part of shared_state_t. This type is marshallable.
   *)


type journal =
    Nn_db.modification Queue.t
(** A journal is a queue of database modifications. Journals are the
    entities that are sent to the slaves
 *)

(** A view is a high-level transaction, involving both the local db and
    the slaves 
 *)
class type view_t =
object
  method pre_commit : journal
    (** Prepares the commit, and returns the journal to commit. This method
	is called before the monitored commit section starts.
     *)
  method pre_commit_mon : journal
    (** Prepares the commit, and returns additional journal entries to commit.
	This method is called before and at the beginning of the monitored
	commit section (i.e. twice)
     *)
  method post_commit : bool -> unit
    (** Needs to be called after a commit. [post_commit true] if the
        commit was successful, and [commit_commit false] if not.
        This is important for updating the state of caches: In the
        first case cache modifications are made permanent, in the
        second case they are undone.
     *)
  method rollback : unit -> unit
    (** Rolls this view back *)
 
end


(** View on datastore state *)
class type ds_view_t =
object
  inherit view_t
  (* method reservation_secret : string*)
    (** The secret random number used for securing transactional writes *)
  method datastore_new_id_e : unit -> int Uq_engines.engine
    (** Allocates a new identifier for use in the [datastore] table *)
  method datastore_list_e : unit -> 
                              Nn_datastores.datastore list Uq_engines.engine
    (** Get the contents of the [datastore] table *)
  method datastore_upd_e : id:int ->
                           identity:string ->
                           size:int64 ->
                           enabled:bool ->
                              unit Uq_engines.engine
    (** Change the row in [datastore] identified by [id] so that the
        fields [identity], [size], and [enabled] are updated.
     *)
  method datastore_del_e : id:int ->
                             unit Uq_engines.engine
    (** Delete this row in the [datastore] table *)
  method admin_table_put_e : key:string -> contents:string -> 
                                unit Uq_engines.engine
    (** Write an admin table (this does not fit anywhere else) *)
end


(** View on blockmap state: *)
class type bm_view_t = 
object
  inherit view_t
  method release : unit -> unit
    (** Release the pinned blocks *)
  method blockmap_get_e : identity:string ->
                            blockmap_t Uq_engines.engine
    (** Get the contents of the [blockalloc] table for this datastore
        [identity], wrapped into a [blockmap_t] object
     *)
  method blockmap_alloc_e : inode:int64 ->
                            rpolicy:rpolicy ->
                            index:int64 ->
                            number:int ->
                            pref:string list ->
                            transacted_datastores ->
                            reserve_info ->
                              Nn_blocklist.blocklist Uq_engines.engine
    (** Reserve blocks and allocate at commit time. [index] is the first
        index number to use in the returned list of blocks, [index+number-1]
        is the last index number. For each index blocks are allocated on
        the datanodes according to [rpolicy] (usually with replicas).
        At reservation time, only the blockmap is updated; at commit time
        the [blockalloc] database table. The blocks are not associated with
        inodes yet.

	[pref]: list of preferred datanodes, given as identity strings.

	[reserve_info]: gives hints where to allocate blocks
     *)
  method blockmap_allocsize : int
    (** Returns the allocation size *)
  method blockmap_dealloc_e : inode:int64 -> (string * int64 * int64) list ->
                                unit Uq_engines.engine
    (** Mark blocks and deallocate at commit time. The passed list consists
        of pairs [(identity,block,number)].
     *)

  method blockmap_load_e : unit -> unit Uq_engines.engine
    (** Ensures that all known blockmaps are loaded *)
  method blockmap_pin : inode:int64 -> (string * int64 * int64) list -> unit
    (** Pin these blocks until commit/rollback. It is required
	that the maps were loaded first ([blockmap_load_e]).
     *)
end


class type inode_view_t =
object
  inherit view_t

  method as_owner : owner
    (** The owner tuple used by this view *)

  (** {2 inode access} *)

  method inode_get_e : id:int64 ->
                         Pfs_rpcapi_aux.inodeinfo option Uq_engines.engine
  method inode_get_with_lock_e : id:int64 ->
                         Pfs_rpcapi_aux.inodeinfo option Uq_engines.engine
    (** like [inode_get_e] but immediately acquires a write lock for the
	inode
     *)
  method inode_alloc_e : Pfs_rpcapi_aux.inodeinfo ->
                            int64 Uq_engines.engine
    (** Inserts a new inode and returns the ID. One cannot set the 
        blocklimit (it is always 0)
     *)
  method inode_upd_e : id:int64 ->
                       keep_blocklimit:bool ->
                       permission:(Pfs_rpcapi_aux.inodeinfo -> errno option) ->
                       Pfs_rpcapi_aux.inodeinfo ->
                         unit Uq_engines.engine
    (** Updates the inode. Note that an update of [blocklimit] is only allowed
        if [keep_blocklimit=false]. [filetype] changes are silently ignored.

	[permission]: may indicate an error. This function is called with the
	previous version of inodeinfo.
     *)
  method inode_dealloc_e :  id:int64 -> unit Uq_engines.engine
    (** deletes the inode. 
	precondition: there must not be any filenames pointing to [id]. This
	is not checked, however.
     *)
  method inode_get_blocks_e : id:int64 ->
                              blkidx:int64 ->
                              len:int64 -> pin_flag:bool ->
                              bm_view_t ->
                                Nn_blocklist.blocklist Uq_engines.engine 
    (** returns blocks. These blocks are immediately marked as to pin in
        the blockmap (if pin_flag)
     *)
  method inode_upd_blocks_e : id:int64 ->
                              del:(int64 * int64) ->
                              add:Nn_blocklist.blocklist ->
                              set_mtime:bool ->
                              set_blocklimit:int64 ->
                              permission:(Pfs_rpcapi_aux.inodeinfo -> errno option) ->
                                unit Uq_engines.engine
    (** First delete the blocks in the [del] range, then add
        the blocks from [add].

        If [set_mtime], sets the mtime fields in inodeinfo to the server
	time.

	[set_blocklimit]: the new [blocklimit] field in inodeinfo.

	[permission]: may indicate an error. This function is called with the
	previous version of inodeinfo.
     *)

  method allocinfo_get_e : id:int64 -> Nn_db.allocinfo list Uq_engines.engine
     (** Gets the allocinfo list for this inode *)

  method inode_upd_time : id:int64 ->
                          mtime:Pfs_rpcapi_aux.time option ->
                          ctime:Pfs_rpcapi_aux.time option ->
                              unit
    (** This update of mtime/ctime is lock-free *)
  method inode_set_anonymous : id:int64 -> unit
    (** Sets that the inode is anonymous *)

  method inode_set_used : int64 -> unit
    (** Fake that this inode is considered as used. This enables that
	zombie inodes can be accessed
     *)


  (** {2 Filenames} *)

  (** For simplicity, the filename access is also defined here. Inodes and
      filenames interact with each other to some extent, and it is important
      that inode and filename operations are executed in the right order
      with respect to each other.
   *)

  method filename_get_e : dir_inode:int64 -> name:string -> 
                             int64 option Uq_engines.engine
    (** Returns the inode of the member [name] of directory [dir_inode] *)
  method filename_rev_get_e : inode:int64 ->
                                (int64 * string) list Uq_engines.engine
    (** filename_rev_get does not acquire locks. Returns [] for inode=1 *)
  method filename_rev_get_dir_e : inode:int64 ->
                                (int64 * string * (unit->unit)) Uq_engines.engine
    (** Works only for directories, and obtains an existence lock for the
	directory. May [ECONFLICT]. 

	The return value is [(parent_dir_inode, dir_name, unlock)]

	For inode=1, the value is [(1,"/",unlock)].
     *)
  method filename_parent_dir_e : inode:int64 ->
                                   int64 option Uq_engines.engine
    (** Returns the parent directory *)
  method filename_count_e : inode:int64 ->
                              int Uq_engines.engine
    (** The count is the number of names for an inode (same as length of
        list returned by [filename_rev_get_e])
     *)

  method filename_list_e : dir_inode:int64 ->
                               (string * int64) list Uq_engines.engine
    (** Lists subnames of a potential directory name, together with inodes *)
  method filename_link_e : dir_inode:int64 -> name:string -> inode:int64 ->
                             unit Uq_engines.engine
    (** adds this filename. It is checked that the filename does not exist
        before the addition. Also the parent must exist, and have the
        type "directory". The parent must have a name.
        If the linked inode is a directory itself, it is
	checked that there is no other name for it yet.
     *)
  method filename_unlink_e : dir_inode:int64 -> name:string ->
                             owner_restr:string option ->
                               unit Uq_engines.engine
    (** deletes this filename from this directory. If [owner_restr] is passed,
	it is required that the inode is owned by this user (or [EPERM])
     *)
  method filename_rename_e : from_dir_inode:int64 -> from_name:string ->
                             to_dir_inode:int64 -> to_name:string ->
                             owner_restr:string option ->
                               unit Uq_engines.engine
  method delayed_inode_deletes : int64 list * int64 list
    (** Returns [del_now, del_later]:
	[del_now] are the inodes to be deleted at the end of the transaction. 
	[del_later] are the inodes to be marked as anonymous (because there
	are still users)
     *)
end


(** The following classes are used as follows: First, a [shared_state]
    is created. This object is passed on as arguments to the view
    objects. The views, if all connected with the same [shared_state],
    can be committed in one go (in {!Nn_commit}).
 *)

val sh_state_shm : Netmcore.res_id ->   (* pool *)
                   int ->   (* index of the coordinator in nn_nodes *)
                   string ->  (* initial cur_rev_ident *)
                   sh_state_shm

class shared_state : Unixqueue.event_system -> 
               nn_node_config ->  
               Pfs_auth.client_auth ->
               sh_state_shm ->
               shared_state_t

class revclear_view : view_t
  (** This "view" just removes all revisions, and replaces them with a
      single successor
   *)

class ds_view : shared_state_t -> ds_view_t

class bm_view : shared_state_t -> bm_view_t

class inode_view : shared_state_t -> inode_view_t

val encap_time : float -> Pfs_rpcapi_aux.time
  (** Turn the time (as seconds since epoch) into the RPC version *)
