(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf
open Nn_config
open Nn_alloc_types
open Plasma_util.Operators

let dlog = Plasma_util.dlog
let dlogr = Plasma_util.dlogr

module A = Pfs_rpcapi_aux

type errno =
    Plasma_util.errno

exception Transaction_error of errno

let trans_error errno =
  Transaction_error errno

let transerr_e code esys =
  eps_e (`Error(Transaction_error code)) esys

let () =
  Netexn.register_printer
    (Transaction_error `enotrans)
    (fun e ->
       match e with
	 | Transaction_error te ->
	     let te_text = Plasma_util.string_of_errno te in
	     "Transaction_error(" ^ te_text ^ ")"
	 | _ -> assert false
    )

module StrSet = Set.Make(String)

type owner = int * int
    (* (process ID, sequential number) *)

type lock_type = [`Link | `Unlink | `Exists]

type lock_owner = [`Link of owner | `Unlink of owner | `Exists of owner list]


let create_owner() =
  (* Assume multi-processing *)
  let seqnr =
    Oo.id (object end) in
  (Unix.getpid(), seqnr)

let string_of_owner (pid,seqnr) =
  sprintf "<%d.%d>" pid seqnr

type counter_name =
    [ `Transactions_total | `Commits_total | `RPC_filesystem ]

let counters = 
  [ `Transactions_total; `Commits_total; `RPC_filesystem ]

let string_of_counter_name =
  function
    | `Transactions_total -> "transactions_total"
    | `Commits_total -> "commits_total"
    | `RPC_filesystem -> "RPC_filesystem"


class type shared_state_t =
object
  method event_system : Unixqueue.event_system
  method node_config : nn_node_config
  method cur_rev_ident : string
  method new_rev_ident : unit -> string
  method set_rev_ident : string -> unit
  method datastore_max_id : int option
  method datastore_use_id : int -> unit
  method inode_max_id : int64 option
  method inode_use_id : int64 -> unit
  method inode_get_new_id_serialized :
           (Unixqueue.event_system -> int64 Uq_engines.engine) ->
           int64 Uq_engines.engine
  method commit_monitor :  Nn_commit_monitor.commit_monitor
  method slaves : (string * Nn_push.client * bool) list
  method disable_slave : string -> unit
  method minimum_slaves : int
  method enough_slaves : bool
  method is_slave_disabled : string -> bool
  method master : string
  method master_enabled : bool
  method disable_master : unit -> unit
(*
  method exists_blockmap : string -> bool
  method create_blockmap : Nn_blockmap.blockmap_t -> unit
 *)
  method blockmaps : Nn_blockmap.blockmap_t list
  method get_blockmap : string -> Nn_blockmap.blockmap_t
  method blockmap_get_e : identity:string -> 
                              Nn_blockmap.blockmap_t Uq_engines.engine
  method blockmap_serialized :
           int64 list -> (* inodes *)
           (Unixqueue.event_system -> unit Uq_engines.engine) ->
           unit Uq_engines.engine
  method forget_identity : string -> unit
  method lock_inode : int64 -> owner -> (unit -> unit) option
  method unlock_inodes : owner -> unit
  method lock_filename : (int64 * string) -> lock_type -> owner -> 
                         (unit -> unit) option
  method unlock_filenames : owner -> unit
  method inode_modify_use_counts : 
            transactions:(int->int) -> int64 -> unit
  method inode_use_counts : int64 -> int
  method zombie_inodes_exist : bool
  method set_zombie_inodes : bool -> unit
  method cauth : Pfs_auth.client_auth
  method db_trans_for_commit : unit -> Nn_db.transaction
  method incr_count : counter_name -> unit
  method reset_count : counter_name -> int
end


type journal =
    Nn_db.modification Queue.t

class type view_t =
object
  method pre_commit : journal
  method pre_commit_mon : journal
  method post_commit : bool -> unit
  method rollback : unit -> unit
end


class type ds_view_t =
object
  inherit view_t
  method datastore_new_id_e : unit -> int Uq_engines.engine
  method datastore_list_e : unit -> 
                              Nn_datastores.datastore list Uq_engines.engine
  method datastore_upd_e : id:int ->
                           identity:string ->
                           size:int64 ->
                           enabled:bool ->
                              unit Uq_engines.engine
  method datastore_del_e : id:int ->
                             unit Uq_engines.engine
  method admin_table_put_e : key:string -> contents:string -> 
                                unit Uq_engines.engine
end


class type bm_view_t =
object
  inherit view_t
  method release : unit -> unit
  method blockmap_get_e : identity:string ->
                            Nn_blockmap.blockmap_t Uq_engines.engine
  method blockmap_alloc_e : inode:int64 ->
                            rpolicy:rpolicy ->
                            index:int64 ->
                            number:int ->
                            pref:string list ->
                            transacted_datastores ->
                            Nn_blockmap.reserve_info ->
                              Nn_blocklist.blocklist Uq_engines.engine
  method blockmap_allocsize : int
  method blockmap_dealloc_e : inode:int64 -> (string * int64 * int64) list ->
                                unit Uq_engines.engine
  method blockmap_load_e : unit -> unit Uq_engines.engine
  method blockmap_pin : inode:int64 -> (string * int64 * int64) list -> unit
end


class type inode_view_t =
object
  inherit view_t
  method as_owner : owner
  method inode_get_e : id:int64 ->
                         Pfs_rpcapi_aux.inodeinfo option Uq_engines.engine
  method inode_get_with_lock_e : id:int64 ->
                         Pfs_rpcapi_aux.inodeinfo option Uq_engines.engine
  method inode_alloc_e : Pfs_rpcapi_aux.inodeinfo ->
                            int64 Uq_engines.engine
  method inode_upd_e : id:int64 ->
                       keep_blocklimit:bool -> 
                       permission:(Pfs_rpcapi_aux.inodeinfo -> errno option) ->
                       Pfs_rpcapi_aux.inodeinfo ->
                         unit Uq_engines.engine
  method inode_dealloc_e :  id:int64 -> unit Uq_engines.engine
  method inode_get_blocks_e : id:int64 ->
                              blkidx:int64 ->
                              len:int64 -> pin_flag:bool ->
                              bm_view_t ->
                                Nn_blocklist.blocklist Uq_engines.engine 
  method inode_upd_blocks_e : id:int64 ->
                              del:(int64 * int64) ->
                              add:Nn_blocklist.blocklist ->
                              set_mtime:bool ->
                              set_blocklimit:int64 ->
                              permission:(Pfs_rpcapi_aux.inodeinfo -> errno option) ->
                                unit Uq_engines.engine
  method inode_upd_time : id:int64 ->
                          mtime:Pfs_rpcapi_aux.time option ->
                          ctime:Pfs_rpcapi_aux.time option ->
                              unit
  method inode_set_anonymous : id:int64 -> unit
  method inode_set_used : int64 -> unit
  method allocinfo_get_e : id:int64 -> Nn_db.allocinfo list Uq_engines.engine
  method filename_get_e : dir_inode:int64 -> name:string -> 
                             int64 option Uq_engines.engine
  method filename_rev_get_e : inode:int64 ->
                                (int64 * string) list Uq_engines.engine
  method filename_rev_get_dir_e : inode:int64 ->
                                (int64 * string * (unit->unit)) Uq_engines.engine
  method filename_parent_dir_e : inode:int64 ->
                                   int64 option Uq_engines.engine
  method filename_count_e : inode:int64 ->
                              int Uq_engines.engine
  method filename_list_e : dir_inode:int64 ->
                               (string * int64) list Uq_engines.engine
  method filename_link_e : dir_inode:int64 -> name:string -> inode:int64 ->
                             unit Uq_engines.engine
  method filename_unlink_e : dir_inode:int64 -> name:string ->
                             owner_restr:string option ->
                               unit Uq_engines.engine
  method filename_rename_e : from_dir_inode:int64 -> from_name:string ->
                             to_dir_inode:int64 -> to_name:string ->
                             owner_restr:string option ->
                               unit Uq_engines.engine
  method delayed_inode_deletes : int64 list * int64 list
    
end

let encap_time t =
  let t1 = Int64.of_float t in
  let t2 = truncate ((t -. Int64.to_float t1) *. 1E9) in
  { A.tsecs = t1; A.tnsecs = t2 }


(**********************************************************************)
(* Shared state                                                       *)
(**********************************************************************)

let create_slave_mclient esys cauth timeout hostport =
  let (h,p) = Plasma_util.parse_host_port hostport in
  let addr = Plasma_util.sockaddr_of_host_port (h,p) in  (** May perform DNS lookup *)
  let conn = Plasma_util.connector_of_sockaddr addr in
  Nn_push.create_client conn cauth timeout esys
  

type sh_state =
    { mutable rev_t : Int64.t;
      mutable rev_cnt : int;
      mutable rev_ident : string;
      mutable datastore_max_id : int option;
      mutable inode_max_id : int64 option;
      mutable disabled_slaves : string list;
      mutable master_disabled : bool;
      mutable inode_lock_mutex : Netmcore_mutex.mutex;
      mutable filename_lock_mutex : Netmcore_mutex.mutex;
      mutable have_zombie_inodes : bool;
      mutable use_count_mutex : Netmcore_mutex.mutex;
      mutable counters : (counter_name * int ref) list;
    }


(* This record must be marshallable! *)
type sh_state_shm =
    { sss_pool : Netmcore.res_id;
      sss_sref_descr : sh_state Netmcore_ref.sref_descr;
      sss_inode_ser : Pfs_condition.ser Netmcore_ref.sref_descr;
      sss_bm_ser : Pfs_condition.int64_ser Netmcore_ref.sref_descr;
      sss_master_index : int;  (* immutable so far *)
      sss_inode_locks : (int64, owner, unit) Netmcore_hashtbl.t_descr;
      sss_owner_inodes : (owner, int64 list, unit) Netmcore_hashtbl.t_descr;
      sss_filename_locks : 
	(int64 * string, lock_owner, unit) Netmcore_hashtbl.t_descr;
      sss_owner_filenames : 
	(owner, (int64*string) list, unit) Netmcore_hashtbl.t_descr;
      sss_commit_monitor_heap :
	Nn_commit_monitor.commit_monitor_shm Netmcore_ref.sref_descr;
      sss_use_count_heap :
	(int64, int, unit) Netmcore_hashtbl.t_descr;
    }


let sh_state_shm pool master_index init_rev =
  let s0 = 
    { rev_t = 0L;
      rev_cnt = 0;
      rev_ident = init_rev;
      datastore_max_id = None;
      inode_max_id = None;
      disabled_slaves = [];
      master_disabled = false;
      inode_lock_mutex = Netmcore_mutex.dummy();
      filename_lock_mutex = Netmcore_mutex.dummy();
      have_zombie_inodes = false;
      use_count_mutex = Netmcore_mutex.dummy();
      counters = []
    } in
  let sref = Netmcore_ref.sref pool s0 in
  Netmcore_heap.modify
    (Netmcore_ref.heap sref)
    (fun mut ->
       let s = Netmcore_ref.deref_ro sref in
       s.inode_lock_mutex <- Netmcore_mutex.create mut `Normal;
       s.filename_lock_mutex <- Netmcore_mutex.create mut `Normal;
       s.use_count_mutex <- Netmcore_mutex.create mut `Normal;
    );
  let inode_ser = Pfs_condition.serializer_heap pool in
  let bm_ser = Pfs_condition.int64_serializer_heap pool 17 (* CONFIG *) in
  let inode_locks = Netmcore_hashtbl.create pool () in
  let owner_inodes = Netmcore_hashtbl.create pool () in
  let filename_locks = Netmcore_hashtbl.create pool () in
  let owner_filenames = Netmcore_hashtbl.create pool () in
  let commit_monitor_heap = Nn_commit_monitor.create_commit_monitor_heap pool in
  let use_count_heap = Netmcore_hashtbl.create pool () in
  { sss_pool = pool;
    sss_sref_descr = Netmcore_ref.descr_of_sref sref;
    sss_inode_ser = Netmcore_ref.descr_of_sref inode_ser;
    sss_bm_ser = Netmcore_ref.descr_of_sref bm_ser;
    sss_master_index = master_index;
    sss_inode_locks = Netmcore_hashtbl.descr_of_hashtbl inode_locks;
    sss_owner_inodes = Netmcore_hashtbl.descr_of_hashtbl owner_inodes;
    sss_filename_locks = Netmcore_hashtbl.descr_of_hashtbl filename_locks;
    sss_owner_filenames = Netmcore_hashtbl.descr_of_hashtbl owner_filenames;
    sss_commit_monitor_heap = Netmcore_ref.descr_of_sref commit_monitor_heap;
    sss_use_count_heap = Netmcore_hashtbl.descr_of_hashtbl use_count_heap;
  }


class shared_state esys (conf:nn_node_config) cauth
                   (sss : sh_state_shm)
                   : shared_state_t =
  let sref = 
    Netmcore_ref.sref_of_descr sss.sss_pool sss.sss_sref_descr in
  let inode_ser = 
    Netmcore_ref.sref_of_descr sss.sss_pool sss.sss_inode_ser in
  let inode_serializer =
    Pfs_condition.serializer inode_ser esys "inode" in
  let bm_ser = 
    Netmcore_ref.sref_of_descr sss.sss_pool sss.sss_bm_ser in
  let bm_serializer =
    Pfs_condition.int64_serializer bm_ser esys "blockmap" in
  let master_index =
    sss.sss_master_index in
  let master = 
    List.nth conf#nn_nodes master_index in
  let slaves = 
    List.rev
      (snd
	 (List.fold_left
	    (fun (i,l) hp ->
	       if i = master_index then
		 (i+1,l)
	       else
		 let slave = 
		   create_slave_mclient esys cauth conf#nn_node_timeout hp in
		 let node =
		   (hp, slave) in
		 (i+1,node::l)
	    ) 
	    (0,[]) 
	    conf#nn_nodes
	 )
      ) in
  let n_slaves = 
    List.length slaves in
  let inode_locks =
    Netmcore_hashtbl.hashtbl_of_descr sss.sss_pool sss.sss_inode_locks in
  let owner_inodes =
    Netmcore_hashtbl.hashtbl_of_descr sss.sss_pool sss.sss_owner_inodes in
  let filename_locks =
    Netmcore_hashtbl.hashtbl_of_descr sss.sss_pool sss.sss_filename_locks in
  let owner_filenames =
    Netmcore_hashtbl.hashtbl_of_descr sss.sss_pool sss.sss_owner_filenames in
  let commit_monitor_heap =
    Netmcore_ref.sref_of_descr sss.sss_pool sss.sss_commit_monitor_heap in
  let commit_monitor =
    Nn_commit_monitor.create_commit_monitor 
      sss.sss_pool commit_monitor_heap esys in 
  let use_count_heap =
    Netmcore_hashtbl.hashtbl_of_descr sss.sss_pool sss.sss_use_count_heap in
  let () =
    dlog "Nn_state.shared_state: pre-init done" in
    
object(self)
  val mutable db_trans = None

  method event_system = esys
  method node_config = conf
  method cur_rev_ident = 
    Netmcore_ref.deref_p sref (fun s -> String.copy s.rev_ident)

  method new_rev_ident() =
    (** Create unique identifiers. We take the time, and count the number
        of identifiers we already created per second. There is also a 
        random number part to avoid that other instances of the namenode
        can generate the same identifiers
     *)
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 let t = Int64.of_float (Unix.time()) in
	 if t = s.rev_t then (
	   assert(s.rev_cnt < 999_999_999);
	   s.rev_cnt <- s.rev_cnt+1;  (* an int! *)
	 )
	 else
	   s.rev_cnt <- 0;
	 s.rev_t <- Netmcore_heap.add mut t;
	 let r =
	   sprintf "%011Ld.%09d-%s"
	     t s.rev_cnt (Digest.to_hex (Plasma_rng.random_bytes 16)) in
	 r
      )

  method set_rev_ident r =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 s.rev_ident <- Netmcore_heap.add mut r
      )

  method datastore_max_id = 
    Netmcore_ref.deref_p sref 
      (fun s -> Netmcore_heap.copy s.datastore_max_id)

  method datastore_use_id id =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 let overwrite =
	   match s.datastore_max_id with
	     | None -> true
	     | Some m -> id > m in
	 if overwrite then
	   s.datastore_max_id <- Netmcore_heap.add mut (Some id)
      )

  method inode_max_id = 
    Netmcore_ref.deref_p sref 
      (fun s -> Netmcore_heap.copy s.inode_max_id)

  method inode_use_id id =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 let overwrite =
	   match s.inode_max_id with
	     | None -> true
	     | Some m -> id > m in
	 if overwrite then
	   s.inode_max_id <- Netmcore_heap.add mut (Some id)
      )

  method inode_get_new_id_serialized :
     (Unixqueue.event_system -> int64 Uq_engines.engine) ->
     int64 Uq_engines.engine =
    fun f ->
      inode_serializer # serialized f

  method commit_monitor = commit_monitor

  method master = master

  method slaves = 
    Netmcore_ref.deref_p sref 
      (fun s ->
	 List.map
	   (fun (hp, client) ->
	      let disabled = List.mem hp s.disabled_slaves in
	      (hp, client, not disabled)
	   )
	   slaves
      )

  method disable_slave hp =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 if not (List.mem hp s.disabled_slaves) then
	   s.disabled_slaves <- 
	     Netmcore_heap.add mut (hp :: s.disabled_slaves)
      )

  method is_slave_disabled hp =
    Netmcore_ref.deref_p sref 
      (fun s ->
	 List.mem hp s.disabled_slaves
      )

  method minimum_slaves = conf#nn_node_alive_min - 1

  method enough_slaves =
    Netmcore_ref.deref_p sref 
      (fun s ->
	 let dead_count = List.length s.disabled_slaves in
	 let alive_count = n_slaves - dead_count in
	 alive_count >= self # minimum_slaves
      )

  method master_enabled =
    Netmcore_ref.deref_p sref 
      (fun s ->
	 not s.master_disabled
      )

  method disable_master() =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 s.master_disabled <- true
      )

  val blockmap_client = Nn_blockmap_box.client()
  val blockmaps = Hashtbl.create 19 
(*  val blockmap_caches = Hashtbl.create 19  *)

(*
  method exists_blockmap id =
    Hashtbl.mem blockmaps id

  method create_blockmap bm =
    dlogf
      "Creating blockmap for %s"
      bm#identity;
    Hashtbl.replace blockmaps bm#identity bm
 *)

  method blockmaps =
    Hashtbl.fold (fun _ bm l -> bm::l) blockmaps []

  method get_blockmap identity =
    Hashtbl.find blockmaps identity

  method blockmap_get_e ~identity =
    try
      let bm = Hashtbl.find blockmaps identity in
      new Uq_engines.epsilon_engine (`Done bm) esys
    with
      | Not_found ->
	  dlogr
	    (fun () ->
	       sprintf
		 "Missing blockmap for %s" identity);
	  let ds = Nn_datastore_news.get ~wait:true identity in
	  let id = ds.Nn_datastores.id in
	  let size = ds.Nn_datastores.size in
	  dlogr (fun () ->
		   sprintf "Accessing blockmap for %s" identity);
	  let (create_flag, bm) = 
	    blockmap_client # access ~id ~identity ~size in
	  dlogr (fun () ->
		   sprintf "Done accessing blockmap for %s - create_flag=%B"
		     identity create_flag);
	  if create_flag then (
	    (* FIXME: this should also be asynchronous! Maybe some pipe can
	       be used for notification
	     *)
	    Nn_blockmap.fill_blockmap bm
	  );
	  if not bm#filled then (
	    (* There could be a competing request for the blockmap, which is
	       right now filling it. In this case we have create_flag=false,
	       but the blockmap is not yet ready
	     *)
	    dlogr (fun () ->
		     sprintf "Blockmap not ready (busy waiting): %s" identity);
	    (* busy waiting... *)
	    Uq_engines.delay_engine 1.0
	      (fun () -> self#blockmap_get_e ~identity)
	      esys
	  ) else (
	    Hashtbl.replace blockmaps identity bm;
	    eps_e (`Done bm) esys
	  )

(*
	  ( try
	      let bm_cache = Hashtbl.find blockmap_caches identity in
	      bm_cache#get_engine()
	    with
	      | Not_found ->
		  dlogf
		    "Missing cache for %s - creating cache and loading"
		    identity;
		  let bm_cache =
		    Uq_engines.cache
		      (fun esys ->
			 let ds = Nn_datastore_news.get ~wait:true identity in
			 let id = ds.Nn_datastores.id in
			 let size = ds.Nn_datastores.size in
			 Nn_db.with_ro_trans
			   (Nn_blockmap.blockmap_from_db_e
			      ~id ~identity ~size)
			 ++ (fun v ->
			       self # create_blockmap v;
			       eps_e (`Done v) esys
			    )
		      )
		      esys in
		  Hashtbl.add blockmap_caches identity bm_cache;
		  bm_cache#get_engine()
	  )
 *)

  method blockmap_serialized :
     int64 list -> (Unixqueue.event_system -> unit Uq_engines.engine) ->
     unit Uq_engines.engine =
    fun l f ->
      bm_serializer # multi_serialized l f

(*
  method blockmap_prioritizer =
    Uq_engines.prioritizer esys
 *)

  method forget_identity id =
    (** Deleting/disabling datastores is a fragile operation. Remember that we
        might have handed out blockmap objects, or that the blockmap
        might be currently being loaded.
     *)
    ( try
	let bm = Hashtbl.find blockmaps id in
	bm # inactivate();
	Hashtbl.remove blockmaps id
      with
	| Not_found -> ()
    )
(*
    ( try
	let bm_cache = Hashtbl.find blockmap_caches id in
	bm_cache # abort();
	Hashtbl.remove blockmap_caches id
      with
	| Not_found -> ()
    )
 *)

      (* important: to avoid deadlocks, first modify inode_locks, then
	 owner_inodes
       *)

  method lock_inode id owner =
    let own_inode() =
      let l = 
	try Netmcore_hashtbl.find_c owner_inodes owner
	with Not_found -> [] in
      let l' =
	id :: l in
      Netmcore_hashtbl.replace owner_inodes owner l' in

    let disown_inode() =
      try
	let l = 
	  Netmcore_hashtbl.find_c owner_inodes owner in
	let l' = List.filter (fun i -> id <> i) l in
	if l' = [] then
	  Netmcore_hashtbl.remove owner_inodes owner
	else
	  Netmcore_hashtbl.replace owner_inodes owner l'
      with Not_found -> () in

    let free_inode() =
      Netmcore_ref.deref_p sref
	(fun s -> Netmcore_mutex.lock s.inode_lock_mutex);
      Netmcore_hashtbl.remove inode_locks id;
      disown_inode();
      Netmcore_ref.deref_p sref
	(fun s -> Netmcore_mutex.unlock s.inode_lock_mutex)
    in

    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.lock s.inode_lock_mutex);
    let r =
      try
	let o = Netmcore_hashtbl.find_c inode_locks id in
	if o = owner then Some (fun () -> ()) else None
      with
	| Not_found ->
	    Netmcore_hashtbl.replace inode_locks id owner;
	    own_inode();
	    Some free_inode in
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.unlock s.inode_lock_mutex);
    r

  method unlock_inodes owner =
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.lock s.inode_lock_mutex);
    let inodes =
      try Netmcore_hashtbl.find_c owner_inodes owner
      with Not_found -> [] in
    Netmcore_hashtbl.remove owner_inodes owner;
    List.iter
      (fun id ->
	 try
	   let o = Netmcore_hashtbl.find_c inode_locks id in
	   assert(o=owner);
	   Netmcore_hashtbl.remove inode_locks id
	 with
	   | Not_found -> assert false
      )
      inodes;
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.unlock s.inode_lock_mutex);


  method lock_filename (dir_inode,fn) typ (owner : owner ) =
    let spot = (dir_inode, fn) in
    (* A location in the filename tree is identified by [spot], i.e. the
       name [fn] in the directory referenced by [dir_inode].
     *)

    let own_filename() =
      let l = 
	try Netmcore_hashtbl.find_c owner_filenames owner
	with Not_found -> [] in
      if not (List.mem spot l) then
	Netmcore_hashtbl.replace owner_filenames owner (spot :: l) in

    let free_filename() =
      try
	let l = 
	  Netmcore_hashtbl.find_c owner_filenames owner in
	let l' = List.filter (fun n -> spot <> n) l in
	if l' = [] then
	  Netmcore_hashtbl.remove owner_filenames owner
	else
	  Netmcore_hashtbl.replace owner_filenames owner l'
      with Not_found -> () in

    let restore_lock old_lock_opt =
      match old_lock_opt with
	| None ->
	    free_filename();
	    Netmcore_hashtbl.remove filename_locks spot
	| Some old ->
	    Netmcore_hashtbl.replace filename_locks spot old in

    let unlock old_lock_opt () =
      (** First remove [owner] from the list of lock owners. If this results
          in an empty list, we can go back to the previous state
          ([old_lock_opt]).

          [unlock] is only designed to undo the last locking request, for the
          case that the operation fails.
       *)
      Netmcore_ref.deref_p sref
	(fun s -> Netmcore_mutex.lock s.filename_lock_mutex);
      ( try
	  match Netmcore_hashtbl.find_c filename_locks spot with
	    | `Link o ->
		assert(o = owner);
		restore_lock old_lock_opt
	    | `Unlink o ->
		assert(o = owner);
		restore_lock old_lock_opt
	    | `Exists l ->
		assert(List.mem owner l);
		let l' = List.filter (fun o -> o <> owner) l in
		if l' = [] then
		  restore_lock old_lock_opt
		else (
		  free_filename();
		  Netmcore_hashtbl.replace filename_locks spot (`Exists l')
		)
	with
	  | Not_found ->
	      assert false
      );
      Netmcore_ref.deref_p sref
	(fun s -> Netmcore_mutex.unlock s.filename_lock_mutex) in

    let set_lock old_lock_opt new_lock =
      Netmcore_hashtbl.replace filename_locks spot new_lock;
      own_filename();
      Some (unlock old_lock_opt)
    in

    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.lock s.filename_lock_mutex);
    let r =
      try
	let old_lock = Netmcore_hashtbl.find_c filename_locks spot in
	match old_lock with
	  | `Link o when typ = `Link || typ = `Exists ->
	      if o <> owner then failwith "lock_filename: conflict";
	      (** If we own [`Link] we are allowed to do everything with the
                  file. So we accept [`Exists] but do not
                  downgrade (which would allow competing [link] operations
                  with uncertain semantics).
	       *)
	      Some (fun () -> ())
	  | `Link o when typ = `Unlink ->
	      if o <> owner then failwith "lock_filename: conflict";
	      (** change link->unlink *)
	      set_lock (Some old_lock) (`Unlink owner)
	  | `Link _ -> assert false
	  | `Unlink o when typ = `Link ->
	      if o <> owner then failwith "lock_filename: conflict";
	      (** change unlink->link *)
	      set_lock (Some old_lock) (`Link owner)
	  | `Unlink o when typ = `Unlink ->
	      if o <> owner then failwith "lock_filename: conflict";
	      Some (fun () -> ())
	  | `Unlink o when typ = `Exists ->
	      (* change unlink->exists. This should not occur, but would be 
		 harmless
	       *)
	      if o <> owner then failwith "lock_filename: conflict";
	      Some (fun () -> ())
	  | `Unlink _ -> assert false
	  | `Exists [o] when typ = `Unlink ->
	      (** change exists->unlink (normal when we delete files). *)
	      if o <> owner then failwith "lock_filename: conflict";
	      set_lock (Some old_lock) (`Unlink owner)
	  | `Exists l ->
	      (** A change from `Exists to `Link is nonsense. The latter
                  guarentees exclusive creation of files.
	       *)
	      if typ <> `Exists then failwith "lock_filename: conflict";
	      let l' =
		if not(List.mem owner l) then owner::l else l in
	    set_lock None (`Exists l')
      with
	| Not_found ->
	  (** If not found we always grant the lock. *)
	    let new_lock =
	      match typ with
		| `Link -> `Link owner
		| `Unlink -> `Unlink owner
		| `Exists -> `Exists [owner] in
	    set_lock None new_lock
	| Failure _ ->
	    None in
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.unlock s.filename_lock_mutex);
    r


  method unlock_filenames owner =
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.lock s.filename_lock_mutex);
    let locks =
      try Netmcore_hashtbl.find_c owner_filenames owner
      with Not_found -> [] in
    Netmcore_hashtbl.remove owner_filenames owner;
    List.iter
      (fun spot ->
	 try
	   match Netmcore_hashtbl.find_c filename_locks spot with
	     | `Link o ->
		 assert(o = owner);
		 Netmcore_hashtbl.remove filename_locks spot
	     | `Unlink o ->
		 assert(o = owner);
		 Netmcore_hashtbl.remove filename_locks spot
	     | `Exists l ->
		 assert(List.mem owner l);
		 let l' = 
		   List.filter (fun o -> o <> owner) l in
		 if l' = [] then
		   Netmcore_hashtbl.remove filename_locks spot
		 else
		   Netmcore_hashtbl.replace filename_locks spot (`Exists l')
	 with
	   | Not_found ->
	       assert false
      )
      locks;
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.unlock s.filename_lock_mutex);

  method inode_modify_use_counts ~transactions inode =
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.lock s.use_count_mutex);
    try
      let n =
	try Netmcore_hashtbl.find_c use_count_heap inode
	with Not_found -> 0 in
      let n' =
	transactions n in
      if n' = 0 then
	Netmcore_hashtbl.remove use_count_heap inode
      else
	Netmcore_hashtbl.replace use_count_heap inode n';
      Netmcore_ref.deref_p sref
	(fun s -> Netmcore_mutex.unlock s.use_count_mutex)
    with
      | error ->
	  Netmcore_ref.deref_p sref
	    (fun s -> Netmcore_mutex.unlock s.use_count_mutex);
	  raise error

  method inode_use_counts inode =
    Netmcore_ref.deref_p sref
      (fun s -> Netmcore_mutex.lock s.use_count_mutex);
    try
      let n = Netmcore_hashtbl.find_c use_count_heap inode in
      Netmcore_ref.deref_p sref
	(fun s -> Netmcore_mutex.unlock s.use_count_mutex);
      n
    with
      | error ->
	  Netmcore_ref.deref_p sref
	    (fun s -> Netmcore_mutex.unlock s.use_count_mutex);
	  raise error

  method zombie_inodes_exist =
    Netmcore_ref.deref_p sref
      (fun s -> s.have_zombie_inodes)

  method set_zombie_inodes flag =
    Netmcore_ref.deref_p sref
      (fun s -> s.have_zombie_inodes <- flag
      )

    

  method cauth = cauth

  method db_trans_for_commit() =
    ( match db_trans with
	| None -> ()
	| Some t -> if t#is_returned then db_trans <- None
    );
    ( match db_trans with
	| None ->
	    (* FIXME: pass the commit_id in a better way *)
	    let t = Nn_db.transact esys (Unix.getpid()) in
	    db_trans <- Some t;
	    t
	| Some t ->
	    t
    )

  method incr_count name =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 let val_ref =
	   try 
	     List.assoc name s.counters 
	   with Not_found -> 
	     let r = ref 0 in
	     s.counters <- Netmcore_heap.add mut ((name,r) :: s.counters);
	     r in
	 incr val_ref
      )


  method reset_count name =
    Netmcore_heap.modify
      (Netmcore_ref.heap sref)
      (fun mut ->
	 let s = Netmcore_ref.deref_ro sref in
	 try 
	   let val_ref = List.assoc name s.counters in
	   let old_val = !val_ref in
	   val_ref := 0;
	   old_val
	 with Not_found -> 
	   0
      )


end

(**********************************************************************)
(* Revision view                                                      *)
(**********************************************************************)

class revclear_view : view_t =
  let journal = Queue.create() in
  let () = Queue.add `Revision_clear journal in
object
  method pre_commit = journal
  method pre_commit_mon = Queue.create()
  method post_commit _ = ()
  method rollback() = ()
end



(**********************************************************************)
(* Datastore view                                                     *)
(**********************************************************************)


(** The datastore view always loads the datastore table anew from db
    when it is established. This view is only needed for doing
    certain modifications of the datastore table. No need to implement
    a cache.

    However, when we disable a datastore (or even delete it), we have
    to propagate this to the various caches.
 *)

class ds_view (shared_state:shared_state_t) 
           : ds_view_t =
  let () = (
    if not (shared_state#master_enabled) then
      failwith "Nn_state: The master db is out of sync. No access permitted"
  ) in
  let esys = Nn_db.ro_esys() in
  let datastore_cache =  (** Cached [datastore] table *)
    Uq_engines.cache
      (fun esys ->
	 Nn_db.datastore_list_ro_e()
	 ++ (fun l ->
	       (* Update [shared_state] on the fly *)
	       shared_state#datastore_use_id 0;
	       List.iter
		 (fun ds -> 
		    shared_state#datastore_use_id ds.Nn_datastores.id)
		 l;
	       eps_e (`Done l) esys
	    )
      )
      esys in
  let journal = Queue.create() in     (** Queue of updates *)
  let to_forget = Queue.create() in   (** Identities to fortget after commit *)
object(self)

  method datastore_list_e() =
    datastore_cache # get_engine()

  method datastore_new_id_e() =
    (** Check whether [shared_state] already knows the maximum ID. If so, just
        increase by 1. Otherwise, get from [datastore_cache] once which will
        update the maximum ID
     *)
    match shared_state # datastore_max_id with
      | None ->
	  let e = datastore_cache # get_engine() in
	  new Uq_engines.map_engine
	    ~map_done:(fun _ -> 
			 (** Now we can safely assume that max ID is known *)
			 match shared_state # datastore_max_id with
			   | None ->
			       assert false
			   | Some m ->
			       let id = m+1 in
			       shared_state # datastore_use_id id;
			       `Done id
		      )
	    e
      | Some m ->
	  (** max is m *)
	  let id = m+1 in
	  shared_state # datastore_use_id id;
	  new Uq_engines.epsilon_engine (`Done id) esys

  method datastore_upd_e ~id ~identity ~size ~enabled =
    (** First ensure we know the newest value. Then update the value,
        and record the update in our journal.
     *)
    new Uq_engines.map_engine
      ~map_done:(fun l ->
		   try
		     let have_id =
		       List.exists
			 (fun ds -> ds.Nn_datastores.id = id) l in
		     if have_id then (
		       (* it's an update *)
		       let ds =
			 List.find
			   (fun ds -> ds.Nn_datastores.id = id) l in
		       if ds.Nn_datastores.identity <> identity then
			 failwith "Nn_state.datastore_upd_e: cannot change \
                                   identity";
		       if ds.Nn_datastores.size > size then
			 failwith "Nn_state.datastore_upd_e: cannot shrink \
                                   datastore size";
		       let ds' =
			 { ds with
			     Nn_datastores.size = size;
			     enabled = enabled
			 } in
		       let l' =
			 List.map
			   (fun ds ->
			      if ds.Nn_datastores.id = id then ds' else ds)
			   l in
		       datastore_cache # put l';
		       if not enabled && ds.Nn_datastores.enabled then
			 Queue.push identity to_forget
		     )
		     else (
		       (* it's an addition *)
		       let have_identity =
			 List.exists
			   (fun ds -> ds.Nn_datastores.identity = identity) 
			   l in
		       if have_identity then
			 failwith "Nn_state.datastore_upd_e: not unique";
		       let ds =
			 { Nn_datastores.id = id;
			   identity = identity;
			   size = size;
			   enabled = enabled;
			   node = None;
			   alive = false;
			 } in
		       let l' = ds :: l in
		       datastore_cache # put l'
		     );
		     (* Finally remember the update so we can push it to
                        the database and the slaves later
		      *)
		     let je =
		       `Datastore_upd(id,identity,size,enabled) in
		     Queue.push je journal;
		     `Done()
		   with
		     | error ->
			 `Error error
		)
      (datastore_cache # get_engine())

  method datastore_del_e ~id  =
    (** First ensure we know the newest value. Then record the delete in
        our journal.
     *)
    new Uq_engines.map_engine
      ~map_done:(fun l ->
		   let have_id =
		     List.exists
		       (fun ds -> ds.Nn_datastores.id = id) l in
		   if have_id then (
		     let ds =
		       List.find
			 (fun ds -> ds.Nn_datastores.id = id) l in
		     Queue.push (`Datastore_del id) journal;
		     Queue.push ds.Nn_datastores.identity to_forget;
		     `Done ()
		   )
		   else
		     `Error (Transaction_error `enonode)
		)
      (datastore_cache # get_engine())

  method admin_table_put_e ~key ~contents =
    Nn_db.admin_table_exists_ro_e ~key
    ++ (fun ok ->
	  if ok then (
	    Queue.push (`Admin_table_put(key,contents)) journal;
	    eps_e (`Done ()) esys
	  )
	  else
	    eps_e (`Error (Transaction_error `enoent)) esys
       )

  method pre_commit =
    journal

  method pre_commit_mon =
    Queue.create()

  method post_commit ok =
    if ok then (
      (** Call [forget_identity] for all disabled or deleted datastores.
          This is important to ensure that at enable time the caches
          referring to this datastore are loaded from db
       *)
      Queue.iter
	(fun identity ->
	   shared_state # forget_identity identity
	)
	to_forget;
    )

  method rollback() =
    ()
end


(**********************************************************************)
(* Blockmap view                                                      *)
(**********************************************************************)

(** The blockmap view maintains a shared cache (in [shared_state_t]).
 *)

class bm_view (shared_state:shared_state_t)
           : bm_view_t =
  let () = (
    if not (shared_state#master_enabled) then
      failwith "Nn_state: The master db is out of sync. No access permitted"
  ) in
  let esys = Nn_db.ro_esys() in
  let bm_owner = Nn_blockmap.create_owner() in
object(self)

  method blockmap_get_e ~identity =
    shared_state # blockmap_get_e ~identity

  method private blockmap_mget_e identities =
    (** Start an engine getting the blockmaps for [identities]. If
        this list is empty, get the blockmaps for all enabled datastores.
	When the engine is done, the blockmaps can be retrieved directly
        via [shared_state#blockmaps].

        This method fails if there is an invalid identity, or an
        identity is destroyed while loading. Failure results in an
        engine transitioning to an [`Error] or [`Aborted] state.
     *)
    let identities' =
      if identities = [] then
	let ds_list =
	  Nn_datastore_news.list ~wait:true () in
	List.map
	  (fun ds -> ds.Nn_datastores.identity)
	  (List.filter
	     (fun ds -> ds.Nn_datastores.enabled)
	      ds_list)
      else
	identities in
    dlogr
      (fun () ->
	 sprintf "blockmap_mget_e: loading datastores: %s"
	   (String.concat "," identities'));
    Plasma_util.msync_engine 
      (List.map
	 (fun identity -> 
	    new Uq_engines.map_engine
	      ~map_done:(fun _ -> `Done())
	      (self # blockmap_get_e ~identity))
	 identities'
      )
      esys

  method blockmap_alloc_e ~inode ~rpolicy ~index ~number ~pref td ri =
    let `Balanced repl = rpolicy in
    new Uq_engines.map_engine
      ~map_done:(fun () ->
		   try
		     let blockmaps = shared_state#blockmaps in
		     let td_blockmaps =
		       List.filter
			 (fun bm -> List.mem bm#id td.td_idlist)
			 blockmaps in
		     let blocksize = shared_state#node_config#dn_blocksize in
		     let blocks =
		       try
			 Nn_alloc_balanced.allocate_balanced_strategy
			   ~inode
			   td_blockmaps td bm_owner repl index number blocksize 
			   pref ri
		       with
			 | No_space -> raise (Transaction_error `enospc) in
		     `Done blocks
		   with
		     | error ->
			 `Error error
		)
      (self#blockmap_mget_e [])

  method blockmap_allocsize =
    let blocksize = shared_state#node_config#dn_blocksize in
    Nn_alloc_balanced.get_allocsize blocksize

  method blockmap_dealloc_e ~inode blocks =
    (** First get all blockmaps we need: *)
    let ids = ref StrSet.empty in
    List.iter
      (fun (id,_,_) -> ids := StrSet.add id !ids)
      blocks;
    let ids_l = StrSet.fold (fun id acc -> id::acc) !ids [] in
    self#blockmap_mget_e
      ids_l
    >> (fun st ->
	  ( match st with
	      | `Done () ->
		  dlogr
		    (fun () ->
		       sprintf
			 "bm free(inode=%Ld,owner=%s)" inode
			 (Nn_blockmap.string_of_owner bm_owner));
		  List.iter
		    (fun (id,block,number) ->
		       let bm = shared_state#get_blockmap id in
		       bm#free [block,number] bm_owner;
		    )
		    blocks
	      | _ -> ()
	  );
	  st
       )

  method blockmap_load_e () =
    (** First get all known blockmap identities: *)
    let ids = ref StrSet.empty in
    List.iter
      (fun ds -> ids := StrSet.add ds.Nn_datastores.identity !ids)
      (Nn_datastore_news.list ~wait:true ());
    let ids_l = StrSet.fold (fun id acc -> id::acc) !ids [] in
    self#blockmap_mget_e
      ids_l

  method blockmap_pin ~inode blocks =
    dlogr
      (fun () ->
	 sprintf
	   "bm pin(inode=%Ld,owner=%s)" inode
	   (Nn_blockmap.string_of_owner bm_owner));
    List.iter
      (fun (id,block,number) ->
	 try
	   let bm = shared_state#get_blockmap id in
	   bm#pin [block,number] bm_owner
	 with
	   | Not_found ->
	       failwith "blockmap_pin: blockmap not loaded"
      )
      blocks


  method pre_commit =
    Queue.create()

  method pre_commit_mon =
    let journal = Queue.create() in
    (** Check all blockmaps, and extract db changes *)
    List.iter
      (fun bm ->
	 let id = bm # id in
	 let l = bm#get_changes bm_owner in
	 List.iter
	   (fun (index,data) ->
	      Queue.push (`Blockalloc_upd(id,index,data)) journal
	   )
	   l
	 (** caveat: [get_changes] must be called from the serialized
	     code section implementing the commit protocol. Otherwise
	     it is not guarenteed that [get_changes] includes the
	     changes of the still running previous commit, and the db image
	     of the blockmap gets out of sync with the RAM representation.
	  *)
      )
      shared_state#blockmaps;
    journal


  method post_commit ok =
    if ok then (
      (** Make changes permanent:
	  (Note that this [commit] is the one that must not overlap
	  with [inode_get_blocks_e].)
       *)
      List.iter
	(fun bm -> bm#commit bm_owner)
	shared_state#blockmaps;
    )
    else (
      (** Unreserve all blocks: *)
      List.iter
	(fun bm -> bm#rollback bm_owner)
	shared_state#blockmaps;
    )


  method rollback () =
    self#post_commit false

  method release() =
      List.iter
	(fun bm -> bm#release bm_owner)
	shared_state#blockmaps;
  

end


(**********************************************************************)
(* Inode view                                                         *)
(**********************************************************************)

(* new type [inode_detail]:

  idea:
   - i_blocks_dom defines the range of indexes for which we have 
     authoritative information (and need not/must not look into the db)
   - i_blocks contains this information. If a block is included in this
     list the block exists. If it is not included, it is deleted.

  inode_get_blocks:
   - Split the passed range (blkidx,len) into the subset that
     intersects with i_blocks_dom, and the other subset that does not
     intersect
   - The first subset is responded from i_blocks: just run a "sub"
     operation
   - The second subset: get this data from the db
   - finally merge all

 *)


type inode_detail =
    { i_info : Pfs_rpcapi_aux.inodeinfo;
      i_blocks_dom : Plasma_util.I64RM.t;
      i_blocks : Nn_blocklist.blocklist;
      i_ai :  Nn_db.allocinfo list option; (* None: not yet updated *)
    }

type inode =
    { i_id : int64;
      i_detail : inode_detail option; (* None: deleted *)
    }

let create_detail new_ii =
  { i_info = new_ii;
    i_blocks_dom = Plasma_util.I64RM.empty;
    i_blocks = Plasma_util.StrMap.empty;
    i_ai = None;
  }


class inode_view (shared_state:shared_state_t) 
           : inode_view_t =
  let () = (
    if not (shared_state#master_enabled) then
      failwith "Nn_state: The master db is out of sync. No access permitted"
  ) in
  let esys = Nn_db.ro_esys() in
  let journal = Queue.create() in
  let self_owner = create_owner() in
object(self)
  val inodes = Hashtbl.create 7
    (** Cached updates of this transaction *)

  val inode_links = Hashtbl.create 7
    (** This number [n] means that we know about at least [n] links for the
	inode. If [n=0] we have to check at commit time whether the inode
	needs to be deleted. Enter here only something if we also have a
	lock for the inode.
     *)

  val inode_used = Hashtbl.create 7
    (** For which inodes we increased the use counter *)

  method as_owner = self_owner

  method private use_inode inode =
    if not (Hashtbl.mem inode_used inode) then (
      Hashtbl.add inode_used inode ();
      shared_state # inode_modify_use_counts
	~transactions:(fun n -> n+1)
	inode
    )

  method private modify_link_number inode f =
    self#use_inode inode;
    let n = try Hashtbl.find inode_links inode with Not_found -> 0 in
    let n' = f n in
    Hashtbl.replace inode_links inode n'
    
  method inode_get_e ~id =
    try
      let i = Hashtbl.find inodes id in
      self # use_inode id;
      let r =
	match i.i_detail with
	  | None -> None
	  | Some d -> Some d.i_info in
      new Uq_engines.epsilon_engine (`Done r) esys
    with
      | Not_found ->
	  (* Look now in the shm inodecache: *)
	  try
	    let ii = Nn_inodecache.lookup id in
	    if ii.A.anonymous && not(Hashtbl.mem inode_used id) then raise Exit;
	    self # use_inode id;
	    eps_e (`Done (Some ii)) esys
	  with
	    | Not_found ->
		(* Get it from the db *)
		Nn_db.inode_get_ro_e ~id
		>> (function
		      | `Done None ->
			  Nn_inodecache.delete id; (* should not be necessary *)
			  `Done None
		      | `Done (Some ii) ->
			  if ii.A.anonymous && not(Hashtbl.mem inode_used id)
			  then `Done None
			  else (
			    self # use_inode id;
			    Nn_inodecache.update id ii;
			    `Done (Some ii)
			  )
		      | st -> st
		   )
	    | Exit ->
		raise Not_found

  method inode_get_with_lock_e ~id =
    let unlock_opt =
      shared_state # lock_inode id self_owner in
    match unlock_opt with
      | None ->
	  new Uq_engines.epsilon_engine
	    (`Error (Transaction_error `econflict)) esys
      | Some unlock ->
	  self # inode_get_e ~id

  method inode_get_blocks_e ~id ~blkidx ~len ~pin_flag bmv =
    (** One difficulty here is to immediately set the pin_flag in the
	block map - before a competing transaction can commit. Because of
	this we first load all blockmaps
     *)
    dlogr
      (fun () ->
	 sprintf "Nn_state.inode_get_blocks: inode=%Ld idx=%Ld len=%Ld pin=%B"
	   id blkidx len pin_flag
      );
    self # use_inode id;
    if len = 0L then
      eps_e (`Done Nn_blocklist.empty) esys
    else (
      let blkidx_max = Int64.pred (Int64.add blkidx len) in
      dlog "Nn_state.inode_get_blocks_e: loading blockmap";
      bmv # blockmap_load_e ()
      ++ (fun () ->
	    dlog "Nn_state.inode_get_blocks_e: blockmap loaded";
	    try
	      let i = Hashtbl.find inodes id in
	      (** There are uncommitted updates for this inode. Check these. *)
	      match i.i_detail with
		| None ->
		    (** inode is deleted *)
		    new Uq_engines.epsilon_engine (`Done Nn_blocklist.empty) esys
		| Some d ->
		    self # inode_get_blocks_upd_e
		      ~id ~blkidx ~len ~pin_flag bmv i d
	    with
	      | Not_found ->
		  (** Get everything from the DB *)
		  let q =
		    Plasma_util.I64RM.add
		      (blkidx, blkidx_max) 
		      (blkidx, blkidx_max) 
		      Plasma_util.I64RM.empty in
		  self # inode_get_blocks_from_db_and_pin_e 
		    ~id ~pin_flag bmv q
	 )    
    )

  method private inode_get_blocks_upd_e ~id ~blkidx ~len ~pin_flag bmv i d =
    (** Case: we have uncommitted updates in [d] *)
    (** The query range is [(blkidx,blkidx+len-1)]. We are now
	looking for the intersection of the query range with
	[i_blocks_dom]. For this intersection of blocks we respond
	from [i_blocks] (as [q_blocks]), and for the other blocks [db_dom]
	we have to retrieve the information from the database.
     *)
    let blkidx_max = Int64.pred (Int64.add blkidx len) in
    let q =
      Plasma_util.I64RM.add 
	(blkidx, blkidx_max) (blkidx, blkidx_max) Plasma_util.I64RM.empty in
    let q_blocks =
      Nn_blocklist.sub (blkidx, blkidx_max) d.i_blocks in
    dlogr
      (fun () ->
	 sprintf "Nn_state.inode_get_blocks_upd_e: q_blocks=%s"
	   (Nn_blocklist.to_string q_blocks));
    let db_dom =
      Plasma_util.I64RM.fold
	(fun k _ acc -> Plasma_util.I64RM.remove k acc)
	d.i_blocks_dom
	q in
    (** Before starting another engine for getting DB data set the pin
	flag for [q_blocks]. (We do not undo this in case of an error -
	all errors here are fatal anyway.)
     *)
    if pin_flag then
      self # inode_pin_blocks id q_blocks bmv;
    self # inode_get_blocks_from_db_and_pin_e ~id ~pin_flag bmv db_dom 
    ++ (fun db_blocks ->
	  let all_blocks = Nn_blocklist.merge q_blocks db_blocks in
	  dlogr
	    (fun () ->
	       sprintf "Nn_state.inode_get_blocks_upd_e: all_blocks=%s"
		 (Nn_blocklist.to_string all_blocks)
	    );
	  (** As we have uncommitted updates, fix the
	      seqno contained in the blocklist. We
	      set it generally to the latest seqno.
	      Also, we clear [inode_committed] for all
	      blocks (even for those from the db).
	      The API promises that the [inode_seqno] and
	      [inode_committed] fields are identical
	      in all blocklist elements.
	   *)
	  let useqno = d.i_info.Pfs_rpcapi_aux.seqno in
	  let all_blocks' =
	    Nn_blocklist.map
	      (fun bi ->
		 { bi with
		     Pfs_rpcapi_aux.inode_seqno = useqno;
		     inode_committed = false
		 }
	      )
	      all_blocks in
	  eps_e (`Done all_blocks') esys
       )

  method private inode_get_blocks_from_db_and_pin_e ~id ~pin_flag bmv dom
         : Nn_blocklist.blocklist Uq_engines.engine =
    (** Do the db lookup, optionally followed by postprocessing.
	The db lookup and the following [pin] call must not
	overlap with a blockmap commit. Because of this it is
	run via this serializer. The serializer is also
	used during the last steps of the commit, i.e. for the
	phase 2 of the commit and the post commit hook.

	(E.g.: A competing transaction is freeing the same blocks we
	retrieve here. If the free operation is being committed while we
	are reading and pinning the blocks, it becomes possible that
	we read at the moment the blocks are not yet freed, but when
	we want to pin them, the free op is done, resulting in an
	invalid pin operation.)
	
     *)
    let r = ref Nn_blocklist.empty in
    dlog "Nn_state.inode_get_blocks_from_db_and_pin_e: waiting for serializer";
    shared_state # blockmap_serialized
      [id]
      (fun _ ->
	 dlog "Nn_state.inode_get_blocks_from_db_and_pin_e: serial block";
	 let list =
	   Plasma_util.I64RM.fold 
	     (fun k _ acc -> k :: acc) dom [] in
	 dlogr
	   (fun () ->
	      sprintf "Nn_state.inode_get_blocks_from_db_and_pin_e: dom=%s"
		(String.concat ","
		   (List.map (fun (min,max) -> sprintf "%Ld-%Ld" min max) list)
		));
	 let getters =
	   List.map
	     (fun (min,max) ->
		let len = Int64.succ (Int64.sub max min) in
		Nn_db.inodeblocks_get_ro_e ~inode:id ~blkidx:min ~len 
	     )
	     list in
	 (* CHECK: maybe it is better to run the [getters] engines
	    in a serialized way, one after the other
	  *)
	 Uq_engines.msync_engine
	   getters
	   (fun blist acc -> Nn_blocklist.merge blist acc)
	   Nn_blocklist.empty
	   esys
	 ++ (fun blist ->
	       dlogr
		 (fun () ->
		    sprintf 
		      "Nn_state.inode_get_blocks_from_db_and_pin_e: blist=%s"
		      (Nn_blocklist.to_string blist));
	       if pin_flag then
		 self # inode_pin_blocks id blist bmv;
	       r := blist;
	       eps_e (`Done ()) esys
	    )
      )
    ++ (fun () -> eps_e (`Done !r) esys)


  method private inode_pin_blocks inode blocklist bmv =
    Nn_blocklist.iter
      (fun identity _ bi ->
	 let b = bi.Pfs_rpcapi_aux.block in
	 let l = bi.Pfs_rpcapi_aux.length in
	 bmv # blockmap_pin ~inode [identity, b, l]
      )
      blocklist


  method inode_alloc_e ii =
    (** First ensure that we know the maximum ID. This access is serialized
        so it cannot overlap with the insertion of a new ID
     *)
    new Uq_engines.seq_engine
      (shared_state # inode_get_new_id_serialized
	 (fun esys ->
	    match shared_state#inode_max_id with
	      | None ->
		  new Uq_engines.map_engine
		    ~map_done:(fun m ->
				 let id = Int64.succ m in
				 shared_state # inode_use_id id;
				 `Done id
			      )
		    (Nn_db.inode_maxid_ro_e ())
	      | Some m ->
		  let id = Int64.succ m in
		  shared_state # inode_use_id id;
		  new Uq_engines.epsilon_engine (`Done id) esys
	 ))
      (fun id ->
	 let ii' = 
	   { ii with 
	       Pfs_rpcapi_aux.blocklimit = 0L; 
	       committed = false;
	       seqno = 1L;
	   } in
	 Queue.push (`Inode_ins(id,ii')) journal;
	 let d = create_detail ii' in
	 let i = { i_id = id; i_detail = Some d } in
	 Hashtbl.add inodes id i;
	 let unlock_opt =
	   shared_state # lock_inode id self_owner in
	 assert(unlock_opt <> None);   (* the inode is new! *)
	 self # modify_link_number id (fun _ -> 0);
	 new Uq_engines.epsilon_engine (`Done id) esys
      )

  method private update_e ~id ~blocklimit ~set_mtime ~set_ctime
                          ~permission
                           ii_opt new_bis del_blkidx del_len =
    (** Combined update: if [ii_opt] is set, this is the new inodeinfo.
        The blocks in [new_bis] are added to the blocklist.
        The blocks [del_blkidx] to [del_blkidx+len-1] are deleted.

	The inodeinfo is now always updated - at least [seqno] is incremented,
	and [committed] is set to false.
	If [set_mtime], the mtime field is set to server time. 
	If [set_ctime], the ctime field is set to server time. 
	[blocklimit=`Ignore]: New values in [ii_opt] are ignored.
	[blocklimit=`Set n]: The blocklimit is set to [n].
	[blocklimit=`Update]: The blocklimit is updated as in [ii_opt]
     *)
    let unlock_opt =
      shared_state # lock_inode id self_owner in
    match unlock_opt with
      | None ->
	  new Uq_engines.epsilon_engine
	    (`Error (Transaction_error `econflict)) esys
      | Some unlock ->
	  (** First get the inodeinfo *)
	  let e =
	    new Uq_engines.seq_engine
	      (self # inode_get_e ~id)
	      (function
		 | None ->
		     raise (Transaction_error `estale)
		 | Some old_ii ->
		     (match permission old_ii with
			| Some error ->
			    raise (Transaction_error error)
			| None ->
			    ()
		     );
		     (** Get new i_detail with the ii_opt update (if any) *)
		     let new_ii0 =
		       match ii_opt with
			 | None -> old_ii
			 | Some ii -> ii  in
		     let now = Unix.gettimeofday() in
		     let new_ii =
		       { new_ii0 with 
			   Pfs_rpcapi_aux.blocklimit = (
			     match blocklimit with
			       | `Ignore ->
				   old_ii.Pfs_rpcapi_aux.blocklimit 
			       | `Set n ->
				   n
			       | `Update ->
				   new_ii0.Pfs_rpcapi_aux.blocklimit
			   );
			   mtime = (
			     if set_mtime then
			       encap_time now
			     else
			       new_ii0.Pfs_rpcapi_aux.mtime);
			   ctime = (
			     if set_ctime then
			       encap_time now
			     else
			       new_ii0.Pfs_rpcapi_aux.ctime);
			   seqno = Int64.succ old_ii.Pfs_rpcapi_aux.seqno;
			   committed = false;
			   anonymous = old_ii.Pfs_rpcapi_aux.anonymous;
		       } in
		     let i_detail0 =
		       try
			 let i_old = Hashtbl.find inodes id in
			 match i_old.i_detail with
			   | None   -> create_detail new_ii
			   | Some d -> { d with i_info = new_ii }
		       with
			 | Not_found -> create_detail new_ii in
		     (** Delete the blocks: *)
		     let i_detail1 =
		       if del_len > 0L then (
			 let del_blkidx_max = 
			   Int64.pred(Int64.add del_blkidx del_len) in
			 let i_blocks_dom1 =
			   Plasma_util.I64RM.add
			     (del_blkidx, del_blkidx_max) 
			     (del_blkidx, del_blkidx_max) 
			     i_detail0.i_blocks_dom in
			 let i_blocks1 =
			   Nn_blocklist.remove 
			     (del_blkidx, del_blkidx_max)i_detail0.i_blocks in
			 { i_detail0 with
			     i_blocks_dom = i_blocks_dom1;
			     i_blocks = i_blocks1
			 } 
		       )
		       else i_detail0 in
		     (** Now apply new_bis: *)
		     let new_bis_dom = Nn_blocklist.domain new_bis in
		     let i_blocks_dom2 =
		       List.fold_left
			 (fun acc (min,max) -> 
			    Plasma_util.I64RM.add (min,max) (min,max) acc)
			 i_detail1.i_blocks_dom
			 new_bis_dom in
		     let i_blocks2 =
		       Nn_blocklist.merge i_detail1.i_blocks new_bis in
		     let i_detail2 =
		       { i_detail1 with
			   i_blocks_dom = i_blocks_dom2;
			   i_blocks = i_blocks2
		       } in
		     (** Update i_ai: *)
		     let i_detail3 =
		       if new_bis <> Plasma_util.StrMap.empty then (
			 match i_detail2.i_ai with
			   | None -> i_detail2
			   | Some ailist ->
			       let aiht =
				 Nn_db.update_allocinfo id ailist new_bis in
			       let ailist' =
				 Hashtbl.fold 
				   (fun _ ai acc -> ai::acc) aiht [] in
			       { i_detail2 with i_ai = Some ailist' }
		       )
		       else i_detail2 in
		     (** Update the journal: *)
		     Queue.push (`Inode_upd(id,new_ii)) journal;
		     if del_len > 0L then
		       Queue.push
			 (`Inodeblocks_del(id,del_blkidx,del_len))
			 journal;
		     Queue.push (`Inodeblocks_ins_abstract(id,new_bis)) journal;
		     (** Update the cache [inodes] *)
		     let i = { i_id = id; i_detail = Some i_detail3 } in
		     Hashtbl.replace inodes id i;
		     new Uq_engines.epsilon_engine (`Done()) esys
	      ) in
	  e 
	  >> (fun st ->
		( match st with
		    | `Error _ | `Aborted -> unlock()
		    | `Done _ -> ()
		);
		st
	     )

  method inode_upd_e ~id ~keep_blocklimit ~permission ii =
    let blocklimit =
      if keep_blocklimit then `Ignore else `Update in
    self # update_e
      ~id ~blocklimit ~set_mtime:false ~set_ctime:false ~permission
      (Some ii) Nn_blocklist.empty 0L 0L

  method inode_upd_blocks_e ~id ~del ~add ~set_mtime ~set_blocklimit 
                            ~permission =
    let (del_blkidx, del_len) = del in
    let blocklimit =
      if set_blocklimit >= 0L then
	`Set set_blocklimit
      else
	`Ignore in
    self # update_e
      ~id ~blocklimit ~set_mtime ~set_ctime:false ~permission
      None add del_blkidx del_len

  method inode_upd_time ~id ~mtime ~ctime =
    Queue.push
      (`Inode_upd_time(id, mtime, ctime))
      journal

  method inode_set_anonymous ~id =
    Queue.push (`Inode_set_anonymous id) journal
    
  method inode_set_used inode =
    self # use_inode inode

  method inode_dealloc_e ~id =
    (** We only have to check whether the row really exists *)
    let unlock_opt = shared_state # lock_inode id self_owner in
    match unlock_opt with
      | None ->
	  new Uq_engines.epsilon_engine
	    (`Error (Transaction_error `econflict)) esys
      | Some unlock ->
	  let e =
	    self # inode_get_e ~id
	    ++ (function
		  | None ->
		      transerr_e `estale esys
		  | Some _ ->
		      eps_e (`Done ()) esys
	       )
	    ++ (fun () ->
		  (** Check that there are no filenames: *)
		  self # filename_rev_get_e ~inode:id
	       )
	    ++ (fun names ->
		  if names <> [] || id = 1L then
		    transerr_e `enotempty esys
		  else (
		    Queue.push (`Inode_del id) journal;
		    let i = { i_id = id; i_detail = None } in
		    Hashtbl.replace inodes id i;
		    eps_e (`Done ()) esys
		  )
	       ) in
	  e
	  >> (fun st ->
		( match st with
		    | `Error _ | `Aborted -> unlock()
		    | `Done _ -> ()
		);
		st
	     )

  method allocinfo_get_e ~id =
    try
      let i = Hashtbl.find inodes id in
      match i.i_detail with
	| None -> eps_e (`Done []) esys
	| Some d -> 
	    ( match d.i_ai with
		| None ->
		    Nn_db.allocinfo_get_ro_e ~inode:id 
		    ++ (fun db_list ->
			  let aiht = 
			    Nn_db.update_allocinfo id db_list d.i_blocks in
			  let ails =
			    Hashtbl.fold (fun _ ai acc -> ai::acc) aiht [] in
			  let d' =
			    { d with i_ai = Some ails } in
			  let i' =
			    { i with i_detail = Some d' } in
			  Hashtbl.replace inodes id i';
			  eps_e (`Done ails) esys
		       )
		| Some l -> 
		    eps_e (`Done l) esys
	    )
    with
      | Not_found ->
	  Nn_db.allocinfo_get_ro_e ~inode:id

  (** filenames *)

  val mutable fnupd_by_spot = Hashtbl.create 7
    (** Cached updates by spot. This is [None] if the spot was deleted,
        and [Some i] when it was added with inode [i]

	(spot = pair (dir_inode, name))
     *)

  val mutable fnupd_by_inode = Hashtbl.create 7
    (** Cached updates by inode. This is a pair of two lists [(add,del)]
        where [add] is a list of spots to which the inode was added and
        [del] as list of spots from which the inode was deleted. Both lists
        do not intersect

	TODO: add, del could be sets
     *)

  method filename_get_e ~dir_inode ~name =
    try
      let inode_opt = Hashtbl.find fnupd_by_spot (dir_inode,name) in
      eps_e (`Done inode_opt) esys
    with
      | Not_found ->
	  (* Look now in the shm dentrycache: *)
	  try
	    let inode = Nn_dentrycache.lookup dir_inode name in
	    eps_e (`Done (Some inode)) esys
	  with
	    | Not_found ->
		(* Get it from the db *)
		Nn_db.names_get_ro_e ~dir_inode ~name
		>> (function
		      | `Done None ->
			  Nn_dentrycache.delete dir_inode name;
			     (* should not be necessary *)
			  `Done None
		      | `Done (Some inode) ->
			  Nn_dentrycache.update dir_inode name inode;
			  `Done (Some inode)
		      | st -> st
		   )

  method filename_rev_get_e ~inode =
    let add_spots,del_spots = 
      try Hashtbl.find fnupd_by_inode inode
      with Not_found -> [], [] in
    Nn_db.names_rev_get_ro_e ~inode
    ++ (fun spots ->
	  let spots' =
	    add_spots @ 
	      (List.filter (fun sp -> not(List.mem sp del_spots)) spots) in
	  (** Not all unlinks are represented in [fnupd_by_inode]. Also check
              with [fnupd_by_spot]:
	   *)
	  let spots'' =
	    List.filter
	      (fun sp -> 
		 try Hashtbl.find fnupd_by_spot sp <> None 
		 with Not_found -> true
	      )
	      spots' in
	  eps_e (`Done spots'') esys
       )


  method filename_rev_get_dir_e ~inode =
    (* Same, but works only for directories, and gets an exist lock on the
       path
     *)
    if inode = 1L then
      eps_e (`Done ( 1L, "/", (fun () -> ()) )) esys
    else (
      self # inode_get_e ~id:inode 
      ++ (fun ii_opt ->
	    match ii_opt with
	      | None ->
		  eps_e (`Error (trans_error `estale)) esys
	      | Some ii ->
		  if ii.Pfs_rpcapi_aux.filetype <> `ftype_directory then
		    eps_e (`Error (trans_error `enotdir)) esys
		  else
		    eps_e (`Done ()) esys
	 )
      ++ (fun () ->
	    (** rev-lookup the directory inode *)
	    self # filename_rev_get_e ~inode
	 )
      ++ (fun spots ->
	    match spots with
	      | [ (containing_inode, name) as spot ] ->
		  (** At this point there is no guarantee that
		      this spot is linked to [inode]. A competing
		      transaction may have replaced it in the
		      meantime! To cope with this, we lock
		      the spot and check again.
		   *)
		  let owner = self_owner in
		  ( match shared_state#lock_filename spot `Exists owner with
		      | None ->
			  (* cannot get lock *)
			  eps_e (`Error(trans_error `econflict)) esys
		      | Some unlock ->
			  (* ok, we got the lock. However, the directory might
			     be deleted or renamed in the meantime 
			   *)
			  let e =
			    self # filename_get_e
			      ~dir_inode:containing_inode ~name
			    ++ (fun inode_opt ->
				  if inode_opt <> Some inode then 
				    eps_e (`Error(trans_error `econflict)) esys
				  else
				    eps_e
				      (`Done (containing_inode,name,unlock)) esys
			       ) in
			  e 
			  >> (fun st ->
				( match st with
				    | `Error _ | `Aborted -> unlock()
				    | `Done _ -> ()
				);
				st
			     )
		  )
	      | [] ->
		  (** Funny error: we found a directory without name *)
		  eps_e (`Error (trans_error `efhier)) esys
	      | _ ->
		  (** Strange error: This cannot be a directory anymore. *)
		  eps_e (`Error (trans_error `enotdir)) esys
	 )
    )

  method filename_parent_dir_e ~inode =
    if inode = 1L then (* root *)
      eps_e (`Done (Some 1L)) esys
    else
      Nn_db.names_get_parent_ro_e ~inode

  method filename_count_e ~inode =
    (* TODO: we can implement this better! *)
    self # filename_rev_get_e ~inode
    ++ (fun spots ->
	  eps_e (`Done(List.length spots)) esys
       )

  method filename_list_e ~dir_inode =
    Nn_db.names_list_ro_e ~dir_inode
    ++ (fun matches ->
	  dlogr
	    (fun () ->
	       sprintf
		 "filename_list(%Ld)=%s" 
		 dir_inode (String.concat "," (List.map fst matches)));
	  (** First check whether matches have been deleted *)
	  let matches' =
	    List.filter
	      (fun (n,i) ->
		 try Hashtbl.find fnupd_by_spot (dir_inode,n) <> None
		 with Not_found -> true
	      )
	      matches in
	  (** Find additional names. This is a bit ugly. (TODO) *)
	  let matches'' =
	    Hashtbl.fold
	      (fun (di,n) m acc ->
		 match m with
		   | Some inode ->
		       if di = dir_inode then
			 let acc' =
			   List.filter (fun (q,_) -> q <> n) acc in
			 (n,inode) :: acc'
		       else
			 acc
		   | None ->
		       acc
	      )
	      fnupd_by_spot
	      matches' in
	  eps_e (`Done matches'') esys
       )


  method private push_link ~dir_inode ~name ~inode upd_links =
    (** Do the link operation: *)
    Hashtbl.replace fnupd_by_spot (dir_inode,name) (Some inode);
    let add,del = 
      try Hashtbl.find fnupd_by_inode inode
      with Not_found -> [], [] in
    let add' = (dir_inode,name) :: add in
    let del' = List.filter (fun sp -> sp <> (dir_inode,name)) del in
    Hashtbl.replace fnupd_by_inode inode (add', del');
    Queue.push
      (`Names_ins (dir_inode,name,inode))
      journal;
    if upd_links then
      self # modify_link_number inode (fun n -> n+1);
    ()

  method filename_link_e ~dir_inode ~name ~inode =
    let acquire_locks other_unlock =
      let owner = self_owner in
      (** We have to get two locks:
	  - [`Link] for spot [dir_inode, name]
	  - a lock for the inode
       *)
      match shared_state # lock_filename (dir_inode,name) `Link owner with
	| None ->
	    other_unlock(); None
	| Some unlock_name ->
	    ( match shared_state # lock_inode inode owner with
		| None ->
		    other_unlock(); unlock_name(); None
		| Some unlock_inode ->
		    let unlock() =
		      other_unlock(); unlock_name(); unlock_inode() in
		    Some unlock
	    )  in

    (* Before we can do the link operation, we need to know the spot of
       the directory [dir_inode] in the hierarchy. This implicitly
       allocates a lock for the spot.
     *)
    self # filename_rev_get_dir_e ~inode:dir_inode
    ++ (fun (_, _, dir_unlock) ->
	  match acquire_locks dir_unlock with
	    | None ->
		eps_e (`Error(Transaction_error `econflict)) esys
	    | Some unlock ->
		(* we already know here that [dir_inode] exists and
		   is a directory.
		 *)
		let e =
		  (** Check: The name must be new *)
		  self # filename_get_e ~dir_inode ~name 
		  ++ (fun ex_inode_opt ->
			if ex_inode_opt <> None then
			  transerr_e `eexist esys
			else (
			  (** Get the inodeinfo of the file to link: *)
			  self # inode_get_e ~id:inode
			)
		     )
		  ++ (fun ii_opt ->
			(** If the inode is a directory, we have to check
			    that there is no other name pointing to it
			 *)
			match ii_opt with
			  | None ->
			      transerr_e `estale esys
			  | Some ii ->
			      match ii.Pfs_rpcapi_aux.filetype with
				| `ftype_directory ->
				    (** Get other names of the directory *)
				    self # filename_rev_get_e ~inode 
				    ++ (fun names ->
					  if names <> [] || inode=1L then
					    transerr_e `efhier esys
					  else
					    (** All ok *)
					    eps_e (`Done ()) esys
				       )
				| _ -> 
				    (** no directory: any number of links allowed *)
				    eps_e (`Done ()) esys
		     ) in
		e 
		>> (fun st ->
		      ( match st with
			  | `Error _ | `Aborted -> unlock()
			  | `Done _ ->
			      self # push_link ~dir_inode ~name ~inode true
		      );
		      st
		   )
       )
	    
  method private push_unlink ~dir_inode ~name ~inode n_links =
    (** We do not check here whether [name] really exists in the db.
        Just delete if it exists.
     *)
    let spot = (dir_inode,name) in
    Hashtbl.replace fnupd_by_spot spot None;
    let add,del = 
      try Hashtbl.find fnupd_by_inode inode
      with Not_found -> [], [] in
    let add' = List.filter (fun sp -> sp <> spot) add in
    let del' = if List.mem spot del then del else spot :: del in
    Hashtbl.replace fnupd_by_inode inode (add', del');
    Queue.push (`Names_del spot) journal;
    if n_links >= 0 then
      self # modify_link_number inode (fun n -> n_links);
    ()

  method filename_unlink_e ~dir_inode ~name ~owner_restr =
    let owner = self_owner in
    let unlock_i = ref (fun () -> ()) in
    match shared_state#lock_filename (dir_inode,name) `Unlink owner with
      | None ->
	  transerr_e `econflict esys
      | Some unlock ->
	  (** First look the inode up (want to know the type). *)
	  let e =
	    self # filename_get_e ~dir_inode ~name 
	    ++ (fun inode_opt ->
		  match inode_opt with
		    | None ->
			transerr_e `enoent esys
		    | Some inode ->
			(** Have to lock here the inode, unfortunately.
			    (TODO: get rid of this lock.) We do this here
			    so we can always delete the inode as post
			    operation if the number of links drops to 0
			 *)
			match shared_state # lock_inode inode owner with
			  | None ->
			      transerr_e `econflict esys
			  | Some unlock_inode ->
			      unlock_i := unlock_inode;
			      (** Get the inodeinfo *)
			      self # inode_get_e ~id:inode
			      ++ (fun ii_opt ->
				    eps_e (`Done(inode,ii_opt)) esys
				 )
	       )
	    ++ (fun (inode,ii_opt) ->
		  match ii_opt with
		    | None ->
			(* This is theoretically posssible when a competing
			   transaction unlinks in the right moment and
			   commits. We conclude that the filename is already
			   unlinked then.
			 *)
			eps_e (`Done()) esys
		    | Some ii ->
			(** Maybe check owner: *)
			( match owner_restr with
			    | None -> ()
			    | Some u ->
				if ii.A.usergroup.A.user <> u then
				  raise(Transaction_error `eperm)
			);
			(** Get the number of links: *)
			self # filename_rev_get_e ~inode
			++ (fun (names) ->
			      let n = List.length names in
			      (** Regular files and symlinks can be
				  always deleted
			       *)
			      let ft = ii.Pfs_rpcapi_aux.filetype in
			      match ft with
				| `ftype_regular | `ftype_symlink ->
				    self # push_unlink
				      ~dir_inode ~name ~inode (n-1);
				    eps_e (`Done()) esys
				| `ftype_directory ->
				    (* only empty directories can be deleted: *)
				    self # filename_list_e ~dir_inode:inode
				    ++ (fun entries ->
					  if entries <> [] || inode=1L then
					    transerr_e `enotempty esys
					  else (
					    self # push_unlink
					      ~dir_inode ~name ~inode (n-1);
					    eps_e (`Done()) esys
					  )
				       )
			   ) 
	       ) in
	  (** We keep the locks on success. *)
	  e 
	  >> (fun st ->
		( match st with
		    | `Error _ | `Aborted -> unlock(); !unlock_i()
		    | `Done _ -> ()
		);
		st
	     )


  method filename_rename_e ~from_dir_inode ~from_name ~to_dir_inode ~to_name 
                           ~owner_restr =
    (* A rename is almost the same as an unlink followed by a link. However,
       a few details are different:
       - Renaming non-empty directories is allowed
       - The destination must not be in the tree of the source
       - no need to lock the inode (link count cannot drop to 0)
     *)
    let owner = self_owner in
    let unlocks = ref [] in
    let from_spot = (from_dir_inode,from_name) in
    let to_spot = (to_dir_inode,to_name) in
    let e =
      match shared_state#lock_filename from_spot `Unlink owner with
	| None ->
	    transerr_e `econflict esys
	| Some unlock_unlink ->
	    unlocks := unlock_unlink :: !unlocks;
	    (** First look the inode up (want to know the type). *)
	    self # filename_get_e ~dir_inode:from_dir_inode ~name:from_name
	    ++ (fun from_inode_opt ->
		  match from_inode_opt with
		    | None ->
			transerr_e `enoent esys
		    | Some from_inode ->
			self # inode_get_e ~id:from_inode
			++ (fun from_ii_opt ->
			      eps_e (`Done(from_inode,from_ii_opt)) esys
			   )
	       )
	    ++ (fun (from_inode,from_ii_opt) ->
		  match from_ii_opt with
		    | None ->
			transerr_e `econflict esys
		    | Some from_ii ->
			(* Maybe check owner: *)
			( match owner_restr with
			    | None -> ()
			    | Some u ->
				if from_ii.A.usergroup.A.user <> u then
				  raise(Transaction_error `eperm)
			);
			(* The following ensures that the destination directory
			   exists, and has a name.
			 *)
			self # filename_rev_get_dir_e ~inode:to_dir_inode
			++ (fun (_, _, dir_unlock) ->
			      unlocks := dir_unlock :: !unlocks;
			      let lopt =
				shared_state # lock_filename 
				  to_spot `Link owner in
			      match lopt with
				| None ->
				    transerr_e `econflict esys
				| Some unlock ->
				    unlocks := unlock :: !unlocks;
				    eps_e (`Done()) esys
			   )
			++ (fun () ->
			      (* Check: destination is new *)
			      self # filename_get_e
				~dir_inode:to_dir_inode ~name:to_name
			      ++ (fun ex_inode_opt ->
				    if ex_inode_opt <> None then
				      transerr_e `eexist esys
				    else
				      eps_e (`Done()) esys
				 )
			   )
			++ (fun () ->
			      (* Check: destination not a child of the source *)
			      self # filename_child_check_e 
				from_inode to_dir_inode
			      ++ (fun is_child ->
				    if is_child then
				      transerr_e `efhier esys
				    else (
				      self # push_unlink
					~dir_inode:from_dir_inode
					~name:from_name 
					~inode:from_inode (-1);
				      self # push_link
					~dir_inode:to_dir_inode
					~name:to_name
					~inode:from_inode false;
				      eps_e (`Done ()) esys
				    )
				 )
			   )
	       )
    in
    (** We keep the locks on success. *)
    e 
    >> (fun st ->
	  ( match st with
	      | `Error _ | `Aborted ->
		  List.iter (fun f -> f()) !unlocks
	      | `Done _ -> ()
	  );
	  st
       )
      

  method private filename_child_check_e super_dir_inode sub_dir_inode =
    (* Checks whether sub_dir_inode is a child of super_dir_inode.
       Here, sub_dir_inode = super_dir_inode does count as a 
       child relationship.
     *)
    let rec climb_e ~inode =
      if inode = super_dir_inode then
	eps_e (`Done true) esys
      else if inode = 1L then
	eps_e (`Done false) esys
      else
	self # filename_parent_dir_e ~inode
	++ (fun parent_inode_opt ->
	      match parent_inode_opt with
		| None ->
		    eps_e (`Done false) esys
		| Some parent_inode ->
		    climb_e ~inode:parent_inode
	   ) in
    climb_e ~inode:sub_dir_inode


  method delayed_inode_deletes =
    let del_now = ref [] in
    let del_later = ref [] in
    (** Maybe we have to unlink inode at the end of the transaction. *)
    Hashtbl.iter
      (fun inode n ->
	 let users = 
	   try shared_state#inode_use_counts inode with Not_found -> 0 in
	 assert(users <> 0);  (* because this object is a user *)
	 if n=0 && inode <> 1L then (
	   if users = 1 then
	     del_now := inode :: !del_now
	   else
	     del_later := inode :: !del_later
	 
	 )
      )
      inode_links;
    (!del_now, !del_later)

  (** generic *)

  val q_updcache = Queue.create()

  method pre_commit =
    Queue.clear q_updcache; (* The journal does not survive until post_commit *)
    Queue.iter
      (fun m ->
	 match m with
	   | `Inode_ins(_,_)
	   | `Inode_upd(_,_)
	   | `Inode_del _
	   | `Inode_upd_time(_,_,_)
	   | `Inode_set_anonymous _
	   | `Names_ins(_,_,_)
	   | `Names_del(_,_) ->
	       Queue.push m q_updcache
	   | _ ->
	       ()
      )
      journal;
    journal

  method pre_commit_mon =
    Queue.create()

  method post_commit ok =
    (* This method is included in the commit serialization! *)
    if ok then (
      Queue.iter
	(function
	   | `Inode_ins(inode,ii) ->
	       Nn_inodecache.update inode ii
	   | `Inode_upd(inode,ii) ->
	       Nn_inodecache.update inode ii
	   | `Inode_del inode ->
	       Nn_inodecache.delete inode
	   | `Inode_set_anonymous inode ->
	       Nn_inodecache.delete inode
	   | `Inode_upd_time(inode,mtime_opt,ctime_opt) ->
	       ( try
		   let old_ii = Nn_inodecache.lookup inode in
		   let new_ii =
		     { old_ii with
			 A.mtime = (match mtime_opt with
				      | None -> old_ii.A.mtime
				      | Some t -> t
				   );
			 A.ctime = (match ctime_opt with
				      | None -> old_ii.A.ctime
				      | Some t -> t
				   );
		     } in
		   Nn_inodecache.update inode new_ii
		 with Not_found -> ()
	       )
	   | `Names_ins(dir_inode,name,inode) ->
	       Nn_dentrycache.update dir_inode name inode
	   | `Names_del(dir_inode,name) ->
	       Nn_dentrycache.delete dir_inode name
	   | _ ->
	       ()
	)
	q_updcache;
      Queue.clear q_updcache
    );
    self#rollback()

  method rollback () =
    (** Just release all locks *)
    shared_state # unlock_inodes self_owner;
    shared_state # unlock_filenames self_owner;
    Hashtbl.iter
      (fun inode () ->
	 shared_state # inode_modify_use_counts
	   ~transactions:(fun n -> n-1)
	   inode
      )
      inode_used
end
