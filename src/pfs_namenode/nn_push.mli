(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Push updates to the slaves (client) *)

type client = Rpc_proxy.ManagedClient.mclient

type commit_id = int

val create_client : Rpc_client.connector -> Pfs_auth.client_auth -> 
                    float -> Unixqueue.event_system ->
                      client
  (** Creates a new client. The [float] arg is the message timeout *)

val begin_transaction_e : client -> commit_id ->
                            string -> string -> unit Uq_engines.engine
  (** Starts the transaction: [begin_transaction_e c clustername exp_rev].
      Also transmits the datastore cache ([set_ds_cache]).
   *)      

val push_e : Nn_db.modification -> client -> commit_id -> unit Uq_engines.engine
  (** Pushes the db modification to the slave which the client connects
      to
   *)

val prepare_commit_e : client -> commit_id -> bool Uq_engines.engine
  (** Prepares the commit. The resulting bool value is [true] if the
      commit is possible and allowed.
   *)

val commit_e : client -> commit_id -> unit Uq_engines.engine
  (** Commits the transaction *)

val trigger_shutdown : client -> unit
  (** Triggers the shutdown of this client *)

val wait_until_ready_e : Rpc_client.connector -> Unixqueue.event_system ->
                           unit Uq_engines.engine
  (** Busy-waits until the nameslave program is available *)
