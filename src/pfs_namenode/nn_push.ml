(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Uq_engines.Operators

let dlogf = Plasma_util.dlogf

module Nslave_proxy = Pfs_rpcapi_clnt.Make'Nameslave(Rpc_proxy.ManagedClient)

type client = Rpc_proxy.ManagedClient.mclient

type commit_id = int

let create_client conn cauth tmo esys =
  dlogf
    "Nn_push.create_client tmo=%f" tmo;
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Nameslave.V1._program ]
      ~msg_timeout:tmo
      ~msg_timeout_is_fatal:true
      ~initial_ping:true
      ~auth_methods:(Pfs_auth.rpc_proxy_auth_methods cauth true)
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  mclient

let trigger_shutdown mclient =
  Rpc_proxy.ManagedClient.trigger_shutdown mclient (fun () -> ())

let rec wait_until_ready_e conn esys =
  dlogf
    "Nn_push.wait_until_ready_e";
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Nameslave.V1._program ]
      ~msg_timeout:1.0
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  Uq_engines.meta_engine
    (Plasma_util.rpc_engine mclient Nslave_proxy.V1.null'async ())
  ++ (fun st ->
	Rpc_proxy.ManagedClient.trigger_shutdown mclient (fun () -> ());
	match st with
	  | `Done() -> eps_e (`Done()) esys
	  | _ ->
	      Uq_engines.delay_engine 
		1.0
		(fun () ->  wait_until_ready_e conn esys)
		esys
     )


let begin_transaction_e mclient commit_id cn rev =
  try
    let l = Nn_db.ds_cache_contents() in
    Plasma_util.rpc_engine
      mclient Nslave_proxy.V1.begin_transaction'async (commit_id,cn,rev)
    ++ (fun () ->
	  (* FIXME: questionable code *)
	  let l' =
	    Array.of_list (List.map Nn_datastores.to_xdr l) in
	  Plasma_util.rpc_engine
	    mclient Nslave_proxy.V1.set_ds_cache'async l'
       )
  with err ->
    eps_e (`Error err) (Rpc_proxy.ManagedClient.event_system mclient)
      
let prepare_commit_e mclient commit_id =
  Plasma_util.rpc_engine mclient Nslave_proxy.V1.prepare_commit'async commit_id

let commit_e mclient commit_id =
  Plasma_util.rpc_engine mclient Nslave_proxy.V1.commit'async commit_id

let push_e m mclient commit_id =
  match m with
    | #Pfs_rpcapi_aux.modification as m1 ->
	Plasma_util.rpc_engine 
	  mclient 
	  Nslave_proxy.V1.push'async
	  (commit_id,m1)
    | `Inodeblocks_ins_abstract(inode,blist) ->
        let bi_list = Nn_blocklist.to_blockinfo_list blist in
        let m1 = `Inodeblocks_ins(inode,Array.of_list bi_list) in
	Plasma_util.rpc_engine 
	  mclient 
	  Nslave_proxy.V1.push'async
	  (commit_id,m1)
