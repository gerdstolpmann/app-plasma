(*
  Copyright 2013 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

 open Printf

class type dump_config = 
object 
  method nn_dump_dir : string
end

type dump_entry =
    { dump_prefix : string;
      dump_ref : string;
      dump_suffix : string;
      dump_finished : bool;
      dump_timestamp : float
    }

let create_dump config (prefix,nref) =
  let dir = config # nn_dump_dir in

  let rec next k =
    if k = 100_000 then
      failwith "Nn_dumps.create_dump: cannot create dump directory";
    let name = sprintf "%s_%s_%05d" prefix nref k in
    let full_name = sprintf "%s/%s" dir name in
    try
      Unix.mkdir full_name 0o700;
      k
    with
      | Unix.Unix_error(Unix.EEXIST,_,_) ->
          next (k+1)
  in
  let k = next 0 in
  { dump_prefix = prefix;
    dump_ref = nref;
    dump_suffix = sprintf "%05d" k;
    dump_finished = false;
    dump_timestamp = Unix.time()
  }

let dump_directory config entry =
  let name = 
    sprintf "%s_%s_%s" entry.dump_prefix entry.dump_ref entry.dump_suffix in
  sprintf "%s/%s" config#nn_dump_dir name
  

let dump_exists config entry =
  let full_dir = dump_directory config entry in
  Sys.is_directory full_dir


let finish_dump config entry =
  let full_dir = dump_directory config entry in
  let f = open_out (sprintf "%s/dump_succeeded" full_dir) in
  let st = Unix.fstat (Unix.descr_of_out_channel f) in
  close_out f;
  { entry with
    dump_finished = true;
    dump_timestamp = Unix.(st.st_mtime)
  }


let name_re = Netstring_str.regexp "^\\([^_]+\\)_\\(.*\\)_\\([0-9]+\\)$"

let dest_token name =
  match Netstring_str.string_match name_re name 0 with
    | None ->
         failwith "Nn_dumps.dest_token: bad format"
    | Some m ->
         let prefix = Netstring_str.matched_group m 1 name in
         let nref = Netstring_str.matched_group m 2 name in
         let suffix = Netstring_str.matched_group m 3 name in
         (prefix,nref,suffix)


let create_token (prefix,nref,suffix) =
  prefix ^ "_" ^ nref ^ "_" ^ suffix

let list_dumps config =
  let dir = config # nn_dump_dir in
  let p = Sys.readdir dir in
  let l =
    List.filter
      (fun name ->
         let fulldir = sprintf "%s/%s" dir name in
         Sys.is_directory fulldir &&
           Netstring_str.string_match name_re name 0 <> None
      )
      (Array.to_list p) in
  List.map
    (fun name ->
       match Netstring_str.string_match name_re name 0 with
         | None -> assert false
         | Some m ->
             let prefix = Netstring_str.matched_group m 1 name in
             let nref = Netstring_str.matched_group m 2 name in
             let suffix = Netstring_str.matched_group m 3 name in
             let fin_file = sprintf "%s/%s/dump_succeeded" dir name in
             let ex_fin_file = Sys.file_exists fin_file in
             let ts_file =
               if ex_fin_file then fin_file else sprintf "%s/%s" dir name in
             let st = Unix.stat ts_file in
             { dump_prefix = prefix;
               dump_ref = nref;
               dump_suffix = suffix;
               dump_finished = ex_fin_file;
               dump_timestamp = Unix.(st.st_mtime);
             }
    )
    l

let get_dump_files config entry =
  let d = dump_directory config entry in
  let l = Array.to_list (Sys.readdir d) in
  List.flatten
    (List.map
       (fun n ->
          if n.[0] = '.' || n.[String.length n - 1] = '~' || 
               n = "dump_succeeded" 
          then
            []
          else
            let p = sprintf "%s/%s" d n in
            let st = Unix.LargeFile.stat p in
            [ n, Unix.LargeFile.(st.st_size) ]
       )
       l
    )

let remove_dump config entry =
  let d = dump_directory config entry in
  let cmd = sprintf "rm -rf %s" (Filename.quote d) in
  let code = Sys.command cmd in
  if code <> 0 then
    failwith ("Command not successful: " ^ cmd)

    
let equal d1 d2 =
  d1.dump_prefix = d2.dump_prefix &&
    d1.dump_ref = d2.dump_ref &&
      d1.dump_suffix = d2.dump_suffix
