(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Two-phase commit protocol *)


val commit_e : (unit -> Nn_db.modification Queue.t) ->
               (unit -> Nn_db.modification Queue.t) ->
               (bool -> unit) ->
               Nn_state.shared_state_t ->
               string ->
                 bool Uq_engines.engine
  (** [commit_e get_journal get_journal_mon post_commit shared_state debug_name]:

      The function calls [get_journal()] and [get_journal_mon()] to
      get the list of modifications. See below for the idea of
      [get_journal_mon].

      It writes the modifications to the local database and all slaves,
      and commits the update. The bool result says whether the commit
      was successful. If [false], the transaction is guaranteed to be
      rolled back. The commit engine may also enter an error state.
      This is considered as a critical violation of the protocol.

      If the queue of modification is empty, the local transaction is
      committed, and no remote operations are involved. (This means
      that the user only submitted read requests and no writes.)

      If there are modifications, a new revision string is created
      and appended to the journal before anything is written.

      The function invokes [post_commit] when the commit or
      rollback is done. The bool arg is the same as in the result.

      Commits are partly serialized (using the commit monitor in
      [shared_state]).  The function [get_journal] is called before
      the monitored section is entered. The function [get_journal_mon]
      is called twice: Once before the section starts, and once at the
      beginning of the section.  This second call replaces the values
      coming from the first call.  The problem is here that we need
      the modifications for determining the condition the commit
      monitor should wait for. However, some of the modifications may
      already have changed when the monitor allows that the execution
      flow continues. These changes can be reflected by
      [get_journal_mon].

      The monitored code section includes the calls of [post_commit].

      As side effects, the function may disable slaves, and even
      (self-)disable the coordinator.
   *)


val trans_e :  Nn_state.shared_state_t ->
               Nn_state.view_t list -> 
              'a Uq_engines.engine -> 
               string ->
                'a Uq_engines.final_state Uq_engines.engine
  (** [let e' = trans_e sh vl e debug_name]: Runs the engine [e] until
      it is finished,
      and if [e] was successful, the modifications of the views [vl]
      are taken as the modifications to be done in the transaction,
      and the transaction is committed.

      If [e] is not successful,
      the views and the db transaction are rolled back. 

      The returned engine [e'] is [`Done(st)] when
      the transaction is over (after commit or rollback). [st] reflects the
      final state of [e].

      [e'] reaches an error state only if the commit fails (critical error).
   *)

