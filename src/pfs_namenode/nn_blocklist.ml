(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Plasma_util
open Pfs_rpcapi_aux
open Printf

module Blockinfo = struct
  type key = int64
  type t = blockinfo

  let split split_idx bi =
    let idx_end = Int64.add bi.index bi.length in
    if split_idx <= bi.index || split_idx >= idx_end then
      failwith "Nn_blocklist.Blockinfo.split: cannot split";
    let left_lenL = Int64.sub split_idx bi.index in
    let left =
      { bi with
	  length = left_lenL
      } in
    let right_lenL = Int64.sub idx_end split_idx in
    let right =
      { bi with
	  index = split_idx;
	  block = Int64.add bi.block left_lenL;
	  length = right_lenL
      } in
    (left, right)

  let merge b1 b2 =
    try
      let idx_end1 = Int64.add b1.index b1.length in
      if idx_end1 < b1.index then raise Not_found;  (* overflow *)
      let idx_end2 = Int64.add b2.index b2.length in
      if idx_end2 < b2.index then raise Not_found;  (* overflow *)
      let block_end1 = Int64.add b1.block b1.length in
      if block_end1 < b1.block then raise Not_found;  (* overflow *)
      let block_end2 = Int64.add b2.block b2.length in
      if block_end2 < b2.block then raise Not_found;  (* overflow *)
      let compat =
	b1.identity = b2.identity &&
	b1.ticket = b2.ticket &&
	b1.inode_seqno = b2.inode_seqno &&
	b1.inode_committed = b2.inode_committed in
      let left_compat =
	compat && idx_end1 = b2.index && block_end1 = b2.block in
      let right_compat =
	compat && idx_end2 = b1.index && block_end2 = b1.block in
      if left_compat then
	Some { b1 with length = Int64.add b1.length b2.length }
      else
	if right_compat then
	  Some { b2 with length = Int64.add b1.length b2.length }
	else
	  raise Not_found
    with
      | Not_found -> None
end


module BlockRM = RangeMap(I64)(Blockinfo)


type blocklist = BlockRM.t StrMap.t


let to_blocklist l =
  List.fold_left
    (fun acc bi ->
       assert(bi.length >= 1L);
       let idx_from = bi.index in
       let idx_to = Int64.pred (Int64.add idx_from bi.length) in
       let brm =
	 try StrMap.find bi.identity acc
	 with Not_found -> BlockRM.empty in
       let brm' =
	 BlockRM.add (idx_from,idx_to) bi brm in
       StrMap.add bi.identity brm' acc
    )
    StrMap.empty
    l


let to_blockinfo_list blist =
  StrMap.fold
    (fun _ brm acc ->
       List.rev (BlockRM.fold (fun _ bi acc -> bi::acc) brm acc)
    )
    blist
    []


let to_string blist =
  let bi_list = to_blockinfo_list blist in
  String.concat ","
    (List.map
       (fun bi ->
	  sprintf "%s:%Ld-%Ld@%Ld-%Ld"
	    bi.identity
	    bi.index
	    (Int64.pred (Int64.add bi.index bi.length))
	    bi.block
	    (Int64.pred (Int64.add bi.block bi.length))
       )
       bi_list
    )


let empty_ticket =
  { range_start = 0L;
    range_length = 0L;
    safetrans_id = 0L;
    safetrans_tmo = 0L;
    safetrans_vfy = 0L;
    read_perm = false;
    write_perm = false
  }


let empty = StrMap.empty

let identities blist =
  List.fold_left
    (fun acc (id, _) -> StrSet.add id acc) 
    StrSet.empty
    (StrMap.bindings blist)

let norm blist =
  (* Just throw empty stuff away *)
  StrMap.filter
    (fun identity brm -> brm <> BlockRM.empty)
    blist

let dnmap f blist =
  norm (StrMap.mapi f blist)

let dnmap2 f blist1 blist2 =
  let identities =
    StrSet.fold
      (fun id acc -> id::acc)
      (StrSet.union (identities blist1) (identities blist2))
      [] in
  let l =
    List.map
      (fun identity ->
	 let brm1 =
	   try StrMap.find identity blist1
	   with Not_found -> BlockRM.empty in
	 let brm2 =
	   try StrMap.find identity blist2
	   with Not_found -> BlockRM.empty in
	 (identity, f identity brm1 brm2)
      )
      identities in
  norm
    (List.fold_left (fun acc (id,brm) -> StrMap.add id brm acc) StrMap.empty l)

let sub range blist =
  dnmap
    (fun identity brm ->
       BlockRM.sub range brm
    )
    blist

let remove range blist =
  dnmap
    (fun identity brm ->
       BlockRM.remove range brm
    )
    blist

let merge blist1 blist2 =
  dnmap2
    (fun identity brm1 brm2 ->
       (* add the elements from brm2 to brm1, overriding existing elements *)
       BlockRM.fold
	 BlockRM.add
	 brm2
	 brm1
    )
    blist1
    blist2

let iter f blist =
  StrMap.iter
    (fun identity brm ->
       BlockRM.iter
	 (fun key bi -> 
	    f identity key bi
	 )
	 brm
    )
    blist

let fold f blist init =
  StrMap.fold
    (fun identity brm acc ->
       BlockRM.fold
	 (fun key bi acc -> 
	    f identity key bi acc
	 )
	 brm
	 acc
    )
    blist
    init

let map f blist =
  dnmap
    (fun _ brm ->
       BlockRM.fold
	 (fun key bi acc ->
	    let bi' = f bi in
	    BlockRM.add key bi' acc
	 )
	 brm
	 BlockRM.empty
    )
    blist

let domain blist =
  let set =
    StrMap.fold
      (fun _ brm acc ->
	 BlockRM.fold
	   (fun key _ acc ->
	      I64RM.add key key acc
	   )
	   brm
	   acc
      )
      blist
      I64RM.empty in
  I64RM.fold
    (fun key _ acc -> key :: acc)
    set
    []


let cardinal blist =
  fold (fun _ (min,max) _ acc -> 
	  let n = Int64.succ (Int64.sub max min) in
	  Int64.add n acc
       )
    blist
    0L
