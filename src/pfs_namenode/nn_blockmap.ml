(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* TODO: return blocks in ascending order *)


open Printf
open Uq_engines.Operators

let dlogf = Plasma_util.dlogf

exception Inactive

type reserve_info =
    { ri_blocks : Nn_db.allocinfo list;  
    }

type triv_range = int64 * int64

type owner = int * int
    (* (process ID, sequential number) *)

let create_owner() =
  (* Assume multi-processing *)
  let seqnr =
    Oo.id (object end) in
  (Unix.getpid(), seqnr)

let string_of_owner (pid,seqnr) =
  sprintf "<%d.%d>" pid seqnr

class type blockmap_t =
object
  method active : bool
  method filled : bool
  method id : int
  method identity : string
  method size : int64
  method stats : int64 * int64 * int64
  method fill_blockmap : int64 -> string -> unit
  method fill_from_db : int64 -> int -> unit
  method fill_done : unit -> unit
  method has_free_blocks : bool
  method reserve : int -> owner -> reserve_info -> triv_range list
  method free : triv_range list -> owner -> unit
  method pin : triv_range list -> owner -> unit
  method commit : owner -> unit
  method rollback : owner -> unit
  method release : owner -> unit
  method get_changes : owner -> (int64 * string) list
  method inactivate : unit -> unit
  method dump : string
end


let create_reserve_info ?(blocks=[]) () =
  { ri_blocks = blocks
  }


let rec mklist p0 p1 =
  if p0 <= p1 then
    p0 :: mklist (p0+1) p1
  else
    []


let bmaprowsize = Nn_config.bmaprowsize
let bmaprowsizeL = Int64.of_int bmaprowsize


module Int = struct
  type t = int
  let compare x y =
    Pervasives.compare (x:int) (y:int)
end

module IntSet = Set.Make(Int) 
module LRU_int = Plasma_util.LRU(Int)


let count_ones bv =
  Bitv.fold_left (fun acc b -> if b then acc+1 else acc) 0 bv

let select_row rowset inode_opt avoid =
  (* Select a row from [rowset] for [inode_opt] *)
  (* FIXME: rowset is an IntSet.t. This is not a good data structure for this,
     because we can only iterate over the set.
     FIXME: Another criterion for the selection: distance to preferred rows.
   *)
  let n = IntSet.cardinal rowset in
  let k = 
    match inode_opt with
      | None ->
	  Plasma_rng.random_int n
      | Some inode ->
	  let s = Digest.string (Int64.to_string inode) in
	  Int64.to_int
	    (Int64.rem
	       (Rtypes.int64_of_int8 (Rtypes.read_int8 s 0)) 
	       (Int64.of_int n))
  in
  let j = ref 0 in
  let row = ref (IntSet.choose rowset) in
  ( try
      IntSet.iter
	(fun r -> 
	   if !j = k then (
	     row := r; 
	     if not (avoid !row) then raise Exit
	   );
	   incr j
	)
	rowset
    with Exit -> ()
  );
  !row


class blockmap ~id ~identity ~size : blockmap_t =
  let () = assert(size > 0L) in
  let size_rows_64 = Int64.succ (Int64.div (Int64.pred size) bmaprowsizeL) in
  let () =
    (* impl. limitation *)
    if size_rows_64 > Int64.of_int Sys.max_array_length then
      failwith "Nn_state.blockmap: datastore too large" in
  let size_rows = Int64.to_int size_rows_64 in
  let size_rem = Int64.to_int (Int64.rem size bmaprowsizeL) in
  let n_last = if size_rem = 0 then bmaprowsize else size_rem in
object(self)
  val mutable active = true
  val mutable filled = false
  val mutable committed_map = [| |]
    (** row k contains the bits of the blocks k*1024 to k*1024+1023
        (except for the last row which may be shorter). The bits are
        stored as {!Bitv} bit vector. Bit '1' means the block is used.
        A '0' leaves it open whether it is free or reserved.

        (1024 = bmaprowsize)
     *)
   val mutable pin_map = [| |]
    (** same layout as [committed_map]. The meaning depends on the
	value of the corresponding bit in  [committed_map]:
	- If the bit in [committed_map] is 0, a 1 in [pin_map] can have
	  two causes: (1) a new block is allocated but not committed,
	  or (2) a competing transaction did already commit the [free],
	  but we still access this block
	- If the bit in [committed_map] is 1, a 1 in [pin_map] means
	  that the block is pinned, and that there was no committed [free]
	  yet (or no [free] is going to happen at all)

        Only if both the bits in [committed_map] and [pin_map]
	are 0, the block is really free and can be allocated again.
     *)
   val mutable stats = [| |]
     (** per-row statistics:

	 {[ stats.(k) = (n_used_k, n_free_pinned_k, n_alloc_k) ]}

	 where
	 - [n_used_k]: the number of committed used blocks in row [k]
	 - [n_free_pinned_k]: the number of committed free blocks that are
           pinned by a transaction in row [k]. This also includes newly
	   allocated blocks
	 - [n_alloc_k]: the number of allocatable blocks
      *)
  val mutable n_used = 0L
  val mutable n_free_pinned = 0L
    (** #blocks that are free and committed but where there is a pin bit *)
  val mutable alloctab = [| |]
    (** maps the number of allocatable blocks to the set of rows *)
  val mutable alloctab_out_of_date = false
    (** false: have to recompute alloctab, n_used, n_reserved *)
  val mutable owner = Hashtbl.create 7
    (** maps owner objects to changes: see [get_owner_tuple] *)
  val mutable rowowner = Hashtbl.create 7
    (** maps rows to owner objects *)
  val mutable lru_rows = LRU_int.create()
    (** queue of least recently modified rows *)
    

  initializer (
    committed_map <-
      Array.init
	size_rows
	(fun k ->
	   let len = if k=size_rows-1 then n_last else bmaprowsize in
	   Bitv.create len false
	);
    pin_map <- Array.map Bitv.copy committed_map;
    stats <-
      Array.init
	size_rows
	(fun k ->
	   let len = if k=size_rows-1 then n_last else bmaprowsize in
	   (0,0,len)
	);
    alloctab_out_of_date <- true
  )

  method dump =
    let b = Buffer.create 500 in
    for row = 0 to size_rows-1 do
      let crow = committed_map.(row) in
      let krow = pin_map.(row) in
      let (used,freepinned,alloc) = stats.(row) in
      bprintf b "[%d]: (used=%d freepinned=%d alloc=%d)\n"
	row used freepinned alloc;
      for k=0 to  Bitv.length crow-1 do
	let block =
	  Int64.add
	    (Int64.mul (Int64.of_int row) bmaprowsizeL) (Int64.of_int k) in
	if Int64.rem block 64L = 0L then (
	  if k > 0 then bprintf b "\n";
	  bprintf b "%6Ld: " block;
	);
	let c =
	  match Bitv.get crow k, Bitv.get krow k with
	    | false, false -> '_'
	    | false, true  -> 'k'
	    | true,  false -> '*'
	    | true,  true  -> '#' in
	Buffer.add_char b c
      done;
      bprintf b "\n";
    done;
    Buffer.contents b

  method active = active
  method filled = filled
  method id = id
  method identity = identity

  method size = 
    if not active then raise Inactive;
    size

  method fill_from_db idx n =
    let esys = Nn_db.ro_esys() in
    let _e =
    Nn_db.blockalloc_list_ro_e
      ~datastore:id
      ~blkidx_min:idx
      ~blkidx_max:(Int64.add idx (Int64.of_int (n-1)))
      ++ (fun rows ->
	    List.iter
	      (fun (index,data) ->
		 self#fill_blockmap index data
	      )
	      rows;
	    new Uq_engines.epsilon_engine (`Done ()) esys
	 ) in
    Unixqueue.run esys
     

  method fill_blockmap idx data =
    if not active then raise Inactive;
    if idx < 0L || idx >= size || Int64.rem idx bmaprowsizeL <> 0L then
      invalid_arg "fill_blockmap";
    let row = Int64.to_int(Int64.div idx bmaprowsizeL) in
    if row >= size_rows then
      invalid_arg "fill_blockmap";
    let l_expected =
      if row = size_rows-1 then
	n_last
      else
	bmaprowsize in
    if String.length data <> l_expected then
      failwith "fill_blockmap";
    let bv = Bitv.of_string data in
    let n1 = Bitv.fold_left (fun n b -> if b then n+1 else n) 0 bv in
    committed_map.(row) <- bv;
    pin_map.(row) <- Bitv.create l_expected false;
    stats.(row) <- (n1, 0, l_expected-n1);
    alloctab_out_of_date <- true;

  method fill_done() =
    filled <- true

  method private fix_alloctab() =
    if alloctab_out_of_date then (
      n_used <- 0L;
      n_free_pinned <- 0L;
      alloctab <- Array.init (bmaprowsize+1) (fun _ -> IntSet.empty);
      Array.iteri
	(fun row (u,fk,al) ->
	   alloctab.(al) <- IntSet.add row alloctab.(al);
	   n_used <- Int64.add n_used (Int64.of_int u);
	   n_free_pinned <- Int64.add n_free_pinned (Int64.of_int fk);
	)
	stats;
      alloctab_out_of_date <- false
    )

  method stats =
    if not active then raise Inactive;
    self # fix_alloctab();
    let n_trans = n_free_pinned in
    let n_free = Int64.sub (Int64.sub size n_used) n_trans in
    (n_used, n_trans, n_free)

  method has_free_blocks =
    if not active then raise Inactive;
    let (_,_,n_free ) = self#stats in
    n_free > 0L

  method private use_row row =
    LRU_int.add row lru_rows;
    if LRU_int.length lru_rows > 100 then
      ignore(LRU_int.take lru_rows)


  method private get_owner_tuple row ownobj =
    (** Returns 
       [(orow_res, orow_pin, orow_or, orow_and, n_res, n_pin, n_or, n_and)].

	Here, [orow_res] says which blocks have once been reserved during
        the transaction (i.e. blocks are kept here that were first allocated
        and then freed). [orow_res] has negative logic: 0=block is reserved.

	[orow_pin] says which blocks are pinned during the transaction.
	[orow_pin] has positive logic: 1=block is pinned.

        [orow_or] says which blocks are finally allocated (i.e. blocks are
        not in here that were first allocated and then freed in the same
        transaction). [orow_or] has positive logic: 1=block is allocated.

        [orow_and] says which blocks are finally freed (note that you cannot
        undo [free] during a transaction except rolling it back).
        [orow_and] has negative logic: 0=block is freed.

        [n_res], [n_pin], [n_or], [n_and]: Number of blocks in the
        respective bitsets
     *)
    let orow_ht =
      try
	Hashtbl.find owner ownobj
      with
	| Not_found ->
	    let ht = Hashtbl.create 7 in
	    Hashtbl.add owner ownobj ht;
	    ht in
    try Hashtbl.find orow_ht row
    with Not_found ->
      let len = (if row = size_rows-1 then n_last else bmaprowsize) in
      let orow_res = Bitv.create len true in
      let orow_pin = Bitv.create len false in
      let orow_or = Bitv.create len false in
      let orow_and = Bitv.create len true in
      let tuple = (orow_res, orow_pin, orow_or, orow_and,
		   ref 0, ref 0, ref 0, ref 0) in
      Hashtbl.add orow_ht row tuple;
      let rol =
	try Hashtbl.find rowowner row 
	with Not_found -> [] in
      Hashtbl.replace rowowner row (ownobj :: rol);
      tuple

  method private orow_pin ownobj row =  (* may raise Not_found *)
    let orow_ht = Hashtbl.find owner ownobj in
    let (_, orow_pin, _, _, _, _, _, _) = Hashtbl.find orow_ht row in
    orow_pin

  method private reset_owner ownobj =
    (* Just reset everything, only keep the pinned blocks *)
    try
      let orow_ht = Hashtbl.find owner ownobj in
      let orow_ht' = Hashtbl.create (Hashtbl.length orow_ht) in
      Hashtbl.iter
	(fun row (orow_res, orow_pin, orow_or, orow_and, 
		  n_res, n_pin, n_or, n_and) ->
	   let len = (if row = size_rows-1 then n_last else bmaprowsize) in
	   Hashtbl.add
	     orow_ht'
	     row
	     (Bitv.create len true,
	      orow_pin,
	      Bitv.create len false,
	      Bitv.create len true,
	      ref 0,
	      n_pin,
	      ref 0,
	      ref 0
	     )
	)
	orow_ht;
      Hashtbl.replace owner ownobj orow_ht'
    with Not_found -> ()

  method private remove_owner ownobj =
    try
      let orow_ht = Hashtbl.find owner ownobj in
      Hashtbl.iter
	(fun row _ ->
	   let rol = 
	     try Hashtbl.find rowowner row 
	     with Not_found -> [] in
	   let rol' =
	     List.filter (fun o -> o <> ownobj) rol in
	   if rol' <> [] then
	     Hashtbl.replace rowowner row rol'
	   else
	     Hashtbl.remove rowowner row
	)
	orow_ht;
      Hashtbl.remove owner ownobj;
    with
      | Not_found -> ()

  method reserve n ownobj ri =
    if not active then raise Inactive;
    self # fix_alloctab();
    if n <= 0 then
      invalid_arg "reserve";
(*
    if self#n_free < Int64.of_int n then
      failwith "Nn_state.blockmap: not enough free blocks";
 *)
    (** Determine preferred rows (from [ri]) *)
    let bmap_blocks =
      List.filter
	(fun b -> b.Nn_db.ai_identity = identity) 
	ri.ri_blocks in
    let pref_rows =
      List.fold_left 
	(fun acc b ->
	   let row =
	     Int64.to_int(Int64.div b.Nn_db.ai_block bmaprowsizeL) in
	   IntSet.add row acc
	)
	IntSet.empty
	bmap_blocks in
    let pref_rows_l = 
      List.rev(IntSet.fold (fun row acc -> row :: acc) pref_rows []) in
    (** [pref_rows_l] is in reverse order, i.e. we try the most bottom row
       first. Follows a rule of thumb: This is most likely the end of the
       file
     *)
    (** Also allow to allocate from the following rows of [pref_rows] *)
    let pref2_rows =
      IntSet.fold
	(fun row acc ->
	   let row' = row+1 in
	   if row' < size_rows && not (IntSet.mem row' pref_rows) then
	     IntSet.add row' acc
	   else
	     acc
	)
	pref_rows
	IntSet.empty in
    let pref2_rows_l = 
      List.rev(IntSet.fold (fun row acc -> row :: acc) pref2_rows []) in
    (** Loop: While we still need blocks, continue. In [pr] we remember the
	list of preferred rows
     *)
    let pr = ref (pref_rows_l @ pref2_rows_l) in
    let n = ref n in  (* remaining *)
    let blocks = ref [] in
    while !n > 0 do
      let row = ref None in
      (** First try to satisfy the request from the preferred rows *)
      while !row = None && !pr <> [] do
	match !pr with
	  | p_row :: pr' ->
	      let (_,_,n_alloc) = stats.(p_row) in
	      if n_alloc > 0 then (
		let n_here = min n_alloc !n in
		let bl = self # reserve_at p_row n_here ownobj in
		blocks := bl @ !blocks;
		row := Some p_row;
		n := !n - n_here;
	      );
	      pr := pr'
	  | [] -> assert false
      done;

      (** Second strategy: Look for free rows *)
      if !row = None then (
	if alloctab.(bmaprowsize) <> IntSet.empty then (
	  let r = 
	    select_row
	      alloctab.(bmaprowsize) (*ri.ri_inode*) None (fun _ -> false) in
	  let n_here = min bmaprowsize !n in
	  let bl = self # reserve_at r n_here ownobj in
	  blocks := bl @ !blocks;
	  row := Some r;
	  n := !n - n_here;
	)
      );

      (** Third strategy: Get blocks from the rows with the maximum number
	  of free blocks
       *)
      if !row = None then (
	let j = ref (bmaprowsize-1) in
	while !j > 0 && alloctab.( !j ) = IntSet.empty do 
	  decr j
	done;
	if !j > 0 then (
	  let rowset = alloctab.( !j ) in
	  assert (rowset  <> IntSet.empty );
	  let avoid r = LRU_int.mem r lru_rows in
	  (* avoid recently modified rows *)
	  let r = select_row rowset (*ri.ri_inode*) None avoid in
	  let n_here = min !j !n in
	  let bl = self # reserve_at r n_here ownobj in
	  blocks := bl @ !blocks;
	  row := Some r;
	  n := !n - n_here;
	)
      );

      (** Prepare for next round: The next [row] is now preferred *)
      match !row with
	| None -> 
	    (* could not find enough blocks! *)
	    n := 0
	| Some r ->
	    if r+1 < size_rows then
	      pr := [ r+1 ]
	    else
	      pr := []
    done;
    assert(!n = 0);
    List.rev !blocks


  method private reserve_at row n ownobj : triv_range list =
    (** returns blocks in reverse order! *)
    (** Reserve [n] blocks in [row] for [ownobj] *)
    let crow = committed_map.(row) in
    let krow = pin_map.(row) in
    let (orow_res, orow_pin, orow_or, orow_and, 
	 n_res, n_pin, n_or, n_and) =
      self # get_owner_tuple row ownobj in
    let blocks = ref [] in
    let c = ref 0 in
    for k = 0 to Bitv.length crow - 1 do
      if !c < n && not (Bitv.get crow k) && not (Bitv.get krow k) 
      then (
	assert(Bitv.get orow_res k);
	assert(not (Bitv.get orow_or k));
	assert(not (Bitv.get orow_pin k));
	Bitv.set krow k true; (* the block is pinned *)
	Bitv.set orow_res k false;
	Bitv.set orow_pin k true;
	Bitv.set orow_or k true;
	let block = 
	  Int64.add
	    (Int64.mul (Int64.of_int row) bmaprowsizeL) 
	    (Int64.of_int k) in
	( match !blocks with
	    | (last_block, last_n) :: blocks' 
		when block = Int64.add last_block last_n ->
		blocks := (last_block, Int64.succ last_n) :: blocks'
	    | _ ->
		blocks := (block, 1L) :: !blocks
	);
	incr n_or;   (* number of 1 bits in orow_or *)
	incr n_res;  (* number of 0 bits in orow_res *)
	incr n_pin;  (* number of 1 bits in orow_pin *)
	incr c
      )
    done;
    assert(!c = n);
    let (u,fk,al) = stats.(row) in
    let fk' = fk + n in
    let al' = al - n in
    stats.(row) <- (u,fk',al');
    alloctab.( al ) <- IntSet.remove row alloctab.( al );
    alloctab.( al' ) <- IntSet.add row alloctab.( al' );
    n_free_pinned <- Int64.add n_free_pinned (Int64.of_int n);
    self # use_row row;
    !blocks


  method free l ownobj =
    if not active then raise Inactive;
    self # fix_alloctab();
    List.iter
      (fun (block, number) ->
	 let block_end = Int64.add block number in
	 let b = ref block in
	 while !b < block_end do
	   let remaining = Int64.sub block_end !b in
	   let row = Int64.div !b bmaprowsizeL in
	   if row >= size_rows_64 then
	     invalid_arg "Nn_state.blockmap.free";
	   let row_n = Int64.sub bmaprowsizeL (Int64.rem !b bmaprowsizeL) in
	   let n = min remaining row_n in
	   self # free_at (Int64.to_int row) !b n ownobj;
	   b := Int64.add !b n
	 done
      )
      l

  method private free_at row sblock number ownobj =
    (** Free the blocks [sblock] to [sblock+number-1] which are all in [row] *)
    let crow = committed_map.(row) in
    let (orow_res, orow_pin, orow_or, orow_and, 
	 n_res, n_pin, n_or, n_and) =
      self # get_owner_tuple row ownobj in
    self # use_row row;
    let n = ref 0L in
    let k_cursor = ref (Int64.to_int(Int64.rem sblock bmaprowsizeL)) in
    while !n < number do
      let block = Int64.add sblock !n in
      let k = !k_cursor in
      let cok = Bitv.get crow k in
      let rok = Bitv.get orow_or k in
      if not cok && not rok then
	failwith
	  (sprintf 
	     "Nn_state.blockmap.free: cannot free this block (id=%s b=%Ld)"
	     identity block);
      assert(cok <> rok);  (* not both! *)
      if rok then (
	(** Case: We allocated the block earlier in this transaction.
            We keep the block reserved and pinned. No need to modify [orow_and]
            as the block is still free in [committed_map].
	 *)
	assert(not(Bitv.get orow_res k));
	assert(Bitv.get orow_pin k);
	Bitv.set orow_or k false;
	decr n_or;    (* number of 1 bits in orow_or *)
      );
      if cok then (
	(** Case: The block is allocated in the committed view. No
            need to set it reserved as it is allocated anyway.
            Only modify [orow_and] so it will be freed at commit time.
	    Also note that the block is not pinned (but it is possible
	    that it is already pinned).
	 *)
	assert(Bitv.get orow_res k);
	Bitv.set orow_and k false;
	incr n_and;   (* number of 0 bits in orow_and *)
      );
      n := Int64.succ !n;
      incr k_cursor
    done
      (** [free_at] does not change which block is reserved, so
          [alloctab] remains unchanged
       *)
      
  method pin l ownobj =
    if not active then raise Inactive;
    self # fix_alloctab();
    List.iter
      (fun (block, number) ->
	 let block_end = Int64.add block number in
	 let b = ref block in
	 while !b < block_end do
	   let remaining = Int64.sub block_end !b in
	   let row = Int64.div !b bmaprowsizeL in
	   if row >= size_rows_64 then
	     invalid_arg "Nn_state.blockmap.free";
	   let row_n = Int64.sub bmaprowsizeL (Int64.rem !b bmaprowsizeL) in
	   let n = min remaining row_n in
	   self # pin_at (Int64.to_int row) !b n ownobj;
	   b := Int64.add !b n
	 done
      )
      l

  method private pin_at row sblock number ownobj =
    (** Pins the blocks [sblock] to [sblock+number-1] which are all in [row] *)
    let crow = committed_map.(row) in
    let krow = pin_map.(row) in
    let (orow_res, orow_pin, orow_or, orow_and, 
	 n_res, n_pin, n_or, n_and) =
      self # get_owner_tuple row ownobj in
    let n = ref 0L in
    let k_cursor = ref(Int64.to_int(Int64.rem sblock bmaprowsizeL)) in
    while !n < number do
      let block = Int64.add sblock !n in
      let k = !k_cursor in
      let cok = Bitv.get crow k in
      let rok = Bitv.get orow_or k in
      if not cok && not rok then
	failwith
	  (sprintf 
	     "Nn_state.blockmap.pin: cannot pin this block (id=%s b=%Ld) [1]"
	     identity block);
      assert(cok <> rok);  (* not both! *)
      (** Case: We allocated the block earlier in this transaction.
	  It was already pinned at that time.
	  So: [ if rok then <do nothing> ]
	  *)
      if cok then (
	(** Case: The block is allocated in the committed view. We have
	    to set it as pinned. It is an error if it was freed before.
	 *)
	assert(Bitv.get orow_res k);
	if not(Bitv.get orow_and k) then
	  failwith
	    (sprintf 
	       "Nn_state.blockmap.pin: cannot pin this block (id=%s b=%Ld) [2]"
	       identity block);
	if not(Bitv.get orow_pin k) then (
	  Bitv.set orow_pin k true;
	  incr n_pin;   (* number of 1 bits in orow_pin *)
	);
	Bitv.set krow k true
      );
      n := Int64.succ !n;
      incr k_cursor
    done
      (** [pin_at] does not change which block is reserved, so
          [alloctab] remains unchanged
       *)
    
  method get_changes ownobj =
    if not active then raise Inactive;
    self # fix_alloctab();
    let orow_ht_opt =
      try Some(Hashtbl.find owner ownobj)
      with Not_found -> None in
    match orow_ht_opt with
      | None ->
	  []
      | Some orow_ht ->
	  let changes = ref [] in
	  Hashtbl.iter
	    (fun row (orow_res, orow_pin, orow_or, orow_and, 
		      _, _, n_or, n_and) ->
	       if !n_or > 0 || !n_and > 0 then (
		 let crow = committed_map.(row) in
		 let crow' =
		   Bitv.bw_and (Bitv.bw_or crow orow_or) orow_and in
		 let s =
		   Bitv.to_string crow' in
		 let block = Int64.mul (Int64.of_int row) bmaprowsizeL in
		 changes := (block, s) :: !changes
	       )
	    )
	    orow_ht;
	  !changes


  method commit ownobj =
    if not active then raise Inactive;
    self # fix_alloctab();
    let orow_ht_opt =
      try Some(Hashtbl.find owner ownobj)
      with Not_found -> None in
    match orow_ht_opt with
      | None ->
	  ()
      | Some orow_ht ->
	  Hashtbl.iter
	    (fun row (orow_res, orow_pin, orow_or, orow_and, 
		      n_res, n_pin, n_or, n_and) ->
	       let crow = committed_map.(row) in
	       let krow = pin_map.(row) in
	       let (u,fk,al) = stats.(row) in
	       let n = Bitv.length crow in
	       (* new committed row: add the new bits from orow_or, clear
		  bits via orow_and
		*)
	       let crow' = Bitv.bw_and (Bitv.bw_or crow orow_or) orow_and in
	       (* for computing fk' we need this: *)
	       let fkrow' = Bitv.bw_and krow (Bitv.bw_not crow') in
	       (* update stats: *)
	       let fk' = count_ones fkrow' in
	       let u' = count_ones crow'  in
	       let al' = n - u' - fk' in
	       (* store *)
	       committed_map.(row) <- crow';
	       stats.(row) <- (u',fk',al');
	       n_used <- Int64.add n_used (Int64.of_int (u' - u));
	       n_free_pinned <- Int64.add n_free_pinned(Int64.of_int(fk' - fk));
	       alloctab.( al ) <- IntSet.remove row alloctab.( al );
	       alloctab.( al' ) <- IntSet.add row alloctab.( al' );
	    )
	    orow_ht;
	  (* Delete everything from orow_ht except which blocks are pinned: *)
	  self # reset_owner ownobj

  method rollback ownobj =
    if not active then raise Inactive;
    self # fix_alloctab();
    let orow_ht_opt =
      try Some(Hashtbl.find owner ownobj)
      with Not_found -> None in
    match orow_ht_opt with
      | None ->
	  ()
      | Some orow_ht ->
	  (* Drop everything except which blocks are pinned: *)
	  self # reset_owner ownobj

  method release ownobj =
    if not active then raise Inactive;
    self # fix_alloctab();
    let orow_ht_opt =
      try Some(Hashtbl.find owner ownobj)
      with Not_found -> None in
    match orow_ht_opt with
      | None ->
	  ()
      | Some orow_ht ->
	  self # remove_owner ownobj;
	  Hashtbl.iter
	    (fun row (orow_res, orow_pin, orow_or, orow_and, 
		      n_res, n_pin, n_or, n_and) ->
	       let rownlist =
		 try Hashtbl.find rowowner row with Not_found -> [] in
	       let crow = committed_map.(row) in
	       let (u,fk,al) = stats.(row) in
	       let n = Bitv.length crow in
	       (* new pin row: this is the OR of all other orow_pin rows.
		  (Remember we called [remove_owner] already!)
		*)
	       let krow' =
		 List.fold_left
		   (fun acc rownobj ->
		      try Bitv.bw_or acc (self # orow_pin rownobj row)
		      with Not_found -> acc
		   )
		   (Bitv.create n false)
		   rownlist in
	       (* for computing fk' we need this: *)
	       let fkrow' = Bitv.bw_and krow' (Bitv.bw_not crow) in
	       (* update stats: *)
	       let fk' = count_ones fkrow' in
		 (* no better way - there may be several owners freeing the
		    same block
		  *)
	       let al' = n - u - fk' in
	       (* store *)
	       pin_map.(row) <- krow';
	       stats.(row) <- (u,fk',al');
	       n_free_pinned <- Int64.add n_free_pinned(Int64.of_int(fk' - fk));
	       alloctab.( al ) <- IntSet.remove row alloctab.( al );
	       alloctab.( al' ) <- IntSet.add row alloctab.( al' );
	    )
	    orow_ht

  method inactivate() =
    active <- false;
    committed_map <- [| |];
    pin_map <- [| |];
    alloctab <- [| |];
    Hashtbl.clear owner;
    Hashtbl.clear rowowner

end


let blockmap_from_db_e ~id ~identity ~size conn esys =
  let bm = 
    new blockmap ~id ~identity ~size in
  new Uq_engines.seq_engine
    (Nn_db.blockalloc_list_e
       ~datastore:id
       ~blkidx_min:0L
       ~blkidx_max:Int64.max_int
	conn
       esys)
    (fun rows ->
       List.iter
	 (fun (index,data) ->
	    bm#fill_blockmap index data
	 )
	 rows;
       bm # fill_done();
       new Uq_engines.epsilon_engine (`Done bm) esys
    )


let fill_blockmap bmap =
  dlogf "Nn_blockmap.fill_blockmap identity=%s" bmap#identity;
  let chunk_size = Nn_config.bmaprowsize * 100 in
  let idx = ref 0L in
  while !idx < bmap#size do
    bmap # fill_from_db !idx chunk_size;
    idx := Int64.add !idx (Int64.of_int chunk_size)
  done;
  bmap # fill_done();
  dlogf "Nn_blockmap.fill_blockmap done identity=%s" bmap#identity



let string_of_trl trl =
  String.concat ","
    (List.map
       (fun (b_min,n) ->
	  let b_max = Int64.pred (Int64.add b_min n) in
	  sprintf "%Ld-%Ld" b_min b_max
       )
       trl
    )
