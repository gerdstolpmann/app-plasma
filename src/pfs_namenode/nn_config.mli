(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Configuration of name nodes *)

class type nn_node_config =
object
  method nn_clustername : string
    (** The clustername *)
  method nn_hostname : string
    (** The hostname of this node. Must be resolvable to a reasonable
        IP address so that other nodes can reach this node.
     *)
  method nn_nodes : string list
    (** The name nodes in "host:port" syntax *)
  method nn_node_timeout : float
    (** The timeout for connects and RPC calls to name nodes *)
  method nn_node_alive_min : int
    (** How many name nodes need to be alive to continue with operations *)
  method nn_node_alive_min_startup : int
    (** How many name nodes need to be alive at cluster startup *)
  method nn_rank : string
    (** The rank of this node compared to other nodes. At startup time,
        the node with the lowest rank is chosen as coordinator.
        Each node must have a distinct rank. Ranks are compared as in
	[String.compare].
     *)
  method nn_elect_timeout : float
    (** For how many seconds to wait for neighbor namenodes *)
  method nn_inodecaches : string list
    (** Which inodecaches are announced to the clients *)
  method nn_replication : int
    (** Default replication factor *)
  method nn_lock_timeout : float
    (** After these many seconds transactions are not retried on [ECONFLICT] *)
  method nn_pool_size : int
    (** How much pool RAM is allocated (shared memory) *)
  method nn_consistency_checks : bool
    (** Whether to do more consistency checks, even the time-consuming ones *)
  method nn_dump_dir : string
    (** A local directory for data dumps (e.g. checkpoints, logs) *)
  method dn_blocksize : int
    (** Expected blocksize of datanodes *)
  method dn_port : int
    (** The data node port *)
  method dn_node_timeout : float
    (** The timeout for connects and RPC calls to data nodes *)
  method dn_security_level : Pfs_auth.level
    (** The security level the datanodes expect *)
  method dn_discovery_addrs : Unix.inet_addr list
    (** The address to use for discovering datanodes. Normally [[224.0.0.1]],
	i.e. multicasting is enabled for this. 
     *)
  method dn_discovery_period : float
    (** Every how many seconds a new discovery messge is emitted *)
  method dn_multicast_ttl : int
    (** The number of hops (routers+1) for multicasting. Default is 1 *)
  method dn_max_errors : int
    (** For the liveliness monitoring: how many errors (missing response
	messages) are at most acceptable. If more errors occur, the datanode
	is considered as dead.
     *)
end


(** The subset of {!Nn_config.nn_node_config} that is minimally required
    for contacting the namenode. Right now, only the authentication daemon
    uses this.
 *)
class type nn_min_config =
object
  method nn_clustername : string
  method nn_nodes : string list
  method nn_node_timeout : float
end


val extract_node_config : Netplex_types.config_file -> nn_node_config
  (** Config section must look like (see {!Nn_config.nn_node_config}
      for explanations):

      {[
         netplex {
           ...
           namenodes {
             clustername = "cluster1";  (* required *)
             node { addr = "hostname:port" };
             node { addr = "hostname:port" };
             ...
             rank = "0";                (* mandatory *)
             dump_dir = "/var/lib/plasma"; (* mandatory *)
             hostname = "hostname";     (* optional *)
             timeout = 60.0;            (* optional *)
             alive_min = 1;             (* optional *)
             alive_min_startup = 1;     (* optional *)
             elect_timeout 300.0;       (* optional *)
             replication = 3;           (* optional *)
             lock_timeout = 300;        (* optional *)
             pool_size = 268435456;     (* optional *)
           }
           datanodes {
             port = 2728;                      (* mandatory *)
             blocksize = 65536;                (* mandatory *)
             timeout = 60.0;                   (* optional *)
             security_level = "auth";          (* optional *) 
             discovery { addr = "224.0.0.1" }; (* optional *)
             ...
             discovery_period = 1.0;           (* optional *)
             multicast_ttl = 2;                (* optional *)
             max_errors = 1;                   (* optional *)
           }
           ...
         }
      ]}

      The parameter [rank] can also be given by running a script that
      outputs the rank to stdout. This script must be given as 
      parameter [rank_script].

      Instead of describing the nodes directly in the config file
      using [node] subsections, one can also point to external file:

      {[ 
         node_list = "filename";
         port = <default_port>;
      ]}

      These two parameters can replace the [node] subsections for
      [namenodes]. The external file enumerates the hosts, one
      hostname a line (with shell-style comments).

      Special values:
      - [alive_min] and [alive_min_startup] can also be set to "all"
        (in double quotes). This means all configured name nodes must
        be available
   *)

(*
      - [replication] can also be set to "max" (in double quotes). This
        means that the replication is set to the number of data nodes.
 *)

val extract_min_config : Netplex_types.config_file -> nn_min_config
  (** Config section must look like (see {!Nn_config.nn_node_config}
      for explanations):

      {[
         netplex {
           ...
           namenodes {
             clustername = "cluster1";  (* required *)
             node { addr = "hostname:port" };
             node { addr = "hostname:port" };
             ...
             timeout = 60.0;            (* optional *)
           }
         }
      ]}
   *)


val bmaprowsize : int
  (** block map row size - how many bits are in one row of the block map.
      Changing this value also affects the database.
   *)
