(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

 *)
(* $Id$ *)

(** Namenode component *)

open Uq_engines.Operators

let dlogf = Plasma_util.dlogf

module Bool_var =
  Netplex_cenv.Make_var_type(struct type t = bool end)


let monitor_factory db_conf nn_conf cauth =
  Rpc_netplex.rpc_factory
    ~configure:(fun cf _ -> ())
    ~hooks:(fun conf ->
      ( object
        inherit Netplex_kit.empty_processor_hooks()
	  
        method post_add_hook socksrv ctrl =
	  Nn_monitor.post_add_hook socksrv ctrl

	method post_start_hook cont =
	  let esys = (Netplex_cenv.self_cont()) # event_system in
	  Nn_db.init db_conf esys;
	  Nn_monitor.post_start_hook cont nn_conf cauth

	method receive_message cont msg args =
	  Nn_monitor.receive_message cauth cont msg args

	method shutdown() =
	  Nn_monitor.shutdown()

	end
      )
    )
    ~name:"nn_monitor"
    ~setup:(fun _ _ -> ()  (* TODO *) )
    ()


let add_monitor_service (cf, cfaddr, db_conf, nn_conf, cauth) socksrv ctrl =
  (* FIXME: There should be better functions for creating these objects in
     Netplex
   *)
  let sockpath =
    ctrl#controller_config#socket_directory ^ "/nn_monitor.rpc" in
  let sockaddr =
    `Socket (Unix.ADDR_UNIX sockpath) in
  let protocol : Netplex_types.protocol =
    Netplex_kit.create_protocol
      "RPC" [| sockaddr |] in
  let socksrv_conf : Netplex_types.socket_service_config =
    Netplex_kit.create_socket_service_config
      "Nn_monitor" [protocol] ctrl#controller_config in
  let facobj = monitor_factory db_conf nn_conf cauth in
  let processor = 
    facobj # create_processor 
      ctrl#controller_config 
      cf cfaddr in
  let socksrv = 
    Netplex_sockserv.create_socket_service
      processor
      socksrv_conf in
  let wm = 
    Netplex_workload.create_constant_workload_manager
      1 in
  ctrl # add_service socksrv wm


let extract_port cont =
  let sock_serv = cont # socket_service # socket_service_config in
  let proto =
    try List.find (fun p -> p#name = "RPC") sock_serv#protocols
    with Not_found ->
      failwith "Cannot find protocol 'RPC'" in
  let addrs = Array.to_list proto#addresses in
  try
    let addr = 
      List.find
	(function
	  | `Socket addr ->
	    ( match addr with
	      | Unix.ADDR_INET(_,p) -> true
	      | _ -> false
	    )
	  | _ ->
	    false
	)
	addrs in
    match addr with
      | `Socket(Unix.ADDR_INET(_,p)) -> p
      | _ -> assert false
  with
    | Not_found ->
      failwith "Cannot determine my own canonical port"


let read_ug_admin() =
  let esys = Unixqueue.create_unix_event_system() in
  let t = Nn_db.transact esys 0 in  (* FIXME: commit_id *)
  let e = Nn_db.read_ug_admin_e t#connection esys in
  Uq_engines.when_state  (* OK *)
    ~is_error:(fun e ->
      dlogf
	"Nn_manager.read_ug_admin: Exception %s"
	(Netexn.to_string e))
    e;
  Unixqueue.run e#event_system;
  t#return();
  match e#state with
    | `Done v -> v
    | `Error e -> raise e
    | `Aborted -> failwith "Aborted"
    | `Working _ -> assert false


module Unit_encap = 
  Netplex_encap.Make_encap(struct type t = unit end)


let bm_process =
  (* this needs to be Lazy.forced when the controller is up *)
  lazy(
    Netmcore_process.def_process
      (fun db_conf ->
	let esys = (* (Netplex_cenv.self_cont()) # event_system *)
	  Unixqueue.create_unix_event_system() in
	Nn_db.init db_conf esys;
	Nn_blockmap_box.serve()
      )
  )

let bm_fork() = fst(Lazy.force bm_process)

let bm_join() = snd(Lazy.force bm_process)

let bm_started = ref false

let role_setup_done = ref false

let factory() =
  Rpc_netplex.rpc_factory
    ~configure:(fun cf cfaddr -> 
      let db_conf = Pfs_db.extract_db_config cf in
      let nn_conf = Nn_config.extract_node_config cf in
      let access = Pfs_auth.extract_access_config cf cf#root_addr in
      let cauth = Pfs_auth.extract_client_config cf cf#root_addr in
      let pool =
	Netmcore_mempool.create_mempool nn_conf#nn_pool_size in
      Pfs_pmanage.register_shm
	(Netmcore_mempool.shm_name pool);
      (cf, cfaddr, db_conf, nn_conf, pool, access, cauth)
    )
    ~hooks:(fun (cf, cfaddr, db_conf, nn_conf, pool, access, cauth) ->
      ( object
	inherit Netplex_kit.empty_processor_hooks()

	method post_add_hook socksrv ctrl =
	  Netmcore.add_plugins ctrl;
	  ctrl # add_plugin Netplex_sharedvar.plugin;
	  Nn_datastore_news.post_add_hook socksrv ctrl;
	  add_monitor_service
	    (cf, cfaddr, db_conf, nn_conf, cauth) socksrv ctrl;
		  (** Note that the monitor is only started on the
                      coordinator node. We don't know this node yet.
		   *)

	method pre_start_hook _ _ _ =
	  ignore(Lazy.force bm_process);
	  if not !bm_started then (
            (* Start the blockmap server: *)
            let _bm_pid = 
	      Netmcore_process.start
		~inherit_resources:`All (* because of pool *)
		(bm_fork()) db_conf in
	    Netmcore_process.release_join_point (bm_join());
	    bm_started := true
	  )

	method post_start_hook cont =
	  (** This can now be run several times!
	   *)
          try
	    let count =
	      Netplex_semaphore.increment
		"plasma.namenode.manager.proc_count" in
	    let first_process = (count=1L) in
	    (* If first_process this is the first created process.
	       It has to do some additional things.
	     *)
	    let esys = (Netplex_cenv.self_cont()) # event_system in
	    let port = extract_port cont in
	    Nn_db.init db_conf esys;
	    if first_process then
	      Nn_db.commit_all_prepared_transactions();
	    dlogf
	      "Waiting for datastore news";
	    ignore(Nn_datastore_news.list ~wait:true ());
	    (* TODO: don't wait here *)
	    dlogf
	      "Datastore news feed is ready";
	    Nn_ug.post_start_hook pool;
	    if first_process then (
	      let ug = read_ug_admin() in
	      Nn_ug.set_ug_files ~init:true ug
	    );
	    Nn_authticket.post_start_hook pool;
	    Nn_inodecache.post_start_hook pool;
	    Nn_dentrycache.post_start_hook pool;
	    Nn_slave.post_start_hook cont;
	    Nn_db.ds_cache_init `From_datastore_news_channel;
            if first_process then
              Nn_sync.init();
	    (** Start the election procedure: *)
	    if first_process then (
              let ee = Nn_elect.elect_e nn_conf port cauth esys in
	      Uq_engines.when_state (* OK *)
		~is_done:(
		  fun (coord_ip_opt,r) ->
		    match coord_ip_opt with
	  	      | Some i ->
				  (* become master *)
				  (* it is no problem when Nn_coord's post
				     start hook is called several times
				   *)
			Nn_coord.post_start_hook
			  cont nn_conf cauth pool i r;
			let _e =
			  Nn_coord.wait_until_slaves_are_ready_e()
			  ++ (fun () ->
			    Nn_coord.revision_transaction_e())
			  ++ (fun () ->
			    Nn_coord.activate_timers();
			    cont # send_message
			      cont#socket_service_name
			      "plasma.namenode.manager.start_master"
			      [| string_of_int i; r |];
			    eps_e (`Done()) esys
			  ) in
			()

		      | None ->
			(* become slave *)
			cont # send_message
			  cont#socket_service_name
			  "plasma.namenode.manager.start_slave"
			  [| |]
		)
		ee;
	    )
          with
            | error ->
              let bt = Printexc.get_backtrace() in
              cont#log `Crit ("Exception: " ^ (Netexn.to_string error));
              cont#log `Crit ("Backtrace: " ^ bt);

		  (** Debug: dump access counts of shared vars now and then *)
		  (*
		    let _t =
		    Netplex_cenv.create_timer
		    (fun _t ->
		    Netplex_sharedvar.dump "*" `Debug;
		    true
		    ) 60.0 in
		    ()
		   *)

	method receive_message cont msg args =
	  dlogf "Msg: %s" msg;
	  match msg with
	    | "plasma.namenode.manager.start_master" ->
	      let i = int_of_string args.(0) in
	      let r = args.(1) in
	      dlogf "Nn_datanode_ctrl.post_start_hook";
	      Nn_datanode_ctrl.post_start_hook pool cauth;
	      dlogf "Nn_datanode_ctrl.reset()";
	      Nn_datanode_ctrl.reset();
	      dlogf "Nn_coord.post_start_hook";
	      Nn_coord.post_start_hook cont nn_conf cauth pool i r;
	      let r = Nn_elect.get_role() in
	      assert(r = `Coordinator);
	      role_setup_done := true;
	      dlogf "Coordinator process initialized!";

	    | "plasma.namenode.manager.start_slave" ->
	      let r = Nn_elect.get_role() in
	      assert(r <> `Coordinator);
	      role_setup_done := true;
	      dlogf "Slave process initialized!";

	    | n when n = Nn_elect.stop_election_msg ->
	      Nn_elect.stop_election()

	    | _ ->
	      Nn_datastore_news.receive_message cont msg args

	method system_shutdown() =
	  dlogf "System shutdown";
	  let count =
	    Netplex_semaphore.increment
	      "plasma.namenode.manager.shutdown_count" in
	  let first_process = (count=1L) in
	  if first_process then (
		      (* A second attempt would hang *)
	    let c = Nn_blockmap_box.client() in
	    c#shutdown();
	    c#close();
	  );
	  dlogf "Done system shutdown";

	method shutdown() =
	  dlogf "Shutdown"

	end
      )
    )
    ~setup:(fun srv (cf, cfaddr, db_conf, nn_conf, pool, access, cauth) ->
      Pfs_auth.configure_rpc_server srv access;
      Nn_elect.setup_elect_api srv nn_conf;
      let r = Nn_elect.get_role() in
      dlogf "setup role=%s role_setup=%B" 
	(Nn_elect.string_of_role r) !role_setup_done;
      if !role_setup_done then
	match r with
	  | `Coordinator ->
	    Nn_coord.setup srv nn_conf
	  | `Slave ->
	    Nn_slave.setup srv nn_conf
	  | _ ->
	    ()
    )
    ~name:"nn_manager"
    ()
