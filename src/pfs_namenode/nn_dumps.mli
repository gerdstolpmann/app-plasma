(*
  Copyright 2013 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Dump file management

Dump files are stored in the local file system in the [config#nn_dump_dir]
directory. Dump files are used for checkpoints and for log data
(transaction deltas).

In the [config#nn_dump_dir] directory every dump gets its own subdirectory.
These subdirectories have names of the form <prefix>_<ref>_<suffix> where

 - <prefix> denotes the kind of dump ("checkpoint", "log")
 - <ref> is the name of the dump, see below
 - <suffix> makes the directory name unique if there are several instances
   for the same ref

For checkpoints:
 - <prefix> is always "checkpoint"
 - <ref> is the timestamp in the format "YYYYMMDDHHMMSS"

For log files:
 - <prefix> is always "log"
 - <ref> is the <ref>_<suffix> string of the checkpoint the log files
   refer to

Examples:
 - checkpoint_20130604145547_0000
 - log_20130604145547_0000_0001

The dump directories are only considered valid if they contain a file
"dump_succeeded".

 *)

class type dump_config = 
object 
  method nn_dump_dir : string
end

type dump_entry =
    { dump_prefix : string;
      dump_ref : string;
      dump_suffix : string;
      dump_finished : bool;
      dump_timestamp : float
    }

val create_dump : #dump_config -> (string * string) -> dump_entry
  (** [create_dump conf (prefix, ref)]: Creates a new dump directory
      and returns the entry.
   *)

val finish_dump : #dump_config -> dump_entry -> dump_entry
  (** [finish_dump conf entry]: creates the "dump_succeeded" file
      and returns the updated entry (new timestamp, finished=true).
   *)

val list_dumps : #dump_config -> dump_entry list
  (** List all dump directories (including unfinished ones) *)

val dump_exists : #dump_config -> dump_entry -> bool
  (** Whether the dump directory exists *)

val dump_directory : #dump_config -> dump_entry -> string
  (** Return the absolute path to this dump *)

val get_dump_files : #dump_config -> dump_entry -> (string * int64) list
  (** Returns the files in the dump directory, together with the file
      size in bytes. The file "dump_succeeded" is not returned by this
      function.

      Files starting with a dot or ending in ~ are also not returned.
   *)

val remove_dump : #dump_config -> dump_entry -> unit
  (** Deletes a dump directory *)

val equal : dump_entry -> dump_entry -> bool
  (** whether both entries are about the same dump *)

val create_token : (string * string * string) -> string
val dest_token : string -> (string * string * string)
