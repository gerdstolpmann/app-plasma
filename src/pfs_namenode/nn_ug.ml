(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* This is put into shared memory *)
type ug_shared =
    { mutable mutex : Netmcore_mutex.mutex;
      (** Protects accesses to this record *)

      mutable ug_tables : (string * string) list;
      (** The current value *)

      mutable version : int;
      (** The version number of this value *)
    }

(* This is put into local memory *)
type ug_local =
    { mutable ug_local : Plasma_ug.ug_admin_ro option;
      (** The last value copied over to local memory *)

      mutable ug_files : (string * string) list;
      (** The unparsed files *)

      mutable version_local : int;
      (** The version of ug_local *)

      shared : ug_shared Netmcore_ref.sref;
      (** Pointer to ug_shared *)
    }

module Ug_local_var =
  Netplex_cenv.Make_var_type(struct type t = ug_local end)

let ug_local_name = "current_ug"

module Shared_var =
  Netplex_sharedvar.Make_var_type
    (struct type t = ug_shared Netmcore_ref.sref_descr end )

let shared_var_name = "plasma.namenode.ug.shm"


let dlogf = Plasma_util.dlogf


let get_ug() =
  let ug = Ug_local_var.get ug_local_name in
  let need_update =
    Netmcore_ref.deref_p ug.shared
      (fun sh -> 
	 Netmcore_mutex.lock sh.mutex;
	 if sh.version > ug.version_local then (
	   let tabs = Netmcore_heap.copy sh.ug_tables in
	   let uga = Plasma_ug.parse_ug_admin tabs in
	   ug.ug_local <- Some (uga :> Plasma_ug.ug_admin_ro);
	   ug.ug_files <- tabs;
	   ug.version_local <- sh.version;
	   Netmcore_mutex.unlock sh.mutex;
	   true
	 )
	 else (
	   Netmcore_mutex.unlock sh.mutex;
	   false
	 )
      ) in
  if need_update then
    dlogf "Nn_ug: updating local version to %d" ug.version_local;
  match ug.ug_local with
    | None ->
	failwith "Nn_ug.get_ug_admin: not set"
    | Some x ->
	(x, ug.ug_files)


let get_ug_admin() =
  fst(get_ug())


let get_ug_files() =
  snd(get_ug())


let set_ug_files ?(init=false) files =
  let ug = Ug_local_var.get ug_local_name in
  let v_opt =
    Netmcore_heap.modify
      (Netmcore_ref.heap ug.shared)
      (fun mut ->
	 let sh = Netmcore_ref.deref_ro ug.shared in
	 Netmcore_mutex.lock sh.mutex;
	 let v_opt =
	   if not init || sh.ug_tables = [] then (
	     sh.ug_tables <- Netmcore_heap.add mut files;
	     sh.version <- sh.version+1;
	     Some sh.version
	   )
	   else None in
	 Netmcore_mutex.unlock sh.mutex;
	 v_opt
      ) in
  match v_opt with
    | None -> failwith "Nn_ug.set_ug_admin: init exception"
    | Some v ->
	let ug_obj = Plasma_ug.parse_ug_admin files in
	ug.ug_local <- Some (ug_obj :> Plasma_ug.ug_admin_ro);
	ug.ug_files <- files;
	ug.version_local <- v;
	dlogf "Nn_ug: updating global+local version to %d" ug.version_local


let post_start_hook pool =
  let shm =
    if Netplex_sharedvar.create_var ~enc:true shared_var_name then (
      let s0 =
        { mutex = Netmcore_mutex.dummy();
	  ug_tables = [];
	  version = 0;
        } in
      let shm = Netmcore_ref.sref pool s0 in
      Netmcore_heap.modify
        (Netmcore_ref.heap shm)
        (fun mut ->
           let s = Netmcore_ref.deref_ro shm in
           s.mutex <- Netmcore_mutex.create mut `Normal
        );
      let descr = Netmcore_ref.descr_of_sref shm in
      Shared_var.set shared_var_name descr;
      shm
    )
    else (
      ignore(Netplex_sharedvar.wait_for_enc_value shared_var_name);
      let descr = Shared_var.get shared_var_name in
      Netmcore_ref.sref_of_descr pool descr
    ) in
  
  Ug_local_var.set ug_local_name
    { ug_local = None;
      ug_files = [];
      version_local = 0;
      shared = shm
    }
