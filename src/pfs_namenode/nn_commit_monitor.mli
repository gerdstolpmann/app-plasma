(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** This monitor ensures that only certain commits can be done in parallel.
    The model is as follows:
    - Each worker process serializes its own commits
    - The commits done by two worker commits can usually run in parallel

    The criterion whether two commits c1 and c2 can run in parallel:
    - Only one of the commits may modify the datastores table
    - Only one of the commits may modify data related to the same 
      inode (with the exception [inode_upd_time] and the exceptions
      directory names)
    - Only one of the commits may modify the same block allocation row

    The updates done by [inode_upd_time] are ignored here - this is only
    used for updating the mtime of directories if a file is added or
    deleted. (Or better: The DB will implicitly serialize these updates.)
 *)

module I64Set : Set.S with type elt = int64

module BRowSet : Set.S with type elt = int * int64 (* ds_id, index *)

type update_characteristics =
    { upd_ds : bool;            (** whether datastores are updated *)
      upd_inodes : I64Set.t;    (** the modified inodes *)
      upd_blocks : BRowSet.t;   (** the modified block rows *)
    }

type commit_monitor
  (** The process-local part of the monitor *)

type commit_monitor_shm
  (** The shm part of the monitor *)

val create_commit_monitor_heap :
       Netmcore.res_id -> commit_monitor_shm Netmcore_ref.sref

val create_commit_monitor :
        Netmcore.res_id -> commit_monitor_shm Netmcore_ref.sref -> 
        Unixqueue.event_system ->
           commit_monitor

val monitor_e : 
       commit_monitor ->
       update_characteristics ->
       (int -> 'a Uq_engines.engine) ->
         'a Uq_engines.engine
  (** [monitor_e cm uc f]:
      Tries to get a slot for doing a commit described by [uc]. When the
      commit can be done, [f commit_id] is called, and the result is
      returned.
   *)

val get_update_characteristics : 
       Nn_db.modification Queue.t list -> update_characteristics
  (** Extract [uc] from a queue of DB updates *)
