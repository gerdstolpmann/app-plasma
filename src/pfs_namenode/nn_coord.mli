(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** The implementation of the namenode component for the coordinator
    node

    This is a multi-processing component supporting more than one
    process.
 *)

val post_start_hook : 
      Netplex_types.container -> Nn_config.nn_node_config -> 
      Pfs_auth.client_auth -> Netmcore.res_id -> int -> string -> 
        unit
  (** [post_start_hook cont conf cauth pool master_index transaction_revision] *)


val activate_timers : unit -> unit
  (** Called when we can run transactions to enable the housekeeping 
      timers. This fn must only be called in one of the worker processes.
   *)

val setup : 
      Rpc_server.t -> Nn_config.nn_node_config -> unit


val wait_until_slaves_are_ready_e : unit -> unit Uq_engines.engine
  (** Wait until the slaves respond *)

val revision_transaction_e : unit -> unit Uq_engines.engine
  (** This initial transaction clears all revisions. As a side effect,
     it is also tested whether the slaves can really commit.
   *)
