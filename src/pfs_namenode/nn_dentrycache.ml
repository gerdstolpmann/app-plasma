(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** The inodecache checks whether inode metadata is still up to date.
    It requests notifications from the main nameserver.
 *)

open Pfs_rpcapi_aux
open Plasma_util.Operators
open Printf

let dlog = Plasma_util.dlog
let dlogr = Plasma_util.dlogr

let max_size = 10000
  (* FIXME: configurable *)

type header =
    { mutable mutex : Netmcore_mutex.mutex
    }

(* This is "easy going LRU". We add lookups and updates to the new generation,
   and when the new gen becomes too long, we oldify it. At this opportunity
   the previous old gen is deleted from the cache.
 *)

type dentrycache_local =
    { de_shm : (int64 * string, int64, header) Netmcore_hashtbl.t;
        (* maps (dir_inode,name) to inode *)
      old_gen_shm : (int64 * string, unit) Netmcore_queue.squeue;
        (* the old generation *)
      new_gen_shm : (int64 * string, unit) Netmcore_queue.squeue;
        (* the new generation *)
    }

type dentrycache_descr =
    { de_descr : (int64 * string, int64, header) Netmcore_hashtbl.t_descr;
      old_gen_descr : (int64 * string, unit) Netmcore_queue.squeue_descr;
      new_gen_descr : (int64 * string, unit) Netmcore_queue.squeue_descr;
    }

module Dentrycache_local_var =
  Netplex_cenv.Make_var_type(struct type t = dentrycache_local end)

let dentrycache_local_name = "current_dentrycache"

module Dentrycache_descr_var =
  Netplex_sharedvar.Make_var_type(struct type t = dentrycache_descr end)

let dentrycache_descr_name = "plasma.namenode.dentrycache.shm"

let get_dentrycache() =
  Dentrycache_local_var.get dentrycache_local_name

let protect dc f =
  (* Catch the case Out_of_pool_memory. We react by dropping the
     cache entirely
   *)
  try f()
  with
    | Netmcore_mempool.Out_of_pool_memory ->
	Netlog.logf `Warning
	  "Nn_dentrycache: out of pool memory. Clearing cache!";
	Netmcore_hashtbl.clear dc.de_shm;
	Netmcore_queue.clear dc.old_gen_shm;
	Netmcore_queue.clear dc.new_gen_shm

let reorganize dc =
  let ql = Netmcore_queue.length dc.new_gen_shm in
  if ql >= max_size then (
    (* Reorganize. Basically, we remove all elements from the cache
       that occur in old gen but not in new gen. Also, the old gen
       is dropped and replaced by new gen. Unfortunately, we cannot
       just move the queues, because this would imply an update of
       the descriptors.
     *)
    (* Bug in Ocamlnet-3.4.1: Netmcore_queue.iter does not work *)
    let old_size = Netmcore_hashtbl.length dc.de_shm in
    let new_gen_ht = Hashtbl.create max_size in

    while not (Netmcore_queue.is_empty dc.new_gen_shm) do
      Netmcore_queue.pop_p dc.new_gen_shm
	(fun x ->
	   if not (Hashtbl.mem new_gen_ht x) then
	     Hashtbl.add new_gen_ht (Netmcore_heap.copy x) ()
	)
    done;
    while not (Netmcore_queue.is_empty dc.old_gen_shm) do
      Netmcore_queue.pop_p dc.old_gen_shm
	(fun x ->
	   if not (Hashtbl.mem new_gen_ht x) then (
	     Netmcore_hashtbl.remove dc.de_shm x
	   )
	)
    done;
    Netmcore_queue.clear dc.old_gen_shm;
    Netmcore_queue.clear dc.new_gen_shm;
    Hashtbl.iter
      (fun x _ ->
	 Netmcore_queue.push x dc.old_gen_shm
      )
      new_gen_ht;
    let new_size = Netmcore_hashtbl.length dc.de_shm in
    Netlog.logf `Info
      "Dentrycache: garbage collection after %d additions. Shrinking from %d \
       to %d entries"
      ql old_size new_size;
  )
  

let lookup dir_inode name =
  let dc = get_dentrycache() in
  let hdr = Netmcore_hashtbl.header dc.de_shm in
  Netmcore_mutex.lock hdr.mutex;
  ( try
      let file_inode = Netmcore_hashtbl.find_c dc.de_shm (dir_inode,name) in 
              (* or Not_found *)
      (* Move this entry to the youngest position: *)
      protect dc
	(fun () ->
	   Netmcore_queue.push (dir_inode,name) dc.new_gen_shm;
	   reorganize dc
	);
      Netmcore_mutex.unlock hdr.mutex;
      file_inode
    with Not_found ->
      Netmcore_mutex.unlock hdr.mutex;
      raise Not_found
  )


let update dir_inode name file_inode =
  let dc = get_dentrycache() in
  let hdr = Netmcore_hashtbl.header dc.de_shm in
  Netmcore_mutex.lock hdr.mutex;
  protect dc
    (fun () ->
       Netmcore_hashtbl.replace dc.de_shm (dir_inode,name) file_inode;
       Netmcore_queue.push (dir_inode,name) dc.new_gen_shm;
       reorganize dc
    );
  Netmcore_mutex.unlock hdr.mutex


let delete dir_inode name =
  let dc = get_dentrycache() in
  let hdr = Netmcore_hashtbl.header dc.de_shm in
  Netmcore_mutex.lock hdr.mutex;
  protect dc
    (fun () ->
       Netmcore_hashtbl.remove dc.de_shm (dir_inode,name)
    );
  Netmcore_mutex.unlock hdr.mutex


let post_start_hook pool =
  let local =
    if Netplex_sharedvar.create_var ~enc:true dentrycache_descr_name then (
      let h0 =
        { mutex = Netmcore_mutex.dummy();
        } in
      let de_shm = Netmcore_hashtbl.create pool h0 in
      Netmcore_heap.modify
        (Netmcore_hashtbl.heap de_shm)
        (fun mut ->
           let h = Netmcore_hashtbl.header de_shm in
           h.mutex <- Netmcore_mutex.create mut `Normal
        );
      let de_descr = Netmcore_hashtbl.descr_of_hashtbl de_shm in
      let old_gen_shm = Netmcore_queue.create pool () in
      let old_gen_descr = Netmcore_queue.descr_of_squeue old_gen_shm in
      let new_gen_shm = Netmcore_queue.create pool () in
      let new_gen_descr = Netmcore_queue.descr_of_squeue new_gen_shm in
      Dentrycache_descr_var.set dentrycache_descr_name
	{ de_descr;
	  old_gen_descr;
	  new_gen_descr;
	};
      let local =
	{ de_shm;
	  old_gen_shm;
	  new_gen_shm;
	} in
      local
    )
    else (
      ignore(Netplex_sharedvar.wait_for_enc_value dentrycache_descr_name);
      let descr = Dentrycache_descr_var.get dentrycache_descr_name in
      let local =
	{ de_shm = Netmcore_hashtbl.hashtbl_of_descr pool descr.de_descr;
	  old_gen_shm = 
	    Netmcore_queue.squeue_of_descr pool descr.old_gen_descr;
	  new_gen_shm =
	    Netmcore_queue.squeue_of_descr pool descr.new_gen_descr;
	} in
      local
    ) in
  Dentrycache_local_var.set dentrycache_local_name local
