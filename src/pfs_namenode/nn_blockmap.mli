(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

exception Inactive


type reserve_info =
    { ri_blocks : Nn_db.allocinfo list;  
      (** Already allocated blocks of this inode. This list should only
	  include blocks of the range immediately preceding the newly
	  allocated range. The algorithm looks then for free blocks in the
	  vicinity.
       *)
    }
   (** Optional information for block reservations *)

val create_reserve_info : 
      ?blocks:Nn_db.allocinfo list ->
      unit -> reserve_info
  (** Creates a [reserve_info] record *)


type triv_range = int64 * int64
    (** (startblock, number) *)

val string_of_trl : triv_range list -> string
  (** For debugging *)


type owner

val create_owner : unit -> owner
  (** The owner is an opaque ID which is guaranteed to be unique in the
      whole Netplex system
   *)

val string_of_owner : owner -> string
  (** debug string *)

(** block maps are in-memory representation of the per-datastore bitmaps
    whether blocks are used or free. Changes to block maps are done in a
    transactional way. One can finally get the accumulated updates of
    a transaction for writing them out to the db
 *)
class type blockmap_t =
object
  method active : bool
    (** whether this object refers to an existing row in the [datastore]
        table. If the object is inactive, the other methods (except [id]
        and [identity]) will fail with [Inactive].
     *)
  method filled : bool
    (** whether the object is completely filled with data *)
  method id : int
    (** the datastore db table id *)
  method identity : string
    (** This is the blockmap for this datastore identity *)
  method size : int64
    (** Size of the blockmap in blocks *)
  method stats : int64 * int64 * int64
    (** Number of used, transitional, free blocks *)
  method fill_blockmap : int64 -> string -> unit
    (** [fill_blockmap index data]: Fills the blockmap at [index]
        with [data], a string consisting of 0 and 1. [index] must be
        a multiple of 1024. (This is only allowed once per row for init,
        and is outside any transaction.)
     *)
  method fill_from_db : int64 -> int -> unit
    (** [fill_from_db index n]: Fills the [n] rows at [index] from the
	db (synchronously)
     *)
  method fill_done : unit -> unit
    (** Set that the map is filled, i.e. [filled] returns true *)
  method has_free_blocks : bool
    (** whether there are still free blocks *)
  method reserve : int -> owner -> reserve_info -> triv_range list
    (** [reserve n owner ri]: finds [n] free blocks and returns their indices.
        The reservation is owned by [owner], an arbitrary object.
	Reserved blocks are turned into used blocks at commit time,
	and freed again on [abort]. Reserved blocks are implicitly also
	pinned.

	If not enough blocks are free, it is tried to reserve as much as
	possible. The return value [[]] means that no more blocks are
	available.

	Requirement: [n>0], [n<=bmaprowsize]
     *)
  method free : triv_range list -> owner -> unit
    (** [free l owner]: deallocates these blocks. If they are still
        only reserved but not yet committed, they keep their reserved
        status until commit time, and are freed only then. It is
	allowed to free pinned blocks (but it is not allowed to
	first free and then pin blocks).
     *)
  method pin : triv_range list -> owner -> unit
    (** [pin l owner]: marks these blocks as "to keep" even if a 
	competing transaction frees them. Pinned blocks are a special
	form of transitional blocks. Basically, a block is pinned when
	metadata about it is handed out to the client, so the client
	can be sure that the block keeps it current contents for the
	time of the pinning. The pinning is removed when the datanode
	access ticket is cancelled. As long as the pinning lasts the
	contents of the block are not modified (except in the case
	when the client requests new blocks and fills them), i.e.
	pinning prevents that the block is allocated for something else.

	It is allowed that several owners pin the same block. In this
	case the block remains pinned until all owners unpin the block.

	Reason 1 for pinning: Transaction X wants to read the block.
	Transaction Y frees it at the same time, and commits this.
	Now we have to ensure that X can still read the block although
	the competing transaction has deleted it.

	Reason 2: Transaction X wants to circumvent the security system.
	X gets a ticket for reading X, frees the block and commits.
	Now Transaction Y allocates a new block for a different file,
	and this could be the same block if it were not pinned until the
	end of the lifetime of the ticket. This would allow a security
	violation by X because X could read the block just written by Y
	with its ticket.

	Pinned blocks are not entered into the db, so it is not required
	to [get_changes].
     *)
  method get_changes : owner -> (int64 * string) list
    (** [get_changes owner]: Returns the changes made by [owner].
        This is a list of database rows ([start_index,row_data]),
        to be used for SQL [UPDATE]s. If [owner] is unknown, the empty
        list is returned.

	This method must be called before [commit], [rollback] or [release].
     *)
  method commit : owner -> unit
    (** [commit owner]: commits [owner]'s reservations, and makes them
        permanent. If there are pinned blocks, these blocks remain pinned
	for this owner.

	If owner is unknown this is a no-op.
     *)
  method rollback : owner -> unit
    (** [rollback owner]: unreserves [owner]'s blocks. This does not
	remove the block pinnings, though.

	If owner is unknown this is a no-op.
     *)
  method release : owner -> unit
    (** [release owner]: Unpins the blocks of this owner. Effectively,
	everything about [owner] is deleted in the blockmap. Note that
	we have OR logic here: a block is considered as pinned as long
	as there is at least one pinning owner.
     *)
  method inactivate : unit -> unit
    (** Sets [active] to [false] *)
  method dump : string
    (** Get a dump of the internal state *)
end


class blockmap : id:int -> identity:string -> size:int64 -> blockmap_t
  (** Create a blockmap for DB row [id] and datastore [identity],
      with an initial [size]. All blocks are free in the created blockmap.
   *)

val blockmap_from_db_e : id:int -> identity:string -> size:int64 -> 
                         Pfs_db.ro_async_connection ->
                         Unixqueue.event_system ->
                           blockmap_t Uq_engines.engine
  (** Create a blockmap object, and initialize it from the [blockalloc]
      db table
   *)

val fill_blockmap : blockmap_t -> unit
  (** Fills the blockmap from db (synchronous) *)
