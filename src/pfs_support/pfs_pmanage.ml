(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

let pmanage_file () =
  let socket_dir =
    try
      match Netplex_cenv.self_obj() with
	| `Container cont ->
	    cont # socket_service # socket_service_config # 
	      controller_config # socket_directory
	| `Controller ctrl ->
	    ctrl # controller_config # socket_directory
    with
      | Not_found ->
	  failwith "Pfs_pmanage: called outside a Netplex context" in
  Filename.concat socket_dir "pmanage"

let read fd =
  let f = Unix.in_channel_of_descr fd in
  let l = ref [] in
  try
    while true do
      let line = input_line f in
      assert(line <> "");
      let c = line.[0] in
      let arg = String.sub line 1 (String.length line - 1) in
      let obj =
	match c with
	  | 'F' -> `File arg
	  | 'S' -> `Shm arg
	  | _ -> assert false in
      l := obj :: !l
    done;
    assert false
  with
    | End_of_file ->
	!l


let read_write f =
  let fn = pmanage_file() in
  if not (Sys.file_exists fn) then (
    close_out(open_out_gen [Open_wronly; Open_creat] 0o600 fn); 
    (* but no truncation *)
  );
  let fd = Unix.openfile fn [Unix.O_RDWR] 0 in
  Unix.lockf fd Unix.F_LOCK 0;
  let r = f (read fd) in
  ignore(Unix.lseek fd 0 Unix.SEEK_SET);
  Unix.ftruncate fd 0;
  let f = Unix.out_channel_of_descr fd in
  List.iter
    (fun obj ->
       match obj with
	 | `File arg ->
	     output_string f ("F" ^ arg ^ "\n")
	 | `Shm arg ->
	     output_string f ("S" ^ arg ^ "\n")
    )
    (List.rev r);
  close_out f (* also unlocks *)


let register_file name =
  if name = "" || name.[0] <> '/' then
    failwith "Pfs_manage.register_file: the name is not absolute";
  read_write
    (fun l ->
       `File name :: l
    )


let register_shm name =
  if name = "" || name.[0] <> '/' then
    failwith "Pfs_manage.register_shm: the name is not absolute";
  read_write
    (fun l ->
       `Shm name :: l
    )


let delete_registered_objects() =
  read_write
    (fun l ->
       List.iter
	 (function
	    | `File name ->
		( try Sys.remove name with _ -> () )
	    | `Shm name ->
		( try Netsys_posix.shm_unlink name with _ -> () )
	 )
	 l;
       []
    )
