(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Authentication in server context *)

(** {2 Server Configuration} *)

type level =
    [ `None
    | `Authenticated
    | `Integrity
    | `Privacy
    ]
    (** Levels of protection:
	- [`None]: No protection (unauthenticated access)
	- [`Authenticated]: The user needs to log in, but the messages
	  are unprotected otherwise
	- [`Integrity]: The user needs to log in, and additionally
	  the messages are integrity-protected (signed). This is the
	  recommended minimum level.
	- [`Privacy]: The user needs to log in, and additionally
	  the messages are encypted (this includes integrity-protection).
     *)

type access =
    { min_level : level;
        (** The minimum level of protection for incoming connections *)

      users : (string * string * string * string * int) list;
	(** The users with passwords. Incoming connections can log in
	    as any of these users. The passwords are sometimes also
	    used for outgoing connections to other servers.

	    The tuple is:
	    [ (user, password, salted_password, salt, iteration_count) ]
	 *)
    }

val string_of_level : level -> string
val level_of_string : string -> level
  (** Conversions to/from string *)


val extract_access_config : 
      Netplex_types.config_file -> Netplex_types.address -> access
    (** Extracts a config section from a Netplex config file like:
	
	{[
	  access {
            min_level = "auth";   (* or "int", or "priv" *)
	    user { name = "name"; password = "pw" };
	    (* more users possible *)
	  }
	]}

	If this section is missing, a level of [`None] is assumed.

	It is possible to use [password_file] instead of [password] to
	read the password from the first line of a given file (relative
	to the config file).
     *)

val privileged_user : string
  (** The name of the privileged users: "proot" (Plasma root) *)

val bottom_user : string
  (** The name of the user with minimum rights: "pnobody" (Plasma nobody) *)

val configure_rpc_server : Rpc_server.t -> access -> unit
  (** Configures the RPC server so the passed level of access protection
      is required.

      For many services, the user "proot" is required at minimum (if
      access control is enabled). This is already checked by the configured
      authentication method.

      The procedure 0 is handled specially here and never requires
      authentication.
   *)


(** {2 Client Configuration} *)

(** This is for internal clients only! *)

type client_auth =
    { level : level;        (** The level actually used *)
      proot_pw : string;    (** The password of "proot" *)
      pnobody_pw : string;  (** The password of "pnobody" *)
    }

val extract_client_config : 
      Netplex_types.config_file -> Netplex_types.address -> client_auth
    (** Extracts a config section from a Netplex config file like:
	
	{[
	  access {
            client_level = "auth";   (* or "int", or "priv" *)
	    user { name = "proot"; password = "pw" };
	    (* Optionally also a record for "pnobody" possible *)
	  }
	]}

	If this section is missing, a level of [`None] is assumed.
     *)

val rpc_proxy_auth_methods : client_auth -> bool -> Rpc_client.auth_method list
  (** Returns a value suitable for the [mclient_auth_methods] field.
      The bool says whether the access is privileged ("proot" is used
      instead of "pnobody").
   *)

      
