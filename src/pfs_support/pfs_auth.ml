(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

type level =
    [ `None
    | `Authenticated
    | `Integrity
    | `Privacy
    ]

let string_of_level =
  function
    | `None -> "None"
    | `Authenticated -> "Authenticated"
    | `Integrity -> "Integrity"
    | `Privacy -> "Privacy"

let level_of_string =
  function
    | "None" | "none" -> `None
    | "Authenticated" | "Auth" | "authenticated" | "auth" -> `Authenticated
    | "Integrity" | "Int" | "integrity" | "int" -> `Integrity
    | "Privacy" | "Priv" | "privacy" | "priv" -> `Privacy
    | s ->
	failwith ("Pfs_auth.level_of_string: unknown level: " ^ s)


type access =
    { min_level : level;
      users : (string * string * string * string * int) list;
    }

type client_auth =
    { level : level;
      proot_pw : string;
      pnobody_pw : string;
    }


let scram_ic = 4096

let extract_access_config cf addr =
  match cf#resolve_section addr "access" with
    | [] ->
	{ min_level = `None;
	  users = []
	}
    | [access] ->
	let min_level_s =
	  try cf#string_param (cf#resolve_parameter access "min_level")
	  with Not_found -> "priv" in
	let min_level =
	  try level_of_string min_level_s
	  with Failure msg ->
	    failwith
	      (msg ^ " - in parameter " ^ cf#print access ^ ".min_level") in
	let user_sects =
	  cf#resolve_section access "user" in
	let users =
	  List.map
	    (fun user ->
	       let name =
		 try cf#string_param (cf#resolve_parameter user "name")
		 with Not_found ->
		   failwith ("Missing parameter: " ^ cf#print user ^ ".name") in
	       let pw =
		 try cf#string_param (cf#resolve_parameter user "password")
		 with Not_found ->
		   ( let file =
		       try 
			 cf#string_param 
			   (cf#resolve_parameter user "password_file")
		       with Not_found ->
			  failwith ("Missing parameter: " ^ cf#print user ^ 
				      ".password (or .password_file)") in
		     let file =
		       if Filename.is_implicit file then
			 Filename.dirname cf#filename ^ "/" ^ file
		       else
			 file in
		     try
		       let f = open_in file in
		       let pw = input_line f in
		       close_in f;
		       pw
		     with
		       | error ->
			   failwith ("Error reading file " ^ file ^ ": " ^ 
				       Netexn.to_string error)
		   ) in
	       let salt = Netmech_scram.create_salt() in
	       let spw = Netmech_scram.salt_password pw salt scram_ic in
	       (name, pw, spw, salt, scram_ic)
	    )
	    user_sects
	in
	{ min_level = min_level;
	  users = users
	}
    | _ ->
	failwith ("Pfs_auth.extract_access_config: several sections " ^ 
		    cf#print addr ^ ".access")


let get_pw acc user =
   try
     let (_,pw,_,_,_) =
       List.find
	 (fun (u,_,_,_,_) -> u=user)
	 acc.users in
     pw
   with Not_found -> ""


let privileged_user = "proot"
let bottom_user = "pnobody"

let extract_client_config cf addr =
  match cf#resolve_section addr "access" with
    | [] ->
	{ level = `None;
	  proot_pw = "";
	  pnobody_pw = ""
	}
    | [access] ->
	let level_s =
	  try cf#string_param (cf#resolve_parameter access "client_level")
	  with Not_found -> "none" in
	let level =
	  try level_of_string level_s
	  with Failure msg ->
	    failwith
	      (msg ^ " - in parameter " ^ cf#print access ^ ".client_level") in
	let acc = extract_access_config cf addr in
	{ level = level;
	  proot_pw = get_pw acc privileged_user;
	  pnobody_pw = get_pw acc bottom_user;
	}
    | _ ->
	failwith ("Pfs_auth.extract_client_config: several sections " ^ 
		    cf#print addr ^ ".access")


let prog_ht = 
  Hashtbl.create 49

let () =
  (* Configure services requiring a specific user *)
  List.iter
    (fun (prog,user) ->
       let prog_nr = Rpc_program.program_number prog in
       Hashtbl.add prog_ht prog_nr user
    )
    [ Pfs_rpcapi_aux.program_Datanode_ctrl'V1,         privileged_user;
      Pfs_rpcapi_aux.program_Dn_admin'V1,              privileged_user;
      Pfs_rpcapi_aux.program_Elect'V1,                 privileged_user;
      Pfs_rpcapi_aux.program_Nameslave'V1,             privileged_user;
      Pfs_rpcapi_aux.program_Namesync'V1,              privileged_user;
    ]


let configure_rpc_server rpc acc =
  let ml =
    if acc.min_level = `None then
      [Rpc_server.auth_none]
    else 
      let server_key_verifier =
	( object
            method scram_credentials username = (* may raise Not_found *)
	      try
		let user =
		  List.find
		    (fun (u,_,_,_,_) -> u=username)
		    acc.users in
		let (_,_,pw,salt,ic) = user in
		(pw,salt,ic)
	      with
		| Not_found ->
		    Netlog.logf `Warning
		      "No password set for user: %s" username;
		    raise Not_found
	  end
	) in
      let gss_api =
	new Netmech_scram_gssapi.scram_gss_api
	  ~server_key_verifier
	  (Netmech_scram.profile ~return_unknown_user:true `GSSAPI) in
      let m1 =
	Rpc_auth_gssapi.server_auth_method
          ~require_privacy:(acc.min_level = `Privacy)
          ~require_integrity:(acc.min_level = `Integrity)
          ~user_name_format:`Plain_name
          ~shared_context:true  (* for UDP *)
          (* FIXME: seq_number_window *)
          gss_api
          Netmech_scram_gssapi.scram_mech in
      let null_proc =
	Rtypes.uint4_of_int 0 in
      let m1a =
	( object
	    method name = m1#name
	    method flavors = m1#flavors
	    method peek = m1#peek
	    method authenticate srv conn_id details f =
	      m1#authenticate srv conn_id details
		(fun ar ->
		   match ar with (* Do we need a specific user? *)
		     | Rpc_server.Auth_positive
			 (uname,rverftyp,rverf,enc_opt,dec_opt) ->
			 let ar' =
			   try
			     let u = Hashtbl.find prog_ht details#program in
			     if u <> uname then
			       Rpc_server.Auth_negative(Rpc.Auth_too_weak)
			     else
			       ar
			   with
			     | Not_found ->
				 ar in
			 f ar'
		     | _ -> f ar
		)
	  end
	) in
      let m2a = (* permit procedure 0 with AUTH_NONE *)
	( object
	    method name = Rpc_server.auth_none#name
	    method flavors = Rpc_server.auth_none#flavors
	    method peek = Rpc_server.auth_none#peek
	    method authenticate srv conn_id details f =
	      if details#procedure = null_proc then
		Rpc_server.auth_none#authenticate srv conn_id details f
	      else
		f (Rpc_server.Auth_negative(Rpc.Auth_too_weak))
	  end
	) in
      [m1a; m2a]
  in
  if not(Rpc_server.is_dummy rpc) then
    Rpc_server.set_auth_methods rpc ml


let rpc_proxy_auth_methods cauth priv =
  if cauth.level = `None then
    [Rpc_client.auth_none]
  else
    let client_key_ring =
      ( object
          method password_of_user_name n =
	    if n = privileged_user then
	      cauth.proot_pw
	    else if n = bottom_user then
	      cauth.pnobody_pw
	    else raise Not_found
          method default_user_name =
            if priv then Some privileged_user else Some bottom_user
	end
      ) in
    let gss_api =
      new Netmech_scram_gssapi.scram_gss_api
	~client_key_ring
	(Netmech_scram.profile `GSSAPI) in
    let m =
      Rpc_auth_gssapi.client_auth_method
	~privacy:(if cauth.level = `Privacy then `Required else `None)
	~integrity:(if cauth.level = `Integrity then `Required else `None)
	~user_name_interpretation:(`Plain_name Netgssapi.nt_user_name)
	gss_api
	Netmech_scram_gssapi.scram_mech in
    [m]
