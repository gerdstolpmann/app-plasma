(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf
open Uq_engines.Operators

class type db_config =
object
  method db_host : string option
  method db_hostaddr : string option
  method db_port : string option
  method db_dbname : string option
  method db_user : string option
  method db_password : string option
  method db_ro_connections_max : int
end

let dlog = Plasma_util.dlog
let dlogr = Plasma_util.dlogr


let dummy_db_config() =
  ( object
      method db_host = None
      method db_hostaddr = None
      method db_port = None
      method db_dbname = None
      method db_user = None
      method db_password = None
      method db_ro_connections_max = 20
    end
  )


let extract_db_config cf =
  let databases =
    cf#resolve_section cf#root_addr "database" in
  match databases with
    | [] ->
        failwith "Missing 'database' config"
    | _ :: _ :: _ ->
        failwith "More than one 'database' config"
    | [database] ->
	let param f name =
          try Some(f (cf#resolve_parameter database name))
          with Not_found -> None in
	let host =
	  (param cf#string_param "host") in
	let hostaddr =
	  (param cf#string_param "hostaddr") in
	let port =
	  (param cf#string_param "port") in
	let dbname =
	  (param cf#string_param "dbname") in
	let user =
	  (param cf#string_param "user") in
	let password =
	  (param cf#string_param "password") in
	let ro_connections_max =
	  (try cf#int_param
	         (cf#resolve_parameter database "ro_connections_max") 
	   with Not_found -> 20) in
	( object
	    method db_host = host
	    method db_hostaddr = hostaddr
	    method db_port = port
	    method db_dbname = dbname
	    method db_user = user
	    method db_password = password
	    method db_ro_connections_max = ro_connections_max
	  end
	)


class type ['mode] async_connection =
object
  inherit Postgresql.connection
  method continue_with : (bool -> unit) -> unit
  method next : bool -> unit
  method idle : bool

  method send : ?noprepare:bool -> ?params : string array ->
                ?binary_params : bool array -> string -> unit
    (* Unless noprepare, prepare the statement first *)

end


type read_write = [`R|`W]
type read_only = [`R]
type rw_async_connection = read_write async_connection
type ro_async_connection = read_only async_connection


class sync_connect (db_conf : db_config) =
  Postgresql.connection
    ?host:db_conf#db_host
    ?hostaddr:db_conf#db_hostaddr
    ?port:db_conf#db_port
    ?dbname:db_conf#db_dbname
    ?user:db_conf#db_user
    ?password:db_conf#db_password
    ()

class ['mode] connect (db_conf : db_config) =
object(self)
  inherit sync_connect db_conf as super

  val mutable processing = false
  val queue = Queue.create()

  val prepared = Hashtbl.create 5
  val mutable prep_id = 0

  method send_query ?params ?binary_params query =
    if processing then
      failwith "Db.send_query: previous SQL command is still being processed";
    super # send_query ?params ?binary_params query;
    processing <- true

  method send_query_prepared ?params ?binary_params query =
    if processing then
      failwith "Db.send_query_prepared: previous SQL command is still \
                being processed";
    super # send_query_prepared ?params ?binary_params query;
    processing <- true

  method send ?(noprepare=false) ?params ?binary_params query =
    if processing then
      failwith "Db.send: previous SQL command is still being processed";
    if noprepare then
      self # send_query ?params ?binary_params query
    else (
      let sname =
	try
	  Hashtbl.find prepared query
	with
	  | Not_found ->
	      let id = prep_id in
	      prep_id <- prep_id + 1;
	      let _prep_r =
		super # exec ~expect:[Postgresql.Command_ok]
		  (sprintf 
		     "PREPARE s%d AS %s" id query) in
	      let sname = sprintf "s%d" id in
	      Hashtbl.replace prepared query sname;
	      sname in
      self # send_query_prepared ?params ?binary_params sname
    )

  method get_result =
    if not processing then
      failwith "Db.get_result: No running SQL command";
    let r = super # get_result in
    if r = None then
      processing <- false;
    r

  method continue_with f =
    if not processing && Queue.is_empty queue then
      f true
    else
      Queue.push f queue

  method next ok =
    if not(Queue.is_empty queue) then (
      let f = Queue.take queue in
      f ok
    )

  method idle =
    not processing

end


class async_exec (conn : _ async_connection) esys 
                 ?expect ?params ?binary_params 
		 ?(copy_in_lines=[])
                 ?(copy_out_channel = new Netchannels.output_null())
		 ?noprepare
		 query =
  let () =
    conn # set_nonblocking true in
  let fdi =
    conn # socket in
  let fd =
    Netsys_posix.file_descr_of_int fdi in
  let protect f arg =
    try
      f arg
    with
      | error ->
	  Netlog.logf `Crit "Exception sending SQL %s: %s"
	    query (Netexn.to_string error);
	  raise error in
  let flush() =
    dlog "SQL: flush";
    protect (fun () -> conn # flush) () in
  let consume_input () =
    dlog "SQL: consume_input";
    protect (fun () -> conn # consume_input) () in
  let put_copy_data data =
    dlog "SQL: put_copy_data";
    protect (fun () -> conn # put_copy_data data) () in
  let put_copy_end () =
    dlog "SQL: put_copy_end";
    protect (fun () -> conn # put_copy_end ()) () in
  let copy_out_buf =
    lazy(String.create 65536) in
  let copy_out_done =
    ref None in
  ( object(self)
      inherit [Postgresql.result] Uq_engines.engine_mixin (`Working 0) esys

      val mutable started = false
      val mutable final_state = None
      val mutable propagated = false

      initializer (
	conn # continue_with
	  (fun ok -> 
	     if ok then (
	       (* conn # set_nonblocking true; - only affects sending queries *)
	       dlogr
		 (fun () -> sprintf "Sending SQL: %s" query);
	       try
		 assert(conn # flush);   (* send buffer should be empty *)
		 conn # send ?noprepare ?params ?binary_params query;
		 started <- true;

		 dlogr
		   (fun () -> sprintf "Polling SQL: %s" query);

		 if flush () then
		   self # rd_poll None None
		 else
		   self # wr_poll None None
	       with
		 | error ->
		     Netlog.logf `Crit "Exception sending SQL %s: %s"
		       query (Netexn.to_string error)
	     )
	     else
	       self # abort()
	  )
      )

      method event_system = esys

	(* FIXME PERF: Do not use an engine as pe. Something more low-level
	   that does not need an explicit restart (but keeps its old
	   observation status unless changed).
	 *)

      method private wr_poll rd_pe_opt wr_pe_opt =
	(* To be called if the query could not be flushed immediately. Wait
	   until fd is writable, and flush again
	 *)
	let wr_pe =
	  match wr_pe_opt with
	    | Some wr_pe -> wr_pe
	    | None ->
		new Uq_engines.poll_engine
		  [Unixqueue.Wait_out fd, (-1.0)] esys in
	self # wr_restart rd_pe_opt wr_pe

      method private wr_restart rd_pe_opt wr_pe =
	( match wr_pe # state with
	    | `Working _ -> ()
	    | _ -> wr_pe # restart()
	);
	Uq_engines.when_state  (* OK *)
	  ~is_done:(fun _ ->  
		      if flush() then
			self # rd_poll rd_pe_opt (Some wr_pe) 
		      else (
			dlog "SQL: wr-polling for more";
			self # wr_restart rd_pe_opt wr_pe
		      )
		   )
	  ~is_error:self#pe_is_error
	  wr_pe
	
      method private rd_poll rd_pe_opt wr_pe_opt =
	(* Wait until the SQL result is available. This can be called after
	   sending the query, or after sending the COPY data
	 *)
	let rd_pe =
	  match rd_pe_opt with
	    | Some rd_pe -> rd_pe
	    | None ->
		new Uq_engines.poll_engine
		  [Unixqueue.Wait_in fd, (-1.0)] esys in
	self # rd_restart rd_pe wr_pe_opt

      method private rd_restart rd_pe wr_pe_opt =
	( match rd_pe # state with
	    | `Working _ -> ()
	    | _ -> rd_pe # restart()
	);
	Uq_engines.when_state  (* OK *)
	  ~is_done:(fun _ -> self # rd_check rd_pe wr_pe_opt)
	  ~is_error:self#pe_is_error
	  rd_pe

      method private rd_check rd_pe wr_pe_opt =
	consume_input();
	if conn#is_busy then (
	  dlog "SQL: rd-polling for more";
	  self # rd_restart rd_pe wr_pe_opt
	)
	else 
	  self # pe_is_done rd_pe wr_pe_opt
	    
      method private wr_copy rd_pe wr_pe_opt get_cmd =  (* for COPY *)
	(* After sending a COPY query and after having received Copy_in,
	   run commands that send the data.
	 *)
	let cmd_opt = get_cmd() in
	match cmd_opt with
	  | None ->
	      (* done with COPY - look again at command result *)
	      dlog "SQL: Waiting for copy status";
	      self # rd_check rd_pe wr_pe_opt
	  | Some cmd ->
	      self # wr_copy_this rd_pe wr_pe_opt get_cmd cmd

      method private wr_copy_this rd_pe wr_pe_opt get_cmd cmd =
	(* Cannot send cmd immediately - wait until fd is writable and try
	   again
	 *)
	let ok = cmd() in
	if ok then
	  self # wr_copy rd_pe wr_pe_opt get_cmd
	else
	  let wr_pe =
	    match wr_pe_opt with
	      | Some wr_pe -> wr_pe
	      | None ->
		  new Uq_engines.poll_engine
		    [Unixqueue.Wait_out fd, (-1.0)] esys in 
	  ( match wr_pe # state with
	      | `Working _ -> ()
	      | _ -> wr_pe # restart()
	  );
	  Uq_engines.when_state  (* OK *)
	    ~is_done:(fun _ -> 
			self # wr_copy_this rd_pe (Some wr_pe) get_cmd cmd)
	    ~is_error:self#pe_is_error
	    wr_pe

      method private pe_is_done rd_pe wr_pe_opt =
	(* result available *)
        match conn#get_result with
	  | None   -> 
              ( match !copy_out_done with
                  | Some r ->
                      (* This one is special *)
		      dlogr(fun () -> 
                            sprintf "Done with SQL (after copy): %s" query);
                      final_state <- Some (`Done r);
		      self # propagate_final_state()
                  | None ->
                      self # missing_result()
              )
	  | Some r ->
              self # process_result rd_pe wr_pe_opt r

      method private missing_result() =
        Netlog.logf `Crit "Missing SQL result";
        final_state <- 
	  Some(`Error (Failure "Db.async_exec: out of sync"));
	self # propagate_final_state()
                                                 
      method private process_result rd_pe wr_pe_opt r =
        try
          let s =
	    match expect with
	      | None -> `Done r
	      | Some l ->
		  if List.mem r#status l then
		    `Done r 
		  else
		    `Error(Postgresql.Error
			     (Postgresql.Unexpected_status
			        (r#status,r#error,l))) in
          ( match r#status with
              | Postgresql.Copy_in ->
		  (* now send copy data *)
		  dlog "SQL: Sending copy data";
		  let lines = ref copy_in_lines in
		  let trailer = ref [ put_copy_end ] in
		  let get_cmd() =
		    match !lines with
		      | l :: lines' ->
			  lines := lines';
			  Some(fun () -> put_copy_data (l ^ "\n"))
		      | [] ->
			  ( match !trailer with
			      | cmd :: trailer' ->
				  trailer := trailer';
				  Some cmd
			      | [] ->
				  None
			  ) in
		  self # wr_copy rd_pe wr_pe_opt get_cmd
              | Postgresql.Copy_out ->
                  if !copy_out_done <> None then
                    self # pe_is_error
                             (Failure "Connection out of synch after COPY OUT")
                  else (
                    dlog "SQL: Receiving copy data";
                    let b = Lazy.force copy_out_buf in
                    let code = conn # getline_async b in
                    ( match code with
                        | Postgresql.EndOfData ->
                            dlog "SQL: End of copy data";
                            copy_out_channel # flush();
                            copy_out_channel # close_out();
                            copy_out_done := Some r;
                            self # rd_check rd_pe wr_pe_opt
                        | Postgresql.NoData ->
                            self # rd_check rd_pe wr_pe_opt
                        | Postgresql.DataRead n ->
                            copy_out_channel # really_output b 0 n;
                            self # rd_check rd_pe wr_pe_opt
                        | Postgresql.PartDataRead n ->
                          self # pe_is_error
                                   (Failure "COPY OUT buffer exceeded");
                    )
                  )
              | _ ->
		  dlogr (fun () -> sprintf "Done with SQL: %s" query);
		  final_state <- Some s;
		  ( match conn#get_result with
		      | None ->
			  dlog "Last SQL result";
			  self # propagate_final_state()
		      | Some _ ->
                          self # pe_is_error
                                   (Failure "More than one SQL result!")
		  )
	  )
	with
	  | error ->
	      Netlog.logf `Crit "Db.async_exec: pg exception %s"
		          (Netexn.to_string error);
	      rd_pe # abort();
	      ( match wr_pe_opt with
		  | Some pe -> pe # abort()
		  | None -> ()
	      );
	      final_state <- Some(`Error error);
	      self # propagate_final_state()
                                          
      method private pe_is_error error =
	Netlog.logf `Crit "Db.async_exec: pe exception %s"
	  (Netexn.to_string error);
	final_state <- Some(`Error error);
	self # propagate_final_state()

      method abort() =
	if final_state = None then (
	  if started then (
	    conn # request_cancel;
	    final_state <- Some `Aborted;
	  ) else (
	    final_state <- Some `Aborted;
	    self # propagate_final_state()
	  )
	)

      method private propagate_final_state() =
	if not propagated then (
	  match final_state with
	    | Some st ->
		propagated <- true;
		(** We don't want to get exceptions here. So run the
                    [set_state] method from the event queue. [set_state]
                    invokes callbacks waiting for state change notifications.
		 *)
		let g = Unixqueue.new_group esys in
		Unixqueue.once esys g 0.0 (fun () -> self # set_state st);
		let ok =
		  match st with
		    | `Done _ -> true
		    | _ -> false in
		conn # next ok
	    | None -> 
		()
	)
	else
	  Netlog.logf `Warning
	    "Not propagating further final state"
    end
  )


let copy_out_e conn esys ch table_name columns =
  let stmt =
    sprintf "COPY %s (%s) TO STDOUT"
            table_name
            (String.concat "," columns) in
  new async_exec conn esys ~copy_out_channel:ch ~noprepare:true stmt
  ++ (fun r ->
        if r#status <> Postgresql.Copy_out then
          failwith ("Pfs_db.copy_out_e: bad status from db: " ^ 
                      Postgresql.result_status r#status ^ " - " ^ 
                        r#error);
        eps_e (`Done()) esys
     )  


let truncate (conn : #Postgresql.connection) table_names =
  let s = String.concat "," table_names in
  Netlog.logf `Notice "Truncating database tables: %s" s;
  let stmt = sprintf "TRUNCATE %s" s in
  let _r =
    conn # exec
      ~expect:[ Postgresql.Command_ok ]
      stmt in
  ()


let copy_in (conn : #Postgresql.connection) file table_name columns =
  Netlog.logf `Notice "Loading into database table: %s" table_name;
  let stmt =
    sprintf "COPY %s (%s) FROM STDIN"
            table_name
            (String.concat "," columns) in
  let _r =
    conn # exec
      ~expect:[ Postgresql.Copy_in ]
      stmt in
  let f = open_in_bin file in
  let n = 65536 in
  let buf = String.create n in
  ( try
      while true do
        let k = input f buf 0 n in
        if k = 0 then raise End_of_file;
        let flag = conn # put_copy_data ~len:k buf in
        assert flag;
      done
    with
      | End_of_file -> ()
      | error -> close_in f; raise error
  );
  close_in f;
  let flag = conn # put_copy_end() in
  assert flag;
  ()


let () =
  Netexn.register_printer
    (Postgresql.Error Postgresql.Binary)
    (fun e -> 
       match e with
	 | Postgresql.Error e' ->
	     "Postgresql.Error(" ^ Postgresql.string_of_error e' ^ ")"
	 | _ ->
	     assert false
    )
