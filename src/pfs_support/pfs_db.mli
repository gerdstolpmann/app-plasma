(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Utility functions for database accesses *)

class type db_config =
object
  method db_host : string option
  method db_hostaddr : string option
  method db_port : string option
  method db_dbname : string option
  method db_user : string option
  method db_password : string option
  method db_ro_connections_max : int
end

val dummy_db_config : unit -> db_config

val extract_db_config : Netplex_types.config_file -> db_config
  (** Extracts the [db_config] from this Netplex config file:

      {[
         netplex {
           ...
           database {
             dbname = "<name of database>";  (* required *)
             host = "<hostname>";
             hostaddr = "<ipaddr>";
             port = <port>;
             user = "<user name>";
             password = "<password>";
             ro_connections_max = <c>;
           };
           ...
         }
      ]}

      See the documentation of PostgreSQL for [dbname], [host], [hostaddr],
      [port], [user], and [password] (but it should be obvious).

      [ro_connections_max] is the number of db connections dedicated for
      read-only database accesses. Defaults to 20.
   *)


class type ['mode] async_connection =
object
  inherit Postgresql.connection

  method continue_with : (bool -> unit) -> unit
    (** [continue_with f]: Set [f] as the continuation. If the connection
        is non-busy [f] is executed immediately. Otherwise it is pushed
        onto the queue of delayed continuations. The argument of [f] is
        [true] if the previous statement was successful.
     *)

  method next : bool -> unit
    (** If there is a delayed continuation execute it now *)

  method idle : bool
    (** whether nothing is being executed *)

  method send : ?noprepare:bool -> ?params : string array ->
                ?binary_params : bool array -> string -> unit
    (** Unless [noprepare] is set, prepare the query first, and execute it
	then. If the query is already prepared, it is executed directly.
     *)

end


type read_write = [`R|`W]
    (** the [mode] that is conventionally used for read-write connections *)

type read_only = [`R]
    (** the [mode] that is conventionally used for read-only connections *)

type rw_async_connection = read_write async_connection
    (** abbrev *)

type ro_async_connection = read_only async_connection
    (** abbrev *)



class sync_connect : db_config -> Postgresql.connection
  (** A normal synchronous connection *)

class ['mode] connect : db_config -> ['mode] async_connection
  (** Right now, we only have a sync connect *)

class async_exec : 'mode async_connection ->
                   Unixqueue.event_system ->
                   ?expect : Postgresql.result_status list -> 
                   ?params : string array ->
                   ?binary_params : bool array -> 
                   ?copy_in_lines : string list ->
                   ?copy_out_channel : Netchannels.out_obj_channel ->
                   ?noprepare : bool ->
                   string ->
                     [Postgresql.result] Uq_engines.engine
  (** Execution as an engine.

      The query is sent immediately. The response is awaited in an
      asynchronous way. When it is complete, the engine transitions
      to state [`Done r] where [r] is the result. In case of an error,
      the engine transitions to [`Error e] where [e] is the exception.

      [copy_in_lines] is for "COPY IN" statements.

      [copy_out_channel] is the channel receiving data from "COPY OUT"
      statements.

      [noprepare] does not prepare the statement (useful if it is not constant).

      

      An [abort] invocation cancels the current query.
   *)


val copy_out_e : 'mode async_connection ->
                 Unixqueue.event_system ->
                 Netchannels.out_obj_channel ->
                 string ->
                 string list ->
                   unit Uq_engines.engine
  (** [copy_out_e conn esys ch table_name columns]: Submits a COPY OUT
      statement for [table_name] and the given [columns]. The data is
      written to [ch]. The statement uses connection [conn], and the
      even processing is done with [esys].

      The format is a tab-separated file with \N for NULL fields, and
      the normal escape sequences \b, \f, \n, \r, \t, \v and \\.
      The length of each line is limited, and the statement will fail
      if the limit is exceeded (currently 64K).
   *)

val truncate : #Postgresql.connection -> string list -> unit
  (** [truncate conn tables]: truncates the names tables (sync) *)

val copy_in : #Postgresql.connection -> string -> string -> string list -> unit
  (** [copy_in conn file table columns]: Loads the dump [file] into
      [table] with [columns]. The table must already exist. (Sync.)
   *)

