(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* FIXME: Better random number generator than Random from stdlib *)

(*
let () =
  (* Init this module if it is used: *)
  Random.self_init();
  (* Also register a post fork handler. These handlers are respected by
     Netplex, and called for each new child. We reinitialize there, so
     each child gets a different sequence of random numbers.
   *)
  Netsys_posix.register_post_fork_handler
    ( object
	method name = "rng"
	method run () = Random.self_init()
      end
    )
 *)


let random_bytes n =
  let s = String.create n in
(*
  for k = 0 to n-1 do
    s.[k] <- Char.chr(Random.int 256)
  done;
 *)
  Netsys_rng.fill_random s;
  s


let random_int64() =
(*
  let b1 = Random.bits() in
  let b2 = Random.bits() in
  let b3 = Random.bits() land 15 in
  Int64.logor
    (Int64.of_int b1)
    (Int64.logor
       (Int64.shift_left (Int64.of_int b2) 30)
       (Int64.shift_left (Int64.of_int b3) 60))
 *)
  let s = random_bytes 8 in
  Rtypes.int64_of_int8(Rtypes.read_int8_unsafe s 0)


let random_int n =
(*
  Random.int n
 *)
  let k = random_int64() in
  Int64.to_int (Int64.rem (Int64.abs k) (Int64.of_int n))
