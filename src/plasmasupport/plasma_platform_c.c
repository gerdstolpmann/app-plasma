/*
  Copyright 2011 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
/* $Id$ */

#include "caml/mlvalues.h"
#include "caml/alloc.h"
#include "caml/memory.h"
#include "caml/fail.h"
#include "caml/signals.h"
#include "caml/custom.h"
#include "caml/callback.h"
#include "caml/bigarray.h"

#include <string.h>


CAMLprim value plasma_blit_memory(value memv,
				  value memoffv,
				  value destv,
				  value destoffv,
				  value lenv)
{
    struct caml_bigarray *mem = Bigarray_val(memv);
    struct caml_bigarray *dest = Bigarray_val(destv);
    long memoff = Long_val(memoffv);
    long destoff = Long_val(destoffv);
    long len = Long_val(lenv);

    memmove(((char*) dest->data) + destoff, ((char*) mem->data) + memoff, len);

    return Val_unit;
}


CAMLprim value plasma_fill_memory(value memv,
				  value memoffv,
				  value lenv,
				  value fillv
				  )
{
    struct caml_bigarray *mem = Bigarray_val(memv);
    long memoff = Long_val(memoffv);
    long len = Long_val(lenv);
    int fill = Int_val(fillv);

    memset(((char*) mem->data) + memoff, fill, len);

    return Val_unit;
}


CAMLprim value plasma_sys_is_darwin(value dummy) {
#if defined(__darwin__) || defined(__DARWIN__) || defined(__APPLE__)
    return Val_bool(1);
#else
    return Val_bool(0);
#endif
}
