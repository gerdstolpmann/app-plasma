(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Modules *)

module I = struct 
  type t = int
  let compare x y = x-y
end

module IntSet = Set.Make(I)
module IntMap = Map.Make(I)
module I64Set = Set.Make(Int64)
module I64Map = Map.Make(Int64)
module StrSet = Set.Make(String)
module StrMap = Map.Make(String)

(** Logging *)

let debug = ref false

let dlog msg =
  if !debug then
    Netlog.log `Debug (* prerr_endline *) ("[" ^ string_of_int (Unix.getpid()) ^ "] " ^ msg)

let anon_b = Buffer.create 1

let dlogf fmt =
  if !debug then
    (* Printf.ksprintf dlog fmt *)
    let b = Buffer.create 80 in
    Printf.kbprintf (fun b -> dlog (Buffer.contents b)) b fmt
  else
    Printf.ifprintf anon_b fmt
    (* Printf.ksprintf (fun _ -> ()) fmt *)

let dlogr f =
  if !debug then dlog(f ())

let init_debug cmd_conf =
  let config_parser = Netplex_config.read_config_file in
  let cf =
    match Netplex_main.config_tree_opt cmd_conf with
      | None ->
          config_parser (Netplex_main.config_filename cmd_conf)
      | Some tree ->
          Netplex_config.repr_config_file
	    (Netplex_main.config_filename cmd_conf) tree in
  (* get controller.max_level: *)
  try
    let ctrl_section =
      List.hd (cf # resolve_section cf#root_addr "controller") in
    let max_level =
      cf # resolve_parameter ctrl_section "max_level" in
    debug := (cf # string_param max_level = "debug")
  with
    | _ -> ()
  


(** Utility functions *)

open Printf

let parse_host_port s =
  try
    Scanf.sscanf s "%s@:%d%!" 
      (fun h p -> (h,p))
  with
    | _ -> failwith "Util.parse_host_port"

let host_port_of_sockaddr =
  function
    | Unix.ADDR_INET(ip,p) ->
	(Unix.string_of_inet_addr ip, p)
    | Unix.ADDR_UNIX s ->
	failwith "Util.host_port_of_sockaddr"

let string_of_sockaddr =
  Netsys.string_of_sockaddr


(* FIXME: configurable number of attempts *)

let ip_of_host h =
  let rec attempt k =
    try Unix.inet_addr_of_string h
    with _ ->
      ( try
	  let e = Unix.gethostbyname h in
	  if k > 1 then
	    Netlog.logf `Warning
	      "Host finally found: %s (after %d errors)" h (k-1);
	  e.Unix.h_addr_list.(0)
	with
	  | Not_found ->
	      if k=3 then (
		Netlog.logf `Err
		  "Host not found: %s (after %d attempts)" h k;
		failwith ("Host not found: " ^ h)
	      )
	      else (
		Netlog.logf `Warning
		  "Host not found: %s (attempt %d)" h k;
		Unix.sleep 1;
		attempt(k+1)
	      )
      ) in
  attempt 1

let sockaddr_of_host_port (h,p) =
  let ip = ip_of_host h in
  Unix.ADDR_INET(ip,p)


let binop_inet_addr f (ip1 : Unix.inet_addr) (ip2 : Unix.inet_addr) =
  let s1 = (Obj.magic ip1 : string) in
  let s2 = (Obj.magic ip2 : string) in
  let l = String.length s1 in
  if l <> String.length s2 then
    failwith "logand_inet_addr";
  let s3 = String.create l in
  for k = 0 to l-1 do
    s3.[k] <- Char.chr(f (Char.code s1.[k]) (Char.code s2.[k]));
  done;
  (Obj.magic s3 : Unix.inet_addr)

let logand_inet_addr =
  binop_inet_addr ( land )

let logor_inet_addr =
  binop_inet_addr ( lor )

let logxor_inet_addr =
  binop_inet_addr ( lxor )

let lognot_inet_addr ip =
  binop_inet_addr
    (fun p1 p2 -> lnot p1)
    ip
    ip

let is_ipv4_inet_addr (ip : Unix.inet_addr) =
  String.length (Obj.magic ip) = 4

let is_ipv6_inet_addr (ip : Unix.inet_addr) =
  String.length (Obj.magic ip) = 16

let is_multicast_inet_addr ip =
  if is_ipv4_inet_addr ip then
    logand_inet_addr
      ip
      (Unix.inet_addr_of_string "240.0.0.0")
    = (Unix.inet_addr_of_string "224.0.0.0")
  else
    if is_ipv6_inet_addr ip then
      logand_inet_addr
	ip
	(Unix.inet_addr_of_string "ffff::0")
      = (Unix.inet_addr_of_string "ff00::0")
    else
      false


let connector_of_sockaddr =
  Rpc_client.connector_of_sockaddr


let ip_of_sockaddr addr =
  match addr with
    | Unix.ADDR_INET(ip,_) -> ip
    | Unix.ADDR_UNIX _ -> Unix.inet_addr_loopback (* FIXME: IPv6 not supp *)


type errno =
    (* everything but `ok *)
    [ `enotrans | `efailedcommit | `elongtrans | `efailed | `eperm
    | `enoent | `eaccess | `eexist | `efhier | `einval | `efbig
    | `enospc | `erofs | `enametoolong | `econflict | `ecoord
    | `enonode | `etbusy | `eio | `estale | `eloop | `enotdir 
    | `eisdir | `enotempty | `ebadpath
    ]

let string_of_errno te =
  match te with
    | `enotrans -> "ENOTRANS"
    | `efailedcommit -> "EFAILEDCOMMIT"
    | `elongtrans -> "ELONGTRANS"
    | `efailed -> "EFAILED"
    | `eperm -> "EPERM"
    | `enoent ->  "ENOENT"
    | `eaccess -> "EACCESS"
    | `eexist -> "EEXIST"
    | `efhier -> "EFHIER"
    | `einval -> "EINVAL"
    | `efbig -> "EFBIG"
    | `enospc -> "ENOSPC"
    | `erofs -> "EROFS"
    | `enametoolong -> "ENAMETOOLONG"
    | `econflict -> "ECONFLICT"
    | `ecoord -> "ECOORD"
    | `enonode -> "ENONODE"
    | `etbusy -> "ETBUSY" 
    | `eio -> "EIO"
    | `estale -> "ESTALE"
    | `eloop -> "ELOOP"
    | `enotdir -> "ENOTDIR"
    | `eisdir -> "EISDIR"
    | `enotempty -> "ENOTEMPTY"
    | `ebadpath -> "EBADPATH"

exception Plasma_error of errno

let fallocate fd pos len =
  if len < 0L then
    invalid_arg "Plasma_util.fallocate";
  if len > Int64.sub Int64.max_int pos then 
    invalid_arg "Plasma_util.fallocate";
  try
    if not(Netsys_posix.have_fallocate()) then
      raise (Unix.Unix_error(Unix.ENOSYS,"",""));
    Netsys_posix.fallocate fd pos len;
    (* fallocate does not report errors reliably! *)
    let st = Unix.LargeFile.fstat fd in
    if st.Unix.LargeFile.st_size < Int64.add pos len then
      failwith "Plasma_util.fallocate: cannot allocate disk space"
  with
    | Unix.Unix_error(Unix.ENOSYS,_,_) ->
	(* emulation. Note that this temporarily modifies the file position
	   (no pwrite)
	 *)
	let old_pos = Unix.LargeFile.lseek fd 0L Unix.SEEK_CUR in
	let eof =
	  Unix.LargeFile.lseek fd 0L Unix.SEEK_END in
	let s = "\000" in
	let k_start =
	  if pos < eof then Int64.sub eof pos else 0L in
	let k = ref k_start in
	while !k < len do
	  let p = Int64.add pos !k in
	  if p >= eof then (
	    ignore(Unix.LargeFile.lseek fd p Unix.SEEK_SET);
	    ignore(Unix.single_write fd s 0 1);
	  );
	  k := Int64.add !k 512L
	done;
	let new_eof = Int64.add pos len in
	if new_eof > eof then
	  Unix.LargeFile.ftruncate fd new_eof;
	ignore(Unix.LargeFile.lseek fd old_pos Unix.SEEK_SET)


(* read config file support : *)

let comment_re = Pcre.regexp "[^ \t]*#.*$"

let strip_comments s =
  Pcre.qreplace ~rex:comment_re ~templ:"" s


let node_list ?(port_param="port") ?(node_param="node") ?(empty_ok=false)
              (cf:Netplex_types.config_file) addr =
  let port_opt =
    try
      let p = cf#resolve_parameter addr port_param in
      Some(cf#int_param p)
    with Not_found -> None in
  let parse_host_port s =
    try
      parse_host_port s
    with _ ->
      ( match port_opt with
	  | None -> 
	      failwith("Not in <host>:<port> syntax (no default port given): " 
			 ^ s)
	  | Some p ->
	      (s,p) 
      ) in
  let nodes =
    cf#resolve_section addr node_param in
  let direct_addrs =
    List.map
      (fun n ->
	 try
	   let p = cf#resolve_parameter n "addr" in
	   let a = cf#string_param p in
	   let (h,p) = parse_host_port a in
	   sprintf "%s:%d" h p
	 with
	   | Not_found ->
	       failwith ("missing 'addr' parameter in '" ^ cf#print n ^ "'")
      )
      nodes in
  let node_list_opt =
    try
      let p = cf#resolve_parameter addr (node_param ^ "_list") in
      Some(cf#string_param p)
    with Not_found -> None in
  match node_list_opt with
    | None ->
	if nodes = [] && not empty_ok then
	  failwith ("missing '" ^ node_param ^ "' or '" ^ node_param ^ 
		      "_list' in '" ^ cf#print addr ^ "'");
	direct_addrs

    | Some node_list_file ->
	let fn =
	  if Filename.is_implicit node_list_file then
	    Filename.dirname cf#filename ^ "/" ^ node_list_file
	  else
	    node_list_file in
	let f = open_in fn in
	let addrs = ref [] in
	( try
	    while true do
	      let line = input_line f in
	      let line' = strip_comments line in
	      if line' <> "" then (
		let (h,p) = parse_host_port line' in
		addrs := (sprintf "%s:%d" h p) :: !addrs
	      )
	    done
	  with
	    | End_of_file -> ()
	);
	close_in f;

	direct_addrs @ List.rev !addrs





(**********************************************************************)
(* shared memory                                                      *)
(**********************************************************************)

(* Notes about OSX: Shared memory support is quite strange. The rules are:
   - If shm_open creates the shm block, you need to ftruncate it immediately.
     If you close it without truncating, you get EINVAL. Same if you mmap
     it without truncating.
   - You can only truncate once.
   - If shm_open opens an existing shm block, the fd behaves like a normal
     descriptor (can be closed etc.)

   Consequences:
   - After get_shm_fd call enlarge_shm immediately

 *)

exception Out_of_shared_memory

let rec get_shm_fd prefix n =
  let r = String.sub (Digest.to_hex(Plasma_rng.random_bytes 16)) 0 8 in
  let name = Printf.sprintf "/%s_%s_%d" prefix r n in
  try
    (Netsys_posix.shm_open
       name [Netsys_posix.SHM_O_RDWR; 
	     Netsys_posix.SHM_O_CREAT;
	     Netsys_posix.SHM_O_EXCL ] 0o600,
     name)
  with
    | Unix.Unix_error(Unix.EEXIST,_,_) ->
	get_shm_fd prefix (n+1)


let open_shm_fd name =
  Netsys_posix.shm_open 
    name
    [ Netsys_posix.SHM_O_RDWR ]
    0


let enlarge_shm fd size =
  (* one must never shrink shm! Risk of sigbus! *)
  let sizeL = Int64.of_int size in
  let st = Unix.LargeFile.fstat fd in
  let old_size = st.Unix.LargeFile.st_size in
  Unix.ftruncate fd size;
  if sizeL > old_size then (
    try
      fallocate fd old_size (Int64.sub sizeL old_size)
    with
      | Unix.Unix_error(Unix.ENOSPC,_,_) ->
	  ( try Unix.LargeFile.ftruncate fd old_size with _ -> ());
	  raise Out_of_shared_memory
      | Unix.Unix_error(Unix.ESPIPE,_,_) ->
          ()
  )

let map_shm fd pos len =
  (* we try to avoid here an implicit enlargement of the shm file from
     map_file. This may turn out as race condition when several processes
     do the same with the same shm file, and suddenly one process finds
     them file truncated by the other...
   *)
  if pos < 0L || len < 0 then
    invalid_arg "Plasma_util.map_shm";
  ( try
      let eof = Unix.LargeFile.lseek fd 0L Unix.SEEK_END in
      if Int64.sub eof (Int64.of_int len) < pos then
        failwith "Plasma_util.map_shm: shm file not large enough";
    with
      | Unix.Unix_error(Unix.ESPIPE,_,_) -> 
          () (* this check is unavailable on some OS *)
  );
  Bigarray.Array1.map_file 
    fd ~pos Bigarray.char Bigarray.c_layout true len


let map_shm_int64 fd pos len =
  (* len: in bytes *)
  if pos < 0L || len < 0 then
    invalid_arg "Plasma_util.map_shm_int64";
  ( try
      let eof = Unix.LargeFile.lseek fd 0L Unix.SEEK_END in
      if Int64.sub eof (Int64.of_int len) < pos then
        failwith "Plasma_util.map_shm_int64: shm file not large enough";
    with
      | Unix.Unix_error(Unix.ESPIPE,_,_) -> 
          () (* this check is unavailable on some OS *)
  );
  Bigarray.Array1.map_file 
    fd ~pos Bigarray.int64 Bigarray.c_layout true (len / 8)


(**********************************************************************)
(* Helpers for async code                                             *)
(**********************************************************************)

(* Old definitions now moved to ocamlnet: *)

let signal_engine  =
  Uq_engines.signal_engine

let fmap_engine = 
  Uq_engines.fmap_engine

let msync_engine l esys =
  Uq_engines.msync_engine l (fun _ () -> ()) () esys

let delay_engine =
  Uq_engines.delay_engine

let failsafe_e =
  Uq_engines.meta_engine


let string_of_state = 
  Uq_engines.string_of_state

let rpc_engine  =
  Rpc_proxy.ManagedClient.rpc_engine


let q_seq_engine queue esys f =
  Uq_engines.stream_seq_engine
    () 
    (Stream.from 
       (fun _ -> 
	  if Queue.is_empty queue then
	    None
	  else
	    let x = Queue.take queue in
	    Some(fun () -> f x)
       ))
    esys


let abort_if_working eng =
  match eng#state with
      `Working _ ->
	eng # abort()
    | _ ->
	()
;;


let trace_engine id e =
  let () =
    dlogf
      "trace_engine %s: creating" id in
  ( object
      method request_notification f =
	dlogf
	  "trace_engine %s: request_notification" id;
	e#request_notification 
	  (fun () ->
	     dlogf
	       "trace_engine %s: notify (state %s)" 
	       id (string_of_state e#state);
	     try
	       let keep = f() in
	       dlogf
		 "trace_engine %s: notify back (keep=%b)" id keep;
	       keep
	     with
	       | err ->
		   dlogf
		     "trace_engine %s: notify exception %s"
		     id (Netexn.to_string err);
		   raise err
	  )
      method state = e#state
      method abort = e#abort
      method event_system = e#event_system
    end
  )

let ignore_lost_connection emit x =
  (** Call the [emit] function and ignore [Connection_lost] exceptions.
      This is required when [emit] is invoked after some delay, and 
      not immediately after receiving the request
   *)
  try emit x 
  with Rpc_server.Connection_lost -> ()


let rpc_srv_reply ?(onerror=fun _ -> ())
                  ?(onaborted=fun _ -> ())
                  ?(ondone=fun _ -> ())
                  procname sess emit 
                  (te : _ Uq_engines.final_state Uq_engines.engine) =
  (** [rpc_srv_reply name sess emit te]: Watches the engine [te] (as
      returned by [trans_e]), and when [te] finishes, the result is
      passed back as result of the RPC.

      The engine [te] must follow the conventions for transactional
      engines. Such engines never transition to [`Error] or [`Abort],
      but always to [`Done] (corresponding to the fact that transactions
      handle errors so they do not propagate automatically). The [`Done]
      state has the original state value as argument, i.e. we see
      [`Done(`Done v)], [`Done(`Error e)], and [`Done `Aborted], so
      the value of the inner engine enclosed by the transaction handler is
      still visible.

   *)
  Uq_engines.when_state  (* OK *)
    ~is_done:(function
		| `Done arg -> 
		    ignore_lost_connection emit arg;
		    ondone arg
		| `Error error -> 
		    Netlog.logf `Err
		      "Error in %s: %s" procname (Netexn.to_string error);
		    ignore_lost_connection 
		      (Rpc_server.reply_error sess) Rpc.System_err;
		    onerror error
		| `Aborted ->
		    Netlog.logf `Err
		      "Execution of %s was aborted" procname;
		    ignore_lost_connection 
		      (Rpc_server.reply_error sess) Rpc.System_err;
		    onaborted()
	     )
    ~is_error:(fun error ->
		 Netlog.logf `Err
		   "Error in %s: %s" procname (Netexn.to_string error);
		 ignore_lost_connection 
		   (Rpc_server.reply_error sess) Rpc.System_err;
		 onerror error
	      )
    ~is_aborted:(fun () ->
		   Netlog.logf `Err
		     "Execution of %s was aborted" procname;
		   ignore_lost_connection 
		     (Rpc_server.reply_error sess) Rpc.System_err;
		   onaborted()
		)
    te


module Operators = struct
  let map_e = new Uq_engines.map_engine
  let eps_e = new Uq_engines.epsilon_engine
  let msync_e = msync_engine
  let qseq_e = q_seq_engine
  let ( ++ ) = new Uq_engines.seq_engine
  let ( >> ) = fmap_engine
    
(** Remember:
    
    - [ e1 ++ (fun r -> e2) ]
      means that we run [e1] first, and if it is successful with result [r],
      we start [e2]
    - [ e1 >> (fun s1 -> s2) ]
      means that we run [e1], and any result state [s1] is mapped into [s2]
      (including error states)
    - [ eps_e x esys ]
      "lifts" the value [x] into engine calculus
    - [ msync_e [e1;e2;...] esys ]
      waits until all argument engines e1,e2,... are finished
    - [ qseq_e q esys ]
      runs the engines in the queue [q] sequentially (destroying the queue)
 *)

end


(** Datatype helpers *)


let map_table f ht =
  Hashtbl.fold
    (fun x y acc -> Hashtbl.add acc x (f y); acc)
    ht
    (Hashtbl.create (Hashtbl.length ht))


let rev_table ht =
  (* inverts a hashtable (i.e. key->value becomes value->key) *)
  Hashtbl.fold
    (fun x y acc -> Hashtbl.add acc y x; acc)
    ht
    (Hashtbl.create (Hashtbl.length ht))





(** Compute/verify reservation IDs *)

let extract64 s =
  (** Extract 64 bits (as [int64]) from a string with at least 8 bytes *)
  Rtypes.int64_of_int8(Rtypes.read_int8 s 0)


let compute_verifier res_id res_secret range_start range_len rperm wperm =
  let s = 
    sprintf "%Ld/%Ld/%Ld/%Ld/%B/%B" 
      res_id
      res_secret
      range_start
      range_len
      rperm
      wperm in
  let h = Digest.string s in
  extract64 h



(** Simple compression for XDR data *)

(* Observation: there are many sequences of zero bytes.

   Scheme:

   compressed_data := zero_byte_descriptors 0x00 data1

   zero_byte_descriptors: for every zero byte in data1 we have here two
   bits, and the bits say how often the zero byte needs to be repeated
   (one to three times).
 *)

let xdr_compress data =
  let n = String.length data in
  let b1 = Buffer.create 80 in
  let b2 = Buffer.create 80 in
  let cur_descr = ref 0 in
  let cur_shift = ref 0 in
  let k = ref 0 in
  while !k < n do
    let c = data.[ !k ] in
    if c = '\000' then (
      let p = ref 1 in
      if !k+1 < n && data.[ !k+1 ] = '\000' then incr p;
      if !k+2 < n && data.[ !k+2 ] = '\000' then incr p;
      cur_descr := !cur_descr lor (!p lsl !cur_shift);
      cur_shift := !cur_shift + 2;
      if !cur_shift = 8 then (
        Buffer.add_char b1 (Char.chr !cur_descr);
        cur_descr := 0;
        cur_shift := 0;
      );
      Buffer.add_char b2 '\000';
      k := !k + !p;
    )
    else (
      let k1 = 
        try String.index_from data !k '\000' with Not_found -> n in
      Buffer.add_substring b2 data !k (k1 - !k);
      k := k1;
    )
  done;
  if !cur_shift > 0 then
    Buffer.add_char b1 (Char.chr !cur_descr);
  Buffer.add_char b1 '\000';
  Buffer.add_buffer b1 b2;
  Buffer.contents b1

let xdr_uncompress data =
  let zeros = String.make 3 '\000' in
  let n = String.length data in
  let k =
    try ref(String.index data '\000' + 1)
    with Not_found -> failwith "Plasma_util.xdr_uncompress: bad data" in
  let j = ref 1 in
  let cur_descr = ref (Char.code data.[ 0 ]) in
  let cur_shift = ref 0 in
  let b = Buffer.create (n + 2 * !k) in
  while !k < n do
    let c = data.[ !k ] in
    if c = '\000' then (
      let p = (!cur_descr lsr !cur_shift) land 3 in
      Buffer.add_substring b zeros 0 p;
      cur_shift := !cur_shift + 2;
      if !cur_shift = 8 then (
        cur_descr := Char.code data.[ !j ];
        cur_shift := 0;
        incr j;
      );
      incr k
    )
    else (
      let k1 = 
        try String.index_from data !k '\000' with Not_found -> n in
      Buffer.add_substring b data !k (k1 - !k);
      k := k1;
    )
  done;
  Buffer.contents b


(* Other idea:

   Stateful encoding. Mode X and mode Y.

   Mode X: bytes follow verbatim. Special meaning:
     - 0x80: switch to mode Y
     - 0x81: escape char (like backslash)

   Mode Y:
     - 0x00-0x7f: small number preceded by zeros:
          bits 5-6: number of preceding zero bytes
          bits 0-4: the number
     - 0x80: switch to mode X
     - 0x81 - 0x8f: the following 1-15 characters are literal
     - 0x90 - 0xef: the byte 0x20 - 0x7f
     - 0xf0 - 0xff: bits 2-3: number of zero bytes
                    bits 0-1: number of literal bytes
 *)


(** LRU cache *)

module LRU(Ord : Map.OrderedType) : sig
  type elt = Ord.t
  type t

  val create : unit -> t
    (** new cache *)

  val length : t -> int
    (** number of elements *)

  val is_empty : t -> bool
    (** whether empty *)

  val add : elt -> t -> unit
    (** adds a new element, or if it is already in the cache, marks it as
       "just used"
     *)

  val take : t -> elt
    (** takes the least recently used element (or raises Not_found) *)

  val mem : elt -> t -> bool
    (** whether member *)

  val clear : t -> unit
    (** removes all elements *)

  val remove : elt -> t -> unit
    (** removes an arbitrary element *)

end = struct
  type elt = Ord.t

  module M = Map.Make(Ord)
  module IS = Set.Make(Int64)
  module IM = Map.Make(Int64)

  type t =
      { mutable length : int;
	mutable max_age : int64;
	mutable age_of_elt : int64 M.t;
	mutable elt_of_age : elt IM.t;
	mutable ages : IS.t;
      }
   (* Note negative logic: if the age value grows, we consider the
      entry to be younger
    *)

  let create() =
    { length = 0;
      max_age = 0L;
      age_of_elt = M.empty;
      elt_of_age = IM.empty;
      ages = IS.empty;
    }

  let clear lru =
    lru.length <- 0;
    lru.max_age <- 0L;
    lru.age_of_elt <- M.empty;
    lru.elt_of_age <- IM.empty;
    lru.ages <- IS.empty

  let length lru =
    lru.length

  let is_empty lru =
    lru.length = 0

  let add p lru =
    ( try
	let age = M.find p lru.age_of_elt in
	lru.ages <- IS.remove age lru.ages;
	lru.elt_of_age <- IM.remove age lru.elt_of_age;
	lru.length <- lru.length - 1
      with
	| Not_found -> ()
    );
    let new_age = Int64.succ lru.max_age in
    lru.max_age <- new_age;
    lru.ages <- IS.add new_age lru.ages;
    lru.age_of_elt <- M.add p new_age lru.age_of_elt;
    lru.elt_of_age <- IM.add new_age p lru.elt_of_age;
    lru.length <- lru.length + 1
    
  let remove p lru =
    try
      let age = M.find p lru.age_of_elt in
      lru.ages <- IS.remove age lru.ages;
      lru.elt_of_age <- IM.remove age lru.elt_of_age;
      lru.age_of_elt <- M.remove p lru.age_of_elt;
      lru.length <- lru.length - 1
    with
      | Not_found -> ()

  let take lru =
    let age = IS.min_elt lru.ages in
    let p = IM.find age lru.elt_of_age in
    remove p lru;
    p

  let mem p lru =
    M.mem p lru.age_of_elt

end


(** Find ranges *)

(** Problem here is that there is a set of integers, and these should not
    be handled as individual elements of a set, but ranges of ints are
    the elements
 *)

module type ExtOrderedType = sig
  type t
  val compare : t -> t -> int
  val pred : t -> t option
  val succ : t -> t option
end


module I64 = struct
  type t = int64
  let compare = Int64.compare
  let pred n =
    if n = Int64.min_int then
      None
    else
      Some(Int64.pred n)
  let succ n =
    if n = Int64.max_int then
      None
    else
      Some(Int64.succ n)
end


module RangeSet(Ord : ExtOrderedType) : sig 
  type elt = Ord.t

  type t

  val empty : t

  val add : elt -> t -> t
    (** [add x s]: Returns the set where [x] is a new element *)

  val iter : (elt -> elt -> unit) -> t -> unit
    (** [iter f s]: applies [f] to all ranges in [s], i.e. pairs
	[(r1,r2)] so that all elements from [r1] to [r2] are member
	of the set, and [pred r1] is not a member, and [succ r2]
	is not a member either.
     *)
end = struct

  module M = Map.Make(Ord)

  type elt = Ord.t

  type t =
      { max_of_min : elt M.t;
	min_of_max : elt M.t;
	member : unit M.t
      }

  let empty =
    { max_of_min = M.empty;
      min_of_max = M.empty;
      member = M.empty
    }

  let add x s =
    if M.mem x s.member then
      s
    else (
      let preceding_range =
	match Ord.pred x with
	  | None -> None
	  | Some px ->
	      if M.mem px s.member then
		Some (M.find px s.min_of_max, px)
	      else
		None in
      let succeeding_range =
	match Ord.succ x with
	  | None -> None
	  | Some sx ->
	      if M.mem sx s.member then
		Some (sx, M.find sx s.max_of_min)
	      else
		None in
      match preceding_range, succeeding_range with
	| None, None ->
	    { max_of_min = M.add x x s.max_of_min;
	      min_of_max = M.add x x s.min_of_max;
	      member = M.add x () s.member
	    }
	| (Some(pmin,pmax)), None ->
	    { max_of_min = M.add pmin x s.max_of_min;
	      min_of_max = M.add x pmin (M.remove pmax s.min_of_max);
	      member = M.add x () s.member
	    }
	| None, (Some(smin,smax)) ->
	    { max_of_min = M.add x smax (M.remove smin s.max_of_min);
	      min_of_max = M.add smax x s.min_of_max;
	      member = M.add x () s.member
	    }
	| (Some(pmin,pmax)), (Some(smin,smax)) ->
	    { max_of_min = M.add pmin smax (M.remove smin s.max_of_min);
	      min_of_max = M.add smax pmin (M.remove pmax s.min_of_max);
	      member = M.add x () s.member
	    }
    )

  let iter f s =
    M.iter f s.max_of_min
  
end


(** Maps of ranges *)

(** Very similar to the set of ranges. As we want to map ranges to values,
    there is a new phenomenon, though. It can happen that the values are
    mergable or non-mergable. In case of the latter, the ranges
    (x, y) and (y+1, z) cannot be merged to a single range (x,z) because
    the mapped values are incompatible.

    Application: Blocklists. The key space are the file indexes.
    The values are the blocks on a single datanode. It can happen
    that key ranges are mapped to totally different ranges of blocks.
 *)

module type Mergable = sig
  type key
  type t
  val merge : t -> t -> t option
    (** Merges two values into one. This can be refused by returning [None] *)
  val split : key -> t -> t * t
    (** [let (v1,v2) = split k v]: Splits a value so that [v1] covers all
	keys < [k] and [v2] contains the rest. This function should fail
	if [v1] or [v2] would mean the empty set.
     *)

end

module I64RangeAtom = struct
  type key = int64
  type t = int64 * int64  (* from, to *)
  let merge (min1,max1) (min2,max2) =
    if Int64.succ max1 = min2 then
      Some(min1, max2)
    else if Int64.succ max2 = min1 then
      Some(min2, max1)
    else
      None
  let split p (min,max) =
    if p <= min || p > max then 
      failwith "Plasma_util.I64RangeAtom: cannot split";
    (min, Int64.pred p), (p, max)
end


(** Just an example of Mergable *)
module Trivial_blockrange = struct
  type key = int64
  type t =
      { idx_from : int64;
	block_from : int64;
	len : int
      }

  let split k block =
    let idx_end = Int64.add block.idx_from (Int64.of_int block.len) in
    if k <= block.idx_from || k >= idx_end then
      failwith "Plasma_util.Trivial_blockrange.split: cannot split";
    let left_lenL = Int64.sub k block.idx_from in
    let left =
      { idx_from = block.idx_from;
	block_from = block.block_from;
	len = Int64.to_int left_lenL;
      } in
    let right_lenL = Int64.sub idx_end k in
    let right =
      { idx_from = k;
	block_from = Int64.add block.block_from left_lenL;
	len = Int64.to_int right_lenL;
      } in
    (left, right)

  let merge b1 b2 =
    let idx_end1 = Int64.add b1.idx_from (Int64.of_int b1.len) in
    let idx_end2 = Int64.add b2.idx_from (Int64.of_int b2.len) in
    let block_end1 = Int64.add b1.block_from (Int64.of_int b1.len) in
    let block_end2 = Int64.add b2.block_from (Int64.of_int b2.len) in
    if idx_end1 = b2.idx_from && block_end1 = b2.block_from then
      Some {
	idx_from = b1.idx_from;
	block_from = b1.block_from;
	len = b1.len + b2.len
      }
    else
      if idx_end2 = b1.idx_from && block_end2 = b1.block_from then
	Some {
	  idx_from = b2.idx_from;
	  block_from = b2.block_from;
	  len = b1.len + b2.len
	}
      else
	None
end


module type RangeMapType = sig
  type point
  type key = point * point
  type value
  type t

  val empty : t

  val add : key -> value -> t -> t
    (** [add x y s]: Returns the map with the new binding [x] to [y]. If
	possible, the new binding is merged with existing bindings.
     *)

  val remove : key -> t -> t
    (** [remove k s]: Returns the map where the range [k] is removed.
	If [k] overlaps with bindings, these bindings are split, and only
	the intersection with [k] is removed.
     *)

  val sub : key -> t -> t
    (** [sub k s]: Returns the map with all bindings from [s] that fall
	into the range [k]. Bindings are split if necessary.
     *)

  val iter : (key -> value -> unit) -> t -> unit
    (** [iter f s]: applies [f] to all bindings in [s] *)

  val fold : (key -> value -> 'a -> 'a) -> t -> 'a -> 'a
    (** folding function *)

  val find : point -> t -> key * value
    (** Finds key and value for a point *)

end


module RangeMap(Ord : ExtOrderedType)(Val : Mergable with type key=Ord.t) 
  : RangeMapType with type point = Ord.t and type value = Val.t
  = 
struct
  module M = Map.Make(Ord)

  type point = Ord.t
  type key = Ord.t * Ord.t
  type value = Val.t

  type t =
      { max_of_min : Ord.t M.t;  (* if (x,y) is a key, this maps x to y *)
	min_of_max : Ord.t M.t;  (* if (x,y) is a key, this maps y to x *)
	value : value M.t        (* if (x,y) is a key, this maps x to value *)
      }
   (* CHECK: min_of_max is actually not needed. Remove it once we know
      more about future developments
    *)

  let ( << ) p q = Ord.compare p q < 0
  let ( <<= ) p q = Ord.compare p q <= 0
  let ( >> ) p q = Ord.compare p q > 0
  let ( >>= ) p q = Ord.compare p q >= 0

  let try_pred p = match Ord.pred p with Some x -> x | None -> raise Not_found
  let try_succ p = match Ord.succ p with Some x -> x | None -> raise Not_found

  let empty =
    { max_of_min = M.empty;
      min_of_max = M.empty;
      value = M.empty
    }

(*
  let find_lin p s = (* BAD *)
    let smallest = ref None in
    ( try
	M.iter
	  (fun q v ->
	     if q <<= p
	     then smallest := Some(q,v)
	     else raise Exit
	  )
	  s.value
      with Exit -> ()
    );
    match !smallest with
      | None -> raise Not_found
      | Some(q_min,v) ->
	  let q_max = M.find q_min s.max_of_min in
	  if p >> q_max then raise Not_found;
	  ((q_min, q_max), v)
 *)

  let find p s =
    (* We use here M.split (cost: O(log^2 n)). This is still asymptotically
       cheaper than iterating over the map. The GC is a bit more stressed,
       though.
     *)
    let s_left, match_opt, s_right = M.split p s.max_of_min in
    match match_opt with
      | None ->
	  let (q_min, q_max) = M.max_binding s_left in (* or Not_found *)
	  if p << q_min || p >> q_max then raise Not_found;
	  let v = M.find q_min s.value in
	  (q_min, q_max), v
      | Some p_max ->
	  let v = M.find p s.value in
	  (p, p_max), v

  let map_add_opt x y_opt m =
    match y_opt with
      | None -> m
      | Some y -> M.add x y m

  let split p s =
    (* Returns two sets s1 and s2, so that s1 contains all keys < p, and
       s2 contains all keys >= p

       FIXME:
       Possible optimization: we are actually only interested in either the left
       or the right return set, but not both.
     *)
    let (max_of_min_lt, max_of_min_p, max_of_min_gt) = 
      M.split p s.max_of_min in
    let max_of_min_ge =
      map_add_opt p max_of_min_p max_of_min_gt in
    let (min_of_max_lt, min_of_max_p, min_of_max_gt) =
      M.split p s.min_of_max in
    let min_of_max_ge =
      map_add_opt p min_of_max_p min_of_max_gt in
    let (value_lt, value_p, value_gt) =
      M.split p s.value in
    let value_ge =
      map_add_opt p value_p value_gt in
    try
      let p_pred = try_pred p in
      let (q_min, q_max) = M.max_binding max_of_min_lt in (* or Not_found *)
      if q_max << p then raise Not_found;  (* no real split necessary *)
      let (r_max, r_min) = M.min_binding min_of_max_ge in (* or Not_found *)
      if r_min >>= p then raise Not_found;  (* no real split necessary *)
      assert(q_min = r_min);
      assert(q_max = r_max);
      let v = M.find q_min value_lt in
      let (v_lt_opt, v_ge_opt) = 
	let (v_lt, v_ge) = Val.split p v in
	(Some v_lt, Some v_ge) in
      let s_left =
	{ max_of_min = M.add q_min p_pred max_of_min_lt;
	  min_of_max = M.add p_pred q_min min_of_max_lt;
	  value = map_add_opt q_min v_lt_opt value_lt
	} in
      let s_right =
	{ max_of_min = M.add p q_max max_of_min_ge;
	  min_of_max = M.add q_max p min_of_max_ge;
	  value = map_add_opt p v_ge_opt value_ge
	} in
      (s_left, s_right)
    with Not_found ->
      (* s can be decomposed into two subsets without overlap with p *)
      let s_left =
	{ max_of_min = max_of_min_lt;
	  min_of_max = min_of_max_lt;
	  value = value_lt
	} in
      let s_right =
	{ max_of_min = max_of_min_ge;
	  min_of_max = min_of_max_ge;
	  value = value_ge
	} in
      (s_left, s_right)


  let any k x_opt y_opt =
    if x_opt <> None then x_opt else y_opt

  let disjoint_union s1 s2 =
    (* works only if no real merge is required! *)
    { max_of_min = M.merge any s1.max_of_min s2.max_of_min;
      min_of_max = M.merge any s1.min_of_max s2.min_of_max;
      value = M.merge any s1.value s2.value
    }

  let remove (p1,p2) s =
    if p1 >> p2 then invalid_arg "Plasma_util.RangeMap.remove";
    let s_left_of_p1, s_right_of_p1 = split p1 s in
    let s_right_of_p2 = 
      match Ord.succ p2 with
	| None -> empty
	| Some p2_succ -> snd(split p2_succ s_right_of_p1) in
    disjoint_union s_left_of_p1 s_right_of_p2

  let sub (p1,p2) s =
    if p1 >> p2 then invalid_arg "Plasma_util.RangeMap.sub";
    let _, s_right_of_p1 = split p1 s in
    let s_center = 
      match Ord.succ p2 with
	| None -> s_right_of_p1
	| Some p2_succ -> fst(split p2_succ s_right_of_p1) in
    s_center

  let add (p1,p2) v s =
    if p1 >> p2 then invalid_arg "Plasma_util.RangeMap.add";
    (* First remove old stuff: *)
    let s1 = remove (p1,p2) s in
    (* Now there could be a range to the left we can merge with: *)
    let s2, is_added =
      try
	let p1_pred = try_pred p1 in  (* or Not_found *)
	let (q1_min,q1_max), v1 = find p1_pred s1 in  (* or Not_found *)
	assert (q1_max = p1_pred);  (* because we removed first *)
	let vm1 = 
	  match Val.merge v1 v with Some v -> v | None -> raise Not_found in
	(* Test whether we can go even one step further, and merge with
	   something on the right
	 *)
	try
	  let p2_succ = try_succ p2 in  (* or Not_found *)
	  let (q2_min,q2_max), v2 = find p2_succ s1 in  (* or Not_found *)
	  assert (q2_min = p2_succ);  (* because we removed first *)
	  let vm2 = 
	    match Val.merge vm1 v2 with Some v -> v | None -> raise Not_found in
	  (* Merge with both sides: *)
	  { max_of_min = M.add q1_min q2_max (M.remove q2_min s1.max_of_min);
	    min_of_max = M.add q2_max q1_min (M.remove q1_max s1.min_of_max);
	    value = M.add q1_min vm2 (M.remove q2_min s1.value)
	  }, true
	with Not_found ->
	  (* Do only the merge with the left *)
	  { max_of_min = M.add q1_min p2 s1.max_of_min;
	    min_of_max = M.add p2 q1_min (M.remove q1_max s1.min_of_max);
	    value = M.add q1_min vm1 s1.value
	  }, true
      with Not_found -> s1, false in
	
    (* Check for the merge on the right (but only if not yet checked) *)
    let s3 =
      try
	if is_added then raise Not_found;
	let p2_succ = try_succ p2 in  (* or Not_found *)
	let (q2_min,q2_max), v2 = find p2_succ s2 in  (* or Not_found *)
	assert (q2_min = p2_succ);  (* because we removed first *)
	let vm2 = 
	  match Val.merge v2 v with Some v -> v | None -> raise Not_found in
	  (* Do the merge with the right *)
	{ max_of_min = M.add p1 q2_max (M.remove q2_min s2.max_of_min);
	  min_of_max = M.add q2_max p1 s2.min_of_max;
	  value = M.add p1 vm2 (M.remove q2_min s2.value)
	}
      with Not_found -> 
	(* Just add without merging *)
	if is_added then
	  s2
	else
	  { max_of_min = M.add p1 p2 s2.max_of_min;
	    min_of_max = M.add p2 p1 s2.min_of_max;
	    value = M.add p1 v s2.value
	  } in
    s3

  let iter f s =
    M.iter 
      (fun q_min v ->
	 let q_max = M.find q_min s.max_of_min in
	 f (q_min,q_max) v
      )
      s.value

  let fold f s acc0 =
    M.fold
      (fun q_min v acc ->
	 let q_max = M.find q_min s.max_of_min in
	 f (q_min,q_max) v acc
      )
      s.value
      acc0

end


(** A RangeMap for the "trivial" representation where a range is just an
    interval [(min,max)] of [int64] numbers
 *)
module I64RM = RangeMap(I64)(I64RangeAtom)
