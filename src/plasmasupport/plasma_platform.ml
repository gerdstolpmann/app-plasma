(*
  Copyright 2011 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Netsys_mem

external blit_memory_unsafe :
           memory -> int -> memory -> int -> int -> unit
  = "plasma_blit_memory" "noalloc"

external fill_memory_unsafe :
           memory -> int -> int -> char -> unit
  = "plasma_fill_memory" "noalloc"

let blit_memory mem memoff dest destoff len =
  let memlen = Bigarray.Array1.dim mem in
  let destlen = Bigarray.Array1.dim dest in
  if len < 0 || memoff < 0 || memoff > memlen - len || 
     destoff < 0 || destoff > destlen - len 
  then
    invalid_arg "Plasma_platform.blit_memory";
  blit_memory_unsafe mem memoff dest destoff len

let fill_memory mem memoff len fill =
  let memlen = Bigarray.Array1.dim mem in
  if len < 0 || memoff < 0 || memoff > memlen - len then
    invalid_arg "Plasma_platform.blit_memory";
  fill_memory_unsafe mem memoff len fill

external plasma_sys_is_darwin : unit -> bool
  = "plasma_sys_is_darwin"

let sys_is_darwin = plasma_sys_is_darwin()
