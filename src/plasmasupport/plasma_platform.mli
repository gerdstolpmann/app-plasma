(*
  Copyright 2011 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Some C functions *)

val blit_memory : 
      Netsys_mem.memory -> int -> Netsys_mem.memory -> int -> int -> unit
  (** [blit_memory src srcoff dst dstoff len] copies [len] characters
      from buffer [src], starting at character number [srcoff], to
      string [dst], starting at character number [dstoff]

      Raise [Invalid_argument] if [srcoff] and [len] do not
      designate a valid subbuffer of [src], or if [dstoff] and [len]
      do not designate a valid substring of [dst].

      This function avoids the creation of a subarray like in
      {[
        Bigarray.Array1.blit
           (Bigarray.Array1.sub src srcoff len)
           (Bigarray.Array1.sub dst dstoff len)
      ]}
   *)

val fill_memory : Netsys_mem.memory -> int -> int -> char -> unit
  (** [fill_memory mem pos len x]: Fills the buffer [mem] from 
      [pos] to [pos+len-1] with character [x].
   *)

val sys_is_darwin : bool
  (** Whether we are running on Darwin (OS X) *)
