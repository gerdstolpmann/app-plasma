(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Users and groups *)

val req_admin_tables : string list
  (** Which admin tables are required (normally "passwd" and "group",
      but in the future more such tables could be defined)
   *)

class type ug_admin =
object
  method getpwnam : string -> Unix.passwd_entry
  method getpwuid : int -> Unix.passwd_entry
    (** Find a passwd entry by name or by uid *)

  method getgrnam : string -> Unix.group_entry
  method getgrgid : int -> Unix.group_entry
    (** Find a group entry by name or by gid *)

  method getgroups : string -> Plasma_util.StrSet.t
    (** [getgroups user]: Returns the set of effective group
	memberships for [user]
     *)

  method users : Unix.passwd_entry list
  method groups : Unix.group_entry list
    (** Return the users and groups *)

  method add_group : Unix.group_entry -> unit
    (** Add a group. If the name or gid of the group is already used,
	the previous definition is replaced.

	It is not checked whether the group members exist.
     *)

  method del_group : string -> unit
    (** Delete the group. The group is removed from all users where
	it is included in the list of supplementary groups. If the group
	is a primary group of a user, the removal will fail.
     *)

  method add_user : Unix.passwd_entry -> unit
    (** Add a user. The groups to which this user is added must already
	exist. If the name or uid of the user is already used, the
	previous definition is replaced.
     *)

  method del_user : string -> unit
    (** Delete the user *)

  method admin_tables : (string * string) list
    (** The contents of this object as admin tables. This method returns
	pairs [(table_name,table_contents)].
     *)
end


(** Subtype of [ug_admin] for read-only accesses (for shm) *)
class type ug_admin_ro =
object
  method getpwnam : string -> Unix.passwd_entry
  method getpwuid : int -> Unix.passwd_entry
    (** Find a passwd entry by name or by uid *)

  method getgrnam : string -> Unix.group_entry
  method getgrgid : int -> Unix.group_entry
    (** Find a group entry by name or by gid *)

  method getgroups : string -> Plasma_util.StrSet.t
    (** [getgroups user]: Returns the set of effective group
	memberships for [user]
     *)

  method users : Unix.passwd_entry list
  method groups : Unix.group_entry list
    (** Return the users and groups *)

  method admin_tables : (string * string) list
    (** The contents of this object as admin tables. This method returns
	pairs [(table_name,table_contents)].
     *)
end


val parse_ug_admin : (string * string) list -> ug_admin
  (** Parses the admin tables passed as argument (as pairs 
      [(table_name,table_contents)]). The tables denoted by 
      the function [req_admin_tables] must be passed as minimum.

      Unparseable lines are silently ignored.

      User entries referring to nonexisting group entries are silently
      ignored.
   *)


(** {2 Authentication tickets} *)

(** An authentication ticket is a string

    {[ <type>:<rpc_user>:<rpc_password>:<user>:<group>:<supp_groups>:<verifier> ]}

    where:
    - [type] is the string [SCRAM-SHA1] (identifying the authentication scheme)
    - [rpc_user]: this is a suggestion for a user ID to authenticate as on
      the RPC level
    - [rpc_password]: the password of [rpc_user]
    - [user]: the user name of the principal the ticket is permitting
    - [group]: the group name of the principal
    - [supp_groups]: a comma-separated list of further groups
    - [verifier]: an int64 verifying the ticket (from {!Nn_authticket})

    The strings (except [type]) are Base64-encoded.
 *)

val serialize_auth_ticket :
      rpc_user:string -> 
      rpc_password:string ->
      user:string ->
      group:string ->
      supp_groups:Plasma_util.StrSet.t ->
      verifier:int64 ->
        string

val rpc_login_of_auth_ticket : string -> string * string
  (** Returns [(rpc_user,rpc_password)] *)

val principal_of_auth_ticket : string -> string * string * Plasma_util.StrSet.t
  (** Returns [(user,group,supp_groups)] *)

val verifier_of_auth_ticket : string -> int64
  (** Returns [(verifier)] *)

