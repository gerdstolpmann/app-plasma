(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

class type dn_shm_config =
object
  method dn_blocksize : int
  method dn_shm_queue_length : int
end


type dn_shm = 
    { length : int;
      blocksize : int;
      mem : Netsys_mem.memory
    }

let create (conf : dn_shm_config) =
  let size = conf#dn_blocksize * conf#dn_shm_queue_length in
  let (fd, n) = Plasma_util.get_shm_fd "plasma" 0 in
  try
    Plasma_util.enlarge_shm fd size;
    Unix.close fd;
    n
  with
    | error ->
        (* On OS X, a just created shm descriptor cannot be closed w/o error.
           One gets EINVAL, but the descriptor is finally closed.
         *)
	( try Unix.close fd with _ -> () );
        raise error
  
let unlink name =
  try
    Netsys_posix.shm_unlink name
  with
    | Unix.Unix_error(Unix.ENOENT,_,_) -> ()

let openshm conf name =
  let size = conf#dn_blocksize * conf#dn_shm_queue_length in
  let fd = Plasma_util.open_shm_fd name in
  try
    let mem = Plasma_util.map_shm fd 0L size in
    Unix.close fd;
    { length = conf#dn_shm_queue_length;
      blocksize = conf#dn_blocksize;
      mem = mem
    }
  with
    | error ->
	Unix.close fd;
	raise error

let length shm =
  shm.length

let blocksize shm =
  shm.blocksize

let slot shm i =
  if i < 0 || i >= shm.length then
    failwith "Dn_shm.slot: bad index";
  let p = i * shm.blocksize in
  (shm.mem, p)
    
