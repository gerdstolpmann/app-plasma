(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** I/O helper processes. These implement the [Datanode_io] program.
    This implementation is synchronous, i.e. only one request can be done
    at a time. In order to achieve parallelism, several I/O helper processes
    should be started.
 *)

let dlogf = Plasma_util.dlogf

module Rpcapi_clnt = Pfs_rpcapi_clnt
module Rpcapi_srv = Pfs_rpcapi_srv
module Rpcapi_aux = Pfs_rpcapi_aux

module Dn = Pfs_rpcapi_clnt.Make'Datanode(Rpc_proxy.ManagedClient)

module Dn_store_var =
  Netplex_cenv.Make_var_type(struct type t = Dn_store.dn_store end)

module Dn_shm_var =
  Netplex_cenv.Make_var_type(struct type t = Dn_shm.dn_shm end)

let current_dn_store() =
  Dn_store_var.get "current_dn_store"

let current_dn_shm() =
  Dn_shm_var.get "current_dn_shm"

type clients =
    (Unix.sockaddr, Rpc_proxy.ManagedClient.t) Hashtbl.t

module Dn_clients_var =
   Netplex_cenv.Make_var_type(struct type t = clients end)
     
let current_dn_clients() =
  Dn_clients_var.get "current_dn_clients"


let proc_read conf (slot, block, pos, len) =
  (** Implementation of the [read] RPC: Do the read, and put the result
      data directly into the shm slot.
   *)
  let store = current_dn_store() in
  let shm = current_dn_shm() in
  let (mem,mpos) = Dn_shm.slot shm slot in
  try
    Dn_store.read_block store block pos mem mpos len
  with
    | error ->
	Netlog.logf `Err
	  "proc_read: exception %s for block=%Ld pos=%d len=%d"
	  (Netexn.to_string error) block pos len;
	raise (Rpc.Rpc_server Rpc.System_err)
  
let proc_read_shm conf (shm_obj, block, pos, len) =
  if shm_obj.Rpcapi_aux.shm_length < len then
    failwith "proc_read_shm: shm object too small";
  if shm_obj.Rpcapi_aux.shm_offset < 0L then
    failwith "proc_read_shm: negative shm offset";
  let mem_fd = Plasma_util.open_shm_fd shm_obj.Rpcapi_aux.shm_path in
  let mem = 
    try
      Plasma_util.map_shm
	mem_fd shm_obj.Rpcapi_aux.shm_offset shm_obj.Rpcapi_aux.shm_length 
    with error -> 
      ( try Unix.close mem_fd with _ -> () );
      raise error in
  Unix.close mem_fd;
  let store = current_dn_store() in
  try
    Dn_store.read_block store block pos mem 0 len;
    Netsys_mem.memory_unmap_file mem
  with
    | error ->
	Netlog.logf `Err
	  "proc_read_shm: exception %s for block=%Ld pos=%d len=%d \
             shm_offset=%Ld shm_length=%d"
	  (Netexn.to_string error) block pos len shm_obj.Rpcapi_aux.shm_offset
	  shm_obj.Rpcapi_aux.shm_length;
	raise (Rpc.Rpc_server Rpc.System_err)

  
let proc_write conf (slot, block) =
  (** Implementation of the [write] RPC: Do the write, and take the write
      data directly from the shm slot.
   *)
  let store = current_dn_store() in
  let shm = current_dn_shm() in
  let (mem,mpos) = Dn_shm.slot shm slot in
  try
    Dn_store.write_block store block mem mpos
  with
    | error ->
	Netlog.logf `Err
	  "proc_write: exception %s for block=%Ld"
	  (Netexn.to_string error) block;
	raise (Rpc.Rpc_server Rpc.System_err)

let proc_write_shm conf (shm_obj, block) =
  let store = current_dn_store() in
  if shm_obj.Rpcapi_aux.shm_length < Dn_store.get_blocksize store then
    failwith "proc_write_shm: shm object too small";
  if shm_obj.Rpcapi_aux.shm_offset < 0L then
    failwith "proc_write_shm: negative shm offset";
  let mem_fd = Plasma_util.open_shm_fd shm_obj.Rpcapi_aux.shm_path in
  let mem =
    try
      Plasma_util.map_shm
	mem_fd shm_obj.Rpcapi_aux.shm_offset shm_obj.Rpcapi_aux.shm_length 
    with error -> 
      ( try Unix.close mem_fd with _ -> () );
      raise error in
  Unix.close mem_fd;
  try
    Dn_store.write_block store block mem 0;
    Netsys_mem.memory_unmap_file mem
  with
    | error ->
	Netlog.logf `Err
	  "proc_write_shm: exception %s for block=%Ld \
             shm_offset=%Ld shm_length=%d"
	  (Netexn.to_string error) block shm_obj.Rpcapi_aux.shm_offset
	  shm_obj.Rpcapi_aux.shm_length;
	raise (Rpc.Rpc_server Rpc.System_err)

let create_dn_client conn =
  let esys = Unixqueue.create_unix_event_system() in
  let mclient_config =
    Rpc_proxy.ManagedClient.create_mclient_config
      ~programs:[ Pfs_rpcapi_clnt.Datanode.V1._program ]
      ~msg_timeout:(300.0)
      ~msg_timeout_is_fatal:true
      ~initial_ping:true
      ~idle_timeout:15.0
      () in
  let mclient =
    Rpc_proxy.ManagedClient.create_mclient
      mclient_config
      conn
      esys in
  mclient

let get_dn_client node =
  let clients = current_dn_clients() in
  let (host,port) = Plasma_util.parse_host_port node in
  let saddr = Plasma_util.sockaddr_of_host_port (host,port) in
  try
    Hashtbl.find clients saddr
  with
    | Not_found ->
	let client = 
	  create_dn_client (Plasma_util.connector_of_sockaddr saddr) in
	Hashtbl.replace clients saddr client;
	client

let check_clients() =
  (* do a "run" on the clients so idle connections are closed *)
  let clients = current_dn_clients() in
  Hashtbl.iter
    (fun _ client ->
       let esys = Rpc_proxy.ManagedClient.event_system client in
       Unixqueue.run esys
    )
    clients

let proc_copy conf (block,dest_node,dest_ident,dest_block,
		    dest_ticket,slot) =
  (** Implementation of the [copy] RPC: First check if this is a local
      copy.
   *)
  let store = current_dn_store() in
  let identity = Dn_store.get_identity store in
  (** Allocate a buffer for copying: *)
  let shm = current_dn_shm() in
  let (mem,mpos) = Dn_shm.slot shm slot in
  (** Read the block: *)
  Dn_store.read_block store block 0 mem 0 conf#dn_blocksize;
  if identity = dest_ident then (
    (** Case local copy: Just write *)
    Dn_store.write_block store dest_block mem 0;
  )
  else (
    (** Case remote copy: *)
    let ms = 
      Xdr_mstring.memory_based_mstrings # create_from_memory
	mem 0 conf#dn_blocksize false in
    let client = get_dn_client dest_node in
    Dn.V1.write client 
      (dest_block,`dnch_rpc ms,dest_ticket)
  )

let proc_sync conf () =
  (** Implementation of the [sync] RPC: Just call [fsync] *)
  let store = current_dn_store() in
  Dn_store.sync store

let proc_size conf () =
  (** Implementation of the [size] RPC: Return this value *)
  let store = current_dn_store() in
  Dn_store.get_size store

let proc_identity conf () =
  (** Implementation of the [identity] RPC: Return this value *)
  let store = current_dn_store() in
  Dn_store.get_identity store


let setup srv (conf : Dn_config.dn_config) =
  Rpcapi_srv.Datanode_io.V1.bind
    ~proc_null:(fun () -> ())
    ~proc_read:(proc_read conf)
    ~proc_read_shm:(proc_read_shm conf)
    ~proc_write:(proc_write conf)
    ~proc_write_shm:(proc_write_shm conf)
    ~proc_copy:(proc_copy conf)
    ~proc_sync:(proc_sync conf)
    ~proc_size:(proc_size conf)
    ~proc_identity:(proc_identity conf)
    srv


let rec get_plasma_datanode_shm_name () =
  (** Get the value of the sharedvar variable plasma.datanode.shm.name.
      If the variable is still unset, busy wait on it
   *)
  let v_opt = 
    Netplex_sharedvar.wait_for_value "plasma.datanode.shm.name" in
  match v_opt with
    | None ->
	Netsys.sleep 0.1;
	get_plasma_datanode_shm_name ()
    | Some shm_name ->
	shm_name


let factory() =
  Rpc_netplex.rpc_factory
    ~configure:(fun cf _ -> Dn_config.extract_dn_config cf)
    ~hooks:(fun conf ->
	      ( object
		  inherit Netplex_kit.empty_processor_hooks()

		  method post_add_hook _ ctrl =
		    ctrl # add_plugin Netplex_sharedvar.plugin

		  method post_start_hook cont =
		    (** Open the store, and put a reference to it into a
                       container variable where [current_dn_store()] will
                       expect it. There is no easier way to pass this down.
		     *)
		    let store = 
		      Dn_store.open_store (conf :> Dn_store.dn_store_config) in
		    Dn_store_var.set "current_dn_store" store;

		    (** Get the name of the shm object. *)
		    let shm_name = get_plasma_datanode_shm_name () in
		    
		    (** Open the shm object, and put the reference to it
                        into a container variable, too.
		     *)
		    let shm =
		      Dn_shm.openshm
			(conf :> Dn_shm.dn_shm_config) 
			shm_name in
		    Dn_shm_var.set "current_dn_shm" shm;

		    let clients = Hashtbl.create 5 in
		    Dn_clients_var.set "current_dn_clients" clients;

		    let _tm =
		      Netplex_cenv.create_timer
			(fun _ -> check_clients(); true)
			1.0 in
		    ()
		      

		  method pre_finish_hook cont =
		    let store = current_dn_store() in
		    Dn_store.close_store store
		end
	      )
	   )
    ~name:"dn_io"
    ~setup
    ()

let add_service conf cf cfaddr ctrl =
  (* FIXME: There should be better functions for creating these objects in
     Netplex
   *)
  let sockpath =
    ctrl#controller_config#socket_directory ^ "/dn_io.rpc" in
  let sockaddr =
    `Socket (Unix.ADDR_UNIX sockpath) in
  let protocol : Netplex_types.protocol =
    Netplex_kit.create_protocol
       ~lstn_backlog:(2 * conf#dn_io_processes)
       "RPC" [| sockaddr |] in
  let socksrv_conf : Netplex_types.socket_service_config =
    Netplex_kit.create_socket_service_config
       "Dn_io" [protocol] ctrl#controller_config in
  let facobj = factory() in
  let processor = 
    facobj # create_processor 
      ctrl#controller_config 
      cf cfaddr in
  let socksrv = 
    Netplex_sockserv.create_socket_service
      processor
      socksrv_conf in
  let wm = 
    Netplex_workload.create_constant_workload_manager
      conf#dn_io_processes in
  ctrl # add_service socksrv wm
