(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type dn_config =
object
  method dn_clustername : string
  method dn_directory : string
  method dn_blocksize : int
  method dn_io_processes : int
  method dn_shm_queue_length : int
  method dn_sync_period : float
  method dn_multicast_group : Unix.inet_addr option
end


let extract_dn_config cf =
  let datanodes =
    cf#resolve_section cf#root_addr "datanode" in
  match datanodes with
    | [] ->
	failwith "Missing 'datanode' config"
    | _ :: _ :: _ ->
	failwith "More than one 'datanode' config"
    | [datanode] ->
	let param name =
	  try cf#resolve_parameter datanode name
	  with Not_found ->
	    failwith ("Missing parameter: datanode." ^ name) in
	let clustername =
	  cf#string_param (param "clustername") in
	let directory =
	  cf#string_param (param "directory") in
	let blocksize =
	  cf#int_param (param "blocksize") in
	let io_processes =
	  cf#int_param (param "io_processes") in
	let shm_queue_length =
	  cf#int_param (param "shm_queue_length") in
	let sync_period =
	  cf#float_param (param "sync_period") in
	let dn_mcast_group =
	  try
	    Some(Unix.inet_addr_of_string
		   (cf#string_param
		      (cf#resolve_parameter datanode "multicast_group")))
	  with
	    | Not_found ->
		None
	    | _ ->
		failwith ("Error parsing parameter: " ^ 
			    cf#print datanode ^ ".multicast_group") in
	( object
	    method dn_clustername = clustername
	    method dn_directory = directory
	    method dn_blocksize = blocksize
	    method dn_io_processes = io_processes
	    method dn_shm_queue_length = shm_queue_length
	    method dn_sync_period = sync_period
	    method dn_multicast_group = dn_mcast_group
	  end
	)

	
	


