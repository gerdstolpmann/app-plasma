(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(** Shared memory for transmitting data from the main process to the
    I/O processes and vice versa
 *)

class type dn_shm_config =
object
  method dn_blocksize : int
    (** The blocksize. This value is used for new stores, and only existing
        stores can be opened that have the same blocksize
     *)
  method dn_shm_queue_length : int
    (** The size of the shm queue, in blocks. Should be about two times
        the maximum number of dn_io processes
     *)
end

type dn_shm


val create : dn_shm_config -> string
  (** Creates the shm object and returns its name *)

val unlink : string -> unit
  (** Unlinks the name. Failsafe *)

val openshm : dn_shm_config -> string -> dn_shm
  (** Maps the shm object *)

val length : dn_shm -> int
  (** Returns [dn_shm_queue_length] *)

val blocksize : dn_shm -> int
  (** Returns [dn_blocksize] *)

val slot : dn_shm -> int -> Netsys_mem.memory * int
  (** [let (mem,pos) = slot shm i]: Accesses the slot [i] of the shm
      object [shm]. Requires that [ 0 <= i < length ]. The slot occupies
      the byte range [pos] to [pos+blocksize-1] in [mem]
   *)


