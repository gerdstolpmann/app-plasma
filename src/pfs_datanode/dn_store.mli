(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type dn_store_config =
object
  method dn_directory : string
    (** The directory containing the store. There are two files:
        - config: first line contains the identity string. Second line
          contains the block size
        - data: one big file with the data blocks
     *)
  method dn_blocksize : int
    (** The blocksize. This value is used for new stores, and only existing
        stores can be opened that have the same blocksize
     *)
end


val init_store : dn_store_config -> string -> int64 -> unit
  (** [init_dn_store config identity size]:
      Initializes the store. The directory must already exist. The files
      [config] and [data] must not exist.
   *)

type dn_store

val open_store : dn_store_config -> dn_store
  (** Opens the store *)

val close_store : dn_store -> unit
  (** Closes the store *)

val get_identity : dn_store -> string
  (** Returns the identity string *)

val get_blocksize : dn_store -> int
  (** Returns the block size *)

val get_size : dn_store -> int64
  (** Returns the number of blocks *)

val read_block : dn_store -> int64 -> int -> Netsys_mem.memory -> int -> 
                 int -> unit
  (** [read_block st block blockpos mem mpos len]: Reads the bytes
      [blockpos] to [blockpos+len-1] from [block] into the range
      [mpos] to [mpos+len-1] of [mem].
   *)

val write_block : dn_store -> int64 -> Netsys_mem.memory -> int -> unit
  (** [write_block st block blockpos mem mpos]: Writes a complete block out.
      The block data are in [mem] from byte position [mpos] to 
      [mpos+blocksize-1]. The data is written into [block].
   *)

val will_read_block : dn_store -> int64 -> unit
  (** [will_read_block st block]: announces that this block will be read
      in the near future
   *)

val sync : dn_store -> unit
  (** Syncs the store to disk *)

