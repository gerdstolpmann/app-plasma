(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

class type dn_config =
object
  method dn_clustername : string
    (** The clustername *)
  method dn_directory : string
    (** The directory containing the store. There are two files:
        - config: first line contains the identity string. Second line
          contains the block size
        - data: one big file with the data blocks
     *)
  method dn_blocksize : int
    (** The blocksize. This value is used for new stores, and only existing
        stores can be opened that have the same blocksize
     *)
  method dn_io_processes : int
    (** The number of dn_io processes. *)
  method dn_shm_queue_length : int
    (** The size of the shm queue, in blocks. Should be about two times
        the maximum number of dn_io processes
     *)
  method dn_sync_period : float
    (** How often the file is synced to disk *)
  method dn_multicast_group : Unix.inet_addr option
    (** The multicast group to join *)
end


val extract_dn_config : Netplex_types.config_file -> dn_config
  (** Config section must look like (see {!Dn_config.dn_config}
      for explanations):

      {[
         netplex {
           ...
	   datanode {
	    clustername = "<name>";
	    directory = "<data_dir>";
	    blocksize = <blocksize>;  (* int *)
	    io_processes = <p>;       (* int *)
	    shm_queue_length = <q>;   (* int *)
	    sync_period = <s>;        (* float *)
	  };
          ...
        }
      ]}
  *)

