(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let unopt =
  function
    | None -> assert false
    | Some x -> x

let random() =
  let s = Plasma_rng.random_bytes 16 in
  Digest.to_hex s


let () =
  let directory = ref None in
  let blocksize = ref (1024 * 1024) in
  let size = ref None in

  Arg.parse
    [ "-blocksize", Arg.Set_int blocksize,
      "<n>   Set blocksize";
    ]
    (fun s ->
       if !directory = None then
	 directory := Some s
       else
	 if !size = None then
	   size := Some(Int64.of_string s)
	 else
	   raise(Arg.Bad ("Unexpected argument: "  ^ s))
    )
    (sprintf "%s [options] <directory> <size>\n\
              \n\
              Initialize the datanode <directory> with <size> blocks\n"
       (Filename.basename Sys.argv.(0)));

  if !directory = None || !size = None then
    failwith "Bad usage. Use -help to get usage!";
  
  let config =
    ( object
	method dn_directory = unopt !directory
	method dn_blocksize = !blocksize
      end
    ) in
  let identity = random() in
  Dn_store.init_store config identity (unopt !size);

  printf "Initialized datanode directory %s with identity %s\n"
    config#dn_directory identity
