(*
  Copyright 2010 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

let usage() =
  prerr_endline "usage: plasma_admin <mode> [-help|options]";
  prerr_endline "";
  prerr_endline "  available modes:";
  prerr_endline "  - add_datanode";
  prerr_endline "  - enable_datanode";
  prerr_endline "  - disable_datanode";
  prerr_endline "  - list_datanodes";
  prerr_endline "  - check_datanode";
  prerr_endline "  - destroy_datanode";
  prerr_endline "  - create_checkpoint";
  prerr_endline "  - install_checkpoint";
  prerr_endline "  - fsck"


let auth_ht = Hashtbl.create 5


let get_pw u =
  try
    Hashtbl.find auth_ht u
  with Not_found ->
    try
      Sys.getenv ("PLASMA_PASSWD_" ^ u)
    with Not_found ->
      let pw = ref "" in
      Netsys_posix.with_tty
	(fun tty ->
	   pw := (Netsys_posix.tty_read_password ~tty
		    ("Plasma password of " ^ u ^ "? "))
	);
      Hashtbl.add auth_ht u !pw;
      !pw


let auth client =
  let client_key_ring =
    ( object
        method password_of_user_name n =
	  get_pw n
        method default_user_name =
          Some Pfs_auth.privileged_user
      end
    ) in
  let gss_api =
    new Netmech_scram_gssapi.scram_gss_api
      ~client_key_ring
      (Netmech_scram.profile `GSSAPI) in
  let m =
    Rpc_auth_gssapi.client_auth_method
      ~privacy:`Required
      ~user_name_interpretation:(`Plain_name Netgssapi.nt_user_name)
      gss_api
      Netmech_scram_gssapi.scram_mech in
  Rpc_client.set_auth_methods client [m]


let find_coord namenode clustername =
  if namenode = "" then
    failwith "No namenode specified (parameter -namenode <host>:<port>)";
  if clustername = "" then
    failwith "No clustername specified (parameter -cluster <name>)";
  let conn =
    Plasma_util.connector_of_sockaddr
      (Plasma_util.sockaddr_of_host_port 
	 (Plasma_util.parse_host_port namenode)) in
  let client =
    Pfs_rpcapi_clnt.Coordination.V1.create_client2
      (`Socket(Rpc.Tcp, conn, Rpc_client.default_socket_config)) in
  auth client;
  ( try
      let c_opt =
	Pfs_rpcapi_clnt.Coordination.V1.find_coordinator client clustername in
      match c_opt with
	| None ->
	    failwith ("Cannot find coordinator for cluster " ^ clustername)
	| Some hp ->
	    Rpc_client.shut_down client;
	    hp
    with
      | error ->
	  prerr_endline ("ERROR: " ^ Netexn.to_string error);
	  exit 2
  )


let with_dn_admin namenode f =
  let conn = 
    Plasma_util.connector_of_sockaddr
      (Plasma_util.sockaddr_of_host_port 
	 (Plasma_util.parse_host_port namenode)) in
  let client =
    Pfs_rpcapi_clnt.Dn_admin.V1.create_client2
      (`Socket(Rpc.Tcp, conn, Rpc_client.default_socket_config)) in
  auth client;
  ( try
      let () = f client in
      print_endline "OK"
    with
      | error ->
	  prerr_endline ("ERROR: " ^ Netexn.to_string error);
	  exit 2
  );
  Rpc_client.shut_down client


let with_nn_namesync namenode f =
  let conn = 
    Plasma_util.connector_of_sockaddr
      (Plasma_util.sockaddr_of_host_port 
	 (Plasma_util.parse_host_port namenode)) in
  let client =
    Pfs_rpcapi_clnt.Namesync.V1.create_client2
      (`Socket(Rpc.Tcp, conn, Rpc_client.default_socket_config)) in
  auth client;
  ( try
      let () = f client in
      print_endline "OK"
    with
      | error ->
	  prerr_endline ("ERROR: " ^ Netexn.to_string error);
	  exit 2
  );
  Rpc_client.shut_down client


let add_datanode() =
  let namenode = ref "" in
  let cluster = ref "" in
  let size = ref 0L in
  let identities = ref [] in
  Arg.parse
    [ "-namenode", Arg.Set_string namenode,
      "<host>:<port>    Set the namenode to contact";

      "-cluster", Arg.Set_string cluster,
      "<name>     Set the clustername";

      "-size", Arg.String (fun s -> size := Int64.of_string s),
      "<blocks>   Set the size of the datanode to add (required option)";
    ]
    (fun id ->
       identities := id :: !identities)
    "usage: plasma_admin add_datanode -size <blocks> <identity> ...";

  if !size <= 0L then
    failwith "No -size or bad -size argument";

  let coord = find_coord !namenode !cluster in
  with_dn_admin coord
    (fun client ->
       List.iter
	 (fun id ->
	    Pfs_rpcapi_clnt.Dn_admin.V1.add_datanode client (id, !size)
	 )
	 !identities
    )
    
    
let enable_datanode() =
  let namenode = ref "" in
  let cluster = ref "" in
  let identity = ref "" in
  let datanode = ref "" in
  Arg.parse
    [ "-namenode", Arg.Set_string namenode,
      "<host>:<port>    Set the namenode to contact";

      "-cluster", Arg.Set_string cluster,
      "<name>     Set the clustername";
    ]
    (fun s ->
       if !identity = "" then 
	 identity := s
       else
	 if !datanode = "" then
	   datanode := s
	 else
	   raise (Arg.Bad("Extra argument: " ^ s))
    )
    "usage: plasma_admin enable_datanode <identity> <datanodehost:port>";

  if !identity = "" || !datanode = "" then
    failwith "Missing arguments";

  let coord = find_coord !namenode !cluster in
  with_dn_admin coord
    (fun client ->
       Pfs_rpcapi_clnt.Dn_admin.V1.enable_datanode client (!identity, !datanode)
    )


let disable_datanode() =
  let namenode = ref "" in
  let cluster = ref "" in
  let identity = ref "" in
  Arg.parse
    [ "-namenode", Arg.Set_string namenode,
      "<host>:<port>    Set the namenode to contact";

      "-cluster", Arg.Set_string cluster,
      "<name>     Set the clustername";
    ]
    (fun s ->
       if !identity = "" then 
	 identity := s
       else
	 raise (Arg.Bad("Extra argument: " ^ s))
    )
    "usage: plasma_admin disable_datanode <identity>";

  if !identity = "" then
    failwith "Missing argument";

  let coord = find_coord !namenode !cluster in
  with_dn_admin coord
    (fun client ->
       Pfs_rpcapi_clnt.Dn_admin.V1.disable_datanode client !identity
    )


let list_datanodes() = 
  let namenode = ref "" in
  let cluster = ref "" in
  let blocks = ref false in
  Arg.parse
    [ "-namenode", Arg.Set_string namenode,
      "<host>:<port>    Set the namenode to contact";

      "-cluster", Arg.Set_string cluster,
      "<name>     Set the clustername";

      "-blocks", Arg.Set blocks,
      "     Also print absolute block numbers";
    ]
    (fun s ->
       raise (Arg.Bad("Extra argument: " ^ s)))
    "usage: plasma_admin list_datanodes";

  let coord = find_coord !namenode !cluster in
  with_dn_admin coord
    (fun client ->
       let identities =
	 Pfs_rpcapi_clnt.Dn_admin.V1.datanodes client () in
       Array.iter
	 (fun id ->
	    let enabled =
	      Pfs_rpcapi_clnt.Dn_admin.V1.is_enabled client id in
	    let alive =
	      Pfs_rpcapi_clnt.Dn_admin.V1.is_alive client id in
	    let hostport =
	      Pfs_rpcapi_clnt.Dn_admin.V1.lookup client id in
	    let st =
	      Pfs_rpcapi_clnt.Dn_admin.V1.fsstat client id in
	    let all_used =
	      Int64.add 
		st.Pfs_rpcapi_aux.used_blocks st.Pfs_rpcapi_aux.trans_blocks in
	    let percentage =
	      Int64.to_float all_used /. 
		Int64.to_float st.Pfs_rpcapi_aux.total_blocks in
	    printf "%-32s  %-10s  %3d%%  %s%s\n"
	      id
	      (if enabled then
		 if alive then "alive" else "DEAD"
	       else
		 "disabled"
	      )
	      (truncate (100.0 *. percentage))
	      (if !blocks then
		 sprintf "%8Ld  %8Ld  " all_used st.Pfs_rpcapi_aux.total_blocks
	       else "")
	      (if hostport = "" then "not associated to datanode" else hostport)
	 )
	 identities
    )


let check_datanode() =
  failwith "not implemented"


let destroy_datanode() =
  let namenode = ref "" in
  let cluster = ref "" in
  let identity = ref "" in
  Arg.parse
    [ "-namenode", Arg.Set_string namenode,
      "<host>:<port>    Set the namenode to contact";

      "-cluster", Arg.Set_string cluster,
      "<name>     Set the clustername";
    ]
    (fun s ->
       if !identity = "" then 
	 identity := s
       else
	 raise (Arg.Bad("Extra argument: " ^ s))
    )
    "usage: plasma_admin destroy_datanode <identity>";

  if !identity = "" then
    failwith "Missing argument";

  let coord = find_coord !namenode !cluster in
  with_dn_admin coord
    (fun client ->
       Pfs_rpcapi_clnt.Dn_admin.V1.destroy_datanode client !identity
    )


let rec cp_wait client dump =
  let st = 
    Pfs_rpcapi_clnt.Namesync.V1.get_checkpointing_status client dump in
  if st = `etbusy then (
    Unix.sleep 1;
    cp_wait client dump 
  )
  else
    st


let cp_print st =
  match st with
    | `ok -> printf "Finished.\n%!"
    | `enoent -> printf "Checkpoint not found.\n%!"
    | `estale -> printf "No status data for this checkpoint available.\n%!"
    | `efailed -> printf "Error.\n%!"
    | #Plasma_util.errno as e -> printf "Error code %s.%!"
                  (Plasma_util.string_of_errno e)


let create_checkpoint() =
  let namenode = ref "" in
  let cluster = ref "" in
  let wait = ref true in
  Arg.parse
    [ "-namenode", Arg.Set_string namenode,
      "<host>:<port>    Set the namenode to contact";

      "-cluster", Arg.Set_string cluster,
      "<name>     Set the clustername";

      "-no-wait", Arg.Clear wait,
      "     Do not wait until the checkpointing operation finishes";
    ]
    (fun s ->
       raise (Arg.Bad("Extra argument: " ^ s))
    )
    "usage: plasma_admin create_checkpoint";
  let coord = find_coord !namenode !cluster in
  with_nn_namesync coord
    (fun client ->
       let st1 = Pfs_rpcapi_clnt.Namesync.V1.create_checkpoint client () in
       match st1 with
         | `ok dump ->
              let triple = 
                Pfs_rpcapi_aux.
                  (dump.dump_prefix, dump.dump_ref, dump.dump_suffix) in
              let tok = Nn_dumps.create_token triple in
              printf "Checkpoint: %s\n%!" tok;
              if !wait then (
                let st2 = cp_wait client dump in
                cp_print st2
              )
         | `econflict ->
              failwith "Another checkpoint is in progress"
         | `eperm ->
              failwith "Checkpoints currently blocked"              
         | #Plasma_util.errno as e ->
              failwith ("Error code " ^ Plasma_util.string_of_errno e)
    )


let dest_dump s =
  let (prefix,nref,suffix) = Nn_dumps.dest_token s in
  { Nn_dumps.dump_prefix = prefix;
    dump_ref = nref;
    dump_suffix = suffix;
    dump_finished = false;
    dump_timestamp = 0.0
  }


let install_checkpoint() =
  let conf_file = ref "" in
  let from = ref `Local in
  let name = ref `None in
  let do_load = ref true in
  Arg.parse
    [ "-conf", Arg.Set_string conf_file,
      "<file>    Config file of name node";

      "-from-host", Arg.String (fun s -> from := `Remote s),
      "<host>    Get the checkpoint from this (live) namenode";

      "-latest", Arg.Unit (fun () -> name := `Latest),
      "     Get the latest checkpoint (only remote)";

      "-checkpoint", Arg.String (fun s -> name := `This (dest_dump s)),
      "<name>   Get this checkpoint";

      "-no-load", Arg.Clear do_load,
      "    Only copy the checkpoint, but do not load it into the db";
    ]
    (fun s ->
       raise (Arg.Bad("Extra argument: " ^ s))
    )
    "usage: plasma_admin install_checkpoint";

  if !conf_file = "" then
    failwith "Missing -conf";

  Netlog.current_logger := Netlog.channel_logger stderr (*`Debug*) `Info;
(* Rpc_client.Debug.enable := true; *)

  let esys = Unixqueue.create_unix_event_system() in
  let cf = Netplex_config.read_config_file !conf_file in
  let conf = Nn_config.extract_node_config cf in
  let db_conf = Pfs_db.extract_db_config cf in
  Nn_db.init db_conf esys;

  let ldump =
    match !from with
      | `Local ->
           let d =
             match !name with
               | `This d -> d
               | `Latest ->
                    let dumps = Nn_dumps.list_dumps conf in
                    let best = Nn_sync.latest_dump dumps in
                    ( match best with
                        | Some d -> d
                        | None ->
                             failwith "no latest dump"
                    )
               | `None ->
                    failwith "specify either -latest or -checkpoint" in
           d
      | `Remote host ->
           let cauth = Pfs_auth.extract_client_config cf cf#root_addr in
           let hp =
             try
               List.find
                 (fun hp ->
                    let (h,p) = Plasma_util.parse_host_port hp in
                    h = host
                 )
                 conf#nn_nodes
             with 
               | Not_found ->
                    failwith "Host not found in config file" in
           printf "Getting dump from %s\n%!" hp;
           let ld =
             match !name with
               | `This d ->
                    Nn_sync.get_remote_dump
                      conf cauth conf#nn_clustername hp d
               | `Latest ->
                    let d = 
                      Nn_sync.determine_latest_checkpoint
                        conf cauth conf#nn_clustername hp in
                    let triple = 
                      Nn_dumps.(d.dump_prefix, d.dump_ref, 
                                d.dump_suffix) in
                    printf "Remote dump name: %s\n%!"
                           (Nn_dumps.create_token triple);
                    Nn_sync.get_remote_dump
                      conf cauth conf#nn_clustername hp d
               | `None ->
                    failwith "specify either -latest or -checkpoint" in
           ld in
  let triple = 
    Nn_dumps.(ldump.dump_prefix, ldump.dump_ref, ldump.dump_suffix) in
  printf "Local dump name: %s\n%!" (Nn_dumps.create_token triple);
  if !do_load then (
    printf "Loading local dump\n%!";
    let db = new Pfs_db.sync_connect db_conf in
    let _r = db # exec ~expect:[ Postgresql.Command_ok ] "START TRANSACTION" in
    Nn_db.load db (Nn_dumps.dump_directory conf ldump);
    let _r = db # exec ~expect:[ Postgresql.Command_ok ] "COMMIT" in
    printf "Done\n%!"
  )


let sync e =
  Unixqueue.run e#event_system;
  match e#state with
    | `Done v -> v
    | `Error e -> raise e
    | `Aborted -> failwith "Aborted"
    | `Working _ -> assert false


let fsck() =
  let conf = ref "" in
  Arg.parse
    [ "-conf", Arg.Set_string conf,
      "<file>    Config file of name node";
    ]
    (fun s -> raise (Arg.Bad("Extra argument: " ^ s)))
    "usage: plasma_admin fsck -conf <file>";

  if !conf = "" then
    failwith "Missing -conf";

  Netlog.current_logger := Netlog.channel_logger stderr `Info;

  let cf = Netplex_config.read_config_file !conf in
  let db_conf = Pfs_db.extract_db_config cf in
  let db = (new Pfs_db.connect db_conf  : Pfs_db.ro_async_connection) in

  printf "Checking blockmap\n%!";

  (* First read datastores: *)
  let esys = Unixqueue.create_unix_event_system() in
  let ds_list = sync (Nn_db.datastore_list_e db esys) in
  
  (* read inodes: *)
  let inodes = sync (Nn_db.inode_listall_e db esys) in

  List.iter
    (fun ds ->
       let id = ds.Nn_datastores.id in
       let identity = ds.Nn_datastores.identity in
       let sizeL = ds.Nn_datastores.size in

       printf "* Datastore ID=%d identity=%s size=%Ld:\n%!"
	 id identity sizeL;

       if sizeL > Int64.of_int max_int then
	 failwith "too large";
       let size = Int64.to_int sizeL in
       let bv = Bitv.create size false in

       (* Read the blockalloc table: *)
       let ba_list = sync (
	 Nn_db.blockalloc_list_e
	   ~datastore:id ~blkidx_min:0L ~blkidx_max:Int64.max_int db esys
       ) in
       List.iter
	 (fun (blkidxL, blkmap) ->
	    let blkidx = Int64.to_int blkidxL in
	    let row = Bitv.of_string blkmap in
	    Bitv.blit row 0 bv blkidx (Bitv.length row)
	 )
	 ba_list;

       (* Iterate over inodes and check *)
       List.iter
	 (fun inode ->
	    let blocks = sync(Nn_db.inodeblocks_getall_e 
				~inode ~dsid:id
				db esys) in
	    List.iter
	      (fun (blkidx,bL,numL) ->
		 let bendL = Int64.add bL numL in
		 if numL <= 0L then
		   printf "  Non-positive number of blocks. \
                             inode=%Ld block=%Ld num=%Ld\n%!"
		     inode bL numL
		 else if bL < 0L || bendL >= sizeL then
		   printf "  Block out of range: \
                           inode=%Ld block=%Ld num=%Ld\n%!"
		     inode bL numL
		 else (
		   let k = ref 0L in
		   while !k < numL do
		     let b = Int64.to_int (Int64.add bL !k) in
		     if Bitv.get bv b then
		       Bitv.set bv b false
		     else
		       printf "  Block out of sync: inode=%Ld block=%Ld\n%!"
			 inode bL;
		     k := Int64.succ !k
		   done
		 )
	      )
	      blocks
	 )
	 inodes;

       if not (Bitv.all_zeros bv) then
	 printf "  There are unreferenced blocks.\n%!"
       
    )
    ds_list



let help() =
  usage()


let dispatch() =
  if Array.length Sys.argv < 2 then
    failwith "Bad usage";

  let mode_str = Sys.argv.(1) in
  incr Arg.current;

  match mode_str with
    | "add_datanode" -> add_datanode()
    | "enable_datanode" -> enable_datanode()
    | "disable_datanode" -> disable_datanode()
    | "list_datanodes" -> list_datanodes()
    | "check_datanode" -> check_datanode()
    | "destroy_datanode" -> destroy_datanode()
    | "create_checkpoint" -> create_checkpoint()
    | "install_checkpoint" -> install_checkpoint()
    | "fsck" -> fsck()
    | "help" | "-help" | "--help" -> help()
    | _ -> failwith ("Bad usage")

let main () =
  try
    dispatch()
  with
    | Failure m ->
	prerr_endline ("plasma_admin: " ^ m);
	usage();
	exit 2
	  
let () = 
  main()
    
