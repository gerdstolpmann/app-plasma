/* $Id$ -*- c -*- */


enum page_type {
    ROOT_PAGE = 0,
    INT_PAGE = 1,
    LEAF_PAGE = 2
};

typedef string key<>;

struct key_p {
    key    k;
    hyper  p;
};

struct root_page {
    int    max_key_length;
    int    max_key_number;
    hyper  root_pointer;
};

struct int_page {
    hyper  int_min_p;
    key_p  int_keys<>;
};

struct leaf_page {
    hyper  prev_leaf_pointer;
    hyper  next_leaf_pointer;
    key_p  leaf_keys<>;
};

union page switch(page_type t) {
  case ROOT_PAGE: root_page x;
  case INT_PAGE:  int_page x;
  case LEAF_PAGE: leaf_page x;
};
