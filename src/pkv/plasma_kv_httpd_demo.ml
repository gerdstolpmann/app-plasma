(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

(* This is a demo how to serve Plasma KV lookups via HTTP. The interesting
   thing here is that the demo is fully asynchronous.

   This is a modified version of proxy_engine.ml, an example HTTP
   service distributed with Ocamlnet (in examples/nethttpd).

   See the usage text below for more information.

 *)

open Printf
open Uq_engines.Operators


let reject_and_error code notification =
  let env = notification # environment in
  env # set_status code;
  env # set_output_header_field "Content-type" "text/plain";
  env # send_output_header();
  let msg =
    sprintf "HTTP error %d - %s"
      (Nethttp.int_of_http_status code)
      (Nethttp.string_of_http_status code) in
  env # out_channel # output_string msg;
  env # out_channel # close_out();
  Netlog.logf `Err "Error: %s" msg;
  notification # schedule_finish()


let internal_server_error notification err =
  let env = notification # environment in
  env # set_status `Internal_server_error;
  env # set_output_header_field "Content-type" "text/plain";
  env # send_output_header();
  env # out_channel # output_string
    "An exception occurred while processing the \
                              request, see log messages for details\n";
  env # out_channel # close_out();
  Netlog.logf `Err "Exception: %s"
    (Netexn.to_string err);
  notification # schedule_finish()



let on_request db esys notification =
  (* This function is called when the full HTTP request has been received. We set up
   * another engine to connect to the remote service, and copy data from this service
   * to our output channel.
   *)
  ( try
      let env = notification # environment in
      (* Create [cgi], and check CGI arguments. Mode is [`Direct], i.e. there is no
       * output buffering by [cgi]. This is important for us because this means that
       * [cgi # output] and [env # output_ch_async] are just the same channel - we
       * are going to use the latter for our gateway.
       *)
      let cgi =
	Netcgi_common.cgi_with_args 
	  (new Netcgi_common.cgi)
	  (env :> Netcgi.cgi_environment)
	  (`Direct "")
	  env#input_channel
	  (fun _ _ _ -> `Automatic) in
      let path0 = env#cgi_script_name in
      let path = if path0 = "" then "/" else path0 in
      let buf = String.create 65536 in

      let e =
	Pkv_api.lookup_large_e
	  db
	  path
	  buf
	  (fun size ->
	     cgi # set_header
	       ~status: `Ok
	       ~content_type:"application/content-stream"
	       ~fields:[ "Content-Length", [Int64.to_string size] ]
	       ();
	     `Async_out(env#output_ch_async, esys)
	  ) in

(*
      cgi # set_header
	~status: `Ok
	~content_type:"application/content-stream"
	~fields:[ "Content-Length", [Int64.to_string 1000L] ]
	();
      env # output_ch # output_string (String.make 1000 'X');
      notification # schedule_finish();
 *)

      Uq_engines.when_state  (* OK *)
	~is_done:(fun _ -> notification # schedule_finish())
	~is_error:(fun err ->
		     match err with
		       | Not_found ->
			   (* The key was not found. Map to 404 *)
			   cgi # set_header
			     ~status:`Not_found
			     ~content_type:"text/plain"
			     ();
			   cgi # output # output_string
			     "The key could not be found in the database\n";
			   notification # schedule_finish()
		       | _ ->
			   internal_server_error notification err
		  )
	e
    with
	e ->
	  internal_server_error notification e
  )
;;

let on_request_header db esys 
                      (notification : Nethttpd_engine.http_request_header_notification) =
  (* After receiving the HTTP header: We always decide to accept the HTTP body, if any
   * is following. We do not set up special processing of this body, it is just
   * buffered until complete. Then [on_request] will be called.
   *
   * An advanced server could set up a further notification for the HTTP body. This
   * additional function would be called whenever new body data arrives. (Do so by
   * calling [notification # environment # input_ch_async # request_notification].)
   *)
  let env = notification # environment in
  if env#cgi_request_method <> "GET" then
    notification # schedule_reject_body
      ~on_request:(reject_and_error `Method_not_allowed) ()
  else (
    let len =
      try Int64.of_string(env#input_header_field ~default:"0" "Content-Length")
      with _ -> 0L in
    if len <> 0L then
      notification # schedule_reject_body
	~on_request:(reject_and_error `Bad_request) ()
    else
      notification # schedule_accept_body ~on_request:(on_request db esys) ()
  )
;;

let serve_connection db esys fd =
  (* Creates the http engine for the connection [fd]. When a HTTP header is received
   * the function [on_request_header] is called.
   *)
  let config = Nethttpd_engine.default_http_engine_config in
  Unix.set_nonblock fd;
  let _http_engine = 
    new Nethttpd_engine.http_engine
      ~on_request_header:(on_request_header db esys) () 
      config fd esys in
  ()
;;

let rec accept db esys srv_sock_acc =
  (* This function accepts the next connection using the [acc_engine]. After the
   * connection has been accepted, it is served by [serve_connection], and the
   * next connection will be waited for (recursive call of [accept]). Because
   * [server_connection] returns immediately (it only sets the callbacks needed
   * for serving), the recursive call is also done immediately.
   *)
  let acc_engine = srv_sock_acc # accept() in
  Uq_engines.when_state  (* OK *)
    ~is_done:(fun (fd,fd_spec) ->
		if srv_sock_acc # multiple_connections then (
		  Unix.setsockopt fd Unix.TCP_NODELAY true;
		  serve_connection db esys fd;
		  accept db esys srv_sock_acc
                ) else 
		  srv_sock_acc # shut_down())
    ~is_error:(fun _ -> srv_sock_acc # shut_down())
    acc_engine;
;;

let start dbname port =
  (* We set up [lstn_engine] whose only purpose is to create a server socket listening
   * on the specified port. When the socket is set up, [accept] is called.
   *)
  Netlog.logf `Info
    "Service starting up at http://127.0.0.1:%d" port;
  let esys = Unixqueue.create_unix_event_system () in
  let cfg = Plasma_client_config.get_config() in
  let c = Plasma_client.open_cluster_cc cfg esys in
  Plasma_client.configure_auth_daemon c;
  Plasma_client.configure_buffer c 128;

  let opts = { (* Uq_engines.default_listen_options with *)
		 Uq_engines.lstn_backlog = 20;
		 Uq_engines.lstn_reuseaddr = true } in
  let setup_e =
    Pkv_api.opendb_e c dbname [`Transactional]
    ++ (fun db ->
	  Uq_engines.listener
	    (`Socket(`Sock_inet(Unix.SOCK_STREAM, Unix.inet_addr_loopback, port),
		     opts)) 
	    esys
	  ++ (fun fd ->
		eps_e (`Done (db,fd)) esys
	     )
       ) in
  Uq_engines.when_state   (* OK *)
    ~is_done:(fun (db,fd) -> accept db esys fd) 
    ~is_error:(fun err ->
		 Netlog.logf `Err "Exception: %s" (Netexn.to_string err))
    setup_e;
  (* Start the main event loop. *)
  Unixqueue.run esys
;;

let conf_debug() =
  (* Set the environment variable DEBUG to either:
       - a list of Netlog module names
       - the keyword "ALL" to output all messages
       - the keyword "LIST" to output a list of modules
     By setting DEBUG_WIN32 additional debugging for Win32 is enabled.
   *)
  let debug = try Sys.getenv "DEBUG" with Not_found -> "" in
  if debug = "ALL" then
    Netlog.Debug.enable_all()
  else if debug = "LIST" then (
    List.iter print_endline (Netlog.Debug.names());
    exit 0
  )
  else (
    let l = Netstring_str.split (Netstring_str.regexp "[ \t\r\n]+") debug in
    List.iter
      (fun m -> Netlog.Debug.enable_module m)
      l
  );
  if (try ignore(Sys.getenv "DEBUG_WIN32"); true with Not_found -> false) then
    Netsys_win32.Debug.debug_c_wrapper true
;;


let usage =
  "

This demo starts an HTTP server accepting GET requests. The path in the
GET requests is interpreted as a key in a Plasma KV database, and looked
up. The server responds with the corresponding value. For instance, a
GET for http://localhost:<port>/<path> would try to locate /<path> in the
database (with leading slash), and output the value.

Before you can use this server, you need to prepare a database, e.g.:

$ plasma_kv create -db /demo -max-key-length 256
$ find /etc -type f | plasma_kv insert -db /demo -files

This would populate the database with all files from the /etc file tree.

Also, it is required that the Plasma authentication daemon is running
on the machine where this demo is started.

Command-line options:
"


let () =
  Netsys_signal.init();
  conf_debug();

  let dbname = ref "/demo" in
  let port = ref 8765 in

  Arg.parse
    [ "-db", Arg.Set_string dbname,
      (sprintf "<plasmapath>   Access this DB in PlasmaFS (default: %s)"
	 !dbname);

      "-port", Arg.Set_int port,
      (sprintf "<p>   Listen on this TCP port (default: %d)" !port)
    ]
    (fun arg -> raise(Arg.Bad("Unexpected arg: " ^ arg)))
    usage;

  start !dbname !port
;;
