(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Pkv_xdr_aux
open Uq_engines.Operators
open Printf

module P = Plasma_rpcapi_aux

let rec write c inode pos s spos len =
  let n = Plasma_client.write c inode pos s spos len in
  if n < len then
    write c inode (Int64.add pos (Int64.of_int n)) s (spos+n) (len-n)



(**********************************************************************)
(* Data files                                                         *)
(**********************************************************************)

(* Format of data files: The file is a sequence of unpadded records.
   Each record has the format:

   <l_key>
   <l_key_inverted>
   <key>
   <l_value>
   <l_value_inverted>
   <value>

   where:
   - <l_key> is an int64 of the length of <key>
   - <l_key_inverted> = <l_key> with inverted bits
   - <key> is the data of the key
   - <l_value> is an int64 of the length of <value>
   - <l_value_inverted> = <l_value> with inverted bits
   - <value> is the data of the value
 *)

let data_create c name =
  (* An empty data file is just an empty file *)
  Plasma_client.retry c name
    (fun () ->
       Plasma_client.with_trans c
	 (fun t ->
	    let c = Plasma_client.cluster t in
	    ignore(Plasma_client.create_file t name 
		     (Plasma_client.regular_ii c 0o666))
	 )
    )
    ()

type data_file =
    { data_cluster : Plasma_client.plasma_cluster;
      data_inode : int64;
      data_ii : Plasma_rpcapi_aux.inodeinfo;
      mutable data_eof : int64;
    }


let data_open_e t data_inode =
  let esys = Plasma_client.event_system (Plasma_client.cluster t) in
  Plasma_client.get_inodeinfo_e t data_inode
  ++ (fun data_ii ->
	let data_eof = data_ii.P.eof in
	eps_e
	  (`Done 
	     { data_cluster = Plasma_client.cluster t;
	       data_inode;
	       data_eof;
	       data_ii
	     }
	  )
	  esys
     )

let data_open t data_inode =
  Plasma_client.sync (data_open_e t) data_inode

let data_clear df t =
  Plasma_client.drop_inode df.data_cluster df.data_inode;
  Plasma_client.truncate t df.data_inode 0L;
  df.data_eof <- 0L


let data_item_length_e df pos =
  (* Read a key or a value length at pos, and return the length *)
  let c = df.data_cluster in
  let esys = Plasma_client.event_system c in
  let l_buf = String.create 8 in
  let l_inv_buf = String.create 8 in
  Plasma_client.read_e
    ~lazy_validation:true
    c df.data_inode pos (`String l_buf) 0 8
  ++ (fun (n, _, _) ->
	if n < 8 then
	  failwith "Pkv_files.data_item_length: reading beyond EOF";
	Plasma_client.read_e
	  ~lazy_validation:true
	  c df.data_inode (Int64.add pos 8L) (`String l_inv_buf) 0 8
	++ (fun (n, _, _)  ->
	      if n < 8 then
		failwith "Pkv_files.data_item_length: reading beyond EOF";
	      let l = Rtypes.int64_of_int8 (Rtypes.read_int8 l_buf 0) in
	      let l_inv = Rtypes.int64_of_int8 (Rtypes.read_int8 l_inv_buf 0) in
	      if l <> Int64.lognot l_inv || l < 0L then
		failwith "Pkv_files.data_item_length: reading at bad position";
	      eps_e (`Done l) esys
	   )
     )

let data_item_length df pos =
  Plasma_client.sync (data_item_length_e df) pos


let data_item_e df pos =
  (* Read a key at pos, and return the key plus the next position *)
  let c = df.data_cluster in
  let esys = Plasma_client.event_system c in
  data_item_length_e df pos
  ++ (fun lL ->
	if lL > Int64.of_int max_int then
	  failwith "Pkv_files.data_item: item too large";
	let l = Int64.to_int lL in
	let s = String.create l in
	Plasma_client.read_e
	  ~lazy_validation:true
	  c df.data_inode (Int64.add pos 16L) (`String s) 0 l
	++ (fun (n, _, _) ->
	      if n <> l then
		failwith "Pkv_files.data_item: reading beyond EOF";
	      eps_e (`Done (s, Int64.add (Int64.add pos 16L) lL)) esys
	   )
     )

let data_item df pos =
  Plasma_client.sync (data_item_e df) pos


let data_item_to_ch df pos buf ch f_close =
  (* Read a value at pos, and write the value to ch. Return the next position *)
  let c = df.data_cluster in
  let l = data_item_length df pos in
  let p = ref 0L in
  let buflen = String.length buf in
  let buflenL = Int64.of_int buflen in
  while !p < l do
    let q = Int64.to_int (min (Int64.sub l !p) buflenL) in
    let (n, _, _) =
      Plasma_client.read
	~lazy_validation:true
	c df.data_inode (Int64.add (Int64.add pos 16L) !p) (`String buf) 0 q in
    if n <> q then
      failwith "Pkv_files.data_item_to_ch: reading beyond EOF";
    ch # really_output buf 0 q;
    p := Int64.add !p (Int64.of_int q);
  done;
  f_close ch;
  (Int64.add (Int64.add pos 16L) l)

let data_item_to_dev df pos buf dev f_close_e =
  (* Read a value at pos, and write the value to dev
     Return the next position
   *)
  let c = df.data_cluster in
  let esys = Plasma_client.event_system c in
  let buflen = String.length buf in
  let buflenL = Int64.of_int buflen in

  let rec next_e p l =
    if p < l then (
      let q = Int64.to_int (min (Int64.sub l p) buflenL) in
      Plasma_client.read_e
	~lazy_validation:true
	c df.data_inode (Int64.add (Int64.add pos 16L) p) (`String buf) 0 q
      ++ (fun (n, _, _)  ->
	    if n <> q then
	      failwith "Pkv_files.data_item_to_ch: reading beyond EOF";
	    Uq_io.really_output_e dev (`String buf) 0 q
	    ++ (fun () ->
		  next_e (Int64.add p (Int64.of_int q)) l
	       )
	 )
    )
    else
      f_close_e dev in
  data_item_length_e df pos
  ++ (fun l ->
	next_e 0L l
	++ (fun () ->
	      eps_e (`Done(Int64.add (Int64.add pos 16L) l)) esys
	   )
     )

let data_lookup df pos buf f f_close =
  (* Read key and value at pos. The value is written to a channel obtained
     by [ch = f key size]. Returns the next position.
   *)
  let key, pos_value = data_item df pos in
  let l = data_item_length df pos_value in
  let ch = f key l in
  data_item_to_ch df pos_value buf ch f_close


let data_lookup_dev df pos buf f f_close_e =
  (* Read key and value at pos. The value is written to a device obtained
     by [dev = f key size]. Returns the next position.
   *)
  data_item_e df pos
  ++ (fun (key, pos_value) ->
	data_item_length_e df pos_value
	++ (fun l ->
	      let dev = f key l in
	      data_item_to_dev df pos_value buf dev f_close_e
	   )
     )


let data_lookup_small df pos =
  (* For small values that fit into RAM *)
  let key, pos_value = data_item df pos in
  let value, pos_next = data_item df pos_value in
  (key, value, pos_next)


exception Skip

let data_iterate df buf f =
  (* Iterates over the whole file, and calls [ch = f pos key size] for each key.
     The value is written to [ch]. [f] may raise [Skip].
   *)
  let pos = ref 0L in
  while !pos < df.data_eof do
    let f1 key size = f !pos key size in
    try
      pos := data_lookup df !pos buf f1 (fun ch -> ch#close_out())
    with Skip ->
      let l_keyL = data_item_length df !pos in
      let p_valL = Int64.add !pos (Int64.add l_keyL 16L) in
      let l_valL = data_item_length df p_valL in
      let p_next = Int64.add p_valL (Int64.add l_valL 16L) in
      pos := p_next
  done


let write_data_item_length df pos l =
  (* Write this *)
  let c = df.data_cluster in
  let buf = String.create 16 in
  Rtypes.write_int8 buf 0 (Rtypes.int8_of_int64 l);
  Rtypes.write_int8 buf 8 (Rtypes.int8_of_int64 (Int64.lognot l));
  write c df.data_inode pos (`String buf) 0 16


let data_append df key vallen ch =
  (* Appends key and the value coming from [ch]. The value must have a
     length of [vallen]
   *)
  let c = df.data_cluster in
  let pos = df.data_eof in
  let l_key = String.length key in
  write_data_item_length df pos (Int64.of_int l_key);
  write c df.data_inode (Int64.add pos 16L) (`String key) 0 l_key;
  let pos1 = Int64.add (Int64.add pos 16L) (Int64.of_int l_key) in
  write_data_item_length df pos1 vallen;
  let pos2 = Int64.add pos1 16L in
  let s = String.create 65536 in
  let p = ref 0L in
  while !p < vallen do
    let q = Int64.to_int(min (Int64.sub vallen !p) 65536L) in
    ch # really_input s 0 q;
    write c df.data_inode (Int64.add pos2 !p) (`String s) 0 q;
    p := Int64.add !p (Int64.of_int q);
  done;
  df.data_eof <- Int64.add pos2 vallen;
  pos


let data_append_ch df key vallen f_close =
  (* Appends key. The function returns a channel to which the value
     can be written. At close time f_close is called.
   *)
  let c = df.data_cluster in
  let pos = df.data_eof in
  let l_key = String.length key in
  write_data_item_length df pos (Int64.of_int l_key);
  write c df.data_inode (Int64.add pos 16L) (`String key) 0 l_key;
  let pos1 = Int64.add (Int64.add pos 16L) (Int64.of_int l_key) in
  write_data_item_length df pos1 vallen;
  let pos2 = Int64.add pos1 16L in
  let p = ref 0L in

  ( object
      method output s spos slen =
	if (Int64.of_int slen > Int64.sub vallen !p) then
	  failwith "Pkv_files.data_append_ch: too much data is written";
	write c df.data_inode (Int64.add pos2 !p) (`String s) spos slen;
	p := Int64.add !p (Int64.of_int slen);
	slen
      method flush() = ()
      method close_out() =
	if (!p <> vallen) then
	  failwith "Pkv_files.data_append_ch: not enough data is written";
	df.data_eof <- Int64.add pos2 vallen;
	f_close pos
    end
  )


let test_append df key v =
  data_append df key
    (Int64.of_int (String.length v))
    (new Netchannels.input_string v)




(**********************************************************************)
(* Del files                                                          *)
(**********************************************************************)

(* Del files contain the entries that are considered as deleted.
   The format is the same as data files, only that the value is
   not set to the real value, but to the file pointer of the entry
 *)

let del_create t name =
  data_create t name


let del_open t inode =
  data_open t inode


let del_open_e t inode =
  data_open_e t inode

let del_load df ht =
  (* Loads the contents into the hash table *)
  let pos = ref 0L in
  while !pos < df.data_eof do
    let (key, value, next_pos) = data_lookup_small df !pos in
    if String.length value <> 8 then
      failwith "Pkv_files.del_load: invalid entry found";
    let file_pointer = Rtypes.int64_of_int8 (Rtypes.read_int8 value 0) in
    Hashtbl.replace ht (key,file_pointer) ();
    pos := next_pos
  done


let del_clear df t =
  data_clear df t


let del_append df key file_pointer =
  let s = String.create 8 in
  Rtypes.write_int8 s 0 (Rtypes.int8_of_int64 file_pointer);
  let ch = new Netchannels.input_string s in
  data_append df key 8L ch


(**********************************************************************)
(* Index files                                                        *)
(**********************************************************************)

(* Index files are B+ trees. Each page of the tree is a block of the
   filesystem. The layout of the pages is given by the XDR definition
   [page].
 *)

type idx_file =
    { idx_page_size : int;
      idx_cluster : Plasma_client.plasma_cluster;
      idx_inode : int64;
      mutable idx_eof : int64;
      idx_clean : (int64, page) Hashtbl.t;
      idx_dirty : (int64, page) Hashtbl.t;
      idx_req : (int64, page Uq_engines.engine) Hashtbl.t;
    }


let vxdrt_page =
  Xdr.validate_xdr_type xdrt_page


let idx_flush xf =
  (* Write out dirty pages *)
  let c = xf.idx_cluster in
  let l =
    Hashtbl.fold (fun pos _ acc -> pos :: acc) xf.idx_dirty [] in
  let l_sorted =
    List.sort Int64.compare l in
  List.iter
    (fun pos ->
       let pg = Hashtbl.find xf.idx_dirty pos in
       let xdr = _of_page pg in
       let data = String.make xf.idx_page_size '\000' in
       let p = ref 0 in
       Xdr.pack_xdr_value xdr vxdrt_page [] 
	 (fun s -> 
	    String.blit s 0 data !p (String.length s);
	    p := !p + String.length s
	 );
       write c xf.idx_inode pos (`String data) 0 (String.length data);
       Hashtbl.add xf.idx_clean pos pg;
    )
    l_sorted;
  Hashtbl.clear xf.idx_dirty;
  Plasma_client.flush c xf.idx_inode 0L 0L


let add_page xf pg =
  let pos = xf.idx_eof in
  Hashtbl.replace xf.idx_dirty pos pg;
  xf.idx_eof <- Int64.add pos (Int64.of_int xf.idx_page_size);
  pos


let touch_page xf pos pg =
  Hashtbl.remove xf.idx_clean pos;
  Hashtbl.replace xf.idx_dirty pos pg


let idx_open_e t idx_inode =
  let c = Plasma_client.cluster t in
  let esys = Plasma_client.event_system c in
  Plasma_client.get_inodeinfo_e t idx_inode
  ++ (fun idx_ii  ->
	let idx_eof = idx_ii.P.eof in
	Plasma_client.blocksize_e c
	++ (fun idx_page_size  ->
	      let xf = 
		{ idx_page_size;
		  idx_cluster = c;
		  idx_inode;
		  idx_eof;
		  idx_clean = Hashtbl.create 47;
		  idx_dirty = Hashtbl.create 47;
		  idx_req = Hashtbl.create 47;
		} in
	      eps_e (`Done xf) esys
	   )
     )

let idx_open t idx_inode =
  Plasma_client.sync (idx_open_e t) idx_inode


let get_page_e xf pos =
  let esys = Plasma_client.event_system xf.idx_cluster in
  try
    eps_e(`Done(Hashtbl.find xf.idx_clean pos)) esys
  with Not_found ->
    try
      eps_e(`Done(Hashtbl.find xf.idx_dirty pos)) esys
    with Not_found ->
      try
	Hashtbl.find xf.idx_req pos
      with Not_found ->
	let e = (
	  let c = xf.idx_cluster in
	  let data = String.create xf.idx_page_size in
	  Plasma_client.read_e
	    ~lazy_validation:true
	    c xf.idx_inode pos (`String data) 0 xf.idx_page_size
	  ++ (fun (n, _, _) ->
		if n <> xf.idx_page_size then
		  failwith "Pkv_files.get_page: reading beyond EOF";
		let pg =
		  _to_page (Xdr.unpack_xdr_value
			      ~prefix:true ~fast:true data vxdrt_page []) in
		Hashtbl.replace xf.idx_clean pos pg;
		eps_e (`Done pg) esys
	     )
	  >> (fun st ->
		Hashtbl.remove xf.idx_req pos;
		st
	     )
	) in
	Hashtbl.replace xf.idx_req pos e;
	e
	
let get_page xf pos =
  Plasma_client.sync (get_page_e xf) pos


let get_max_key_length xf =
  let pg = get_page xf 0L in
  match pg with
    | `root_page root_pg -> root_pg.max_key_length
    | _ -> assert false


let get_max_key_number xf =
  let pg = get_page xf 0L in
  match pg with
    | `root_page root_pg -> root_pg.max_key_number
    | _ -> assert false


let compute_max_key_number xf =
  let key_len = get_max_key_length xf in
  let key_len_rnd = ((key_len - 1) / 4 + 2) * 4  in
  let empty_int_pg =
    `int_page 
      { int_min_p = 0L;
	int_keys = [| |]
      } in
  let empty_leaf_pg =
    `leaf_page 
      { prev_leaf_pointer = 0L;
	next_leaf_pointer = 0L;
	leaf_keys = [| |]
      } in
  let n_list =
    List.map
      (fun pg ->
	 let size_pg =
	   String.length
	     (Xdr.pack_xdr_value_as_string (_of_page pg) vxdrt_page []) in
	 (xf.idx_page_size - size_pg) / key_len_rnd
      )
      [ empty_int_pg;
	empty_leaf_pg
      ] in
  List.fold_left ( min ) max_int n_list
  


let idx_create c name max_key_length =
  let inode =
    Plasma_client.retry c name
      (fun () ->
	 Plasma_client.with_trans c
	   (fun t ->
	      Plasma_client.create_file t name 
		(Plasma_client.regular_ii c 0o666)
	   )
      )
      () in
  Plasma_client.retry c name
    (fun () ->
       Plasma_client.with_trans c
	 (fun t ->
	    let xf = idx_open t inode in
	    let root_pg =
	      { max_key_length = max_key_length;
		max_key_number = 0;
		root_pointer = 0L;
	      } in
	    Hashtbl.add xf.idx_dirty 0L (`root_page root_pg);
	    let n = compute_max_key_number xf in
	    root_pg.max_key_number <- n;
	    idx_flush xf
	 )
    )
    ()


let bsearch kp key =
  (* Searches key in the array kp.
     If found, the index is returned. If not found, the next index p is returned
     so that kp.(p) is smaller than the key. p=-1 is possible.
   *)
  let rec loop l r =
    (* Inv.: key >= kp.(l) && key < kp.(r) *)
    let m = (l+r) / 2 in
    let k = kp.(m).k in
    let d = String.compare k key in
    if d = 0 then
      m
    else
      if d > 0 then (
	if l < m-1 then (
	  if key < kp.(m-1).k then loop l (m-1) else m-1
	) else
	  l
      )
      else (
	if m+1 < r then (
	  if key >= kp.(m+1).k then loop (m+1) r else m
	) else
	  r-1
      )
  in
  let kp_size = Array.length kp in
  if key < kp.(0).k then
    (-1)
  else
    if key >= kp.(kp_size-1).k then
      kp_size-1
    else
      loop 0 (kp_size-1)


let tree_shape xf =
  let rec walk pos indent =
    printf "%s" (String.make indent ' ');
    let pg = get_page xf pos in
    match pg with
      | `root_page rp ->
	  printf "@%Ld: root\n" pos;
	  if rp.root_pointer <> 0L then
	    walk rp.root_pointer (indent+4)
      | `int_page ip ->
	  let n = Array.length ip.int_keys in
	  printf "@%Ld: int %s-%s\n"
	    pos
	    (ip.int_keys.(0).k)
	    (ip.int_keys.(n-1).k);
	  if ip.int_min_p <> 0L then
	    walk ip.int_min_p (indent+4);
	  for k = 0 to n-1 do
	    walk ip.int_keys.(k).p (indent+4);
	  done
      | `leaf_page lp ->
	  let n = Array.length lp.leaf_keys in
	  printf "@%Ld: leaf %s-%s\n"
	    pos
	    (lp.leaf_keys.(0).k)
	    (lp.leaf_keys.(n-1).k)
  in
  walk 0L 0


let validate_tree xf =
  let rec walk pos k_min_opt k_end_opt =
    let pg = get_page xf pos in
    match pg with
      | `root_page rp ->
	  if rp.root_pointer <> 0L then
	    walk rp.root_pointer None None
      | `int_page ip ->
	  let n = Array.length ip.int_keys in
	  if ip.int_min_p <> 0L then
	    walk ip.int_min_p k_min_opt (Some ip.int_keys.(0).k);
	  ( match k_min_opt with
	      | None -> ()
	      | Some k_min ->
		  if ip.int_keys.(0).k <> k_min then
		    printf "@%Ld: int first key error. Expected: %s, Actual: %s\n%!"
		      pos k_min ip.int_keys.(0).k
	  );
	  let k_end_opt_0 =
	    if n >= 2 then
	      Some ip.int_keys.(1).k
	    else
	      k_end_opt in
	  walk ip.int_keys.(0).p (Some ip.int_keys.(0).k) k_end_opt_0;

	  let last_k = ref ip.int_keys.(0).k in
	  for j = 1 to n-1 do
	    let k = ip.int_keys.(j).k in
	    let p = ip.int_keys.(j).p in
	    if k <= !last_k then
	      printf "@%Ld: int key order error. j=%d: Expected: %s, Actual: %s\n%!"
		pos j !last_k k;
	    ( match k_end_opt with
		| None -> ()
		| Some k_end ->
		    if k >= k_end then
		      printf "@%Ld: int key too big error. j=%d: End: %s, Actual: %s\n%!"
			pos j k_end k;
	    );
	    let k_end_opt_j =
	      if j < n-1 then
		Some ip.int_keys.(j+1).k
	      else
		k_end_opt in
	    walk p (Some k) k_end_opt_j;
	    last_k := k
	  done
      | `leaf_page lp ->
	  let n = Array.length lp.leaf_keys in
	  ( match k_min_opt with
	      | None -> ()
	      | Some k_min ->
		  if lp.leaf_keys.(0).k <> k_min then
		    printf "@%Ld: leaf first key error. Expected: %s, Actual: %s\n%!"
		      pos k_min lp.leaf_keys.(0).k
	  );
	  let last_k = ref lp.leaf_keys.(0).k in
	  for j = 1 to n-1 do
	    let k = lp.leaf_keys.(j).k in
	    if k <= !last_k then
	      printf "@%Ld: leaf key order error. j=%d: Expected: %s, Actual: %s\n%!"
		pos j !last_k k;
	    ( match k_end_opt with
		| None -> ()
		| Some k_end ->
		    if k >= k_end then
		      printf "@%Ld: leaf key too big error. j=%d: End: %s, Actual: %s\n%!"
			pos j k_end k;
	    );
	    last_k := k
	  done
  in
  walk 0L None None


let idx_lookup_e xf key =
  let esys = Plasma_client.event_system xf.idx_cluster in
  let rec walk_e pos =
    (* Try to locate key in the page at pos *)
    get_page_e xf pos
    ++ (fun pg ->
	  match pg with
	    | `root_page p ->
		if p.root_pointer = 0L then raise Not_found;
		walk_e p.root_pointer
	    | `int_page p ->
		let u = bsearch p.int_keys key in
		if u < 0 then
		  walk_e p.int_min_p
		else
		  walk_e p.int_keys.(u).p
	    | `leaf_page p ->
		let u = bsearch p.leaf_keys key in
		if u < 0 || p.leaf_keys.(u).k <> key then raise Not_found;
		eps_e (`Done (p.leaf_keys.(u).p)) esys
       ) in
  walk_e 0L


let idx_lookup xf key =
  Plasma_client.sync (idx_lookup_e xf) key


let idx_iterate xf f =
  let rec walk_down pos =
    (* Go to the left-most leaf *)
    let pg = get_page xf pos in
    match pg with
      | `root_page p ->
	  if p.root_pointer = 0L then raise Not_found;
	  walk_down p.root_pointer
      | `int_page p ->
	  walk_down p.int_min_p
      | `leaf_page p ->
	  pos in
  let rec walk_right pos =
    (* Iterate over the leaves *)
    let pg = get_page xf pos in
    match pg with
      | `root_page _
      | `int_page _ ->
	  assert false
      | `leaf_page lp ->
	  Array.iter
	    (fun kp -> f kp.k kp.p)
	    lp.leaf_keys;
	  if lp.next_leaf_pointer <> 0L then
	    walk_right lp.next_leaf_pointer in
  try
    walk_right (walk_down 0L)
  with Not_found -> ()


type walk_return =
  | W_none
      (* Nothing to update *)
  | W_ins_key of string * int64
      (* Insert this key and idx pos *)
  | W_upd_key of string
      (* Update the key (special case, can only occur if we return from
	 the leftmost leaf after splitting it)
       *)


exception Key_exists of int64


let idx_insert xf key p replace_flag =
  let mkn = get_max_key_number xf in
  let mkl = get_max_key_length xf in
  if String.length key > mkl then
    invalid_arg "Pkv_files.idx_insert: key too long";

  let new_singleton_leaf() =
    let pg =
      `leaf_page
	{ prev_leaf_pointer = 0L;
	  next_leaf_pointer = 0L;
	  leaf_keys = [| { k = key; p = p } |]
	} in
    add_page xf pg in

  let rec walk pos =
    (* Try to locate key in the page at pos *)
    let pg = get_page xf pos in
    match pg with
      | `root_page rp ->
	  if rp.root_pointer = 0L then (
	    (* Add the very first leaf page *)
	    let pos1 = new_singleton_leaf() in
	    rp.root_pointer <- pos1;
	    touch_page xf pos pg;
	    W_none
	  )
	  else (
	    match walk rp.root_pointer with
	      | W_none -> W_none
	      | W_upd_key _ -> W_none
	      | W_ins_key(key1, pos1) ->
		  (* We have to create a first internal page *)
		  let pg_i =
		    `int_page
		      { int_min_p = rp.root_pointer;
			int_keys = [| { k = key1; p = pos1 } |]
		      } in
		  let pos_i = add_page xf pg_i in
		  rp.root_pointer <- pos_i;
		  touch_page xf pos pg;
		  W_none
	  )
      | `int_page ip ->
	  let u = bsearch ip.int_keys key in
	  let upd =
	    if u < 0 then (
	      assert(ip.int_min_p <> 0L);
	      walk ip.int_min_p
	    )
	    else
	      walk ip.int_keys.(u).p in
	  ( match upd with
	      | W_none -> W_none
	      | W_ins_key(new_key, new_p) ->
		  (* We have to add this to pg. If pg is full then split it *)
		  let n = Array.length ip.int_keys in
		  let v = bsearch ip.int_keys new_key + 1 in
 		  let new_int_keys =
		    Array.init (n+1)
		      (fun i ->
			 if i < v then ip.int_keys.(i) else
			   if i = v then { k = new_key; p = new_p } else
			     ip.int_keys.(i-1)
		      ) in
		  if n+1 <= mkn then (
		    ip.int_keys <- new_int_keys;
		    touch_page xf pos pg;
		    if v = 0 then W_upd_key new_key else W_none
		  ) else (
		    (* Split the page *)
		    let n_l = (n+1) / 2 in
		    let n_r = (n+1) - n_l in
		    let pg_l =
		      `int_page
			{ int_min_p = ip.int_min_p;
			  int_keys = Array.sub new_int_keys 0 n_l;
			} in
		    let pg_r =
		      `int_page
			{ int_min_p = 0L;
			  int_keys = Array.sub new_int_keys n_l n_r;
			} in
		    touch_page xf pos pg_l;
		    let pos_r = add_page xf pg_r in
		    W_ins_key(new_int_keys.(n_l).k, pos_r)
		  )
	      | W_upd_key upd_key ->
		  if u >= 0 then (
		    ip.int_keys.(u).k <- upd_key;
		    touch_page xf pos pg;
		  );
		  if u = 0 then W_upd_key upd_key else W_none
	  )
      | `leaf_page lp ->
	  let u = bsearch lp.leaf_keys key in
	  if u >= 0 && lp.leaf_keys.(u).k = key then (
	    if replace_flag then (
	      lp.leaf_keys.(u).p <- p;
	      touch_page xf pos pg;
	      raise Exit
	    )
	    else
	      raise (Key_exists lp.leaf_keys.(u).p);
	  );
	  let n = Array.length lp.leaf_keys in
	  let v = u+1 in
 	  let new_leaf_keys =
	    Array.init (n+1)
	      (fun i ->
		 if i < v then lp.leaf_keys.(i) else
		   if i = v then { k = key; p = p } else
		     lp.leaf_keys.(i-1)
	      ) in
	  if n+1 <= mkn then (
	    (* note that v=0 is only possible for the left-most leaf *)
	    lp.leaf_keys <- new_leaf_keys;
	    touch_page xf pos pg;
	    if v=0 then W_upd_key key else W_none
	  ) else (
	    (* Split the page *)
	    let n_l = (n+1) / 2 in
	    let n_r = (n+1) - n_l in
	    let lp_l =
		{ lp with
		    leaf_keys = Array.sub new_leaf_keys 0 n_l;
		} in
	    let lp_r =
		{ prev_leaf_pointer = pos;
		  next_leaf_pointer = lp.next_leaf_pointer;
		  leaf_keys = Array.sub new_leaf_keys n_l n_r;
		} in
	    let pos_r = add_page xf (`leaf_page lp_r) in
	    lp_l.next_leaf_pointer <- pos_r;
	    touch_page xf pos (`leaf_page lp_l);
	    W_ins_key(lp_r.leaf_keys.(0).k, pos_r)
	  )
  in
  try
    match walk 0L with
      | W_none -> ()
      | _ -> assert false
  with
    | Exit -> ()


let test_client() =
  let esys = Unixqueue.create_unix_event_system() in
  let cc = Plasma_client_config.get_config () in
  let c = Plasma_client.open_cluster_cc cc esys in
  Plasma_client.configure_buffer c 1000;
  Plasma_client.configure_auth_daemon c;
  c
