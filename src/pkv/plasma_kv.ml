(*
  Copyright 2012 Gerd Stolpmann

  This file is part of Plasma, a distributed filesystem and a
  map/reduce computation framework. Unless you have a written license
  agreement with the copyright holder (Gerd Stolpmann), the following
  terms apply:

  Plasma is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Plasma is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Plasma.  If not, see <http://www.gnu.org/licenses/>.

*)
(* $Id$ *)

open Printf

exception Error of string
  (* Error message w/o usage string. In contrast, Failure will print
     usage info
   *)

let usage() =
  prerr_endline "usage: plasma_kv <mode> [-help|options]";
  prerr_endline "";
  prerr_endline "  available modes:";
  prerr_endline "  - create";
  prerr_endline "  - iterate";
  prerr_endline "  - lookup";
  prerr_endline "  - insert";
  prerr_endline "  - delete";
  prerr_endline "  - vacuum"

let get_pw ht u =
  try
    Hashtbl.find ht u
  with Not_found ->
    try
      Sys.getenv ("PLASMA_PASSWD_" ^ u)
    with Not_found ->
      let pw = ref "" in
      Netsys_posix.with_tty
        (fun tty ->
           pw := (Netsys_posix.tty_read_password ~tty
                    ("Plasma password of " ^ u ^ "? "))
        );
      Hashtbl.add ht u !pw;
      !pw


let sugg_mem = 64 * 1024 * 1024 (* 64M *)


let access_cluster esys =
  let cluster = ref None in
  let namenodes = ref [] in
  let rep = ref 0 in
  let debug = ref false in
  let open_cluster = ref None in
  let auth = ref `None in
  let pref = ref [] in
  let local_pref = ref false in
  let args =
    [ "-cluster", Arg.String (fun s -> cluster := Some s),
      "<name>    Set the cluster name";
      
      "-namenode", Arg.String (fun n -> namenodes := n :: !namenodes),
      "<host>:<port>   Also use this namenode (can be given several times)";

      "-rep", Arg.Set_int rep,
      "<n>   Set replication factor for new files";

      "-pref", Arg.String (fun s -> pref := s :: !pref),
      "<identity>    Consider the data node with this identity as preferred place";

      "-local-pref", Arg.Set local_pref,
      "    Consider the local node as preferred place";

      "-auth", Arg.String (fun s -> auth := `Direct s),
      "<name>   Set the RPC user to authenticate as at the namenode";

      "-debug", Arg.Set debug,
      "   Enable debugging";

    ] in
  ( object
      method args = args
      method cluster = !cluster
      method namenodes = !namenodes
      method rep = !rep
      method open_cluster() =
        match !open_cluster with
          | None ->
              let nn_nodes =
                if !namenodes = [] then None else Some !namenodes in
              let cfg = Plasma_client_config.get_config
                ?clustername:!cluster
                ?nn_nodes () in
              let c = Plasma_client.open_cluster_cc cfg esys in
              open_cluster := Some c;
              ( match !auth with
                  | `None ->
                      (* Do something reasonable... *)
                      ( try
                          let ticket = Sys.getenv "PLASMAFS_AUTH_TICKET" in
                          Plasma_client.configure_auth_ticket c ticket
                        with Not_found ->
                          Plasma_client.configure_auth_daemon c
                            (* fallback: no authentication if there is no
                               reachable daemon
                             *)
                      )

                  | `Direct u ->
                      let ht = Hashtbl.create 5 in
                      Plasma_client.configure_auth c u "pnobody" (get_pw ht);
              );
              let bsize = Plasma_client.blocksize c in
              let bufs = max 1 (sugg_mem / bsize) in
              Plasma_client.configure_buffer c bufs;
              if !local_pref then (
                let local_ids = Plasma_client.local_identities c in
                pref := local_ids @ !pref
              );
              Plasma_client.configure_pref_nodes c !pref;
              c
          | Some c -> c

      method config() =
        Netlog.current_logger := 
          Netlog.channel_logger stderr (if !debug then `Debug else `Info);
        Plasma_util.debug := !debug
    end
  )


let iterate() =
  let esys = Unixqueue.create_unix_event_system() in
  let ac = access_cluster esys in
  let count = ref false in
  let dbname = ref "" in

  Arg.parse
    ( ac#args @
	[ "-db", Arg.Set_string dbname,
	  "<file>  Access this PlasmaFS key/value database file (w/o extension)";

	  "-count", Arg.Set count,
	  "  count only";
	]
    )
    (fun arg -> raise(Arg.Bad("bad arg: " ^ arg)))
    "usage: plasma_kv iterate -db /plasmafs/file";

  if !dbname = "" then
    failwith "Missing -db";

  ac#config();
  let c = ac#open_cluster() in
  let db = Pkv_api.opendb c !dbname [`Transactional] in
  let n = ref 0L in
  Pkv_api.iterate
    db
    (fun key ->
       n := Int64.succ !n;
       if not !count then
	 print_endline key
    );
  if !count then
    printf "%Ld\n" !n



let lookup() =
  let esys = Unixqueue.create_unix_event_system() in
  let ac = access_cluster esys in
  let dbname = ref "" in
  let from_stdin = ref false in
  let not_found = ref "NOT FOUND" in
  let lf = ref true in
  let interval = ref None in
  let keys = ref [] in

  Arg.parse
    ( ac#args @
	[ "-db", Arg.Set_string dbname,
	  "<file>  Access this PlasmaFS key/value database file (w/o extension)";

	  "-stdin", Arg.Set from_stdin,
	  "  read keys from stdin";

	  "-not-found", Arg.Set_string not_found,
	  "<string>   set the output string when the key is not found";

	  "-no-lf", Arg.Clear lf,
	  "   do not append LF to printed values";

	  "-interval", Arg.Int (fun n -> interval := Some n),
	  "<n>   Check every n seconds whether there are updates in the file";
	]
    )
    (fun arg -> keys := arg :: !keys)
    "usage: plasma_kv lookup -db /plasmafs/file <key> [...]";

  if !dbname = "" then
    failwith "Missing -db";

  ac#config();
  let c = ac#open_cluster() in
  let db = Pkv_api.opendb c !dbname [`Transactional] in
  let out = new Netchannels.output_channel stdout in
  let buf = String.create 65536 in
  let lookup key =
    ( try
	Pkv_api.lookup_large db key buf (fun _ -> out);
      with
	| Not_found -> out#output_string !not_found
    );
    if !lf then out#output_string "\n"
  in
	  
  List.iter lookup  (List.rev !keys);

  if !from_stdin then (
    let t_check = ref (Unix.gettimeofday()) in
    try
      while true do
	let key = input_line stdin in
	let t = Unix.gettimeofday() in
	( match !interval with
	    | None -> ()
	    | Some i ->
		if t -. !t_check >= float i then (
		  if Pkv_api.newer_version_available db then (
		    Pkv_api.abort db;
		    Pkv_api.reopen db;
		  );
		  t_check := t
		)
	);
	lookup key;
	flush stdout
      done
    with End_of_file -> ()
  )


let insert() =
  let esys = Unixqueue.create_unix_event_system() in
  let ac = access_cluster esys in
  let dbname = ref "" in
  let mode = ref `None in

  Arg.parse
    ( ac#args @
	[ "-db", Arg.Set_string dbname,
	  "<file>  Access this PlasmaFS key/value database file (w/o extension)";

	  "-files", (Arg.Unit
		       (fun () -> 
			  if !mode <> `None then
			    failwith("Ambiguous mode");
			  mode := `Files
		       )),
	  "   Read file names from stdin, and use names as key, and contents as values";

	  "-keys-and-files", (Arg.Unit
		       (fun () -> 
			  if !mode <> `None then
			    failwith("Ambiguous mode");
			  mode := `Keys_and_files
		       )),
	  "   Read lines \"<key> TAB <file>\" from stdin, and use file contents as values";
	]
    )
    (fun arg -> 
       if !mode <> `None then
	 failwith("Ambiguous mode");
       mode := `Key arg
    )
    "usage: plasma_kv insert -db /plasmafs/file [<key>]";

  if !dbname = "" then
    failwith "Missing -db";

  ac#config();
  let c = ac#open_cluster() in
  let db = Pkv_api.opendb c !dbname [`Transactional] in

  let insert_entry key in_ch =
    let len_opt = 
      try Some(LargeFile.in_channel_length in_ch)
      with Sys_error _ -> None in
    match len_opt with
      | Some len ->
	  Pkv_api.insert_large db key len
	    (new Netchannels.input_channel in_ch)
      | None ->
	  let b = Buffer.create 876 in
	  let b_ch = new Netchannels.output_buffer b in
	  let in_obj_ch = new Netchannels.input_channel in_ch in
	  b_ch # output_channel in_obj_ch;
	  let s = Buffer.contents b in
	  Pkv_api.insert db key s in

  ( match !mode with
      | `None ->
	  failwith "Don't know what to insert"
      | `Key key ->
	  insert_entry key stdin
      | `Files ->
	  ( try
	      while true do
		let line = input_line stdin in
		let in_ch = open_in line in
		insert_entry line in_ch;
		close_in in_ch
	      done
	    with
	      | End_of_file -> ()
	  )
      | `Keys_and_files ->
	  ( try
	      while true do
		let line = input_line stdin in
		( try
		    let p_tab = String.index line '\t' in
		    let key = String.sub line 0 p_tab in
		    let filename = 
		      String.sub line (p_tab+1) (String.length line-p_tab-1) in
		    let in_ch = open_in filename in
		    insert_entry key in_ch;
		    close_in in_ch
		  with
		    | Not_found ->
			failwith "No TAB found in input"
		)
	      done
	    with
	      | End_of_file -> ()
	  )
  );
  Pkv_api.commit db



let delete() =
  let esys = Unixqueue.create_unix_event_system() in
  let ac = access_cluster esys in
  let dbname = ref "" in
  let mode = ref `None in

  Arg.parse
    ( ac#args @
	[ "-db", Arg.Set_string dbname,
	  "<file>  Access this PlasmaFS key/value database file (w/o extension)";

	  "-stdin", (Arg.Unit
		       (fun () -> 
			  if !mode <> `None then
			    failwith("Ambiguous mode");
			  mode := `Stdin
		       )),
	  "   Read key to delete from stdin";

	]
    )
    (fun arg -> 
       match !mode with
	 | `Keys keys ->
	     mode := `Keys(arg::keys)
	 | `None ->
	     mode := `Keys[arg]
	 | _ ->
	     failwith("Ambiguous mode");
    )
    "usage: plasma_kv delete -db /plasmafs/file [<key> ...]";

  if !dbname = "" then
    failwith "Missing -db";

  ac#config();
  let c = ac#open_cluster() in
  let db = Pkv_api.opendb c !dbname [`Transactional] in

  ( match !mode with
      | `None ->
	  failwith "Don't know what to delete"
      | `Keys keys ->
	  List.iter
	    (fun key ->
	       Pkv_api.delete db key
	    )
	    (List.rev keys)
      | `Stdin ->
	  ( try
	      while true do
		let key = input_line stdin in
		Pkv_api.delete db key
	      done
	    with
	      | End_of_file -> ()
	  )
  );
  Pkv_api.commit db


let create() =
  let esys = Unixqueue.create_unix_event_system() in
  let ac = access_cluster esys in
  let n = ref 64 in
  let dbname = ref "" in

  Arg.parse
    ( ac#args @
	[ "-db", Arg.Set_string dbname,
	  "<file>  Create this PlasmaFS key/value database file (w/o extension)";

	  "-max-key-length", Arg.Set_int n,
	  ( sprintf "<n>   Set the maximum length of keys (default: %d)" !n);
	]
    )
    (fun arg -> raise(Arg.Bad("bad arg: " ^ arg)))
    "usage: plasma_kv create -db /plasmafs/file";

  if !dbname = "" then
    failwith "Missing -db";

  ac#config();
  let c = ac#open_cluster() in
  let db = Pkv_api.opendb c !dbname [`Create !n; `Transactional] in
  Pkv_api.commit db


let vacuum() =
  let esys = Unixqueue.create_unix_event_system() in
  let ac = access_cluster esys in
  let dbname = ref "" in

  Arg.parse
    ( ac#args @
	[ "-db", Arg.Set_string dbname,
	  "<file>  vacuum this PlasmaFS key/value database file (w/o extension)";
	]
    )
    (fun arg -> raise(Arg.Bad("bad arg: " ^ arg)))
    "usage: plasma_kv vacuum -db /plasmafs/file";

  if !dbname = "" then
    failwith "Missing -db";

  ac#config();
  let c = ac#open_cluster() in
  let db = Pkv_api.opendb c !dbname [`Transactional] in
  Pkv_api.vacuum db


let dispatch() =
  if Array.length Sys.argv < 2 then
    failwith "Bad usage";

  let mode_str = Sys.argv.(1) in
  incr Arg.current;

  match mode_str with
    | "create" -> create()
    | "delete" -> delete()
    | "insert" -> insert()
    | "lookup" -> lookup()
    | "iterate" -> iterate()
    | "vacuum" -> vacuum()
    | "help" | "-help" | "--help" -> usage()
    | _ -> failwith "bad usage"


let main () =
  try
    dispatch()
  with
    | Plasma_client.Plasma_error code ->
        prerr_endline ("plasma_kv: " ^ Plasma_util.string_of_errno code);
        exit 2

    | Unix.Unix_error(code,fn,file) ->
        prerr_endline ("plasma_kv: " ^ 
                         (if file <> "" then file ^ ": " else "") ^
                         Unix.error_message code ^ 
                         (if fn <> "" then " (" ^ fn ^ ")" else "")
                      );
        exit 2

    | Failure m ->
        prerr_endline ("plasma_kv: " ^ m);
        usage();
        exit 2

    | error ->
        prerr_endline ("plasma_kv exception: " ^ Netexn.to_string error);
        exit 2


let () = main()
