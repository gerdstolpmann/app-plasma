#use "topfind";;
#require "plasmaclient";;
#load "pkv.cma";;
open Pkv_files;;
open Pkv_api;;
open Printf;;

let c = test_client();;

(*
let t = Plasma_client.start c;;
let i = idx_open t "/pkv.idx";;

let iter() =
  idx_iterate i (fun k v -> printf "%s=%Ld\n%!" k v);;

let ins i prefix n =
  for k = 1 to n do
    idx_insert i (prefix ^ string_of_int k) (Int64.of_int k)
  done;;
 *)

let get_ii name =
  let t = Plasma_client.start c in
  let inode = Plasma_client.lookup t name false in
  let ii = Plasma_client.get_inodeinfo t inode in
  Plasma_client.abort t;
  ii ;;
