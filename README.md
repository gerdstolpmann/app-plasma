# Plasma

Plasma is a distributed filesystem and map/reduce implementation.

 - [Project page](http://plasma.camlcity.org)

 - [README](README)

 - [Installation](INSTALL)

 - [LICENSE](LICENSE)

