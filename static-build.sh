# This does a semi-static build: -lpcre, -lz, and -lpq are statically
# linked. They are taken from the ./static subdirectory if possible.

set -e

v="$(./configure -version)"
base="plasma-linux64-$v"
path="$PWD/packages/$base"

rm -rf "$path"
mkdir -p "$path"
./configure  -bindir "$path/bin" -sharedir "$path/share/plasma"

export PATH="$PWD/helpers/static-linking:$PATH"
export STATIC_LIBS="pcre z pq"
export EARLY_ARGS="-L$PWD/static"
omake
omake bin-install

mkdir -p "$path/doc/html"
cp doc/html/* "$path/doc/html"

cp `which netplex-admin` $path/bin/
cp README-linux64 LICENSE $path/

cd packages
tar czf "$base.tar.gz" "$base"
